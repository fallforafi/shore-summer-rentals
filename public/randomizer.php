<?php
use App\Functions\Functions;
use App\Models\Domain\Sql;


	# ----------------------------------------------------------------------------------------------------
	# * FILE: /randomizer.php
	# ----------------------------------------------------------------------------------------------------
//////////////////////////////////////////////////////////////////////////////////////////////////
	ini_set("html_errors", FALSE);
	////////////////////////////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////////////////////////
	/*$path = "";
	$full_name = "";
	$file_name = "";
	$full_name = $_SERVER["SCRIPT_FILENAME"];
	if (strlen($full_name) > 0) {
		$osslash = ((strtoupper(substr(PHP_OS, 0, 3)) == 'WIN') ? '\\' : '/');
		$file_pos = strpos($full_name, $osslash."cron".$osslash);
		if ($file_pos !== false) {
			$file_name = substr($full_name, $file_pos);
		}
		$path = substr($full_name, 0, (strlen($file_name)*(-1)));
	}
	if (strlen($path) == 0) $path = "..";
	define("EDIRECTORY_ROOT", $path);
	define("BIN_PATH", EDIRECTORY_ROOT."/bin");*/
	////////////////////////////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////////////////////////
	$_inCron = true;
	
    ////////////////////////////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////////////////////////

	$time_start = getmicrotime();
	////////////////////////////////////////////////////////////////////////////////////////////////////

	$sqlDomain = "	SELECT
						D.`id`, D.`database_host`, D.`database_port`, D.`database_username`, D.`database_password`, D.`database_name`, D.`url`
					FROM `Domain` AS D
					LEFT JOIN `Control_Cron` AS CC ON (CC.`domain_id` = D.`id`)
					WHERE CC.`running` = 'N'
					AND CC.`type` = 'randomizer'
					AND D.`status` = 'A'
					AND (ADDDATE(CC.`last_run_date`, INTERVAL 20 MINUTE) <= NOW() OR CC.`last_run_date` = '0000-00-00 00:00:00')
					ORDER BY
						IF (CC.`last_run_date` IS NULL, 0, 1),
						CC.`last_run_date`,
						D.`id`
					LIMIT 1";

		$resDomain = Sql::fetchMain($sqlDomain);

	if (count($resDomain) > 0) {

		$rowDomain = $resDomain;
		define("SELECTED_DOMAIN_ID", $rowDomain[0]->id);

		$sqlUpdate = "UPDATE `Control_Cron` SET `running` = 'N', `last_run_date` = NOW() WHERE `domain_id` = ".SELECTED_DOMAIN_ID." AND `type` = 'randomizer'";//
		Sql::updateSqlMain($sqlUpdate);

	 	$messageLog = "Starting cron";
       Functions::log_addCronRecord('', "randomizer", $messageLog, false, $cron_log_id);
	//////////////////////////////////////////////////////////////////////////////////////////////////
	}				
	else {
		exit;
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////

	$_inCron = false;
	////////////////////////////////////////////////////////////////////////////////////////////////////

	$last_listing_randomizer = 0;
	if (!Functions::setting_get("last_listing_randomizer_domain", $last_listing_randomizer)) {
		if (!Functions::setting_set("last_listing_randomizer_domain", "0")) {
			if (!Functions::setting_new("last_listing_randomizer_domain", "0")) {
				print "Randomizer - last_listing_randomizer error - Domain - ".SELECTED_DOMAIN_ID." - ".date("Y-m-d H:i:s")."\n";
                $messageLog = "Database error - LINE: ".__LINE__;
                Functions::log_addCronRecord('', "randomizer", $messageLog, true, $cron_log_id);
			}
		}
	}
	if (!$last_listing_randomizer) {
		$last_listing_randomizer = 0;
	}

	/*
	 * Randomizer Listings for each domain
	 *
	 ****************************************************************************************************/
    $messageLog = "Randomizer Listings - LINE: ".__LINE__;
    Functions::log_addCronRecord('', "randomizer", $messageLog, true, $cron_log_id);
	$level = implode(",", Functions::system_getLevelDetail("ListingLevel"));
	//$level = '10,30';
    unset($whereLevel);
    if ($level){
        $whereLevel  = " Listing.level IN (".$level.")"; 
    }
	
	if (config('params.LISTING_SCALABILITY_OPTIMIZATION') == "on") {
		$sql = "SELECT id, level FROM Listing WHERE `status` = 'A' ".($whereLevel ? "AND ( $whereLevel OR (Listing.is_featured = 1 AND Listing.featured_date <= NOW() AND Listing.featured_date != '000-00-00'))" : "")." ORDER BY level, id LIMIT ".$last_listing_randomizer.", ".BLOCK."";
	} else {
		$sql = "SELECT id, level FROM Listing ".($whereLevel ? "WHERE ( $whereLevel OR (Listing.is_featured = 1 AND Listing.featured_date <= NOW() AND Listing.featured_date != '000-00-00'))" : "")." ORDER BY id LIMIT ".$last_listing_randomizer.", ".BLOCK."";
	}
	$result = Sql::fetch($sql);
	$num_rows = count($result);

	$sql = "UPDATE Listing_FeaturedTemp SET status = 'D' WHERE status = 'R';";
	Sql::updateSql($sql);

	foreach ($result as $row) {

		$sqlRand = "SELECT RAND()*1000000000000000 AS RN";
		$rowRand = Sql::fetchMain($sqlRand);

		if ($row->level == 10 || $row->level == 30) {
	        $sql = "INSERT INTO Listing_FeaturedTemp VALUES (".$row->id.", ".$rowRand[0]->RN.", ".$rowRand[0]->RN.", 'I');";
			Sql::insertSql($sql);
	    }

		// if (LISTING_SCALABILITY_OPTIMIZATION != "on") {
			$sql = "UPDATE `Listing` SET `random_number` = ".$rowRand[0]->RN." WHERE id = ".$row->id;
			Sql::updateSql($sql);
			$sql = "UPDATE `Listing_Summary` SET `random_number` = ".$rowRand[0]->RN." WHERE id = ".$row->id;
			Sql::updateSql($sql);
		// }
	}

	$sql = "UPDATE Listing_FeaturedTemp SET status = 'R' WHERE status = 'I';";
			Sql::updateSql($sql);

	$sql = "DELETE FROM Listing_FeaturedTemp WHERE status = 'D';";
	Sql::deleteSql($sql);

	if ($num_rows < BLOCK) {
		if (!Functions::setting_set("last_listing_randomizer_domain", "0")) {
			print "Randomizer - last_listing_randomizer error - Domain - ".SELECTED_DOMAIN_ID." - ".date("Y-m-d H:i:s")."\n";
            $messageLog = "Database error - LINE: ".__LINE__;
            Functions::log_addCronRecord('', "randomizer", $messageLog, true, $cron_log_id);
		}
		$last_listing_randomizer = 0;
	} else {
		if (!Functions::setting_set("last_listing_randomizer_domain", ($last_listing_randomizer + BLOCK))) {
			print "Randomizer - last_listing_randomizer error - Domain - ".SELECTED_DOMAIN_ID." - ".date("Y-m-d H:i:s")."\n";
            $messageLog = "Database error - LINE: ".__LINE__;
            Functions::log_addCronRecord('', "randomizer", $messageLog, true, $cron_log_id);
		}
		$last_listing_randomizer = $last_listing_randomizer + BLOCK;
	}


	$sqlUpdate = "UPDATE `Control_Cron` SET `running` = 'N' WHERE `domain_id` = ".SELECTED_DOMAIN_ID." AND `type` = 'randomizer'";
	Sql::updateSqlMain($sqlUpdate);

	////////////////////////////////////////////////////////////////////////////////////////////////////
	$time_end = getmicrotime();
	$time = $time_end - $time_start;
	print "Randomizer on Domain - ".SELECTED_DOMAIN_ID." - ".date("Y-m-d H:i:s")." - ".round($time, 2)." seconds.\n";
	if (!Functions::setting_set("last_datetime_randomizer", date("Y-m-d H:i:s"))) {
		if (!Functions::setting_new("last_datetime_randomizer", date("Y-m-d H:i:s"))) {
			print "last_datetime_randomizer error - Domain - ".SELECTED_DOMAIN_ID." - ".date("Y-m-d H:i:s")."\n";
            $messageLog = "Database error - LINE: ".__LINE__;
            Functions::log_addCronRecord('', "randomizer", $messageLog, true, $cron_log_id);
		}
	}
    $messageLog = "Cron finished";
    Functions::log_addCronRecord('', "randomizer", $messageLog, true, $cron_log_id, true, round($time, 2));
	////////////////////////////////////////////////////////////////////////////////////////////////////
?>
