<?php	
use App\Functions\Functions;
use App\Models\Domain\Sql;

	# * FILE: /export_listings.php
	# ----------------------------------------------------------------------------------------------------

	ini_set("html_errors", FALSE);

////////////////////////////////////////////////////////////////////////////////////////////////////
	/*$path = url();
	$full_name = "";
	$file_name = "";
	$full_name = $_SERVER["SCRIPT_FILENAME"];
	if (strlen($full_name) > 0) {
		$osslash = ((strtoupper(substr(PHP_OS, 0, 3)) == 'WIN') ? '\\' : '/');
		$file_pos = strpos($full_name, $osslash."cron".$osslash);
		if ($file_pos !== false) {
			$file_name = substr($full_name, $file_pos);
		}
		$path = substr($full_name, 0, (strlen($file_name)*(-1)));
	}
	if (strlen($path) == 0) $path = "/var/www/html";
	define("BIN_PATH", EDIRECTORY_ROOT."/bin");
	////////////////////////////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////////////////////////
	

	////////////////////////////////////////////////////////////////////////////////////////////////////
	function getmicrotime() {
		list($usec, $sec) = explode(" ", microtime());
		return ((float)$usec + (float)$sec);
	}
	$time_start = getmicrotime();*/
	////////////////////////////////////////////////////////////////////////////////////////////////////
	$_inCron = true;
    $loadSitemgrLangs = true;

	//////////////////////////////////////////////////////////////////////////////////////////////////

	$sql_check_export = "SELECT
							D.`database_host`, D.`database_port`, D.`database_username`, D.`database_password`, D.`database_name`, CEL.`id`, CEL.`type`, CEL.`filename`, CEL.`domain_id`, CEL.`block`, CEL.`total_listing_exported`
						FROM `Domain` AS D
						LEFT JOIN `Control_Export_Listing` AS CEL ON (CEL.`domain_id` = D.`id`)
						WHERE CEL.`scheduled` = 'Y'
						AND D.`status` = 'A'
						ORDER BY
							IF (CEL.`last_run_date` IS NULL, 0, 1),
							CEL.`last_run_date`,
							D.`id`
						LIMIT 1";
	//d($sql_check_export);die;

	$result_check_export = Sql::fetchMain($sql_check_export);

	if (count($result_check_export) > 0) {
		$row = $result_check_export;
		$type = $row[0]->type;
		$filename = $row[0]->filename;
		$domain_id = $row[0]->domain_id;
		
		$limit = $row[0]->block;
		$start = $row[0]->total_listing_exported;
		$end = $limit;

		$exportFilePath = public_path()."/custom/domain_$domain_id/import_files";
        
        define("SELECTED_DOMAIN_ID", $domain_id);
     
        $messageLog = "Starting cron";
        Functions::log_addCronRecord('', "export_listings", $messageLog, false, $cron_log_id);
		
		$files = glob($exportFilePath."/export_*.progress");
		
		if ($files[0] && is_array($files)) {
			foreach ($files as $file) {
				if (strrpos($file, "export_".str_replace(".zip", ".progress", $filename)) === false) {
					if (unlink($file)) {
                        $messageLog = "Remove .progress file $file. - LINE: ".__LINE__;
                        Functions::log_addCronRecord('', "export_listings", $messageLog, true, $cron_log_id);
                    } else {
                        $messageLog = "Unable to unlink .progress file $file. Check permissions. - LINE: ".__LINE__;
                        Functions::log_addCronRecord('', "export_listings", $messageLog, true, $cron_log_id);
                    }
				}
			}
		}
		
	} else {
		exit;
	}

	$_inCron = false;

	////////////////////////////////////////////////////////////////////////////////////////////////////
	/*function getmicrotime() {
		list($usec, $sec) = explode(" ", microtime());
		return ((float)$usec + (float)$sec);
	}
	$time_start = getmicrotime();*/
	////////////////////////////////////////////////////////////////////////////////////////////////////
		
	if ($type == "csv") {
		Functions::export_ExportToCSV("listing", 'edirectory_sample1.csv', false, $domain_id);



	} else if ($type == "csv - data") {
		$sqlCListing = "SELECT COUNT(`id`) AS `total` FROM `Listing`";
		$resCListing = Sql::fetch($sqlCListing);
		$rowCListing = $resCListing;
		$count = $rowCListing[0]->total;

		if ($start >= $count) {
            print "Export Listings on Domain ".SELECTED_DOMAIN_ID." - ".date("Y-m-d H:i:s")." - ".round($time, 2)." seconds.\n";
            $messageLog = "Cron finished";
            exit;
        }

		$request["domain_id"] = $domain_id;
		$request["item_type"] = "Listing";
		$request["fields_excluded"] = "image_id, thumb_id, promotion_id, discount_id, video_snippet, custom_checkbox0, custom_checkbox1, custom_checkbox2, custom_checkbox3, custom_checkbox4, custom_checkbox5, custom_checkbox6, custom_checkbox7, custom_checkbox8, custom_checkbox9, custom_dropdown0, custom_dropdown1, custom_dropdown2, custom_dropdown3, custom_dropdown4, custom_dropdown5, custom_dropdown6, custom_dropdown7, custom_dropdown8, custom_dropdown9, custom_text0, custom_text1, custom_text2, custom_text3, custom_text4, custom_text5, custom_text6, custom_text7, custom_text8, custom_text9, custom_short_desc0, custom_short_desc1, custom_short_desc2, custom_short_desc3, custom_short_desc4, custom_short_desc5, custom_short_desc6, custom_short_desc7, custom_short_desc8, custom_short_desc9, custom_long_desc0, custom_long_desc1, custom_long_desc2, custom_long_desc3, custom_long_desc4, custom_long_desc5, custom_long_desc6, custom_long_desc7, custom_long_desc8, custom_long_desc9, listingtemplate_id, importID";
		$request["path"] = $exportFilePath;
		$request["export_from"] = "cron";
		$request["zip_filename"] = $filename;
		
		$block = round($count / $limit, 1);
		if ($block < 1) $block = 1;
		
		if ($start) $step = round($start / $limit);
		else $step = 0;

		$request["item_current"] = $start;
		$request["item_limit"] = $limit;
		$request["item_start"] = $start;
		$request["item_end"] = $end;
		$request["item_count"] = $count;
		$request["item_block"] = $block;
		$request["item_step"] = $step;
		$request["file_extension"] = "csv";
		
		$exportObj = new Export($request);
		$exportObj->execute();
	}

	$time_end = getmicrotime();
	$time = $time_end - $time_start;
	print "Export Listings on Domain ".SELECTED_DOMAIN_ID." - ".date("Y-m-d H:i:s")." - ".round($time, 2)." seconds.\n";
    $messageLog = "Cron finished";
    Functions::log_addCronRecord('', "export_listings", $messageLog, true, $cron_log_id, true, round($time, 2));
?>