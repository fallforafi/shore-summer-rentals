<?php	
use App\Functions\Functions;
use App\Models\Domain\Sql;

	# * FILE: sitemap.php
	# ----------------------------------------------------------------------------------------------------
	
	ini_set("html_errors", FALSE);
	

	////////////////////////////////////////////////////////////////////////////////////////////////////
	/*$path = url();
	$full_name = "";
	$file_name = "";
	$full_name = $_SERVER["SCRIPT_FILENAME"];
	if (strlen($full_name) > 0) {
		$osslash = ((strtoupper(substr(PHP_OS, 0, 3)) == 'WIN') ? '\\' : '/');
		$file_pos = strpos($full_name, $osslash."cron".$osslash);
		if ($file_pos !== false) {
			$file_name = substr($full_name, $file_pos);
		}
		$path = substr($full_name, 0, (strlen($file_name)*(-1)));
	}
	if (strlen($path) == 0) $path = "/var/www/html";
	define("BIN_PATH", EDIRECTORY_ROOT."/bin");
	////////////////////////////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////////////////////////
	

	////////////////////////////////////////////////////////////////////////////////////////////////////
	function getmicrotime() {
		list($usec, $sec) = explode(" ", microtime());
		return ((float)$usec + (float)$sec);
	}
	$time_start = getmicrotime();*/
	////////////////////////////////////////////////////////////////////////////////////////////////////
	$_inCron = true;
	

	

   	$sqlDomain = "	SELECT
						D.`id`
					FROM `Domain` AS D
					LEFT JOIN `Control_Cron` AS CC ON (CC.`domain_id` = D.`id`)
					WHERE CC.`running` = 'N'
					AND CC.`type` = 'sitemap'
					AND D.`status` = 'A'
					AND (ADDDATE(CC.`last_run_date`, INTERVAL 1 DAY) <= NOW() OR CC.`last_run_date` = '0000-00-00 00:00:00')
					ORDER BY
						IF (CC.`last_run_date` IS NULL, 0, 1),
						CC.`last_run_date`,
						D.`id`
					LIMIT 1";

	$resDomain = Sql::fetchMain($sqlDomain);

	if (count($resDomain) > 0) {

		$rowDomain = $resDomain;
		define("SELECTED_DOMAIN_ID", $rowDomain[0]->id);


	 $messageLog = "Starting cron";
     Functions::log_addCronRecord('', "sitemap", $messageLog, false, $cron_log_id);
	//////////////////////////////////////////////////////////////////////////////////////////////////
	} else {
		exit;
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////

	$_inCron = false;
	////////////////////////////////////////////////////////////////////////////////////////////////////

	define("EDIRECTORY_ROOTs",public_path());

    if(file_exists(EDIRECTORY_ROOT.'/sitemap.xml')) {
    	unlink(EDIRECTORY_ROOT.'/sitemap.xml');
    }
    Functions::sitemap_createFile();
    Functions::sitemapgen_makeSitemap(EDIRECTORY_ROOTs);
    //Functions::sitemapgen_makeSitemapNews(EDIRECTORY_ROOT);
	Functions::sitemap_finishFile();
    ////////////////////////////////////////////////////////////////////////////////////////////////////

    $sqlUpdate = "UPDATE `Control_Cron` SET `running` = 'N' WHERE `domain_id` = ".SELECTED_DOMAIN_ID." AND `type` = 'sitemap'";
	Sql::updateSqlMain($sqlUpdate);

    ////////////////////////////////////////////////////////////////////////////////////////////////////
    $time_end = getmicrotime();
    $time = $time_end - $time_start;
    print "Sitemap Generator on Domain ".SELECTED_DOMAIN_ID." - ".date("Y-m-d H:i:s")." - ".round($time, 2)." seconds.\n";
    if (!Functions::setting_set("last_datetime_sitemap", date("Y-m-d H:i:s"))) {
        if (!Functions::setting_new("last_datetime_sitemap", date("Y-m-d H:i:s"))) {
            print "last_datetime_sitemap error - Domain - ".SELECTED_DOMAIN_ID." - ".date("Y-m-d H:i:s")."\n";
            $messageLog = "Database error - LINE: ".__LINE__;
            Functions::log_addCronRecord('', "sitemap", $messageLog, true, $cron_log_id);
        }
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////

    $messageLog = "Cron finished";
    Functions::log_addCronRecord('', "sitemap", $messageLog, true, $cron_log_id, true, round($time, 2));
?>
