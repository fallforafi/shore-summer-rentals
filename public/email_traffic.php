<?php	
use App\Functions\Functions;
use App\Models\Domain\Sql;
use App\Functions\Contact;
use App\Functions\ListingLevel;


	

	$sqlDomain = "	SELECT
						D.`id`, D.`database_host`, D.`database_port`, D.`database_username`, D.`database_password`, D.`database_name`, D.`url`
					FROM `Domain` AS D
					LEFT JOIN `Control_Cron` AS CC ON (CC.`domain_id` = D.`id`)
					WHERE CC.`running` = 'N'
					AND CC.`type` = 'email_traffic'
					AND D.`status` = 'A'
					AND (ADDDATE(CC.`last_run_date`, INTERVAL 20 MINUTE) <= NOW() OR CC.`last_run_date` = '0000-00-00 00:00:00')
					ORDER BY
						IF (CC.`last_run_date` IS NULL, 0, 1),
						CC.`last_run_date`,
						D.`id`
					LIMIT 1";

	$resDomain = Sql::fetchMain($sqlDomain);

	if (count($resDomain) > 0) {

		$rowDomain = $resDomain;
		define("SELECTED_DOMAIN_ID", $rowDomain[0]->id);


		$sqlUpdate = "UPDATE `Control_Cron` SET `running` = 'Y', `last_run_date` = NOW() WHERE `domain_id`= ".SELECTED_DOMAIN_ID." AND `type` = 'email_traffic'";
	 Sql::updateSqlMain($sqlUpdate);
		
	

	        $messageLog = "Starting cron";
     Functions::log_addCronRecord('', "email_traffic", $messageLog, false, $cron_log_id);
	//////////////////////////////////////////////////////////////////////////////////////////////////
	} else {
		exit;
	}

	Functions::setting_get("sitemgr_email", $sitemgr_email);
	Functions::setting_get("edir_default_language", $edir_default_language);

	//////////////////////////////////////////////////////////////////////////////////////////////////
	$last_listing_traffic = 0;
	if (!Functions::setting_get("last_listing_traffic", $last_listing_traffic)) {
		if (!Functions::setting_set("last_listing_traffic", "0")) {
			if (!Functions::setting_new("last_listing_traffic", "0")) {
				print "Email Traffic - last_listing_traffic error - Domain - 1 - ".date("Y-m-d H:i:s")."\n";
                $messageLog = "Database error - LINE: ".__LINE__;
                Functions::log_addCronRecord('', "email_traffic", $messageLog, true, $cron_log_id);
			}
		}
	}
	if (!$last_listing_traffic) {
		$last_listing_traffic = 0;
	}
    
	unset($allNot);
	$sqlNot = "SELECT * FROM Email_Notification WHERE deactivate = '0' AND id = ".config('params.SYSTEM_EMAIL_TRAFFIC');
	$resultNot = Sql::fetch($sqlNot);
	if (count($resultNot) > 0){
        $messageLog = "Read Email Notification info - LINE: ".__LINE__;
        Functions::log_addCronRecord('', "email_traffic", $messageLog, true, $cron_log_id);
		foreach ($resultNot as $rowNot) {
			$allNot[$edir_default_language]["bcc"] = $rowNot->bcc;
			$allNot[$edir_default_language]["content_type"] = $rowNot->content_type;
			$allNot[$edir_default_language]["body"] = $rowNot->body;
			$allNot[$edir_default_language]["subject"] = $rowNot->subject;
		}
	} else {
        $messageLog = "Email Notification disabled - LINE: ".__LINE__;
        Functions::log_addCronRecord('', "email_traffic", $messageLog, true, $cron_log_id);
    }
	if ($allNot && (count($allNot) > 0)) {
        $messageLog = "Select accounts to send email - LINE: ".__LINE__;
        Functions::log_addCronRecord('', "email_traffic", $messageLog, true, $cron_log_id);
        
        $allNotSQL = "(DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 30 DAY), '%Y%m%d') >= DATE_FORMAT(last_traffic_sent, '%Y%m%d'))";
		$sqlAccount = "SELECT id FROM Account WHERE notify_traffic_listing = 'y' AND is_sponsor = 'y'";
		$resultAccount = Sql::fetchMain($sqlAccount);
		$idsAccount = "";
		if (count($resultAccount) > 0){
			foreach ($resultAccount as $rowAcc) {
				$idsAccount .= $rowAcc->id.",";
			}
		}
		
		$idsAccount = Functions::string_substr($idsAccount, 0, -1);
		
		$listingLevelObj = new ListingLevel();
		$listingLevelObj->ListingLevel();

		$levelValue = $listingLevelObj->getValues();
		$idsLevels = "";

		foreach ($levelValue as $value) {
		//Functions::setting_get("email_traffic_listing_".$value, ${"email_traffic_listing_".$value});
			//if (${"email_traffic_listing_".$value}){
				$idsLevels .= $value.",";
			//}
		}

		$idsLevels = Functions::string_substr($idsLevels, 0, -1);

		if ($idsAccount && $idsLevels){
			$sql = 	"".
					" SELECT ".
					" id, ".
					" account_id ".
					" FROM ".
					" Listing ".
					" WHERE ".
					" account_id IN (".$idsAccount.") ".
					" AND ".
					" status = 'P' ".
					" AND ".
					" level IN (".$idsLevels.") ".
					" AND ".$allNotSQL.
					" ORDER BY ".
					" id ".
					" LIMIT ".
					$last_listing_traffic.", ".BLOCK."";
			$result = Sql::fetch($sql);
			$num_rows = count($result);
			$default_url=url();
			if (count($result) > 0){

                $messageLog = "Send email to accounts - LINE: ".__LINE__;
                Functions::log_addCronRecord('', "email_traffic", $messageLog, true, $cron_log_id);
				foreach ($result as $row) {

					$contactObj = new Contact();
					$contactObj->Contact($row->account_id);

					if ($contactObj->getString("email")) {

						$email_data["body"]         = $allNot[$edir_default_language]["body"];
						$email_data["subject"]      = $allNot[$edir_default_language]["subject"];
						$email_data["bcc"]          = $allNot[$edir_default_language]["bcc"];
						$email_data["content_type"] = $allNot[$edir_default_language]["content_type"];

						$email_data["subject"] = str_replace("DEFAULT_URL", $default_url, $email_data["subject"]);
						$email_data["body"]    = str_replace("DEFAULT_URL", $default_url, $email_data["body"]);

						if ($email_data["content_type"] == "text/plain"){
							$email_data["body"] = nl2br($email_data["body"]);
						}

						$data_email = Sql::retrieveListingReport(4474);
						$table = Functions::report_PrepareListingStatsReviewToEmail($data_email, $row->id, "listing", $default_url);//$non_secure_url


						$email_data["body"] = str_replace("[TABLE_STATS]", $table, $email_data["body"]);

						$email_data["subject"] = Functions::system_replaceEmailVariables($email_data["subject"], $row->id, "listing");
						$email_data["body"]    = Functions::system_replaceEmailVariables($email_data["body"], $row->id, "listing");

						$to           = 'amoos@golpik.com';//$contactObj->getString("email");
						$from_email   = $sitemgr_email;
						$from_name    = 'Amoos';//EDIRECTORY_TITLE
						$bcc          = 'amoosjohn@gmail.com';//$email_data["bcc"]
						$subject      = $email_data["subject"];
						$message      = $email_data["body"];
						$content_type = $email_data["content_type"];

						if ($content_type == "text/plain"){
							$style = "<html>
										<body>
											<div>";

							$style2 = "		</div>
										</body>
										</html>";

							$message = $style.$message.$style2;
							$content_type = "text/html";
						}

						$error = false;
                        if ($table) {

                        	$data= [
            'sitemgr_msg' => $message,
            'subject' => $subject,
            'email_send' =>$to,
            'bcc' =>$bcc
        ];

    Mail::send('email.owner',['data' => $data], function($message) use ($data) {
   
        $message->from('amoosjohn@gmail.com');
     $message->to($data['email_send'])->bcc($data['bcc'])->subject($data['subject']);
    });	

                            //system_mail($to, $subject, $message, $from_name." <".$from_email.">", $content_type, '', '', $error);
                            if ($bcc) {

                                //system_mail($bcc, $subject, $message, $from_name." <".$from_email.">", $content_type, '', '', $error);
                            }
                        }

					}

					$sql = "UPDATE Listing SET last_traffic_sent = NOW() WHERE id = ".$row->id."";
					

				}
			}
		}
	}

	if ($num_rows < BLOCK) {
		if (!Functions::setting_set("last_listing_traffic", "0")) {
			print "Email Traffic - last_listing_traffic error - Domain - 1 - ".date("Y-m-d H:i:s")."\n";
            $messageLog = "Database error - LINE: ".__LINE__;
            Functions::log_addCronRecord('', "email_traffic", $messageLog, true, $cron_log_id);
		}
		$last_listing_traffic = 0;
	} else { 
		if (!Functions::setting_set("last_listing_traffic", ($last_listing_traffic + BLOCK))) {
			print "Email Traffic - last_listing_traffic error - Domain - 1 - ".date("Y-m-d H:i:s")."\n";
            $messageLog = "Database error - LINE: ".__LINE__;
            Functions::log_addCronRecord('', "email_traffic", $messageLog, true, $cron_log_id);
		}
		$last_listing_traffic = $last_listing_traffic + BLOCK;
	}

	$sqlUpdate = "UPDATE `Control_Cron` SET `running` = 'N' WHERE `domain_id` = '1' AND `type` = 'email_traffic'";
	Sql::updateSqlMain($sqlUpdate);

	////////////////////////////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////////////////////////
	$time_end = getmicrotime();
	$time = $time_end - $time_start;
	print "Email Traffic on Domain 1 - ".date("Y-m-d H:i:s")." - ".round($time, 2)." seconds.\n";
	if (!Functions::setting_set("last_datetime_listingtraffic", date("Y-m-d H:i:s"))) {
		if (!Functions::setting_new("last_datetime_listingtraffic", date("Y-m-d H:i:s"))) {
			print "last_datetime_listingtraffic error - Domain - 1 - ".date("Y-m-d H:i:s")."\n";
            $messageLog = "Database error - LINE: ".__LINE__;
            Functions::log_addCronRecord('', "email_traffic", $messageLog, true, $cron_log_id);
		}
	}
    $messageLog = "Cron finished";
    Functions::log_addCronRecord('', "email_traffic", $messageLog, true, $cron_log_id, true, round($time, 2));
	////////////////////////////////////////////////////////////////////////////////////////////////////

?>