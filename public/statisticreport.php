#!/usr/bin/php -q
<?php	
use App\Functions\Functions;
use App\Models\Domain\Sql;
use App\Functions\ListingCategory;
use App\Functions\Location1;
use App\Functions\Location3;
use App\Functions\Location4;
	/*==================================================================*\
	######################################################################
	#                                                                    #
	# Copyright 2005 Arca Solutions, Inc. All Rights Reserved.           #
	#                                                                    #
	# This file may not be redistributed in whole or part.               #
	# eDirectory is licensed on a per-domain basis.                      #
	#                                                                    #
	# ---------------- eDirectory IS NOT FREE SOFTWARE ----------------- #
	#                                                                    #
	# http://www.edirectory.com | http://www.edirectory.com/license.html #
	######################################################################
	\*==================================================================*/

	# --------------------------------------------------------------------------------------------------
	# * FILE: cron/statisticreport.php
	# --------------------------------------------------------------------------------------------------
	
    /*function getmicrotime() {
        list($usec, $sec) = explode(" ", microtime());
        return ((float)$usec + (float)$sec);
    }*/
	if (!isset($_GET['refresh'])) {

		////////////////////////////////////////////////////////////////////////////////////////////////////
		ini_set("html_errors", FALSE);
		////////////////////////////////////////////////////////////////////////////////////////////////////

	
		$_inCron = true;
	

		////////////////////////////////////////////////////////////////////////////////////////////////////
		
		$time_start = getmicrotime();
		////////////////////////////////////////////////////////////////////////////////////////////////////
			$sqlDomain = "	SELECT
							D.`id`, D.`database_host`, D.`database_port`, D.`database_username`, D.`database_password`, D.`database_name`, D.`url`
						FROM `Domain` AS D
						LEFT JOIN `Control_Cron` AS CC ON (CC.`domain_id` = D.`id`)
						WHERE CC.`running` = 'N'
						AND CC.`type` = 'statisticreport'
						AND D.`status` = 'A'
						AND (ADDDATE(CC.`last_run_date`, INTERVAL 1 DAY) <= NOW() OR CC.`last_run_date` = '0000-00-00 00:00:00')
						ORDER BY
							IF (CC.`last_run_date` IS NULL, 0, 1),
							CC.`last_run_date`,
							D.`id`
						LIMIT 1";

	$resDomain = Sql::fetchMain($sqlDomain);

	if (count($resDomain) > 0) {

		$rowDomain = $resDomain;
		define("SELECTED_DOMAIN_ID", $rowDomain[0]->id);

		$sqlUpdate = "UPDATE `Control_Cron` SET `running` = 'N', `last_run_date` = NOW() WHERE `domain_id` = ".SELECTED_DOMAIN_ID." AND `type` = 'statisticreport'";//
		Sql::updateSqlMain($sqlUpdate);

	 	$messageLog = "Starting cron";
       Functions::log_addCronRecord('', "statisticreport", $messageLog, false, $cron_log_id);
	//////////////////////////////////////////////////////////////////////////////////////////////////
	} else {
		exit;
	}
		$_inCron = false;
		//////////////////////////////////////////////////////////////////////////////////////////////

		$_inCron = true;
	}

	if (!$_inCron) {
		////////////////////////////////////////////////////////////////////////////////////////////////////
		$time_start = getmicrotime();
		//////////////////////////////////////////////////////////////////////////////////////////////
		
	}

	# --------------------------------------------------------------------------------------------------
	# getting the start date and time
	# --------------------------------------------------------------------------------------------------
	$cron_startdate = date('Y-m-d H:i:s');

	# --------------------------------------------------------------------------------------------------
	# top Keywords
	# --------------------------------------------------------------------------------------------------
    $messageLog = "Top Keywords - LINE: ".__LINE__;
    Functions::log_addCronRecord('', "statisticreport", $messageLog, true, $cron_log_id);
	$sql = "SELECT DATE(search_date) AS day, keyword, module, count(keyword) as quantity FROM Report_Statistic WHERE `search_date` <= NOW() AND keyword <> '' GROUP BY keyword, module ORDER BY search_date ASC, module ASC, quantity DESC, keyword ASC";
	$results = Sql::fetch($sql);

	foreach ($results as $row) {
		$sql = "UPDATE Report_Statistic_Daily SET `quantity`=(`quantity`+".$row->quantity.") WHERE `day` = '".$row->day."' AND `module` = '".$row->module."' AND `key` = 'keywords' AND `value` = '".$row->keyword."' LIMIT 1";
		$affected=Sql::updateSql($sql);
		if (!$affected) {
			$sql = "INSERT INTO Report_Statistic_Daily VALUES ('".$row->day.",'".$row->module."','keywords','".$row->keyword."',".$row->quantity.")";
			Sql::insertSql($sql);
		}
	}


	# --------------------------------------------------------------------------------------------------
	# top Where
	# --------------------------------------------------------------------------------------------------
    $messageLog = "Top Where - LINE: ".__LINE__;
    Functions::log_addCronRecord('', "statisticreport", $messageLog, true, $cron_log_id);
	$sql = "SELECT DATE(search_date) AS day, search_where, module, count(search_where) as quantity FROM Report_Statistic WHERE `search_date` <= NOW() AND search_where <> '' GROUP BY search_where, module ORDER BY search_date ASC, module ASC, quantity DESC, search_where ASC";
	$results = Sql::fetch($sql);
	foreach ($results as $row) {
		$sql = "UPDATE Report_Statistic_Daily SET `quantity`=(`quantity`+".$row->quantity.")  WHERE `day` = '".$row->day."' AND `module` = '".$row->module."' AND `key` = 'where' AND `value` = '".$row->search_where."' LIMIT 1";
		$affected=Sql::updateSql($sql);
		if (!$affected) {
			$sql = "INSERT INTO Report_Statistic_Daily VALUES ('".$row->day."','".$row->module."','where','".$row->search_where."', ".$row->quantity.")";
			Sql::insertSql($sql);
		}
	}

	# ----------------------------------------------------------------------------------------------------
	# top Category
	# ----------------------------------------------------------------------------------------------------
    $messageLog = "Top Category - LINE: ".__LINE__;
    Functions::log_addCronRecord('', "statisticreport", $messageLog, true, $cron_log_id);
	$sql = "SELECT DATE(search_date) AS day, category_id, module, count(category_id) as quantity FROM Report_Statistic WHERE `search_date` <= NOW() AND category_id > 0 GROUP BY category_id, module ORDER BY search_date ASC, category_id ASC, module ASC";
	$results = Sql::fetch($sql);

	$listingCategory    = new ListingCategory();
	/*$eventCategory      = new EventCategory();
	$classifiedCategory = new ClassifiedCategory();
	$articleCategory	= new ArticleCategory();
	$blogCategory		= new BlogCategory();*/

	foreach ($results as $row) {

		if($row->module == 'l') {
			$listingCategory->setNumber('id', $row->category_id);
			$categoriesArray = $listingCategory->getFullPath();
		}

		/*if($row->module == 'e') {
			$eventCategory->setNumber('id', $row->category_id);
			$categoriesArray = $eventCategory->getFullPath();
		}

		if($row->module == 'c') {
			$classifiedCategory->setNumber('id', $row->category_id);
			$categoriesArray = $classifiedCategory->getFullPath();
		}

		if($row->module == 'a') {
			$articleCategory->setNumber('id', $row->category_id);
			$categoriesArray = $articleCategory->getFullPath();
		}

		if($row->module == 'p') {
			$blogCategory->setNumber('id', $row->category_id);
			$categoriesArray = $blogCategory->getFullPath();
		}
        */
        if($row->module == 'd') {
			$listingCategory->setNumber('id', $row->category_id);
			$categoriesArray = $listingCategory->getFullPath();
		}

		$categoryPath = array();
		foreach($categoriesArray as $eachCategory) {
			$categoryPath[] = $eachCategory['title'];
		}
		$categoryTitle = implode(" >> ", $categoryPath);

		if (Functions::string_strlen(trim($categoryTitle))) {
			$sql = "UPDATE Report_Statistic_Daily SET `quantity`=(`quantity`+".$row->quantity.") WHERE `day` = '".$row->day."' AND `module` = '".$row->module."' AND `key` = 'categories' AND `value` = '".$categoryTitle."' LIMIT 1";
			$affected=Sql::updateSql($sql);
			if (!$affected) {
				$sql = "INSERT INTO Report_Statistic_Daily VALUES ('".$row->day."','".$row->module."','categories','".$categoryTitle."', ".$row->quantity.")";
				Sql::insertSql($sql);
			}
		}

	}

	unset($listingCategory);
	//unset($eventCategory);
	//unset($classifiedCategory);
	//unset($articleCategory);

	# ----------------------------------------------------------------------------------------------------
	# top Locations
	# ----------------------------------------------------------------------------------------------------
	$messageLog = "Top Locations - LINE: ".__LINE__;
    Functions::log_addCronRecord('', "statisticreport", $messageLog, true, $cron_log_id);
	$_locations = explode(",", config('params.EDIR_LOCATIONS'));
	
	$location_coluns_array="";
	foreach ($_locations as $_location_level) {

		$objLocationLabel = "Location".$_location_level;
		//${"Location".$_location_level} = new $objLocationLabel;
		$Location1= new Location1();
		$Location3= new Location3();
		$Location4= new Location4();
		$location_coluns_array[]="location_".$_location_level;	
	}

	$location_coluns = implode(", ", $location_coluns_array);
	
	$sql = "SELECT DATE(search_date) AS day, ".$location_coluns.", module, count(".$location_coluns_array[0].") as quantity FROM Report_Statistic WHERE `search_date` <= NOW() AND ".$location_coluns_array[0]." > 0 GROUP BY ".$location_coluns.", module ORDER BY search_date ASC";

	$results = Sql::fetch($sql) or die(mysql_error());
	foreach ($results as $row){

		$locationPath = array();

		foreach ($_locations as $_location_level) {			
			if('location_'.$_location_level > 0) {
				${"Location".$_location_level}->setNumber('id', 'location_'.$_location_level);
				$getLocation = ${"Location".$_location_level}->retrieveLocationById();
				$locationPath[] = $getLocation['name'];				
			}
		}

		if ( !in_array(null, $locationPath, true) ) { 

			$location = implode(" >> ", $locationPath);
			
			if (Functions::string_strlen(trim($location))) {
				$sql = "UPDATE Report_Statistic_Daily SET `quantity`=(`quantity`+".$row->quantity.") WHERE `day` = '".$row->day."' AND `module` = '".$row->module."' AND `key` = 'locations' AND `value` = ".$location." LIMIT 1";
			$affected=Sql::updateSql($sql);
			if (!$affected) {
					$sql = "INSERT INTO Report_Statistic_Daily VALUES ('".$row->day."','".$row->module."','locations',".$location.", ".$row->quantity.")";
					Sql::insertSql($sql);
				}
			}
		}
	}

	foreach ($_locations as $_location_level)
		unset (${"Location".$_location_level});

	# ----------------------------------------------------------------------------------------------------
	# clear old data
	# ----------------------------------------------------------------------------------------------------
    $messageLog = "Clear Old Data - LINE: ".__LINE__;
    Functions::log_addCronRecord('', "statisticreport", $messageLog, true, $cron_log_id);
	$sql = "DELETE FROM `Report_Statistic` WHERE `search_date` <= NOW()";
	d($sql);
	Sql::deleteSql($sql) or die(mysql_error());

	$sqlUpdate = "UPDATE `Control_Cron` SET `running` = 'N' WHERE `domain_id` = ".SELECTED_DOMAIN_ID." AND `type` = 'statisticreport'";
	Sql::updateSqlMain($sqlUpdate);
	
	//////////////////////////////////////////////////////////////////////////////////////////////////
	$time_end = getmicrotime();
	$time = $time_end - $time_start;
	if (!isset($_GET['refresh'])) {
        print "Process Statistic on Domain ".SELECTED_DOMAIN_ID." - ".date("Y-m-d H:i:s")." - ".round($time, 2)." seconds.\n"; 
    } else {
        print date("Y-m-d H:i:s")." - ".round($time, 2);
    }
	if (!Functions::setting_set("last_datetime_statisticreport", date("Y-m-d H:i:s"))) {
		if (!Functions::setting_new("last_datetime_statisticreport", date("Y-m-d H:i:s"))) {
			print "last_datetime_statisticreport error - Domain - ".SELECTED_DOMAIN_ID." - ".date("Y-m-d H:i:s")."\n";
            $messageLog = "Database error - LINE: ".__LINE__;
            Functions::log_addCronRecord('', "statisticreport", $messageLog, true, $cron_log_id);
		}
	}
    $messageLog = "Cron finished";
    Functions::log_addCronRecord('', "statisticreport", $messageLog, true, $cron_log_id, true, round($time, 2));
	
?>