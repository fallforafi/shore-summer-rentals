<?php
use App\Functions\Functions;
use App\Models\Domain\Sql;



	# ----------------------------------------------------------------------------------------------------
	# * FILE:location_update.php
	# ----------------------------------------------------------------------------------------------------

	////////////////////////////////////////////////////////////////////////////////////////////////////
	ini_set("html_errors", FALSE);
	////////////////////////////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////////////////////////

	/*$path = "";
	$full_name = "";
	$file_name = "";
	$full_name = $_SERVER["SCRIPT_FILENAME"];
	if (strlen($full_name) > 0) {
		$osslash = ((strtoupper(substr(PHP_OS, 0, 3)) == 'WIN') ? '\\' : '/');
		$file_pos = strpos($full_name, $osslash."cron".$osslash);
		if ($file_pos !== false) {
			$file_name = substr($full_name, $file_pos);
		}
		$path = substr($full_name, 0, (strlen($file_name)*(-1)));
	}
	if (strlen($path) == 0) $path = "..";
	define("EDIRECTORY_ROOT", $path);
	define("BIN_PATH", EDIRECTORY_ROOT."/bin");*/
	////////////////////////////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////////////////////////
	$_inCron = true;
	

	$edir_all_locations = explode(",", config('params.EDIR_ALL_LOCATIONS'));

	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	$time_start = getmicrotime();
	


	////////////////////////////////////////////////////////////////////////////////////////////////////
	$sqlDomain = "	SELECT
						D.`id`, D.`database_host`, D.`database_port`, D.`database_username`, D.`database_password`, D.`database_name`, D.`url`
					FROM `Domain` AS D
					LEFT JOIN `Control_Cron` AS CC ON (CC.`domain_id` = D.`id`)
					WHERE CC.`running` = 'N'
					AND CC.`type` = 'location_update'
					AND D.`status` = 'A'
					AND (ADDDATE(CC.`last_run_date`, INTERVAL 5 MINUTE) <= NOW() OR CC.`last_run_date` = '0000-00-00 00:00:00')
					ORDER BY
						IF (CC.`last_run_date` IS NULL, 0, 1),
						CC.`last_run_date`,
						D.`id`
					LIMIT 1";

		$resDomain = Sql::fetchMain($sqlDomain);

	if (count($resDomain) > 0) {
			$rowDomain = $resDomain;
		define("SELECTED_DOMAIN_ID", $rowDomain[0]->id);

		$sqlUpdate = "UPDATE `Control_Cron` SET `running` = 'Y', `last_run_date` = NOW() WHERE `domain_id` = ".SELECTED_DOMAIN_ID." AND `type` = 'location_update'";
		Sql::updateSqlMain($sqlUpdate);
        $messageLog = "Starting cron";
        Functions::log_addCronRecord('', "location_update", $messageLog, false, $cron_log_id);

	////////////////////////////////////////////////////////////////////////////////////////////////////
		/*$domainHost = $rowDomain["database_host"].($rowDomain["database_port"]? ":".$rowDomain["database_port"]: "");
		$domainUser = $rowDomain["database_username"];
		$domainPass = $rowDomain["database_password"];
		$domainDBName = $rowDomain["database_name"];
		$domainURL = $rowDomain["url"];

		$linkDomain = mysql_connect($domainHost, $domainUser, $domainPass, true);
		mysql_query("SET NAMES 'utf8'", $linkDomain);
		mysql_query('SET character_set_connection=utf8', $linkDomain);
		mysql_query('SET character_set_client=utf8', $linkDomain);
		mysql_query('SET character_set_results=utf8', $linkDomain);
		mysql_select_db($domainDBName, $linkDomain);*/


		$sqlChange = "SELECT a.id, a.location_id, a.location_level, a.parent_old_id,
			                 a.parent_new_id, a.parent_level,
							 ifnull(a1.name,         ifnull(a2.name,         ifnull(a3.name,         ifnull(a4.name, a5.name)))) location_title,
							 ifnull(a1.abbreviation, ifnull(a2.abbreviation, ifnull(a3.abbreviation, ifnull(a4.abbreviation, a5.abbreviation)))) location_abbreviation,
							 ifnull(a1.friendly_url, ifnull(a2.friendly_url, ifnull(a3.friendly_url, ifnull(a4.friendly_url, a5.friendly_url)))) location_friendly_url
						FROM LocationChangeLOG a
						 LEFT JOIN Location_1 a1 on (a.location_id = a1.id AND a.location_level = 1)
						 LEFT JOIN Location_2 a2 on (a.location_id = a2.id AND a.location_level = 2)
						 LEFT JOIN Location_3 a3 on (a.location_id = a3.id AND a.location_level = 3)
						 LEFT JOIN Location_4 a4 on (a.location_id = a4.id AND a.location_level = 4)
						 LEFT JOIN Location_5 a5 on (a.location_id = a5.id AND a.location_level = 5)
					   WHERE a.modules_updated like 'n' and domain_id = ".SELECTED_DOMAIN_ID;
		DB::setFetchMode(PDO::FETCH_ASSOC);

		$resChange = Sql::fetchMain($sqlChange);

		$module_tables = array('Classified', 'Event', 'Listing', 'Listing_Summary');

		$array_task_finished = array();
		foreach ($resChange as $rowChange) {

			extract($rowChange);
			$location_title = $location_title;//db_formatString(
			$location_abbreviation = $location_abbreviation;
			$location_friendly_url =$location_friendly_url;

			foreach ($module_tables as $table) {

				// updating text elements from Listing_Summary -----------------
				if ($table == 'Listing_Summary') {
					$sqlUpdateLS = "update {$table}
										   set location_{$location_level}_title = {$location_title},
										       location_{$location_level}_abbreviation = {$location_abbreviation},
										       location_{$location_level}_friendly_url = {$location_friendly_url}
										 where location_{$location_level} = {$location_id} ";
					$parent_level ? $sqlUpdateLS .= "and location_{$parent_level} = {$parent_old_id}" : $sqlUpdateLS .= "";

					Sql::updateSql($sqlUpdateLS);

					if ($parent_level && $parent_new_id){
						$sqlUpdateLS = "SELECT name,abbreviation,friendly_url FROM Location_".$parent_level." WHERE id = ".$parent_new_id;

						$rowChange = Sql::fetchMain($sqlUpdateLS);
						$sqlUpdateLS = "update {$table}
										   set location_{$parent_level}_title = '{$rowChange["name"]}',
										       location_{$parent_level}_abbreviation = '{$rowChange["abbreviation"]}',
										       location_{$parent_level}_friendly_url = '{$rowChange["friendly_url"]}'
										 where location_{$location_level} = {$location_id} and location_{$parent_level} = {$parent_old_id}";

					Sql::updateSql($sqlUpdateLS);
					}
				}
				// -------------------------------------------------------------

				// updating fulltextsearch_where fields (pt. 1/2) --------------
				$sqlList = "select id from {$table}
									 where location_{$location_level} = {$location_id} ";
				$parent_level ? $sqlList .= "and location_{$parent_level} = {$parent_old_id}" : $sqlList .= "";

				$resList = Sql::fetch($sqlList);
				foreach ($resList as $rowList) {

					//todo: handle unique indexes insert errors
					$sql = "insert ignore into LocationFulltextwhereList (item_table, item_id, domain_id) values ('{$table}', {$rowList->id}, ".SELECTED_DOMAIN_ID.")";
					Sql::insertSqlMain($sql);
				}
				// -------------------------------------------------------------

				// updating location ids ---------------------------------------
				if ($parent_level){
					$sqlUpdateModule = "update {$table} set location_{$parent_level} = {$parent_new_id}
										 where location_{$location_level} = {$location_id} and location_{$parent_level} = {$parent_old_id}";
					Sql::insertSql($sql);
				}
				// -------------------------------------------------------------

			}

			$array_task_finished[] = $id;
		}


		// updating fulltextsearch_where fields (pt. 2/2) ----------------------
		foreach ($module_tables as $table) {

			//todo: consider limit for performance / scalability purposes
			//DB::setFetchMode(PDO::FETCH_ASSOC);

			DB::connection('domain')->setFetchMode(PDO::FETCH_ASSOC);


			$sqlRebuild = "select item_table, item_id, domain_id from LocationFulltextwhereList where item_table like '{$table}' and domain_id = ".SELECTED_DOMAIN_ID;
			$resRebuild = Sql::fetchMain($sqlRebuild);

			if (count($resRebuild) > 0) {

				foreach ($resRebuild as $rowBuild) {
					$item_id = $rowBuild['item_id'];

					$sql = "select address, zip_code, location_1, location_2, location_3, location_4, location_5 from {$table} where id = {$item_id}";
					$res = Sql::fetch($sql);
					$row = $res;

					$fulltextsearch_where = array();
					$fulltextsearch_where[] = $item_id;
					if ($row['address']) $fulltextsearch_where[] = $row['address'];
					if ($row['zip_code']) $fulltextsearch_where[] = $row['zip_code'];

					foreach($edir_all_locations as $level) {
						if ($row['location_'.$level]) {
							$location_id = $row['location_'.$level];

							$sqlLoc = "select name, abbreviation from Location_{$level} where id = {$location_id}";
							$resLoc = Sql::fetch($sqlLoc);
							$rowLoc = $resLoc;

							if ($rowLoc['name']) $fulltextsearch_where[] = $rowLoc['name'];
							if ($rowLoc['abbreviation']) $fulltextsearch_where[] = $rowLoc['abbreviation'];
						}
					}

					$fulltextsearch_where = implode(' ', $fulltextsearch_where);
					$sql = "update {$table} set fulltextsearch_where = {$fulltextsearch_where} where id = {$item_id}";
					$res = Sql::updateSql($sql);

					$sql = "delete from LocationFulltextwhereList where item_table like '{$table}' and item_id = {$item_id} and domain_id = ".SELECTED_DOMAIN_ID;
					Sql::deleteSqlMain($sql);

				}
			}

			DB::setFetchMode(PDO::FETCH_CLASS);

			DB::connection('domain')->setFetchMode(PDO::FETCH_CLASS);
		}
		// ---------------------------------------------------------------------

		// updating LocationChangeLOG status
		if ((is_array($array_task_finished)) and (count($array_task_finished) > 0)) {
			foreach ($array_task_finished as $task_id) {
				$sqlFinishTask = "update LocationChangeLOG set modules_updated = 'y' where id = {$task_id} and domain_id = ".SELECTED_DOMAIN_ID;
			Sql::updateSql($sqlFinishTask);
			}
		}


	////////////////////////////////////////////////////////////////////////////////////////////////////
	} else {
		exit;
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////



	$sqlUpdate = "UPDATE `Control_Cron` SET `running` = 'N', last_run_date = NOW() WHERE `domain_id` = ".SELECTED_DOMAIN_ID." AND `type` = 'location_update'";
	Sql::updateSqlMain($sqlUpdate);

	////////////////////////////////////////////////////////////////////////////////////////////////////
	$time_end = getmicrotime();
	$time = $time_end - $time_start;
	print "Location update on Domain ".SELECTED_DOMAIN_ID." - ".date("Y-m-d H:i:s")." - ".round($time, 2)." seconds.\n";
	if (!Functions::setting_set("last_datetime_location_update", date("Y-m-d H:i:s"))) {
		if (!Functions::setting_new("last_datetime_location_update", date("Y-m-d H:i:s"))) {
			print "last_datetime_location_update error - Domain - ".SELECTED_DOMAIN_ID." - ".date("Y-m-d H:i:s")."\n";
            $messageLog = "Database error - LINE: ".__LINE__;
            Functions::log_addCronRecord('', "location_update", $messageLog, true, $cron_log_id);
		}
	}
    $messageLog = "Cron finished";
    Functions::log_addCronRecord('', "location_update", $messageLog, true, $cron_log_id, true, round($time, 2));
	////////////////////////////////////////////////////////////////////////////////////////////////////
?>