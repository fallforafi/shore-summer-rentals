<?php	
use App\Functions\Functions;
use App\Models\Domain\Sql;

	
	/*$path = "";
	$full_name = "";
	$file_name = "";
	$full_name = $_SERVER["SCRIPT_FILENAME"];
	if (strlen($full_name) > 0) {
		$osslash = ((strtoupper(substr(PHP_OS, 0, 3)) == 'WIN') ? '\\' : '/');
		$file_pos = strpos($full_name, $osslash."cron".$osslash);
		if ($file_pos !== false) {
			$file_name = substr($full_name, $file_pos);
		}
		$path = substr($full_name, 0, (strlen($file_name)*(-1)));
	}
	if (strlen($path) == 0) $path = url();
	define("EDIRECTORY_ROOT", $path);

	define("BIN_PATH", EDIRECTORY_ROOT."/bin");

	//////////////////////////////////////////////////////////////////////////////////////////////////
	function getmicrotime() {
		list($usec, $sec) = explode(" ", microtime());
		return ((float)$usec + (float)$sec);
	}
	$time_start = getmicrotime();*/
	//////////////////////////////////////////////////////////////////////////////////////////////////
	
$sqlDomain = "	SELECT
						D.`id`, D.`database_host`, D.`database_port`, D.`database_username`, D.`database_password`, D.`database_name`, D.`url`
					FROM `Domain` AS D
					LEFT JOIN `Control_Cron` AS CC ON (CC.`domain_id` = D.`id`)
					WHERE CC.`running` = 'N'
					AND CC.`type` = 'daily_maintenance'
					AND D.`status` = 'A'
					AND (ADDDATE(CC.`last_run_date`, INTERVAL 1 DAY) <= NOW() OR CC.`last_run_date` = '0000-00-00 00:00:00')
					ORDER BY
						IF (CC.`last_run_date` IS NULL, 0, 1),
						CC.`last_run_date`,
						D.`id`
					LIMIT 1";

	$resDomain = Sql::fetchMain($sqlDomain);

	if (count($resDomain) > 0) {

		$rowDomain = $resDomain;
		define("SELECTED_DOMAIN_ID", $rowDomain[0]->id);

		
		$sqlUpdate = "UPDATE `Control_Cron` SET `running` = 'Y', `last_run_date` = NOW() WHERE `domain_id` = ".SELECTED_DOMAIN_ID." AND `type` = 'daily_maintenance'";
		Sql::updateSqlMain($sqlUpdate);

	        $messageLog = "Starting cron";
     Functions::log_addCronRecord('', "daily_maintenance", $messageLog, false, $cron_log_id);
	//////////////////////////////////////////////////////////////////////////////////////////////////
	} else {
		exit;
	}




    //33: Search Results – Premium Placement for Top 5 Results
	$messageLog = "Update search position listing - LINE: ".__LINE__;
    Functions::log_addCronRecord('', "daily_maintenance", $messageLog, true, $cron_log_id);
	$sqlUpdate = "UPDATE Listing SET search_pos_date = '0000-00-00', search_pos = 6 WHERE search_pos_date < CURDATE() AND search_pos_date != '0000-00-00'";
	$result = Sql::updateSql($sqlUpdate);
	$sqlUpdate = "UPDATE Listing_Summary SET search_pos_date = '0000-00-00', search_pos = 6 WHERE search_pos_date < CURDATE() AND search_pos_date != '0000-00-00'";
	$result = Sql::updateSql($sqlUpdate);


	//////////////////////////////////////////////////////////////////////////////////////////////////
    $messageLog = "Update expired listings - LINE: ".__LINE__;
    Functions::log_addCronRecord('', "daily_maintenance", $messageLog, true, $cron_log_id);
	$sql = "SELECT id FROM Listing WHERE renewal_date < NOW() AND renewal_date != '0000-00-00' AND status != 'E' AND isppilisting = 'n'";
	$result = Sql::fetch($sql);
	foreach ($result as $row) {
		$listingObj = new Listing();
		$listingObj->Listing($row->id);
		$listingObj->setString("status", "E");
		$listingObj->Save();
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////

	$messageLog = "Update expired listings badges - LINE: ".__LINE__;
    Functions::log_addCronRecord('', "daily_maintenance", $messageLog, true, $cron_log_id);
	$sql = "SELECT id FROM Listing_Choice WHERE renewal_date < NOW() AND renewal_date != '0000-00-00' AND status != 'E'";
	$result = Sql::fetch($sql);
	foreach ($result as $row) {
		$listingObj = new ListingChoice();
		$listingObj->ListingChoice("", "", $row->id);
		$listingObj->setString("status", "E");
		$listingObj->Save();
	}


	//////////////////////////////////////////////////////////////////////////////////////////////////
	$messageLog = "Update expired banners - LINE: ".__LINE__;
     Functions::log_addCronRecord('', "daily_maintenance", $messageLog, true, $cron_log_id);
    $sql = "SELECT id FROM Banner WHERE renewal_date < NOW() AND renewal_date != '0000-00-00' AND expiration_setting = '2' AND status != 'E'";
	$result = Sql::fetch($sql);
	foreach ($result as $row) {
		$bannerObj = new Banner();
		$bannerObj->Banner($row->id);
		$bannerObj->setString("status", "E");
		$bannerObj->Save();	
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////
    $messageLog = "Deleting email list from PPI and Listing - LINE: ".__LINE__;
 	Functions::log_addCronRecord('', "daily_maintenance", $messageLog, true, $cron_log_id);
	$sql = "UPDATE Listing SET email_list = '' WHERE unique_email = 'y'";
	$result = Sql::updateSql($sql);
	$sql = "UPDATE PPI SET email_list = '' WHERE unique_email = 'y'";
	$result = Sql::updateSqlMain($sql);
	//////////////////////////////////////////////////////////////////////////////////////////////////


	 $messageLog = "Update expired Promotional Codes - LINE: ".__LINE__;
    Functions::log_addCronRecord('', "daily_maintenance", $messageLog, true, $cron_log_id);
	$sql = "UPDATE Discount_Code SET status = 'E' WHERE expire_date < NOW() AND expire_date != '0000-00-00' AND status != 'E'";
	$result = Sql::updateSql($sql);
	//////////////////////////////////////////////////////////////////////////////////////////////////


	//////////////////////////////////////////////////////////////////////////////////////////////////
    $messageLog = "Update expired invoices - LINE: ".__LINE__;
    Functions::log_addCronRecord('', "daily_maintenance", $messageLog, true, $cron_log_id);
	$sql = "UPDATE Invoice SET status = 'E' WHERE expire_date < NOW() AND expire_date != '0000-00-00' AND status != 'E' AND status != 'R'";
	$result = Sql::updateSql($sql);
	//////////////////////////////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////////////////////////////
    $messageLog = "Delete old invoices - LINE: ".__LINE__;
    Functions::log_addCronRecord('', "daily_maintenance", $messageLog, true, $cron_log_id);
	$sql = "SELECT id FROM Invoice WHERE status = 'N'";
	$result = Sql::fetch($sql);
	if (count($result) > 0) {
		foreach ($result as $row) {	
			$invoice_ids[] = $row->id;
		}
	}
	if (isset($invoice_ids)) {
		$invoice_ids = implode(",",$invoice_ids);
		$sql = "DELETE FROM Invoice WHERE id IN ($invoice_ids)";
		$result = Sql::fetch($sql);
		$sql = "DELETE FROM Invoice_Listing WHERE invoice_id IN ($invoice_ids)";
		$result = Sql::fetch($sql);
		$sql = "DELETE FROM Invoice_Event WHERE invoice_id IN ($invoice_ids)";
		$result = Sql::fetch($sql);
		$sql = "DELETE FROM Invoice_Banner WHERE invoice_id IN ($invoice_ids)";
		$result = Sql::fetch($sql);
		$sql = "DELETE FROM Invoice_Classified WHERE invoice_id IN ($invoice_ids)";
		$result = Sql::fetch($sql);
		$sql = "DELETE FROM Invoice_Article WHERE invoice_id IN ($invoice_ids)";
		$result = Sql::fetch($sql);
	}
	//////////////////////////////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////////////////////////////
    $messageLog = "Update listing statistic - LINE: ".__LINE__;
    Functions::log_addCronRecord('', "daily_maintenance", $messageLog, true, $cron_log_id);
	$sql = "SELECT COUNT(id) AS total FROM Listing WHERE status = 'P'";
	$result = Sql::fetch($sql);
	if ($result) {
		if ($row = $result) {
			$this_value = ((int)$row[0]->total) ? (((int)($row[0]->total/10)+1)*10) : ("0");
			$sql = "UPDATE ItemStatistic SET value = '".$this_value."' WHERE name = 'l_pending'";
			$result = Sql::updateSql($sql);
			if (count($result) <= 0) {
				$sql = "INSERT INTO ItemStatistic (value, name) VALUES ('".$this_value."', 'l_pending')";
				$result = Sql::updateSql($sql);
			}
		}
	}
	$sql = "SELECT COUNT(id) AS total FROM Listing WHERE renewal_date > NOW() AND renewal_date <= DATE_ADD(NOW(), INTERVAL ".config('params.DEFAULT_LISTING_DAYS_TO_EXPIRE')." DAY)";
	$result = Sql::fetch($sql);
	if ($result) {
		if ($row = $result) {
			$this_value = ((int)$row[0]->total) ? (((int)($row[0]->total/10)+1)*10) : ("0");
			$sql = "UPDATE ItemStatistic SET value = '".$this_value."' WHERE name = 'l_expiring'";
			$result = Sql::updateSql($sql);
			if (count($result) <= 0) {
				$sql = "INSERT INTO ItemStatistic (value, name) VALUES ('".$this_value."', 'l_expiring')";
				$result = Sql::updateSql($sql);
			}
		}
	}
	$sql = "SELECT COUNT(id) AS total FROM Listing WHERE status = 'E'";
	$result = Sql::fetch($sql);
	if ($result) {
		if ($row = $result){
			$this_value = ((int)$row[0]->total) ? (((int)($row[0]->total/10)+1)*10) : ("0");
			$sql = "UPDATE ItemStatistic SET value = '".$this_value."' WHERE name = 'l_expired'";
			$result = Sql::updateSql($sql);
			if (count($result) <= 0) {
				$sql = "INSERT INTO ItemStatistic (value, name) VALUES ('".$this_value."', 'l_expired')";
			$result = Sql::updateSql($sql);

			}
		}
	}
	$sql = "SELECT COUNT(id) AS total FROM Listing WHERE status = 'A'";
	$result = Sql::fetch($sql);
	if ($result) {
		if ($row = $result){
			$this_value = ((int)$row[0]->total) ? (((int)($row[0]->total/10)+1)*10) : ("0");
			$sql = "UPDATE ItemStatistic SET value = '".$this_value."' WHERE name = 'l_active'";
			$result = Sql::updateSql($sql);
			if (count($result) <= 0) {
				$sql = "INSERT INTO ItemStatistic (value, name) VALUES ('".$this_value."', 'l_active')";
			$result = Sql::updateSql($sql);
			}
		}
	}
	$sql = "SELECT COUNT(id) AS total FROM Listing WHERE status = 'S'";
	$result = Sql::fetch($sql);
	if ($result) {
		if ($row = $result){
			$this_value = ((int)$row[0]->total) ? (((int)($row[0]->total/10)+1)*10) : ("0");
			$sql = "UPDATE ItemStatistic SET value = '".$this_value."' WHERE name = 'l_suspended'";
			$result = Sql::updateSql($sql);
			if (count($result) <= 0) {
				$sql = "INSERT INTO ItemStatistic (value, name) VALUES ('".$this_value."', 'l_suspended')";
			$result = Sql::updateSql($sql);
			}
		}
	}
	$sql = "SELECT COUNT(id) AS total FROM Listing WHERE entered >= '".date("Y-m-d", mktime(0, 0, 0, date("m")-1 , date("d"), date("Y")))."'";
	$result = Sql::fetch($sql);
	if ($result) {
		if ($row = $result){
			$this_value = ((int)$row[0]->total) ? (((int)($row[0]->total/10)+1)*10) : ("0");
			$sql = "UPDATE ItemStatistic SET value = '".$this_value."' WHERE name = 'l_added30'";
			$result = Sql::updateSql($sql);
			if (count($result) <= 0) {
				$sql = "INSERT INTO ItemStatistic (value, name) VALUES ('".$this_value."', 'l_added30')";
			$result = Sql::updateSql($sql);
			}
		}
	}
	/////////////////////////////////////////////////////////////////////////////////////////////////////



	$sql = "DELETE FROM Inquiry_Email_Ip";
	$result = Sql::deleteSqlMain($sql);
	
	$sql = "DELETE FROM Blocked_users WHERE automatic = 1";
	$result = Sql::deleteSql($sql);
	//////////////////////////////////////////////////////////////////////////////////////////////////

	/////////// Delete Unused Image Files  ///////////////////////////
    $messageLog = "Delete unused image files - LINE: ".__LINE__;
    Functions::log_addCronRecord('', "daily_maintenance", $messageLog, true, $cron_log_id);
	$sqlDomain = "SELECT id FROM Domain";
	$resultDomain = Sql::fetchMain($sqlDomain);
	if (count($resultDomain) > 0) {
		foreach ($resultDomain as $rowDomain) {
			$dir = "custom/domain_".$rowDomain->id."/image_files";
			$imageFiles = glob("$dir/_*.*");
			foreach ($imageFiles as $file) unlink($file);

			$dir = "custom/domain_".$rowDomain->id."/image_files";
			$imageFiles = glob("$dir/resize_*.jpg");
			foreach ($imageFiles as $file) unlink($file);


		}
	}

	$dir = url()."/custom/profile";
	$profileFiles = glob("$dir/_*.*");
	foreach ($profileFiles as $file) unlink($file);
	//////////////////////////////////////////////////////////////////////////////////////////////////



	$default_url=url();

	$messageLog = "Sending notification to users that added a listing and didnt make the payment - LINE: ".__LINE__;
    Functions::log_addCronRecord('', "daily_maintenance", $messageLog, true, $cron_log_id);

	$sql = "SELECT * FROM Listing WHERE status = 'P' AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 7 DAY), '%Y%m%d') <= DATE_FORMAT(entered, '%Y%m%d') Limit 1";


	$result = Sql::fetch($sql);	
	foreach ($result as $listing) {

		$sql_contact = "SELECT * FROM Contact WHERE account_id = ".$listing->account_id;
		$result_contact = Sql::fetchMain($sql_contact);	

		foreach ($result_contact as $row_contact) {


			if ($emailNotificationObj = Functions::system_checkEmail(config('params.SYSTEM_LISTING_WITHOUT_PAYMENT'))) {
				$subject = $emailNotificationObj->getString("subject");
				$body    = $emailNotificationObj->getString("body");
				$subject = Functions::system_replaceEmailVariables($subject, $listing->id, 'listing');
				$body    = Functions::system_replaceEmailVariables($body, $listing->id, 'listing');
				$body    = str_replace(array("ITEM_TITLE", "LISTING_TITLE"), $listing->title, $body);
				$subject = str_replace("DEFAULT_URL", $default_url, $subject);
				$body    = str_replace("DEFAULT_URL", $default_url, $body);
				$subject = html_entity_decode($subject);
				$body    = html_entity_decode($body);

				 $data= [
            'sitemgr_msg' => $body,
            'subject' => $subject,
            'email_send' =>$row_contact->email
        ];

    Mail::send('email.owner',['data' => $data], function($message) use ($data) {
   
        $message->from('amoosjohn@gmail.com');
     $message->to($data['email_send'])->bcc('amoosjohn@gmail.com')->subject($data['subject']);
    });


				//system_mail($row_contact["email"], $subject, $body, EDIRECTORY_TITLE." <$sitemgr_email>", $emailNotificationObj->getString("content_type"), "", $emailNotificationObj->getString("bcc"), $error);
			}

		}
		
	}


	$messageLog = "Sending 7 days listing activation email - LINE: ".__LINE__;
    Functions::log_addCronRecord('', "daily_maintenance", $messageLog, true, $cron_log_id);
	$sql = "SELECT * FROM Listing WHERE status = 'A' order by id desc Limit 1";
	$result = Sql::fetch($sql);	
	if($result)
	{
	foreach ($result as $row) {


			if ($emailNotificationObj = Functions::system_checkEmail(config('params.SYSTEM_7_DAYS_ACTIVATED'))) {

				$subject    = $emailNotificationObj->getString("subject");
				$subject    = Functions::system_replaceEmailVariables($subject, $row->id, 'listing');
				$body       = $emailNotificationObj->getString("body");
				$body       = Functions::system_replaceEmailVariables($body,$row->id, 'listing');
				
				$subject    = str_replace("DEFAULT_URL", $default_url, $subject);
				$body       = str_replace("DEFAULT_URL", $default_url, $body);
				
				$body       = html_entity_decode($body);
				$subject    = html_entity_decode($subject);
				$contactObj = new Contact();
				$contactObj->Contact($row->account_id);

				$data= [
            'sitemgr_msg' => $body,
            'subject' => $subject,
            'email_send' =>$contactObj->email
        ];

    Mail::send('email.owner',['data' => $data], function($message) use ($data) {
   
        $message->from('amoosjohn@gmail.com');
     $message->to($data['email_send'])->bcc('amoosjohn@gmail.com')->subject($data['subject']);
    });

				//system_mail($row_contact["email"], $subject, $body, EDIRECTORY_TITLE." <$sitemgr_email>", $emailNotificationObj->getString("content_type"), "", $emailNotificationObj->getString("bcc"), $error);
			

}
		}
		
	}



	  //////  Delete old entries from Cron_Log Table  //////////
    $messageLog = "Delete old entries from Cron_Log Table - LINE: ".__LINE__;
    Functions::log_addCronRecord('', "daily_maintenance", $messageLog, true, $cron_log_id);

	$sql = "DELETE FROM Cron_Log WHERE (date <= DATE_SUB(NOW(), INTERVAL '".config('params.CRON_LOG_CLEAR_INTERVAL')."' DAY))";
	$result = Sql::deleteSqlMain($sql);
	////////////////////////////////////////////////////////////////////////////////////////////////////

	$sqlUpdate = "UPDATE `Control_Cron` SET `running` = 'N' WHERE `domain_id` = '1' AND `type` = 'daily_maintenance'";
	Sql::updateSqlMain($sqlUpdate);

	////////////////////////////////////////////////////////////////////////////////////////////////////
	$time_end = getmicrotime();
	$time = $time_end - $time_start;
	print "Daily maintenance on Domain 1 - ".date("Y-m-d H:i:s")." - ".round($time, 2)." seconds.\n";
	if (!Functions::setting_set("last_datetime_dailymaintenance", date("Y-m-d H:i:s"))) {
		if (!Functions::setting_new("last_datetime_dailymaintenance", date("Y-m-d H:i:s"))) {
			print "last_datetime_dailymaintenance error - Domain - 1 - ".date("Y-m-d H:i:s")."\n";
            $messageLog = "Database error - LINE: ".__LINE__;
            Functions::log_addCronRecord('', "daily_maintenance", $messageLog, true, $cron_log_id);
		}
	}
    $messageLog = "Cron finished";
    Functions::log_addCronRecord('', "daily_maintenance", $messageLog, true, $cron_log_id, true, round($time, 2));

?>