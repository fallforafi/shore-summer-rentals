<?php	
use App\Functions\Functions;
use App\Models\Domain\Sql;
use App\Functions\Listing;
use App\Functions\Contact;
use App\Functions\Location3;
use App\Functions\Location4;


	/*==================================================================*
	######################################################################
	#   
	######################################################################
	\*==================================================================*/

	# ----------------------------------------------------------------------------------------------------
	# * FILE: renewal_reminder.php
	# ----------------------------------------------------------------------------------------------------

	////////////////////////////////////////////////////////////////////////////////////////////////////
	ini_set("html_errors", FALSE);
	////////////////////////////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////////////////////////
	/*$path = "";
	$full_name = "";
	$file_name = "";
	$full_name = $_SERVER["SCRIPT_FILENAME"];
	if (strlen($full_name) > 0) {
		$osslash = ((strtoupper(substr(PHP_OS, 0, 3)) == 'WIN') ? '\\' : '/');
		$file_pos = strpos($full_name, $osslash."cron".$osslash);
		if ($file_pos !== false) {
			$file_name = substr($full_name, $file_pos);
		}
		$path = substr($full_name, 0, (strlen($file_name)*(-1)));
	}
	if (strlen($path) == 0) $path = "/var/www/html";
	define("EDIRECTORY_ROOT", $path);
	define("BIN_PATH", EDIRECTORY_ROOT."/bin");*/
	////////////////////////////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////////////////////////
	$_inCron = true;
	//////////////////////////////////////////////////////////////////////////////////////////////////
	/*function getmicrotime() {
		list($usec, $sec) = explode(" ", microtime());
		return ((float)$usec + (float)$sec);
	}
	$time_start = getmicrotime();*/
	//

	//////////////////////////////////////////////////////////////////////////////////////////////////
	$sqlDomain = "	SELECT
						D.`id`, D.`database_host`, D.`database_port`, D.`database_username`, D.`database_password`, D.`database_name`, D.`url`
					FROM `Domain` AS D
					LEFT JOIN `Control_Cron` AS CC ON (CC.`domain_id` = D.`id`)
					WHERE CC.`running` = 'N'
					AND CC.`type` = 'renewal_reminder'
					AND D.`status` = 'A'
					AND (ADDDATE(CC.`last_run_date`, INTERVAL 20 MINUTE) <= NOW() OR CC.`last_run_date` = '0000-00-00 00:00:00')
					ORDER BY
						IF (CC.`last_run_date` IS NULL, 0, 1),
						CC.`last_run_date`,
						D.`id`
					LIMIT 1";//

	$resDomain = Sql::fetchMain($sqlDomain);

	if (count($resDomain) > 0) {

		$rowDomain = $resDomain;
		define("SELECTED_DOMAIN_ID", $rowDomain[0]->id);

		$sqlUpdate = "UPDATE `Control_Cron` SET `running` = 'Y', `last_run_date` = NOW() WHERE `domain_id` = ".SELECTED_DOMAIN_ID." AND `type` = 'renewal_reminder'";
		Sql::updateSqlMain($sqlUpdate);

	        $messageLog = "Starting cron";
       Functions::log_addCronRecord('', "renewal_reminder", $messageLog, false, $cron_log_id);
	//////////////////////////////////////////////////////////////////////////////////////////////////
	} else {
		exit;
	}

//define("SELECTED_DOMAIN_ID", 1);
//////////////////////////////////////////////////////////////////////////////////////////////////

	$_inCron = false;
	
	//////////////////////////////////////////////////////////////////////////////////////////////////

	/*$url = $domainURL;
	(SSL_ENABLED == "on" && FORCE_MEMBERS_SSL == "on") ? $url_protocol = "https://" : $url_protocol = "http://";
	$default_url = $url_protocol.$url.(EDIRECTORY_FOLDER ? EDIRECTORY_FOLDER : "");*/
	Functions::setting_get("sitemgr_email", $sitemgr_email);
	Functions::setting_get("edir_default_language", $edir_default_language);
	////////////////////////////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////////////////////////
	$last_listing_reminder = 0;
	if (!Functions::setting_get("last_listing_reminder", $last_listing_reminder)) {
		if (!Functions::setting_set("last_listing_reminder", "0")) {
			if (!Functions::setting_new("last_listing_reminder", "0")) {
				print "Renewal Reminder - last_listing_reminder error - Domain - ".SELECTED_DOMAIN_ID." - ".date("Y-m-d H:i:s")."\n";
                $messageLog = "Database error - LINE: ".__LINE__;
                Functions::log_addCronRecord('', "renewal_reminder", $messageLog, true, $cron_log_id);
			}
		}
	}
	if (!$last_listing_reminder) {
		$last_listing_reminder = 0;
	}

	unset($allNot);
    $messageLog = "Read Email Notification - LINE: ".__LINE__;
    Functions::log_addCronRecord('', "renewal_reminder", $messageLog, true, $cron_log_id);
	$sqlNot = "SELECT * FROM Email_Notification WHERE deactivate = '0' AND (days > 0 OR email LIKE '0 day renewal reminder') ORDER BY days";
	$resultNot = Sql::fetch($sqlNot);
		foreach ($resultNot as $rowNot) {
		$allNot[$rowNot->days][$edir_default_language]["bcc"] = $rowNot->bcc;
		$allNot[$rowNot->days][$edir_default_language]["content_type"] = $rowNot->content_type;
		$allNot[$rowNot->days][$edir_default_language]["body"] = $rowNot->body;
		$allNot[$rowNot->days][$edir_default_language]["subject"] = $rowNot->subject;
	
	}



	if ($allNot && (count($allNot) > 0)) {

		$allNotCount = 0;
		$before_days = 0;
		foreach ($allNot as $days=>$this_email_data) {
			if ($days == 0)
				$allNotSQL[] = "(DATE_FORMAT(renewal_date, '%Y%m%d') = DATE_FORMAT(NOW(), '%Y%m%d'))";
			if ($allNotCount == 0) {
				$allNotSQL[] = "(DATE_FORMAT(renewal_date, '%Y%m%d') > DATE_FORMAT(NOW(), '%Y%m%d') AND DATE_FORMAT(DATE_SUB(renewal_date, INTERVAL ".$days." DAY), '%Y%m%d') <= DATE_FORMAT(NOW(), '%Y%m%d') AND reminder != ".$days.")";
			} else {
				$allNotSQL[] = "(DATE_FORMAT(DATE_SUB(renewal_date, INTERVAL ".$before_days." DAY), '%Y%m%d') > DATE_FORMAT(NOW(), '%Y%m%d') AND DATE_FORMAT(DATE_SUB(renewal_date, INTERVAL ".$days." DAY), '%Y%m%d') <= DATE_FORMAT(NOW(), '%Y%m%d') AND reminder != ".$days.")";
			}
			$before_days = $days;
			$allNotCount++;
		}

        $messageLog = "Select accounts - LINE: ".__LINE__;
        Functions::log_addCronRecord('', "renewal_reminder", $messageLog, true, $cron_log_id);
		$sql = 	"".
				" SELECT ".
				" id, account_id, title, renewal_date, reminder ".
				" FROM ".
				" Listing ".
				" WHERE ".
				" account_id > 0 ".
				" AND ".
				" renewal_date != '0000-00-00' ".
				" AND ".
				" ( ".
				implode(" OR ", $allNotSQL).
				" ) ".
				" ORDER BY ".
				" id".
				" LIMIT ".
				$last_listing_reminder.", 1";


		$result = Sql::fetch($sql);
	
	

		$num_rows = count($result);

		$today_date = explode("-", date("Y-m-d"));
		$today_year = $today_date[0];
		$today_month = $today_date[1];
		$today_day = $today_date[2];

        $messageLog = "Send emails - LINE: ".__LINE__;
        Functions::log_addCronRecord('', "renewal_reminder", $messageLog, true, $cron_log_id);
		foreach ($result as $row) {
			$renewal_date = explode("-", $row->renewal_date);
			$renewal_year = $renewal_date[0];
			$renewal_month = $renewal_date[1];
			$renewal_day = $renewal_date[2];

			$reminder = 0;
			$allNotCount = 0;
			$before_days = 0;
			foreach ($allNot as $days=>$this_email_data) {
				if ($days == 0)
					$has_zeroDayReminder = true;
				if ($allNotCount == 0) {
					if ((date("Ymd", mktime(0, 0, 0, $renewal_month, $renewal_day, $renewal_year)) > date("Ymd", mktime(0, 0, 0, $today_month, $today_day, $today_year))) && (date("Ymd", mktime(0, 0, 0, $renewal_month, $renewal_day-$days, $renewal_year)) <= date("Ymd", mktime(0, 0, 0, $today_month, $today_day, $today_year)))) {
						$reminder = $days;

					}


				} else {
					if ((date("Ymd", mktime(0, 0, 0, $renewal_month, $renewal_day-$before_days, $renewal_year)) > date("Ymd", mktime(0, 0, 0, $today_month, $today_day, $today_year))) && (date("Ymd", mktime(0, 0, 0, $renewal_month, $renewal_day-$days, $renewal_year)) <= date("Ymd", mktime(0, 0, 0, $today_month, $today_day, $today_year)))) {
						$reminder = $days;
					}
				}
				$before_days = $days;
				$allNotCount++;

				
			}

			$contactObj = new Contact();
			$contactObj->Contact($row->account_id);
			
			if (($reminder && $contactObj->getString("email")) || $has_zeroDayReminder) {

				#54: New Email Notification Variables
				$listingObj = new Listing();
				$listingObj->Listing($row->id);

				//Address
                unset($array_location_string2);
                $array_location_string2 = array();
                $array_location_string2[] = $listingObj->getNumber("address");

                if ($listingObj->getNumber("location_4")) {
                    $auxLocation4 = new Location4();
                    $auxLocation4->Location4($listingObj->getNumber("location_4"));

                    $array_location_string2[] = $auxLocation4->getString("name");
                }
                if ($listingObj->getNumber("location_3")) {
                    $auxLocation3 = new Location3();
                    $auxLocation3->getLocation3($listingObj->getNumber("location_3"));

                    $array_location_string2[] = $auxLocation3->getString("name");
                }

                if ($listingObj->getNumber("zip_code")) {
                    $array_location_string2[] = $listingObj->getNumber("zip_code");
                }
                $property_address = implode(", ",$array_location_string2);
				########################################
$default_url=url();


				$email_data["body"]         = $allNot[$reminder][$edir_default_language]["body"];
				$email_data["subject"]      = $allNot[$reminder][$edir_default_language]["subject"];
				$email_data["bcc"]          = $allNot[$reminder][$edir_default_language]["bcc"];
				$email_data["content_type"] = $allNot[$reminder][$edir_default_language]["content_type"];

				$email_data["subject"] = str_replace("DEFAULT_URL", $default_url, $email_data["subject"]);
				$email_data["body"]    = str_replace("DEFAULT_URL", $default_url, $email_data["body"]);

				$email_data["subject"] = str_replace("LISTING_RENEWAL_DATE", $row->renewal_date, $email_data["subject"]);
				$email_data["body"]    = str_replace("LISTING_RENEWAL_DATE", $row->renewal_date, $email_data["body"]);

				$email_data["subject"] = str_replace("DAYS_INTERVAL", $reminder, $email_data["subject"]);
				$email_data["body"]    = str_replace("DAYS_INTERVAL", $reminder, $email_data["body"]);

				#54: New Email Notification Variables
				$email_data["subject"] 	= str_replace("RENTAL_ID", $row->id, $email_data["subject"]);
				$email_data["body"] 	= str_replace("RENTAL_ID", $row->id, $email_data["body"]);
				$email_data["subject"] 	= str_replace("EXPIRATION_DATE", Functions::format_date($row->renewal_date, 'F d, Y'), $email_data["subject"]);
				$email_data["body"]    	= str_replace("EXPIRATION_DATE", Functions::format_date($row->renewal_date, 'F d, Y'), $email_data["body"]);
				$email_data["subject"] 	= str_replace("PROPERTY_ADDRESS", $property_address, $email_data["subject"]);
				$email_data["body"]    	= str_replace("PROPERTY_ADDRESS", $property_address, $email_data["body"]);
				########################################

				$email_data["subject"] = Functions::system_replaceEmailVariables($email_data["subject"], $row->id, "listing");
				$email_data["body"]    = Functions::system_replaceEmailVariables($email_data["body"], $row->id, "listing");

				$email_data["body"]    = html_entity_decode($email_data["body"]);

				$to           = $contactObj->getString("email");//
				$from_email   = $sitemgr_email;
				$from_name    = 'Amoos';//EDIRECTORY_TITLE
				$bcc          = $email_data["bcc"];
				$subject      = $email_data["subject"];
				$message      = $email_data["body"];
				$content_type = $email_data["content_type"];

				$message = str_replace("\r\n", "\n", $message);
				$message = str_replace("\n", "\r\n", $message);

				$error = false;

				$data= [
            'sitemgr_msg' => $message,
            'subject' => $subject,
            'email_send' =>$to,
            'bcc' =>$bcc
        ];

    Mail::send('email.owner',['data' => $data], function($message) use ($data) {
   
        $message->from('amoosjohn@gmail.com');
     $message->to($data['email_send'])->bcc($data['bcc'])->subject($data['subject']);
    });	



				//system_mail($to, $subject, $message, $from_name." <".$from_email.">", $content_type, '', '', $error);

				if ($bcc) {
					//system_mail($bcc, $subject, $message, $from_name." <".$from_email.">", $content_type, '', '', $error);
				}

			}

			$sql = "UPDATE Listing SET reminder = ".$reminder." WHERE id = ".$row->id."";
			Sql::updateSql($sql);

		}

	} else {
        $messageLog = "Email Notification disabled - LINE: ".__LINE__;
        Functions::log_addCronRecord('', "renewal_reminder", $messageLog, true, $cron_log_id);
    }

	if ($num_rows < BLOCK) {
		if (!Functions::setting_set("last_listing_reminder", "0")) {
			print "Renewal Reminder - last_listing_reminder error - Domain - ".SELECTED_DOMAIN_ID." - ".date("Y-m-d H:i:s")."\n";
            $messageLog = "Database error - LINE: ".__LINE__;
            Functions::log_addCronRecord('', "renewal_reminder", $messageLog, true, $cron_log_id);
		}
		$last_listing_reminder = 0;
	} else {
		if (!Functions::setting_set("last_listing_reminder", ($last_listing_reminder + BLOCK))) {
			print "Renewal Reminder - last_listing_reminder error - Domain - ".SELECTED_DOMAIN_ID." - ".date("Y-m-d H:i:s")."\n";
            $messageLog = "Database error - LINE: ".__LINE__;
            Functions::log_addCronRecord('', "renewal_reminder", $messageLog, true, $cron_log_id);
		}
		$last_listing_reminder = $last_listing_reminder + BLOCK;
	}

	$sqlUpdate = "UPDATE `Control_Cron` SET `running` = 'N' WHERE `domain_id` = ".SELECTED_DOMAIN_ID." AND `type` = 'renewal_reminder'";
	Sql::updateSqlMain($sqlUpdate);

	////////////////////////////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////////////////////////
	$time_end = getmicrotime();
	$time = $time_end - $time_start;
	print "Renewal Reminder on Domain ".SELECTED_DOMAIN_ID." - ".date("Y-m-d H:i:s")." - ".round($time, 2)." seconds.\n";
	if (!Functions::setting_set("last_datetime_renewalreminder", date("Y-m-d H:i:s"))) {
		if (!Functions::setting_new("last_datetime_renewalreminder", date("Y-m-d H:i:s"))) {
			print "last_datetime_renewalreminder error - Domain - ".SELECTED_DOMAIN_ID." - ".date("Y-m-d H:i:s")."\n";
            $messageLog = "Database error - LINE: ".__LINE__;
            Functions::log_addCronRecord('', "renewal_reminder", $messageLog, true, $cron_log_id);
		}
	}
    $messageLog = "Cron finished";
    Functions::log_addCronRecord('', "renewal_reminder", $messageLog, true, $cron_log_id, true, round($time, 2));
	
?>