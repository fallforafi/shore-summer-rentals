
# Shore Summer Rentals
Made with ❤️ Laravel 5.

# What is Shore Summer Rentals? 
ShoreSummerRentals.com is your one-stop shop for vacation rentals by owners up and down the Eastern Shore coastline. Our locally-owned business matches Jersey Shore, as well as Delaware beach and Ocean City, Maryland homeowners, to quality tenants looking to a book getaway. Those looking for rentals will be happy to know we offer last minute specials, full summer and partial summer houses, winter rentals and even properties for those under the age of 25.
Website: https://www.shoresummerrentals.com


## Installation Steps

### 1. Require the Package

After taking pull run the following command: 

```bash
composer update
```

### 2. Add the DB Credentials & APP_URL

Next make sure to create a new database and add your database credentials to your .env file:

```
DB_HOST=localhost
DB_DATABASE=homestead
DB_USERNAME=homestead
DB_PASSWORD=secret
```

You will also want to update your website URL inside of the `APP_URL` variable inside the .env file:

```
APP_URL=http://localhost:8000
```

> Only if you are on Laravel 5.4 will you need to [Add the Service Provider.]