<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStaticBlocksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('static_blocks', function (Blueprint $table) { 
			$table->increments('ID');
			$table->string('image', 128);
			$table->timestamps();
			$table->tinyInteger('status')->default(1);
			$table->integer('deleted_at');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('static_blocks');
	}

}
