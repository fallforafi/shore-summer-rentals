<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStaticBlocksLanguageTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('static_blocks_languages', function (Blueprint $table) { 
			$table->increments('ID');
			$table->integer('language_id');
			$table->integer('static_blocks_id');
			$table->string('title', 512);
			$table->string('subject', 512);
			$table->string('block_type', 128);
			$table->string('teaser', 128);
			$table->integer('code');
			$table->text('description');
			$table->string('meta_title', 512);
			$table->string('meta_description', 512);
			$table->timestamps();
			$table->tinyInteger('status')->default(1);
			$table->integer('deleted_at');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('static_blocks_languages');
	}

}
