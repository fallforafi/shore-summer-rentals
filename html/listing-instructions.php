<?php include("header.php"); ?>

	
	<section class="inner-page listing-instructions-page">
		<div class="container">
			<h2>CREATING & EDITING</h2>
			<h3>Updating Your Rental Listing</h3>
<p>
<big>			
<div class="blue">EXTREMELY IMPORTANT TIP - SAVE YOUR INFORMATION OFTEN. </div>
I highly recommend that you click the SUBMIT button at the bottom of this Listing Description page every couple of minutes so that you do not lose your information.  
<br/><br/>
PHOTOS - you MUST click the SUBMIT button at the very bottom of this Listing Description page to save the photos.  Simply clicking the SUBMIT button when uploading, without clicking it once again on the Listing Description page, will NOT save them.
<br/><br/>
Please note that ads will not be activated live on the website until ownership is verified and listing is complete including several photos, rates and detailed descriptions.  We will contact you if we cannot verify ownership from our end.
</big></p>


		
		<h3>NEW OWNER REGISTRATION</h3>
Click on “List Your Property” in the upper right hand side of the Home Page.<br/>
Scroll down the page to “Select” a Main Membership.<br/>
Type in an eye catching Listing Title that will stand out.<br/>
Click on Add Vacation Rental under Category.<br/>
Choose Payment Type.<br/>
Enter Promo Code (if applicable).<br/>
Click “Continue”.<br/>
Fill out Registration Form with the Account Holder contact information. (please note this is not the property information but the billing address and contact information for the person in charge of the account)
Click “Submit”.<br/>
Enter payment information if paying by credit card.<br/>
Click “Place Order and Continue” button.<br/>
The system will walk you through creating your rental listing.<br/>


<h3>LISTING DESCRIPTION PAGE</h3>
<p>Be sure to enter very detailed descriptions that highlight your rental property as well as all of the Features and Amenities.  The more details, the more rentals you will secure.</p>

<p>This is also where you will enter the contact information of the person who you want to receive phone calls, texts and emails from potential rentals.  This can be someone other than the account holder.  If this is the same person as account holder, you still need to fill in the fields.</p>

<p>Be sure to enter keywords of highlights of your home as well for renters who will search by keyword (ie. pet friendly, pool, rooftop deck, etc.).</p>

<p>When you are ready to go to the next step, click the Submit button at the bottom of the page and this will bring you to your account dashboard.  This will be where you can edit your rates, payment terms, calendar and much more.  Be sure to click SUBMIT every couple of minutes so that you do not lose your information.</p>

<p>Please note that phone numbers, email addresses and/or external links are prohibited from being displayed in the property description field or note area.  Our website has specific locations for this information, and our system is programmed to display this information in authorized areas only.</p>



		</div>
	</section>
	
	
<?php include("footer.php"); ?>