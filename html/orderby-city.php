<?php include("header.php"); ?>

	
	<section class="search-area2 mt30">
			<div class="container">

				<div class="form-group col-sm-6">
					<input type="text" name="keyword" class="form-control keyword ac_input" id="keyword_resp" placeholder="Search by address, city, state, zip or rental id" value="" autocomplete="off">
				</div>


				<div class="form-group col-sm-2 search-checkin input-append">
				  <input id="searchCheckin" name="checkin" class="form-control hasDatepicker" type="text" value="" placeholder="check in" data-provide="datepicker">
				  <span class="add-on"><i class="fa fa-calendar"></i></span>
				</div>

				<div class="form-group col-sm-2 search-checkin input-append">
				  <input id="searchCheckout" name="checkout" class="form-control hasDatepicker" type="text" value="" placeholder="check out" data-provide="datepicker">
				  <span class="add-on"><i class="fa fa-calendar"></i></span>
				</div>

				<div class="form-group col-sm-2 search-button text-center">
					<button type="submit" class="btn btn-info btn-search form-control" id="search_form_submit"> <i class="fa fa-search"></i> Search</button>
				</div>
				
				<div class="clearfix"></div>
				
				<div class="adv-fom-bot clr-fom inline-control col-sm-12 text-center">
					
					<div class="form-group">
						<select name="sleeps" class="form-control">
						<option value="">Sleeps | Any</option>
																					<option value="0">0</option>
																												<option value="1exactly">Sleeps | 1 Exactly</option>
								<option value="1orLess">Sleeps | 1 Or Less</option>
								<option value="1orMore">Sleeps | 1 Or More</option>
																												<option value="2exactly">Sleeps | 2 Exactly</option>
								<option value="2orLess">Sleeps | 2 Or Less</option>
								<option value="2orMore">Sleeps | 2 Or More</option>
																												<option value="3exactly">Sleeps | 3 Exactly</option>
								<option value="3orLess">Sleeps | 3 Or Less</option>
								<option value="3orMore">Sleeps | 3 Or More</option>
																												<option value="4exactly">Sleeps | 4 Exactly</option>
								<option value="4orLess">Sleeps | 4 Or Less</option>
								<option value="4orMore">Sleeps | 4 Or More</option>
																												<option value="5exactly">Sleeps | 5 Exactly</option>
								<option value="5orLess">Sleeps | 5 Or Less</option>
								<option value="5orMore">Sleeps | 5 Or More</option>
																												<option value="6exactly">Sleeps | 6 Exactly</option>
								<option value="6orLess">Sleeps | 6 Or Less</option>
								<option value="6orMore">Sleeps | 6 Or More</option>
																												<option value="7exactly">Sleeps | 7 Exactly</option>
								<option value="7orLess">Sleeps | 7 Or Less</option>
								<option value="7orMore">Sleeps | 7 Or More</option>
																												<option value="8exactly">Sleeps | 8 Exactly</option>
								<option value="8orLess">Sleeps | 8 Or Less</option>
								<option value="8orMore">Sleeps | 8 Or More</option>
																												<option value="9exactly">Sleeps | 9 Exactly</option>
								<option value="9orLess">Sleeps | 9 Or Less</option>
								<option value="9orMore">Sleeps | 9 Or More</option>
																												<option value="10exactly">Sleeps | 10 Exactly</option>
								<option value="10orLess">Sleeps | 10 Or Less</option>
								<option value="10orMore">Sleeps | 10 Or More</option>
																												<option value="11exactly">Sleeps | 11 Exactly</option>
								<option value="11orLess">Sleeps | 11 Or Less</option>
								<option value="11orMore">Sleeps | 11 Or More</option>
																												<option value="12exactly">Sleeps | 12 Exactly</option>
								<option value="12orLess">Sleeps | 12 Or Less</option>
								<option value="12orMore">Sleeps | 12 Or More</option>
																												<option value="13exactly">Sleeps | 13 Exactly</option>
								<option value="13orLess">Sleeps | 13 Or Less</option>
								<option value="13orMore">Sleeps | 13 Or More</option>
																		</select>
					</div>
					
					<div class="form-group">
						<select name="bedroom" class="form-control">
						<option value="">Bedrooms | Any</option>
																					<option value="1exactly">Bedrooms | 1 Exactly</option>
								<option value="1orLess">Bedrooms | 1 Or Less</option>
								<option value="1orMore">Bedrooms | 1 Or More</option>
																												<option value="2exactly">Bedrooms | 2 Exactly</option>
								<option value="2orLess">Bedrooms | 2 Or Less</option>
								<option value="2orMore">Bedrooms | 2 Or More</option>
																												<option value="3exactly">Bedrooms | 3 Exactly</option>
								<option value="3orLess">Bedrooms | 3 Or Less</option>
								<option value="3orMore">Bedrooms | 3 Or More</option>
																												<option value="4exactly">Bedrooms | 4 Exactly</option>
								<option value="4orLess">Bedrooms | 4 Or Less</option>
								<option value="4orMore">Bedrooms | 4 Or More</option>
																												<option value="5exactly">Bedrooms | 5 Exactly</option>
								<option value="5orLess">Bedrooms | 5 Or Less</option>
								<option value="5orMore">Bedrooms | 5 Or More</option>
																												<option value="6exactly">Bedrooms | 6 Exactly</option>
								<option value="6orLess">Bedrooms | 6 Or Less</option>
								<option value="6orMore">Bedrooms | 6 Or More</option>
																												<option value="7exactly">Bedrooms | 7 Exactly</option>
								<option value="7orLess">Bedrooms | 7 Or Less</option>
								<option value="7orMore">Bedrooms | 7 Or More</option>
																												<option value="8exactly">Bedrooms | 8 Exactly</option>
								<option value="8orLess">Bedrooms | 8 Or Less</option>
								<option value="8orMore">Bedrooms | 8 Or More</option>
																												<option value="9exactly">Bedrooms | 9 Exactly</option>
								<option value="9orLess">Bedrooms | 9 Or Less</option>
								<option value="9orMore">Bedrooms | 9 Or More</option>
																												<option value="10exactly">Bedrooms | 10 Exactly</option>
								<option value="10orLess">Bedrooms | 10 Or Less</option>
								<option value="10orMore">Bedrooms | 10 Or More</option>
																												<option value="11exactly">Bedrooms | 11 Exactly</option>
								<option value="11orLess">Bedrooms | 11 Or Less</option>
								<option value="11orMore">Bedrooms | 11 Or More</option>
																												<option value="12exactly">Bedrooms | 12 Exactly</option>
								<option value="12orLess">Bedrooms | 12 Or Less</option>
								<option value="12orMore">Bedrooms | 12 Or More</option>
																												<option value="13exactly">Bedrooms | 13 Exactly</option>
								<option value="13orLess">Bedrooms | 13 Or Less</option>
								<option value="13orMore">Bedrooms | 13 Or More</option>
																		</select>
					</div>
						
					<div class="form-group">
						<select id="rate_min" name="rate_min" class="form-control">
						<option value="">Rate | Min</option>
													<option value="100">Rate | $100.00</option>
													<option value="150">Rate | $150.00</option>
													<option value="200">Rate | $200.00</option>
													<option value="250">Rate | $250.00</option>
													<option value="300">Rate | $300.00</option>
													<option value="350">Rate | $350.00</option>
													<option value="400">Rate | $400.00</option>
													<option value="450">Rate | $450.00</option>
													<option value="500">Rate | $500.00</option>
													<option value="550">Rate | $550.00</option>
													<option value="600">Rate | $600.00</option>
													<option value="650">Rate | $650.00</option>
													<option value="700">Rate | $700.00</option>
													<option value="750">Rate | $750.00</option>
													<option value="800">Rate | $800.00</option>
													<option value="850">Rate | $850.00</option>
													<option value="900">Rate | $900.00</option>
													<option value="950">Rate | $950.00</option>
													<option value="1000">Rate | $1000.00</option>
													<option value="1050">Rate | $1050.00</option>
													<option value="1100">Rate | $1100.00</option>
													<option value="1150">Rate | $1150.00</option>
													<option value="1200">Rate | $1200.00</option>
													<option value="1250">Rate | $1250.00</option>
													<option value="1300">Rate | $1300.00</option>
													<option value="1350">Rate | $1350.00</option>
													<option value="1400">Rate | $1400.00</option>
													<option value="1450">Rate | $1450.00</option>
													<option value="1500">Rate | $1500.00</option>
													<option value="1550">Rate | $1550.00</option>
													<option value="1600">Rate | $1600.00</option>
													<option value="1650">Rate | $1650.00</option>
													<option value="1700">Rate | $1700.00</option>
													<option value="1750">Rate | $1750.00</option>
													<option value="1800">Rate | $1800.00</option>
													<option value="1850">Rate | $1850.00</option>
													<option value="1900">Rate | $1900.00</option>
													<option value="1950">Rate | $1950.00</option>
													<option value="2000">Rate | $2000.00</option>
													<option value="2050">Rate | $2050.00</option>
													<option value="2100">Rate | $2100.00</option>
													<option value="2150">Rate | $2150.00</option>
													<option value="2200">Rate | $2200.00</option>
													<option value="2250">Rate | $2250.00</option>
													<option value="2300">Rate | $2300.00</option>
													<option value="2350">Rate | $2350.00</option>
													<option value="2400">Rate | $2400.00</option>
													<option value="2450">Rate | $2450.00</option>
													<option value="2500">Rate | $2500.00</option>
													<option value="2550">Rate | $2550.00</option>
													<option value="2600">Rate | $2600.00</option>
													<option value="2650">Rate | $2650.00</option>
													<option value="2700">Rate | $2700.00</option>
													<option value="2750">Rate | $2750.00</option>
													<option value="2800">Rate | $2800.00</option>
													<option value="2850">Rate | $2850.00</option>
													<option value="2900">Rate | $2900.00</option>
													<option value="2950">Rate | $2950.00</option>
													<option value="3000">Rate | $3000.00</option>
													<option value="3050">Rate | $3050.00</option>
													<option value="3100">Rate | $3100.00</option>
													<option value="3150">Rate | $3150.00</option>
													<option value="3200">Rate | $3200.00</option>
													<option value="3250">Rate | $3250.00</option>
													<option value="3300">Rate | $3300.00</option>
													<option value="3350">Rate | $3350.00</option>
													<option value="3400">Rate | $3400.00</option>
													<option value="3450">Rate | $3450.00</option>
													<option value="3500">Rate | $3500.00</option>
													<option value="3550">Rate | $3550.00</option>
													<option value="3600">Rate | $3600.00</option>
													<option value="3650">Rate | $3650.00</option>
													<option value="3700">Rate | $3700.00</option>
													<option value="3750">Rate | $3750.00</option>
													<option value="3800">Rate | $3800.00</option>
													<option value="3850">Rate | $3850.00</option>
													<option value="3900">Rate | $3900.00</option>
													<option value="3950">Rate | $3950.00</option>
																			<option value="4000">Rate | $4000.00</option>
													<option value="4100">Rate | $4100.00</option>
													<option value="4200">Rate | $4200.00</option>
													<option value="4300">Rate | $4300.00</option>
													<option value="4400">Rate | $4400.00</option>
													<option value="4500">Rate | $4500.00</option>
													<option value="4600">Rate | $4600.00</option>
													<option value="4700">Rate | $4700.00</option>
													<option value="4800">Rate | $4800.00</option>
													<option value="4900">Rate | $4900.00</option>
													<option value="5000">Rate | $5000.00</option>
													<option value="5100">Rate | $5100.00</option>
													<option value="5200">Rate | $5200.00</option>
													<option value="5300">Rate | $5300.00</option>
													<option value="5400">Rate | $5400.00</option>
													<option value="5500">Rate | $5500.00</option>
													<option value="5600">Rate | $5600.00</option>
													<option value="5700">Rate | $5700.00</option>
													<option value="5800">Rate | $5800.00</option>
													<option value="5900">Rate | $5900.00</option>
													<option value="6000">Rate | $6000.00</option>
													<option value="6100">Rate | $6100.00</option>
													<option value="6200">Rate | $6200.00</option>
													<option value="6300">Rate | $6300.00</option>
													<option value="6400">Rate | $6400.00</option>
													<option value="6500">Rate | $6500.00</option>
													<option value="6600">Rate | $6600.00</option>
													<option value="6700">Rate | $6700.00</option>
													<option value="6800">Rate | $6800.00</option>
													<option value="6900">Rate | $6900.00</option>
													<option value="7000">Rate | $7000.00</option>
													<option value="7100">Rate | $7100.00</option>
													<option value="7200">Rate | $7200.00</option>
													<option value="7300">Rate | $7300.00</option>
													<option value="7400">Rate | $7400.00</option>
													<option value="7500">Rate | $7500.00</option>
													<option value="7600">Rate | $7600.00</option>
													<option value="7700">Rate | $7700.00</option>
													<option value="7800">Rate | $7800.00</option>
													<option value="7900">Rate | $7900.00</option>
													<option value="8000">Rate | $8000.00</option>
													<option value="8100">Rate | $8100.00</option>
													<option value="8200">Rate | $8200.00</option>
													<option value="8300">Rate | $8300.00</option>
													<option value="8400">Rate | $8400.00</option>
													<option value="8500">Rate | $8500.00</option>
													<option value="8600">Rate | $8600.00</option>
													<option value="8700">Rate | $8700.00</option>
													<option value="8800">Rate | $8800.00</option>
													<option value="8900">Rate | $8900.00</option>
													<option value="9000">Rate | $9000.00</option>
													<option value="9100">Rate | $9100.00</option>
													<option value="9200">Rate | $9200.00</option>
													<option value="9300">Rate | $9300.00</option>
													<option value="9400">Rate | $9400.00</option>
													<option value="9500">Rate | $9500.00</option>
													<option value="9600">Rate | $9600.00</option>
													<option value="9700">Rate | $9700.00</option>
													<option value="9800">Rate | $9800.00</option>
													<option value="9900">Rate | $9900.00</option>
																			<option value="10000">Rate | $10000.00</option>
													<option value="11000">Rate | $11000.00</option>
													<option value="12000">Rate | $12000.00</option>
													<option value="13000">Rate | $13000.00</option>
													<option value="14000">Rate | $14000.00</option>
													<option value="15000">Rate | $15000.00</option>
													<option value="16000">Rate | $16000.00</option>
													<option value="17000">Rate | $17000.00</option>
													<option value="18000">Rate | $18000.00</option>
													<option value="19000">Rate | $19000.00</option>
													<option value="20000">Rate | $20000.00</option>
													<option value="21000">Rate | $21000.00</option>
													<option value="22000">Rate | $22000.00</option>
													<option value="23000">Rate | $23000.00</option>
													<option value="24000">Rate | $24000.00</option>
													<option value="25000">Rate | $25000.00</option>
													<option value="26000">Rate | $26000.00</option>
													<option value="27000">Rate | $27000.00</option>
													<option value="28000">Rate | $28000.00</option>
													<option value="29000">Rate | $29000.00</option>
													<option value="30000">Rate | $30000.00</option>
													<option value="31000">Rate | $31000.00</option>
													<option value="32000">Rate | $32000.00</option>
													<option value="33000">Rate | $33000.00</option>
													<option value="34000">Rate | $34000.00</option>
													<option value="35000">Rate | $35000.00</option>
													<option value="36000">Rate | $36000.00</option>
													<option value="37000">Rate | $37000.00</option>
													<option value="38000">Rate | $38000.00</option>
													<option value="39000">Rate | $39000.00</option>
													<option value="40000">Rate | $40000.00</option>
													<option value="41000">Rate | $41000.00</option>
													<option value="42000">Rate | $42000.00</option>
													<option value="43000">Rate | $43000.00</option>
													<option value="44000">Rate | $44000.00</option>
													<option value="45000">Rate | $45000.00</option>
													<option value="46000">Rate | $46000.00</option>
													<option value="47000">Rate | $47000.00</option>
													<option value="48000">Rate | $48000.00</option>
													<option value="49000">Rate | $49000.00</option>
													<option value="50000">Rate | $50000.00</option>
													<option value="51000">Rate | $51000.00</option>
													<option value="52000">Rate | $52000.00</option>
													<option value="53000">Rate | $53000.00</option>
													<option value="54000">Rate | $54000.00</option>
													<option value="55000">Rate | $55000.00</option>
													<option value="56000">Rate | $56000.00</option>
													<option value="57000">Rate | $57000.00</option>
													<option value="58000">Rate | $58000.00</option>
													<option value="59000">Rate | $59000.00</option>
													<option value="60000">Rate | $60000.00</option>
													<option value="61000">Rate | $61000.00</option>
													<option value="62000">Rate | $62000.00</option>
													<option value="63000">Rate | $63000.00</option>
													<option value="64000">Rate | $64000.00</option>
													<option value="65000">Rate | $65000.00</option>
													<option value="66000">Rate | $66000.00</option>
													<option value="67000">Rate | $67000.00</option>
													<option value="68000">Rate | $68000.00</option>
													<option value="69000">Rate | $69000.00</option>
													<option value="70000">Rate | $70000.00</option>
													<option value="71000">Rate | $71000.00</option>
													<option value="72000">Rate | $72000.00</option>
													<option value="73000">Rate | $73000.00</option>
													<option value="74000">Rate | $74000.00</option>
													<option value="75000">Rate | $75000.00</option>
													<option value="76000">Rate | $76000.00</option>
													<option value="77000">Rate | $77000.00</option>
													<option value="78000">Rate | $78000.00</option>
													<option value="79000">Rate | $79000.00</option>
													<option value="80000">Rate | $80000.00</option>
													<option value="81000">Rate | $81000.00</option>
													<option value="82000">Rate | $82000.00</option>
													<option value="83000">Rate | $83000.00</option>
													<option value="84000">Rate | $84000.00</option>
													<option value="85000">Rate | $85000.00</option>
													<option value="86000">Rate | $86000.00</option>
													<option value="87000">Rate | $87000.00</option>
													<option value="88000">Rate | $88000.00</option>
													<option value="89000">Rate | $89000.00</option>
													<option value="90000">Rate | $90000.00</option>
													<option value="91000">Rate | $91000.00</option>
													<option value="92000">Rate | $92000.00</option>
													<option value="93000">Rate | $93000.00</option>
													<option value="94000">Rate | $94000.00</option>
													<option value="95000">Rate | $95000.00</option>
													<option value="96000">Rate | $96000.00</option>
													<option value="97000">Rate | $97000.00</option>
													<option value="98000">Rate | $98000.00</option>
													<option value="99000">Rate | $99000.00</option>
													<option value="100000">Rate | $100000.00</option>
											</select>
					</div>
					
					<div class="form-group">
						<select id="rate_max" name="rate_max" class="form-control">
						<option value="any" selected="selected">Rate | Max</option>
													<option value="100">Rate | $100.00</option>
													<option value="150">Rate | $150.00</option>
													<option value="200">Rate | $200.00</option>
													<option value="250">Rate | $250.00</option>
													<option value="300">Rate | $300.00</option>
													<option value="350">Rate | $350.00</option>
													<option value="400">Rate | $400.00</option>
													<option value="450">Rate | $450.00</option>
													<option value="500">Rate | $500.00</option>
													<option value="550">Rate | $550.00</option>
													<option value="600">Rate | $600.00</option>
													<option value="650">Rate | $650.00</option>
													<option value="700">Rate | $700.00</option>
													<option value="750">Rate | $750.00</option>
													<option value="800">Rate | $800.00</option>
													<option value="850">Rate | $850.00</option>
													<option value="900">Rate | $900.00</option>
													<option value="950">Rate | $950.00</option>
													<option value="1000">Rate | $1000.00</option>
													<option value="1050">Rate | $1050.00</option>
													<option value="1100">Rate | $1100.00</option>
													<option value="1150">Rate | $1150.00</option>
													<option value="1200">Rate | $1200.00</option>
													<option value="1250">Rate | $1250.00</option>
													<option value="1300">Rate | $1300.00</option>
													<option value="1350">Rate | $1350.00</option>
													<option value="1400">Rate | $1400.00</option>
													<option value="1450">Rate | $1450.00</option>
													<option value="1500">Rate | $1500.00</option>
													<option value="1550">Rate | $1550.00</option>
													<option value="1600">Rate | $1600.00</option>
													<option value="1650">Rate | $1650.00</option>
													<option value="1700">Rate | $1700.00</option>
													<option value="1750">Rate | $1750.00</option>
													<option value="1800">Rate | $1800.00</option>
													<option value="1850">Rate | $1850.00</option>
													<option value="1900">Rate | $1900.00</option>
													<option value="1950">Rate | $1950.00</option>
													<option value="2000">Rate | $2000.00</option>
													<option value="2050">Rate | $2050.00</option>
													<option value="2100">Rate | $2100.00</option>
													<option value="2150">Rate | $2150.00</option>
													<option value="2200">Rate | $2200.00</option>
													<option value="2250">Rate | $2250.00</option>
													<option value="2300">Rate | $2300.00</option>
													<option value="2350">Rate | $2350.00</option>
													<option value="2400">Rate | $2400.00</option>
													<option value="2450">Rate | $2450.00</option>
													<option value="2500">Rate | $2500.00</option>
													<option value="2550">Rate | $2550.00</option>
													<option value="2600">Rate | $2600.00</option>
													<option value="2650">Rate | $2650.00</option>
													<option value="2700">Rate | $2700.00</option>
													<option value="2750">Rate | $2750.00</option>
													<option value="2800">Rate | $2800.00</option>
													<option value="2850">Rate | $2850.00</option>
													<option value="2900">Rate | $2900.00</option>
													<option value="2950">Rate | $2950.00</option>
													<option value="3000">Rate | $3000.00</option>
													<option value="3050">Rate | $3050.00</option>
													<option value="3100">Rate | $3100.00</option>
													<option value="3150">Rate | $3150.00</option>
													<option value="3200">Rate | $3200.00</option>
													<option value="3250">Rate | $3250.00</option>
													<option value="3300">Rate | $3300.00</option>
													<option value="3350">Rate | $3350.00</option>
													<option value="3400">Rate | $3400.00</option>
													<option value="3450">Rate | $3450.00</option>
													<option value="3500">Rate | $3500.00</option>
													<option value="3550">Rate | $3550.00</option>
													<option value="3600">Rate | $3600.00</option>
													<option value="3650">Rate | $3650.00</option>
													<option value="3700">Rate | $3700.00</option>
													<option value="3750">Rate | $3750.00</option>
													<option value="3800">Rate | $3800.00</option>
													<option value="3850">Rate | $3850.00</option>
													<option value="3900">Rate | $3900.00</option>
													<option value="3950">Rate | $3950.00</option>
																			<option value="4000">Rate | $4000.00</option>
													<option value="4100">Rate | $4100.00</option>
													<option value="4200">Rate | $4200.00</option>
													<option value="4300">Rate | $4300.00</option>
													<option value="4400">Rate | $4400.00</option>
													<option value="4500">Rate | $4500.00</option>
													<option value="4600">Rate | $4600.00</option>
													<option value="4700">Rate | $4700.00</option>
													<option value="4800">Rate | $4800.00</option>
													<option value="4900">Rate | $4900.00</option>
													<option value="5000">Rate | $5000.00</option>
													<option value="5100">Rate | $5100.00</option>
													<option value="5200">Rate | $5200.00</option>
													<option value="5300">Rate | $5300.00</option>
													<option value="5400">Rate | $5400.00</option>
													<option value="5500">Rate | $5500.00</option>
													<option value="5600">Rate | $5600.00</option>
													<option value="5700">Rate | $5700.00</option>
													<option value="5800">Rate | $5800.00</option>
													<option value="5900">Rate | $5900.00</option>
													<option value="6000">Rate | $6000.00</option>
													<option value="6100">Rate | $6100.00</option>
													<option value="6200">Rate | $6200.00</option>
													<option value="6300">Rate | $6300.00</option>
													<option value="6400">Rate | $6400.00</option>
													<option value="6500">Rate | $6500.00</option>
													<option value="6600">Rate | $6600.00</option>
													<option value="6700">Rate | $6700.00</option>
													<option value="6800">Rate | $6800.00</option>
													<option value="6900">Rate | $6900.00</option>
													<option value="7000">Rate | $7000.00</option>
													<option value="7100">Rate | $7100.00</option>
													<option value="7200">Rate | $7200.00</option>
													<option value="7300">Rate | $7300.00</option>
													<option value="7400">Rate | $7400.00</option>
													<option value="7500">Rate | $7500.00</option>
													<option value="7600">Rate | $7600.00</option>
													<option value="7700">Rate | $7700.00</option>
													<option value="7800">Rate | $7800.00</option>
													<option value="7900">Rate | $7900.00</option>
													<option value="8000">Rate | $8000.00</option>
													<option value="8100">Rate | $8100.00</option>
													<option value="8200">Rate | $8200.00</option>
													<option value="8300">Rate | $8300.00</option>
													<option value="8400">Rate | $8400.00</option>
													<option value="8500">Rate | $8500.00</option>
													<option value="8600">Rate | $8600.00</option>
													<option value="8700">Rate | $8700.00</option>
													<option value="8800">Rate | $8800.00</option>
													<option value="8900">Rate | $8900.00</option>
													<option value="9000">Rate | $9000.00</option>
													<option value="9100">Rate | $9100.00</option>
													<option value="9200">Rate | $9200.00</option>
													<option value="9300">Rate | $9300.00</option>
													<option value="9400">Rate | $9400.00</option>
													<option value="9500">Rate | $9500.00</option>
													<option value="9600">Rate | $9600.00</option>
													<option value="9700">Rate | $9700.00</option>
													<option value="9800">Rate | $9800.00</option>
													<option value="9900">Rate | $9900.00</option>
																			<option value="10000">Rate | $10000.00</option>
													<option value="11000">Rate | $11000.00</option>
													<option value="12000">Rate | $12000.00</option>
													<option value="13000">Rate | $13000.00</option>
													<option value="14000">Rate | $14000.00</option>
													<option value="15000">Rate | $15000.00</option>
													<option value="16000">Rate | $16000.00</option>
													<option value="17000">Rate | $17000.00</option>
													<option value="18000">Rate | $18000.00</option>
													<option value="19000">Rate | $19000.00</option>
													<option value="20000">Rate | $20000.00</option>
													<option value="21000">Rate | $21000.00</option>
													<option value="22000">Rate | $22000.00</option>
													<option value="23000">Rate | $23000.00</option>
													<option value="24000">Rate | $24000.00</option>
													<option value="25000">Rate | $25000.00</option>
													<option value="26000">Rate | $26000.00</option>
													<option value="27000">Rate | $27000.00</option>
													<option value="28000">Rate | $28000.00</option>
													<option value="29000">Rate | $29000.00</option>
													<option value="30000">Rate | $30000.00</option>
													<option value="31000">Rate | $31000.00</option>
													<option value="32000">Rate | $32000.00</option>
													<option value="33000">Rate | $33000.00</option>
													<option value="34000">Rate | $34000.00</option>
													<option value="35000">Rate | $35000.00</option>
													<option value="36000">Rate | $36000.00</option>
													<option value="37000">Rate | $37000.00</option>
													<option value="38000">Rate | $38000.00</option>
													<option value="39000">Rate | $39000.00</option>
													<option value="40000">Rate | $40000.00</option>
													<option value="41000">Rate | $41000.00</option>
													<option value="42000">Rate | $42000.00</option>
													<option value="43000">Rate | $43000.00</option>
													<option value="44000">Rate | $44000.00</option>
													<option value="45000">Rate | $45000.00</option>
													<option value="46000">Rate | $46000.00</option>
													<option value="47000">Rate | $47000.00</option>
													<option value="48000">Rate | $48000.00</option>
													<option value="49000">Rate | $49000.00</option>
													<option value="50000">Rate | $50000.00</option>
													<option value="51000">Rate | $51000.00</option>
													<option value="52000">Rate | $52000.00</option>
													<option value="53000">Rate | $53000.00</option>
													<option value="54000">Rate | $54000.00</option>
													<option value="55000">Rate | $55000.00</option>
													<option value="56000">Rate | $56000.00</option>
													<option value="57000">Rate | $57000.00</option>
													<option value="58000">Rate | $58000.00</option>
													<option value="59000">Rate | $59000.00</option>
													<option value="60000">Rate | $60000.00</option>
													<option value="61000">Rate | $61000.00</option>
													<option value="62000">Rate | $62000.00</option>
													<option value="63000">Rate | $63000.00</option>
													<option value="64000">Rate | $64000.00</option>
													<option value="65000">Rate | $65000.00</option>
													<option value="66000">Rate | $66000.00</option>
													<option value="67000">Rate | $67000.00</option>
													<option value="68000">Rate | $68000.00</option>
													<option value="69000">Rate | $69000.00</option>
													<option value="70000">Rate | $70000.00</option>
													<option value="71000">Rate | $71000.00</option>
													<option value="72000">Rate | $72000.00</option>
													<option value="73000">Rate | $73000.00</option>
													<option value="74000">Rate | $74000.00</option>
													<option value="75000">Rate | $75000.00</option>
													<option value="76000">Rate | $76000.00</option>
													<option value="77000">Rate | $77000.00</option>
													<option value="78000">Rate | $78000.00</option>
													<option value="79000">Rate | $79000.00</option>
													<option value="80000">Rate | $80000.00</option>
													<option value="81000">Rate | $81000.00</option>
													<option value="82000">Rate | $82000.00</option>
													<option value="83000">Rate | $83000.00</option>
													<option value="84000">Rate | $84000.00</option>
													<option value="85000">Rate | $85000.00</option>
													<option value="86000">Rate | $86000.00</option>
													<option value="87000">Rate | $87000.00</option>
													<option value="88000">Rate | $88000.00</option>
													<option value="89000">Rate | $89000.00</option>
													<option value="90000">Rate | $90000.00</option>
													<option value="91000">Rate | $91000.00</option>
													<option value="92000">Rate | $92000.00</option>
													<option value="93000">Rate | $93000.00</option>
													<option value="94000">Rate | $94000.00</option>
													<option value="95000">Rate | $95000.00</option>
													<option value="96000">Rate | $96000.00</option>
													<option value="97000">Rate | $97000.00</option>
													<option value="98000">Rate | $98000.00</option>
													<option value="99000">Rate | $99000.00</option>
													<option value="100000">Rate | $100000.00</option>
											</select>
					</div>
					
					<div class="form-group">
						<select id="period" name="period" class="form-control">
						<option value="">Rate period</option>
						<option value="day">Daily</option>
						<option value="week">Weekly</option>
						<option value="month">Monthly</option>
						<option value="season">per Season</option>
					</select>
					</div>
					
				</div>
				

			</div>
		
	</section>
	
	
	<section class="result-area">
		<div class="container">
			<div class="hed crossline"><h2>LAST MINUTE DEALS | SHORESUMMERRENTALS.COM</h2><hr/></div>
			
			
			<div class="result__lft col-sm-9">
				<div class="noresults">
                           <div class="resultsMessage">
                                <h1>No Results Returned for your search</h1><p>Your search returned no results. Although this is unusual, it happens from time to time. Perhaps the search term you used is a little generic, or perhaps we just don't have any content for that search.</p><h2>Suggestions:</h2>»Be more specific with your search terms<br>»Check your spelling<br>»If you can't find via search, try browsing by section<br><br><p>If you believe you have come here in error, please contact the site manager and report a problem.</p>
						</div>
				</div>
				
				
				<div class="results-info-listing">
					<div class="search-info">
						<p><i>52</i> Results</p>
					</div>
				</div>
				
				
				<div class="tabs-area col-sm-12 p0">
					<!-- Nav tabs -->
					<ul class="nav nav-tabs">
					  <li class="active"><a href="#listView" data-toggle="tab"><i class="fa fa-list"></i> List View</a></li>
					  <li><a href="#mapView" data-toggle="tab"><i class="fa fa-map-marker"></i> Map View</a></li>
					</ul>

					<!-- Tab panes -->
					<div class="tab-content">
					  
					  <div class="tab-pane active" id="listView">
					  
					  
					  					
	<div class="result-box result--thumb">

        <div id="listing_summary_5890" class="result_summary summary-small summary ">
            
                        <div class="summary__img col-sm-3">
                            <a href="#" class="image"><img src="images/5267_photo_23673.jpg" alt="" ></a>
                        </div>
                        <div class="summary__dtl col-sm-6">
                            <h4><a href="http://www.shoresummerrentals.com/listing/5890.html">Homestead 508, Studio Bedroom, 1 Bathroom, Sleeps 2</a></h4>

                                <div class="summary-address">
                                    <address>
										<span>Location: 805 East 8th Street, 508, 08226</span>
                                    </address>
                                </div>
								
								<p>Studio BR, 1 BA, Sleeps 2</p>
								
                            <p>Rental ID# <a href="http://www.shoresummerrentals.com/listing/5890.html">5890</a></p>
							
                            <p class="quick-info">
                                <a href="" data-toggle="modal" data-target="#quickInfo5890">
                                    <i class="fa fa-chevron-right"></i> Quick Property Info
                                </a>
                            </p>

                            <div class="review">
                                <div class="rate">
									<div class="rate-stars">
										<div class="stars-rating color-6">
											<div class="rate-0"></div>
										</div>
									</div>
									<p><a rel="nofollow" href="https://www.shoresummerrentals.com/popup/popup.php?pop_type=reviewformpopup&amp;item_type=listing&amp;item_id=5890" class="iframe fancy_window_review">Be the first to review this property!</a></p>
								</div>
							</div>
                        </div>
                        
					<div class="summary__atc col-sm-3 text-right">
							<p>no minimum stay</p>
                            <p><a class="btn btn-primary" href="http://www.shoresummerrentals.com/listing/5890.html">View Details</a></p>
 
                                <div class="compare-featured fnc-fom">
                                    <div class="inputgroup"><input id="5890" type="checkbox" name="comparison_chk" value="5890" listingname="Homestead 508" onclick="javascript: compare_check(this);" class="form-control">
                                    <label> Compare </label>
									</div>
                                </div>
                        </div>
						
                    </div>

</div>
						
					
						  					
	<div class="result-box result--thumb">

        <div id="listing_summary_5890" class="result_summary summary-small summary ">
            
                        <div class="summary__img col-sm-3">
                            <a href="#" class="image"><img src="images/5267_photo_23673.jpg" alt="" ></a>
                        </div>
                        <div class="summary__dtl col-sm-6">
                            <h4><a href="http://www.shoresummerrentals.com/listing/5890.html">Homestead 508, Studio Bedroom, 1 Bathroom, Sleeps 2</a></h4>

                                <div class="summary-address">
                                    <address>
										<span>Location: 805 East 8th Street, 508, 08226</span>
                                    </address>
                                </div>
								
								<p>Studio BR, 1 BA, Sleeps 2</p>
								
                            <p>Rental ID# <a href="http://www.shoresummerrentals.com/listing/5890.html">5890</a></p>
							
                            <p class="quick-info">
                                <a href="" data-toggle="modal" data-target="#quickInfo5890">
                                    <i class="fa fa-chevron-right"></i> Quick Property Info
                                </a>
                            </p>

                            <div class="review">
                                <div class="rate">
									<div class="rate-stars">
										<div class="stars-rating color-6">
											<div class="rate-0"></div>
										</div>
									</div>
									<p><a rel="nofollow" href="https://www.shoresummerrentals.com/popup/popup.php?pop_type=reviewformpopup&amp;item_type=listing&amp;item_id=5890" class="iframe fancy_window_review">Be the first to review this property!</a></p>
								</div>
							</div>
                        </div>
                        
					<div class="summary__atc col-sm-3 text-right">
							<p>no minimum stay</p>
                            <p><a class="btn btn-primary" href="http://www.shoresummerrentals.com/listing/5890.html">View Details</a></p>
 
                                <div class="compare-featured fnc-fom">
                                    <div class="inputgroup"><input id="5890" type="checkbox" name="comparison_chk" value="5890" listingname="Homestead 508" onclick="javascript: compare_check(this);" class="form-control">
                                    <label> Compare </label>
									</div>
                                </div>
                        </div>
						
                    </div>

</div>
						
					
						<div class="result-box">
						  <div class="result_summary summary_sm specials">
								<p><strong>Location:</strong> <a href="http://www.shoresummerrentals.com/listing/2426.html">Brigantine, New Jersey 08203</a></p>

								<p><strong>Ends 01/19/2017</strong></p>

								<p><strong>Rental ID:</strong>
								<a href="http://www.shoresummerrentals.com/listing/2426.html">2426 - NEWLY RENOVATED 3 BEDROOM 2ND FLOOR CONDO, 3 Bedrooms, 1.5 Bathrooms, Sleeps 6</a></p>

								<p><strong>Last Minute Specials</strong>: WINTER RENTAL – OFF SEASON RENTAL – PRIME SEASON RENTAL - LONG WEEKEND RENTALS OR BY THE WEEK OR MONTH. CALL OR EMAIL OWNER FOR MORE INFORMATION.</p>

							</div>
						</div>

	
	
						<div class="result-box">
						  <div class="result_summary summary_sm specials">
								<p><strong>Location:</strong> <a href="http://www.shoresummerrentals.com/listing/2426.html">Brigantine, New Jersey 08203</a></p>

								<p><strong>Ends 01/19/2017</strong></p>

								<p><strong>Rental ID:</strong>
								<a href="http://www.shoresummerrentals.com/listing/2426.html">2426 - NEWLY RENOVATED 3 BEDROOM 2ND FLOOR CONDO, 3 Bedrooms, 1.5 Bathrooms, Sleeps 6</a></p>

								<p><strong>Last Minute Specials</strong>: WINTER RENTAL – OFF SEASON RENTAL – PRIME SEASON RENTAL - LONG WEEKEND RENTALS OR BY THE WEEK OR MONTH. CALL OR EMAIL OWNER FOR MORE INFORMATION.</p>

							</div>
						</div>

	
	
						<div class="result-box">
						  <div class="result_summary summary_sm specials">
								<p><strong>Location:</strong> <a href="http://www.shoresummerrentals.com/listing/2426.html">Brigantine, New Jersey 08203</a></p>

								<p><strong>Ends 01/19/2017</strong></p>

								<p><strong>Rental ID:</strong>
								<a href="http://www.shoresummerrentals.com/listing/2426.html">2426 - NEWLY RENOVATED 3 BEDROOM 2ND FLOOR CONDO, 3 Bedrooms, 1.5 Bathrooms, Sleeps 6</a></p>

								<p><strong>Last Minute Specials</strong>: WINTER RENTAL – OFF SEASON RENTAL – PRIME SEASON RENTAL - LONG WEEKEND RENTALS OR BY THE WEEK OR MONTH. CALL OR EMAIL OWNER FOR MORE INFORMATION.</p>

							</div>
						</div>

	
	
						<div class="result-box">
						  <div class="result_summary summary_sm specials">
								<p><strong>Location:</strong> <a href="http://www.shoresummerrentals.com/listing/2426.html">Brigantine, New Jersey 08203</a></p>

								<p><strong>Ends 01/19/2017</strong></p>

								<p><strong>Rental ID:</strong>
								<a href="http://www.shoresummerrentals.com/listing/2426.html">2426 - NEWLY RENOVATED 3 BEDROOM 2ND FLOOR CONDO, 3 Bedrooms, 1.5 Bathrooms, Sleeps 6</a></p>

								<p><strong>Last Minute Specials</strong>: WINTER RENTAL – OFF SEASON RENTAL – PRIME SEASON RENTAL - LONG WEEKEND RENTALS OR BY THE WEEK OR MONTH. CALL OR EMAIL OWNER FOR MORE INFORMATION.</p>

							</div>
						</div>

	


						
						
	
	
					  </div>
					  
					  <div class="tab-pane" id="mapView">
					  
						MAP ...
					  
					  </div>
	
					</div>
				</div>
				
						
			</div>
			
			<div class="result__rgt aside col-sm-3  ">
			
			<div id="return_filter" class="sidebar-filters clrlist listview clrhm">
			
			<h2>Compare Properties</h2>
			
            <div class="filter-box p20">
                <ul>
                    <div id="comparison_itemDiv"></div>
                </ul>
                <div align="center">
                    <button class="btn btn-primary btn-small" id="compare_btn" data-toggle="tooltip" title="" data-original-title="needs 2 or more to compare">Compare</button>
                    
                    <button class="btn btn-small" id="clear_btn" type="reset" >Clear</button>
                </div>
				<script>
				jQuery('button').tooltip();
			</script>
            </div> 
         

                    
            <div>
                
                <h2>You refined by...</h2>

                <div class="filter-box ">
                    <ul>                   
                        <li>
                            <b>Last Minute Specials</b>
                            <span>
                                <a rel="nofollow" href="https://www.shoresummerrentals.com/rating-5/orderby-city">x</a>
                            </span>
                        </li>
                        
                                                
                        <li>
                            <b>5 Stars</b>
                            <span>
                                <a rel="nofollow" href="https://www.shoresummerrentals.com/listing/orderby-city/specials-lastminutespecials">x</a>
                            </span>
                        </li>
                        
                                                
                    </ul>
                                        <a rel="nofollow" class="remove-all" href="https://www.shoresummerrentals.com/listing/search">Remove all</a>
                </div>
                
            </div>
        
        
        <h2>Refine Search</h2>
        <div class="">
      
			<div class="panel-group accordion-plus" id="accordion">
					  
					  <div class="panel panel-default active">
						<div class="panel-heading">
						  <h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#side_collapse1">
							  State
							</a>
						  </h4>
						</div>
						<div id="side_collapse1" class="panel-collapse collapse in">
						  <div class="panel-body">
							<ul id="list_location" class="item-select">
								<li>
									<a rel="nofollow" href="https://www.shoresummerrentals.com/listing/florida/rating-5/orderby-city/specials-lastminutespecials">Florida</a>
								</li>
								<li>
									<a rel="nofollow" href="https://www.shoresummerrentals.com/listing/new-jersey/rating-5/orderby-city/specials-lastminutespecials">New Jersey</a>
								</li>
							</ul>
						  </div>
						</div>
					  </div>
					  
					  <div class="panel panel-default">
						<div class="panel-heading">
						  <h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse2" class="collapsed">
							  Special Offers
							</a>
						  </h4>
						</div>
						<div id="collapse2" class="panel-collapse collapse ">
						  <div class="panel-body">
							       
                <ul id="list_specials" class="item-select">
                    
                                    <li>
                        <a rel="nofollow" href="https://www.shoresummerrentals.com/rating-5/orderby-city/specials-lastminutespecials/view-ocean_front">Beachfront Rentals</a>
                    </li>
                                    <li>
                        <a rel="nofollow" href="https://www.shoresummerrentals.com/rating-5/orderby-city/specials-lastminutespecials-fullsummerrentals">Full Summer Rentals</a>
                    </li>
                                    <li>
                        <a rel="nofollow" href="https://www.shoresummerrentals.com/rating-5/orderby-city/specials-lastminutespecials-underageof25">Under Age of 25</a>
                    </li>
                                    <li>
                        <a rel="nofollow" href="https://www.shoresummerrentals.com/rating-5/orderby-city/specials-lastminutespecials/feature-petsallowed">Pet Friendly Rentals</a>
                    </li>
                                    <li>
                        <a rel="nofollow" href="https://www.shoresummerrentals.com/rating-5/orderby-city/specials-lastminutespecials/view-bay_front">Bayfront Rentals</a>
                    </li>
                                    <li class="active">
                        <a rel="nofollow" href="https://www.shoresummerrentals.com/rating-5/orderby-city/specials-lastminutespecials">Last Minute Specials</a>
                    </li>
                                    <li>
                        <a rel="nofollow" href="https://www.shoresummerrentals.com/rating-5/orderby-city/specials-lastminutespecials-partialweekrentals">Partial Week Rentals</a>
                    </li>
                                    <li>
                        <a rel="nofollow" href="https://www.shoresummerrentals.com/rating-5/orderby-city/specials-lastminutespecials-wintermonthlyrentals">Winter Monthly Rentals</a>
                    </li>
                                    
                </ul>             

                  
						  </div>
						</div>
					  </div>
				
					  <div class="panel panel-default">
						<div class="panel-heading">
						  <h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#side_collapse3" class="collapsed">
							  Rating
							</a>
						  </h4>
						</div>
						<div id="side_collapse3" class="panel-collapse collapse ">
						  <div class="panel-body">
								<ul id="list_rating" class="item-select">
									<li class="active">
										<a rel="nofollow" href="https://www.shoresummerrentals.com/orderby-city/specials-lastminutespecials/rating-5">
											5 Stars                        </a>
									</li>
								</ul>
						  </div>
						</div>
					  </div>
				</div>
			
        
                <input type="hidden" id="maxcomparison_itens" value="5">
        <input type="hidden" id="comparison_url" value="http://www.shoresummerrentals.com/listing/compare_listing.php">
		</div>

                </div>
				
			</div>
			
			
			
			<section class="featured-area mt30 imgzoom-hover">
				<div class="container">
					
					<div class="hed crossline"><h2>FEATURED RENTAL PROPERTIES</h2><hr/></div>
					
				<div class="featured-box col-sm-3 cols-5">
					<div class="featured__inr">
						<div class="featured__img">
							<img src="images/featured1.jpg" alt="">
						</div>
						<div class="featured__cont">
						<ul>
							<li>Ocean City</li>
							<li>New Jersey</li>
							<li>4 Beds | Bathrooms 2 | Sleeps 8</li>
						</ul>
						<div class="lnk-btn more-btn"><a href="#">View Details</a></div>
						</div>
					</div>
				</div>
				
				
				<div class="featured-box col-sm-3 cols-5">
					<div class="featured__inr">
						<div class="featured__img">
							<img src="images/featured2.jpg" alt="">
						</div>
						<div class="featured__cont">
						<ul>
							<li>Ocean City</li>
							<li>New Jersey</li>
							<li>4 Beds | Bathrooms 2 | Sleeps 8</li>
						</ul>
						<div class="lnk-btn more-btn"><a href="#">View Details</a></div>
						</div>
					</div>
				</div>
				
				<div class="featured-box col-sm-3 cols-5">
					<div class="featured__inr">
						<div class="featured__img">
							<img src="images/featured3.jpg" alt="">
						</div>
						<div class="featured__cont">
						<ul>
							<li>Ocean City</li>
							<li>New Jersey</li>
							<li>4 Beds | Bathrooms 2 | Sleeps 8</li>
						</ul>
						<div class="lnk-btn more-btn"><a href="#">View Details</a></div>
						</div>
					</div>
				</div>
				
				
				<div class="featured-box col-sm-3 cols-5">
					<div class="featured__inr">
						<div class="featured__img">
							<img src="images/featured4.jpg" alt="">
						</div>
						<div class="featured__cont">
						<ul>
							<li>Ocean City</li>
							<li>New Jersey</li>
							<li>4 Beds | Bathrooms 2 | Sleeps 8</li>
						</ul>
						<div class="lnk-btn more-btn"><a href="#">View Details</a></div>
						</div>
					</div>
				</div>
				
							
				
				<div class="featured-box col-sm-3 cols-5">
					<div class="featured__inr">
						<div class="featured__img">
							<img src="images/featured4.jpg" alt="">
						</div>
						<div class="featured__cont">
						<ul>
							<li>Ocean City</li>
							<li>New Jersey</li>
							<li>4 Beds | Bathrooms 2 | Sleeps 8</li>
						</ul>
						<div class="lnk-btn more-btn"><a href="#">View Details</a></div>
						</div>
					</div>
				</div>
				
				</div>
			</section>
			
			
			
		</div>
	</section>
	
	
<?php include("footer.php"); ?>