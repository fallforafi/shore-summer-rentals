<?php include("header.php"); ?>

	
	<section class="spam-warning-page">
		<div class="container">
			<h2>EMAIL CONFIRMATION</h2>
			<h3>Avoid Missing Messages from Renters</h3>
		<p>Please add <a href="mailto:webmaster@shoresummerrentals.com">webmaster@shoresummerrentals.com</a>, maria@shoresummerrentals.com, chris@shoresummerrentals.com, raya@shoresummerrentals.com and info@shoresummerrentals.com to your e-mail contact and/or e-mail address book to avoid renter contact e-mails being tagged as spam and not delivered to your inbox. This SHOULD help assure delivery of the e-mail inquiry to your e-mail account. We send ALL contact e-mails from potential renters to the e-mail accounts you provide when registering but we cannot control what your e-mail service provider does with it once it leaves our servers. As a backup to this, we provide a history of ALL e-mail contacts you receive in your E-mail History Tab that can be accessed in the&nbsp;dashboard section&nbsp;when logged into your account. If you notice e-mails in your history that never made it to your inbox, please contact your service provider.</p>
	
		<p>It is strictly the owners responsibility to <a href="https://secure.vacationrentals2u.com/login/">login</a> to your <a href="https://secure.vacationrentals2u.com/login/">Shore</a><a href="https://secure.vacationrentals2u.com/login/">SummerRentals.com</a> account daily to ensure that all of the renter inquiries sent to your account are retrieved.</p>
	
		</div>
	</section>
	
	
<?php include("footer.php"); ?>