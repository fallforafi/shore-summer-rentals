<?php include("header.php"); ?>

	
	<section class="login-page">
		<div class="container">
			<h1>OWNER FRAUD ALERT</h1>
				<h3>Protecting Your Information</h3>
				
				
				<h4>Thank you from an owner</h4>
				<p>Maria, We would like to thank you for the warning of not accepting checks for more than rental rates and then being asked to send money order back to the potential tenants. We had received a check for $8,500.00. I took it to the bank and indicated my concern. After 5 days the bank has to assume that the check is good and they credit your account. They asked me to wait a few days before sending money order.  Within a few days, the federal reserve responded indicating there was no such account. By your alert, we were not out $3,500.00.</p>
				
				<h4>See Scam Details Below</h4>
				<p>We've recently been alerted to the fact that there is a scam being used to promote a "Counterfeit Cashier's Check Scam" to property owners, not on our site but others. For your own protection, please read this email in its entirety for explanations of variations of these scams and how to help protect yourself.</p>
				
				<p>Please remember that all monetary transactions involve risk and if anything seems out of the ordinary, it might warrant extra scrutiny. The specific scams we have been alerted to go something like this:</p>
				
				<h4>Variation 1:</h4>
				<ul>
					<li>1) Cashier's check is offered for deposit/rent.</li>
					<li>2) Value of cashier's check exceeds the actual price - renter asks you to wire/send the balance back.</li>
					<li>3) Banks will cash the fake cashier's check, release the funds to your account, AND THEN HOLD YOU RESPONSIBLE WHEN THE CHECK IS LATER DETERMINED TO BE FAKE.</li>
				</ul>
		
				<h4>Variation 2:</h4>
				<ul>
					<li>1) Cashier's check is offered for deposit/rent.</li>
					<li>2) Value of cashier's check is the correct amount, however the renter immediately cancels and asks you to wire/send the balance back (and keep a rebooking fee for the cancellation).</li>
					<li>3) Banks will cash the fake cashier's check, release the funds to your account, AND THEN HOLD YOU RESPONSIBLE WHEN THE CHECK IS LATER DETERMINED TO BE FAKE.</li>
				</ul>
				
				<p>Remember, counterfeit cashier's checks are usually cashed by your bank and the funds are released to you. It might take many days before the bank determines it is fake and holds you responsible for the amount. You should also never accept a cashier's check for an amount more than is owed to you. If you receive a cashier's check, please do not immediately issue any refund to a renter until you verify the check is valid. Please contact your bank for more information on confirming the validity of a cashier's check, especially if it is of international origin.</p>
				
				
				<p>If you feel like you have been the victim of fraud on the Internet, please contact your local law enforcement. In the United States, you may also file a complaint of Internet fraud with the FBI and NW3C at <a href="http://www.ic3.gov/complaint/default.aspx">http://www.ic3.gov/default.aspx</a></p>
				
				
				<h4>WHO TO NOTIFY ABOUT FRAUD OR SCAM ATTEMPTS?</h4>
				
				<ul>
					<li><a href="http://ftc.gov/multimedia/video/scam-watch/file-a-complaint.shtm">FTC Video: How to report scams to the FTC</a></li>
					<li>FTC toll free hotline: 877-FTC-HELP (877-382-4357)</li>
					<li><a href="http://www.ftc.gov/"><span style="color: #0000ff;">FTC online complaint form</a></li>
					<li>Canadian PhoneBusters hotline: 888-495-8501</li>
					<li><a href="http://www.competitionbureau.gc.ca/">Competition Bureau Canada</a>: 800-348-5358</li>
					<li><a href="http://www.ic3.gov/">Internet Fraud Complaint Center</a></li>
				</ul>
				
				
		
		</div>
	</section>
	
	
<?php include("footer.php"); ?>