<?php include("header.php"); ?>

	
	<section class="inner-page listing-instructions-page">
		<div class="container">
			<h2>CASH BACK REFERRAL PROGRAM</h2>
			<h3>Tell Your Friends About ShoreSummerRentals.com</h3>
			
			<p>"Get Paid" for sharing <a href="http://www.shoresummerrentals.com">ShoreSummerRentals.com</a> with your friends! <br><br></p>
			
			<p><em><strong>You can easily earn up to $75 for each referral!</strong></em> There is no limit, you can earn as much as you want.&nbsp; It’s easy, when your friends join us, tell them to<a href="contactus.php"> contact us&nbsp;</a>with your name and email address immediately upon <a href="advertise.php">registering</a> on our website.&nbsp; We will take care of the rest. &nbsp;&nbsp;Your friends will be able to advertise on both of our websites, <a href="http://www.shoresummerrentals.com/">ShoreSummerRentals.com</a> &amp; <a href="http://www.vacationrentals2u.com">VacationRentals2U.com</a>&nbsp;for one low price and you get paid in 90 days, immediately after the <a href="guarantee-and-verify.html">Rental Guarantee</a> policy has passed.  You do not even have to be a previous or active customer.</p>
			
			
			<strong>How Does The Referral Program Work?</strong>
			<p>For the first new customer you refer to <a href="http://www.shoresummerrentals.com/">ShoreSummerRentals.com</a>, we will pay you <em><strong>$50 cash</strong></em>. For each additional new customer you send to us within a 12 month period, we will pay you <em><strong>$75 cash for each new referral</strong></em>.&nbsp; There is no limit to the amount of money you can earn.</p>
			
			<p>Each new referral will need to purchase an annual subscription to <a href="http://www.shoresummerrentals.com/">ShoreSummerRentals.com</a>.</p>
			
			<strong>IT'S SIMPLE AND A VERY EASY WAY TO EARN SOME EXTRA CASH QUICK!</strong>
			<p>Who Can Participate?   Anyone!</p>
			<p>THAT’S IT!  REFER YOUR FRIENDS NOW AND START EARNING EASY CASH!</p>
			
			<strong>General Terms and Conditions</strong>
			<p>Referrer cannot refer themselves or their own management company.<br/>
			Referrals must be a brand new <a href="http://www.shoresummerrentals.com/">ShoreSummerRentals.com</a> customer and must not have or previously had a property listing with us at any other time.</p>
			
			<p>Referral &nbsp;bonus' cannot be applied retroactively for word-of-mouth referrals.&nbsp;&nbsp;Again, the referred must <a href="contactus.php">contact us</a>&nbsp;immediately upon <a href="advertise.php">registration</a> on <a href="http://www.shoresummerrentals.com/">ShoreSummerRentals.com</a>.&nbsp; This program is valid beginning November 19, 2010 and can be terminated or modified at any time.<br><br>
			This offer is not transferrable.</p>
			
			
			<p>Referrer's participation in this Program constitutes their acceptance of the program Terms and Conditions and any changes thereto. &nbsp;Referrers and referrals are responsible for remaining knowledgeable as to any changes <a href="http://www.shoresummerrentals.com/">ShoreSummerRentals.com</a>&nbsp;may make to the program Terms and Conditions.</p>
			
			<p><a href="http://www.shoresummerrentals.com/">ShoreSummerRentals.com</a> &amp; <a href="http://SSRARCAvacationrentals2u.com">VacationRentals2U.com</a> reserves the right to terminate this program at any time and, in its sole discretion, to honor any reward or refuse any referral bonus if <a href="http://www.shoresummerrentals.com/">ShoreSummerRentals.com</a> determines that it was obtained under wrongful or fraudulent circumstances, that inaccurate or incomplete information was provided in creating a listing, or that any term of this Program or <a href="http://www.shoresummerrentals.com/">ShoreSummerRentals.com</a>&nbsp;<a href="terms-of-use.html">Terms of Service </a>have been violated.</p>
			
						
			
		</div>
	</section>
	
	
<?php include("footer.php"); ?>