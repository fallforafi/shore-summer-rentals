<?php include("header.php"); ?>

	
	<section class="spam-warning-page">
		<div class="container">
			<h2>OWNER'S TESTIMONIALS</h2>
			<h3>See What They're Saying About ShoreSummerRentals.com</h3>
			
			<p>ShoreSummerRentals.com has become the destination for those looking to rent out their vacation properties. If you have a home to rent out, advertise it among our U.S. and New Jersey Shore rentals by owner listings. Read below straight from other owners about what makes us so special and how easy it is once you choose ShoreSummerRentals.com.</p>
			
			
			<blockquote>
				<p>"Maria, I wanted to let you know how happy I am with the service I've gotten already (and it's only been a little more than a week!). Raya has been wonderful in responding to my questions, the website looks terrific, and the responses I've been getting have been great (hopefully the people are as good in person as they sound over the phone). You and Raya have made the process very user friendly and seamless. (Now if you could guarantee it will be a sunny summer I'll really be impressed!) Thanks again. I will be happy to recommend your company in the future. All the best!"</p>
				<strong>- Laurie, Listing No. 4871, North Wildwood, NJ</strong>
			</blockquote>
			
			<blockquote>
				<p>I have been extremely happy with the exposure and response we got from the ads we posted on ShoreSummerRentals.com this past summer. My husband and I purchased a triplex in Ocean City in January 2009 and used your site exclusively to advertise it for rentals. The results we have gotten exceeded my expectations and I look forward to doing even better next year. We are looking at a property to purchase in Strathmere, NJ and I was wondering if it could be listed on Shore Summer Rentals under Ocean City or Sea Isle or both, if we were to rent it out. I wouldn't even consider buying a rental property in Strathmere if I couldn't get it listed on Shore Summer Rentals. Thank you!</p>
				<strong>- Amy, Listing No.'s 3546 and 3547, Ocean City, NJ</strong>
			</blockquote>
			
			<blockquote>
				<p>"I am very impressed by Maria's website and have had much success over the years. As I mentioned to her, I have always recommended ShoreSummerRentals.com to my friends and will continue to do so. Her Customer Service Department is A1! Thanks again.</p>
				<strong>- Audrey, Listing No. 4371, Long Beach Island, NJ - Beach Haven Crest</strong>
			</blockquote>
			
			<blockquote>
				<p>"Greetings! Once again, I can't begin to tell you how happy I am with your service. I really think you attract great renters. I just had a small problem on Saturday - my renter broke a glass table top and immediately called me. Apologized and said he would have had it fixed, but they were checking out and he would pay for damages. I have had no problems with anyone who rented through you. I have 3 repeat renters!!! Thanks again!"</p>
				<strong>- John, Listing No. 2741, Wildwood Crest, NJ</strong>
			</blockquote>
			
			<blockquote>
				<p>"Maria, I have just signed on my 4th consecutive winter renter – all via your website. Thanks so much!"</p>
				<strong>- Bob, Listing No. 2520, Wildwood Crest/Diamond Beach, NJ</strong>
			</blockquote>
			
			<blockquote>
				<p>"We had a very good season this summer. Our place was rented from the last week in June through September 4. Most rentals since we owned the place and started advertising with you. Thanks!"</p>
				<strong>- Lee, Listing No. 3248, Wildwood Crest, NJ</strong>
			</blockquote>
			
			<blockquote>
				<p>"Maria, I had a very good year, fully booked from 16 June to 19 Sep, with all but 4 summer guests coming from your website."</p>
				<strong>- Sally, Listing No. 2041, Ocean City, NJ</strong>
			</blockquote>
			
			<blockquote>
				<p>"Hi Maria, I just signed up and can't believe my responses. Not able to put signs on my building and had no exposure to rentals. Now I get inquiries and rentals through your website."</p>
				<strong>- Angela, Listing No.'s 3771, 3772, 3773, North Wildwood, NJ</strong>
			</blockquote>
			
			<blockquote>
				<p>"Thank you Maria for all of your hard work. Your site has become our primary method of renters finding us!! Your site is amazing and I get more business from it than any other site or company ever!! Thank you for creating such an amazing site that has been invaluable in the renting of our property for many years now! Thank you thank you!"</p>
				<strong>- Michele, Listing No. 2343, Ocean City, NJ</strong>
			</blockquote>
			
			<blockquote>
				<p>"Maria, We closed today on the sale of our property. The new owner has agreed to take over the listing on your site. He will be calling you soon to make the change. Thanks for all your help. The site has been a real winner for us."</p>
				<strong>- Bruce, Long Beach Island, New Jersey</strong>
			</blockquote>
			
			<blockquote>
				<p>"Maria: I just have to tell you how happy I am with your service. I have owned my home for the past 20 years. Never having to rent it, I listed it with 2 local realtors. I tried newspaper ads. I was even in a rental advertising co-op with more newspaper ads. None of these methods produced a single day of rentals. A friend suggested I look up your website. I'm a big fan of Google so when I saw you were #1 on Google I thought I'd give your service a try. WOW, I didn't get much play in June but AS SOON AS I LISTED MY FIRST SPECIAL, the phone and e-mails just kept rolling in. I just confirmed my last week in August and now I'm working on September. Thanks for saving my summer."</p>
				<strong>- Mark, Listing No. 3331, Ocean City, New Jersey</strong>
			</blockquote>
			
			<blockquote>
				<p>"Hi Maria: I hope you had a nice vacation. Thanks so much for adding listing number 3389 to your site. The past 4 years, we rented the apartment (and another) to the foreign students who worked on the boardwalk. This year, due to the dollar and the economy, we didn't even fill one of our apartments, so we decided in the MIDDLE of JUNE to rent Apartment B (listing 3389) to families, via weekly rentals. Although the apartment is very nice, it has a small kitchen, so we were always reluctant to advertise it for weekly rentals. We added the apartment to your site on June 15th, and today we rented our last week for the summer! At least 4 of the 8 weeks (and three weekends) were rented via SSR! I can't tell you how delighted I am! We thought we might have to sell, and in this market we would have lost thousands. We are hoping that we will rent the apartment throughout the year, especially with your "last minute specials". Since June 15th, we have had 7,772 visitors to our SSR site as of today! That's over 200 visitors a day! Unbelievable. We would like to add another apartment to your site. We are going to allow pets in this apartment. We will DEFINITELY be renewing all three properties with you at the time of renewal, with the specials package and any other packages you might have. Your site is the very best by far, and we are on many others. Thanks for everything you do."</p>
				<strong>- Sue, Listings 2418 & 3389, Ocean City, New Jersey</strong>
			</blockquote>
			
			<blockquote>
			<p>"Maria, just wanted to pass along that the "specials" package worked great, even though it was not placed until just several days prior to the rental period. Within 24 hours we had the full week booked and turned away probably 10-15 other inquiries! Well worth the relatively small fee for the service."</p>
				<strong>- Rich Henderson, Listing No. 1677, Wildwood Crest, New Jersey</strong>
			</blockquote>
			
			
			<blockquote>
				<p>"Maria, We have already had two rentals from the "Owner's Special" page. People call because they can't quite believe their luck. I am currently in Ocean City and there are plenty of Vacancy and For Rent signs. We consider ourselves fortunate to have some income coming in. We were looking at empty, income-less weeks during high season for the first time in years. Once we listed an Owner's Special on SSR, we got an average of two responses per day which resulted in doubling our income. Thanks, SSR."</p>
				<strong>- Patty, Listing No. 3271, Ocean City, New Jersey</strong>
			</blockquote>
			
			<blockquote>
				<p>"Hi Maria, Thanks to you and your awesome website. I am totally rented for the summer. The stress is gone. I now only need partial or full weeks for September and October."</p>
				<strong>- Jill, Listing No.'s 2601 and 3110, North Wildwood, New Jersey</strong>
			</blockquote>
			
			<blockquote>
				<p>"My Dearest Maria, I cannot believe that we are now beginning our fifth season in The Wildwoods and fifth season with Shore <a href="http://www.shoresummerrentals.com">Summer Rentals.com</a>! This year is truly going to be the best season ever in our 5 years for rentals! We already have over $100k on the books for all of our units. Considering that it is February 29th and we are almost completely SOLD OUT at two of our 7 properties that we list exclusively with Shore <a href="http://www.shoresummerrentals.com">Summer Rentals.com</a> it is truly amazing! Your website brings us 75-80% of our rentals. The other 20% are from repeat tenants and referrals, word of mouth, our signage, and our weekend Rental Open Houses that we have January thru March. My husband Ken and I would like to thank you for the last 5 Seasons and look forward to 5 more!"</p>
				<strong>- Christine, Listing No.'s 1121 & 1803, The Wildwoods</strong>
			</blockquote>
			
				
			<blockquote>
				<p>"Great site, I am booked for the season due to you."</p>
				<strong>- Bob, Listing No. 3008, Lavallette, New Jersey</strong>
			</blockquote>
			
			<blockquote>
				<p>"Dear Maria, Want to thank you for a great website. I rented my home in Manahawkin for the whole summer within 2 weeks of listing it. It was nice to get such quick results in this current market. No way a realtor could have done as well and at a fraction of the price!</p>
				<strong>- Chet, Listing No. 1706, Long Beach Island/Manahawkin, NJ</strong>
			</blockquote>
			
					
			<blockquote>
				<p>"Maria, we are still totally satisfied with your website. We were fully booked from mid June to September 23rd this year. We are turning people down and have 3 weeks rented for next season. Your site is great."</p>
				<strong>- Glenn, Listing No. 1393, Cape May, NJ</strong>
			</blockquote>
			
			<blockquote>
				<p>"Dear Maria, Thanks for your timely and courteous response. We joined your website last year (March time frame) when we returned home from vacationing in Florida. The rental season was not shaping up to be 'all that we had hoped for,' and, we had 'gaps' in our rental forecast for the summer season with no foreseeable way of filling-in some of the 'difficult weeks.' Shortly after we signed-up with Shore Summer Rentals, the phone literally started to ring-off the hook. The weeks quickly filled, and, you helped save the season. I could have easily gotten 20 renters through your website alone. Thanks for your recent guidelines. Being a good 'landlord' is a challenge, at times. Honesty is advertising is always the best policy. You may want to tell New Jersey owners that the security deposit is for actual damage--not excess cleaning because the renters were slobs. One positive observation I have made during the last 2-years--the people we get from your website are much more thoughtful and respectful of our house than the people we get through the local real estate agent in Wildwood Crest."</p>
				<strong>- Ron, Listing No. 2088, Wildwood Crest, NJ</strong>
			</blockquote>
			
		    <blockquote>
				<p>"Hi Maria, This is out first year as owners and we want to let you know how much we love your website. We also listed with a realtor who did not find us one tenant. Through your website, along with some advertising in a local paper, we rented 7 weeks and are still receiving many inquires. Hope to get one or two more weeks. Thanks for posting my Last Minute Special last night, it was booked by 10 am today with four inquiries following. I can't say enough about your website. All of my guests so far have been really great people."</p>
				<strong>- Tina, Listing No. 2754, North Wildwood, NJ</strong>
			</blockquote>
			
			<blockquote>
				<p>"Hi Maria, Just wanted to thank you once again for the wonderful work you do with your website. This year, we have booked up all prime summer weeks with the help of your site. Your creativity with your site makes it user friendly and easy to find via the internet. I am constantly telling others about your website and all have found yours to be superior over some of the others out there. I used to have my properties listed on 2 other sites, but have found that I really get the best results on yours. I hope you're doing well in your new office. One day, when I am in the Ocean City area, I would really enjoy dropping by to meet you!"</p>
				<strong>- Lisa, Listing No.'s 1185, 1511 & 1825, North Wildwood, NJ</strong>
			</blockquote>
			
			<blockquote>
				<p>"Thanks...it worked for me. I had one week open (7/28) two weeks ago and I can't even tell you how many calls I had after posting the special. I ended up breaking up the week to two 1/2 week renters. Got to love last minute specials!!"</p>
				<strong>- Donna, Listing No. 2064, North Wildwood, NJ</strong>
			</blockquote>
			
			<blockquote>
				<p>"Maria, The last minute special you posted for my condo, listing 2553, was very successful! When you get a chance, please remove the special since I was able to rent the last week of August and Labor Day weekend. I want to thank you for all of your help and encouragement during this rental season. I was very nervous during the winter and early spring months when I was not getting interest in my condo. As you said, by May and June I was able to fill most of the weeks. As the summer progressed, I was able to rent my condo every single week! I am so pleased with how everything went this season, and my success is very much a credit to your fabulous website. The website gave my condo more exposure than I had anticipated! I'm already looking forward to next year's rental season. Thanks again for you continuous dedication to helping me rent my condo!"</p>
				<strong>- Kerri, Listing No. 2553, Wildwood Crest, NJ</strong>
			</blockquote>
			
			<blockquote>
				<p>"Thanks for all the updates - thanks for always being the best - we have been so successful with the site!"</p>
				<strong>- Bette, Listing No. 2126, North Wildwood, NJ</strong>
			</blockquote>
			
		    <blockquote>
				<p>"Hi Maria, Just wanted to say thank you for all your great work and help. Your web site is great!!!"</p>
				<strong>- Scott, Listing No. 1830, Spring Lake, NJ</strong>
			</blockquote>
			
		    <blockquote>
				<p>"Hi Maria, My family and I just wanted to thank you for the terrific website that you have created. Two of our homes are listed on your site and I was too busy to create the listing for our condo. After letting too much time pass and realizing that we didn't have many rentals, I finally got around to emailing you about the 3rd listing. You and your staff were kind enough to create the listing for us. When we listed our legacy condo on your site several weeks ago on April 2, we had more than a 50% vacancy rate. We were afraid that it was not going to be fully rented for the summer. After being listed on your site less than 20 days, we are now 90% rented with only 1 week left to rent. Your site is unbelievable and we continue to highly recommend you to our friends. Thank you again for your excellent customer service and for being the #1 rental site for owners! Your new office is only 1 block from one our homes. I am going to stop in and say hi one day! Thanks again!"</p>
				<strong>- Danielle & Family, Listing No.'s 2105, & 1769, Ocean City, NJ</strong>
			</blockquote>
			
			<blockquote>
				<p>"Maria, In addition to listing my house with a realtor, I listed with another FSBO website and although it indicates I am getting hits I have yet to have one email or telephone call; whereas with your site, for every 10 hits or so, I get one or the other. Oh, I gave your website to some folks who didn't have as successful of a rental season as I did. Hope they sign with you."</p>
				<strong>- Sally, Cape May, NJ</strong>
			</blockquote>
			
		    <blockquote>
				<p>"Hi Maria, Please let me take this opportunity to tell you how pleased we are with your site. We have rented all the weeks we wanted with no hassle at all. All our customers were lovely people so far and we couldn't be more pleased. Your customer service is Exceptional and I really appreciate that. I have told many people about you and hope they will be smart and use your site as it is so much better than any of the realtors. Thanks again for a great season!</p>
				<strong>- Carole and Dave, Listing No. 2172, Ocean City, NJ</strong>
			</blockquote>
			
			<blockquote>
				<p>"Maria, Thanks so much and I have to tell you that I have gotten so many inquiries and rentals from your site!! I can't thank you enough for having this site it's made my first year as a "landlord" much more enjoyable then it would have if I was doing this on my own.</p>
				<strong>- Donna, Listing No. 2064, North Wildwood, NJ</strong>
			</blockquote>
			
			<blockquote>
				<p>"Maria, Thanks for the Help! We rented 2 of the 3 weeks in July that were not rented! Your Specials Section is GREAT!"</p>
				<strong>- Ray, Listing No. 1456 & 1181, North Wildwood, NJ</strong>
			</blockquote>
			
			<blockquote>
			<p>"Thought I'd give you some feedback on your site. This year, our first with SSR, we had 8 weeks sold by your site, 4 from private rentals and 2 from VRBO. Realtors can't compete! I will drop the expensive VRBO website next year because of the huge traffic generated by your site. You're doing great!"</p>
				<strong>- Jim, Listing No. 1294, Ocean City, NJ</strong>
			</blockquote>
			
			<blockquote>
				<p>"I just wanted to let you know we've had another great season with Shore Summer Rentals website. Since signing up with Shore Summer Rentals we have had the past two summers totally booked. I can't begin to tell you how many people we've had to turn away being our property was booked up so early in the season. Inquiries are still coming in!!! Our listing has had over 7,000 hits in a year's time. Best of all is the savings in real estate commissions. Keep up the good work.</p>
				<strong>- Judy, Listing No. 1105 Point Pleasant, NJ</strong>
			</blockquote>
			
		    <blockquote>
				<p>"I just wanted to let you know how pleased I am with the people who saw my property on your site and rented from me. Two of them re-rented for next year and I'm happy to have them. I am also starting to already get some inquiries about next year, so that can only be good. We have had wonderful success with our condo between your site and realtors, we are very happy since we have been booked from 6/18 straight through to Labor Day! Also, we've left the sign up the whole time so it's good advertisement for both of us. Again, thanks and we will definitely be renewing in January." </p>
				<strong>- Dennis & Dianne, Listing No. 1221, North Wildwood</strong>
			</blockquote>
			
		    <blockquote>
				<p>"Hi Maria, Still thrilled over the response we get from our SSR listing. 11 weeks rented for Summer of 2009 already with only 2 being from traditional agents! Very good stuff!!! Thanks as always..."</p>
				<strong>- Frank, Listing No. 2410, Ocean City, New Jersey</strong>
			</blockquote>
			
			<blockquote>
				<p>"Hi Maria, I have listing #1981 and I'm in Long Beach Island and usually have just a few rentals each year. This year was sooo different, I could have rented out each week 3 times over! I just wanted to thank you, I had wonderful renters and repeats for next year. In this economy, it really helped and now not stressed out about mortgage. Great job - the site has saved me!" </p>
				<strong>- Lisa, Listing No. 1981, Long Beach Island, Brant Beach, NJ</strong>
			</blockquote>
			
			<blockquote>
				<p>"Thank you Maria, A note of good cheer - I got all of my summer rentals from your website! How great is that? Thanks."</p>
				<strong>- Susan, Listing No. 4146, Wildwood, NJ</strong>
			</blockquote>
			
			<blockquote>
				<p>"Hi Maria, We are so pleased with all the rentals we have gotten from your website. We didn't expect so much success. Thank you so much."</p>
				<strong>- Peg, Listing No. 2066, Ocean City, NJ</strong>
			</blockquote>
			
			<blockquote>
				<p>"Hi Maria, The pictures are fantastic. Putting our listing on your website was the smartest thing I've ever done. I've gotten an incredible number of hits and have already started securing rentals. Thank you again."</p>
				<strong>- Todd, Listing No. 2071, Seaside Heights, NJ</strong>
			</blockquote>
			
			<blockquote>
				<p>"Hi Maria, We love your site. We've owned for 14 years now but this has been our BEST rental season EVER for us. I look forward to continuing our membership and continuing to refer others to you."</p>
				<strong>- Claire, Listing No. 1764, Ocean City, NJ</strong>
			</blockquote>
			
			<blockquote>
				<p>"Maria, We have been extremely pleased with the results and we've enjoyed working with you!!"</p>
				<strong>- Barbara, Listing 1924, Ortley Beach, NJ</strong>
			</blockquote>
			
			<blockquote>
				<p>"Maria, I just wanted to also let you know that we have had another very successful season thanks to your efforts. We were completely booked the entire summer and into September. Thank you for your great service."</p>
				<strong>- Gary, Listing No.'s 3295 and 2889, Cape May/Townbank, NJ Are</strong>
			</blockquote>
			
					
			<blockquote>
				<p>"Hi Maria, Thanks for a great rental season. All in all, it went really well; we had renters for almost all of July and August. All of the people loved the house. We just wanted to thank you for the wonderful service you provide. I tell everyone about it."</p>
				<strong>- Vidette & Mike, Listing No. 2489, Cape May, NJ</strong>
			</blockquote>
			
			<blockquote>
				<p>"Hi Maria, Just wanted to drop a note to say thank you for making this 2009 rental season a success. Within one week of signing up with Shore <a href="http://www.shoresummerrentals.com">Summer Rentals.com</a> we had 5 weeks rented. Your site was very well thought-out and extremely user-friendly. What a great idea!!! Thanks again."</p>
				<strong>- Lisa, Listing No. 3722, Ocean City, NJ</strong>
			</blockquote>
			
			<blockquote>
				<p>"Maria, We were fully rented for the year thanks to you."</p>
				<strong>- Gerry, Listing No. 1177, Ocean City, NJ</strong>
			</blockquote>
			
		    <blockquote>
				<p>"Maria, I have been working the "Last Minute Specials" section of the website all summer. Every one of my rentals (except 2) came from your website this year. Since the house is still for sale I just started to rent July 2nd. My next door neighbor got me my first 2 rentals. After that it's all your doing. I have been doing all partial week rentals. Although it's exhausting--- I am not loosing too many nightly rentals. I have tried to sandwich in the rentals to match per floor so it's not too much back and forth, because I still check in and check out my guests. I do my own cleaning as well. I have changed specials to suit my needs. The price is always the same. ($199 per night) 3 night minimum and hardly a day goes by without a hit. I just checked my count of how many views there were over 2000 on each floor (4000). That's amazing. I am thankful to this website because I have more control over our property. Just thought you would like to know how good your website is."</p>
				<strong>- Anna, Listings No. 3419 & 3420</strong>
			</blockquote>
			
		    <blockquote>
				<p>"Hi Maria, While many take the time to complain, rarely do we spend a few moments to let others know of a "job well done." I want to say that I am very appreciative of your service and your site. I think you personally go out of your way to assure the site is helpful and useful to renters and owners. I am especially thankful for your responsiveness to my questions about how I can use your service to it's fullest. I cannot think of a time when you did not send me a response within 24 hours. It also seems that you keep abreast of the latest information to help us rent our properties and avoid potential scams. The site is improved constantly and updated daily. Thanks a bunch for all your help and effort!"</p>
				<strong>- Sally, Listing No. 1384, Cape May, New Jerse</strong>
			</blockquote>
			
			<blockquote>
				<p>"Maria, I have in fact rented the house for the year. Your site immediately produced inquiries for a winter lease and 3 for a yearly lease. Ideally, I wanted a winter, followed by a weekly or monthly in the summer, in order to maximize income. As you might guess, the calls for all the other scenarios poured in immediately after, including quite a few for weeks in June and July. Anyway, I'm covered for the year. My next door neighbors, who are both realtors in Brigantine told me how amazed they are that I always have renters, and they couldn't figure out how I did it. They have a few personal properties in the area that have been vacant. I gave them your information, so hopefully they will get in touch. Very cool if two active realtors use your services to rent out their own properties. By the way, I also placed ads in the Press, and a couple of local papers. There were no responses, and they were relatively expensive, so thanks again for your site."</p>
				<strong>- Ken, Listing No. 1033, Brigantine, New Jersey</strong>
			</blockquote>
			
			<blockquote>
				<p>"Maria, I want to thank you for having such a great website. After initially listing with realtors and getting just a few bookings, a friend told me about ShoreSummerRentals.com. Our house has been fully rented since we listed with you and have had to turn away many other people interested in renting our house. Not only did we get prime time rentals, we also got rentals in May and in the fall. I also have weeks already booked for the summer of next year. I also want to commend you on your professionalism with your prompt responses to any inquiries I have made. You're a class act!!"</p>
				<strong>- Lynn, Listing No. 2665, Ortley Beach, New Jersey</strong>
			</blockquote>
			
			<blockquote>
				<p>"Hi Maria, Thanks so much for this website. It saves me a lot of time when trying to rent my property. I even signed up late this year (March or April) and still had my property fully rented within a month or so. I could have rented 2 properties (if I had another one) with the volume of calls that I received."</p>
				<strong>- Michele, Listing No. 3141, Ortley Beach, New Jersey</strong>
			</blockquote>
			
			<blockquote>
				<p>"Maria, We entered a special for unit #1314 and had 10 calls in a week and rented the property. Please remove our special. Thanks!!"</p>
				<strong>- Ron, Listing No. 1314, Ocean City, New Jersey</strong>
			</blockquote>
			
			<blockquote>
				<p>"Maria and Chris, This summer started out like we weren't going have to any rentals due to my stupidity and then I got up and running. Still running. Rented in September and of course Irish Weekend. Your " Specials " are the bomb. Still getting last minute calls. Thank you both for being so nice to us over the past years. Hope you had a terrific summer."</p>
				<strong>- Dan and Carol, Listing No. 1746, Wildwood, New Jersey</strong>
			</blockquote>
			
			<blockquote>
				<p>"Hey Maria - Just wanted to let you know that I rented all my split weeks! We had so many responses and had to turn many vacationers away. Thanks!"</p>
				<strong>- Linda, Listing No. 2687, Wildwood Crest, New Jersey</strong>
			</blockquote>
			
			<blockquote>
				<p>"Maria, I wanted to share my great success story from using your website. In the past years we always rented via word of mouth and for the most part always had a week or two open. This year I took over fully managing my brother's Lavallette property and had a record setting year under these difficult and challenging times. I booked the entire season and at a much higher rate than we have in the past. I also just rented the week of Labor Day, which has never been rented before and we have owned this house for 40 years. If I compare the bookings from your site vs. the big national competitors, there is no comparison. I most likely will not be using their site again. Thank you for such a great service, I have shared your website with several other people. Thank you!"</p>
				<strong>- Chris, Listing No. 3284, Lavallette, New Jersey</strong>
			</blockquote>
			
			
			<blockquote>
				<p>"Maria, Once again your idea of changing my main picture, adding more pictures, and going with the specials package worked! In less than 5 days I booked my remaining partial weeks. GREAT JOB! Thank you. You're the best!!"</p>
				<strong>- Terri, Listing No. 3216, North Wildwood, New Jersey</strong>
			</blockquote>
			
			<blockquote>
				<p>"I just wanted to express how impressed I was with the website. I rented my cottage within 48 hours of listing my property on your site. My goals for the property were obtained through your site. Thanks, I will definitely be advertising next season."</p>
				<strong>- Joann, Listing No. 3241, Point Pleasant Beach, New Jersey</strong>
			</blockquote>
			
			<blockquote>
				<p>"Maria, AS ALWAYS - THANKS SO MUCH FOR OFFERING YOUR SITE! We rented 11 full weeks and 4 partial weeks. We rented June through August and 3 long weekends in September. We also rented one long weekend in October because of your site. Thanks for all of your hard work."</p>
				<strong>- Pam, Listing No. 1558 Wildwood (Crest Border), New Jersey</strong>
			</blockquote>
			
			<blockquote>
				<p>"Maria, Thank you so much for your help and guidance. After talking with you I added pictures and changed my ad (following your suggestions) and in one day I sold 3 weeks which is incredible. Thank you so much."</p>
				<strong>-Mary, Listing No. 2296, Sea Isle City, New Jersey</strong>
			</blockquote>
			
			<blockquote>
				<p>"Thank you. This has been my best season since I've owned the house (3 yrs)."</p>
				<strong>- Barbara, Listing No. 2916, Cape May, New Jersey</strong>
			</blockquote>
			
			<blockquote>
				<p>"Thanks for your help again!!! I also want to thank you for being so responsive and helpful. We have booked from Memorial Day Weekend and then the middle of June through the end of Aug. in a short period of time. Thanks again."</p>
				<strong>- Vince, Listing No. 3275, Wildwood Crest, New Jersey</strong>
			</blockquote>
			
			<blockquote>
				<p>"Hi Maria, It's only January and we're already fully rented for the summer season. Discovering your website has been very good for us. We don't have to advertise anywhere else. Whatever you are doing, keep it up."</p>
				<strong>- Loretta, Listing No. 2904, Ventnor, NJ</strong>
			</blockquote>
			
			<blockquote>
				<p>"Hi Maria, I just have to make a comment....you are one of the least expensive sites I've seen. Heck the realtors take 12% of each rental, if we had to rely on them instead of you we would all be losing a more money then the little fee your site charges. Maybe it's because I'm in the marketing business and I understand the cost involved in any programming changes to a website, but you provide a valuable service at a reasonable cost. I have yet to find a site better than yours. Anyway, just thought I'd you should hear that some of us truly understand and appreciate all your hard work."</p>
				<strong>- Donna, Listing No. 2062, Seaside Heights, NJ</strong>
			</blockquote>
			
			<blockquote>
				<p>"Maria & Chris, I wanted to let you know how grateful I am of your services. I was skeptical at first, being a Pennsylvania Licensed Realtor, that your website would work. How wrong was I! We are listed with several rental agencies and they didn't have a chance to show our vacation home before your website gave me a backlog of renters looking to rent our home. We are booked beyond our expectations and are taking reservations for next year now! With this success, I might consider buying more investment properties."</p>
				<strong>- Kevin, Listing No. 2631, North Wildwood, NJ</strong>
			</blockquote>
			
			<blockquote>
				<p>"Maria, Your site has been our only source of rentals and has far exceeded expectations."</p>
				<strong>- Don, Listing No. 1481, Ocean City, NJ</strong>
			</blockquote>
			
			<blockquote>
				<p>"Hi Maria, Just wanted to say again how great Shore Summer Rentals has been for us! We have, as of today, only 2 weeks left open and 1 of them is for the first week of September. We decided to rent to under 25 and with that raised our security to $1,000.00, asked for all of the parents signatures, and have informed them of our "no tolerance" rules. We had an absolute flurry of bookings. The entire month of May is booked as well! We would never have been able to book this quickly, efficiently (or cheaply) with a realtor. I really feel sorry when I see nice properties near ours with "For Rent" signs out front and no signs of activity. This is our 3rd year and we've really got a great system down."</p>
				<strong>- Barbara and Bob, Listing No. 1702, Wildwood, NJ</strong>
			</blockquote>
			
			<blockquote>
			<p>"Maria, Thanks for posting my Last Minute Special. You can remove it now as it has done it's job! Thanks so much for your proactive response to our rental problem, you responded like a champ! We received over 2,000 hits in June and had 5 rentals! You will always be a part of our marketing strategy!"</p>
				<strong>- Jim, Listing No. 1294, Ocean City, NJ</strong>
			</blockquote>
			
			
			<blockquote>
				<p>"The website is WONDERFUL! ShoreSummerRentals.com has gotten me most of my rentals this year and I truly appreciate it. Your website must see ALOT of traffic. Keep up the good work and we will be back next year."</p>
				<strong>- Joe and Keri, Listing No. 2794, Dewey Beach, DE</strong> 
			</blockquote>
	
			<blockquote>
				<p>"Hello Maria, I want you to know your website was referred to me by a friend who used it to rent her Strathmere property. Last year, I had a few weeks rented. This year, I rented 7 weeks and so far, my renters have been really great people looking for a nice clean, neat place to stay. I am sure next year, I will sell out, but I've kept the other 3 weeks for myself and family! I try to go the "extra mile" for my guests, and your website has enabled me to offer loads of information and photos without a lot of hassle, which allows me to spend more time on the creative end so guests enjoy their stay even more. Thanks for a great website!"</p>
				<strong>- Susan, Listing No. 2199, North Wildwood, NJ</strong> 
			</blockquote>
			
			<blockquote>
				<p>"Hi Maria, I'm full for the rest of the season. Thanks to your website."</p>
				<strong>- Kevin, Listing No. 1937, North Wildwood, NJ</strong> 
			</blockquote>
			
			<blockquote>
				<p>"Hi Maria, It's been a good summer and June through August were our most active months for rental inquiries of our shore vacation condo. Being a new owner, it was our first time renting. We had booked the month of August and hopefully look forward to the return of those families next season. With your help, and that of the staff from ShoreSummerRentals.com., the first time rental experience was less traumatic then I thought it would be for a new owner. You have provided all the tools that were necessary in renting and walk us through the process. We look forward to the new changes." </p>
				<strong>- Vito, Listing No. 2506, Seaside Heights, NJ</strong> 
			</blockquote>
			
			<blockquote>
				<p>"Hi Maria, I want to thank you for helping me with our rentals. We went from one week to five weeks with your suggestions and help and we were lucky to attract young families who took care of the condo, so it was worth it."</p>
				<strong>- Belinda, Listing No. 2176, Wildwood, NJ</strong> 
			</blockquote>
			
			<blockquote>
				<p>"Hello Maria! Greg and I are sending you this email to let you know that Adam and Phyllis (Listing #2112) told us to use shoresummerrentals.com to list our home in Brigantine if we really want to get it rented this year. We want you to know that they were absolutely right! This is our first year trying to rent our home online in Brigantine. Last year we used a sign outside and a real estate agent and did not rent one week. This year we have had great success renting our home because of your website. We will use you for all of our future listing needs. Greg and I can't say enough good things about your website. We will tell everyone who is considering a website to list their rental property to use <a href="http://www.shoresummerrentals.com">www.shoresummerrentals.com</a>. Thanks again for your support and help with our rental needs."</p>
				<strong>- Ellen and Greg G., Listing 2572, Brigantine, NJ</strong> 
			</blockquote>
			
			<blockquote>
				<p>"Maria, You have a wonderful site and we are thrilled to have been so successful with the help of your website. I have enrolled in others in the past and yours is by far the best. I appreciate the constant updating and care you put into your site."</p>
				<strong>- Jennie, Listing No. 1671, Wildwood Crest, NJ</strong> 
			</blockquote>
			
			<blockquote>
				<p>"Maria, it is 3/15 and once again, your site has been wonderful in getting our whole summer booked - except for two weeks, which I'm sure will be rented soon. Thanks again for everything."</p>
				<strong>- Pam, Listing No. 1558, Wildwood Crest, NJ</strong> 
			</blockquote>
			
			<blockquote>
				<p>"Getting great leads Maria! Impressed! Thank you!"</p>
				<strong>- Kristen, Listing No.'s 2461, 2462, 2463, Bethany Beach, DE</strong> 
			</blockquote>
			
			<blockquote>
				<p>"Thanks Maria! We happy to say we're rented for the summer. Thanks again."</p>
				<strong>- Donna, Listing No. 2074, Wildwood, NJ</strong> 
			</blockquote>
			
			
			<blockquote>
				<p>"Maria, You are doing a great job - keep the renters coming..."</p>
				<strong>- Paul, Listing No. 2049, Cape May, NJ</strong> 
			</blockquote>
			
			<blockquote>
				<p>"Hi Maria, Well yesterday my first prospective renters showed up at my door in Seaside Heights. After speaking with them and going over the rules and regulations, they decided to rent my unit for the Memorial Day weekend. Today, I filled out the Rental Agreement and Thank You Letter that was provided and mailed them off to my renters. The information you provided was so easy, helpful and professional. I am looking forward to many more renters because of your service."</p>
				<strong>- Joan, Listing No. 2423, Seaside Heights, NJ</strong> 
			</blockquote>
			
			<blockquote>
				<p>"WOW! You guys have been busy. Thanks for making vacation rental advertising SOOOO easy for us as Homeowners. You are the best!"</p>
				<strong>- Karen, Listing No. 1516, North Wildwood, NJ</strong> 
			</blockquote>
			
			<blockquote>
				<p>"Maria, I wanted to let you know how pleased we are with the response to our Shore Summer Rentals ad. We listed our home with a realtor and on ShoreSummerRentals.com. The realtor found us rentals for 2 weeks and the ad resulted in a rental for the entire month of August. Just wanted to say thanks for a great site."</p>
				<strong>- Loretta, Listing No. 2904, Ventnor, NJ</strong> 
			</blockquote>
			
			<blockquote>
				<p>"Dear Maria, I want to thank you so very much for giving me the ability to advertise my property on your site... I have recommended you to many people here in South Jersey. I have had wonderful tenants this summer, your site produces some great people and I have also secured a wonderful winter rental. The class of tenants that your site attracts are wonderful. Thank you so much and I will update my ad for next summer."</p>
				<strong>- Debbie, Listing No. 2954, Sea Village Marina Houseboat</strong> 
			</blockquote>
			
			<blockquote>
				<p>"Maria, I imagine the majority of emails you receive deal with complaints, problems or questions. This one is to compliment you on the constant improvements you make to the site. Since the brokers no longer do much of anything with rentals, I have found the site to be invaluable for renting my property. It is without a doubt the best investment I ever made. Keep up the good work.</p>
				<strong>- Bruce, Listing No. 1946, Long Beach Island, NJ</strong> 
			</blockquote>
			
			<blockquote>
				<p>"Thanks Maria, We have had such a great experience, we're looking at another property to buy. We LOVE your site."</p>
				<strong>- Gary, Listing No. 2889 and 2929, Cape May/Townbank</strong> 
			</blockquote>
			
			<blockquote>
				<p>"Maria and Chris, This note is to let you know just how well the power of the internet can be used to get astonishing results in a relatively short period of time. We listed our new Cape May condo (Listing #1899) on your website in January. Being first-time shore property owners with a huge mortgage payment, we were concerned that the management company wasn't advertising or promoting the new complex enough. We knew we had to find another way to get our property rented. That is when we came to you. Now, about 9,000 hits later, our new condo has caused us much less worry and a great feeling of success. While the complex has rented at a rate of 60 to 75% in the period from June 12th through September 30th, our condo has rented 103 of those 111 days so far (with a great chance at renting the seven open days later this summer), a possible 99 percent rental rate over an extended sixteen-week summer! Of course, we have done a little work using your website to post our pictures and promote our property, etc., but it has been well worth it as my wife and I look ahead to next season for even more success. We appreciate your advice and prompt technical support whenever called upon. If there was a shadow of a doubt about using ShoreSummerRentals.com back in winter, we can tell you now that the relatively small amount invested has repaid us many times over in our new venture as seashore landlords. Coincidentally, we first used your website a couple of seasons ago as renters, finding a beautiful Ocean City condo for our summer family vacation. We couldn't imagine being without your website and the interest it has generated. We highly recommend ShoreSummerRentals.com to anyone wanting to increase their rental success rate, while saving on expensive commissions."</p>
				<strong>- Steve and Gayle, Listing No. 1899, Cape May, NJ</strong> 
			</blockquote>
			
			<blockquote>
				<p>"Hi Maria, I would like to list my Stone Harbor property on your site. I currently have a listing in Wildwood Crest listed through your site and have had a great response. I am anxious to get this up and running so at your earliest convenience, please let me know."</p>
				<strong>- Kara, Listing No.'s 2444, Diamond Beach/Wildwood Crest, NJ</strong> 
			</blockquote>
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
	
		</div>
	</section>
	
	
<?php include("footer.php"); ?>