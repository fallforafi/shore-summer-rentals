<?php include("header.php"); ?>

	
	
	<section class="inner-page listing-instructions-page">
		<div class="container">
			
			<div class="listing__lft col-sm-9">
				<h3>Pet Friendly Summer and Winter Rentals in Brigantine, 2 Bedrooms, 1 Bathroom, Sleeps 6</h3>

				<address itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress">
					<strong>Property Address:</strong>
						<span itemprop="streetAddress">4619 Harbour Beach Blvd.</span>
							<span>Brigantine, New Jersey, 08203</span>
							<meta itemprop="addressCountry" content="USA">
							<meta itemprop="addressRegion" content="NJ">
                            <meta itemprop="addressLocality" content="Brigantine">
                            <meta itemprop="postalCode" content="08203">
							| <strong>Rental ID:</strong> #4693                        
                </address>
				
				<ul class="nav nav-pills nav-detail scrolink">
                        <li><a href="#overview">Overview</a></li>
                        <li><a href="#location">Map</a></li>
                        <li><a href="#availability">View Calendar</a></li>
                        <li><a href="#rates">Rates</a></li>
                        <li><a href="#photos">Photos</a></li>
                        <li "><a data-toggle="modal" data-target="#emailModal" class="bg-orange emailonwer-btn">Email</a>
				        </li>
                </ul>
				
				
				
				<div class="clearfix"></div>
				
				
				<div class="listing__gallery-area thumb-indicat text-center">
				
				
						<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
						  <!-- Indicators -->
						  <ol class="carousel-indicators">
							<li data-target="#carousel-example-generic" data-slide-to="0" class="active">
								<img src="images/5199_photo_20530.jpg" alt="..."></li>
							<li data-target="#carousel-example-generic" data-slide-to="1">
								<img src="images/5199_photo_20558.jpg" alt="...">
							</li>
							<li data-target="#carousel-example-generic" data-slide-to="2">
								<img src="images/5199_photo_20560.jpg" alt="...">
							</li>
						  </ol>

						  <!-- Wrapper for slides -->
						  <div class="carousel-inner">
							
							<div class="item active">
							  <img src="images/5199_photo_20530.jpg" alt="...">
							</div>
							
							<div class="item">
							  <img src="images/5199_photo_20558.jpg" alt="...">
							</div>
							
							<div class="item">
							  <img src="images/5199_photo_20560.jpg" alt="...">
							</div>
							
						  </div>

						  <!-- Controls -->
						  <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
							<span class="glyphicon glyphicon-chevron-left fa fa-angle-left black"></span>
						  </a>
						  <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
							<span class="glyphicon glyphicon-chevron-right fa fa-angle-right black"></span>
						  </a>
						</div>


				</div>
				
				
				
			<div id="overview" class="geninfo-area">
						<h3>General Information 
								<span class="general-icons pull-right small ">
									<a href="http://www.weather.com/weather/today/08203" target="_blank">
									<img src="images/weather.png" alt=""> Weather </a>
                                </span>
								
                                <span class="general-icons pull-right small mr10">
								<img src="images/wheelchair.png" alt=""/>
								Wheelchair Access</span></h3>

								<address class="bg-silver p10" itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress">
									<strong>Property Address:</strong>
									<span itemprop="streetAddress">4619 Harbour Beach Blvd.</span>
									<span>Brigantine, New Jersey, 08203</span>
								</address>

						<div class="rooms-dtl col-sm-12 list-col-3 mb30">
						  <h6><strong>Rental ID:</strong> #4693</h6>
						  <ul class="amenities">
							<li><strong>Bedrooms:</strong> 2</li>
							<li><strong>Sleeps:</strong> 6</li>
							<li><strong>Pool:</strong> No</li>
							<li><strong>Bathrooms:</strong> 1</li>
							<li><strong>Type:</strong> Single Family</li>
							<li><strong>Pets:</strong> Yes</li>
							<li><strong>Linens:</strong> No</li>
							<li><strong>Outside Shower:</strong> Yes</li>
							<li><strong>Internet:</strong> Yes</li>
							<li><strong>Boat Slip:</strong> No</li>
							<li><strong>Elevator:</strong> No</li>
							<li><strong>Parking Spaces:</strong> No</li>
							<li><strong>Grill:</strong> No</li>
							<li><strong>Wheelchair Access:</strong> Yes</li>
							<li><strong>Distance to Beach:</strong> 2 blocks</li>
						  </ul>
						</div>
  
  

  <h4>Property Overview</h4>
  <p>I am a private owner of beautiful duplexes located two blocks to the beach. I own multiple properties. I am not represented by any Real Estate Company. This is a family owned and operated beach rental business licensed to rent by The City of Brigantine.
  We started this rental business back in May of 2002. We found that renters were having difficulty finding landlords that would allow them to bring their pets along with them on vacation, however, I believe that pets are a part of the family and should
  not be left behind. So we began this pet friendly rental business to provide a service that was difficult to find on the island of Brigantine.
  <br>
  <br> All of my units are designed specifically to provide you with the best vacation experience you ever had. Each unit comes with a private fenced in backyard not shared with any other tenants, so that your pet can also experience a wonderful stay. Any
  pet would rather be with you on vacation rather than at a public boarding facility. Each unit is equipped with Comcast Blast Package high speed internet access (hard-line and wireless). Feel free to bring your computer along with you. Our pledge as
  owners is to offer a clean, comfortable environment for you, your family and your pet without the extra costs of a realtor.
  <br>
  <br> We DO NOT rent to minors, tolerate heavy alcoholic drinking, college parties, bachelor parties, graduation parties, proms, after proms or rent to anyone having these intentions. If you make a reservation and attempt to do any of the above, prepare
  for an immediate eviction, loss of money and legal repercussions. Please do not perceive this notice to be a threat of any kind, it is a promise.
  <br>
  <br> This is a family environment and I will personally make certain that everyone s stay is enjoyable. Please take a moment to scroll down to Additional Links and check out my personal web site by clicking the link below to view other properties I own,
  more specific details, exact pricing, special discounts and availability. Once you view my personal web site, all of your questions will be answered. Very important !
  <br>
  <br> Monthly Winter Rentals and weekends are available for rent from February 1, 2016 to May 15, 2016. Weekend rates vary. Contact me for specific information. Monthly Winter Rentals beginning on February 1, 2016 to May 15, 2016 are 1000.00 per month includes
  water, sewer, internet access and cable tv. Rate is based on number of occupants, pets, duration of stay, choice of unit, credit and or advanced payments. Are you buying a house? In the middle of a settlement? Having a house built or raised? If so,
  you came to the right place ! I have everything you need, just bring your suitcase. Ride along with me until your new home is ready for move in. Monthly Winter Rentals end on May 15, 2016 by 11:00AM. Contact me for specific details.</p>
  
  
  <h4>Amenities</h4>
  <div class="bg-silver col-sm-12 p10 list-col-3">
    <h6><strong>Amenities:</strong></h6>
    <ul class="amenities">
      <li>Alarm Clock </li>
      <li>Ceiling Fan </li>
      <li>Coffee Maker </li>
      <li>Cookware </li>
      <li>Cooking Range </li>
      <li>Deck Furniture </li>
      <li>Dishwasher </li>
      <li>Dishes </li>
      <li>DVD Player </li>
      <li>Internet </li>
      <li>Iron and Board </li>
      <li>Microwave </li>
      <li>Outside Shower </li>
      <li>Oven </li>
      <li>Refrigerator/Freezer </li>
      <li>Sofa Bed </li>
      <li>Television </li>
      <li>Toaster </li>
      <li>Utensils </li>
      <li>Vacuum </li>
      <li>VCR </li>
      <li>Washer/Dryer </li>
      <li>WiFi </li>
    </ul>
  </div>
  
  <div class="clearfix"></div>
  
  <div class="bg-white col-sm-12 p10 list-col-3">
    <h6><strong>Bedding Sizes:</strong> 1 Queen Bed, 2 Full Beds, 1 Twin Bed, 1 Sofa Bed</h6>
  </div>
  
  <div class="bg-silver col-sm-12 p10 list-col-3">
    <h6><strong>Features:</strong></h6>
    <ul class="amenities">
      <li>
        Air Conditioning </li>
      <li>
        Balcony </li>
      <li>
        Cable/Satellite TV </li>
      <li>
        Deck </li>
      <li>
        Dining Room </li>
      <li>
        Full Kitchen </li>
      <li>
        Wheelchair Access </li>
      <li>
        Heated </li>
      <li>
        Internet Access </li>
      <li>
        Living Room </li>
      <li>
        Patio </li>
      <li>
        Pets Allowed </li>
    </ul>
  </div>
  
  
  <div class="bg-white col-sm-12 p20 list-col-3">
    <h6><strong>Activities:</strong></h6>
    <ul class="amenities">
      <li>
        Beachcombing </li>
      <li>
        Bicycling </li>
      <li>
        Bike Rentals </li>
      <li>
        Boat Rentals </li>
      <li>
        Boating </li>
      <li>
        Churches </li>
      <li>
        Cinemas </li>
      <li>
        Deepsea Fishing </li>
      <li>
        Fishing </li>
      <li>
        Fitness Center </li>
      <li>
        Golf </li>
      <li>
        Jet Skiing </li>
      <li>
        Kayaking </li>
      <li>
        Marina </li>
      <li>
        Miniature Golf </li>
      <li>
        Playground </li>
      <li>
        Restaurants </li>
      <li>
        Shopping </li>
      <li>
        Sightseeing </li>
      <li>
        Bay Fishing </li>
      <li>
        Surf Fishing </li>
      <li>
        Surfing </li>
      <li>
        Swimming </li>
      <li>
        Walking </li>
    </ul>
  </div>

  
  
  <div class="pro-loc" id="location">
	<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d10429.216621497719!2d-74.80691114145769!3d39.00106203367174!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c0a869eccbe7d5%3A0x07185a0a5d2693a0!2sNorth+Wildwood%2C+NJ+08260%2C+USA!5e0!3m2!1sen!2s!4v1457569593106" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
  </div>
  
  
  <div class="check-availability" id="availability">
	<h3>Check Availability</h3>
	
	<div class="calender-plugin">
		<img src="images/calender-plugin.jpg" alt="" />
	</div>
	
  </div>
  
  <hr class="dvr" />
  
  <div class="rates-cont" id="rates">  
	<h3>Rates</h3>
	<p class="bg-silver p10">Payment Types Accepted: Money Order, Cashiers Check, Personal Check</p>
	
	<p>Notes</p>
	
  </div>
  
  <hr class="dvr" />
  
  <div class="listing__photos" id="photos">
	
	<div class="photo-box col-sm-6">
		<div class="photo__inr">
			<figure>&nbsp;</figure>
			<div class="photo__img">
				<img src="images/3267_photo_2571.jpg" />
			</div>
		</div>
	</div>
	
	
	<div class="photo-box col-sm-6">
		<div class="photo__inr">
			<figure>Kitchen</figure>
			<div class="photo__img">
				<img src="images/3267_photo_2573.jpg" />
			</div>
		</div>
	</div>
	
	
	
	<div class="photo-box col-sm-6">
		<div class="photo__inr">
			<figure>Living Area</figure>
			<div class="photo__img">
				<img src="images/3267_photo_2573.jpg" />
			</div>
		</div>
	</div>
	
	
	<div class="photo-box col-sm-6">
		<div class="photo__inr">
			<figure>Master Bedroom</figure>
			<div class="photo__img">
				<img src="images/3267_photo_2577.jpg" />
			</div>
		</div>
	</div>
	
	<div class="photo-box col-sm-6">
		<div class="photo__inr">
			<figure>Dining Area</figure>
			<div class="photo__img">
				<img src="images/3267_photo_2579.jpg" />
			</div>
		</div>
	</div>
	
	
  </div>
  
  
  
  <div class="owner-info-area col-sm-12 p0" id="owner">
	<h3>Owner Info</h3>
	<p>Owner Name: Larry DeLutiis</p>
	
	<div class="btns-group mb20 mt20">
			<a href="#" data-toggle="modal" data-target="#emailModal" class="btn-lg white bg-orange emailonwer-btn">Email Owner</a>
			<a class="btn-lg black bg-silver phone-btn"><i class="fa fa-phone"></i> <span data-hover="(610) 291-0899" class="def">Owner Phone Number</span></a>
			<a href="#" class="btn-lg white bg-blue seeall-btn">Click to See All of My Rentals</a>
	</div>
	
	<p>Rental ID: #1500</p>
	<p>There have been 3529 visitors to this page since the counter was last reset in 2015.</p>
	<p>This listing was first published here in 2015.</p>
	<p>Date last modified - 06/30/2015</p>
	
  </div>
  
  
  
</div>

			
			</div>
		
			<div class="listing__rgt col-sm-3">
			
			
				<div class="detailBox">
					<h4>$1750.00 - $1950.00</h4>
					<span class="perweek">per week</span>
						<hr class="dvr">
					<p>3 Bedrooms / 2 Baths / Sleeps 8</p>
					<hr class="dvr">
					<p>Check in: 2pm<br>Check out: 10am</p>
				</div>

				<div class="scrolink">
					<a class="btn btn-lg btn-primary btn-block" href="#availability">View Calendar</a>
					<a href="#owner" class="btn  btn-lg btn-block white bg-orange">Owner Info</a>
				</div>
				
				<div class="rate">
				
				<div class="rate-stars col-sm-12 p0">
					<a  href="http://www.shoresummerrentals.com/popup/popup.php?pop_type=reviewformpopup&amp;item_type=listing&amp;item_id=1500" class="iframe fancy_window_review star-rating">
						<div class="stars-rating ">
							<div class="rate-0"></div>
						</div>
					</a>
				</div>
				
				<p><a rel="nofollow" href="http://www.shoresummerrentals.com/popup/popup.php?pop_type=reviewformpopup&amp;item_type=listing&amp;item_id=1500" class="iframe fancy_window_review">Be the first to review this property!</a></p>
				</div>
				
				
				<a class="btn btn-lg bg-silver btn-block black" ><i class="fa fa-phone"></i> <span class="def" data-hover="(610) 291-0899" class="def">Owner Phone Number</span></a>
				
				<a data-toggle="modal" data-target="#emailFriendModal" class="btn btn-lg bg-silver btn-block emailtofriend-btn black">@ Email to Friend</a>
				
					
				<div class="checker">
					<p id="dates_available">Your dates are <strong>Available!</strong></p>
					
					
                    <div class="form-group col-sm-6 pl0">
                      <input type="text" id="dpd1" value="" placeholder="Arrival Date" data-provide="datepicker" class="form-control" />  
                      <span class="add-on"><i class="fa fa-calendar"></i></span>
                    </div>

                    <div class="form-group col-sm-6 pl0 ">
                      <input type="text" id="dpd2" value="" placeholder="Departure Date" data-provide="datepicker" class="form-control" />
                      <span class="add-on"><i class="fa fa-calendar"></i></span>
                    </div>
                
				
				</div>
					
			</div>
			
			
		</div>
	</section>
	
	

<!-- emailModal -->
<div class="modal fade emailModal" id="emailModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog w60">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4></h4>
      </div>
      <div class="modal-body">
        <iframe id="fancybox-frame1457578912627" name="fancybox-frame1457578912627" class="fancybox-iframe" frameborder="0" vspace="0" hspace="0" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen="" scrolling="auto" src="http://www.shoresummerrentals.com/popup/popup.php?pop_type=listing_emailform&amp;id=1500&amp;receiver=owner" width="100%"></iframe>
      </div>
    </div>
  </div>
</div>


<!-- emailFriendModal -->
<div class="modal fade emailModal" id="emailFriendModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog w60">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4></h4>
      </div>
      <div class="modal-body">
        <iframe id="fancybox-frame1457579659809" name="fancybox-frame1457579659809" class="fancybox-iframe" frameborder="0" vspace="0" hspace="0" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen="" scrolling="auto" src="http://www.shoresummerrentals.com/popup/popup.php?pop_type=listing_emailform&amp;id=1500&amp;receiver=friend" width="100%"></iframe>
      </div>
    </div>
  </div>
</div>



	
	<section class="advertisement-area mt30 advertisement-bottom text-center">
        <div class="container">
				<div class="googleAdsBottom">
					
					<script type="text/javascript">
                    google_ad_client	= "pub-0191483045807371";
                    google_ad_width		= 728;
                    google_ad_height	= 90;
                    google_ad_format	= "728x90_as";
                    google_ad_type		= "text_image";
                    google_ad_channel	= "";
                    google_color_border	= "336699";
                    google_color_bg		= "FFFFFF";
                    google_color_link	= "0000FF";
                    google_color_url	= "008000";
                    google_color_text	= "000000";
                </script>

					<script type="text/javascript" src="https://pagead2.googlesyndication.com/pagead/show_ads.js"></script><ins id="aswift_0_expand" style="display:none;border:none;height:0px;margin:0;padding:0;position:relative;visibility:visible;width:728px;background-color:transparent"><ins id="aswift_0_anchor" style="display:block;border:none;height:90px;margin:0;padding:0;position:relative;visibility:visible;width:728px;background-color:transparent"><iframe width="728" height="0" frameborder="0" marginwidth="0" marginheight="0" vspace="0" hspace="0" allowtransparency="true" scrolling="no" allowfullscreen="true" onload="var i=this.id,s=window.google_iframe_oncopy,H=s&amp;&amp;s.handlers,h=H&amp;&amp;H[i],w=this.contentWindow,d;try{d=w.document}catch(e){}if(h&amp;&amp;d&amp;&amp;(!d.body||!d.body.firstChild)){if(h.call){setTimeout(h,0)}else if(h.match){try{h=s.upd(h,i)}catch(e){}w.location.replace(h)}}" id="aswift_0" name="aswift_0" style="left:0;position:absolute;top:0;"></iframe></ins></ins>                    
				</div>
		</div>
	</section>
			
	
	
	<script>
	jQuery( ".def" ).click(function() {
var dataHover = $(this).attr("data-hover");
	jQuery(this).html(dataHover);
});
</script>
	
<?php include("footer.php"); ?>