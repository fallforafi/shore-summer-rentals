	<footer>
		<section class="ftr-quote quote-area col-sm-12 bg-orange clrhm p20 white text-center">
				<div class="go2top gotoTop">
                    <a href="#">
                        <i class="fa fa-chevron-up"></i><small>TOP</small></a>
                </div>
			<div class="container">
				<h2 class="h1">“Your NJ Shore Local Experts”</h2>
			</div>
		</section>

		<section class="ftr col-sm-12 bg-blue white links-orange text-center">
			<div class="container">

				<div class="ftr-logo"><img src="images/logo.png" alt="" /></div>
				
				<div class="social-area clrlist icon2x">
					<ul>
						<li><a href="#"><i class="fa fa-facebook"></i></a></li>
						<li><a href="#"><i class="fa fa-twitter"></i></a></li>
					</ul>					
				</div>
				
				<p>Join <a href="" target="_blank">ShoreSummerRentals.com</a> and <a href="http://www.vacationrentals2u.com/" target="_blank">VacationRentals2U.com</a> Now!</p>
				
				<p>P.O. Box 55, Somers Point, NJ 08244 | Phone: 609-677-1580 | Fax: 609-677-1590</p>
				
				<div class="text-center"><img src="images/credit-cards.png"></div>
				
				<div class="ftr__nav clrlist mt30 mb20">
					<ul class="listdvr">
						<li><a href="advertise.php">List Your Rental</a></li>
						<li><a href="advsearch.php">Find a Rental</a></li>
						<li><a href="orderby-city.php#!specials-lastminutespecials/">Last Minute Specials</a></li>
						<li><a href="orderby-city.php#!specials-underageof25/orderby-city">Under Age of 25 Rentals</a></li>
						<li><a href="orderby-city.php#!specials-fullsummerrentals/orderby-city">Full Summer Rentals</a></li>
						<li><a href="orderby-city.php#!specials-partialweekrentals/orderby-city">Partial Week Rentals</a></li>
						<li><a href="orderby-city.php#!specials-wintermonthlyrentals/orderby-city">Winter Monthly Rentals</a></li>
						<li><a href="orderby-city.php#!view-ocean_front/orderby-city">Beachfront Rentals</a></li>
						<li><a href="orderby-city.php#!iew-bay_front/orderby-city">Bayfront Rentals</a></li>
						<li><a href="orderby-city.php#!feature-petsallowed/orderby-city">Pet Friendly Rentals</a></li>
						<li><a href="orderby-city.php#!specials-forsale/orderby-city">Properties for Sale</a></li>
						<li><a href="new-jersey.php">New Jersey Rentals</a></li>
						<li><a href="florida.php">Florida Rentals</a></li> 
						<li><a href="maryland.php">Maryland Rentals</a></li>
						<li><a href="north-carolina.php">North Carolina Rentals</a></li>
						<li><a href="delaware.php">Delaware Rentals</a></li>
						<li><a href="faqs.php">FAQ</a></li>
						<li><a href="http://shoresummerrentals.us2.list-manage.com/subscribe?u=3fd02cb749e6ae50b2a8ebc8a&amp;id=68363bfde1">Submit Rental Request</a></li>
						<li><a href="privacy-policy.php">Privacy Policy</a></li>
					</ul>
				</div>
				
				<div class="clearfix"></div>
				
				<p class="powered-by">Powered by <a href="http://www.edirectory.com/" target="_blank">eDirectory Cloud Service</a>™</p>
				
				<p class="copyright">Copyright © 2016 Shore Summer Rentals, LLC, All Rights Reserved</p>
				
				<p><a href="terms-of-use.php">Terms of Use</a> &amp; <a href="legal-disclaimer.php">Legal Disclaimer</a></p>
				
			</div>
		</section>
	

	</footer>
	<a href="" class="scrollToTop gotoTop"><i class="fa fa-arrow-up"></i></a>

</section>
    

    <script src="js/bootstrap.min.js"></script>
	
	<script src="js/viewportchecker.js"></script>
    <script src="js/kodeized.js"></script>

	
	
	<script src="js/swiper.jquery.min.js"></script>
	<!-- Initialize Swiper -->
    <script>
		var swiper = new Swiper('.swiper-container', {
			pagination: '.swiper-pagination',
			slidesPerView: '7',
			centeredSlides: false,
			paginationClickable: true,
			nextButton: '.swiper-button-next',
			prevButton: '.swiper-button-prev',
			spaceBetween: 15,
			autoplay: 2500,
			autoplayDisableOnInteraction: false,
			breakpoints: {
            1024: {
                slidesPerView: 4,
                spaceBetween: 40
            },
            768: {
                slidesPerView: 3,
                spaceBetween: 30
            },
            640: {
                slidesPerView: 2,
                spaceBetween: 20
            },
            320: {
                slidesPerView: 1,
                spaceBetween: 10
            }
        }
		});
    </script>
	
	
	   </body>
</html>