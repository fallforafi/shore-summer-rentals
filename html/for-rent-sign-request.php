<?php include("header.php"); ?>

	
	<section class="spam-warning-page">
		<div class="container">
			<h2>FREE RENTAL SIGN FORM</h2>
			<h3>Help Advertising Your Rental</h3>
		<p>Your membership includes a FREE professional "FOR RENT" sign on your property. In certain towns, you are only permitted to hang one larger sign (18" x 24") or three smaller signs (6" x 24"). We provide wire ties with the sign to hang on railing or decking. The sign will have your phone number and rental id number on it. This has been very successful in securing additional rentals.&nbsp; By having the Rental ID on the sign, the renter will be able to pull up your property listing on our website to view interior photos, rates and all the details of your property. <a href="contactus.php">Contact us</a> to request a sign. <strong>Please include your name, property address, unit number, city, state, small or large sign and any other information you think we would need with your <a href="http://www.shoresummerrentals.com/contactus.php">request</a></strong>.
		<br><br>
		<a href="contactus.php"><img src="images/5_photo_224.jpg" alt="" /></a></p>
	<p>
		<a href="contactus.php"><img src="images/5_photo_140.jpg" alt="" /></a></p>
	
	
		<p class="hidden">It is strictly the owners responsibility to <a href="https://secure.vacationrentals2u.com/login/">login</a> to your <a href="https://secure.vacationrentals2u.com/login/">Shore</a>
		<a href="https://secure.vacationrentals2u.com/login/">SummerRentals.com</a> account daily to ensure that all of the renter inquiries sent to your account are retrieved.</p>
	
	
	<p>Please do not request a sign unless we have access to hang it on your property or you will be down in the next week or so to hang it yourself. The signs are costly and have been dropped off to several property owners in the past with owners never hanging them. &nbsp;If you have a lock box with a key to your property or someone in the area who has a key, I would be happy to pick up the key so that I can hang the sign for you. &nbsp;If you would like a sign for your property, please <a href="contactus.php">contact us</a>.&nbsp; The sign will be tied to the decking or a railing. &nbsp;&nbsp;If you require a sturdy stand for the lawn, there will be a cost of $20. You can pay by credit card or mail a check.</p>
	
	
		</div>
	</section>
	
	
<?php include("footer.php"); ?>