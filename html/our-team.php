<?php include("header.php"); ?>

	
	<section class="contact-page">
		<div class="container">
			<h1>OUR TEAM</h1>
			<h3>About Our Jersey Shore Rentals Team</h3>

			<div class="imgs clrlist">
				<ul>
					<li><img src="images/sitemgr_Untitled_design1.png" alt="" /></li>
					<li><img src="images/sitemgr_Untitled_design2.png" alt="" /></li>
				</ul>
			</div>
			
			
<p><a href="http://www.shoresummerrentals.com"><br>Shore Summer Rentals.com</a>, which offers quality jersey shore rentals to the online public, is owned and operated by Chris and Maria Kirk.<br><strong>No booking fees at all.&nbsp;&nbsp;You also have full control of your email leads.</strong>&nbsp; </p>
<p>With the strength of 30 plus years of combined experience in marketing, real estate, vacation rentals and property management. Chris and Maria have a wealth of knowledge in successfully pairing property owners with renters. &nbsp;Chris and Maria were investment property owners themselves at the New Jersey Shore. Their first-hand knowledge of vacation property rental ownership gives them the unique advantage to see rentals from both sides of the fence. From understanding what "quality" really means to valuing the definition of "good tenants", Chris and Maria offer exceptional rental consulting advice to both renters and property owners alike. Please read our Property Owner &nbsp;<a title="Property Owner Testimonials" href="http://shoresummerrentals.com/content/testimonials-from-owners.html">Testimonials</a>.</p>

<p>These invaluable experiences have given both Chris and Maria the necessary tools required for smart marketing and corporate savvy, while Maria possesses unparalleled customer service skills for smooth and detailed daily operations.</p>

<p><a href="mailto:maria@shoresummerrentals.com?subject=About%20Us%20Contact">Chris and Maria</a>&nbsp;live in the South Jersey area and have two children. Maria was born and raised in Cherry Hill, NJ. Chris was born and raised in the Northeast section of Philadelphia, PA. They moved to the Jersey Shore area in 2005 to be closer to the shore area and to be readily available to address their customer's needs. &nbsp;&nbsp;</p>

<p><a href="mailto:maria@shoresummerrentals.com?subject=About%20Us%20Contact"><span style="color: #0000ff;">Maria</span></a></span>&nbsp;handles the operations of the company. Prior to starting ShoreSummerRentals.com, Maria had eight years of commercial real estate/property management experience with firms located in Center City, Philadelphia. Maria decided to expand her horizons in the real estate business obtaining her New Jersey Real Estate license in January 2012. She works part-time selling and renting homes throughout the Southern NJ Shore area from Ocean City, NJ to Cape May, NJ.</p>

<p><a href="mailto:maria@shoresummerrentals.com?subject=About%20Us%20Contact">Chris</span></a></span>&nbsp;is a digital marketing expert with over 12 years experience in marketing NJ Shore vacation homes.&nbsp; Chris also runs his own online advertising agency where he manages Google Adwords, Facebook Ads, Twitter Ads and Pinterest Ads for his clients. &nbsp;Chris has over 15 years working in business intelligence for billion dollar organizations. Chris possesses a Bachelor's degree in Accounting from Temple University.</p>

<p><strong>Office Assistance</strong></p>

<p>Due to the increase in business,&nbsp; <a title="Shore Summer Rentals " href="http://www.shoresummerrentals.com/">Shore Summer Rentals.com</a> &nbsp;now staffs a personal assistant to assist Maria on a daily basis. Raya is a great asset to both Maria and Chris. &nbsp;Raya has been working with Maria and Chris back since 2004 when Chris and Maria owned an ice cream shop on Beach Ave in Cape May, NJ.&nbsp; Raya demonstrated a genuine care for the business and when ShoreSUmmerRentals.com needed help, Raya was the perfect choice.</p>

<p> Raya &nbsp;has a Bachelor's degree in Business Administration and Management. She is fluent in several languages, including English, French, and Bulgarian. Raya offers tremendous customer service skills.&nbsp; Raya and her husband have a 5 year old son.</p></div>


			
		</div>
	</section>
	
	
<?php include("footer.php"); ?>