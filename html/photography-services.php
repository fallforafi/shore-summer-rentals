<?php include("header.php"); ?>

	
	<section class="spam-warning-page">
		<div class="container">
			<h2>PHOTOGRAPHY SERVICES</h2>
			<h3>How do your pictures look?  Are they eye-catching or dark, blurry and pixelated?</h3>
		<p>Eye catching pictures are the biggest difference between an average rental season and a remarkable spectacular rental season including quality tenants. The right camera equipment can make all the difference. Contact us for a quality photo showcasing all of the features of your property that make it special.</p>
		
	<p>Pictures are the first and only impression the renters see. 99% of renters do not drive to the home until check in. Make sure to have several exterior photos, nearby attractions including the beach, bay, amusements, fishing piers, etc. If it does not look appealing, renters will quickly move on to another home. Don’t let this happen to you, take great pictures or use a professional service if possible.</p>
	
	<p>Do not have the time, patience or proper equipment to take eye catching&nbsp;photographs&nbsp;of your rental home?  No problem, we will do it all for you for only $125 per property (discounts for multiple properties). We know what shots to take to showcase the HIGHLIGHTS of your home.</p>
	
	<p>We will drive to your home, take photos of ALL the rooms in your home, download them on the web-site and then create a CD for you with all the photos for you to keep. &nbsp;Just&nbsp; <a href="contactus.php">contact us</a>&nbsp;and we can arrange all the details asap!</p>
	
	<p>We will drive to your home, take photos of ALL the rooms in your home, download them on the web-site and then create a CD for you with all the photos for you to keep. &nbsp;Just&nbsp;<span style="color: #0000ff;"><a href="contactus.php">contact us</span></a></span>&nbsp;and we can arrange all the details asap!<br>&nbsp;</p>
	
	<a href="https://vimeo.com/145011525"><img src="images/sitemgr_Screenshot_2016-01-25_20.16.39.png" alt="" width="500" height="328"></a>
	
	<h3>TAKING YOUR OWN PHOTOS </h3>
	<p>Please note that you can take your own photos and upload them to your ad on your own. If you are taking photos on your own and do not have a digital camera, you should order a disc along with your paper photos as the photos look much better when uploaded directly from your computer. &nbsp;</p>
	
		</div>
	</section>
	
	
<?php include("footer.php"); ?>