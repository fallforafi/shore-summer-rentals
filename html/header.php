<!DOCTYPE html>
<html lang="en" class="livewindowsize">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<!--iPhone from zooming form issue-->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
    <!--<meta name="viewport" content="width=device-width, initial-scale=1">-->
    <title>Home</title>

	<?php include("links.php"); ?>
		
  </head>
  <body class="">

<header>
	
	<section class="hdr-top bg-navy white clrlist listview" >
		<div class="container">
			<a class="navbar-brand" href="index.php"><img src="images/logo.png" alt="logo" /></a>
			
			<h2 class="inline-block col-sm-7 text-center ">"Your NJ Shore Local Experts"</h2>
			
			<ul class="login-area pul-rgt">
				<li><a href="login.php"><i class="fa fa-chevron-right"></i>OWNERS LOGIN</a></li>
				<li><a class="btn" href="advertise.php">LIST YOUR PROPERTY</a></li>
			</ul>
				
		</div>
	</section>
	
	<section class="hdr-nav bg-orange white cross-toggle" data-navitemlimit="">
		<div class="container">
			<nav class="navbar navbar-default" role="navigation" id="slide-nav">
			  <div class="container-fluid">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
				  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				  </button>
				</div>
				
				<div class="date pul-lft">February 22, 2016  | 
				<span id="clock">00:47:30</span></div>
				
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div id="slidemenu">
					<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					  <ul class="nav navbar-nav">
						<li class="dropdown">
						  <a href="#!4" class="dropdown-toggle" data-toggle="dropdown">Renters</a>
							<ul class="dropdown-menu">
								<li><a href="advsearch.php"><i class="fa fa-chevron-right"></i> Find a Rental (Advanced)</a></li>
								<li><a href="orderby-city.php#"><i class="fa fa-chevron-right"></i> Last Minute Specials</a></li>
								<li><a href="orderby-city.php#specials-fullsummerrentals/orderby-city"><i class="fa fa-chevron-right"></i> Full Summer Rentals</a></li>
								<li><a href="orderby-city.php#specials-partialweekrentals/orderby-city"><i class="fa fa-chevron-right"></i> Partial Week Rentals</a></li>
								<li><a href="orderby-city.php#specials-wintermonthlyrentals/orderby-city"><i class="fa fa-chevron-right"></i> Winter Monthly Rentals</a></li>
								<li><a href="orderby-city.php#specials-underageof25/orderby-city"><i class="fa fa-chevron-right"></i> Under Age of 25 Rentals</a></li>
								<li><a href="http://shoresummerrentals.us2.list-manage.com/subscribe?u=3fd02cb749e6ae50b2a8ebc8a&amp;id=68363bfde1"><i class="fa fa-chevron-right"></i> Submit Rental Request</a></li>
								<li><a href="fraud-alert-renters.php"><i class="fa fa-chevron-right"></i> Rental Fraud Alert</a></li>
							</ul>
						</li>
						
						<li class="dropdown">
						  <a href="#!4" class="dropdown-toggle" data-toggle="dropdown">Owners</a>
							<ul class="dropdown-menu" style="margin:0">
								<li><a href="advertise.php"><i class="fa fa-chevron-right"></i> List Your Rental</a></li>
								<li><a href="listing-instructions.php"><i class="fa fa-chevron-right"></i> Listing Instructions</a></li>
								<li><a href="guarantee-and-verify.php"><i class="fa fa-chevron-right"></i> Rental Guarantee</a></li>
								<li><a href="cash-back-referral.php"><i class="fa fa-chevron-right"></i> Cash Back Referrals</a></li>
								<li><a href="login.php"><i class="fa fa-chevron-right"></i> Login</a></li>
							</ul>
						</li>
						
						<li class="dropdown">
						  <a href="#!4" class="dropdown-toggle" data-toggle="dropdown">Testimonials</a>
							<ul class="dropdown-menu" style="margin:0">
                                <li><a href="testimonials-from-owners.php"><i class="fa fa-chevron-right"></i> Owners Testimonials</a></li>
							</ul>
						</li>
						
						<li class="dropdown">
						  <a href="#!4" class="dropdown-toggle" data-toggle="dropdown">Useful Links</a>
							<ul class="dropdown-menu" style="margin:0">
								<li><a href="login.php"><i class="fa fa-chevron-right"></i> Renew Membership</a></li>
								<li><a href="fraud-alert-owner.php"><i class="fa fa-chevron-right"></i> Owner Fraud Alert</a></li>
								<li><a href="faqs.php"><i class="fa fa-chevron-right"></i> FAQ's</a></li>
								<li><a href="spam-warning.php"><i class="fa fa-chevron-right"></i> Spam Warning</a></li>
								<li><a href="for-rent-sign-request.php"><i class="fa fa-chevron-right"></i> Rental Sign Form</a></li>
								<li><a href="photography-services.php"><i class="fa fa-chevron-right"></i> Photography Services</a></li>
							</ul>
						</li>
						
						
						<li class="dropdown">
						  <a href="#!4" class="dropdown-toggle" data-toggle="dropdown">About Us</a>
								<ul class="dropdown-menu" style="margin:0">
									<li><a href="our-team.php"><i class="fa fa-chevron-right"></i> Our Team</a></li>
									<li><a href="why-choose-us.php"><i class="fa fa-chevron-right"></i> Why Choose Us</a></li>
								</ul>
						</li>
						
						<li class="dropdown">
						  <a href="#!4" class="dropdown-toggle" data-toggle="dropdown">Contact Us</a>
							<ul class="dropdown-menu" style="margin:0">
								<li><a href="contactus.php"><i class="fa fa-chevron-right"></i> Contact Us Form</a></li>
								<li><a href="http://shoresummerrentals.us2.list-manage.com/subscribe?u=3fd02cb749e6ae50b2a8ebc8a&amp;id=68363bfde1"><i class="fa fa-chevron-right"></i> Rental Request Form</a></li>
							</ul>
						</li>
						
						
						<li><a href="https://www.shoresummerrentals.com/blog/">Blog</a></li>
						<li><a href="./">Home</a></li>
					  </ul>

					</div><!-- /.navbar-collapse -->
				</div>
			  </div><!-- /.container-fluid -->
			</nav>
		</div>
	</section>
</header>

<section id="page-content">
