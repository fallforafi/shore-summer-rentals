<body class="old_home">
<?php include("header_old.php"); ?>

	<section class="slider-area fadeft">
		<div class="container0">
		<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
		  <!-- Indicators -->
		  <ol class="carousel-indicators">
			<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
			<li data-target="#carousel-example-generic" data-slide-to="1"></li>
			<li data-target="#carousel-example-generic" data-slide-to="2"></li>
		  </ol>

		  <!-- Wrapper for slides -->
		  <div class="carousel-inner">
			<div class="item active">
			  <img src="images/slide1.jpg" alt="...">
			</div>
			<div class="item">
			  <img src="images/slide2.jpg" alt="...">
			</div>
			<div class="item">
			  <img src="images/slide3.jpg" alt="...">
			</div>
		  </div>

		  <a class="left carousel-control " href="#carousel-example-generic" data-slide="prev">
			<span class="glyphicon glyphicon-chevron-left"></span>
		  </a>
		  <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
			<span class="glyphicon glyphicon-chevron-right"></span>
		  </a>
		</div>
		
		
		
		
		<div class="slider-vdo col-sm-3" data-toggle="modal" data-target="#myModalvdo">
			<img src="images/vdo.jpg" alt="">
			<i class="fa fa-play-circle"></i>
		</div>
		<!-- Modal -->
		<div class="modal fade" id="myModalvdo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		  <div class="modal-dialog" role="document">
			<div class="modal-content">
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Media Box Video Title</h4>
			  </div>
			  <div class="modal-body">
				<iframe src="https://player.vimeo.com/video/41307155" width="100%" height="350" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
			  </div>
			</div>
		  </div>
		</div>
		
		
		

		</div>
		<div class="search-area">
			<div class="container">

				<div class="form-group col-sm-6">
					<input type="text" name="keyword" class="form-control keyword ac_input" id="keyword_resp" placeholder="Search by address, city, state, zip or rental id" value="" autocomplete="off">
				</div>


				<div class="form-group col-sm-2 search-checkin input-append">
				  <input id="searchCheckin" name="checkin" class="form-control hasDatepicker" type="text" value="" placeholder="check in" data-provide="datepicker">
				  <span class="add-on"><i class="fa fa-calendar"></i></span>
				</div>

				<div class="form-group col-sm-2 search-checkin input-append">
				  <input id="searchCheckout" name="checkout" class="form-control hasDatepicker" type="text" value="" placeholder="check out" data-provide="datepicker">
				  <span class="add-on"><i class="fa fa-calendar"></i></span>
				</div>

				<div class="form-group col-sm-2 search-button text-center">
					<button type="submit" class="btn btn-info btn-search form-control" id="search_form_submit"> <i class="fa fa-search"></i> Search</button>
				</div>
			
			<div class="fom-bot">
				<div class="col-sm-6">
				<span class="nj-viewall"><a class="pul-lft" href="new-jersey.html"><i class="fa fa-chevron-right"></i> View all NJ Rentals</a></span>
					&nbsp;
				</div>
				<div class="col-sm-6 fullwidth"> 
					<p class="inline-blok pul-rgt"><a href="advsearch.php"><i class="fa fa-chevron-right"></i> advanced search</a></p>
				</div>
			</div>

			</div>
		</div>
	</section>
	
	
	<section class="sm-area col-sm-12 p0">
		<div class="container">
		<div class="social-media">
        <div id="fb-root"></div>
        <script>(function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0";
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>
        <div class="fb-like fb_iframe_widget" data-href="https://www.facebook.com/ShoreSummerRent" data-layout="button_count" data-action="like" data-show-faces="false" data-share="false" fb-xfbml-state="rendered" fb-iframe-plugin-query="action=like&amp;app_id=&amp;container_width=1200&amp;href=https%3A%2F%2Fwww.facebook.com%2FShoreSummerRent&amp;layout=button_count&amp;locale=en_US&amp;sdk=joey&amp;share=false&amp;show_faces=false"><span style="vertical-align: bottom; width: 87px; height: 20px;"><iframe name="f6795d6b" width="1000px" height="1000px" frameborder="0" allowtransparency="true" allowfullscreen="true" scrolling="no" title="fb:like Facebook Social Plugin" src="http://www.facebook.com/v2.0/plugins/like.php?action=like&amp;app_id=&amp;channel=http%3A%2F%2Fstaticxx.facebook.com%2Fconnect%2Fxd_arbiter.php%3Fversion%3D42%23cb%3Df36bf6cb9%26domain%3Dwww.shoresummerrentals.com%26origin%3Dhttp%253A%252F%252Fwww.shoresummerrentals.com%252Ff317f2cffc%26relation%3Dparent.parent&amp;container_width=1200&amp;href=https%3A%2F%2Fwww.facebook.com%2FShoreSummerRent&amp;layout=button_count&amp;locale=en_US&amp;sdk=joey&amp;share=false&amp;show_faces=false" style="border: none; visibility: visible; width: 87px; height: 20px;" class=""></iframe></span></div>
        &nbsp;&nbsp;
                <iframe id="twitter-widget-1" scrolling="no" frameborder="0" allowtransparency="true" class="twitter-follow-button twitter-follow-button-rendered" title="Twitter Follow Button" src="http://platform.twitter.com/widgets/follow_button.b212c8422d3b3079acc6183618b32f10.en.html#dnt=false&amp;id=twitter-widget-1&amp;lang=en&amp;screen_name=shoresummerrent&amp;show_count=false&amp;show_screen_name=false&amp;size=m&amp;time=1457234288967" style="position: static; visibility: visible; width: 64px; height: 20px;" data-screen-name="shoresummerrent"></iframe>
        <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
        &nbsp;&nbsp;
        <span class="PIN_1457234290389_button_pin PIN_1457234290389_white" data-pin-log="button_pinit" data-pin-href="https://www.pinterest.com/pin/create/button/?guid=x8jBC61Chk9z-2&amp;url=http%3A%2F%2Fwww.shoresummerrentals.com&amp;media=http%3A%2F%2Fshoresummerrentals.arcastaging.com%2Fcustom%2Fdomain_1%2Ftheme%2Fdefault%2Fschemes%2Fdefault%2Fimages%2Fimagery%2Fimg-logo.png&amp;description=Shore%20Summer%20Rentals"></span>
        &nbsp;&nbsp;
                <div id="___follow_1" style="text-indent: 0px; margin: 0px; padding: 0px; border-style: none; float: none; line-height: normal; font-size: 1px; vertical-align: baseline; display: inline-block; width: 84px; height: 20px; background: transparent;"><iframe frameborder="0" hspace="0" marginheight="0" marginwidth="0" scrolling="no" style="position: static; top: 0px; width: 84px; margin: 0px; border-style: none; left: 0px; visibility: visible; height: 20px;" tabindex="0" vspace="0" width="100%" id="I1_1457234288153" name="I1_1457234288153" src="https://apis.google.com/u/0/_/widget/render/follow?usegapi=1&amp;annotation=none&amp;height=20&amp;rel=publisher&amp;hl=en&amp;origin=http%3A%2F%2Fwww.shoresummerrentals.com&amp;url=https%3A%2F%2Fplus.google.com%2F106846742271038575073&amp;gsrc=3p&amp;jsh=m%3B%2F_%2Fscs%2Fapps-static%2F_%2Fjs%2Fk%3Doz.gapi.en.JWhb167pjgE.O%2Fm%3D__features__%2Fam%3DAQ%2Frt%3Dj%2Fd%3D1%2Frs%3DAGLTcCPxLBa-KsqyOgC9o4jTQmHzeHbFMQ#_methods=onPlusOne%2C_ready%2C_close%2C_open%2C_resizeMe%2C_renderstart%2Concircled%2Cdrefresh%2Cerefresh%2Conload&amp;id=I1_1457234288153&amp;parent=http%3A%2F%2Fwww.shoresummerrentals.com&amp;pfname=&amp;rpctoken=51837946" data-gapiattached="true"></iframe></div>
    </div>
	
		</div>
	</section>
	
	<section class="summersale-area bg-red white links-white clrlist clrhm p20 mb30">
			<div class="container">
			<ul>
				<li class="col-sm-6">
					<h2><a href="new-jersey.php"><span>VIEW ALL NJ RENTALS - NO FEES</span></a></h2>
				</li>
				<li class="col-sm-2 price-starting"><strong>►►►►►►►</strong></li>
				<li class="col-sm-4"><strong><a class="btn btn-primary" href="advsearch.php">OR PERFORM ADVANCED SEARCH</a></strong></li>
			</ul>
			</div>
			</div>
	</section>
	
	
	<section class="special-area">
		<div class="container">
		
		<div class="hed crossline"><h2>SPECIALS SECTION</h2><hr/></div>
				
				
			<div class="special-lists-area clrlist">
						<ul>
							<li><a href="#"><img src="images/aside1.png" alt="" /></a></li>
							<li><a href="#"><img src="images/aside2.png" alt="" /></a></li>
							<li><a href="#"><img src="images/aside3.png" alt="" /></a></li>
							<li><a href="#"><img src="images/aside4.png" alt="" /></a></li>
							<li><a href="#"><img src="images/aside5.png" alt="" /></a></li>
							<li><a href="#"><img src="images/aside6.png" alt="" /></a></li>
					<li><a href="#"><img src="images/aside7.png" alt="" /></a></li>
					<li><a href="#"><img src="images/aside8.png" alt="" /></a></li>
				</ul>
			</div>
		</div>
	</section>
	
	<section class="special-area">
		<div class="container">
		<div class="advert-area list-col-4 col-sm-12 p0">
					<ul>
						<li><a href="orderby-city.php#"><img src="images/eddvert1.jpg" alt="" /></a></li>
						<li><a href="orderby-city.php#"><img src="images/eddvert2.jpg" alt="" /></a></li>
						<li><a href="orderby-city.php#"><img src="images/eddvert3.jpg" alt="" /></a></li>
						<li><a href="orderby-city.php#"><img src="images/eddvert4.jpg" alt="" /></a></li>	
					</ul>
			</div>
		</div>
	</section>
	
	<section class="special-area">
		<div class="container">
		<div class="advert-area list-col-4 col-sm-12 p0">
					<ul>
						<li><a href="orderby-city.php#"><img src="images/eddvert1.jpg" alt="" /></a></li>
						<li><a href="orderby-city.php#"><img src="images/eddvert2.jpg" alt="" /></a></li>
						<li><a href="orderby-city.php#"><img src="images/eddvert3.jpg" alt="" /></a></li>
						<li><a href="orderby-city.php#"><img src="images/eddvert4.jpg" alt="" /></a></li>	
					</ul>
			</div>

		</div>
	</section>
	
	<section class="main-area">
		<div class="container">
			
			<div class="main__lft col-sm-12 p0 ">
				<div class="featured-area col-sm-12 p0 imgzoom-hover">
				
				<div class="hed crossline"><h2>FEATURED RENTAL PROPERTIES</h2><hr/></div>
				
				<div class="featured-box col-sm-3">
					<div class="featured__inr">
						<div class="featured__img">
							<img src="images/featured1.jpg" alt="" />
						</div>
						<div class="featured__cont">
						<ul>
							<li>Ocean City</li>
							<li>New Jersey</li>
							<li>4 Beds | Bathrooms 2 | Sleeps 8</li>
						</ul>
						<div class="lnk-btn more-btn"><a href="#">View Details</a></div>
						</div>
					</div>
				</div>
			
				<div class="featured-box col-sm-3">
					<div class="featured__inr">
						<div class="featured__img">
							<img src="images/featured2.jpg" alt="" />
						</div>
						<div class="featured__cont">
						<ul>
							<li>Ocean City</li>
							<li>New Jersey</li>
							<li>4 Beds | Bathrooms 2 | Sleeps 8</li>
						</ul>
						<div class="lnk-btn more-btn"><a href="#">View Details</a></div>
						</div>
					</div>
				</div>
			
				<div class="featured-box col-sm-3">
					<div class="featured__inr">
						<div class="featured__img">
							<img src="images/featured3.jpg" alt="" />
						</div>
						<div class="featured__cont">
						<ul>
							<li>Ocean City</li>
							<li>New Jersey</li>
							<li>4 Beds | Bathrooms 2 | Sleeps 8</li>
						</ul>
						<div class="lnk-btn more-btn"><a href="#">View Details</a></div>
						</div>
					</div>
				</div>
			
				<div class="featured-box col-sm-3">
					<div class="featured__inr">
						<div class="featured__img">
							<img src="images/featured4.jpg" alt="" />
						</div>
						<div class="featured__cont">
						<ul>
							<li>Ocean City</li>
							<li>New Jersey</li>
							<li>4 Beds | Bathrooms 2 | Sleeps 8</li>
						</ul>
						<div class="lnk-btn more-btn"><a href="#">View Details</a></div>
						</div>
					</div>
				</div>
				
				<div class="featured-box col-sm-3">
					<div class="featured__inr">
						<div class="featured__img">
							<img src="images/featured1.jpg" alt="" />
						</div>
						<div class="featured__cont">
						<ul>
							<li>Ocean City</li>
							<li>New Jersey</li>
							<li>4 Beds | Bathrooms 2 | Sleeps 8</li>
						</ul>
						<div class="lnk-btn more-btn"><a href="#">View Details</a></div>
						</div>
					</div>
				</div>
			
				<div class="featured-box col-sm-3">
					<div class="featured__inr">
						<div class="featured__img">
							<img src="images/featured2.jpg" alt="" />
						</div>
						<div class="featured__cont">
						<ul>
							<li>Ocean City</li>
							<li>New Jersey</li>
							<li>4 Beds | Bathrooms 2 | Sleeps 8</li>
						</ul>
						<div class="lnk-btn more-btn"><a href="#">View Details</a></div>
						</div>
					</div>
				</div>
			
				<div class="featured-box col-sm-3">
					<div class="featured__inr">
						<div class="featured__img">
							<img src="images/featured3.jpg" alt="" />
						</div>
						<div class="featured__cont">
						<ul>
							<li>Ocean City</li>
							<li>New Jersey</li>
							<li>4 Beds | Bathrooms 2 | Sleeps 8</li>
						</ul>
						<div class="lnk-btn more-btn"><a href="#">View Details</a></div>
						</div>
					</div>
				</div>
			
				<div class="featured-box col-sm-3">
					<div class="featured__inr">
						<div class="featured__img">
							<img src="images/featured4.jpg" alt="" />
						</div>
						<div class="featured__cont">
						<ul>
							<li>Ocean City</li>
							<li>New Jersey</li>
							<li>4 Beds | Bathrooms 2 | Sleeps 8</li>
						</ul>
						<div class="lnk-btn more-btn"><a href="#">View Details</a></div>
						</div>
					</div>
				</div>
			</div>
			
			

				
	
				<div class="vacation-cont col-sm-12 p0">
					<h3>New Jersey Shore Vacation Rentals</h3>
					<p>Plan the perfect vacation rental getaway any time of year with <a href="">ShoreSummerRentals.com</a> As the premier resource for&nbsp;<a href="new-jersey.php">New Jersey Shore rentals</a> guests are sure to find the property that best fits their needs and makes for an amazing&nbsp;home-away-from-home experience.  Use the <a href="advsearch.php">search feature</a> to filter available rental homes by town, number of bedrooms, open dates, budget and more. We've also separated rentals by attributes like <a href="orderby-city.php#view-ocean_front/orderby-city">beachfront</a>, <a href="orderby-city.php#view-bay_front/orderby-city"> bayfront </a>, <a href="orderby-city.php#specials-fullsummerrentals/orderby-city"> full summer</a>, <a href="orderby-city.php#specials-wintermonthlyrentals/orderby-city"> monthly winter </a>, <a href="orderby-city.php#feature-petsallowed/orderby-city">pet friendly </a>,&nbsp;<a href="orderby-city.php#specials-lastminutespecials/orderby-city"> last minute deals </a></span>&nbsp;and more to make your search even easier.<br><br>Once you find the perfect home for you, contact the owner directly from the listing to work out all of the details. &nbsp;Whether you are looking for <a href="orderby-city.php#united-states/new-jersey/ocean-city-14"> Ocean City, NJ rentals </a>, <a href="orderby-city.php#united-states/new-jersey/sea-isle-city"> Sea Isle City rentals </a>, <a href="orderby-city.php#united-states/new-jersey/cape-may-19"> Cape May rentals </a>, <a hreforderby-city.php#united-states/new-jersey/north-wildwood">North Wildwood rentals </a>,&nbsp;<a href="united-states/new-jersey/wildwood"> Wildwood rentals, </a>&nbsp;<a href="orderby-city.php#united-states/new-jersey/wildwood-crest"> Wildwood Crest rentals </a>,&nbsp;<a href="orderby-city.php#united-states/new-jersey/diamond-beach"> Diamond Beach rentals </a></span>,&nbsp;<a href="orderby-city.php#united-states/new-jersey/long-beach-island--beach-haven"> LBI rentals </a> or any <a href="new-jersey.php"> Jersey Shore rentals, </a> we can help you find your next beach getaway accommodations.<br><br> <a href="advsearch.php"> Search Now! </a> </p>
					
					
					<h3>List Your Property</h3>
					
					<p><b>Earn rental income</b><br>
					Bring in extra money by putting your vacation home to work without a lot of additional time or effort.</p>
					<p><b>Easy to list, manage, and book.</b><br>
					We provide you with best-in-class tools to easily create and manage your listing.</p>
					<p><b>Reach a broad national audience.</b><br></p>
					<p><b>Earn rental income</b><br>
					Reach hundreds of thousands of travelers each month with Shore Summer Rentals.</p>
					
				</div>
			</div>
			
		</div>
	</section>
	
	
	
	
	<section class="advertisement-area mt30 advertisement-bottom text-center">
        <div class="container">
				<div class="googleAdsBottom">
					
					<script type="text/javascript">
                    google_ad_client	= "pub-0191483045807371";
                    google_ad_width		= 728;
                    google_ad_height	= 90;
                    google_ad_format	= "728x90_as";
                    google_ad_type		= "text_image";
                    google_ad_channel	= "";
                    google_color_border	= "336699";
                    google_color_bg		= "FFFFFF";
                    google_color_link	= "0000FF";
                    google_color_url	= "008000";
                    google_color_text	= "000000";
                </script>

					<script type="text/javascript" src="https://pagead2.googlesyndication.com/pagead/show_ads.js"></script><ins id="aswift_0_expand" style="display:none;border:none;height:0px;margin:0;padding:0;position:relative;visibility:visible;width:728px;background-color:transparent"><ins id="aswift_0_anchor" style="display:block;border:none;height:90px;margin:0;padding:0;position:relative;visibility:visible;width:728px;background-color:transparent"><iframe width="728" height="0" frameborder="0" marginwidth="0" marginheight="0" vspace="0" hspace="0" allowtransparency="true" scrolling="no" allowfullscreen="true" onload="var i=this.id,s=window.google_iframe_oncopy,H=s&amp;&amp;s.handlers,h=H&amp;&amp;H[i],w=this.contentWindow,d;try{d=w.document}catch(e){}if(h&amp;&amp;d&amp;&amp;(!d.body||!d.body.firstChild)){if(h.call){setTimeout(h,0)}else if(h.match){try{h=s.upd(h,i)}catch(e){}w.location.replace(h)}}" id="aswift_0" name="aswift_0" style="left:0;position:absolute;top:0;"></iframe></ins></ins>                    
				</div>
		</div>
	</section>



<?php include("footer.php"); ?>
