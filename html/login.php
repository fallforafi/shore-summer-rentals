<?php include("header.php"); ?>

	
	<section class="login-page">
		<div class="container">
			
			<div class="login__inr col-sm-6 col-sm-offset-3 bdr1 mt30 p20 clrhm inline-btns">
				<h1>LOG IN TO THE OWNER AREA</h1>
				<div class="login__fom">
					<form>
						
						<div class="form-group">
							<label>Account Holder E-mail</label>
							<input class="form-control" type="email" name="email" />
						</div>
						
						<div class="form-group">
							<label>Password</label>
							<input class="form-control" type="password" name="password" />
						</div>
						
						<div class="form-group">
							<input class="form-control" type="checkbox" name="check" />
							<label>Sign me in automatically</label>
						</div>
						
						<div class="form-group">
							<a href="#">Forgot your password?</a>
							<button class="form-control pul-rgt" type="submit" name="submit" />LOGIN</button>
						</div>
						
						<hr class="dvr" />
						
						<div class="botlinks clrlist listview text-center">
							<ul>
								<li><a href="http://www.shoresummerrentals.com/">Back to Website</a></li>
								<li><a href="http://www.shoresummerrentals.com/advertise.php">Do you want to advertise with us?</a></li>
							</ul>
						</div>
						
					</form>
				</div>
			</div>
	
	
	
		</div>
	</section>
	
	
<?php include("footer.php"); ?>