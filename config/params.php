<?php
return [
    'site_name' => 'SSR',
	'currency'=>[
            'USD'=>['symbol'=>'$','name'=>'US Dollors'],
    ],
    'currency_default'=>'USD',
    'languages'=>[
            'en_uk'=>'English (UK)',
            'en_us'=>'English (Us)',
    ],
    'language_default'=>'en_uk',
    'contentTypes'=>[
            'page'=>'Page',
            'email'=>'Email',
            'block'=>'Block',
    ],
    'imagesPath'=>'https://www.shoresummerrentals.com/custom/domain_1/image_files/',
   
    'FRIENDLY_SEARCH_IDENTIFIERS'=>'id,account,address,view,property_type,development_name,distance_beach,checkin,checkout,period,rate,locations,sleeps,bedroom,bathroom,amenity,feature,activity,category,rating,deals,keyword,where,dist,zip,match,screen,orderby,specials',
    'FRIENDLY_SEARCH_ALIAS'=>'search',
    'EDIR_CHARSET' => 'UTF-8',
    
     # ----------------------------------------------------------------------------------------------------
	# FAIL LOGIN
	# ----------------------------------------------------------------------------------------------------
    
    'FAILLOGIN_MAXFAIL'=>'4',// FAILLOGIN_MAXFAIL + 1 = block account
    'FAILLOGIN_TIMEBLOCK'=>'60',// minutes
    
    //Account locked. Wait X "minute(s) and try again."
    'LANG_MSG_ACCOUNTLOCKED2'=>'minute(s) and try again.',
    //"Account locked. Wait" X minute(s) and try again.
    'LANG_MSG_ACCOUNTLOCKED1'=>'Account locked. Wait',
    
    # ****************************************************************************************************
	# PASSWORD ENCRYPTION (DEFAULT ON)
	# ****************************************************************************************************
    'PASSWORD_ENCRYPTION'=>'on',
    
    //Sorry, your username or password is incorrect.
    'LANG_MSG_USERNAME_OR_PASSWORD_INCORRECT'=>'Sorry, your e-mail or password is incorrect.',
	
	# ----------------------------------------------------------------------------------------------------
	# DIRECTORY ALIAS DEFINITIONS
	# ----------------------------------------------------------------------------------------------------
    'MEMBERS_ALIAS'=>'sponsors',
    'SITEMGR_ALIAS'=>'sitemgr',

    'DEFAULT_URL'=>'http://',

    'EDIR_LANGUAGE'=>'en_us',
    
    'IMAGE_LISTING_THUMB_WIDTH'=>'309', //summary
    
    'IMAGE_LISTING_THUMB_HEIGHT'=>'202',//summary
   
    'UPLOAD_MAX_SIZE'=>'10', //in MB
    
    'FORCE_SAVE_JPG_AS_PNG'=>'off', 
    
    
    'IMAGE_SLIDER_WIDTH'=>'945', //slider
    
    
    'IMAGE_SLIDER_HEIGHT'=>'409', //slider
    
    
    'GALLERY_FREE_RATIO'=>'on', 
    
    
    'IMAGE_LISTING_FULL_WIDTH'=>'690',  //detail
    
    
    'IMAGE_LISTING_FULL_HEIGHT'=>'300',  //detail
    
    'FRIENDLYURL_SEPARATOR'=>'-',
    
    'LANG_LABEL_ACCOUNT'=>'Account',
    
    'CUSTOM_LISTINGTEMPLATE_FEATURE'=>'on',
    
    //Name or Title
    'LANG_LABEL_NAME_OR_TITLE'=>'Name or Title',
	
    //Page Name
    'LANG_LABEL_PAGE_NAME'=>'Page Name',
    
    
    //Summary Description
    'LANG_LABEL_SUMMARY_DESCRIPTION'=>'Summary Description',
    
    //Renewal Date
    'LANG_LABEL_RENEWAL_DATE'=>'Renewal Date',
    
    //Web Address
    'LANG_LABEL_WEB_ADDRESS'=>'Web Address',
    
    //Fax
    'LANG_LABEL_FAX'=>'Web AddressFax',
	
    
    //Long Description
    'LANG_LABEL_LONG_DESCRIPTION'=>'Long Description',
	
    //Status
    'LANG_LABEL_STATUS'=>'Status',
	
    //Level
    'LANG_LABEL_LEVEL'=>'Level',

    //Categories
    'LANG_LABEL_CATEGORY_PLURAL'=>'Categories',
	
    //E-mail
    'LANG_LABEL_EMAIL'=>'E-mail',
	
	
    //Phone
    'LANG_LABEL_PHONE'=>'Phone',

    //Username
   'LANG_LABEL_USERNAME2'=>'Username',
    //Company
    'LANG_LABEL_COMPANY'=>'Company',
    
    //Address Line1
    'LANG_LABEL_ADDRESS1'=>'Address Line1',

    //Address
    'LANG_LABEL_ADDRESS'=>'Address',
    
    //zipcode
    'ZIPCODE_LABEL'=>'Zipcode',
    
    //Country
    'LANG_LABEL_COUNTRY'=>'Country',
    //State
    'LANG_LABEL_STATE'=>'State',
    
    //City
    'LANG_LABEL_CITY'=>'City',
	
	
	//First Name
    'LANG_LABEL_FIRST_NAME'=>'First Name',
	
	//Last Name
    'LANG_LABEL_LAST_NAME'=>'Last Name',
	
	
	//The following fields were not filled or contain errors:
    'LANG_MSG_FIELDS_CONTAIN_ERRORS'=>'The following fields were not filled or contain errors:',
	
    
    //Total categories per item (listings)
    'LISTING_MAX_CATEGORY_ALLOWED'=>'5',// Unlimited
	
    
    //"Maximum of" [MAX_CATEGORY_ALLOWED] categories are allowed
    'LANG_MSG_MAX_OF_CATEGORIES_1'=>'Maximum of',
    
    
    //Maximum of [MAX_CATEGORY_ALLOWED] "categories are allowed"
    'LANG_MSG_MAX_OF_CATEGORIES_2'=>'categories are allowed.',
	

    //Friendly URL Page Name already in use, please choose another Page Name.
    'LANG_MSG_FRIENDLY_URL_IN_USE'=>'Friendly URL Page Name already in use, please choose another Page Name.',
	
    
    # ----------------------------------------------------------------------------------------------------
	# FRIENDLY URL CONSTANTS
	# IMPORTANT - PAY ATTENTION
	# Any changes here need to be done in all .htaccess (modrewrite)
	# ----------------------------------------------------------------------------------------------------
    'FRIENDLYURL_REGULAREXPRESSION'=>"/^[".'-'.'a-zA-Z0-9'."]{1,}/",
	
    
    
    //Page Name contain invalid chars
    'LANG_MSG_PAGE_NAME_INVALID_CHARS'=>'Page Name contain invalid chars',
	
    
    //Latitude must be a number between -90 and 90.
    'LANG_LABEL_MAP_INVALID_LAT'=>'Latitude must be a number between -90 and 90.',
	
    
    //Longitude must be a number between -180 and 180.
    'LANG_LABEL_MAP_INVALID_LON'=>'Longitude must be a number between -180 and 180.',
	
    
    # ----------------------------------------------------------------------------------------------------
	# KEYWORD CONSTANTS
	# ----------------------------------------------------------------------------------------------------
    
    'MAX_KEYWORDS'=>'10',
	
    
    //"Maximum of" [MAX_KEYWORDS] keywords are allowed
    'LANG_MSG_MAX_OF_KEYWORDS_ALLOWED_1'=>'Maximum of',
	
    
    //Maximum of [MAX_KEYWORDS] "keywords are allowed"
    'LANG_MSG_MAX_OF_KEYWORDS_ALLOWED_2'=>'keywords are allowed',
	
    
    //Please include keywords with a maximum of 50 characters each
    'LANG_MSG_PLEASE_INCLUDE_KEYWORDS'=>'Please include keywords with a maximum of 50 characters each',
	
    
    //Attached file was denied. Invalid file type.
    'LANG_MSG_ATTACHED_FILE_DENIED'=>'Attached file was denied. Invalid file type.',

   
   
    //Active
     'LANG_LABEL_ACTIVE'=>'Active',
	//Suspended
    'LANG_LABEL_SUSPENDED'=>'Suspended',
	//Expired
    'LANG_LABEL_EXPIRED'=>'Expired',
	//Pending
    'LANG_LABEL_PENDING'=>'Pending',
	//Received
    'LANG_LABEL_RECEIVED'=>'Received',

    
    
    // DATE FORMAT - SET JUST ONE FORMAT
	// Y - numeric representation of a year with 4 digits (xxxx)
	// m - numeric representation of a month with 2 digits (01 - 12)
	// d - numeric representation of a day of the month with 2 digits (01 - 31)
    'DEFAULT_DATE_FORMAT'=>'m/d/Y',
	
    
    'ZIPCODE_PROXIMITY'=>'on',
    
    'ZIPCODE_US'=>'on',// on/off
    
    'ZIPCODE_CA'=>'off',// on/off
    
    'ZIPCODE_UK'=>'off',// on/off
    'ZIPCODE_AU'=>'off',// on/off
    
    'PAYMENT_FEATURE'=>'on',
    
    'CREDITCARDPAYMENT_FEATURE'=>'on',
    
    
    //Search Position
    'LANG_LABEL_SEARCH_POS'=>'Search Position',

    'CURRENCY_SYMBOL'=>'$',
    'PAYMENT_CURRENCY'=>'USD',

    //For Payment
    'LANG_MSG_NO_ITEMS_SELECTED_REQUIRING_PAYMENT'=>'No items selected or requiring payment.',
    'LANG_MSG_PROBLEMS_WERE_FOUND'=>'The following problems were found',

    //No payment method was selected!
    'LANG_MSG_NO_PAYMENT_METHOD_SELECTED'=>'No payment method was selected!',

    //ITEM RENEWAL PERIOD
    'LISTING_RENEWAL_PERIOD'=>'1Y',
    'EVENT_RENEWAL_PERIOD'  =>'1M',
    'BANNER_RENEWAL_PERIOD' =>'1M',
    'CLASSIFIED_RENEWAL_PERIOD' =>'30D',
    'ARTICLE_RENEWAL_PERIOD'  =>'1Y',

    // RECURRING PAYMENT GATEWAY 
    'PAYPALRECURRING_FEATURE'  =>'off',

    'INVOICEPAYMENT_CURRENCY'  =>'USD',

    # CLOCK TYPE
    # options: 24 or 12
    'CLOCK_TYPE'  =>'12',
    'CLOCK_TYPE'  =>'24',

    //In manual transactions, subtotal and tax are not calculated.
    'LANG_TRANSACTION_MANUAL'=>'In manual transactions, subtotal and tax are not calculated',
    //creditcard
    'LANG_CREDITCARD'  =>'creditcard',
        //(prices amount are per installments)
    'LANG_MSG_PRICES_AMOUNT_PER_INSTALLMENTS'=>'(prices amount are per installments)',

    //Accepted
    'LANG_LABEL_ACCEPTED'=>'Accepted',
    //Approved
    'LANG_LABEL_APPROVED'=>'Approved',
    //Success
    'LANG_LABEL_SUCCESS'=>'Success',
    //Completed
    'LANG_LABEL_COMPLETED'=>'Completed',
    //Y
    'LANG_LABEL_Y'=>'Y',
    //Failed
    'LANG_LABEL_FAILED'=>'Failed',
    //Declined
    'LANG_LABEL_DECLINED'=>'Declined',
    //failure
    'LANG_LABEL_FAILURE'=>'failure',
    //Canceled
    'LANG_LABEL_CANCELED'=>'Canceled',

    //USER ATRIBUTES
    'USERNAME_MAX_LEN'=>'80', // don't forget to verify the field in DB
    'USERNAME_MIN_LEN'=>'4',
    'PASSWORD_MAX_LEN'=>'50',// don't forget to verify the field in DB
    'PASSWORD_MIN_LEN'=>'4',


    //DATE/TIME
    
    'LANG_DATE_MONTHS'=> 'january,february,march,april,may,june,july,august,september,october,november,december',

    //Promotional Code
    'LANG_LABEL_DISCOUNTCODE'=>'Promotional Code',
    'LANG_MSG_CANNOT_BE_USED_TWICE'=>'cannot be used twice.',
    //is not available.
   'LANG_MSG_IS_NOT_AVAILABLE'=>'is not available.',


   //Waiting Site Manager approve for Review
    'LANG_MSG_WAITINGSITEMGRAPPROVE_REVIEW'=>'Waiting Site Manager Approval for Review',
    //Waiting Site Manager approve for Reply
    'LANG_MSG_WAITINGSITEMGRAPPROVE_REPLY'=>'Waiting Site Manager Approval for Reply',
    //Waiting Site Manager approve for Review Reply
    'LANG_MSG_WAITINGSITEMGRAPPROVE_REVIEW_REPLY'=>'Waiting Site Manager Approval for Review and Reply',

    'LANG_LABEL_MESSAGE'=>'Message',
    'LANG_MSG_ENTER_VALID_EMAIL_ADDRESS'=>'Please enter a valid e-mail address',
     'LANG_LEAD_TYPEMESSAGE'=> 'Please type a message.',
     //Levels per category (all modules except for listings)
    'CATEGORY_LEVEL_AMOUNT'=>'5', // Limited to 5
    //Levels per category (listings)
    'LISTING_CATEGORY_LEVEL_AMOUNT'=>'5',// Unlimited
    //First Name is required.
    'LANG_MSG_FIRST_NAME_IS_REQUIRED'=> 'First Name is required.',
    //Last Name is required.
    'LANG_MSG_LAST_NAME_IS_REQUIRED'=> 'Last Name is required.',
    //Company is required.
    'LANG_MSG_COMPANY_IS_REQUIRED'=>  'Company is required.',
    //Phone is required.
    'LANG_MSG_PHONE_IS_REQUIRED'=>  'Phone is required.',
    //E-mail is required.
    'LANG_MSG_EMAIL_IS_REQUIRED'=> 'E-mail is required.',
    //Account is required.
    'LANG_MSG_ACCOUNT_IS_REQUIRED'=>  'Account is required.',

     //Zip is required.
    'LANG_MSG_ZIPCODE_IS_REQUIRED'=> 'Zip is required.',
    //City is required.
    'LANG_MSG_CITY_IS_REQUIRED'=> 'City is required.',
    //State is required.
    'LANG_MSG_STATE_IS_REQUIRED'=> 'State is required.',
    //Address 1 is required.
    'LANG_MSG_ADDRESS1_IS_REQUIRED'=> 'Address 1 is required.',
    //Please choose a different username.
    'LANG_MSG_CHOOSE_DIFFERENT_USERNAME'=> 'Please choose a different e-mail.',
        //Password is required.
   'LANG_MSG_PASSWORD_IS_REQUIRED'=> 'Password is required.',
    //"Agree to terms of use" is required.
    'LANG_MSG_IGREETERMS_IS_REQUIRED"'=> 'Agree to terms of use is required.',

    'PROFILE_IMAGE_RELATIVE_PATH'=>   '/custom/profile',
    'IMAGE_RELATIVE_PATH'=>   '/custom/domain_1/image_files',
 
    //"You have more than" X items. Please contact the administrator to check out it.
    'LANG_MSG_OVERITEM_MORETHAN'=>   'You have more than',
    //You have more than X items. "Please contact the administrator to check out it".
    'LANG_MSG_OVERITEM_CONTACTADMIN'=>   'Please contact the administrator to check out it',
    //Listing Badges
    'LANG_LISTING_DESIGNATION_PLURAL'=>   'Listing Specials',
    //PROFILE
    'PROFILE_IMAGE_WIDTH'=>               '60', //front pages
    'PROFILE_IMAGE_HEIGHT'=>               '60', //front pages
    'PROFILE_MEMBERS_IMAGE_WIDTH'=>        '130', //sponsors/profile pages
    'PROFILE_MEMBERS_IMAGE_HEIGHT'=>       '130', //sponsors/profile pages

   //Site Manager
   'LANG_LABEL_SITE_MANAGER'=>'Site Manager',
     //You have received a message from one of your site members.
    'LANG_NOTIFY_MEMBERSHELP_1'=>'You have received a message from one of your site members.',
    //From
    'LANG_NOTIFY_MEMBERSHELP_2'=>'From',
    //E-mail
    'LANG_NOTIFY_MEMBERSHELP_3'=>'E-mail',
    //Message
   'LANG_NOTIFY_MEMBERSHELP_4'=>'Message',

    //EMAIL NOTIFICATIONS CONSTANTS
    'RENEWAL_30'=>                            '1',
    'RENEWAL_15'=>                            '2',
    'RENEWAL_7'=>                            '3',
    'RENEWAL_1'=>                             '4',
    'SYSTEM_SPONSOR_ACCOUNT_CREATE'=>         '5',
    'SYSTEM_SPONSOR_ACCOUNT_UPDATE'=>         '6',
    'SYSTEM_VISITOR_ACCOUNT_CREATE'=>         '7',
    'SYSTEM_VISITOR_ACCOUNT_UPDATE'=>         '8',
    'SYSTEM_FORGOTTEN_PASS'=>                 '9',
    'SYSTEM_NEW_LISTING'=>                    '10',
    'SYSTEM_NEW_EVENT'=>                      '11',
    'SYSTEM_NEW_BANNER'=>                     '12',
    'SYSTEM_NEW_CLASSIFIED'=>                 '13',
    'SYSTEM_NEW_ARTICLE'=>                    '14',
    'SYSTEM_NEW_CUSTOMINVOICE'=>              '15',
    'SYSTEM_ACTIVE_LISTING'=>                 '16',
    'SYSTEM_ACTIVE_EVENT'=>                   '17',
    'SYSTEM_ACTIVE_BANNER'=>                  '18',
    'SYSTEM_ACTIVE_CLASSIFIED'=>              '19',
    'SYSTEM_ACTIVE_ARTICLE'=>                 '20',
    'SYSTEM_EMAIL_TOFRIEND'=>                 '21',
    'SYSTEM_LISTING_SIGNUP'=>                 '22',
    'SYSTEM_EVENT_SIGNUP'=>                   '23',
    'SYSTEM_BANNER_SIGNUP'=>                  '24',
    'SYSTEM_CLASSIFIED_SIGNUP'=>              '25',
    'SYSTEM_ARTICLE_SIGNUP'=>                 '26',
    'SYSTEM_CLAIM_SIGNUP'=>                   '27',
    'SYSTEM_CLAIM_AUTOMATICALLY_APPROVED'=>   '28',
    'SYSTEM_CLAIM_APPROVED'=>                 '29',
    'SYSTEM_CLAIM_DENIED'=>                   '30',
    'SYSTEM_APPROVE_REPLY'=>                  '31',
    'SYSTEM_APPROVE_REVIEW'=>                 '32',
    'SYSTEM_NEW_REVIEW'=>                     '33',
    'SYSTEM_INVOICE_NOTIFICATION'=>           '34',
    'SYSTEM_NEW_PROFILE'=>                  '35',
    'SYSTEM_EMAIL_TRAFFIC'=>                    '36',
    'SYSTEM_NEW_DEAL'=>                       '37',
    'SYSTEM_DEAL_DONE'=>                      '38',
    'SYSTEM_ACTIVATE_ACCOUNT'=>               '39',
    'SYSTEM_NEW_LEAD'=>                       '40',
    'SYSTEM_CONTACTUS_FEEDBACK'=>             '41',
    'RENEWAL_3'=>                           '42',
    'RENEWAL_0'=>                             '43',
    'SYSTEM_7_DAYS_ACTIVATED'=>             '44',
    'SYSTEM_LISTING_WITHOUT_PAYMENT'=>        '45',
    'SYSTEM_LISTING_OWNER_WARNING'=>        '46',
    'SYSTEM_LISTINGS_SUSPENDED'=>           '47', 
    'SYSTEM_LASTEMAIL_ID'=>                   '47',

    //CUSTOM_INVOICE_FEATURE
    'CUSTOM_INVOICE_FEATURE'=>    'on',

    //EDIRECTORY TITLE
   'EDIRECTORY_TITLE'=>          'ShoreSummerRentals.com',
   //New User Signup (Listing)
   'LANG_NOTIFY_SIGNUPLISTING'=>   'New User Signup (Listing)',

    //maximum email spam
    'THEME_MAX_EMAIL_SPAM'=>   '1000',//15
    //maximum ip spam
    'THEME_MAX_IP_SPAM'=>  '1000',

    'DEPOSIT_AMOUNT_WARNING'=> '20',

    'SOCIALNETWORK_FEATURE'=> 'on',

   //Please select a rating for this item
    'LANG_MSG_REVIEW_SELECTRATING'=> 'Please select a rating for this item',
    //Fraud detected! Please select a rating for this item!
    'LANG_MSG_REVIEW_FRAUD_SELECTRATING'=> 'Fraud detected! Please select a rating for this item!',
	//Please type a valid e-mail address!
    'LANG_MSG_REVIEW_TYPEVALIDEMAIL'=> 'Please type a valid e-mail address!',
    //"Name" and "E-mail" are required to post a comment!
    'LANG_MSG_REVIEW_NAMEEMAILREQUIRED'=> '\"Name\" and \"E-mail\" are required to post a comment!',
        //Please type the code correctly.
    'LANG_MSG_CONTACT_TYPE_CODE'=> 'Please type the code correctly.',
    //You have already given your opinion on this item. Thank you.
    'LANG_MSG_REVIEW_YOUALREADYGIVENOPINION'=> 'You have already given your opinion on this item. Thank you.',

    //Your review has been submitted for approval.
    'LANG_MSG_REVIEW_REVIEWSUBMITTEDAPPROVAL'=> 'Your review has been submitted for approval.',
     'ENABLE_CRON_LOG'=> 'true',

    //EXPIRE CONSTANTS
    'DEFAULT_LISTING_DAYS_TO_EXPIRE'=> '60',
    'DEFAULT_EVENT_DAYS_TO_EXPIRE'=> '60',
    'DEFAULT_CLASSIFIED_DAYS_TO_EXPIRE'=> '10',
    'DEFAULT_ARTICLE_DAYS_TO_EXPIRE'=> '60',

    //LOGS 
    'CRON_LOG_CLEAR_INTERVAL'=>'7', //days
    //EDIR_LOCATIONS
   'EDIR_LOCATIONS'=>'1,3,4',

   'LISTING_SCALABILITY_OPTIMIZATION'=>'off',

   //REPORTS CONSTANTS
    
    'LISTING_REPORT_SUMMARY_VIEW'=>'1',
    'LISTING_REPORT_DETAIL_VIEW'=>'2',
    'LISTING_REPORT_CLICK_THRU'=>'3',
    'LISTING_REPORT_EMAIL_SENT'=>'4',
    'LISTING_REPORT_PHONE_VIEW'=>'5',
   'LISTING_REPORT_FAX_VIEW'=>'6',
    'LISTING_REPORT_SMS'=>'7',
   'LISTING_REPORT_CLICKTOCALL'=>'8',
    'BANNER_REPORT_CLICK_THRU'=>'1',
    'BANNER_REPORT_VIEW'=>'2',
    'REPORT_DAYS_SHOW'=>'20',

    //Click here to try again
    'LANG_MSG_CLICK_HERE_TO_TRY_AGAIN'=>'Click here to try again',
    //Payment transactions may not occur immediately.
    'LANG_MSG_TRANSACTIONS_MAY_NOT_OCCUR_IMMEDIATELY'=>'Payment transactions may not occur immediately.',
    //After your payment is processed, information about your transaction
    'LANG_MSG_AFTER_PAYMENT_IS_PROCESSED_INFO_ABOUT'=>'After your payment is processed, information about your transaction',

        //The payment parameters could not be validated
   'LANG_MSG_PAYMENT_INVALID_PARAMS'=>'The payment parameters could not be validated',

    //SITEMAP CONSTANTS
   'SITEMAP_FEATURE'=>'on',
   # ----------------------------------------------------------------------------------------------------
    'SITEMAP_MAXURL'=>'20000',
    'SITEMAP_HASLISTINGLOCATION'=>'y',
    'SITEMAP_HASLISTINGCATEGORY'=>'y',
    'SITEMAP_HASLISTINGDETAIL'=>'y',
    'SITEMAP_HASPROMOTIONLOCATION'=>'y',
    'SITEMAP_HASPROMOTIONCATEGORY'=>'y',
    'SITEMAP_HASPROMOTIONDETAIL'=>'y',
    'SITEMAP_HASEVENTLOCATION'=>'y',
    'SITEMAP_HASEVENTCATEGORY'=>'y',
    'SITEMAP_HASEVENTDETAIL'=>'y',
    'SITEMAP_HASCLASSIFIEDLOCATION'=>'y',
    'SITEMAP_HASCLASSIFIEDCATEGORY'=>'y',
    'SITEMAP_HASCLASSIFIEDDETAIL'=>'y',
    'SITEMAP_HASARTICLECATEGORY'=>'y',
    'SITEMAP_HASARTICLEDETAIL'=>'y',
    'SITEMAP_HASARTICLENEWS'=>'y',
    'SITEMAP_HASBLOGCATEGORY'=>'y',
    'SITEMAP_HASBLOGDETAIL'=>'"y',
    'SITEMAP_HASCONTENT'=>'y',

];
?>