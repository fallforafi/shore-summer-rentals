<?php
define('TITLE','Lead Management System');
define('COMPANY','Golpik');

$siteParams=array(
        'currency'=>array(
        'symbol'=>'$',
        'code'=>'USD',   
        'name'=>'US Dollors'
    )   
);
$roles=array(
    1=>'Super Admin',
    2=>'Sales'
);

$lead_source=array(
    1=>'Others',
    2=>'Advertisement',
    3=>'Cold calling',
    4=>'External referral',
    5=>'Partner',
    6=>'Web search',
    7=>'Website',
    8=>'Elance',
    9=>'Gru',
    10=>'PPH',
    11=>'Freelancer'
); 

$lead_status=array(
    1=>'Others',
    2=>'Attempted to contact',
    3=>'Contact in Future',
    4=>'Contacted',
    5=>'Junk Lead',
    6=>'Not Contacted',
    7=>'Lost Lead',
    8=>'Pre Qualified'
);	

$industry=array(
    1=>'Others',
    2=>'Large Enterprise',
    3=>'Healthcare',
    4=>'Law Firm',
    5=>'Realtors',
    6=>'IT',
    7=>'Marketing Management',
    8=>'Freight and shipment',
    9=>'E-commerce',
    10=>'CMS',
    11=>'Simple Website',
);

$rating=array(
    1=>'Acquired',
    2=>'Active',
    3=>'Marked Failed',
    4=>'Project Cancelled',
    5=>'Arbitration',
    6=>'Shut Down'
);

$budget=array(
    1=>'under $2000',
    2=>'$2000-$5000',
    3=>'$5001-$8000',
    4=>'above $8000'
);

$start=array(
    1=>'0-1 week',
    2=>'1-3 weeks',
    3=>'4 weeks or more'
);


$projctsleadsType=array(
    1=>'Simple Leads',
    2=>'Market Place Leads'
);