/*
SQLyog Community v12.13 (32 bit)
MySQL - 5.6.25 : Database - too_cute
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`too_cute` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `too_cute`;

/*Table structure for table `administrators` */

DROP TABLE IF EXISTS `administrators`;

CREATE TABLE `administrators` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(190) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `administrators_username_unique` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `administrators` */

/*Table structure for table `categories` */

DROP TABLE IF EXISTS `categories`;

CREATE TABLE `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `url` text,
  `image` varchar(255) DEFAULT NULL,
  `description` text NOT NULL,
  `teaser` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `categories` */

insert  into `categories`(`id`,`name`,`parent_id`,`created_at`,`updated_at`,`url`,`image`,`description`,`teaser`) values (5,'Valentine\'s Day Outfits',NULL,'2016-02-04 09:55:01','2016-02-05 12:10:47','category_30/Valentines-Day-Outfits.htm','6561454579701.jpg','Our newest selection of super cute Valentine\'s Day Tutu outfits and sets are available now!  We have created many Valentine\'s Day tutu outfits that your baby girl  can wear for a first Valentine\'s Day photo shoot or just to look super sweet.  Any of the tutu sets can be worn for First Valentine\'s Day & up or for a Valentine birthday outfit.  Each set is available in almost any size and age.  Just email us if you do not see the size you need and we\'ll be more than happy to make the set for you.  The only size onesie that is hard to make is a newborn onesie, but from size 0-3 months & up is perfectly fine!  Do not worry if the Valentine\'s Day tutu set you like is titled or pictured with a 1 or First Valentine\'s Day because you may choose any age or size.\r\n\r\nMany of our Valentine\'s Day onesies are pictured with some of or most popular tutus, so if you would like just a onesie to match a tutu you already have, you may order just a onesie!  We love for our customers to get more than 1 use out of our tutus.\r\n\r\nWe also have several super cute Valentine\'s Day diaper covers to either match you Valentine\'s Day tutu set or to wear over the onesie without a tutu.\r\n \r\nYou may order the top from any of the tutu sets here:\r\nhttp://www.toocutebirthday.com/item_94/JUST-A-TOP.htm','Many of our Valentine\'s Day onesies are pictured with some of or most popular tutus, so if you would like just a onesie to match a tutu you already have, you may order just a onesie!  We love for our customers to get more than 1 use out of our tutus.'),(6,'Birthday Shirts ONLY',NULL,'2016-02-04 10:04:24','2016-02-05 12:11:18','category_62/Birthday-Shirts-ONLY.htm','1951454580264.jpg','You may order just a birthday shirt here for your little girl from any of our birthday tutu sets.  Even if you would like a custom personalized birthday shirt only, you may order it here as well.\r\n\r\nWe have thousands of outfits listed and it is simply too hard to list every shirt, but you may order any top here by filling in and selecting the information below.','You may order just a birthday shirt here for your little girl from any of our birthday tutu sets.  Even if you would like a custom personalized birthday shirt only, you may order it here as well.\r\n\r\nWe have thousands of outfits listed and it is simply too'),(7,'Simple Tutus Under $35',NULL,'2016-02-05 12:14:00','2016-02-05 12:14:00','category_54/Simple-Tutus-Under-35.htm','9861454674440.jpg','The sets below are simple and affordable for a Mommy on a Budget; for your daughter\'s birthday celebration outfit. The design of the shirt is digitaly made then printed with special adhesive paper, from there we Heat Press on the design on to a shirt for a very beautiful and clear image of the theme you wanted. You may wash the shirt in cold water inside out, and hand to dry. The tutu\'s are simple and have less tulle than our other tutu sets. These tutus are sewn with tulle around an elastic band fit to the perfect size for your daughter. You may also upgrade to have her tutu to be extra full aswell.\r\n','The sets below are simple and affordable for a Mommy on a Budget; for your daughter\'s birthday celebration outfit. The design of the shirt is digitaly made then printed with special adhesive paper, from there we Heat Press on the design on to a shirt for ');

/*Table structure for table `countries` */

DROP TABLE IF EXISTS `countries`;

CREATE TABLE `countries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(50) NOT NULL,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=247 DEFAULT CHARSET=latin1;

/*Data for the table `countries` */

insert  into `countries`(`id`,`code`,`name`) values (1,'AF','Afghanistan'),(2,'AL','Albania'),(3,'DZ','Algeria'),(4,'DS','American Samoa'),(5,'AD','Andorra'),(6,'AO','Angola'),(7,'AI','Anguilla'),(8,'AQ','Antarctica'),(9,'AG','Antigua and Barbuda'),(10,'AR','Argentina'),(11,'AM','Armenia'),(12,'AW','Aruba'),(13,'AU','Australia'),(14,'AT','Austria'),(15,'AZ','Azerbaijan'),(16,'BS','Bahamas'),(17,'BH','Bahrain'),(18,'BD','Bangladesh'),(19,'BB','Barbados'),(20,'BY','Belarus'),(21,'BE','Belgium'),(22,'BZ','Belize'),(23,'BJ','Benin'),(24,'BM','Bermuda'),(25,'BT','Bhutan'),(26,'BO','Bolivia'),(27,'BA','Bosnia and Herzegovina'),(28,'BW','Botswana'),(29,'BV','Bouvet Island'),(30,'BR','Brazil'),(31,'IO','British Indian Ocean Territory'),(32,'BN','Brunei Darussalam'),(33,'BG','Bulgaria'),(34,'BF','Burkina Faso'),(35,'BI','Burundi'),(36,'KH','Cambodia'),(37,'CM','Cameroon'),(38,'CA','Canada'),(39,'CV','Cape Verde'),(40,'KY','Cayman Islands'),(41,'CF','Central African Republic'),(42,'TD','Chad'),(43,'CL','Chile'),(44,'CN','China'),(45,'CX','Christmas Island'),(46,'CC','Cocos (Keeling) Islands'),(47,'CO','Colombia'),(48,'KM','Comoros'),(49,'CG','Congo'),(50,'CK','Cook Islands'),(51,'CR','Costa Rica'),(52,'HR','Croatia (Hrvatska)'),(53,'CU','Cuba'),(54,'CY','Cyprus'),(55,'CZ','Czech Republic'),(56,'DK','Denmark'),(57,'DJ','Djibouti'),(58,'DM','Dominica'),(59,'DO','Dominican Republic'),(60,'TP','East Timor'),(61,'EC','Ecuador'),(62,'EG','Egypt'),(63,'SV','El Salvador'),(64,'GQ','Equatorial Guinea'),(65,'ER','Eritrea'),(66,'EE','Estonia'),(67,'ET','Ethiopia'),(68,'FK','Falkland Islands (Malvinas)'),(69,'FO','Faroe Islands'),(70,'FJ','Fiji'),(71,'FI','Finland'),(72,'FR','France'),(73,'FX','France, Metropolitan'),(74,'GF','French Guiana'),(75,'PF','French Polynesia'),(76,'TF','French Southern Territories'),(77,'GA','Gabon'),(78,'GM','Gambia'),(79,'GE','Georgia'),(80,'DE','Germany'),(81,'GH','Ghana'),(82,'GI','Gibraltar'),(83,'GK','Guernsey'),(84,'GR','Greece'),(85,'GL','Greenland'),(86,'GD','Grenada'),(87,'GP','Guadeloupe'),(88,'GU','Guam'),(89,'GT','Guatemala'),(90,'GN','Guinea'),(91,'GW','Guinea-Bissau'),(92,'GY','Guyana'),(93,'HT','Haiti'),(94,'HM','Heard and Mc Donald Islands'),(95,'HN','Honduras'),(96,'HK','Hong Kong'),(97,'HU','Hungary'),(98,'IS','Iceland'),(99,'IN','India'),(100,'IM','Isle of Man'),(101,'ID','Indonesia'),(102,'IR','Iran (Islamic Republic of)'),(103,'IQ','Iraq'),(104,'IE','Ireland'),(105,'IL','Israel'),(106,'IT','Italy'),(107,'CI','Ivory Coast'),(108,'JE','Jersey'),(109,'JM','Jamaica'),(110,'JP','Japan'),(111,'JO','Jordan'),(112,'KZ','Kazakhstan'),(113,'KE','Kenya'),(114,'KI','Kiribati'),(115,'KP','Korea, Democratic People\'s Republic of'),(116,'KR','Korea, Republic of'),(117,'XK','Kosovo'),(118,'KW','Kuwait'),(119,'KG','Kyrgyzstan'),(120,'LA','Lao People\'s Democratic Republic'),(121,'LV','Latvia'),(122,'LB','Lebanon'),(123,'LS','Lesotho'),(124,'LR','Liberia'),(125,'LY','Libyan Arab Jamahiriya'),(126,'LI','Liechtenstein'),(127,'LT','Lithuania'),(128,'LU','Luxembourg'),(129,'MO','Macau'),(130,'MK','Macedonia'),(131,'MG','Madagascar'),(132,'MW','Malawi'),(133,'MY','Malaysia'),(134,'MV','Maldives'),(135,'ML','Mali'),(136,'MT','Malta'),(137,'MH','Marshall Islands'),(138,'MQ','Martinique'),(139,'MR','Mauritania'),(140,'MU','Mauritius'),(141,'TY','Mayotte'),(142,'MX','Mexico'),(143,'FM','Micronesia, Federated States of'),(144,'MD','Moldova, Republic of'),(145,'MC','Monaco'),(146,'MN','Mongolia'),(147,'ME','Montenegro'),(148,'MS','Montserrat'),(149,'MA','Morocco'),(150,'MZ','Mozambique'),(151,'MM','Myanmar'),(152,'NA','Namibia'),(153,'NR','Nauru'),(154,'NP','Nepal'),(155,'NL','Netherlands'),(156,'AN','Netherlands Antilles'),(157,'NC','New Caledonia'),(158,'NZ','New Zealand'),(159,'NI','Nicaragua'),(160,'NE','Niger'),(161,'NG','Nigeria'),(162,'NU','Niue'),(163,'NF','Norfolk Island'),(164,'MP','Northern Mariana Islands'),(165,'NO','Norway'),(166,'OM','Oman'),(167,'PK','Pakistan'),(168,'PW','Palau'),(169,'PS','Palestine'),(170,'PA','Panama'),(171,'PG','Papua New Guinea'),(172,'PY','Paraguay'),(173,'PE','Peru'),(174,'PH','Philippines'),(175,'PN','Pitcairn'),(176,'PL','Poland'),(177,'PT','Portugal'),(178,'PR','Puerto Rico'),(179,'QA','Qatar'),(180,'RE','Reunion'),(181,'RO','Romania'),(182,'RU','Russian Federation'),(183,'RW','Rwanda'),(184,'KN','Saint Kitts and Nevis'),(185,'LC','Saint Lucia'),(186,'VC','Saint Vincent and the Grenadines'),(187,'WS','Samoa'),(188,'SM','San Marino'),(189,'ST','Sao Tome and Principe'),(190,'SA','Saudi Arabia'),(191,'SN','Senegal'),(192,'RS','Serbia'),(193,'SC','Seychelles'),(194,'SL','Sierra Leone'),(195,'SG','Singapore'),(196,'SK','Slovakia'),(197,'SI','Slovenia'),(198,'SB','Solomon Islands'),(199,'SO','Somalia'),(200,'ZA','South Africa'),(201,'GS','South Georgia South Sandwich Islands'),(202,'ES','Spain'),(203,'LK','Sri Lanka'),(204,'SH','St. Helena'),(205,'PM','St. Pierre and Miquelon'),(206,'SD','Sudan'),(207,'SR','Suriname'),(208,'SJ','Svalbard and Jan Mayen Islands'),(209,'SZ','Swaziland'),(210,'SE','Sweden'),(211,'CH','Switzerland'),(212,'SY','Syrian Arab Republic'),(213,'TW','Taiwan'),(214,'TJ','Tajikistan'),(215,'TZ','Tanzania, United Republic of'),(216,'TH','Thailand'),(217,'TG','Togo'),(218,'TK','Tokelau'),(219,'TO','Tonga'),(220,'TT','Trinidad and Tobago'),(221,'TN','Tunisia'),(222,'TR','Turkey'),(223,'TM','Turkmenistan'),(224,'TC','Turks and Caicos Islands'),(225,'TV','Tuvalu'),(226,'UG','Uganda'),(227,'UA','Ukraine'),(228,'AE','United Arab Emirates'),(229,'GB','United Kingdom'),(230,'US','United States'),(231,'UM','United States minor outlying islands'),(232,'UY','Uruguay'),(233,'UZ','Uzbekistan'),(234,'VU','Vanuatu'),(235,'VA','Vatican City State'),(236,'VE','Venezuela'),(237,'VN','Vietnam'),(238,'VG','Virgin Islands (British)'),(239,'VI','Virgin Islands (U.S.)'),(240,'WF','Wallis and Futuna Islands'),(241,'EH','Western Sahara'),(242,'YE','Yemen'),(243,'YU','Yugoslavia'),(244,'ZR','Zaire'),(245,'ZM','Zambia'),(246,'ZW','Zimbabwe');

/*Table structure for table `migrations` */

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `migrations` */

insert  into `migrations`(`migration`,`batch`) values ('2014_10_12_000000_create_users_table',1),('2014_10_12_100000_create_password_resets_table',1),('2015_12_29_074818_create_tasks_table',1),('2015_12_30_072347_create_nerds_table',2),('2014_10_12_104748_create_administrators_table',3),('2015_12_30_113657_create_contents_table',4),('2016_01_05_095139_create_static_blocks_table',5),('2016_01_05_100242_create_static_blocks_language_table',5);

/*Table structure for table `password_resets` */

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `password_resets` */

/*Table structure for table `products` */

DROP TABLE IF EXISTS `products`;

CREATE TABLE `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `teaser` varchar(255) DEFAULT NULL,
  `description` text NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `keywords` varchar(255) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `deleted` int(2) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `url` text,
  `category_id` int(11) NOT NULL,
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `products` */

insert  into `products`(`id`,`name`,`teaser`,`description`,`image`,`keywords`,`status`,`deleted`,`created_at`,`updated_at`,`url`,`category_id`) values (1,'Valentine\'s Day Outfits','ads','asd','8491454670297.jpg','asd',NULL,NULL,'2016-02-05 11:04:57','2016-02-05 11:53:51','http://www.chipni.com',6),(2,'asd','asd','asd','5411454673009.jpg','dasdsa',NULL,NULL,'2016-02-05 11:05:45','2016-02-05 11:53:48','ads',6),(4,'Valentine\'s Day Outfits','asd\r\nasssssssssss\r\nasdddddd','sadadsassssssssssssssssssss\r\nas\r\nd\r\nas\r\nda\r\nsd\r\nasd\r\nas\r\nd','9131454673931.jpg','sadasdasd',NULL,NULL,'2016-02-05 12:00:52','2016-02-05 12:06:02','dsasadsad',6);

/*Table structure for table `roles` */

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `roles` */

insert  into `roles`(`id`,`role`) values (1,'admin'),(2,'free'),(3,'premium');

/*Table structure for table `states` */

DROP TABLE IF EXISTS `states`;

CREATE TABLE `states` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(50) NOT NULL,
  `name` varchar(100) NOT NULL,
  `country_code` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=latin1;

/*Data for the table `states` */

insert  into `states`(`id`,`code`,`name`,`country_code`) values (1,'Alaska','AK','US'),(2,'Alabama','AL','US'),(3,'American Samoa','AS','US'),(4,'Arizona','AZ','US'),(5,'Arkansas','AR','US'),(6,'California','CA','US'),(7,'Colorado','CO','US'),(8,'Connecticut','CT','US'),(9,'Delaware','DE','US'),(10,'District of Columbia','DC','US'),(11,'Federated States of Micronesia','FM','US'),(12,'Florida','FL','US'),(13,'Georgia','GA','US'),(14,'Guam','GU','US'),(15,'Hawaii','HI','US'),(16,'Idaho','ID','US'),(17,'Illinois','IL','US'),(18,'Indiana','IN','US'),(19,'Iowa','IA','US'),(20,'Kansas','KS','US'),(21,'Kentucky','KY','US'),(22,'Louisiana','LA','US'),(23,'Maine','ME','US'),(24,'Marshall Islands','MH','US'),(25,'Maryland','MD','US'),(26,'Massachusetts','MA','US'),(27,'Michigan','MI','US'),(28,'Minnesota','MN','US'),(29,'Mississippi','MS','US'),(30,'Missouri','MO','US'),(31,'Montana','MT','US'),(32,'Nebraska','NE','US'),(33,'Nevada','NV','US'),(34,'New Hampshire','NH','US'),(35,'New Jersey','NJ','US'),(36,'New Mexico','NM','US'),(37,'New York','NY','US'),(38,'North Carolina','NC','US'),(39,'North Dakota','ND','US'),(40,'Northern Mariana Islands','MP','US'),(41,'Ohio','OH','US'),(42,'Oklahoma','OK','US'),(43,'Oregon','OR','US'),(44,'Palau','PW','US'),(45,'Pennsylvania','PA','US'),(46,'Puerto Rico','PR','US'),(47,'Rhode Island','RI','US'),(48,'South Carolina','SC','US'),(49,'South Dakota','SD','US'),(50,'Tennessee','TN','US'),(51,'Texas','TX','US'),(52,'Utah','UT','US'),(53,'Vermont','VT','US'),(54,'Virgin Islands','VI','US'),(55,'Virginia','VA','US'),(56,'Washington','WA','US'),(57,'West Virginia','WV','US'),(58,'Wisconsin','WI','US'),(59,'Wyoming','WY','US'),(60,'Armed Forces Africa','AE','US'),(61,'Armed Forces Americas (except Canada)','AA','US'),(62,'Armed Forces Canada','AE','US'),(63,'Armed Forces Europe','AE','US'),(64,'Armed Forces Middle East','AE','US'),(65,'Armed Forces Pacific','AP','US');

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `role_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `users` */

insert  into `users`(`id`,`name`,`email`,`password`,`remember_token`,`role_id`,`created_at`,`updated_at`) values (1,'Asim','asim@gmail.com','$2y$10$J.A/baWkOZsZS7I5WtlgPemwAlotRJFb3iqeM/YCGBlZD6J7kBGzy','nrJEWm0J8OJ5n3mtCvbojvbyfpeDITaSBGagkQ5RqpjjPjoIITsMxw89IvtP',3,'2016-01-21 07:55:58','2016-01-29 15:49:06'),(2,'Waqar','waqar@gmail.com','$2y$10$GK2C38FgJkifpXxr.3QU0uzLlUZxrAyFU6jZMb9eYNBeJO7qZbzWO','apqkCjZtiO9iGtTmAtv7LqXgejBA0v1T0ROGnV4RjHVZXR2jh1mDzkdtVVDq',3,'2016-01-21 08:00:13','2016-01-26 07:25:30'),(3,'Kashif','kashif@gmail.com','$2y$10$n3K9s35xBr8UBOejD8Pmle9XFlcitZMC3lv55iqqXO7fMFl2z5ziG',NULL,2,'2016-01-21 08:25:21','2016-01-21 08:25:21'),(4,'Admin','admin@gmail.com','$2y$10$pUBTAxRsPfHr.OWzb/c5.eXRAI.chwOMt3KsdLkWK0lCOcIFs6e/.','BBgRyNImRd3PoPRTJ62yrDmCITauNcolfBui5UkzGqHJVTVFCLrJd4kEjH0x',1,'2016-01-21 10:11:59','2016-01-29 15:54:44'),(5,'Adnan','adnan@gmail.com','$2y$10$4EYOOk6t6JVyHZApo71vfeyVppdDJM0buu4BA23IYB/MCCq0AhIzm',NULL,2,'2016-01-22 13:12:01','2016-01-22 13:12:01'),(6,'Raza','raza@gmail.com','$2y$10$/.2bpgiMgyp0xTGLVtpUaOWWvCFq1vz6lsNd3WOCJT7fGxJJHzOq6',NULL,2,'2016-01-22 13:12:21','2016-01-22 13:12:21'),(7,'Aftab','aftab@gmail.com','$2y$10$hS97cxFJAHD488rbT99A/OygppT3szpXHbUdtnjAH9IWrEOiyZAS.','69OcXy6iOSZQPpOc8er4WjyX4SyXWUnFbwvqDvXquK1GKq6Iecaj1U7cOAGI',2,'2016-01-22 13:48:22','2016-01-26 12:56:28'),(8,'Syed ALi','ali@gmail.com','$2y$10$7W6mZzLkcDaJp2p/miELqu4tFbLeEXfskb/Q79hTjrw538JZcHnKe','9LU4qxDubq9CJaYaiqAVJYZJlNUNlogcGaLky5v7SZ18vGwGnMDjwfuOFrD5',2,'2016-01-22 14:13:22','2016-01-22 14:26:15'),(9,'John','john@gmail.com','$2y$10$P3wmZouMwmD9tpRcvA6KG.YJ2RdraMowJTq5pZUp/bmmAfrHe.GOi','1YdT28W0EG1cYFhVdw5vsSwfZpvOkUR5ImnyymU7TsgaB92HEhU17AnESD25',2,'2016-01-26 13:11:29','2016-01-27 10:56:44'),(10,'Mark','mark@gmail.com','$2y$10$TtM2W21jbKpr9vwhR9VSsuu0ofvH98sXX90r3O0.dKgTQ9hsnS8Am','d4qHYlQ1hEU0WX3RipfuMhNS5x7A67Du1h5Dolx6UcmhOGfDNufP6ceCGmte',2,'2016-01-28 07:45:21','2016-01-28 09:57:50'),(11,'Bush','bush@gmail.com','$2y$10$3uR825I8sZQAn7msvxcwD.Tip8pQiirr30lyaoNjH5Ec47nG2E.FS','esycgGcXC4Upmq4o8ZsoUXZO0BQxmh92tCa3NBsqqUpOPgTq22rjjZ6JuvHi',2,'2016-01-28 09:58:54','2016-01-28 12:25:01'),(12,'Aftab','aftab@yopmail.com','$2y$10$2MrTjSJsjiO5z/tkaBliDephRu6yBWJkOuW.GQjZGKVg5GvFiqxoK','tSwSPANDi4cu1rPsf2DQX8AS3yz8TF4DBRdZU24bSTbQF60DfHgGn5wIKZLo',3,'2016-01-29 15:49:53','2016-01-29 15:53:55');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
