@extends('front')

@section('content')

<?php
use App\Functions\ListingLevel;
use App\Functions\Functions;


 ?>


	<link rel="stylesheet" href="{{ asset('front/css/structure.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('front/css/members.min.css') }}" />
     <link rel="stylesheet" href="{{ asset('front/css/bootstrap.min.css') }}" />
     <link rel="stylesheet" href="{{ asset('front/css/jquery.fancybox.css') }}" />
     <div class="listing-level-container container members">

        <div class="container-fluid">

    <div>

        
        <form name="listinglevel" action="<?php echo url('sponsors/addlisting'); ?>" method="get" enctype="multipart/form-data">
            <input type="hidden" name="id" value="">
            
            <table border="0" cellpadding="0" cellspacing="0" class="levelTable">
            
                <tbody>
                    <tr>
                        <th class="levelTitle">Select listing level</th>
                        <td class="levelTopdetail">
                            Prices					
                        </td>
                    </tr>
                    <tr>
                        <th class="tableOption" colspan="2"><a href="{{ url('advertise') }}" target="_blank" class="listingOption">Listing Options</a></th>
                    </tr>
                 

                    <tr>
                    
                    
                        <td>
                     <table border="0" cellpadding="2" cellspacing="2" class="standardChooseLevel">

                                <input type="hidden" name="listingtemplate_id" value="0">    
                                <tbody>

                                <?php
                            //$levelObj = new ListingLevel(true);
                            //d($levelObj->getLevelValues(),1);die;
                           //$levelvalues = $levelObj->getLevelValues();
                            foreach ($spListinglevel as $level=>$levelvalue) {
                                if($level == 5) continue;
                                ?>
                                <tr>
                                    <th class="listingLevel">{{ Functions::string_ucwords($levelvalue->name) }}
                                    </th>
                                    <td><input type="radio" name="level" value="{{ $levelvalue->value }}" <?php if ($levelvalue->defaultlevel=='y') echo "checked"; ?> style="width: auto;" /></td>
                                </tr>
                                <?php
                            }
                    ?>
                                </tbody>
                            </table>
                        </td>
                        
                        
                        <td class="levelPrice">
                            <table border="0" cellpadding="2" cellspacing="2" class="standard-tableSAMPLE">
                                <tbody>
                                    <tr>
                                    </tr>
                                    <?php
                                foreach ($spListinglevel as $level=>$levelvalue) {
                                if($level == 5) continue;
                                ?>
                                    <tr><th>{{ Functions::string_ucwords($levelvalue->name) }}:</th>
                                    <td><strong>${{ Functions::format_money($levelvalue->price)}}</strong> per year</td>
                                    </tr>   
                                       <?php
                            }
                    ?>             
                                </tbody>
                            </table>
                        </td>
                    
                    </tr>
                
                </tbody>
            </table>   
            <div class="baseButtons">

                <p class="standardButton">
                    <button type="submit" >Submit</button>
                </p>
                <p class="standardButton">
                    <button type="Cancel">Cancel</button>
                </p>

            </div>     
        </form>

       

    </div>


            </div><!-- Close container-fluid div -->
            
        </div>
@endsection