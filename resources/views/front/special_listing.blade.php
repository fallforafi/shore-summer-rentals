 <?php
  /* === GOOGLE MAP JAVASCRIPT NEEDED (JQUERY) ==== */ ?>
@extends('front')
<script type='text/javascript' src='{{ asset('adminlte/maps.js')}}'></script>

<script type='text/javascript' src='{{ asset('adminlte/gmaps.js')}}'></script>
<?php
use App\Functions\Functions;

$currency= Config::get('params.currency');
$keywordForm = Session::get('keyword');
$checkinForm = Session::get('checkin');
$checkoutForm = Session::get('checkout');

$sleepsForm = Session::get('sleeps');
$bedroomForm = Session::get('bedroom');
$rate_minForm = Session::get('rate_min');
$rate_maxForm = Session::get('rate_max');
$periodForm = Session::get('period');




?>

@section('content')
    <section class="search-area2 mt30 section-orderby-top">
            <div class="container">
            <form  class="form" name="search_form" id="search_form" method="get" action="" onSubmit="return CheckForm()"   >
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group col-sm-6">
                    <input type="text" name="keyword" class="form-control keyword ac_input" id="keyword_resp" placeholder="Search by address, city, state, zip or rental id" value="<?php echo isset($keywordForm)?$keywordForm:''; ?>" autocomplete="off">
                </div>


                <div class="form-group col-sm-2 search-checkin input-append">
                  <input id="searchCheckin" name="checkin" class="form-control hasDatepicker" type="text" value="<?php echo isset($checkinForm)?$checkinForm:''; ?>" placeholder="check in" data-provide="datepicker">
                  <span class="add-on"><i class="fa fa-calendar"></i></span>
                </div>

                <div class="form-group col-sm-2 search-checkin input-append">
                  <input id="searchCheckout" name="checkout" class="form-control hasDatepicker" type="text" value="<?php echo isset($checkoutForm)?$checkoutForm:''; ?>" placeholder="check out" data-provide="datepicker">
                  <span class="add-on"><i class="fa fa-calendar"></i></span>
                </div>

                <div class="form-group col-sm-2 search-button text-center">
                    <button type="submit" class="btn btn-info btn-search form-control" id="search_form_submit"> <i class="fa fa-search"></i> Search</button>
                </div>
                
                <div class="clearfix"></div>
                
                <div class="adv-fom-bot clr-fom orderby-form-divTwo inline-control col-sm-12 text-center">
                    
                    <div class="form-group">
                        <select name="sleeps" class="form-control">
                            <option value="">Sleeps | Any</option>
                            
                            <option <?php echo (isset($sleepsForm) && $sleepsForm=='0' )?'SELECTED':'';  ?> value="0">0</option>
                            <option <?php echo (isset($sleepsForm) && $sleepsForm=='1exactly' )?'SELECTED':'';  ?>  value="1exactly">Sleeps | 1 Exactly</option>
                            <option <?php echo (isset($sleepsForm) && $sleepsForm=='1orLess' )?'SELECTED':'';  ?> value="1orLess">Sleeps | 1 Or Less</option>
                            <option <?php echo (isset($sleepsForm) && $sleepsForm=='1orMore' )?'SELECTED':'';  ?> value="1orMore">Sleeps | 1 Or More</option>
                            <option <?php echo (isset($sleepsForm) && $sleepsForm=='2exactly' )?'SELECTED':'';  ?> value="2exactly">Sleeps | 2 Exactly</option>
                            <option <?php echo (isset($sleepsForm) && $sleepsForm=='2orLess' )?'SELECTED':'';  ?> value="2orLess">Sleeps | 2 Or Less</option>
                            <option <?php echo (isset($sleepsForm) && $sleepsForm=='2orMore' )?'SELECTED':'';  ?> value="2orMore">Sleeps | 2 Or More</option>
                            <option <?php echo (isset($sleepsForm) && $sleepsForm=='3exactly' )?'SELECTED':'';  ?> value="3exactly">Sleeps | 3 Exactly</option>
                            <option <?php echo (isset($sleepsForm) && $sleepsForm=='3orLess' )?'SELECTED':'';  ?> value="3orLess">Sleeps | 3 Or Less</option>
                            <option <?php echo (isset($sleepsForm) && $sleepsForm=='3orMore' )?'SELECTED':'';  ?> value="3orMore">Sleeps | 3 Or More</option>
                            <option <?php echo (isset($sleepsForm) && $sleepsForm=='4exactly' )?'SELECTED':'';  ?> value="4exactly">Sleeps | 4 Exactly</option>
                            <option <?php echo (isset($sleepsForm) && $sleepsForm=='4orLess' )?'SELECTED':'';  ?> value="4orLess">Sleeps | 4 Or Less</option>
                            <option <?php echo (isset($sleepsForm) && $sleepsForm=='4orMore' )?'SELECTED':'';  ?> value="4orMore">Sleeps | 4 Or More</option>
                            <option <?php echo (isset($sleepsForm) && $sleepsForm=='5exactly' )?'SELECTED':'';  ?> value="5exactly">Sleeps | 5 Exactly</option>
                            <option <?php echo (isset($sleepsForm) && $sleepsForm=='5orLess' )?'SELECTED':'';  ?> value="5orLess">Sleeps | 5 Or Less</option>
                            <option <?php echo (isset($sleepsForm) && $sleepsForm=='5orMore' )?'SELECTED':'';  ?> value="5orMore">Sleeps | 5 Or More</option>
                            <option <?php echo (isset($sleepsForm) && $sleepsForm=='6exactly' )?'SELECTED':'';  ?> value="6exactly">Sleeps | 6 Exactly</option>
                            <option <?php echo (isset($sleepsForm) && $sleepsForm=='6orLess' )?'SELECTED':'';  ?> value="6orLess">Sleeps | 6 Or Less</option>
                            <option <?php echo (isset($sleepsForm) && $sleepsForm=='6orMore' )?'SELECTED':'';  ?> value="6orMore">Sleeps | 6 Or More</option>
                            <option <?php echo (isset($sleepsForm) && $sleepsForm=='7exactly' )?'SELECTED':'';  ?> value="7exactly">Sleeps | 7 Exactly</option>
                            <option <?php echo (isset($sleepsForm) && $sleepsForm=='7orLess' )?'SELECTED':'';  ?> value="7orLess">Sleeps | 7 Or Less</option>
                            <option <?php echo (isset($sleepsForm) && $sleepsForm=='7orMore' )?'SELECTED':'';  ?> value="7orMore">Sleeps | 7 Or More</option>
                            <option <?php echo (isset($sleepsForm) && $sleepsForm=='8exactly' )?'SELECTED':'';  ?> value="8exactly">Sleeps | 8 Exactly</option>
                            <option <?php echo (isset($sleepsForm) && $sleepsForm=='8orLess' )?'SELECTED':'';  ?> value="8orLess">Sleeps | 8 Or Less</option>
                            <option <?php echo (isset($sleepsForm) && $sleepsForm=='8orMore' )?'SELECTED':'';  ?> value="8orMore">Sleeps | 8 Or More</option>
                            <option <?php echo (isset($sleepsForm) && $sleepsForm=='9exactly' )?'SELECTED':'';  ?> value="9exactly">Sleeps | 9 Exactly</option>
                            <option <?php echo (isset($sleepsForm) && $sleepsForm=='9orLess' )?'SELECTED':'';  ?> value="9orLess">Sleeps | 9 Or Less</option>
                            <option <?php echo (isset($sleepsForm) && $sleepsForm=='9orMore' )?'SELECTED':'';  ?> value="9orMore">Sleeps | 9 Or More</option>
                            <option <?php echo (isset($sleepsForm) && $sleepsForm=='10exactly' )?'SELECTED':'';  ?> value="10exactly">Sleeps | 10 Exactly</option>
                            <option <?php echo (isset($sleepsForm) && $sleepsForm=='10orLess' )?'SELECTED':'';  ?> value="10orLess">Sleeps | 10 Or Less</option>
                            <option <?php echo (isset($sleepsForm) && $sleepsForm=='10orMore' )?'SELECTED':'';  ?> value="10orMore">Sleeps | 10 Or More</option>
                            <option <?php echo (isset($sleepsForm) && $sleepsForm=='11exactly' )?'SELECTED':'';  ?> value="11exactly">Sleeps | 11 Exactly</option>
                            <option <?php echo (isset($sleepsForm) && $sleepsForm=='11orLess' )?'SELECTED':'';  ?> value="11orLess">Sleeps | 11 Or Less</option>
                            <option <?php echo (isset($sleepsForm) && $sleepsForm=='11orMore' )?'SELECTED':'';  ?> value="11orMore">Sleeps | 11 Or More</option>
                            <option <?php echo (isset($sleepsForm) && $sleepsForm=='12exactly' )?'SELECTED':'';  ?> value="12exactly">Sleeps | 12 Exactly</option>
                            <option <?php echo (isset($sleepsForm) && $sleepsForm=='12orLess' )?'SELECTED':'';  ?> value="12orLess">Sleeps | 12 Or Less</option>
                            <option <?php echo (isset($sleepsForm) && $sleepsForm=='12orMore' )?'SELECTED':'';  ?> value="12orMore">Sleeps | 12 Or More</option>
                            <option <?php echo (isset($sleepsForm) && $sleepsForm=='13exactly' )?'SELECTED':'';  ?> value="13exactly">Sleeps | 13 Exactly</option>
                            <option <?php echo (isset($sleepsForm) && $sleepsForm=='13orLess' )?'SELECTED':'';  ?> value="13orLess">Sleeps | 13 Or Less</option>
                            <option <?php echo (isset($sleepsForm) && $sleepsForm=='13orMore' )?'SELECTED':'';  ?> value="13orMore">Sleeps | 13 Or More</option>
                        </select>
                    </div>
                    
                    <div class="form-group">
                        <select name="bedroom" class="form-control">
                            <option <?php echo (isset($bedroomForm) && $bedroomForm=='' )?'SELECTED':'';  ?> value="">Bedrooms | Any</option>
                            <option <?php echo (isset($bedroomForm) && $bedroomForm=='0' )?'SELECTED':'';  ?> value="0">0</option>
                            <option <?php echo (isset($bedroomForm) && $bedroomForm=='1exactly' )?'SELECTED':'';  ?>  value="1exactly">Bedroom | 1 Exactly</option>
                            <option <?php echo (isset($bedroomForm) && $bedroomForm=='1orLess' )?'SELECTED':'';  ?> value="1orLess">Bedroom | 1 Or Less</option>
                            <option <?php echo (isset($bedroomForm) && $bedroomForm=='1orMore' )?'SELECTED':'';  ?> value="1orMore">Bedroom | 1 Or More</option>
                            <option <?php echo (isset($bedroomForm) && $bedroomForm=='2exactly' )?'SELECTED':'';  ?> value="2exactly">Bedroom | 2 Exactly</option>
                            <option <?php echo (isset($bedroomForm) && $bedroomForm=='2orLess' )?'SELECTED':'';  ?> value="2orLess">Bedroom | 2 Or Less</option>
                            <option <?php echo (isset($bedroomForm) && $bedroomForm=='2orMore' )?'SELECTED':'';  ?> value="2orMore">Bedroom | 2 Or More</option>
                            <option <?php echo (isset($bedroomForm) && $bedroomForm=='3exactly' )?'SELECTED':'';  ?> value="3exactly">Bedroom | 3 Exactly</option>
                            <option <?php echo (isset($bedroomForm) && $bedroomForm=='3orLess' )?'SELECTED':'';  ?> value="3orLess">Bedroom | 3 Or Less</option>
                            <option <?php echo (isset($bedroomForm) && $bedroomForm=='3orMore' )?'SELECTED':'';  ?> value="3orMore">Bedroom | 3 Or More</option>
                            <option <?php echo (isset($bedroomForm) && $bedroomForm=='4exactly' )?'SELECTED':'';  ?> value="4exactly">Bedroom | 4 Exactly</option>
                            <option <?php echo (isset($bedroomForm) && $bedroomForm=='4orLess' )?'SELECTED':'';  ?> value="4orLess">Bedroom | 4 Or Less</option>
                            <option <?php echo (isset($bedroomForm) && $bedroomForm=='4orMore' )?'SELECTED':'';  ?> value="4orMore">Bedroom | 4 Or More</option>
                            <option <?php echo (isset($bedroomForm) && $bedroomForm=='5exactly' )?'SELECTED':'';  ?> value="5exactly">Bedroom | 5 Exactly</option>
                            <option <?php echo (isset($bedroomForm) && $bedroomForm=='5orLess' )?'SELECTED':'';  ?> value="5orLess">Bedroom | 5 Or Less</option>
                            <option <?php echo (isset($bedroomForm) && $bedroomForm=='5orMore' )?'SELECTED':'';  ?> value="5orMore">Bedroom | 5 Or More</option>
                            <option <?php echo (isset($bedroomForm) && $bedroomForm=='6exactly' )?'SELECTED':'';  ?> value="6exactly">Bedroom | 6 Exactly</option>
                            <option <?php echo (isset($bedroomForm) && $bedroomForm=='6orLess' )?'SELECTED':'';  ?> value="6orLess">Bedroom | 6 Or Less</option>
                            <option <?php echo (isset($bedroomForm) && $bedroomForm=='6orMore' )?'SELECTED':'';  ?> value="6orMore">Bedroom | 6 Or More</option>
                            <option <?php echo (isset($bedroomForm) && $bedroomForm=='7exactly' )?'SELECTED':'';  ?> value="7exactly">Bedroom | 7 Exactly</option>
                            <option <?php echo (isset($bedroomForm) && $bedroomForm=='7orLess' )?'SELECTED':'';  ?> value="7orLess">Bedroom | 7 Or Less</option>
                            <option <?php echo (isset($bedroomForm) && $bedroomForm=='7orMore' )?'SELECTED':'';  ?> value="7orMore">Bedroom | 7 Or More</option>
                            <option <?php echo (isset($bedroomForm) && $bedroomForm=='8exactly' )?'SELECTED':'';  ?> value="8exactly">Bedroom | 8 Exactly</option>
                            <option <?php echo (isset($bedroomForm) && $bedroomForm=='8orLess' )?'SELECTED':'';  ?> value="8orLess">Bedroom | 8 Or Less</option>
                            <option <?php echo (isset($bedroomForm) && $bedroomForm=='8orMore' )?'SELECTED':'';  ?> value="8orMore">Bedroom | 8 Or More</option>
                            <option <?php echo (isset($bedroomForm) && $bedroomForm=='9exactly' )?'SELECTED':'';  ?> value="9exactly">Bedroom | 9 Exactly</option>
                            <option <?php echo (isset($bedroomForm) && $bedroomForm=='9orLess' )?'SELECTED':'';  ?> value="9orLess">Bedroom | 9 Or Less</option>
                            <option <?php echo (isset($bedroomForm) && $bedroomForm=='9orMore' )?'SELECTED':'';  ?> value="9orMore">Bedroom | 9 Or More</option>
                            <option <?php echo (isset($bedroomForm) && $bedroomForm=='10exactly' )?'SELECTED':'';  ?> value="10exactly">Bedroom | 10 Exactly</option>
                            <option <?php echo (isset($bedroomForm) && $bedroomForm=='10orLess' )?'SELECTED':'';  ?> value="10orLess">Bedroom | 10 Or Less</option>
                            <option <?php echo (isset($bedroomForm) && $bedroomForm=='10orMore' )?'SELECTED':'';  ?> value="10orMore">Bedroom | 10 Or More</option>
                            <option <?php echo (isset($bedroomForm) && $bedroomForm=='11exactly' )?'SELECTED':'';  ?> value="11exactly">Bedroom | 11 Exactly</option>
                            <option <?php echo (isset($bedroomForm) && $bedroomForm=='11orLess' )?'SELECTED':'';  ?> value="11orLess">Bedroom | 11 Or Less</option>
                            <option <?php echo (isset($bedroomForm) && $bedroomForm=='11orMore' )?'SELECTED':'';  ?> value="11orMore">Bedroom | 11 Or More</option>
                            <option <?php echo (isset($bedroomForm) && $bedroomForm=='12exactly' )?'SELECTED':'';  ?> value="12exactly">Bedroom | 12 Exactly</option>
                            <option <?php echo (isset($bedroomForm) && $bedroomForm=='12orLess' )?'SELECTED':'';  ?> value="12orLess">Bedroom | 12 Or Less</option>
                            <option <?php echo (isset($bedroomForm) && $bedroomForm=='12orMore' )?'SELECTED':'';  ?> value="12orMore">Bedroom | 12 Or More</option>
                            <option <?php echo (isset($bedroomForm) && $bedroomForm=='13exactly' )?'SELECTED':'';  ?> value="13exactly">Bedroom | 13 Exactly</option>
                            <option <?php echo (isset($bedroomForm) && $bedroomForm=='13orLess' )?'SELECTED':'';  ?> value="13orLess">Bedroom | 13 Or Less</option>
                            <option <?php echo (isset($bedroomForm) && $bedroomForm=='13orMore' )?'SELECTED':'';  ?> value="13orMore">Bedroom | 13 Or More</option>
                        </select>
                    </div>
                        
                    <div class="form-group">
                        <select id="rate_min" name="rate_min" class="form-control">
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='' )?'SELECTED':'';  ?> value="">Rate | Min</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='100' )?'SELECTED':'';  ?> value="100">Rate | $100.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='150' )?'SELECTED':'';  ?> value="150">Rate | $150.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='200' )?'SELECTED':'';  ?> value="200">Rate | $200.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='250' )?'SELECTED':'';  ?> value="250">Rate | $250.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='300' )?'SELECTED':'';  ?> value="300">Rate | $300.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='350' )?'SELECTED':'';  ?> value="350">Rate | $350.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='400' )?'SELECTED':'';  ?> value="400">Rate | $400.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='450' )?'SELECTED':'';  ?> value="450">Rate | $450.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='500' )?'SELECTED':'';  ?> value="500">Rate | $500.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='550' )?'SELECTED':'';  ?> value="550">Rate | $550.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='600' )?'SELECTED':'';  ?> value="600">Rate | $600.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='650' )?'SELECTED':'';  ?> value="650">Rate | $650.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='700' )?'SELECTED':'';  ?> value="700">Rate | $700.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='750' )?'SELECTED':'';  ?> value="750">Rate | $750.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='800' )?'SELECTED':'';  ?> value="800">Rate | $800.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='850' )?'SELECTED':'';  ?> value="850">Rate | $850.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='900' )?'SELECTED':'';  ?> value="900">Rate | $900.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='950' )?'SELECTED':'';  ?> value="950">Rate | $950.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='1000' )?'SELECTED':'';  ?> value="1000">Rate | $1000.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='1050' )?'SELECTED':'';  ?> value="1050">Rate | $1050.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='1100' )?'SELECTED':'';  ?> value="1100">Rate | $1100.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='1150' )?'SELECTED':'';  ?> value="1150">Rate | $1150.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='1200' )?'SELECTED':'';  ?> value="1200">Rate | $1200.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='1250' )?'SELECTED':'';  ?> value="1250">Rate | $1250.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='1300' )?'SELECTED':'';  ?> value="1300">Rate | $1300.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='1350' )?'SELECTED':'';  ?> value="1350">Rate | $1350.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='1400' )?'SELECTED':'';  ?> value="1400">Rate | $1400.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='1450' )?'SELECTED':'';  ?> value="1450">Rate | $1450.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='1500' )?'SELECTED':'';  ?> value="1500">Rate | $1500.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='1550' )?'SELECTED':'';  ?> value="1550">Rate | $1550.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='1600' )?'SELECTED':'';  ?> value="1600">Rate | $1600.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='1650' )?'SELECTED':'';  ?> value="1650">Rate | $1650.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='1700' )?'SELECTED':'';  ?> value="1700">Rate | $1700.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='1750' )?'SELECTED':'';  ?> value="1750">Rate | $1750.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='1800' )?'SELECTED':'';  ?> value="1800">Rate | $1800.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='1850' )?'SELECTED':'';  ?> value="1850">Rate | $1850.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='1900' )?'SELECTED':'';  ?> value="1900">Rate | $1900.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='1950' )?'SELECTED':'';  ?> value="1950">Rate | $1950.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='2000' )?'SELECTED':'';  ?> value="2000">Rate | $2000.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='2050' )?'SELECTED':'';  ?> value="2050">Rate | $2050.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='2100' )?'SELECTED':'';  ?> value="2100">Rate | $2100.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='2150' )?'SELECTED':'';  ?> value="2150">Rate | $2150.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='2200' )?'SELECTED':'';  ?> value="2200">Rate | $2200.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='2250' )?'SELECTED':'';  ?> value="2250">Rate | $2250.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='2300' )?'SELECTED':'';  ?> value="2300">Rate | $2300.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='2350' )?'SELECTED':'';  ?> value="2350">Rate | $2350.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='2400' )?'SELECTED':'';  ?> value="2400">Rate | $2400.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='2450' )?'SELECTED':'';  ?> value="2450">Rate | $2450.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='2500' )?'SELECTED':'';  ?> value="2500">Rate | $2500.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='2550' )?'SELECTED':'';  ?> value="2550">Rate | $2550.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='2600' )?'SELECTED':'';  ?> value="2600">Rate | $2600.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='2650' )?'SELECTED':'';  ?> value="2650">Rate | $2650.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='2700' )?'SELECTED':'';  ?> value="2700">Rate | $2700.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='2750' )?'SELECTED':'';  ?> value="2750">Rate | $2750.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='2800' )?'SELECTED':'';  ?> value="2800">Rate | $2800.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='2850' )?'SELECTED':'';  ?> value="2850">Rate | $2850.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='2900' )?'SELECTED':'';  ?> value="2900">Rate | $2900.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='2950' )?'SELECTED':'';  ?> value="2950">Rate | $2950.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='3000' )?'SELECTED':'';  ?> value="3000">Rate | $3000.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='3050' )?'SELECTED':'';  ?> value="3050">Rate | $3050.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='3100' )?'SELECTED':'';  ?> value="3100">Rate | $3100.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='3150' )?'SELECTED':'';  ?> value="3150">Rate | $3150.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='3200' )?'SELECTED':'';  ?> value="3200">Rate | $3200.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='3250' )?'SELECTED':'';  ?> value="3250">Rate | $3250.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='3300' )?'SELECTED':'';  ?> value="3300">Rate | $3300.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='3350' )?'SELECTED':'';  ?> value="3350">Rate | $3350.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='3400' )?'SELECTED':'';  ?> value="3400">Rate | $3400.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='3450' )?'SELECTED':'';  ?> value="3450">Rate | $3450.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='3500' )?'SELECTED':'';  ?> value="3500">Rate | $3500.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='3550' )?'SELECTED':'';  ?> value="3550">Rate | $3550.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='3600' )?'SELECTED':'';  ?> value="3600">Rate | $3600.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='3650' )?'SELECTED':'';  ?> value="3650">Rate | $3650.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='3700' )?'SELECTED':'';  ?> value="3700">Rate | $3700.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='3750' )?'SELECTED':'';  ?> value="3750">Rate | $3750.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='3800' )?'SELECTED':'';  ?> value="3800">Rate | $3800.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='3850' )?'SELECTED':'';  ?> value="3850">Rate | $3850.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='3900' )?'SELECTED':'';  ?> value="3900">Rate | $3900.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='3950' )?'SELECTED':'';  ?> value="3950">Rate | $3950.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='4000' )?'SELECTED':'';  ?> value="4000">Rate | $4000.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='4100' )?'SELECTED':'';  ?> value="4100">Rate | $4100.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='4200' )?'SELECTED':'';  ?> value="4200">Rate | $4200.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='4300' )?'SELECTED':'';  ?> value="4300">Rate | $4300.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='4400' )?'SELECTED':'';  ?> value="4400">Rate | $4400.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='4500' )?'SELECTED':'';  ?> value="4500">Rate | $4500.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='4600' )?'SELECTED':'';  ?> value="4600">Rate | $4600.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='4700' )?'SELECTED':'';  ?> value="4700">Rate | $4700.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='4800' )?'SELECTED':'';  ?> value="4800">Rate | $4800.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='4900' )?'SELECTED':'';  ?> value="4900">Rate | $4900.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='5000' )?'SELECTED':'';  ?> value="5000">Rate | $5000.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='5100' )?'SELECTED':'';  ?> value="5100">Rate | $5100.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='5200' )?'SELECTED':'';  ?> value="5200">Rate | $5200.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='5300' )?'SELECTED':'';  ?> value="5300">Rate | $5300.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='5400' )?'SELECTED':'';  ?> value="5400">Rate | $5400.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='5500' )?'SELECTED':'';  ?> value="5500">Rate | $5500.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='5600' )?'SELECTED':'';  ?> value="5600">Rate | $5600.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='5700' )?'SELECTED':'';  ?> value="5700">Rate | $5700.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='5800' )?'SELECTED':'';  ?> value="5800">Rate | $5800.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='5900' )?'SELECTED':'';  ?> value="5900">Rate | $5900.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='6000' )?'SELECTED':'';  ?> value="6000">Rate | $6000.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='6100' )?'SELECTED':'';  ?> value="6100">Rate | $6100.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='6200' )?'SELECTED':'';  ?> value="6200">Rate | $6200.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='6300' )?'SELECTED':'';  ?> value="6300">Rate | $6300.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='6400' )?'SELECTED':'';  ?> value="6400">Rate | $6400.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='6500' )?'SELECTED':'';  ?> value="6500">Rate | $6500.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='6600' )?'SELECTED':'';  ?> value="6600">Rate | $6600.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='6700' )?'SELECTED':'';  ?> value="6700">Rate | $6700.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='6800' )?'SELECTED':'';  ?> value="6800">Rate | $6800.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='6900' )?'SELECTED':'';  ?> value="6900">Rate | $6900.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='7000' )?'SELECTED':'';  ?> value="7000">Rate | $7000.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='7100' )?'SELECTED':'';  ?> value="7100">Rate | $7100.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='7200' )?'SELECTED':'';  ?> value="7200">Rate | $7200.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='7300' )?'SELECTED':'';  ?> value="7300">Rate | $7300.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='7400' )?'SELECTED':'';  ?> value="7400">Rate | $7400.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='7500' )?'SELECTED':'';  ?> value="7500">Rate | $7500.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='7600' )?'SELECTED':'';  ?> value="7600">Rate | $7600.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='7700' )?'SELECTED':'';  ?> value="7700">Rate | $7700.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='7800' )?'SELECTED':'';  ?> value="7800">Rate | $7800.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='7900' )?'SELECTED':'';  ?> value="7900">Rate | $7900.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='8000' )?'SELECTED':'';  ?> value="8000">Rate | $8000.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='8100' )?'SELECTED':'';  ?> value="8100">Rate | $8100.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='8200' )?'SELECTED':'';  ?> value="8200">Rate | $8200.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='8300' )?'SELECTED':'';  ?> value="8300">Rate | $8300.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='8400' )?'SELECTED':'';  ?> value="8400">Rate | $8400.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='8500' )?'SELECTED':'';  ?> value="8500">Rate | $8500.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='8600' )?'SELECTED':'';  ?> value="8600">Rate | $8600.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='8700' )?'SELECTED':'';  ?> value="8700">Rate | $8700.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='8800' )?'SELECTED':'';  ?> value="8800">Rate | $8800.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='8900' )?'SELECTED':'';  ?> value="8900">Rate | $8900.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='9000' )?'SELECTED':'';  ?> value="9000">Rate | $9000.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='9100' )?'SELECTED':'';  ?> value="9100">Rate | $9100.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='9200' )?'SELECTED':'';  ?> value="9200">Rate | $9200.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='9300' )?'SELECTED':'';  ?> value="9300">Rate | $9300.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='9400' )?'SELECTED':'';  ?> value="9400">Rate | $9400.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='9500' )?'SELECTED':'';  ?> value="9500">Rate | $9500.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='9600' )?'SELECTED':'';  ?> value="9600">Rate | $9600.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='9700' )?'SELECTED':'';  ?> value="9700">Rate | $9700.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='9800' )?'SELECTED':'';  ?> value="9800">Rate | $9800.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='9900' )?'SELECTED':'';  ?> value="9900">Rate | $9900.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='10000' )?'SELECTED':'';  ?> value="10000">Rate | $10000.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='11000' )?'SELECTED':'';  ?> value="11000">Rate | $11000.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='12000' )?'SELECTED':'';  ?> value="12000">Rate | $12000.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='13000' )?'SELECTED':'';  ?> value="13000">Rate | $13000.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='14000' )?'SELECTED':'';  ?> value="14000">Rate | $14000.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='15000' )?'SELECTED':'';  ?> value="15000">Rate | $15000.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='16000' )?'SELECTED':'';  ?> value="16000">Rate | $16000.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='17000' )?'SELECTED':'';  ?> value="17000">Rate | $17000.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='18000' )?'SELECTED':'';  ?> value="18000">Rate | $18000.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='19000' )?'SELECTED':'';  ?> value="19000">Rate | $19000.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='20000' )?'SELECTED':'';  ?> value="20000">Rate | $20000.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='21000' )?'SELECTED':'';  ?> value="21000">Rate | $21000.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='22000' )?'SELECTED':'';  ?> value="22000">Rate | $22000.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='23000' )?'SELECTED':'';  ?> value="23000">Rate | $23000.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='24000' )?'SELECTED':'';  ?> value="24000">Rate | $24000.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='25000' )?'SELECTED':'';  ?> value="25000">Rate | $25000.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='26000' )?'SELECTED':'';  ?> value="26000">Rate | $26000.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='27000' )?'SELECTED':'';  ?> value="27000">Rate | $27000.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='28000' )?'SELECTED':'';  ?> value="28000">Rate | $28000.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='29000' )?'SELECTED':'';  ?> value="29000">Rate | $29000.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='30000' )?'SELECTED':'';  ?> value="30000">Rate | $30000.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='31000' )?'SELECTED':'';  ?> value="31000">Rate | $31000.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='32000' )?'SELECTED':'';  ?> value="32000">Rate | $32000.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='33000' )?'SELECTED':'';  ?> value="33000">Rate | $33000.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='34000' )?'SELECTED':'';  ?> value="34000">Rate | $34000.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='35000' )?'SELECTED':'';  ?> value="35000">Rate | $35000.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='36000' )?'SELECTED':'';  ?> value="36000">Rate | $36000.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='37000' )?'SELECTED':'';  ?> value="37000">Rate | $37000.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='38000' )?'SELECTED':'';  ?> value="38000">Rate | $38000.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='39000' )?'SELECTED':'';  ?> value="39000">Rate | $39000.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='40000' )?'SELECTED':'';  ?> value="40000">Rate | $40000.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='41000' )?'SELECTED':'';  ?> value="41000">Rate | $41000.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='42000' )?'SELECTED':'';  ?> value="42000">Rate | $42000.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='43000' )?'SELECTED':'';  ?> value="43000">Rate | $43000.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='44000' )?'SELECTED':'';  ?> value="44000">Rate | $44000.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='45000' )?'SELECTED':'';  ?> value="45000">Rate | $45000.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='46000' )?'SELECTED':'';  ?> value="46000">Rate | $46000.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='47000' )?'SELECTED':'';  ?> value="47000">Rate | $47000.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='48000' )?'SELECTED':'';  ?> value="48000">Rate | $48000.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='49000' )?'SELECTED':'';  ?> value="49000">Rate | $49000.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='50000' )?'SELECTED':'';  ?> value="50000">Rate | $50000.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='51000' )?'SELECTED':'';  ?> value="51000">Rate | $51000.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='52000' )?'SELECTED':'';  ?> value="52000">Rate | $52000.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='53000' )?'SELECTED':'';  ?> value="53000">Rate | $53000.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='54000' )?'SELECTED':'';  ?> value="54000">Rate | $54000.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='55000' )?'SELECTED':'';  ?> value="55000">Rate | $55000.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='56000' )?'SELECTED':'';  ?> value="56000">Rate | $56000.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='57000' )?'SELECTED':'';  ?> value="57000">Rate | $57000.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='58000' )?'SELECTED':'';  ?> value="58000">Rate | $58000.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='59000' )?'SELECTED':'';  ?> value="59000">Rate | $59000.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='60000' )?'SELECTED':'';  ?> value="60000">Rate | $60000.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='61000' )?'SELECTED':'';  ?> value="61000">Rate | $61000.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='62000' )?'SELECTED':'';  ?> value="62000">Rate | $62000.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='63000' )?'SELECTED':'';  ?> value="63000">Rate | $63000.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='64000' )?'SELECTED':'';  ?> value="64000">Rate | $64000.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='65000' )?'SELECTED':'';  ?> value="65000">Rate | $65000.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='66000' )?'SELECTED':'';  ?> value="66000">Rate | $66000.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='67000' )?'SELECTED':'';  ?> value="67000">Rate | $67000.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='68000' )?'SELECTED':'';  ?> value="68000">Rate | $68000.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='69000' )?'SELECTED':'';  ?> value="69000">Rate | $69000.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='70000' )?'SELECTED':'';  ?> value="70000">Rate | $70000.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='71000' )?'SELECTED':'';  ?> value="71000">Rate | $71000.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='72000' )?'SELECTED':'';  ?> value="72000">Rate | $72000.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='73000' )?'SELECTED':'';  ?> value="73000">Rate | $73000.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='74000' )?'SELECTED':'';  ?> value="74000">Rate | $74000.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='75000' )?'SELECTED':'';  ?> value="75000">Rate | $75000.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='76000' )?'SELECTED':'';  ?> value="76000">Rate | $76000.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='77000' )?'SELECTED':'';  ?> value="77000">Rate | $77000.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='78000' )?'SELECTED':'';  ?> value="78000">Rate | $78000.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='79000' )?'SELECTED':'';  ?> value="79000">Rate | $79000.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='80000' )?'SELECTED':'';  ?> value="80000">Rate | $80000.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='81000' )?'SELECTED':'';  ?> value="81000">Rate | $81000.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='82000' )?'SELECTED':'';  ?> value="82000">Rate | $82000.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='83000' )?'SELECTED':'';  ?> value="83000">Rate | $83000.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='84000' )?'SELECTED':'';  ?> value="84000">Rate | $84000.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='85000' )?'SELECTED':'';  ?> value="85000">Rate | $85000.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='86000' )?'SELECTED':'';  ?> value="86000">Rate | $86000.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='87000' )?'SELECTED':'';  ?> value="87000">Rate | $87000.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='88000' )?'SELECTED':'';  ?> value="88000">Rate | $88000.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='89000' )?'SELECTED':'';  ?> value="89000">Rate | $89000.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='90000' )?'SELECTED':'';  ?> value="90000">Rate | $90000.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='91000' )?'SELECTED':'';  ?> value="91000">Rate | $91000.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='92000' )?'SELECTED':'';  ?> value="92000">Rate | $92000.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='93000' )?'SELECTED':'';  ?> value="93000">Rate | $93000.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='94000' )?'SELECTED':'';  ?> value="94000">Rate | $94000.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='95000' )?'SELECTED':'';  ?> value="95000">Rate | $95000.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='96000' )?'SELECTED':'';  ?> value="96000">Rate | $96000.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='97000' )?'SELECTED':'';  ?> value="97000">Rate | $97000.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='98000' )?'SELECTED':'';  ?> value="98000">Rate | $98000.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='99000' )?'SELECTED':'';  ?> value="99000">Rate | $99000.00</option>
                            <option <?php echo (isset($rate_minForm) && $rate_minForm=='100000' )?'SELECTED':'';  ?> value="100000">Rate | $100000.00</option>
                        </select>
                    </div>
                    
                    <div class="form-group">
                        <select id="rate_max" name="rate_max" class="form-control">
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='any' )?'SELECTED':'';  ?> value="any" selected="selected">Rate | Max</option>
                             <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='100' )?'SELECTED':'';  ?> value="100">Rate | $100.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='150' )?'SELECTED':'';  ?> value="150">Rate | $150.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='200' )?'SELECTED':'';  ?> value="200">Rate | $200.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='250' )?'SELECTED':'';  ?> value="250">Rate | $250.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='300' )?'SELECTED':'';  ?> value="300">Rate | $300.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='350' )?'SELECTED':'';  ?> value="350">Rate | $350.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='400' )?'SELECTED':'';  ?> value="400">Rate | $400.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='450' )?'SELECTED':'';  ?> value="450">Rate | $450.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='500' )?'SELECTED':'';  ?> value="500">Rate | $500.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='550' )?'SELECTED':'';  ?> value="550">Rate | $550.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='600' )?'SELECTED':'';  ?> value="600">Rate | $600.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='650' )?'SELECTED':'';  ?> value="650">Rate | $650.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='700' )?'SELECTED':'';  ?> value="700">Rate | $700.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='750' )?'SELECTED':'';  ?> value="750">Rate | $750.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='800' )?'SELECTED':'';  ?> value="800">Rate | $800.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='850' )?'SELECTED':'';  ?> value="850">Rate | $850.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='900' )?'SELECTED':'';  ?> value="900">Rate | $900.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='950' )?'SELECTED':'';  ?> value="950">Rate | $950.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='1000' )?'SELECTED':'';  ?> value="1000">Rate | $1000.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='1050' )?'SELECTED':'';  ?> value="1050">Rate | $1050.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='1100' )?'SELECTED':'';  ?> value="1100">Rate | $1100.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='1150' )?'SELECTED':'';  ?> value="1150">Rate | $1150.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='1200' )?'SELECTED':'';  ?> value="1200">Rate | $1200.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='1250' )?'SELECTED':'';  ?> value="1250">Rate | $1250.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='1300' )?'SELECTED':'';  ?> value="1300">Rate | $1300.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='1350' )?'SELECTED':'';  ?> value="1350">Rate | $1350.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='1400' )?'SELECTED':'';  ?> value="1400">Rate | $1400.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='1450' )?'SELECTED':'';  ?> value="1450">Rate | $1450.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='1500' )?'SELECTED':'';  ?> value="1500">Rate | $1500.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='1550' )?'SELECTED':'';  ?> value="1550">Rate | $1550.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='1600' )?'SELECTED':'';  ?> value="1600">Rate | $1600.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='1650' )?'SELECTED':'';  ?> value="1650">Rate | $1650.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='1700' )?'SELECTED':'';  ?> value="1700">Rate | $1700.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='1750' )?'SELECTED':'';  ?> value="1750">Rate | $1750.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='1800' )?'SELECTED':'';  ?> value="1800">Rate | $1800.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='1850' )?'SELECTED':'';  ?> value="1850">Rate | $1850.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='1900' )?'SELECTED':'';  ?> value="1900">Rate | $1900.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='1950' )?'SELECTED':'';  ?> value="1950">Rate | $1950.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='2000' )?'SELECTED':'';  ?> value="2000">Rate | $2000.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='2050' )?'SELECTED':'';  ?> value="2050">Rate | $2050.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='2100' )?'SELECTED':'';  ?> value="2100">Rate | $2100.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='2150' )?'SELECTED':'';  ?> value="2150">Rate | $2150.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='2200' )?'SELECTED':'';  ?> value="2200">Rate | $2200.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='2250' )?'SELECTED':'';  ?> value="2250">Rate | $2250.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='2300' )?'SELECTED':'';  ?> value="2300">Rate | $2300.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='2350' )?'SELECTED':'';  ?> value="2350">Rate | $2350.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='2400' )?'SELECTED':'';  ?> value="2400">Rate | $2400.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='2450' )?'SELECTED':'';  ?> value="2450">Rate | $2450.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='2500' )?'SELECTED':'';  ?> value="2500">Rate | $2500.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='2550' )?'SELECTED':'';  ?> value="2550">Rate | $2550.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='2600' )?'SELECTED':'';  ?> value="2600">Rate | $2600.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='2650' )?'SELECTED':'';  ?> value="2650">Rate | $2650.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='2700' )?'SELECTED':'';  ?> value="2700">Rate | $2700.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='2750' )?'SELECTED':'';  ?> value="2750">Rate | $2750.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='2800' )?'SELECTED':'';  ?> value="2800">Rate | $2800.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='2850' )?'SELECTED':'';  ?> value="2850">Rate | $2850.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='2900' )?'SELECTED':'';  ?> value="2900">Rate | $2900.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='2950' )?'SELECTED':'';  ?> value="2950">Rate | $2950.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='3000' )?'SELECTED':'';  ?> value="3000">Rate | $3000.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='3050' )?'SELECTED':'';  ?> value="3050">Rate | $3050.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='3100' )?'SELECTED':'';  ?> value="3100">Rate | $3100.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='3150' )?'SELECTED':'';  ?> value="3150">Rate | $3150.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='3200' )?'SELECTED':'';  ?> value="3200">Rate | $3200.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='3250' )?'SELECTED':'';  ?> value="3250">Rate | $3250.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='3300' )?'SELECTED':'';  ?> value="3300">Rate | $3300.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='3350' )?'SELECTED':'';  ?> value="3350">Rate | $3350.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='3400' )?'SELECTED':'';  ?> value="3400">Rate | $3400.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='3450' )?'SELECTED':'';  ?> value="3450">Rate | $3450.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='3500' )?'SELECTED':'';  ?> value="3500">Rate | $3500.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='3550' )?'SELECTED':'';  ?> value="3550">Rate | $3550.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='3600' )?'SELECTED':'';  ?> value="3600">Rate | $3600.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='3650' )?'SELECTED':'';  ?> value="3650">Rate | $3650.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='3700' )?'SELECTED':'';  ?> value="3700">Rate | $3700.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='3750' )?'SELECTED':'';  ?> value="3750">Rate | $3750.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='3800' )?'SELECTED':'';  ?> value="3800">Rate | $3800.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='3850' )?'SELECTED':'';  ?> value="3850">Rate | $3850.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='3900' )?'SELECTED':'';  ?> value="3900">Rate | $3900.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='3950' )?'SELECTED':'';  ?> value="3950">Rate | $3950.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='4000' )?'SELECTED':'';  ?> value="4000">Rate | $4000.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='4100' )?'SELECTED':'';  ?> value="4100">Rate | $4100.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='4200' )?'SELECTED':'';  ?> value="4200">Rate | $4200.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='4300' )?'SELECTED':'';  ?> value="4300">Rate | $4300.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='4400' )?'SELECTED':'';  ?> value="4400">Rate | $4400.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='4500' )?'SELECTED':'';  ?> value="4500">Rate | $4500.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='4600' )?'SELECTED':'';  ?> value="4600">Rate | $4600.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='4700' )?'SELECTED':'';  ?> value="4700">Rate | $4700.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='4800' )?'SELECTED':'';  ?> value="4800">Rate | $4800.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='4900' )?'SELECTED':'';  ?> value="4900">Rate | $4900.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='5000' )?'SELECTED':'';  ?> value="5000">Rate | $5000.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='5100' )?'SELECTED':'';  ?> value="5100">Rate | $5100.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='5200' )?'SELECTED':'';  ?> value="5200">Rate | $5200.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='5300' )?'SELECTED':'';  ?> value="5300">Rate | $5300.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='5400' )?'SELECTED':'';  ?> value="5400">Rate | $5400.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='5500' )?'SELECTED':'';  ?> value="5500">Rate | $5500.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='5600' )?'SELECTED':'';  ?> value="5600">Rate | $5600.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='5700' )?'SELECTED':'';  ?> value="5700">Rate | $5700.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='5800' )?'SELECTED':'';  ?> value="5800">Rate | $5800.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='5900' )?'SELECTED':'';  ?> value="5900">Rate | $5900.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='6000' )?'SELECTED':'';  ?> value="6000">Rate | $6000.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='6100' )?'SELECTED':'';  ?> value="6100">Rate | $6100.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='6200' )?'SELECTED':'';  ?> value="6200">Rate | $6200.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='6300' )?'SELECTED':'';  ?> value="6300">Rate | $6300.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='6400' )?'SELECTED':'';  ?> value="6400">Rate | $6400.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='6500' )?'SELECTED':'';  ?> value="6500">Rate | $6500.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='6600' )?'SELECTED':'';  ?> value="6600">Rate | $6600.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='6700' )?'SELECTED':'';  ?> value="6700">Rate | $6700.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='6800' )?'SELECTED':'';  ?> value="6800">Rate | $6800.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='6900' )?'SELECTED':'';  ?> value="6900">Rate | $6900.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='7000' )?'SELECTED':'';  ?> value="7000">Rate | $7000.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='7100' )?'SELECTED':'';  ?> value="7100">Rate | $7100.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='7200' )?'SELECTED':'';  ?> value="7200">Rate | $7200.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='7300' )?'SELECTED':'';  ?> value="7300">Rate | $7300.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='7400' )?'SELECTED':'';  ?> value="7400">Rate | $7400.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='7500' )?'SELECTED':'';  ?> value="7500">Rate | $7500.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='7600' )?'SELECTED':'';  ?> value="7600">Rate | $7600.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='7700' )?'SELECTED':'';  ?> value="7700">Rate | $7700.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='7800' )?'SELECTED':'';  ?> value="7800">Rate | $7800.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='7900' )?'SELECTED':'';  ?> value="7900">Rate | $7900.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='8000' )?'SELECTED':'';  ?> value="8000">Rate | $8000.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='8100' )?'SELECTED':'';  ?> value="8100">Rate | $8100.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='8200' )?'SELECTED':'';  ?> value="8200">Rate | $8200.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='8300' )?'SELECTED':'';  ?> value="8300">Rate | $8300.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='8400' )?'SELECTED':'';  ?> value="8400">Rate | $8400.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='8500' )?'SELECTED':'';  ?> value="8500">Rate | $8500.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='8600' )?'SELECTED':'';  ?> value="8600">Rate | $8600.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='8700' )?'SELECTED':'';  ?> value="8700">Rate | $8700.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='8800' )?'SELECTED':'';  ?> value="8800">Rate | $8800.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='8900' )?'SELECTED':'';  ?> value="8900">Rate | $8900.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='9000' )?'SELECTED':'';  ?> value="9000">Rate | $9000.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='9100' )?'SELECTED':'';  ?> value="9100">Rate | $9100.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='9200' )?'SELECTED':'';  ?> value="9200">Rate | $9200.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='9300' )?'SELECTED':'';  ?> value="9300">Rate | $9300.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='9400' )?'SELECTED':'';  ?> value="9400">Rate | $9400.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='9500' )?'SELECTED':'';  ?> value="9500">Rate | $9500.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='9600' )?'SELECTED':'';  ?> value="9600">Rate | $9600.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='9700' )?'SELECTED':'';  ?> value="9700">Rate | $9700.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='9800' )?'SELECTED':'';  ?> value="9800">Rate | $9800.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='9900' )?'SELECTED':'';  ?> value="9900">Rate | $9900.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='10000' )?'SELECTED':'';  ?> value="10000">Rate | $10000.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='11000' )?'SELECTED':'';  ?> value="11000">Rate | $11000.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='12000' )?'SELECTED':'';  ?> value="12000">Rate | $12000.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='13000' )?'SELECTED':'';  ?> value="13000">Rate | $13000.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='14000' )?'SELECTED':'';  ?> value="14000">Rate | $14000.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='15000' )?'SELECTED':'';  ?> value="15000">Rate | $15000.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='16000' )?'SELECTED':'';  ?> value="16000">Rate | $16000.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='17000' )?'SELECTED':'';  ?> value="17000">Rate | $17000.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='18000' )?'SELECTED':'';  ?> value="18000">Rate | $18000.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='19000' )?'SELECTED':'';  ?> value="19000">Rate | $19000.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='20000' )?'SELECTED':'';  ?> value="20000">Rate | $20000.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='21000' )?'SELECTED':'';  ?> value="21000">Rate | $21000.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='22000' )?'SELECTED':'';  ?> value="22000">Rate | $22000.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='23000' )?'SELECTED':'';  ?> value="23000">Rate | $23000.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='24000' )?'SELECTED':'';  ?> value="24000">Rate | $24000.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='25000' )?'SELECTED':'';  ?> value="25000">Rate | $25000.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='26000' )?'SELECTED':'';  ?> value="26000">Rate | $26000.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='27000' )?'SELECTED':'';  ?> value="27000">Rate | $27000.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='28000' )?'SELECTED':'';  ?> value="28000">Rate | $28000.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='29000' )?'SELECTED':'';  ?> value="29000">Rate | $29000.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='30000' )?'SELECTED':'';  ?> value="30000">Rate | $30000.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='31000' )?'SELECTED':'';  ?> value="31000">Rate | $31000.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='32000' )?'SELECTED':'';  ?> value="32000">Rate | $32000.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='33000' )?'SELECTED':'';  ?> value="33000">Rate | $33000.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='34000' )?'SELECTED':'';  ?> value="34000">Rate | $34000.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='35000' )?'SELECTED':'';  ?> value="35000">Rate | $35000.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='36000' )?'SELECTED':'';  ?> value="36000">Rate | $36000.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='37000' )?'SELECTED':'';  ?> value="37000">Rate | $37000.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='38000' )?'SELECTED':'';  ?> value="38000">Rate | $38000.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='39000' )?'SELECTED':'';  ?> value="39000">Rate | $39000.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='40000' )?'SELECTED':'';  ?> value="40000">Rate | $40000.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='41000' )?'SELECTED':'';  ?> value="41000">Rate | $41000.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='42000' )?'SELECTED':'';  ?> value="42000">Rate | $42000.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='43000' )?'SELECTED':'';  ?> value="43000">Rate | $43000.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='44000' )?'SELECTED':'';  ?> value="44000">Rate | $44000.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='45000' )?'SELECTED':'';  ?> value="45000">Rate | $45000.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='46000' )?'SELECTED':'';  ?> value="46000">Rate | $46000.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='47000' )?'SELECTED':'';  ?> value="47000">Rate | $47000.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='48000' )?'SELECTED':'';  ?> value="48000">Rate | $48000.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='49000' )?'SELECTED':'';  ?> value="49000">Rate | $49000.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='50000' )?'SELECTED':'';  ?> value="50000">Rate | $50000.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='51000' )?'SELECTED':'';  ?> value="51000">Rate | $51000.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='52000' )?'SELECTED':'';  ?> value="52000">Rate | $52000.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='53000' )?'SELECTED':'';  ?> value="53000">Rate | $53000.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='54000' )?'SELECTED':'';  ?> value="54000">Rate | $54000.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='55000' )?'SELECTED':'';  ?> value="55000">Rate | $55000.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='56000' )?'SELECTED':'';  ?> value="56000">Rate | $56000.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='57000' )?'SELECTED':'';  ?> value="57000">Rate | $57000.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='58000' )?'SELECTED':'';  ?> value="58000">Rate | $58000.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='59000' )?'SELECTED':'';  ?> value="59000">Rate | $59000.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='60000' )?'SELECTED':'';  ?> value="60000">Rate | $60000.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='61000' )?'SELECTED':'';  ?> value="61000">Rate | $61000.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='62000' )?'SELECTED':'';  ?> value="62000">Rate | $62000.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='63000' )?'SELECTED':'';  ?> value="63000">Rate | $63000.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='64000' )?'SELECTED':'';  ?> value="64000">Rate | $64000.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='65000' )?'SELECTED':'';  ?> value="65000">Rate | $65000.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='66000' )?'SELECTED':'';  ?> value="66000">Rate | $66000.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='67000' )?'SELECTED':'';  ?> value="67000">Rate | $67000.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='68000' )?'SELECTED':'';  ?> value="68000">Rate | $68000.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='69000' )?'SELECTED':'';  ?> value="69000">Rate | $69000.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='70000' )?'SELECTED':'';  ?> value="70000">Rate | $70000.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='71000' )?'SELECTED':'';  ?> value="71000">Rate | $71000.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='72000' )?'SELECTED':'';  ?> value="72000">Rate | $72000.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='73000' )?'SELECTED':'';  ?> value="73000">Rate | $73000.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='74000' )?'SELECTED':'';  ?> value="74000">Rate | $74000.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='75000' )?'SELECTED':'';  ?> value="75000">Rate | $75000.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='76000' )?'SELECTED':'';  ?> value="76000">Rate | $76000.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='77000' )?'SELECTED':'';  ?> value="77000">Rate | $77000.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='78000' )?'SELECTED':'';  ?> value="78000">Rate | $78000.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='79000' )?'SELECTED':'';  ?> value="79000">Rate | $79000.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='80000' )?'SELECTED':'';  ?> value="80000">Rate | $80000.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='81000' )?'SELECTED':'';  ?> value="81000">Rate | $81000.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='82000' )?'SELECTED':'';  ?> value="82000">Rate | $82000.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='83000' )?'SELECTED':'';  ?> value="83000">Rate | $83000.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='84000' )?'SELECTED':'';  ?> value="84000">Rate | $84000.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='85000' )?'SELECTED':'';  ?> value="85000">Rate | $85000.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='86000' )?'SELECTED':'';  ?> value="86000">Rate | $86000.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='87000' )?'SELECTED':'';  ?> value="87000">Rate | $87000.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='88000' )?'SELECTED':'';  ?> value="88000">Rate | $88000.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='89000' )?'SELECTED':'';  ?> value="89000">Rate | $89000.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='90000' )?'SELECTED':'';  ?> value="90000">Rate | $90000.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='91000' )?'SELECTED':'';  ?> value="91000">Rate | $91000.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='92000' )?'SELECTED':'';  ?> value="92000">Rate | $92000.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='93000' )?'SELECTED':'';  ?> value="93000">Rate | $93000.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='94000' )?'SELECTED':'';  ?> value="94000">Rate | $94000.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='95000' )?'SELECTED':'';  ?> value="95000">Rate | $95000.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='96000' )?'SELECTED':'';  ?> value="96000">Rate | $96000.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='97000' )?'SELECTED':'';  ?> value="97000">Rate | $97000.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='98000' )?'SELECTED':'';  ?> value="98000">Rate | $98000.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='99000' )?'SELECTED':'';  ?> value="99000">Rate | $99000.00</option>
                            <option <?php echo (isset($rate_maxForm) && $rate_maxForm=='100000' )?'SELECTED':'';  ?> value="100000">Rate | $100000.00</option>

                        </select>
                    </div>
                    
                    <div class="form-group">
                        <select id="period" name="period" class="form-control">
                            <option <?php echo (isset($periodForm) && $periodForm=='' )?'SELECTED':'';  ?> value="">Rate period</option>
                            <option <?php echo (isset($periodForm) && $periodForm=='day' )?'SELECTED':'';  ?> value="day">Daily</option>
                            <option <?php echo (isset($periodForm) && $periodForm=='week' )?'SELECTED':'';  ?> value="week">Weekly</option>
                            <option <?php echo (isset($periodForm) && $periodForm=='month' )?'SELECTED':'';  ?> value="month">Monthly</option>
                            <option <?php echo (isset($periodForm) && $periodForm=='season' )?'SELECTED':'';  ?> value="season">per Season</option>
                        </select>
                    </div>
                    
                </div>
                

            </div>
            </form>
        
    </section>
    
    
    <section class="result-area listing-result-section">
    


  
  
    <div class="container">
            <div class="hed crossline"><h2><?php echo isset($meta_data[0]->title)?strtoupper($meta_data[0]->title):'LISTING RESULTS'; ?></h2><hr/></div>
            
          
            <div class="result__lft col-sm-9">
                <?php 
                
                if(!isset($results) || empty($results)){
                
                ?>
                    <div class="noresults">
                        <div class="resultsMessage">
                         <h1>No Results Returned for your search</h1><p>Your search returned no results. Although this is unusual, it happens from time to time. Perhaps the search term you used is a little generic, or perhaps we just don't have any content for that search.</p><h2>Suggestions:</h2>�Be more specific with your search terms<br>�Check your spelling<br>�If you can't find via search, try browsing by section<br><br><p>If you believe you have come here in error, please contact the site manager and report a problem.</p>
                        </div>
                    </div>
                <?php } else{ ?>
                
                
                
                <div class="results-info-listing">
                    <div class="search-info">
                        <p><i><?php echo $result_count; ?></i> Results</p>
                    </div>
                </div>
                
                
                <div class="tabs-area col-sm-12 p0">
                <?php
                                
                $tempPageurl =   $_SERVER['REQUEST_URI'];
                   if (strpos($tempPageurl, 'screen-') !== false) {
            $screen_array = explode("/screen-", $tempPageurl);
            $page_url = $screen_array[0];
            }   
            else
            {
                $page_url = $tempPageurl;
            } 
                
                $querystring=$page_url; 
                
                echo Functions::pagination($result_count, $per_page,$page_offset, $querystring,'/screen-');
                
                
    
                ?>
                <?php /* ?>
                    <div class="heading_pagenation">
                        <h2>Search Results <span>(<?php echo $result_count; ?>)</span></h2>
                        <?php 
                        //echo $current_offset.'--'.$per_page.;
                        if ($result_pages > 1) {  ?>
                        <nav>
                          <ul class="pagination">
                            <li class="<?php echo $current_offset == 0? 'disabled' : '';?>">
                              <a <?php echo $current_offset == 0? 'onclick="javascript:void(0);" ' : '';?> href="<?php echo $current_offset == 0?'#':url('search-results?'.$querystring); ?>" aria-label="Previous">
                                <span aria-hidden="true">Previous</span>
                              </a>
                            </li>
                            <?php 
                            for($i = 1; $i<=($result_pages);$i++) {?>
                                <li><a style="<?php echo (($i-1)*$per_page==$current_offset)?"background: #21a8a0;background: -moz-linear-gradient(top,  #21a8a0 0%, #137f78 100%);background: -webkit-linear-gradient(top,  #21a8a0 0%,#137f78 100%);background: linear-gradient(to bottom,  #21a8a0 0%,#137f78 100%);filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#21a8a0', endColorstr='#137f78',GradientType=0 );color:#fff;":"";  ?>" href="<?php echo (($i-1)*$per_page==$current_offset)?'javascript:void(0);':url('search-results?page_offset='.($i-1)*$per_page.'&'.$querystring); ?>"><?php echo $i; ?></a></li>
                            <?php } ?>
                            <li class="<?php echo $current_offset+$per_page > $result_count? 'disabled' : '';?>">
                              <a href="<?php echo (($current_offset+$per_page)>$result_count)?'javascript:void(0);':url('search-results?page_offset='.$next_offset.'&'.$querystring); ?>" aria-label="Next">
                                <span aria-hidden="true">Next</span>
                              </a>
                            </li>
                          </ul>
                        </nav>
                        <?php } ?>
                    <div class="clearfix"></div> 
                 </div>
                <?php */ ?>
                
                
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs">
                      <li class="active"><a href="#listView" class="list-view-tab" data-toggle="tab"><i class="fa fa-list"></i> List View</a></li>
                      <li><a href="#mapView" class="map-view-tab" data-toggle="tab"><i class="fa fa-map-marker"></i> Map View</a></li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                      
                      <div class="tab-pane active" id="listView">
                      
                       <div class="result-box result--thumb">
                        <?php
                          $locations = array();
                          $states_in_result = array();
                          $rating_in_result = array();
                          //d($results);die;
                            foreach($results as $key=>$val){  
                            $states_in_result[]  =  $val['location_3'];
                            //$cities_in_result[]  =  $val['location_4'];
                            $rating_in_result[]  =  $val['rating'];
                            ?>              
                       
                        
                   <div id="listing_summary_<?php echo $val['rental_id']; ?>" class="summary-small result_summary summary specials">
                            
                            
                   <div class="summary__dtl col-sm-12">

                   <section>
                       <div>
                        
                    <p><strong>Location:</strong> <a href="{{ url('listing/'.$val['rental_id'].'.html') }}"><?php echo $val['location_4_title'].', '.$val['location_3_title'].', '.$val['zip_code']; ?></a></p>
                  
                        <p><strong>Ends {{ Functions::format_date($val['renewal_date']) }}</strong></p>
                       
        <p><strong>Rental ID:</strong> <a href="{{ url('listing/'.$val['rental_id'].'.html') }}"
        ><?php echo $val['rental_id'].'-'.$val['title'].', '.$val['bedroom'].' Bedrooms, '.$val['bathroom'].' Bathrooms, Sleeps '.$val['sleeps']; ?></a></p>
        <p></p>
        <p><strong>{{ $name_special }}</strong>: {{ $val['lc_description'] }}</p>
        <p></p>

                    </div>


                   </section>
                                
                                
          
                                
                        
                        
                        </div></div>
                        <?php 
                            $locations[] = array(
                                'google_map' => array(
                                    'lat' => $val['latitude'],
                                    'lng' => $val['longitude'],
                                ),
                                'location_address' => $val['address'].' '.$val['zip_code'],
                                'location_name'    => $val['title'],
                            );
                           
                        } ?>
                        
                    
                                            

                        
                    

    

    


                        
                        
    
    
                      </div>
                      <?php 
                       /* Set Default Map Area Using First Location */
                            $map_area_lat = isset( $locations[0]['google_map']['lat'] ) ? $locations[0]['google_map']['lat'] : '';
                            $map_area_lng = isset( $locations[0]['google_map']['lng'] ) ? $locations[0]['google_map']['lng'] : '';
                      
                      ?>
                      <script>
                jQuery( document ).ready( function($) {

                    /* Do not drag on mobile. */
                    var is_touch_device = 'ontouchstart' in document.documentElement;

                    var map = new GMaps({
                        el: '#mapView',
                        lat: '<?php echo $map_area_lat; ?>',
                        lng: '<?php echo $map_area_lng; ?>',
                        scrollwheel: false,
                        draggable: ! is_touch_device
                    });

                    /* Map Bound */
                    var bounds = [];

                    <?php /* For Each Location Create a Marker. */
                    foreach( $locations as $location ){
                        $name = $location['location_name'];
                        $addr = $location['location_address'];
                        $map_lat = $location['google_map']['lat'];
                        $map_lng = $location['google_map']['lng'];
                        ?>
                        /* Set Bound Marker */
                        var latlng = new google.maps.LatLng(<?php echo $map_lat; ?>, <?php echo $map_lng; ?>);
                        bounds.push(latlng);
                        /* Add Marker */
                        map.addMarker({
                            lat: <?php echo $map_lat; ?>,
                            lng: <?php echo $map_lng; ?>,
                            title: '<?php echo $name; ?>',
                            infoWindow: {
                                content: '<p><?php echo $name; ?></p>'
                            }
                        });
                    <?php } //end foreach locations ?>

                    /* Fit All Marker to map */
                    map.fitLatLngBounds(bounds);
                    /* Make Map Responsive */
                    var $window = $(window);
                    function mapWidth() {
                        var size = $('.tab-content').width();
                        $('#mapView').css({width: size + 'px', height: (size/2) + 'px'});
                    }
                    mapWidth();
                    $(window).resize(mapWidth);

                });
                </script>
                      <div class="tab-pane" id="mapView">
                      
                        MAP ...
                      
                      </div>
    
                    </div>
                </div>
                <?php } // results end ?>
                        
            </div>
        
            
            
            
            
            
            
        </div><!----filters side search starts----->
            <?php if(isset($results) && !empty($results)){?>
            <div class="result__rgt aside right-sidebar-result col-sm-3">
            
            <div id="return_filter" class="sidebar-filters clrlist listview clrhm">
                <div id="comparsion_box" style="display: none;">
                    <h2>Compare Properties</h2>
                    
                    <div class="filter-box ">
                        <ul id="compare_ul">                   
                        
                        </ul>
                       <a onclick="empty_list_compare()" rel="nofollow" class="remove-all" href="JAVASCRIPT:void(0);">Remove all</a>
                         <a style="display: none;" id="compare_button" rel="nofollow" class="remove-all" href="{{ url('compare_listing') }}">Compare all</a>
                    </div>
                 </div>

             <?php /* ?>       
            <div>
                
                <h2>You refined by...</h2>

                <div class="filter-box ">
                    <ul>                   
                        <li>
                            <b>Last Minute Specials</b>
                            <span>
                                <a rel="nofollow" href="https://www.shoresummerrentals.com/rating-5/orderby-city">x</a>
                            </span>
                        </li>
                        
                                                
                        <li>
                            <b>5 Stars</b>
                            <span>
                                <a rel="nofollow" href="https://www.shoresummerrentals.com/listing/orderby-city/specials-lastminutespecials">x</a>
                            </span>
                        </li>
                        
                                                
                    </ul>
                                        <a rel="nofollow" class="remove-all" href="https://www.shoresummerrentals.com/listing/search">Remove all</a>
                </div>
                
            </div>
            <?php */ ?>
        
        
        <h2>Refine Search</h2>
        <div class="">
      
            <div class="panel-group accordion-plus" id="accordion">
                      <?php if(isset($statesData) && !empty($statesData)){ ?>
                      <div class="panel panel-default active">
                        <div class="panel-heading sidebar-refine-search firstSidebar-refine">
                          <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#side_collapse1">
                              State
                            </a>
                          </h4>
                        </div>
                        <div id="side_collapse1" class="panel-collapse collapse in">
                          <div class="panel-body sidebar-refine-search-body">
                            <ul id="list_location" class="item-select">
                                <?php 
                                
                                 if(!empty($states_in_result))
                                 {
                                     foreach($statesData as $key=>$val){ 
                                    if(in_array($val->id,$states_in_result))
                                    {
                                        
                                        ?>
                                        <li>
                                            <a rel="nofollow" href="{{ url().'/united-states/'.str_replace(' ', '-',strtolower($val->name)) }}"><?php echo $val->name; ?></a>
                                        </li>
                                <?php  }
                                    else
                                    {
                                        continue;
                                    }
                                   } 
                                 }
                                ?>
                            
                            </ul>
                          </div>
                        </div>
                      </div>
                      <?php } ?>
                      <?php if(isset($cityData) && !empty($cityData)){ ?>
                      <div class="panel panel-default active">
                        <div class="panel-heading">
                          <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#side_collapse1">
                              City
                            </a>
                          </h4>
                        </div>
                        <div id="side_collapse1" class="panel-collapse collapse in">
                          <div class="panel-body sidebar-refine-search-body">
                            <ul id="list_location" class="item-select">
                                <?php 
                                
                                     foreach($cityData as $key=>$val){ 
                                    
                                        
                                        if(in_array($val->location_id,$cities_in_result))
                                    {
                                        
                                        ?>
                                        <li>
                                            <a rel="nofollow" href="<?php echo  url($val->full_friendly_url); ?> "><?php echo $val->title; ?></a>
                                        </li>
                                <?php }
                                else
                                    {
                                        continue;
                                    }
                                   } 
                                 
                                ?>
                            
                            </ul>
                          </div>
                        </div>
                      </div>
                      <?php } ?>
                     <?php 
                        $specials = Functions::getSpecilas();
                        if(isset($specials) && !empty($specials))
                        { 
                           ?>
                            
                      <div class="panel panel-default">
                        <div class="panel-heading">
                          <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse2" class="collapsed">
                              Special Offers
                            </a>
                          </h4>
                        </div>
                        <div id="collapse2" class="panel-collapse collapse ">
                            <div class="panel-body sidebar-refine-search-body">
                            
                                <ul id="list_specials" class="item-select">
                                <?php
                            $tempPageurl =   url('');
                             
                            $viewUrl = '/view';
                            $featureUrl = '/feature';
                            $specialsUrl = '/specials';
                            if(strpos($tempPageurl, $viewUrl) !== false)
                            {
                                $explodedTemp = explode($viewUrl,$tempPageurl);
                                $tempPageurl = $explodedTemp[0];
                            }
                            elseif(strpos($tempPageurl, $featureUrl) !== false)
                            {
                                $explodedTemp = explode($featureUrl,$tempPageurl);
                                $tempPageurl = $explodedTemp[0];
                            }
                            elseif(strpos($tempPageurl, $specialsUrl) !== false)
                            {
                                $explodedTemp = explode($specialsUrl,$tempPageurl);
                                $tempPageurl = $explodedTemp[0];
                            }
                            
                            $thisLink='';
                            foreach($specials as $val){ 
                                 $filter_special_name = str_replace(" ", '', strtolower($val->name));
                                 switch ($filter_special_name) {
                                    case 'beachfrontrentals':
                                        
                                            $thisLink = $tempPageurl.'/view-ocean_front';
                                       
                                        break;                                    
                                    case 'bayfrontrentals':
                                        
                                            $thisLink = $tempPageurl.'/view-ocean_front';
                                        
                                        break;
                                    case 'petfriendlyrentals':
                                        
                                             $thisLink = $tempPageurl.'/feature-petsallowed';
                                        
                                        
                                       
                                        break;
                                    default:
                                        
                                             $thisLink = $tempPageurl.'/specials-'.$filter_special_name;
                                        
                                        
                                        break;
                                }
                                ?>
                                    <li><a rel="nofollow" href="<?php echo $thisLink; ?>"><?php echo $val->name; ?></a></li>

                                    <?php } ?>
                                    
                                
                                </ul>             
                            
                            
                            </div>
                        </div>
                      </div>
                      <?php } ?>
                
                      <div class="panel panel-default">
                        <div class="panel-heading">
                          <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#side_collapse3" class="collapsed">
                              Rating
                            </a>
                          </h4>
                        </div>
                        <div id="side_collapse3" class="panel-collapse collapse ">
                          <div class="panel-body sidebar-refine-search-body">
                                <ul id="list_rating" class="item-select">
                                    <?php 
                                    
                                    $querystring=url('search');
                                   if(!empty($rating_in_result))
                                   {
                                    if(in_array(5,$rating_in_result)){
                                        
                                        if (strpos($querystring, '/rating-5') !== false)
                                        {
                                            $querystring_modified = $querystring;
                                        }
                                        else
                                        {
                                            $querystring_modified = $querystring.'/rating-5';
                                        }
                                    ?>
                                        <li class="active">
                                            <a rel="nofollow" href="{{ $querystring_modified }}">
                                            5 Stars                        </a>
                                        </li>
                                    <?php  } ?>
                                    
                                    <?php if(in_array(4,$rating_in_result)){
                                        
                                         if (strpos($querystring, '/rating-4') !== false)
                                        {
                                            $querystring_modified = $querystring;
                                        }
                                        else
                                        {
                                            $querystring_modified = $querystring.'/rating-4';
                                        }
                                        
                                    ?>
                                        <li class="active">
                                            <a rel="nofollow" href="{{ $querystring_modified }}">
                                            4 Stars                        </a>
                                        </li>
                                    <?php  } ?>
                                    
                                    
                                    <?php if(in_array(3,$rating_in_result)){
                                          if (strpos($querystring, '/rating-3') !== false)
                                        {
                                            $querystring_modified = $querystring;
                                        }
                                        else
                                        {
                                            $querystring_modified = $querystring.'/rating-3';
                                        }
                                        
                                    ?>
                                        <li class="active">
                                            <a rel="nofollow" href="{{ $querystring_modified }}">
                                            3 Stars                        </a>
                                        </li>
                                    <?php  } ?>
                                    
                                    <?php if(in_array(2,$rating_in_result)){
                                        
                                            if (strpos($querystring, '/rating-2') !== false)
                                        {
                                            $querystring_modified = $querystring;
                                        }
                                        else
                                        {
                                            $querystring_modified = $querystring.'/rating-2';
                                        }
                                        
                                        
                                    ?>
                                        <li class="active">
                                            <a rel="nofollow" href="{{ $querystring_modified }}">
                                            2 Stars                        </a>
                                        </li>
                                    <?php  } ?>
                                    
                                    <?php if(in_array(1,$rating_in_result)){
                                        
                                          if (strpos($querystring, '/rating-1') !== false)
                                        {
                                            $querystring_modified = $querystring;
                                        }
                                        else
                                        {
                                            $querystring_modified = $querystring.'/rating-1';
                                        }
                                        
                                        
                                        
                                    ?>
                                        <li class="active">
                                            <a rel="nofollow" href="{{ $querystring_modified }}">
                                            1 Stars                        </a>
                                        </li>
                                    <?php  } 
                                    
                                   }
                                    ?>
                                
                                </ul>
                          </div>
                        </div>
                      </div>
                </div>
            
        
                <input type="hidden" id="maxcomparison_itens" value="5">
        <input type="hidden" id="comparison_url" value="http://www.shoresummerrentals.com/listing/compare_listing.php">
        </div>

                </div>
                
            </div>
            <?php } ?>
            <!-- Filters search side ends -->
 
 <section class="featured-area mt30 imgzoom-hover">
                <div class="container">
                    
                    <div class="hed crossline"><h2>FEATURED RENTAL PROPERTIES</h2><hr/></div>
                @foreach($featured_images as $frp_image)    
                <div class="featured-box col-sm-3 cols-5">
                        <div class="featured__inr">
                            <div class="featured__img">
                                <img src="<?php echo config('params.imagesPath').$frp_image->prefix.'photo_'.$frp_image->image_id.".".strtolower($frp_image->type);?>" alt="<?php echo $frp_image->title; ?>" />
                            </div>
                            <div class="featured__cont">
                                <ul>
                                    <li><?php echo $frp_image->location_4_title; ?></li>
                                    <li><?php echo $frp_image->location_3_title; ?></li>
                                    <li><?php echo $frp_image->bedroom; ?> Beds | Bathrooms <?php echo $frp_image->bathroom; ?> | Sleeps <?php echo $frp_image->sleeps; ?></li>
                                </ul>
                                <div class="lnk-btn more-btn"><a href="{{ url('listing/'.$frp_image->rental_id.'.html') }}">View Details</a></div>
                            </div>
                        </div>
                </div>
                @endforeach
                
            
            
                </div>
            </section>
    </section>
    <script>
  function view_quickInfo(row,resource_id)
  {
    var TOKEN = '{{ csrf_token() }}';
     $.ajax({
        url: "{{ url('quick-info') }}",
        type: 'POST',
        data: {resource_id: resource_id,_token:TOKEN},
        dataType: 'JSON',
        success: function (data) {
            var obj = (data);
            var newContent='';
            if(!$.isEmptyObject(obj))
            {
            $.each( obj, function( key, value ) {
                    if(value.type==='' || value.type=== null) 
                    {
                        $('#quick_info_image').attr('src','{{ asset("images/noimage.jpg") }}' );
                    }
                    else
                    {
                        $('#quick_info_image').attr('src','<?php echo config('params.imagesPath');?>'+value.prefix+'photo_'+value.image_id+'.'+value.type.toLowerCase());    
    
                    }
                    
                    $('#quick_info_title').html(value.title);
                    $('#quick_info_location').html('Location: '+value.address+' '+value.address2+', '+value.location_4_title+', '+value.location_3_title+', '+value.zip_code)
                    $('#quick_info_rentalId').html(resource_id);
                    
                    if(value.min_week===null || value.min_week==='')
                    {
                        $('#quick_info_rate').html('');
                        
                    }
                    else
                    {
                        $('#quick_info_rate').html("<strong> "+'<?php echo $currency[Config::get('params.currency_default')]['symbol']; ?>'+value.min_week+"- "+'<?php echo $currency[Config::get('params.currency_default')]['symbol']; ?>'+value.max_week+"</strong><br> per week");
                    }
                    
                    if(value.required_stay===null || value.required_stay==='')
                    {
                        $('#quick_info_stay').html('');
                    }
                    else
                    {
                        $('#quick_info_stay').html(value.required_stay+' minimum stay');
                    }
                });
                
               
                
//                
//                $("#location_4").html(newContent);
            
            }
            else if($.isEmptyObject(obj))
            { 
              //  newContent='<option value="">Choose state first</option>';
//                $("#location_4").html(newContent);
                return false;
            }
            else if(obj==0)
            { 
              //  newContent='<option value="">Choose state first</option>';
//                $("#location_4").html(newContent);
                return false;
            }
        }
    });
    $('#quickInfo').modal('show');
  }
  
  function compare_listing(result_count,row,resource_id,title)
  {
  
        
        setCookie('result_count',result_count);
        if($("#compare_"+row).prop('checked') == true){
            //alert(getCookie('title_'+row));
            
            
            setCookie('row_'+row,row);
            setCookie('title_'+row,title);
            setCookie('resource_id_'+row,resource_id);
            
            //alert(resource_id);
            // truncate title 
            var title_temp = title;
            if(title_temp.length>25)
            {
               var shortText = jQuery.trim(title_temp).substring(0, 25)
                .trim(this) + "..."; 
            }
            else
            {
                var shortText = title_temp;
            }
            
            
            
            
            var html_temp = ' <li id="compare_li_'+row+'" >'+
                                '<b>'+shortText+'</b>'+
                                '<span>'+
                                    '<a onclick="remove_signle_item('+row+')" rel="nofollow" href="JAVASCRIPT:void(0);">x</a>'+
                                '</span>'+
                            '</li>';
            $('#compare_ul').append(html_temp);
            // get total chiuld of ul compare list
            var count = parseInt($("#compare_ul").children().length);
            setCookie('total_list',count);
            if(count>0)
            {
                $('#comparsion_box').css('display','block');
            }
            else
            {
                $('#comparsion_box').css('display','none');
            }
            if(count>1)
            {
                $('#compare_button').css('display','block');
            }
            else if(count<=1)
            {
               $('#compare_button').css('display','none');  
            } 
            var total_list = getCookie('total_list');
            
            if(total_list>5)
            {
                setCookie('row_'+row,'');
                setCookie('title_'+row,'');
                setCookie('resource_id_'+row,'');
                $('#compare_li_'+row).remove();
                setCookie('total_list',(count-1));
                $("#compare_"+row).prop('checked',false);
            }
            if(total_list>5)
            {
                
                row_number  = '<?php echo isset($result_count)?$result_count:0; ?>';
                //alert(row_number);
                
                for(var i=0;i<row_number;i++)
                {
                    if($("#compare_"+i).prop('checked') == false)
                    {
                        $("#compare_"+i).attr('disabled',true);
                    }
                }
                
                
                
            }
            else
            {
                row_number  = '<?php echo isset($result_count)?$result_count:0; ?>';
                 
                for(var i=0;i<row_number;i++)
                {
                    if($("#compare_"+i).prop('checked') == false)
                    {
                        $("#compare_"+i).attr('disabled',false);
                    }
                }
              
            }
                       
        }
        else
        {
            if(checkCookie(row,resource_id,title))
            {
                
                count = parseInt($("#compare_ul").children().length);
                $('#compare_li_'+row).remove();
                
                count = count-1;
                setCookie('total_list',count);
                row_number  = '<?php echo isset($result_count)?$result_count:0; ?>';
                for(var i=0;i<row_number;i++)
                {
                    if($("#compare_"+i).prop('checked') == false)
                    {
                        $("#compare_"+i).attr('disabled',false);
                    }
                }
                if(count>1)
                {
                    $('#compare_button').css('display','block');
                }
                else if(count<=1)
                {
                   $('#compare_button').css('display','none');  
                } 
                if(count>0)
                {
                    $('#comparsion_box').css('display','block');
                }
                else
                {
                    $('#comparsion_box').css('display','none');
                }
            }
           
        }
       

    
         return true;
    
  }
  
  function checkTotalList()
  {
        var total_list = getCookie('total_list');
       if(total_list>5)
        {
            alert('exceed limit '+total_list);
            row_number  = '<?php echo isset($result_count)?$result_count:0; ?>';
            for(var i=0;i<row_number;i++)
            {
                if($("#compare_"+i).prop('checked') == false)
                {
                    $("#compare_"+i).attr('disabled',true);
                }
            }
            
           return true; 
        }
        else
        {
            row_number  = '<?php echo isset($result_count)?$result_count:0; ?>';
            for(var i=0;i<row_number;i++)
            {
                if($("#compare_"+i).prop('checked') == false)
                {
                    $("#compare_"+i).attr('disabled',false);
                }
            }
            return true;
        }
  }

function setCookie(cname,cvalue) {
    
   
    document.cookie = cname+"="+cvalue+";path=/";
}
function expireCookie(cname,cvalue) {
    
   
    document.cookie = cname+"="+cvalue+";expires=Thu, 01 Jan 1970 00:00:00 UTC ";
}



function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function checkCookie(row,resource_id,title) {
    var row_temp=getCookie("row_"+row);
    if (row_temp != "") {
        setCookie('row_'+row,'');
        
    }
    
    var resource_id_temp=getCookie("resource_id_"+row);
    if (resource_id_temp != "") {
        setCookie('resource_id_'+row,'');
        
    }
    
    var title_temp=getCookie("title_"+row);
    if (title_temp != "") {
        setCookie('title_'+row,'');
        
    }
    return true;
}

function listCookiesCompare() {
   row_number  = '<?php echo isset($result_count)?$result_count:0; ?>';
   for(var i=0;i<row_number;i++)
   {
        if(getCookie('row_'+i)!='')
        {
            
           var resource_id_temp =  getCookie('resource_id_'+i);
            
            // truncate title 
            var title_temp = getCookie('title_'+i);
            if(title_temp.length>20)
            {
               var shortText = jQuery.trim(title_temp).substring(0, 20)
                .trim(this) + "..."; 
            }
            else
            {
                var shortText = title_temp;
            }
           
            if(resource_id_temp==$('#compare_'+getCookie('row_'+i)).attr('value'))
            {
                $('#compare_'+getCookie('row_'+i)).attr('checked','checked');
               
            }
             var html_temp = ' <li id="compare_li_'+i+'" >'+
                '<b>'+shortText+'</b>'+
                '<span>'+
                '<a onclick="remove_signle_item('+i+')" rel="nofollow" href="JAVASCRIPT:void(0);">x</a>'+
                '</span>'+
                '</li>';
                $('#compare_ul').append(html_temp);
            
        }
        else
        {
           //$('#compare_'+getCookie('row_'+i)).attr('checked','');
           continue;
        }
        
   }
   return true;
    
}
function remove_signle_item(row)
{
        count = parseInt($("#compare_ul").children().length);
        $('#compare_li_'+row).remove();
        if(count>-1)
        {
            count = count-1;
        }
         if(count>0)
        {
            $('#comparsion_box').css('display','block');
        }
        else
        {
            $('#comparsion_box').css('display','none');
        }
        if(count>1)
        {
            $('#compare_button').css('display','block');
        }
        else if(count<=1)
        {
           $('#compare_button').css('display','none');  
        } 
    
        setCookie('row_'+row,'');
        setCookie('title_'+row,'');
        setCookie('resource_id_'+row,'');
        setCookie('total_list',(count-1));
        $("#compare_"+row).prop('checked',false);
}
function empty_list_compare()
{
    
    row_number  = '<?php echo isset($result_count)?$result_count:0; ?>';
     for(var i=0;i<row_number;i++)
     {
        var row_temp=getCookie("row_"+i);
        if (row_temp != "") {
            setCookie('row_'+i,'');

        }
        
        var resource_id_temp=getCookie("resource_id_"+i);
        if (resource_id_temp != "") {
            setCookie('resource_id_'+i,'');
            
        }
        
        var title_temp=getCookie("title_"+i);
        if (title_temp != "") {
            setCookie('title_'+i,'');
            
        }
        
        if($("#compare_"+i).prop('checked') == true)
        {
            $("#compare_"+i).attr('checked',false);
        }
     }
    $('#compare_ul').empty();
    $('#comparsion_box').css('display','none');


}
// A $( document ).ready() block.
$( document ).ready(function() {
    listCookiesCompare();
    
    var total_list = parseInt($("#compare_ul").children().length);
    if(total_list>0)
    {
        $('#comparsion_box').css('display','block');
    }
    else
    {
        $('#comparsion_box').css('display','none');
    }
    if(total_list>1)
    {
        $('#compare_button').css('display','block');
    }
    else if(total_list<=1)
    {
       $('#compare_button').css('display','none');  
    } 
    if(total_list>4)
    {
     
        row_number  = '<?php echo isset($result_count)?$result_count:0; ?>';
        for(var i=0;i<row_number;i++)
        {
            if($("#compare_"+i).prop('checked') == false)
            {
                $("#compare_"+i).attr('disabled',true);
            }
        }
        
        
    }
    else
    {
        row_number  = '<?php echo isset($result_count)?$result_count:0; ?>';
        for(var i=0;i<row_number;i++)
        {
            if($("#compare_"+i).prop('checked') == false)
            {
                $("#compare_"+i).attr('disabled',false);
            }
        }
      
    }
});

  </script>
<script>
function CheckForm(){
        var form_get = jQuery('#search_form').serializeArray();

        jQuery.ajax({
          url: "{{ url('advancedsearch-checkform') }}",
          context: document.body,
          data: form_get,
          success: function(redirect_url){
            //console.log(redirect_url);
                window.location.href = redirect_url.return_url;
          }
        });
        return false;
    }



</script>
@endsection



