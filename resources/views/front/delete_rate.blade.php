@extends('front')
<?php
use App\Functions\Functions;
?>

@section('content')

	<link rel="stylesheet" href="{{ asset('front/css/bootstrap.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('front/css/members.min.css') }}" />

     <!--link rel="stylesheet" href="{{ asset('front/css/bootstrap-responsive.min.css') }}" /-->
     

    
    
	<div class="deleteRate-page container members">

        <div class="rate-page-container container-fluid ">

	<div>

								
		
  <nav class="minor-nav">
        <ul>
            <li>
                <a href="<?php echo url('sponsors/listing/'.$listing_id.''); ?>">Listing Information</a>
            </li>

                            <li >
               <a href="<?php echo url('sponsors/backlinks/'.$listing_id.''); ?>">Backlink</a>
                </li>
            
            
            <li>
                <a class="active"  href="<?php echo url('sponsors/rate/'.$listing_id.''); ?>">Rate</a>
            </li>
            
            <li>
                <a href="<?php echo url('sponsors/specials/'.$listing_id.''); ?>">Specials</a>
            </li>
        </ul>
    </nav>

		<div id="well-text">
                    <h2>Delete Rates - </h2>
                </div>


      <p class="informationMessage">Are you sure you want to delete this rate?</p>
        <div class="baseForm">

            <form name="delete_rates" action="<?php echo url('sponsors/delete_rates'); ?>" method="post" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="rate_id" value="<?php echo $id; ?>" />
                <input type="hidden" name="listing_id" value="<?php echo $listing_id; ?>">
                <input type="hidden" name="letter" value="" />
                <input type="hidden" name="screen" value="" />
                				
				
	
	
                <button type="submit" value="Submit" class="input-button-form">Delete</button>
				<a href="<?php echo url('sponsors/rate/'.$listing_id.''); ?>" class="input-button-form">Cancel</a>
				
            </form>

            	
        </div>

        
	</div>
	</div>
	</div><!-- Close container-fluid div -->

	
@endsection