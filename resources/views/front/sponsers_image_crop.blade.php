@extends('popup')

@section('content')

	<link rel="stylesheet" href="{{ asset('front/css/structure.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('front/css/members.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('front/css/bootstrap-responsive.min.css') }}" />
   
   
    <!--link rel="stylesheet" href="https://www.shoresummerrentals.com/scripts/jquery/jcrop/css/jquery.Jcrop.css" type="text/css">
        <script type="text/javascript" src="https://www.shoresummerrentals.com/scripts/jquery/jcrop/js/jquery.Jcrop.js"></script>-->
            
<style>
.container.posrel {
    margin-top: 110px;
    min-height: 220px;
}
</style>
<div class="container posrel">                    
        

                        
                
  <form id="uploadimage" name="uploadimage" action="<?php echo url('sponsors/popupCropImage'); ?>" method="post" class="frmEmail" enctype="multipart/form-data">

                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="pop_type" value="uploadimage">

                        <input type="hidden" name="item_type" value="listing">
                        <input type="hidden" name="main" value="<?php echo $main; ?>">
                        <input type="hidden" name="level" value="<?php echo $level; ?>">
                        <input type="hidden" name="temp" value="<?php echo $temp; ?>">
                        <input type="hidden" name="gallery_item_id" id="gallery_item_id" value="<?php echo $gallery_item_id; ?>">
                        <input type="hidden" name="gallery_id" id="gallery_id" value="<?php echo $gallery_id; ?>">
                        <input type="hidden" name="image_id" id="image_id" value="<?php echo $image_id; ?>">
                        <input type="hidden" name="thumb_id" id="thumb_id" value="<?php echo $thumb_id; ?>">
                        <input type="hidden" name="item_id" id="item_id" value="<?php echo $item_id; ?>">
                        <input type="hidden" name="captions" id="captions" value="<?php echo $captions; ?>">
                        <input type="hidden" name="x1" value="<?php echo $width; ?>" id="x1">
                        <input type="hidden" name="y1" value="0" id="y1">
                      	<input type="hidden" name="x2" value="<?php echo $thumbWidthItem; ?>" id="x2" />
                   		<input type="hidden" name="y2" value="<<?php echo $thumbHeightItem; ?>" id="y2" />
                   		<input type="hidden" name="w" value="<<?php echo $thumbWidthItem ;?>" id="w" />
                    	<input type="hidden" name="h" value="<<?php echo $thumbHeightItem; ?>" id="h" />
                        <input type="hidden" name="gallery_hash" value="<?php echo $gallery_hash; ?>">
                        <input type="hidden" name="domain_id" value="<?php echo $domain_id; ?>">
                        <input type="hidden" name="order" value="">
 <?php                       
    $current_large_image_width = $width;
	$current_large_image_height = $height;

	//d($current_large_image_height,1);die;
	
    if (config('params.GALLERY_FREE_RATIO') == "on") {
        $aspectratio = 0;

    } else {
        $mdc = image_getMdc(config('params.IMAGE_SLIDER_WIDTH'), config('params.IMAGE_SLIDER_HEIGHT'));


        $aspectratio = (config('params.IMAGE_SLIDER_WIDTH')/$mdc) / (config('params.IMAGE_SLIDER_HEIGHT')/$mdc);
        if ($item_type == "listing"){
			$mdc = image_getMdc(config('params.IMAGE_LISTING_FULL_WIDTH'), config('params.IMAGE_LISTING_FULL_HEIGHT'));
        	$aspectratio = (config('params.IMAGE_LISTING_FULL_WIDTH')/$mdc) / (config('params.IMAGE_LISTING_FULL_HEIGHT')/$mdc);
		}
    }
	
?>

	<script type="text/javascript">

	



		function sendFile () {
			setValue('yes');
			$('#uploadimage').submit();
			$('#loadingBar').css('display', '');
			$('#imgFile').css('display', 'none');
			$('#submitButton').css('display', 'none');
		}

		

		function setValue(op){
			if (document.getElementById("uploadThumbID"))
				document.getElementById("uploadThumbID").value=op;
		}
		
		function noUpload(){
			parent.$.fancybox.close();
		}
		
		<?php if($current_large_image_width){?>

			$(function(){
					
				imgWidth = <?php echo $current_large_image_width; ?>;
				imgHeight = <?php echo $current_large_image_height; ?>;
				$('#imgUpload').Jcrop({
					onChange: showPreview,
					onSelect: showPreview,
					bgColor: 'transparent',
					setSelect:   [ 0, 0, imgWidth, imgHeight ],
					aspectRatio: <?php echo $aspectratio; ?>,
					boxWidth: 400,
					boxHeight: 400,
					fullImageWidth: imgWidth,
					fullImageHeight: imgHeight
				});

			});

			function showPreview(coords){
				$('#x1').val(coords.x);
				$('#y1').val(coords.y);
				$('#x2').val(coords.x2);
				$('#y2').val(coords.y2);
				$('#w').val(coords.w);
				$('#h').val(coords.h);
                
                if (coords.w > 0 || coords.h > 0) {
                
                    var rx = <?php echo $thumbWidthItem; ?> / coords.w;
                    var ry = <?php echo $thumbHeightItem; ?> / coords.h;

                    $('#thumbnailPreview').css({
                        width: Math.round(rx * <?php echo $current_large_image_width; ?>) + 'px',
                        height: Math.round(ry * <?php echo $current_large_image_height; ?>) + 'px',
                        marginLeft: '-' + Math.round(rx * coords.x) + 'px',
                        marginTop: '-' + Math.round(ry * coords.y) + 'px'
                    });
                    
                }
			}


		<?php } ?>

	</script>

	
            
            <table border="0" cellpadding="0" cellspacing="0" class="standardForm">
			<tr>
				<td class="TaddImage">
				<img id="imgUpload" src="<?php echo isset($large_image_location)?url($large_image_location):''; ?>"  />	
				</td>
                
                <?php if (config('params.GALLERY_FREE_RATIO') != "on") { ?>
				<td class="TaddPreview">
					<div style="border:1px #e5e5e5 solid; float:left; left:15px; position:relative; overflow:hidden; width:<?php echo isset($thumbWidthItem)?$thumbWidthItem:'0'; ?>px; height:<?php echo isset($thumbHeightItem)?$thumbHeightItem:'0'; ?>px;" 
                    >
						<img src="<?php echo isset($large_image_location)?url($large_image_location):''; ?>" style="position: relative;" id="thumbnailPreview" />
					</div>
				</td>
                <?php } ?>
			</tr>	
		</table>
</div>
        
 

 
        <style>
      
.TaddImage, .TaddPreview {
    width: 400px;
    overflow: hidden;
}

.TaddImage  img,.TaddPreview img{
    width: 100% !important;
    height:auto !important;
}



.jcrop-holder img, img.jcrop-preview {
    width: 400px!important;
    height: auto !important;
}

.jcrop-holder img, img.jcrop-preview {
    width: 400px !important;
    height: auto !important;
}


</style>
        
        
        <br/>
		<input type='hidden' name="thumbWidthItem" value="<?php echo $thumbWidthItem; ?>">
        <input type='hidden' name="thumbHeightItem" value="<?php echo $thumbHeightItem; ?>">
        
        
	<table border="0" cellpadding="0" cellspacing="0" class="standardForm">
					<tbody><tr>
				<input type="hidden" id="uploadThumbID" name="uploadThumb" value="">
				<td class="TimageFile" width="140" height="30"><span>Image File</span>:</td>
				<td class="IimageFile">
					<input type="file" id="imgFile" name="image" size="30" onchange="sendFile();"  class="<?php echo ((!$thumb_id) ? "inputExplode": "inputExplodeSmall");?>" >
					<div id="loadingBar" align="center" style="display: none;">
						<img src="https://www.shoresummerrentals.com/images/content/img_loading_bar.gif">
					</div>
				</td>
			</tr>
					</tbody></table>
	
	<table border="0" cellpadding="0" cellspacing="0" class="standardForm">
		<tbody><tr>
			<td height="24" colspan="2" class="formButton">
				<div id="overlay"></div>
				<p class="input-button-form">
				<button id="submitButton UploadImage" type="submit" onclick="parent.fancyclose();" value="Upload">Submit</button>
				</p>
			</td>
		</tr>
	</tbody></table>                    </form>
                    <script>
                        $(document).ready(function() {
                            $('#uploadimage').submit(function() {
                                var index = $(parent.document).find('.ui-sortable li').length + 1;
                                $('#uploadimage').find('[name="order"]').val(index);
                            });
                        });
                    </script>
                                           <center>
                                <img style="width: 70%;" src="https://www.shoresummerrentals.com/custom/domain_1/images/upload_images_instruct.jpg">                            </center>
<!--                        <p style="color:red;margin-left:150px;margin-top:5px;">--><!--</p>-->
<!--                        <p style="color:red;margin-left:150px;margin-top:5px;">--><!--</p>-->
<!--                        <p style="color:red;margin-left:150px;margin-top:5px;">--><!--</p>-->
<!--                        <p style="color:red;margin-left:150px;margin-top:5px;">--><!--</p>-->
<!--                        <p style="color:red;margin-left:150px;margin-top:5px;">--><!--</p>-->
<!--                        <p style="color:red;margin-left:150px;margin-top:5px;">--><!--</p>-->
<!--                        <p style="color:red;margin-left:150px;margin-top:5px;">--><!--</p>-->
                                    
                </div>
	
	<style>
.modal-content>h2 {
    margin-bottom: 5px;
}
</style>

</div>
             
              @endsection