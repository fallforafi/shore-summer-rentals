<?php
use App\Functions\Functions;
?>

@extends('front')

@section('content')

	<link rel="stylesheet" href="{{ asset('front/css/bootstrap.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('front/css/members.min.css') }}" />
       <link rel="stylesheet" href="{{ asset('front/css/structure.min.css') }}" />
	<link href="https://www.shoresummerrentals.com/custom/domain_1/theme/default/content_custom.min.css" rel="stylesheet" type="text/css" media="all">
    <!--link rel="stylesheet" href="{{ asset('front/css/bootstrap-responsive.min.css') }}" /-->
     
     
    
    
	 <div class="account-container container members">

        <div class="container-fluid">

    <div>

	
 @if (session('message'))
    <p class=" {{ session('message_style') }}">
       <?php  echo session('message'); ?>
    </p>
@endif


            
    <p class="successMessage" id="messageEmail" style="display:none">Your activation e-mail has been sent. Please, check your inbox.</p>
    <p class="errorMessage" id="messageEmailError" style="display:none"></p>
    
            
        <div class="member-form">
            
<?php 
   $accountID=Functions::sess_getAccountIdFromSession();
   $is_sponsor=$account->is_sponsor;
   $notify_traffic_listing=$account->notify_traffic_listing;
   $notify_sms=$account->notify_sms;
   $username=$account->username;
   $active=$account->active;
   $first_name=$contact->first_name;
   $last_name=$contact->last_name;
   $company=$contact->company;
   $address=$contact->address;
   $address2=$contact->address2;
   $city=$contact->city;
   $state=$contact->state;
   $zip=$contact->zip;
   $phone=$contact->phone;
   $fax=$contact->fax;
   $email=$contact->email;


?>
            <form name="account" id="account" method="post" action="<?php echo url('sponsors/updateAccount'); ?>">

                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="account_id" value="{{ $account_id }}" />
                <input type="hidden" name="is_sponsor" value="{{ (isset($is_sponsor))?$is_sponsor:'' }}" />
                
                <div>
                    
                            
   <!-- <script language="javascript" type="text/javascript" src="https://www.shoresummerrentals.com/scripts/checkusername.js"></script>-->

  <script type="text/javascript">
      function checkUsername(username, path, option, current_acc) {

    expression = /(&\B)|(^&)|(#\B)|(^#)/;
    if (expression.exec(username)) {
        username = 'erro';
    }

    $.get("<?php echo url('sponsors/search_username'); ?>", {
        option: option,
        username: username,
        path: path,
        current_acc : current_acc
    }, function (response) {
        $('#checkUsername').html(response);
    });

}
  </script> 
    <script language="javascript" type="text/javascript" src="https://www.shoresummerrentals.com/scripts/activationEmail.js"></script>
                          
        <div id="change-email">

        <div class="left textright">
            <h2>Account Username</h2>
            <span>This needs to be the email address of the account holder. This is how WE will contact you but not necessarily how you will be contacted by the renters, that information needs to be entered on the next page. This is the email to use when logging into your account.</span>
        </div>

        <div class="right">

            <div class="cont_100">

                <label>Account Holder E-mail <a href="javascript: void(0);">* <span>Required field</span></a></label>

                <div class="checking">

                    <input type="text" name="username" value="{{ $username }}" maxlength="80" onblur="checkUsername(this.value, '<?php  echo url(); ?>', 'members',  
                    {{($accountID ? $accountID : 0) }}); populateField(this.value,'email');"/>
                    <label id="checkUsername">&nbsp;</label>


              <?php if ($active == "y") { ?>
                        <span class="positive"> <img src="https://www.shoresummerrentals.com/images/ico-approve.png"/> Activated</span>
                    <?php } else { ?>
                        <span class="negative"> <img src="https://www.shoresummerrentals.com/images/ico-deny.png"/> Un-Activate<a href="javascript: void(0);" onclick="sendEmailActivation({{ $accountID }});">Active</a></span>
                    <?php } ?>
              
                    <input type="hidden" name="active" value="{{ $active }}" />        
             
              <img id="loadingEmail" src="https://www.shoresummerrentals.com/images/img_loading.gif" width="15px;" style="display: none;" />
                    <input type="hidden" name="active" value="{{ $active }}" />        

                </div>

            </div>

        </div>

    </div>

    
    
    <div id="change-password">

        <div class="left textright">

            <h2>Change Password</h2>
            <span>You can change your password at any time.</span>

        </div>

        <div class="right">

            <div class="cont_100">

                <label>Current Password</label>

                <div class="checking">
                    <input type="password" name="current_password" class="input-form-account"  />
                </div>

            </div>

            <div class="cont_50">
                <label>New Password</label>
                <input type="password" name="password" maxlength="50"  />
            </div>

            <div class="cont_50">
                <label>Retype New Password</label>
                <input type="password" name="retype_password"  />
            </div>

        </div>

    </div>

                            
                    
    <div id="contact-info">

        <div class="left textright">
            <h2>Account Holder Contact Information</h2>
            <span>Again, this is how WE will contact you but not necessarily how you will be contacted by the renters, that information needs to be entered on the next page.</span>
        </div>

        <div class="right">

            <div class="cont_50">
                <label>Account Holder First Name <a href="javascript: void(0);">* <span>Required field</span></a></label>
                <input type="text" name="first_name" value="{{ $first_name }}" />
            </div>

            <div class="cont_50">
                <label>Account Holder Last Name <a href="javascript: void(0);">* <span>Required field</span></a></label>
                <input type="text" name="last_name" value="{{ $last_name }}" />
            </div>

            <div class="cont_100">
                <label>Account Holder Company</label>
                <input type="text" name="company" value="{{ $company }}" />
            </div>

            <div class="cont_100">
                <label>Account Holder Mailing Address1 <em>Street Address, P.O. box</em></label>
                <input type="text" name="address" value="{{ $address }}" maxlength="50" />
            </div>

            <div class="cont_100">
                <label>Account Holder Mailing Address2 <em>Apartment, suite, unit, building, floor, etc.</em></label>
                <input type="text" name="address2" value="{{ $address2 }}" maxlength="50" />
            </div>
            
                        
            <div class="cont_50">
                <label>Account Holder Mailing City</label>
                <input type="text" name="city" value="{{ $city }}" />    
            </div>

            <div class="cont_50">
                <label>Account Holder Mailing State</label>
                <input type="text" name="state" value="{{ $state }}" />
            </div>

            <div class="cont_30">
                <label>Account Holder Mailing Zip</label>
                <input type="text" name="zip" value="{{ $zip }}" />
            </div>

            <div class="cont_30">
                <label>Account Holder Phone</label>
                <input type="text" name="phone" value="{{ $phone }}" />
            </div>

            <div class="cont_30">
                <label>Account Holder Fax</label>
                <input type="text" name="fax" value="{{ $fax }}" />
            </div>

                        
            
           <input type="hidden" name="email" id="email" value="{{ $email }}" />
                    </div>

    </div>

    
    <div id="settings">

        <div class="left textright">
            <h2>Settings</h2> 				
            <span>Check your preferences</span>
        </div>

        <div class="right">

            
                        <div class="cont_checkbox">	 				
                <input type="checkbox" name="notify_traffic_listing" id="notify_traffic_listing"  {{ ($notify_traffic_listing == "y" || $notify_traffic_listing == "on") ? "checked=\"checked\"": "" }}/>
                <label for="notify_traffic_listing" >Notify me about listing reviews and listing traffic</label>
            </div>
                        
                        <div class="cont_checkbox">                     
                <input type="checkbox" name="notify_sms" id="notify_sms" {{ ($notify_sms == "y" || $notify_sms == "on") ? "checked=\"checked\"": "" }} />
                <label for="notify_sms" >Notify me about property inquire by SMS</label>
            </div>
                        
                        
            
        </div>
        
    </div>

    
    
                    <div id="form-action">
                        
                        <div class="left "></div>
                        
                        <div class="right">
                            <p class="standardButton">
                                <button type="submit" value="Submit">Submit</button>
                                <button type="button" value="Cancel" onclick="window.location ='<?php echo url('sponsors'); ?>';">Cancel</button>
                            </p>
                        </div>
                    </div>

                </div>
                
            </form>
            
        </div>
 
		
	</div>
 

            </div><!-- Close container-fluid div -->
            
        </div><!-- Close container div -->



		 <script type="text/javascript"> 
        
        if ($("#myChart").length) {
            //This will get the first returned node in the jQuery collection.
            var ctx = $("#myChart").get(0).getContext("2d");
        }

        function initializeDashboard() {
            $(".dial").knob({
                readOnly:   true,
                fgColor:    "#2980b9",
                bgColor:    "#DEE1E3",
                fontWeight: 300,
                thickness:  .2,
                width:      70,
                height:     70
            });

            $(".status, .floating-tip, .alert-new, #item_renewal").tooltip({
                animation: true,
                placement: "top"
            });
             
            if ($("#myChart").length) {
                //Get context with jQuery - using jQuery's .get() method.
                ctx = $("#myChart").get(0).getContext("2d");
                loadChart();
            }
        }

        $(function() {
            $("#alert").fadeOut(5000);
            initializeDashboard();
        });
        
        function showReply(id) {
            $('#review_reply'+id).css('display', '');
            $('#link_reply'+id).css('display', 'none');
            $('#cancel_reply'+id).css('display', '');
        }
        
        function hideReply(id) {
            $('#review_reply'+id).css('display', 'none');
            $('#link_reply'+id).css('display', '');
            $('#cancel_reply'+id).css('display', 'none');
        }
        
        function showLead(id) {
            $('#lead_reply'+id).css('display', '');
            $('#link_lead'+id).css('display', 'none');
            $('#cancel_lead'+id).css('display', '');
        }
        
        function hideLead(id) {
            $('#lead_reply'+id).css('display', 'none');
            $('#link_lead'+id).css('display', '');
            $('#cancel_lead'+id).css('display', 'none');
        }
        
        function saveReply(id) {
            $("#submitReply"+id).css("cursor", "default");
            $("#submitReply"+id).prop("disabled", "disabled");
            $("#submitReply"+id).html('Wait, Loading...');
            
            $.post("https://www.shoresummerrentals.com/sponsors/ajax.php", $("#formReply"+id).serialize(), function(data) {
                if (data == "ok") {
                    $("#msgReviewE"+id).css("display", "none");
                    $("#msgReviewS"+id).css("display", "");
                    $("#msgReviewS"+id).fadeOut(5000);
                } else {
                    $("#msgReviewE"+id).css("display", "");
                    $("#msgReviewS"+id).css("display", "none");
                }
                $("#submitReply"+id).html('Submit');
                $("#submitReply"+id).prop("disabled", "");
                $("#submitReply"+id).css("cursor", "pointer");
            });
        }
        
        function saveLead(id) {
            $("#submitLead"+id).css("cursor", "default");
            $("#submitLead"+id).prop("disabled", "disabled");
            $("#submitLead"+id).html('Wait, Loading...');
            
            $.post("https://www.shoresummerrentals.com/sponsors/ajax.php", $("#formLead"+id).serialize(), function(data) {
                if (data == "ok") {
                    $("#msgLeadE"+id).css("display", "none");
                    $("#msgLeadS"+id).css("display", "");
                    $("#msgLeadS"+id).fadeOut(5000);
                    setTimeout("leadBox('hide', "+id+");", 4000);
                    $("#title_replied"+id).css("display", "none");
                    $("#new_replied"+id).css("display", "");
                } else {
                    $("#msgLeadE"+id).html(data);
                    $("#msgLeadE"+id).css("display", "");
                    $("#msgLeadS"+id).css("display", "none");
                }
                $("#submitLead"+id).html('Submit');
                $("#submitLead"+id).prop("disabled", "");
                $("#submitLead"+id).css("cursor", "pointer");
            });
        }
        
        function reviewBox(option, id) {
            $("#reviews-list").children(".item-review").children(".review-detail").stop(true,true).slideUp();
            $("#reviews-list").children(".item-review").children(".review-summary").stop(true,true).slideDown().removeClass("new");
            if (option == "show") {
                $("#review-summary-"+id).slideUp(); 
                $("#review-detail-"+id).slideDown();
                setItemAsViewed("review", id);
            } else {
                $("#review-summary-"+id).slideDown(); 
                $("#review-detail-"+id).slideUp();
            }            
        }
        
        function leadBox(option, id) {
            $("#leads-list").children(".item-review").children(".review-detail").stop(true,true).slideUp();
            $("#leads-list").children(".item-review").children(".review-summary").stop(true,true).slideDown().removeClass("new");
            if (option == "show") {
                $("#lead-summary-"+id).slideUp(); 
                $("#lead-detail-"+id).slideDown();
                setItemAsViewed("lead", id);
            } else {
                $("#lead-summary-"+id).slideDown(); 
                $("#lead-detail-"+id).slideUp();
            }            
        }
        
        function dealBox(option, id) {
            $("#deals-list").children(".item-review").children(".review-detail").stop(true,true).slideUp();
            $("#deals-list").children(".item-review").children(".review-summary").stop(true,true).slideDown();
            if (option == "show") {
                $("#deal-summary-"+id).slideUp(); 
                $("#deal-detail-"+id).slideDown();
            } else {
                $("#deal-summary-"+id).slideDown(); 
                $("#deal-detail-"+id).slideUp();
            }            
        }
        
        function changeDealStatus(option, id, promocode) {
            $.post("https://www.shoresummerrentals.com/sponsors/deal/deal.php",{action: option, promotion_id: promocode}, function() {
                if (option == "freeUpDeal") {
                    $("#label_used"+id).css("display", "");
                } else {
                    $("#label_used"+id).css("display", "none");
                }
            });
        }
        
        function setItemAsViewed(type, id) {
            $.post("https://www.shoresummerrentals.com/sponsors/ajax.php", {
                ajax_type: 'setItemAsViewed',
                type: type,
                id: id
            }, function () {});
        }
       
        function loadDashboard(item_type, item_id) {
            $.post("<?php echo url('loadDashboard');?>", {
                ajax_type: 'load_dashboard',
                item_type: item_type,
                item_id: item_id,
                _token:'{{ csrf_token() }}'
            }, function (ret) {
                $(".webitem").removeClass("active");
                $("#"+item_type+"_"+item_id).addClass("active");
                scrollPage('#float_layer');
                $("#dashboard").hide().html(ret).fadeIn(800);
                initializeDashboard();
            });
        }

        function selectLegend(option, id, chartdata) {
            var countVisible = 0;
            
            if (option == "viewALL") {
                
                if ( $(".legend-ALL").hasClass("isvisible")) {
//                    $(".legend-ALL").removeClass("isvisible");
//                    $("#optionLegend > li > i").removeClass("checked");
//                    $("#optionLegend > li").removeClass("isvisible");
//                    $("#controlLegend > li").remove();
                } else {
                    countVisible = 2;
                    $("#optionLegend > li > i").addClass("checked");                   
                    $(".legend-ALL").addClass("isvisible");
                    $("#optionLegend > li").not(".isvisible").clone().appendTo("#controlLegend");
                    $("#optionLegend > li").addClass("isvisible");
                }
            } else {
                id: id
                chartdata: chartdata
                $newlegend = $(".legend-"+id).clone();

                if ($(".legend-"+id).hasClass("isvisible")) {
                    
                    //Check if there's at least one other legend selected to prevent empty chart
                    $('#optionLegend li').each(function() {
                        if ($(this).hasClass("isvisible")) {
                            countVisible++;
                        }
                    });

                    if (countVisible > 1) {
                        $(".legend-"+id).children("i").removeClass("checked");
                        $(".legend-"+id).removeClass("isvisible");
                        $("#controlLegend").children(".legend-"+id).remove();
                        $(".legend-ALL").children("i").removeClass("checked");
                        $(".legend-ALL").removeClass("isvisible");
                    }
                } else {
                    countVisible = 2;
                    $(".legend-"+id).children("i").addClass("checked");
                    $(".legend-"+id).addClass("isvisible");
                    $newlegend.appendTo("#controlLegend");
                }
            }
            if (countVisible > 1) {
                controlChart();
            }
        }
                
        function loadChart() {
            var data = {
                labels : chartLabels,
                datasets : initialReport
            };
            var steps = 5;
            var max = maxInitialReport;
            if (max < steps) {
                steps = max;
            }
            var options = {
                bezierCurve : false,
                scaleOverride: true,
                scaleSteps: steps,
                scaleStepWidth: Math.ceil(max / steps),
                scaleStartValue: 0
            };
            ctx = $("#myChart").get(0).getContext("2d");
            new Chart(ctx).Line(data, options);        
        }
       
        function controlChart() {
            
            var datasets = new Array();
            var max = 0;
            var thisHighest = 0;
            $('#optionLegend li').each(function() {
                if ($(this).hasClass("isvisible")) {
                    var reportType = $(this).attr('report');
                    if (reportType) {
                        datasets.push(window[reportType]);
                        thisHighest = Math.max.apply(Math, window[reportType].data);
                        if (thisHighest > max) {
                            max = thisHighest;
                        }
                    }
                }
            });
            
            var steps = 5;
            if (max < steps) {
                steps = max;
            }
            var options = {
                bezierCurve : false,
                scaleOverride: true,
                scaleSteps: steps,
                scaleStepWidth: Math.ceil(max / steps),
                scaleStartValue: 0
            };

            var data = {
                labels : chartLabels,
                datasets : datasets
            };
            ctx = $("#myChart").get(0).getContext("2d");
            new Chart(ctx).Line(data, options);  

        }


function scrollPage(position_id){
    
	if(!position_id){
		$position_id = '#resultsMap';
	}else {
		$position_id = position_id;
	}
	//jQuery('html,body').animate({scrollTop: jQuery($position_id).offset().top},'slow');
    jQuery('html, body').animate({ scrollTop: jQuery('#dashboard').offset().top }, 1500);
}
    </script>
	<script src="{{ asset('/front/js/Chart.js') }}"></script>		
@endsection