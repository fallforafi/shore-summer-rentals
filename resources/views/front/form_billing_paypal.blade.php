<?php 
use App\Functions\Functions;
use App\Functions\Contact;




$itemCount = 1;
$cart_items='';
if (isset($bill_info["listings"])) foreach($bill_info["listings"] as $id => $info) { 


if (config('params.PAYPALRECURRING_FEATURE') == "on") {
					$itemspaid_id[] = "l:".$id;
				} else {
					$cart_items .= "
						<input type=\"hidden\" name=\"item_name_".$itemCount."\" value=\"".$info["title"]."\" />
						<input type=\"hidden\" name=\"item_number_".$itemCount."\" value=\"listing:$id\" />
						<input type=\"hidden\" name=\"amount_".$itemCount."\" value=\"".$info["total_fee"]."\" />";
					if ($payment_tax_status == "on") $cart_items .= "<input type=\"hidden\" name=\"tax_rate_".$itemCount."\" value=\"".$payment_tax_value."\" />";
					else $cart_items .= "<input type=\"hidden\" name=\"tax_rate_".$itemCount."\" value=\"0\" />";
				}

				$itemCount++;
 }

if (isset($bill_info["badges"])) foreach($bill_info["badges"] as $id => $info) { 

	if (config('params.PAYPALRECURRING_FEATURE') == "on") {
					$itemspaid_id[] = "l:".$id;
				} else {
					$cart_items .= "
						<input type=\"hidden\" name=\"item_name_".$itemCount."\" value=\"".$info["name"]."\" />
						<input type=\"hidden\" name=\"item_number_".$itemCount."\" value=\"listing:$id\" />
						<input type=\"hidden\" name=\"amount_".$itemCount."\" value=\"".$info["total_fee"]."\" />";
					if ($payment_tax_status == "on") $cart_items .= "<input type=\"hidden\" name=\"tax_rate_".$itemCount."\" value=\"".$payment_tax_value."\" />";
					else $cart_items .= "<input type=\"hidden\" name=\"tax_rate_".$itemCount."\" value=\"0\" />";
				}

				$itemCount++;
 }

 if (isset($bill_info["custominvoices"])) foreach($bill_info["custominvoices"] as $id => $info) { 

	if (config('params.PAYPALRECURRING_FEATURE') == "on") {
					$itemspaid_id[] = "l:".$id;
				} else {
					$cart_items .= "
						<input type=\"hidden\" name=\"item_name_".$itemCount."\" value=\"".$info["name"]."\" />
						<input type=\"hidden\" name=\"item_number_".$itemCount."\" value=\"listing:$id\" />
						<input type=\"hidden\" name=\"amount_".$itemCount."\" value=\"".$info["total_fee"]."\" />";
					if ($payment_tax_status == "on") $cart_items .= "<input type=\"hidden\" name=\"tax_rate_".$itemCount."\" value=\"".$payment_tax_value."\" />";
					else $cart_items .= "<input type=\"hidden\" name=\"tax_rate_".$itemCount."\" value=\"0\" />";
				}

				$itemCount++;
 }

if (!$stop_payment) {

	$sess_id=Functions::sess_getAccountIdFromSession();
	$contactObj = new Contact();
	$contactObj->Contact($sess_id);
	
	$amount = str_replace(",", ".", $bill_info["total_bill"]);
	$subtotal = $amount;
	if($payment_tax_status == "on"){
	$amount = Functions::payment_calculateTax($amount, $payment_tax_value);
	}
	$paypal_return = 'paypal_return';
	$paypal_cancel_return ='paypal_cancel_return';
	$paypal_notify_url = 'paypal_notify_url';
	$paypal_account_id = Functions::sess_getAccountIdFromSession();
	$paypal_first_name = $contactObj->first_name;
	$paypal_last_name = $contactObj->last_name;
	$paypal_email = $contactObj->email;
	$paypal_address = $contactObj->address;
	$paypal_city = $contactObj->city;
	$paypal_state = $contactObj->state;
	$paypal_zip = $contactObj->zip;
	$phone = str_replace(".", "", $contactObj->phone);
	$phone = str_replace("-", "", $phone);
	$phone = str_replace(" ", "", $phone);
	$paypal_night_phone_a = Functions::string_substr($phone, 0, 3);
	$paypal_night_phone_b = Functions::string_substr($phone, 3, 3);
	$paypal_night_phone_c = Functions::string_substr($phone, 6);

?>

<script language="javascript" type="text/javascript">
					<!--
					function submitOrder() {
						document.getElementById("paypalbutton").disabled = true;
						document.paypalform.submit();
					}
					//-->
				</script>
				<!-- www.paypal.com-->
<form name="paypalform" target="_self" action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post">

					<div style="display: none;">

						<input type="hidden" name="cmd" value="_ext-enter">

					@if (config('params.PAYPALRECURRING_FEATURE') == "on"):
							<input type="hidden" name="redirect_cmd" value="_xclick-subscriptions" />
					@else 
							<input type="hidden" name="redirect_cmd" value="_cart" />
							<input type="hidden" name="upload"       value="1" />
				    @endif	

						<!--maria@shoresummerrentals.com-->
						<input type="hidden" name="business" value="test@demodirectory.com">
						<input type="hidden" name="no_note" value="1">
						<input type="hidden" name="no_shipping" value="1">
						<input type="hidden" name="currency_code" value="USD">
						<input type="hidden" name="lc" value="US">
						<input type="hidden" name="cbt" value="Finish">
						<input type="hidden" name="rm" value="2">
					 	<input type="hidden" name="return" value="{{ $paypal_return }}" />
						<input type="hidden" name="cancel_return" value="{{ $paypal_cancel_return }}" />
						<input type="hidden" name="notify_url"    value="{{ $paypal_notify_url }}" />
						<input type="hidden" name="page_style"    value="PayPal" />
						@if ($payment_tax_status == "on"):
						<input type="hidden" name="custom" value="account_id:{{ $paypal_account_id }}::ip:{{ $_SERVER['REMOTE_ADDR'] }}::tax:{{ Functions::payment_calculateTax($subtotal, $payment_tax_value, true, false) }}::domain_id:1::package_id:{{ isset($package_id) }}" />						
						@else 
							<input type="hidden" name="custom" value="account_id:{{ $paypal_account_id }}::ip:{{ $_SERVER['REMOTE_ADDR'] }}::domain_id:1::package_id:{{ isset($package_id) }}" />	
				    	@endif	
						
						
						<?php echo $cart_items; ?>

						<input type="hidden" name="first_name" value="{{ $paypal_first_name }}" />
						<input type="hidden" name="last_name"  value="{{ $paypal_last_name}} " />
						<input type="hidden" name="email"      value="{{$paypal_email}}" />
						<input type="hidden" name="address1"   value="{{$paypal_address}}" />
						<input type="hidden" name="city"       value="{{$paypal_city}}" />
						<input type="hidden" name="state"      value="{{$paypal_state}}" />
						<input type="hidden" name="zip"        value="{{$paypal_zip}}" />

						<input type="hidden" name="night_phone_a" value="{{$paypal_night_phone_a}}" />
						<input type="hidden" name="night_phone_b" value="{{$paypal_night_phone_b}}" />
						<input type="hidden" name="night_phone_c" value="{{$paypal_night_phone_c}}" />

					

					</div>
		
		<?php 
		if ($payment_process == "signup") {?>
                        

                        <div class="blockcontinue cont_100">
                    
                    <div class="cont_70 empty"></div>
                    
                    <div class="cont_30 ">
                        
                        <p class="bt-highlight checkoutButton biggerbutton">
                       <button type="button" id="paypalbutton" onclick="submitOrder();">Place Order and Continue</button>

                        </p>
                        
                    </div>
                </div>
                    <?php    
                    } else { ?>
							<p class="standardButton paymentButton">
							<button type="button" id="paypalbutton" onclick="submitOrder();">Pay By PayPal</button>
						</p>
					<?php } ?>

						
					
				</form>				
<?php } ?>				