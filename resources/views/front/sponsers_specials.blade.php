<?php
use App\Functions\Functions;
use App\Functions\ListingChoice;
?>
@extends('front')

@section('content')


<?php

$msg = '';
    if (session('message') ) {
        switch (session('message')) {
            case '1':
                $msg = '<p class="successMessage">The badges have been saved.</p>';
                break;
            case '2':
                $msg = '<p class="successMessage">The rate has been saved.</p>';
                break;
            case '3':
                $msg = '<p class="errorMessage">An error occurring when deleting the rate.</p>';
                break;
            case '4':
                $msg = '<p class="successMessage">The rate has been removed.</p>';
                break;
        }
    }


?>

    <link rel="stylesheet" href="{{ asset('front/css/members.min.css') }}" />
        <link rel="stylesheet" href="{{ asset('front/css/structure.min.css') }}" />

     <!--link rel="stylesheet" href="{{ asset('front/css/bootstrap-responsive.min.css') }}" /-->
     
     
      
    
	<div class="special-page-container container members">

        <div class="container-fluid">

	<script>
		function selectBadge(id) {
			var remove = false;
			$("#selectedBadges option").each(function() {
				if ( $(this).val() == id ) {
					remove = true; 
					$(this).remove();
					return remove;
				}
			});

			if ( remove ) {
				$('#badgeImage' + id).css({'border-color':'transparent'});
				$('#linkBaged' + id).text('Select this choice');
			} else {
				$('#selectedBadges').append("<option value='" + id + "'>" + id + "</option>");
				$('#badgeImage' + id).css({'border-color':'#FF5200','border-width':'3px','border-style':'solid'});
				$('#linkBaged' + id).text('Remove this choice');
			}
		}

		function JS_submit() {
			$("#selectedBadges option").attr('selected', true);
			document.badges.submit();
		}
	</script>

	<div>
		
	<nav class="minor-nav">
        <ul>
            <li>
                <a  href="<?php echo url('sponsors/listing/'.$listing_id.''); ?>">Listing Information</a>
            </li>

                            <li >
               <a href="<?php echo url('sponsors/backlinks/'.$listing_id.''); ?>">Backlink</a>
                </li>
            
            
            <li>
                <a href="<?php echo url('sponsors/rate/'.$listing_id.''); ?>">Rate</a>
            </li>
            
            <li>
                <a class="active"  href="<?php echo url('sponsors/badges/'.$listing_id.''); ?>">Specials</a>
            </li>
        </ul>
    </nav>			
		
 

		
	    <div class="baseForm">



	        <form name="badges" action="<?php echo url('sponsors/specialStore'); ?>" method="post" enctype="multipart/form-data">
                         <input type="hidden" name="_token" value="{{ csrf_token() }}">

	            <input type="hidden" name="id" value="<?php echo $listing_id; ?>" />
	            <input type="hidden" name="letter" value="" />
	            <input type="hidden" name="screen" value="" />

                <?php  echo $msg; ?>
	            				
								
									    <table align="standard-table" cellpadding="0" cellspacing="0">
        <tr id="table_badges">
            <th colspan="2">
                <div class="badgeBox">
                    <ul>
@foreach($editorChoices as $editor)
<?php

                        $hasThisBadge = false;
                        $listingChoices= new ListingChoice();
    $listingChoice=$listingChoices->SponserListingChoice($editor->editor_choice_id,$listing_id);

                        if (isset($listingChoice[0]->listing_id)) {
                            $hasThisBadge = true;
                        }
                        ?>

                  <li id="badge{{ $editor->editor_choice_id }}">

                       <?php if ($hasThisBadge ) { ?>    

                       <img id="badgeImage{{ $editor->editor_choice_id }}"  src="<?php echo config('params.imagesPath').$editor->prefix.'photo_'.$editor->image_id.".".strtolower($editor->type);?>"  alt="{{ $editor->name }}" title ="{{ $editor->name }}"  border="0" />   
                     
                 <?php  } else { ?>                                
                        
                    <a class="image" href="javascript:void(0);" onclick="selectBadge({{ $editor->editor_choice_id }})">
                <img id="badgeImage{{ $editor->editor_choice_id }}"  src="<?php echo config('params.imagesPath').$editor->prefix.'photo_'.$editor->image_id.".".strtolower($editor->type);?>"  alt="{{ $editor->name }}" title ="{{ $editor->name }}"  border="0" />  </a>



                 <?php } ?>         
                 <?php if ($hasThisBadge ) { 

                    $listingChoices->renewal_date=$listingChoice[0]->renewal_date;
                     $listingChoices->editor_choice_id=$listingChoice[0]->editor_choice_id;
                    ?>

                    
                                <div class="special-info">
                               

                    <?php if ($listingChoice[0]->renewal_date != "0000-00-00" ) { ?>
                                            <p>Renewal Date: 
                                            <?php echo Functions::format_date($listingChoice[0]->renewal_date);?></p> &nbsp; - &nbsp; 
                                        <?php } else { ?>
                                            <p>Make Your Payment:
                                             <?php echo Functions::format_date($listingChoices->getNextRenewalDate())?></p> &nbsp; - &nbsp; 
                                        <?php } ?>

                                        <p><?php echo $listingChoices->check_status($listingChoice[0]->status);?></p>

                                        <?php if( $listingChoices->needToCheckOut() && ( 
                                            $listingChoice[0]->renewal_date != "0000-00-00" ) ) { ?>
                                            <p><a href="<?php echo url('sponsors/billing'); ?>">Make Your Payment</a></p>
                                        <?php } elseif( $listingChoices->needToCheckOut() || ( $listingChoice[0]->status != "A" ) ) { ?>
                                            <p><a href="<?php echo url('sponsors/billing'); ?>">Make Your Payment</a></p>
                                        <?php } ?>

                                    
                                </div>

                                <div class="special-description">
                                    <label for="description">Description:</label>
                                    <textarea id="description" 
                                    name="description[<?php echo $listingChoice[0]->id; ?>]" rows="4"><?php echo $listingChoice[0]->description; ?></textarea>
                                </div>
                                
                            <?php } else {?>




                                <div class="special-select">

                                    <p class="getRenewalPeriod pull-left">
                                        <strong>${{ $editor->price }}</strong>
                                        per <strong>{{ $editor->renewal_period }}</strong>                                    </p>
                                    <a class="btn pull-right" id="linkBaged{{ $editor->editor_choice_id }}" href="javascript:void(0);" onclick="selectBadge({{ $editor->editor_choice_id }})">Select this choice</a>
                                
                                </div>

                                <?php  } ?>

                            
                    </li>
                        
@endforeach

                                        </ul>
                </div>
            </th>
        </tr>
    </table>

    <select name="choice[]" id="selectedBadges" multiple style="display:none;"></select>        
			    
			    <button type="button" value="Submit" class="input-button-form" onclick="JS_submit()">Save</button>
	            <button type="button" value="Cancel" class="input-button-form" onclick="document.getElementById('formlistingbadgescancel').submit();">Cancel</button>
			
	        </form>

	        <form id="formlistingbadgescancel" action="https://www.shoresummerrentals.com/sponsors/listing/index.php" method="get">
	            <input type="hidden" name="letter" value="" />
	            <input type="hidden" name="screen" value="" />
	            	        </form>

	    </div>

	</div>
	</div>
	</div>



		 <script type="text/javascript">
        
        if ($("#myChart").length) {
            //This will get the first returned node in the jQuery collection.
            var ctx = $("#myChart").get(0).getContext("2d");
        }

        function initializeDashboard() {
            $(".dial").knob({
                readOnly:   true,
                fgColor:    "#2980b9",
                bgColor:    "#DEE1E3",
                fontWeight: 300,
                thickness:  .2,
                width:      70,
                height:     70
            });

            $(".status, .floating-tip, .alert-new, #item_renewal").tooltip({
                animation: true,
                placement: "top"
            });
             
            if ($("#myChart").length) {
                //Get context with jQuery - using jQuery's .get() method.
                ctx = $("#myChart").get(0).getContext("2d");
                loadChart();
            }
        }

        $(function() {
            $("#alert").fadeOut(5000);
            initializeDashboard();
        });
        
        function showReply(id) {
            $('#review_reply'+id).css('display', '');
            $('#link_reply'+id).css('display', 'none');
            $('#cancel_reply'+id).css('display', '');
        }
        
        function hideReply(id) {
            $('#review_reply'+id).css('display', 'none');
            $('#link_reply'+id).css('display', '');
            $('#cancel_reply'+id).css('display', 'none');
        }
        
        function showLead(id) {
            $('#lead_reply'+id).css('display', '');
            $('#link_lead'+id).css('display', 'none');
            $('#cancel_lead'+id).css('display', '');
        }
        
        function hideLead(id) {
            $('#lead_reply'+id).css('display', 'none');
            $('#link_lead'+id).css('display', '');
            $('#cancel_lead'+id).css('display', 'none');
        }
        
        function saveReply(id) {
            $("#submitReply"+id).css("cursor", "default");
            $("#submitReply"+id).prop("disabled", "disabled");
            $("#submitReply"+id).html('Wait, Loading...');
            
            $.post("https://www.shoresummerrentals.com/sponsors/ajax.php", $("#formReply"+id).serialize(), function(data) {
                if (data == "ok") {
                    $("#msgReviewE"+id).css("display", "none");
                    $("#msgReviewS"+id).css("display", "");
                    $("#msgReviewS"+id).fadeOut(5000);
                } else {
                    $("#msgReviewE"+id).css("display", "");
                    $("#msgReviewS"+id).css("display", "none");
                }
                $("#submitReply"+id).html('Submit');
                $("#submitReply"+id).prop("disabled", "");
                $("#submitReply"+id).css("cursor", "pointer");
            });
        }
        
        function saveLead(id) {
            $("#submitLead"+id).css("cursor", "default");
            $("#submitLead"+id).prop("disabled", "disabled");
            $("#submitLead"+id).html('Wait, Loading...');
            
            $.post("https://www.shoresummerrentals.com/sponsors/ajax.php", $("#formLead"+id).serialize(), function(data) {
                if (data == "ok") {
                    $("#msgLeadE"+id).css("display", "none");
                    $("#msgLeadS"+id).css("display", "");
                    $("#msgLeadS"+id).fadeOut(5000);
                    setTimeout("leadBox('hide', "+id+");", 4000);
                    $("#title_replied"+id).css("display", "none");
                    $("#new_replied"+id).css("display", "");
                } else {
                    $("#msgLeadE"+id).html(data);
                    $("#msgLeadE"+id).css("display", "");
                    $("#msgLeadS"+id).css("display", "none");
                }
                $("#submitLead"+id).html('Submit');
                $("#submitLead"+id).prop("disabled", "");
                $("#submitLead"+id).css("cursor", "pointer");
            });
        }
        
        function reviewBox(option, id) {
            $("#reviews-list").children(".item-review").children(".review-detail").stop(true,true).slideUp();
            $("#reviews-list").children(".item-review").children(".review-summary").stop(true,true).slideDown().removeClass("new");
            if (option == "show") {
                $("#review-summary-"+id).slideUp(); 
                $("#review-detail-"+id).slideDown();
                setItemAsViewed("review", id);
            } else {
                $("#review-summary-"+id).slideDown(); 
                $("#review-detail-"+id).slideUp();
            }            
        }
        
        function leadBox(option, id) {
            $("#leads-list").children(".item-review").children(".review-detail").stop(true,true).slideUp();
            $("#leads-list").children(".item-review").children(".review-summary").stop(true,true).slideDown().removeClass("new");
            if (option == "show") {
                $("#lead-summary-"+id).slideUp(); 
                $("#lead-detail-"+id).slideDown();
                setItemAsViewed("lead", id);
            } else {
                $("#lead-summary-"+id).slideDown(); 
                $("#lead-detail-"+id).slideUp();
            }            
        }
        
        function dealBox(option, id) {
            $("#deals-list").children(".item-review").children(".review-detail").stop(true,true).slideUp();
            $("#deals-list").children(".item-review").children(".review-summary").stop(true,true).slideDown();
            if (option == "show") {
                $("#deal-summary-"+id).slideUp(); 
                $("#deal-detail-"+id).slideDown();
            } else {
                $("#deal-summary-"+id).slideDown(); 
                $("#deal-detail-"+id).slideUp();
            }            
        }
        
        function changeDealStatus(option, id, promocode) {
            $.post("https://www.shoresummerrentals.com/sponsors/deal/deal.php",{action: option, promotion_id: promocode}, function() {
                if (option == "freeUpDeal") {
                    $("#label_used"+id).css("display", "");
                } else {
                    $("#label_used"+id).css("display", "none");
                }
            });
        }
        
        function setItemAsViewed(type, id) {
            $.post("https://www.shoresummerrentals.com/sponsors/ajax.php", {
                ajax_type: 'setItemAsViewed',
                type: type,
                id: id
            }, function () {});
        }
       
        function loadDashboard(item_type, item_id) {
            $.post("<?php echo url('loadDashboard');?>", {
                ajax_type: 'load_dashboard',
                item_type: item_type,
                item_id: item_id,
                _token:'{{ csrf_token() }}'
            }, function (ret) {
                $(".webitem").removeClass("active");
                $("#"+item_type+"_"+item_id).addClass("active");
                scrollPage('#float_layer');
                $("#dashboard").hide().html(ret).fadeIn(800);
                initializeDashboard();
            });
        }

        function selectLegend(option, id, chartdata) {
            var countVisible = 0;
            
            if (option == "viewALL") {
                
                if ( $(".legend-ALL").hasClass("isvisible")) {
//                    $(".legend-ALL").removeClass("isvisible");
//                    $("#optionLegend > li > i").removeClass("checked");
//                    $("#optionLegend > li").removeClass("isvisible");
//                    $("#controlLegend > li").remove();
                } else {
                    countVisible = 2;
                    $("#optionLegend > li > i").addClass("checked");                   
                    $(".legend-ALL").addClass("isvisible");
                    $("#optionLegend > li").not(".isvisible").clone().appendTo("#controlLegend");
                    $("#optionLegend > li").addClass("isvisible");
                }
            } else {
                id: id
                chartdata: chartdata
                $newlegend = $(".legend-"+id).clone();

                if ($(".legend-"+id).hasClass("isvisible")) {
                    
                    //Check if there's at least one other legend selected to prevent empty chart
                    $('#optionLegend li').each(function() {
                        if ($(this).hasClass("isvisible")) {
                            countVisible++;
                        }
                    });

                    if (countVisible > 1) {
                        $(".legend-"+id).children("i").removeClass("checked");
                        $(".legend-"+id).removeClass("isvisible");
                        $("#controlLegend").children(".legend-"+id).remove();
                        $(".legend-ALL").children("i").removeClass("checked");
                        $(".legend-ALL").removeClass("isvisible");
                    }
                } else {
                    countVisible = 2;
                    $(".legend-"+id).children("i").addClass("checked");
                    $(".legend-"+id).addClass("isvisible");
                    $newlegend.appendTo("#controlLegend");
                }
            }
            if (countVisible > 1) {
                controlChart();
            }
        }
                
        function loadChart() {
            var data = {
                labels : chartLabels,
                datasets : initialReport
            };
            var steps = 5;
            var max = maxInitialReport;
            if (max < steps) {
                steps = max;
            }
            var options = {
                bezierCurve : false,
                scaleOverride: true,
                scaleSteps: steps,
                scaleStepWidth: Math.ceil(max / steps),
                scaleStartValue: 0
            };
            ctx = $("#myChart").get(0).getContext("2d");
            new Chart(ctx).Line(data, options);        
        }
       
        function controlChart() {
            
            var datasets = new Array();
            var max = 0;
            var thisHighest = 0;
            $('#optionLegend li').each(function() {
                if ($(this).hasClass("isvisible")) {
                    var reportType = $(this).attr('report');
                    if (reportType) {
                        datasets.push(window[reportType]);
                        thisHighest = Math.max.apply(Math, window[reportType].data);
                        if (thisHighest > max) {
                            max = thisHighest;
                        }
                    }
                }
            });
            
            var steps = 5;
            if (max < steps) {
                steps = max;
            }
            var options = {
                bezierCurve : false,
                scaleOverride: true,
                scaleSteps: steps,
                scaleStepWidth: Math.ceil(max / steps),
                scaleStartValue: 0
            };

            var data = {
                labels : chartLabels,
                datasets : datasets
            };
            ctx = $("#myChart").get(0).getContext("2d");
            new Chart(ctx).Line(data, options);  

        }


function scrollPage(position_id){
    
	if(!position_id){
		$position_id = '#resultsMap';
	}else {
		$position_id = position_id;
	}
	//jQuery('html,body').animate({scrollTop: jQuery($position_id).offset().top},'slow');
    jQuery('html, body').animate({ scrollTop: jQuery('#dashboard').offset().top }, 1500);
}
    </script>
	<script src="{{ asset('/front/js/Chart.js') }}"></script>		
@endsection