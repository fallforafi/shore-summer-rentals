@extends('front')

@section('content')

	<link rel="stylesheet" href="{{ asset('front/css/bootstrap.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('front/css/members.min.css') }}" />
	<link rel="stylesheet" href="{{ asset('front/css/content_custom.min.css') }}" />
	<link href="https://www.shoresummerrentals.com/custom/domain_1/theme/default/content_custom.min.css" rel="stylesheet" type="text/css" media="all">
    <!--link rel="stylesheet" href="{{ asset('front/css/bootstrap-responsive.min.css') }}" /-->
 
 <div class="container">
<div class="order">

                <div id="billing-detail">

                    <div class="left textright">
                        <h3>Billing Detail</h3>
                        <p>A quick overview of how much you will be charged.</p>
                    </div>

                    <div class="right">
                        
                        
                        <table class="standard-tableTOPBLUE">
                            <tbody><tr>
                                <th>
                                    Listing                                </th>
                                
                                <th>Level</th>
                                
                                                                    
                                                                        
                                                                    
                                                                    
                                                                    
                                                                    <th>Total</th>
                                                                
                            </tr>
                            
                            <tr>
                                <td>
                                    farhan                                                                    </td>
                                
                                <td>Bronze</td>
                                
                                                                
                                                                        
                                                                    
                                
                                                                    
                                                                <td>
                                    $ 199.00                                </td>
                                                                    
                            </tr>
                            
                                                        
                        </tbody></table>
                        
                                                
                        
                    </div>

                </div>
                
                
                <div id="payment-method">

                    <div class="left textright">
                        <h3>Payment</h3>
                        <p>Make your payment and finish your order.</p>
                    </div>

                    <div class="right">
                        <div class="option">
                            
                
				<script language="javascript" type="text/javascript">
					<!--
					function submitOrder() {
						document.getElementById("authorizebutton").disabled = true;
						document.authorizeform.submit();
					}
					//-->
				</script>

				<form name="authorizeform" target="_self" action="https://www.shoresummerrentals.com/sponsors/signup/processpayment.php?payment_method=authorize" method="post">

					<div style="display: none;">
						
						<input type="hidden" name="pay" value="1">
						<input type="hidden" name="x_tax_amount" value="0">
						<input type="hidden" name="x_subtotal_amount" value="199.00">
						<input type="hidden" name="x_amount" value="199.00">
						<input type="hidden" name="x_invoice_num" value="057989f0243af7">
						<input type="hidden" name="x_cust_id" value="5602">
						<input type="hidden" name="x_listing_ids" value="7363">
						<input type="hidden" name="x_listing_amounts" value="199.00">
						<input type="hidden" name="x_badges_ids" value="">
						<input type="hidden" name="x_badges_amounts" value="">
						<input type="hidden" name="x_event_ids" value="">
						<input type="hidden" name="x_event_amounts" value="">
						<input type="hidden" name="x_banner_ids" value="">
						<input type="hidden" name="x_banner_amounts" value="">
						<input type="hidden" name="x_classified_ids" value="">
						<input type="hidden" name="x_classified_amounts" value="">
						<input type="hidden" name="x_article_ids" value="">
						<input type="hidden" name="x_article_amounts" value="">
						<input type="hidden" name="x_custominvoice_ids" value="">
						<input type="hidden" name="x_custominvoice_amounts" value="">
						<input type="hidden" name="x_domain_id" value="1">
						<input type="hidden" name="x_package_id" value="">

					</div>

					<table align="center" width="95%" cellpadding="2" cellspacing="2" class="standard-table payment-authorize">
						<tbody><tr>
							<th colspan="2" class="standard-tabletitle">Billing Info</th>
						</tr>
						<tr>
							<th>* Card Number:</th>
							<td><input type="text" name="x_card_num" value=""></td>
						</tr>
						<tr>
							<th>* Card Expire date:</th>
							<td><input type="text" name="x_exp_date" value=""><span>mm/yyyy</span></td>
						</tr>
						<tr>
							<th>Card Code:</th>
							<td><input type="text" name="x_card_code" value=""></td>
						</tr>
						<tr>
							<th colspan="2" class="standard-tabletitle">Customer Info
						</th></tr>
						<tr>
							<th>First Name:</th>
							<td><input type="text" name="x_first_name" value="Farhan"></td>
						</tr>
						<tr>
							<th>Last Name:</th>
							<td><input type="text" name="x_last_name" value="Rifat"></td>
						</tr>
						<tr>
							<th>Company:</th>
							<td><input type="text" name="x_company" value=""></td>
						</tr>
						<tr>
							<th>Address:</th>
							<td><input type="text" name="x_address" value="Windsong Palace1"></td>
						</tr>
						<tr>
							<th>City:</th>
							<td><input type="text" name="x_city" value="Karachi"></td>
						</tr>
						<tr>
							<th>State:</th>
							<td><input type="text" name="x_state" value="Sindh"></td>
						</tr>
						<tr>
							<th>Zip:</th>
							<td><input type="text" name="x_zip" value="75010"></td>
						</tr>
						<tr>
							<th>Country:</th>
							<td><input type="text" name="x_country" value=""></td>
						</tr>
						<tr>
							<th>Phone:</th>
							<td><input type="tel" name="x_phone" value="02178601"></td>
						</tr>
						<tr>
							<th>E-mail:</th>
							<td><input type="email" name="x_email" value="farhan.golpik1@gmail.com"></td>
						</tr>
					</tbody></table>

					
					
				</form>

				
                        </div>
                    </div>

                </div>
                
                
                <div class="blockcontinue cont_100">
                    
                    <div class="cont_70 empty"></div>
                    
                    <div class="cont_30 ">
                        
                        <p class="bt-highlight checkoutButton biggerbutton">
                            
                            <button type="button" id="authorizebutton" onclick="submitOrder();"><span>Place Order and Continue</span></button>                                
                        </p>
                        
                    </div>
                </div>

            </div>
              
		  
    

</div>



	<script src="{{ asset('/front/js/Chart.js') }}"></script>		
@endsection