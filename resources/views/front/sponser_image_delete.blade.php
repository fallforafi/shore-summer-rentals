@extends('popup')

@section('content')

  <link rel="stylesheet" type='text/css' href="{{ asset('front/css/structure.min.css') }}" />
      
              <div class="modal-content modal-content-upload">


	<?php if (session('deleteImageSucess') == "success"){ ?>
          <script type="text/javascript">
     setTimeout(function(){
               parent.loadGallery(<?php echo session('item_id') ?>, 'y', 'sponsors', '', '');
          }, 500)
      
        setTimeout(function(){
              parent.$.fancybox.close();
          }, 1500)

       
			</script>
		<?php } ?>           
                
                <h2 class="popup-title">Delete Image                    <span>
                        <a href="javascript:void(0);" onclick="parent.$.fancybox.close();">Close</a>
                    </span>
                </h2>

@if(session('message'))
<?php echo session('message');  ?>
@endif 

            	
	<form name="uploadimage" action="<?php echo url('sponsors/delete_image') ?>" method="post" class="frmEmail" enctype="multipart/form-data">
				<p>Are you sure you want to delete this image?</p><br>
			
				<input type="hidden" name="_token" value="{{ csrf_token() }}">

				<input type="hidden" name="gallery_image_id" value="{{ $gallery_image_id }}">
				<input type="hidden" name="gallery_id" value="{{ $gallery_id }}">
				<input type="hidden" name="item_id" value="{{ $item_id }}">
				<input type="hidden" name="item_type" value="listing">
				<input type="hidden" name="temp" value="">
				<table border="0" cellpadding="0" cellspacing="0" class="standardForm">
					<tbody><tr>
						<td colspan="2" class="formButton">
		<p class="input-button-form"><button type="submit"  value="Submit">Submit</button></p>
						</td>
					</tr>
				</tbody></table>
			</form>
						
		</div>
@endsection