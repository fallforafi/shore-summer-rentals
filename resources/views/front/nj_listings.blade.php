@extends('front')

@section('content')


	<section class="inner-page city-page clrlist ">
		<div class="container">
			

			<h1>NEW JERSEY</h1>
			<h3>Jersey Shore Rentals</h3>

			<p>As one of the leading vacation spots in the country, the New Jersey shore is home to a huge selection of summer rentals.&nbsp; Whether you want to vacation in 
			
            <a href="<?php echo url('united-states/new-jersey/ocean-city-14'); ?>">Ocean City,</a> 
			<a href="<?php echo url('united-states/new-jersey/sea-isle-city'); ?>">Sea Isle City</a>, 
			<a href="<?php echo url('united-states/new-jersey/cape-may-19'); ?>">Cape May</a>, 
			<a href="<?php echo url('united-states/new-jersey/north-wildwood'); ?>">North Wildwood</a>, 
			<a href="<?php echo url('united-states/new-jersey/wildwood'); ?>">Wildwood</a>,
			 <a href="<?php echo url('united-states/new-jersey/wildwood-crest'); ?>">Wildwood Crest</a>, 
			 <a href="<?php echo url('united-states/new-jersey/diamond-beach'); ?>">Diamond Beach</a> or any Jersey Shore town, 
			 
			 <em><strong>Shore Summer Rentals.com </strong></em>can help you find the perfect shore rental in NJ.&nbsp; You will not pay a booking fee to ShoreSummerRentals.com ever. &nbsp;You can use our <a href="<?php echo url('advsearch'); ?>">Advanced Search Tool </a></span> to find <em><strong>NJ beach house</strong></em> rentals, condominiums, townhomes, single family homes and more, all with the amenities you need to make your vacation complete.&nbsp; When conducting a search for jersey shore rentals, it's a good idea to be as specific as possible.&nbsp; By searching with our <a href="advsearch.php">Advanced Search Tool</a>
			 <strong> <a href="<?php echo url('advsearch'); ?>">, </a></strong> you can bring up a list of available properties that most closely match your criteria such as pet friendly, distance to the beach, boat slip or just click the links below to view all homes in that particular town.</p>
			
			<?php if(isset($cities) && !empty($cities)) {
			 
			 ?>
			<div class="hed crossline"><h2>BROWSE BY CITY</h2><hr/></div>
			
    			<div class="citylists list-col-3">
        			<ul>
                        <?php foreach($cities as $key=>$val){ ?>
        				<li><a href="<?php echo url($val['full_url']); ?>"><?php echo $val['title']; ?> <span class="badge">(<?php echo $val['total']; ?>)</span></a></li>
                        <?php } ?>
        				
        			</ul>
    			</div>
            <?php } ?>
			
		</div>
	</section>
	
						

					
@endsection