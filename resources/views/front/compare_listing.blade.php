@extends('front')

@section('content')


    
<div class="well compare_listing-page-container">
        
            <div>
                       
    <div id="less-than-2" class="errorMessage" style="display: none">
        <p style="text-align: center">You can't have less than 2 items on comparison.</p>
    </div>
    <table class="table-striped table-hover table-comparison">
        <?php $total_width  = 60; 
       $divide_by = count($compareList);
       $width = $total_width/$divide_by;

      // $compareList=array_reverse($compareList);
        ?>
        <tbody>
        
                <tr>
                <th></th>
                <?php foreach($compareList as $key=>$val){ ?>
                <th width="<?php echo $width; ?>%">
                <p><img class="compareImage" src="<?php echo config('params.imagesPath').$val->prefix.'photo_'.$val->image_id.".".strtolower($val->type);?>" alt="" title="" border="0"></p>
                <p>Rental ID: <?php echo $val->rental_id; ?></p>
                <p>Property Title: <?php echo $val->title.', '.$val->address.', '.$val->bedroom.' Bedrooms, '.$val->bathroom.' Bathrooms, '.$val->sleeps.' Sleeps '; ?></p>
                <p>Property Address: <?php echo $val->address.' '.$val->address2.', '.$val->location_4_title.', '.$val->location_3_title.', '.$val->zip_code; ?></p>
               
      <button class="btn-comparison-remove" onclick="javascript: remove_signle_item(
        <?php echo $row_number[$key] ?>,<?php echo $val->rental_id; ?>)"> X </button>
                </th>
                <?php } ?>
                
               
                </tr>

                <tr>
                    <th class="td-comparison-label" colspan="6">
                        Details                    
                    </th>
                </tr>
                
                <tr>
                <td class="td-comparison-label">Bedrooms</td>
                     <?php foreach($compareList as $key=>$val){ ?>
                    <td width="<?php echo $width; ?>%">
                        <?php echo $val->bedroom; ?>                            
                    </td>
                    <?php } ?>
                    
                   
                
                </tr>
                
                <tr>
                <td class="td-comparison-label">Sleeps</td>
                 <?php foreach($compareList as $key=>$val){ ?>
                <td width="<?php echo $width; ?>%">
                    <?php echo $val->sleeps; ?>                            
                </td>
                 <?php } ?>
                
                </tr>
                
                <tr>
                <td class="td-comparison-label">Bathrooms</td>
                <?php foreach($compareList as $key=>$val){ ?>
                    <td width="<?php echo $width; ?>%">
                    <?php echo $val->bathroom; ?>                            
                    </td>
                <?php } ?>
                </tr>
                
                
                <tr>
                    <th class="td-comparison-label" colspan="6">
                        Amenities                    
                    </th>
                </tr>
                
                 <tr>
                    <td class="td-comparison-label">Air Hockey</td>
                    
                   
                    <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->amenity_airhockey=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                
                
                
                </tr>
                
                 <tr>
                    <td class="td-comparison-label">Alarm Clock</td>
                    
                   
                    <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->amenity_alarmclock=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                
                
                
                </tr>
                <tr>
                    <td class="td-comparison-label">Answering Machine</td>
                    
                   
                    <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->amenity_answeringmachine=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                
                
                
                </tr>
                <tr>
                    <td class="td-comparison-label">Arcade Games</td>
                    
                   
                    <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->amenity_arcadegames=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                
                
                
                </tr>
                 <tr>
                    <td class="td-comparison-label">Billiards</td>
                    
                   
                    <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->amenity_billiards=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                
                
                
                </tr>
                
                 <tr>
                    <td class="td-comparison-label">Books</td>
                    
                   
                    <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->amenity_books=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                
                
                
                </tr>
               
                
                 <tr>
                    <td class="td-comparison-label">Bluray Player</td>
                    
                   
                    <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->amenity_blurayplayer=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                
                
                
                </tr>
                
                
                
                
                
                
                <tr>
                    <td class="td-comparison-label">Blender</td>
                    
                   
                    <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->amenity_blender=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                
                
                
                </tr>
                
               
                
                
                 <tr>
                    <td class="td-comparison-label">Casette Player</td>
                    
                   
                    <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->amenity_casetteplayer=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                
                
                
                </tr>
                 <tr>
                    <td class="td-comparison-label">Cd Player</td>
                    
                   
                    <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->amenity_cdplayer=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                
                
                
                </tr>
                
                 <tr>
                    <td class="td-comparison-label">Child's High Chair</td>
                    
                   
                    <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->amenity_childshighchair=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                
                
                
                </tr>
                 <tr>
                    <td class="td-comparison-label">Communal Pool</td>
                    
                   
                    <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->amenity_communalpool=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                
                
                
                </tr>
                <tr>
                    <td class="td-comparison-label">Computer</td>
                    
                   
                    <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->amenity_computer=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                
                
                
                </tr>
                
                
                
                
                
                <tr>
                    <td class="td-comparison-label">Ceiling Fan</td>
                    
                   <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->amenity_ceilingfan=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                    
                    
                </tr>
                <tr>
                    <td class="td-comparison-label">Coffee Maker</td>
                    
                     <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->amenity_coffeemaker=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                    
                </tr>
                <tr>
                    <td class="td-comparison-label">Cooking Range</td>
                    
                     <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->amenity_cookingrange=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                    
                </tr>
                <tr>
                    <td class="td-comparison-label">Cookware</td>
                     <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->amenity_cookware=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                
                <tr>
                <td class="td-comparison-label">Deck Furniture</td>
                
                <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->amenity_deckfurniture=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                
                <tr>
                <td class="td-comparison-label">Dishes</td>
                
                <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->amenity_dishes=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                
                
                
                <tr>
                    <td class="td-comparison-label">Dishwasher</td>
                    <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->amenity_dishwasher=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                <tr>
                    <td class="td-comparison-label">DVD Player</td>
                     <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->amenity_dvdplayer=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                    
                
                </tr>
                <tr>
                    <td class="td-comparison-label">Exercise Facilities</td>
                     <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->amenity_exercisefacilities=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                    
                
                </tr>
                
                
                <tr>
                    <td class="td-comparison-label">Foos Ball</td>
                     <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->amenity_foosball=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                    
                
                </tr>
                
                
                
                <tr>
                    <td class="td-comparison-label">Game Table</td>
                     <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->amenity_gametable=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                    
                
                </tr>
                
                <tr>
                    <td class="td-comparison-label">Games</td>
                     <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->amenity_games=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                    
                
                </tr>
                
                 <tr>
                    <td class="td-comparison-label">Grill</td>
                     <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->amenity_grill=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                    
                
                </tr>
                
                <tr>
                    <td class="td-comparison-label">Hair Dryer</td>
                     <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->amenity_hairdryer=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                    
                
                </tr>
                
                 <tr>
                    <td class="td-comparison-label">Ice Maker</td>
                     <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->amenity_icemaker=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                    
                
                </tr>
                
                 <tr>
                    <td class="td-comparison-label">Internet</td>
                     <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->amenity_internet=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                    
                
                </tr>
                
                <tr>
                    <td class="td-comparison-label">Iron And Board</td>
                     <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->amenity_ironandboard=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                    
                
                </tr>
                
                <tr>
                    <td class="td-comparison-label">Kids Games</td>
                     <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->amenity_kidsgames=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                    
                
                </tr>
                
                <tr>
                    <td class="td-comparison-label">Linens Provided</td>
                     <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->amenity_linensprovided=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                    
                
                </tr>
                
                <tr>
                    <td class="td-comparison-label">Lobster Pot</td>
                     <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->amenity_lobsterpot=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                    
                
                </tr>
                
                <tr>
                    <td class="td-comparison-label">Microwave</td>
                    <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->amenity_microwave=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                <tr>
                    <td class="td-comparison-label">Mini Refrigerator</td>
                    <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->amenity_minirefrigerator=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                <tr>
                    <td class="td-comparison-label">Mp3 Radio Dock</td>
                    <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->amenity_mp3radiodock=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                
                
                <tr>
                    <td class="td-comparison-label">Outside Shower</td>
                    <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->amenity_outsideshower=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                <tr>
                    <td class="td-comparison-label">Oven</td>
                    <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->amenity_oven=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                
                <tr>
                    <td class="td-comparison-label">Pin Ball</td>
                    <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->amenity_pinball=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                
                 <tr>
                    <td class="td-comparison-label">Ping Pong</td>
                    <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->amenity_pingpong=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                
                 <tr>
                    <td class="td-comparison-label">Private Pool</td>
                    <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->amenity_privatepool=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                 <tr>
                    <td class="td-comparison-label">Radio</td>
                    <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->amenity_radio=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                
               
                <tr>
                    <td class="td-comparison-label">Refrigerator/Freezer</td>
                    <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->amenity_refrigeratorfreezer=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                 <tr>
                    <td class="td-comparison-label">Sofa Bed</td>
                    <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->amenity_sofabed=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                <tr>
                    <td class="td-comparison-label">Stereo</td>
                    <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->amenity_stereo=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                
                
                
                
                <tr>
                    <td class="td-comparison-label">Telephone</td>
                     <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->amenity_telephone=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                <tr>
                    <td class="td-comparison-label">Television</td>
                    <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->amenity_television=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                
                <tr>
                    <td class="td-comparison-label">Toaster</td>
                    <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->amenity_toaster=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                
                <tr>
                    <td class="td-comparison-label">Toaster Oven</td>
                    <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->amenity_toasteroven=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                <tr>
                    <td class="td-comparison-label">Towels Provided</td>
                    <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->amenity_towelsprovided=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                
                <tr>
                    <td class="td-comparison-label">Toys</td>
                    <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->amenity_toys=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                
                
                
                
                
                
                
                <tr>
                    <td class="td-comparison-label">Utensils</td>
                    <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->amenity_utensils=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                <tr>
                    <td class="td-comparison-label">VCR</td>
                     <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->amenity_vcr=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                
                <tr>
                    <td class="td-comparison-label">Vacuum</td>
                     <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->amenity_vacuum=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                <tr>
                    <td class="td-comparison-label">Video Game Console</td>
                     <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->amenity_videogameconsole=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                
                 <tr>
                    <td class="td-comparison-label">Video Games</td>
                     <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->amenity_videogames=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
               
                <tr>
                    <td class="td-comparison-label">Washer/Dryer</td>
                    <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->amenity_washerdryer=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                <tr>
                    <td class="td-comparison-label">WiFi</td>
                    <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->amenity_wifi=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                <tr>
                    <th class="td-comparison-label" colspan="6">
                        Features                    
                    </th>
                </tr>
                <tr>
                    <td class="td-comparison-label">Air Conditioning</td>
                     <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->feature_airconditioning=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>   
                </tr>
                <tr>
                    <td class="td-comparison-label">Balcony</td>
                     <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->feature_balcony=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                
                <tr>
                    <td class="td-comparison-label">Barbecue Charcoal</td>
                     <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->feature_barbecuecharcoal=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                
                
                
                
                <tr>
                    <td class="td-comparison-label">Barbecue Gas</td>
                    <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->feature_barbecuegas=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                <tr>
                    <td class="td-comparison-label">Cable/Satellite TV</td>
                    <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->feature_cablesatellitetv=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                
                <tr>
                    <td class="td-comparison-label">Club House</td>
                    <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->feature_clubhouse=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                
                <tr>
                    <td class="td-comparison-label">Covered Parking</td>
                    <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->feature_coveredparking=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                
                
                
                
                
                <tr>
                    <td class="td-comparison-label">Deck</td>
                    <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->feature_deck=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                
                <tr>
                    <td class="td-comparison-label">Dining Room</td>
                    <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->feature_diningroom=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                
                <tr>
                    <td class="td-comparison-label">Elevator</td>
                    <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->feature_elevator=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                
                <tr>
                    <td class="td-comparison-label">Full Kitchen</td>
                     <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->feature_fullkitchen=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                
                 <tr>
                    <td class="td-comparison-label">Family Room</td>
                     <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->feature_familyroom=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                
                <tr>
                    <td class="td-comparison-label">Gas Fire Place</td>
                     <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->feature_gasfireplace=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                
                <tr>
                    <td class="td-comparison-label">Gated Community</td>
                     <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->feature_gatedcommunity=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                
                
                <tr>
                    <td class="td-comparison-label">Garage</td>
                    <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->feature_garage=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                
                 <tr>
                    <td class="td-comparison-label">Heated</td>
                    <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->feature_heated=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                
                 <tr>
                    <td class="td-comparison-label">Heated Pool</td>
                    <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->feature_heatedpool=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                
                <tr>
                    <td class="td-comparison-label">Hot Tub Jacuzzi</td>
                    <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->feature_hottubjacuzzi=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                
                <tr>
                    <td class="td-comparison-label">Internet Access</td>
                     <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->feature_internetaccess=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                
                 <tr>
                    <td class="td-comparison-label">Kitchenette</td>
                     <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->feature_kitchenette=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                
                <tr>
                    <td class="td-comparison-label">Living Room</td>
                     <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->feature_livingroom=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                
                <tr>
                    <td class="td-comparison-label">Loft</td>
                     <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->feature_loft=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                
                <tr>
                    <td class="td-comparison-label">Onsite Security</td>
                     <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->feature_onsitesecurity=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                
                <tr>
                    <td class="td-comparison-label">Patio</td>
                     <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->feature_patio=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                
                 <tr>
                    <td class="td-comparison-label">Pets Allowed</td>
                     <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->feature_petsallowed=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                
                <tr>
                    <td class="td-comparison-label">Play room</td>
                     <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->feature_playroom=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                
                 <tr>
                    <td class="td-comparison-label">Pool</td>
                     <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->feature_pool=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                
                <tr>
                    <td class="td-comparison-label">Porch</td>
                     <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->feature_porch=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                
                
                 <tr>
                    <td class="td-comparison-label">Roof Top Deck</td>
                     <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->feature_rooftopdeck=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                
                <tr>
                    <td class="td-comparison-label">Sauna</td>
                     <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->feature_sauna=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                
                 <tr>
                    <td class="td-comparison-label">Smoking Permitted</td>
                     <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->feature_smokingpermitted=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                
                 <tr>
                    <td class="td-comparison-label">Wood Fire Place</td>
                     <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->feature_woodfireplace=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                
               
                <tr>
                    <td class="td-comparison-label">Wheel Chair Access</td>
                     <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->feature_wheelchairaccess=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                
                
                
                <tr>
                    <th class="td-comparison-label" colspan="6">
                        Activities                    
                    </th>
                </tr>
                
                <tr>
                    <td class="td-comparison-label">Antiquing</td>
                     <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->activity_antiquing=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                
                 <tr>
                    <td class="td-comparison-label">Basket Ball Court</td>
                     <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->activity_basketballcourt=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                
                  <tr>
                    <td class="td-comparison-label">Beach Combing</td>
                     <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->activity_beachcombing=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                
                 <tr>
                    <td class="td-comparison-label">Bicycling</td>
                     <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->activity_bicycling=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                
                <tr>
                    <td class="td-comparison-label">Bike Rentals</td>
                     <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->activity_bikerentals=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                
                <tr>
                    <td class="td-comparison-label">Bird Watching</td>
                     <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->activity_birdwatching=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                 <tr>
                    <td class="td-comparison-label">Boat Rentals</td>
                     <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->activity_boatrentals=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                 <tr>
                    <td class="td-comparison-label">Boating</td>
                     <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->activity_boating=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                
                 <tr>
                    <td class="td-comparison-label">Botanical Garden</td>
                     <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->activity_botanicalgarden=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                
                 <tr>
                    <td class="td-comparison-label">Canoe</td>
                     <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->activity_canoe=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                <tr>
                    <td class="td-comparison-label">Churches</td>
                     <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->activity_churches=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                
                <tr>
                    <td class="td-comparison-label">Cinemas</td>
                     <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->activity_cinemas=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                
                  <tr>
                    <td class="td-comparison-label">Bikes Provided</td>
                     <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->activity_bikesprovided=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                
                 <tr>
                    <td class="td-comparison-label">Deep Sea Fishing</td>
                     <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->activity_deepseafishing=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                
                 <tr>
                    <td class="td-comparison-label">Fishing</td>
                     <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->activity_fishing=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                
                 <tr>
                    <td class="td-comparison-label">Fitness Center</td>
                     <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->activity_fitnesscenter=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                
                   <tr>
                    <td class="td-comparison-label">Golf</td>
                     <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->activity_golf=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                
                   <tr>
                    <td class="td-comparison-label">Health Beauty Spa</td>
                     <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->activity_healthbeautyspa=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                
                
                   <tr>
                    <td class="td-comparison-label">Hiking</td>
                     <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->activity_hiking=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                
                   <tr>
                    <td class="td-comparison-label">Horse Back Riding</td>
                     <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->activity_horsebackriding=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                
                   <tr>
                    <td class="td-comparison-label">Horseshoes</td>
                     <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->activity_horseshoes=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                
                 <tr>
                    <td class="td-comparison-label">Hot Air Ballooning</td>
                     <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->activity_hotairballooning=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                
                  <tr>
                    <td class="td-comparison-label">Ice Skating</td>
                     <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->activity_iceskating=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                
                 <tr>
                    <td class="td-comparison-label">Jet Skiing</td>
                     <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->activity_jetskiing=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                
                  <tr>
                    <td class="td-comparison-label">Kayaking</td>
                     <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->activity_kayaking=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                
                  <tr>
                    <td class="td-comparison-label">Live Theater</td>
                     <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->activity_livetheater=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                
                <tr>
                    <td class="td-comparison-label">Marina</td>
                     <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->activity_marina=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                
                 <tr>
                    <td class="td-comparison-label">Miniature Golf</td>
                     <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->activity_miniaturegolf=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                
                 <tr>
                    <td class="td-comparison-label">Mountain Biking</td>
                     <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->activity_mountainbiking=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                
                <tr>
                    <td class="td-comparison-label">Movie Cinemas</td>
                     <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->activity_moviecinemas=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                
                 <tr>
                    <td class="td-comparison-label">Museums</td>
                     <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->activity_museums=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                
                 <tr>
                    <td class="td-comparison-label">Paddle Boating</td>
                     <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->activity_paddleboating=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                
                <tr>
                    <td class="td-comparison-label">Paragliding</td>
                     <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->activity_paragliding=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                
                <tr>
                    <td class="td-comparison-label">Parasailing</td>
                     <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->activity_parasailing=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                
                <tr>
                    <td class="td-comparison-label">Playground</td>
                     <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->activity_playground=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                
                <tr>
                    <td class="td-comparison-label">Recreation Center</td>
                     <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->activity_recreationcenter=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                
                <tr>
                    <td class="td-comparison-label">Restaurants</td>
                     <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->activity_restaurants=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                
                <tr>
                    <td class="td-comparison-label">Rollerblading</td>
                     <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->activity_rollerblading=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                
                <tr>
                    <td class="td-comparison-label">Sailing</td>
                     <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->activity_sailing=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                <tr>
                    <td class="td-comparison-label">Shelling</td>
                     <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->activity_shelling=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                
                 <tr>
                    <td class="td-comparison-label">Shopping</td>
                     <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->activity_shopping=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                
                <tr>
                    <td class="td-comparison-label">Sightseeing</td>
                     <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->activity_sightseeing=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                
                <tr>
                    <td class="td-comparison-label">Skiing</td>
                     <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->activity_skiing=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                <tr>
                    <td class="td-comparison-label">Bay Fishing</td>
                     <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->activity_bayfishing=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                
                <tr>
                    <td class="td-comparison-label">Spa</td>
                     <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->activity_spa=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                
                <tr>
                    <td class="td-comparison-label">Surf Fishing</td>
                     <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->activity_surffishing=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                
                <tr>
                    <td class="td-comparison-label">Surfing</td>
                     <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->activity_surfing=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                
                <tr>
                    <td class="td-comparison-label">Swimming</td>
                     <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->activity_swimming=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                
                <tr>
                    <td class="td-comparison-label">Tennis</td>
                     <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->activity_tennis=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                
                <tr>
                    <td class="td-comparison-label">Theme Parks</td>
                     <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->activity_themeparks=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                
                <tr>
                    <td class="td-comparison-label">Walking</td>
                     <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->activity_walking=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                
                <tr>
                    <td class="td-comparison-label">Waterparks</td>
                     <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->activity_waterparks=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                
                <tr>
                    <td class="td-comparison-label">Water Skiing</td>
                     <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->activity_waterskiing=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                
                <tr>
                    <td class="td-comparison-label">Water Tubing</td>
                     <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->activity_watertubing=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                
                
                <tr>
                    <td class="td-comparison-label">Wildlife Viewing</td>
                     <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->activity_wildlifeviewing=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
                
                <tr>
                    <td class="td-comparison-label">Zoo</td>
                     <?php foreach($compareList as $key=>$val){ ?>
                        <td width="<?php echo $width; ?>%">
                             
                            <?php if($val->activity_zoo=='y'){
                                echo '<i class="fa fa-check"></i>';
                            }
                            else
                            {
                                echo '<i class="fa fa-close"></i>';
                            } 
                            ?>                            
                        </td>
                    <?php } ?>
                </tr>
          
    </tbody></table>
    <!--
    <button class="btn" id="go_back">
        Back to Search
    </button>
    -->
   
    
<script type="text/javascript">


    function remove_signle_item(row_number,resource_id)
{
        var count = getCookie('total_list');
        
        if(count<3)
        {
            alert("You can't have less than 2 items on comparison.");
            return false;
        }
        else
        {
           //setCookie('total_list',(count-1));
                
            setCookie('row_'+row_number,'');
            setCookie('title_'+row_number,'');
            setCookie('resource_id_'+row_number,'');
            
            setCookie('total_list',(count-1));
            location.reload();
            return true;
        }
        
}
function setCookie(cname,cvalue) {
    
   
    document.cookie = cname+"="+cvalue+";path=/";
}
function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}




    function js_takeout(remove_this){
        var listing_id = remove_this+"";
        //Get list as string from hidden input
        var comparison_list = $("#comparison_list").val();
        //String to array
        var comparison_array = comparison_list.split(',');
        //Take out "listing_id"
        comparison_array.splice($.inArray(listing_id, comparison_array),1);

        //array to string
        var comparison_string = comparison_array.join(',');
        var new_comparison_array = comparison_string.split(',');

        //Get form action URL from hidden input
        var redirectUrl = $("#comparison_url").val();

        if(new_comparison_array.length > 1) {
        //create form / submit form
            var form = $('<form action="' + redirectUrl + '" method="post" >' +
            '<input type="hidden" name="comparison_list" value="'+comparison_string+'" />' +
            '</form>');
            $('body').append(form);
            $(form).submit();
        } else {
            $("#less-than-2").fadeIn("slow");

            setTimeout(function() {
                $("#less-than-2").fadeOut("slow");
            }, 5000);
        }
    }

    $(document).ready(function(){
        $('#go_back').click(function(){
            parent.history.back();
            return false;
        });
    });
</script>
                </div><!-- Close container-fluid div -->
        </div>
                    
@endsection