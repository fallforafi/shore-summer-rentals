<?php
use App\Functions\Functions;
use App\Functions\CustomInvoice;

?>

<html>
<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		
		<link href="{{ asset('front/css/invoice.css') }}" rel="stylesheet" type="text/css">			
<style type="text/css">
.fancybox-type-iframe .fancybox-inner {
    height: 700px !important;
}
</style>	
	</head>

	<body class="invoice-body" style="overflow-x: hidden;">

		
		<table border="0" cellpadding="2" cellspacing="2" class="base-invoice" id="table_invoice">
			<tbody><tr>
				<td colspan="2" style="padding: 0;">

					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tbody><tr>
							<td>
								<p>Shore Summer Rentals</p>
								<p>P.O. Box 55</p>
								<p>Somers Point, NJ 08244</p>
								<p></p>
								<p>609.677.1580</p>
								<p>609.677.1590</p>
								<p>maria@shoresummerrentals.com</p>
							</td>
							<td width="250" align="center">
								<img width="112" height="70" src="https://www.shoresummerrentals.com/custom/domain_1/image_files/sitemgr_photo_64860.jpg" alt="Invoice Logo" title="Invoice Logo" border="0" style="display:inline">							</td>
							<td width="150">
								<h1>INVOICE</h1>
							</td>
						</tr>
					</tbody></table>

				</td>
			</tr>
			<tr>
				<td colspan="2">

					<div class="invoice-bill" style=" float:left;">
						<p><b>Bill to:</b></p>
						<p>
						 {{ $contactObj->getString("first_name") }}  {{$contactObj->getString("last_name")}}<br />
						 {{ ( $contactObj->getString("company") ) ? $contactObj->getString("company")."<br />" : "" }}
						 {{ ( $contactObj->getString("address") ) ? $contactObj->getString("address")."&nbsp;" : ""}}
						 {{ ( $contactObj->getString("address2") ) ? $contactObj->getString("address2")."<br />" : ""}}
						 {{ ( $contactObj->getString("city") ) ? $contactObj->getString("city").", " : ""}}
						 {{ ( $contactObj->getString("state") ) ? $contactObj->getString("state")."&nbsp;" : ""}}
						 {{( $contactObj->getString("zip") ) ? $contactObj->getString("zip") : ""}}			

							</p>
													<br><p><strong>Payable to:</strong><br> ShoreSummerRentals.com 
											</p></div>
					
					<div class="invoice-bill" style=" float:right;">
						<p><b>Issuing Date:</b> {{ Functions::format_date($invoiceObj->getString("date"),config('params.DEFAULT_DATE_FORMAT'),"datetime") }}</p>
						<p><b>Invoice #:</b> {{ $invoiceObj->getString("id") }}</p>
					</div>

				</td>
			</tr>
			
			<tr>
				<td colspan="2">

					<table border="0" cellspacing="0" cellpadding="0" class="invoice-content">
						<tbody><tr>
							<th width="300">Item</th>
					<?php if (count($arr_invoice_listing)) { ?>
							<th nowrap="nowrap">Extra Category</th>

					<?php } ?>		
					<th width="80" nowrap="nowrap">Level</th>
					<th nowrap="nowrap">Promotional Code</th>
					<th nowrap="nowrap">Amount</th>
															
						</tr>
						<?php
						if(isset($arr_invoice_listing))
						{
						for($i=0; $i < count($arr_invoice_listing); $i++) 
						{ 

						$level_name = ucfirst($arr_invoice_listing[$i]->level_label);
						?>
							<tr>
								<td><b>Listing:</b> # {{ $arr_invoice_listing[$i]->listing_id }} - {{ $arr_invoice_listing[$i]->listing_address }}</td>
								<td>{{ intval($arr_invoice_listing[$i]->extra_categories) }}	</td>
								<td>{{ $level_name  }}</td>
								<td> <?php if (trim($arr_invoice_listing[$i]->discount_id) != "") { ?>
								{{ $arr_invoice_listing[$i]->discount_id }}
								<?php } else { ?>
								N/A
								<?php }?>
								</td>
								<td>{{ config('params.CURRENCY_SYMBOL')." ".Functions::format_money($arr_invoice_listing[$i]->amount) }}</td>
																
							</tr>

						<?php }
					}
					if(isset($arr_invoice_listingchoice))
					{
						for($i=0; $i < count($arr_invoice_listingchoice); $i++){
							?>
							<tr>
								<td><b>{{ $arr_invoice_listingchoice[$i]->badge_name }}: </b>#{{ $arr_invoice_listingchoice[$i]->rental_id }} - {{ $arr_invoice_listingchoice[$i]->listing_address }}</td>
					
								<td>N/A</td>
								<td>N/A</td>
								<td>N/A</td>
								<td nowrap="nowrap">
								{{ config('params.CURRENCY_SYMBOL')." ".Functions::format_money($arr_invoice_listingchoice[$i]->amount) }}</td>
																
							</tr>
						<?php }
						} 

						if(isset($arr_invoice_custominvoice))
						{
						for($i=0; $i < count($arr_invoice_custominvoice); $i++){
						$customInvoiceObj = new CustomInvoice();
						$customInvoiceObj->CustomInvoice($arr_invoice_custominvoice[$i]->custom_invoice_id);

						$custom_invoice_items = $arr_invoice_custominvoice[$i]->items;
						$custom_invoice_items = explode("\n", $custom_invoice_items);

						$custom_invoice_items_price = $arr_invoice_custominvoice[$i]->items_price;
						$custom_invoice_items_price = explode("\n", $custom_invoice_items_price);

							if ($custom_invoice_items && $custom_invoice_items_price) {
								foreach ($custom_invoice_items as $key => $each_item) {
									$custom_invoice_items_desc[] = $each_item." - ".$custom_invoice_items_price[$key];
								}
							}
						?>	
						<tr>
					<td <?php if (count($arr_invoice_listing)) { echo "colspan=\"4\""; } else { echo "colspan=\"3\""; } ?>> <?php echo $arr_invoice_custominvoice[$i]->title.":<br />";?><?php echo ($custom_invoice_items_desc) ? implode("<br />", $custom_invoice_items_desc) : ""?></td>
								<td nowrap="nowrap" style="vertical-align: top;">
								{{ config('params.CURRENCY_SYMBOL')." ".Functions::format_money($arr_invoice_custominvoice[$i]->subtotal) }}
									
								</td>
							</tr>

						<?php 
						unset($customInvoiceObj, $custom_invoice_items_desc, $custom_invoice_items_price);
						}
						} 
						?>	

					
						<tr>
							<td colspan="3" class="invoice-detailbelow">
								Make checks payable to ShoreSummerRentals.com								<br>
								<b>Questions:</b> Please call 609.677.1580								<br>
								<strong style="font-size: 8pt;">Thank you!</strong>
							</td>


							<th class="invoice-total">
							Total:
							</th>
                            <th class="invoice-total">
							<span>{{ config('params.CURRENCY_SYMBOL')." ".Functions::format_money($invoiceObj->getString("amount")) }}</span>
                            </th>
						</tr>
					</tbody></table>

				</td>
			</tr>
			<tr>
				<td colspan="2" class="invoice-detailbelow" align="center">
								</td>
			</tr>
		</tbody></table>
	
	<p id="print"><a href="javascript:void(0);" onclick="document.getElementById('print').style.display='none';window.print();document.getElementById('print').style.display='block'" style="color:#000000; font: bold 10pt Verdana, Arial, Helvetica, sans-serif;">Click here to print the invoice</a></p>
					
	



</body></html>