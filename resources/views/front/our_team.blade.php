@extends('front')

@section('content')

<style>

.our-Team__container{
	width:1200px;
	padding:0;
}
.content-custom h1, .content-custom h2, .content-custom h3{
    line-height: 1;
    text-indent: 0;
	margin:0;
}
.content-custom{
	margin-top:0;
}
.content-custom p{
	font-size:18px;
	line-height: 1.5;
    font-weight: 300;
	margin-bottom:10px;
}
.content-custom a, .content-custom a:visited {
    text-decoration: underline;
}
.content-custom h1, .content-custom h2, .content-custom h3 {
    font-weight: 400;
}
.abtRental__teams{
	margin:25px 0 10px;
}
.shoreSummer__teamImg{
	margin-top:9px;
}

</style>
	
		<div class="our-Team__container container">

        
		<div class="content content-full margin-top">

			<div class="content-main">
		
				<div class="content-custom"><h1><strong><br>OUR TEAM</strong></h1>
<h2><strong></strong><span style="font-size: x-small;"><strong style="color: #ff6600; font-size: 1.5em;"><strong><span style="font-size: x-large; position:relative; top:15px;"><span style="font-size: xx-large;">About Our Jersey Shore Rentals Team</span></span></strong></strong></span></h2>
<table border="0" class="abtRental__teams">
<tbody>
<tr>
<td>
<h2><a href="https://vimeo.com/145863061"><br></a>&nbsp;<a href="https://vimeo.com/145863061"><img src="http://shoresummerrentals.com/custom/domain_1/image_files/Marketing/MARIA_MISCELLANEOUS/sitemgr_Untitled_design1.png" alt="" width="247" height="182"></a>&nbsp;</h2>
</td>
<td><br>&nbsp;<a href="https://vimeo.com/145005475"><img class="shoreSummer__teamImg" src="http://shoresummerrentals.com/custom/domain_1/image_files/Marketing/MARIA_MISCELLANEOUS/sitemgr_Untitled_design2.png" alt="" width="240" height="183"></a></td>
</tr>
</tbody>
</table>
<p><span style="color: #000000;"><a href="http://www.shoresummerrentals.com"><span style="color: #000000;"><br><span style="color: #0000ff;">Shore</span><span style="color: #0000ff;"> Summer Rentals.com</span></span></a>, which offers quality jersey shore rentals to the online public, is owned and operated by Chris and Maria Kirk.<br><strong>No booking fees at all.&nbsp;&nbsp;You also have full control of your email leads.</strong>&nbsp; </span></p>
<p><span style="color: #000000;">With the strength of 30 plus years of combined experience in marketing, real estate, vacation rentals and property management. Chris and Maria have a wealth of knowledge in successfully pairing property owners with renters. &nbsp;Chris and Maria were investment property owners themselves at the New Jersey Shore. Their first-hand knowledge of vacation property rental ownership gives them the unique advantage to see rentals from both sides of the fence. From understanding what "quality" really means to valuing the definition of "good tenants", Chris and Maria offer exceptional rental consulting advice to both renters and property owners alike. Please read our Property Owner</span>&nbsp;<span style="color: #0000ff;"><a title="Property Owner Testimonials" href="http://shoresummerrentals.com/content/testimonials-from-owners.html"><span style="color: #0000ff;">Testimonials</span></a>.</span>&nbsp;&nbsp;</p>
<p><span style="color: #000000;">These invaluable experiences have given both Chris and Maria the necessary tools required for smart marketing and corporate savvy, while Maria possesses unparalleled customer service skills for smooth and detailed daily operations.</span></p>
<p><span style="color: #000000;"><span style="color: #0000ff;"><a href="mailto:maria@shoresummerrentals.com?subject=About%20Us%20Contact"><span style="color: #0000ff;">Chris and Maria</span></a></span>&nbsp;live in the South Jersey area and have two children. Maria was born and raised in Cherry Hill, NJ. Chris was born and raised in the Northeast section of Philadelphia, PA. They moved to the Jersey Shore area in 2005 to be closer to the shore area and to be readily available to address their customer's needs. &nbsp;&nbsp;</span></p>
<p><span style="color: #000000;"><span style="color: #0000ff;"><a href="mailto:maria@shoresummerrentals.com?subject=About%20Us%20Contact"><span style="color: #0000ff;">Maria</span></a></span>&nbsp;</span><span style="color: #000000;">handles all operations of the company. Prior to starting ShoreSummerRentals.com, Maria had eight years of commercial real estate/property management experience with firms located in Center City, Philadelphia.</span></p>
<p><span style="color: #000000;">Maria decided to expand her horizons in the real estate business obtaining her New Jersey Real Estate license back in January 2012. Maria is licensed with&nbsp;Keller Williams Jersey Shore, the world’s largest real estate franchise by agent count. With over 700 offices and 110,000 real estate professionals worldwide, Keller Williams has the network and systems to give my clients an edge in today's competitive real estate marketplace. &nbsp;As a Realtor who's an expert in this local area, I bring a wealth of knowledge and expertise in buying and selling real estate in the NJ Shore market. &nbsp;It's not the same everywhere, so you need someone you can trust for up-to-date information. Please remember me when looking to buy or sell your next home.&nbsp;</span><span style="color: #0000ff;"><a href="http://www.mariakirk.com"><span style="color: #0000ff;">Please view my real estate website here<span style="color: #000000;">.</span></span></a></span><span style="color: #0000ff;"><span style="color: #0000ff;">&nbsp;&nbsp;</span></span><span style="color: #000000;">I would like to&nbsp;<span style="color: #0000ff;"><a href="http://app.kw.com/KW2NS2T5W"><span style="color: #0000ff;">share my real estate app</span></a></span>&nbsp;with you. &nbsp;You can see what is going on anywhere, from your street to any place in North America! &nbsp;It's free, there is no cost to you.&nbsp;<span style="color: #0000ff;"><a href="http://app.kw.com/KW2NS2T5W"><span style="color: #0000ff;">Please click here to download my app</span></a></span>&nbsp;to your phone and please share with friends and family!</span></p>
<p><a href="http://www.mariakirk.com"><span style="color: #000000;"><img src="http://shoresummerrentals.com/custom/domain_1/image_files/Marketing/MARIA_MISCELLANEOUS/sitemgr_Maria_Kirk_Signature_-_Final.jpg" alt="" width="600" height="160"></span></a></p>
<p><span style="color: #000000;"><span style="color: #0000ff;"><a href="mailto:maria@shoresummerrentals.com?subject=About%20Us%20Contact"><span style="color: #0000ff;">Chris</span></a></span>&nbsp;is a digital marketing expert with over 12 years experience in marketing NJ Shore vacation homes.&nbsp; Chris also runs his own online advertising agency where he manages Google Adwords, Facebook Ads, Twitter Ads and Pinterest Ads for his clients. &nbsp;Chris has over 15 years working in business intelligence for billion dollar organizations. Chris possesses a Bachelor's degree in Accounting from Temple University.</span></p>
<p><span style="color: #000000;"><strong>Office Assistance</strong></span></p>
<p><span style="color: #000000;">Due to the increase in business,&nbsp;<span style="color: #0000ff;"><a title="Shore Summer Rentals " href="http://www.shoresummerrentals.com/"><span style="color: #0000ff;">Shore Summer Rentals.com</span></a></span>&nbsp;now staffs a personal assistant to assist Maria on a daily basis. Raya is a great asset to both Maria and Chris. &nbsp;Raya has been working with Maria and Chris back since 2004 when Chris and Maria owned an ice cream shop on Beach Ave in Cape May, NJ.&nbsp; Raya demonstrated a genuine care for the business and when ShoreSUmmerRentals.com needed help, Raya was the perfect choice. &nbsp;</span></p>
<p><span style="color: #000000;"><span style="color: #0000ff;"><a href="mailto:raya@shoresummerrentals.com?subject=About%20Us%20Contact"><span style="color: #0000ff;">Raya</span></a></span>&nbsp;has a Bachelor's degree in Business Administration and Management. She is fluent in several languages, including English, French, and Bulgarian. Raya offers tremendous customer service skills.&nbsp; Raya and her husband have a 5 year old son.</span></p>
<p><span style="color: #000000;"><br></span></p></div>			
			</div>
			
					
		</div>

	</div>
						

					
@endsection