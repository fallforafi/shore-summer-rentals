<?php
use App\Functions\Functions;
?>
@extends('popup')

@section('content')

	<link rel="stylesheet" href="{{ asset('front/css/members.min.css') }}" />


<div class="modal-content ">

 <?php if($customInvoiceItems){
                 ?>
 <h2>Custom Invoice Title: {{ $customInvoice->getString("title") }}</h2>

                    <h3>Custom Invoice Items</h3>
            <div class="customInvoice">

               

                   

                    <table border="0" cellpadding="2" cellspacing="2" class="standard-tableTOPBLUE">
                        <tr>
                            <th>Description</th>
                            <th style="width: 70px;">Price</th>
                        </tr>
                            <?php foreach($customInvoiceItems as $each_custominvoice_item) { 
                            	?>
                                <tr>
             <td>{{ $each_custominvoice_item['description'] }}</td>
           <td>{{ config('params.CURRENCY_SYMBOL')." ".Functions::format_money($each_custominvoice_item['price']) }}</td>
                                </tr>
                            <?php }?>
                               
               
                    </table>

                <?php } else { ?>
                        <p class="informationMessage">No items found.</p>
                <?php } ?>

            </div>
    </div>

@endsection