@extends('front')

@section('content')

    <link rel="stylesheet" href="{{ asset('front/css/members.min.css') }}" />
	<link rel="stylesheet" href="{{ asset('front/css/content_custom.min.css') }}" />
	<link href="https://www.shoresummerrentals.com/custom/domain_1/theme/default/content_custom.min.css" rel="stylesheet" type="text/css" media="all">
    <!--link rel="stylesheet" href="{{ asset('front/css/bootstrap-responsive.min.css') }}" /-->
     
     
    
    
<style>

.content-custom h1, .content-custom h2, .content-custom h3{
    line-height: 1;
    text-indent: 0;
	margin:0;
}
.content-custom{
	margin-top:0;
}
.content-custom p{
	font-size:18px;
	line-height: 1.5;
    font-weight: 300;
}
.content-custom a, .content-custom a:visited {
    text-decoration: underline;
}
.content-custom h1, .content-custom h2, .content-custom h3 {
    font-weight: 400;
}
h1{
	margin-top:30px;
}
h1.avoid-msgs{
	color: #ff6600;
	font-weight: 600;
	font-size: x-large;
}
em, strong {
    font-weight: 600;
}
.spam-warning-container p{
	font-size: 18px !important;
    line-height: 1.5 !important;
    font-weight: 300;
	margin-top:30px;
}
.spam-warning-container a{
	text-decoration: underline;
	color: #0000ff;
}
.spam-warning-container a:hover{
	text-decoration: underline;
	color: #0000ff;
}
.spam-warning-container a:visited {
    -webkit-transition: all .3s ease-in-out;
    -moz-transition: all .3s ease-in-out;
    transition: all .3s ease-in-out;
    color: #007fc5;
}
p.strct-resp{
	margin-top:0 !important;
}

</style>	  

               
                
    
   <section class="spam-warning-page ">
		<div class="spam-warning-container container">
			<h1><strong>EMAIL CONFIRMATION</strong></h1>
			<h1 class="avoid-msgs"><strong>Avoid Missing Messages from Renters</strong></h1>
		<p>Please add <a href="mailto:webmaster@shoresummerrentals.com">webmaster@shoresummerrentals.com</a>, maria@shoresummerrentals.com, chris@shoresummerrentals.com, raya@shoresummerrentals.com and info@shoresummerrentals.com to your e-mail contact and/or e-mail address book to avoid renter contact e-mails being tagged as spam and not delivered to your inbox. This SHOULD help assure delivery of the e-mail inquiry to your e-mail account. We send ALL contact e-mails from potential renters to the e-mail accounts you provide when registering but we cannot control what your e-mail service provider does with it once it leaves our servers. As a backup to this, we provide a history of ALL e-mail contacts you receive in your E-mail History Tab that can be accessed in the&nbsp;dashboard section&nbsp;when logged into your account. If you notice e-mails in your history that never made it to your inbox, please contact your service provider.</p>
	
		<p class="strct-resp">It is strictly the owners responsibility to <a href="https://secure.vacationrentals2u.com/login/">login</a> to your <a href="https://secure.vacationrentals2u.com/login/">Shore</a><a href="https://secure.vacationrentals2u.com/login/">SummerRentals.com</a> account daily to ensure that all of the renter inquiries sent to your account are retrieved.</p>
	
		</div>
	</section>




		
    </script>
	<script src="{{ asset('/front/js/Chart.js') }}"></script>		
@endsection