<?php 
use App\Models\Domain\Sql;
use App\Functions\Functions;
use App\Functions\ListingLevel;
use App\Functions\Listing;
use App\Functions\ListingChoice;
use App\Functions\EditorChoice;
use App\Functions\Invoice;
use App\Functions\InvoiceListing;

$id = $transaction->id;
$acctId=Functions::sess_getAccountIdFromSession();

//$_GET['id'] = $id

if($id){

		$sql = "SELECT * FROM Payment_Log WHERE id = '$id' AND hidden = 'n' ";
		if($acctId) $sql .= "AND account_id = '$acctId'";
		$transactionObj = Sql::fetch($sql);
		if(count($transactionObj) > 0) 

		if($transactionObj){

			//$invoiceStatusObj   = new InvoiceStatus();
			$listingLevelObj    = new ListingLevel();
			//$eventLevelObj      = new EventLevel();
			//$bannerLevelObj     = new BannerLevel();
			//$classifiedLevelObj = new ClassifiedLevel();
			//$articleLevelObj    = new ArticleLevel();

			$transaction_n["id"]                   = $transactionObj[0]->id;
			$transaction_n["payment_log_id"]       = $transactionObj[0]->id;
			$transaction_n["account_id"]   = $transactionObj[0]->account_id;
			$transaction_n["transaction_currency"]     = $transactionObj[0]->transaction_currency;
			

			$sql ="SELECT * FROM Payment_Listing_Log WHERE payment_log_id = '{$transaction_n["payment_log_id"]}'";

			$r = Sql::fetch($sql);
			$i=0;
			foreach($r as $row){

				$listingObj = new Listing();
				$listingObj->Listing($row->listing_id);


				$transaction_listing_log[$i]["listing_id"]       = $row->listing_id;
				$transaction_listing_log[$i]["listing_title"]    = $row->listing_title;
				$transaction_listing_log[$i]["listing_address"]  = $listingObj->getString('address');
				$transaction_listing_log[$i]["discount_id"]      = ($row->discount_id) ? $row->discount_id: 'N/A';
                $transaction_listing_log[$i]["level"]            = $row->level;
				$transaction_listing_log[$i]["level_label"]      = $row->level_label;
				$transaction_listing_log[$i]["renewal_date"]     = (Functions::format_date($row->renewal_date,config('params.DEFAULT_DATE_FORMAT'),"date")) ? Functions::format_date($row->renewal_date,config('params.DEFAULT_DATE_FORMAT'),"date") : 'N/A';
				$transaction_listing_log[$i]["categories"]       = $row->categories;
				$transaction_listing_log[$i]["extra_categories"] = $row->extra_categories;
				$transaction_listing_log[$i]["listingtemplate"]  = $row->listingtemplate_title;
				$transaction_listing_log[$i]["amount"]           = $row->amount;

				$i++;

			}

			$sql ="SELECT * FROM Payment_ListingChoice_Log WHERE payment_log_id = '{$transaction_n["payment_log_id"]}'";


			$r = Sql::fetch($sql);
			$i=0;
			foreach($r as $row){


				$listingChoiceObj = new ListingChoice();
				$listingChoiceObj->ListingChoice("", "",$row->listingchoice_id);

				$auxListingObj = new Listing();
				$auxListingObj->Listing($listingChoiceObj->getNumber('listing_id'));


			
				$transaction_listingchoice_log[$i]["listing_id"]   		= $listingChoiceObj->getNumber("listing_id");
				$transaction_listingchoice_log[$i]["invoice_id"]   		= $transactionObj[0]->id;
				$transaction_listingchoice_log[$i]["listingchoice_id"]  = $row->listingchoice_id;
				$transaction_listingchoice_log[$i]["listing_title"]  	= $row->listing_title;
				$transaction_listingchoice_log[$i]["listing_address"]   = $auxListingObj->getString('address');
				$transaction_listingchoice_log[$i]["badge_name"]  		= $row->badge_name;
				$transaction_listingchoice_log[$i]["renewal_date"]     = (Functions::format_date($row->renewal_date,config('params.DEFAULT_DATE_FORMAT'),"date")) ? Functions::format_date($row->renewal_date,config('params.DEFAULT_DATE_FORMAT'),"date") : 'N/A';
				$transaction_listingchoice_log[$i]["amount"]           = $row->amount;

				$i++;

			}

			$sql ="SELECT * FROM Payment_CustomInvoice_Log WHERE payment_log_id = '{$transaction_n["payment_log_id"]}'";
			
			$r = Sql::fetch($sql);
			$i=0;
			foreach($r as $row){

				//$customInvoiceObj = new CustomInvoice($row["custom_invoice_id"]);

				$transaction_custominvoice_log[$i]["custom_invoice_id"] = $row->custom_invoice_id;
				$transaction_custominvoice_log[$i]["title"]             = $row->title;
				$transaction_custominvoice_log[$i]["date"]              = $row->date;
				$transaction_custominvoice_log[$i]["items"]             = $row->items;
				$transaction_custominvoice_log[$i]["items_price"]       = $row->items_price;
				$transaction_custominvoice_log[$i]["amount"]            = $row->amount;

				$i++;

			}	





		}	
     
	}

  

if (isset($transaction_listing_log)) {?>

<table border="0" cellpadding="2" cellspacing="2" class="standard-innerTable">
		<tbody><tr>
			<th class="tableTitle" colspan="3"><p>Listings</p></th>
		</tr>
		@foreach($transaction_listing_log as $each_listing) 
			<tr>
				<th>
					<fieldset title='{{ $each_listing["listing_title"] }}'>
					#{{ $each_listing["listing_id"] }} - {{ $each_listing["listing_address"] }}											</fieldset>
				</th>
				<td class="infoLevel">{{ Functions::string_ucwords($each_listing["level_label"]) }}</td>
				<td class="infoAmount">
				{{ $each_listing["amount"]." (".$transaction_n["transaction_currency"].")" }}
				</td>
			</tr>
		
			@endforeach
			</tbody>
</table>
<br/>
<?php } 

if (isset($transaction_listingchoice_log)) {
?>
<table border="0" cellpadding="2" cellspacing="2" class="standard-innerTable">

<tbody><tr>
			<th class="tableTitle" colspan="2"><p>Listing Specials</p></th>
		</tr>
		@foreach($transaction_listingchoice_log as $each_listingchoice) 
			<tr>
				<th>
					<fieldset title='{{ $each_listingchoice["badge_name"] }}'>
					#{{ $each_listingchoice["listing_id"] }} - {{ $each_listingchoice["badge_name"] }}											</fieldset>
				</th>
			
				<td class="infoAmount">
				{{ $each_listingchoice["amount"]." (".$transaction_n["transaction_currency"].")" }}
				</td>
			</tr>
		
			@endforeach
			</tbody>
</table>
<br/>
<?php } 
if (isset($$transaction_custominvoice_log)) {

?>
	<table align="center" border="0" cellpadding="2" cellspacing="2" class="standard-innerTable">
		<tr>
			<th class="tableTitle" colspan="2"><p>Custom Invoice</p></th>
		</tr>
		@foreach($transaction_custominvoice_log as $each_custominvoice)
			<tr>
				<th>
					<fieldset title="{{ $each_custominvoice['title'] }}">
						<?php
						$transactionCustomInvoiceObj = new CustomInvoice();			
						$transactionCustomInvoiceObj->CustomInvoice($each_custominvoice["custom_invoice_id"]);			
						if ($transactionCustomInvoiceObj->getNumber("id") > 0) {
							//if (Functions::string_strpos($url_base, "/".SITEMGR_ALIAS."") !== false) {
								?>
								<!--<a href="$url_base?>/custominvoices/view.php?id=<$each_custominvoice["custom_invoice_id"]?>"></a>-->
									{{ Functions::system_showTruncatedText($each_custominvoice["title"], 60) }}
							
							<?php
							}
						else {
							?>
					{{ Functions::system_showTruncatedText($each_custominvoice["title"], 60) }}							
							<?php
						}
						?>
					</fieldset>
				</th>
				<td class="infoAmount">
					{{ $each_custominvoice["amount"]." (".$transaction_n["transaction_currency"].")" }}
				</td>
			</tr>
	@endforeach
	</table>
	<br />
<?php } ?>
