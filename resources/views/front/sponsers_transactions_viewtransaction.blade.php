<?php
use App\Functions\Functions;
?>
@extends('front')

@section('content')

	<link rel="stylesheet" href="{{ asset('front/css/bootstrap.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('front/css/members.min.css') }}" />
	<link href="https://www.shoresummerrentals.com/custom/domain_1/theme/default/content_custom.min.css" rel="stylesheet" type="text/css" media="all">
    <!--link rel="stylesheet" href="{{ asset('front/css/bootstrap-responsive.min.css') }}" /-->
     
     
    
    
	<div class="view_invoiceContainer container members">

        <div class="container-fluid">

      <div class="row-fluid">
            
              <nav class="minor-nav">
                
                <ul>
                    <li>
                        <a href="<?php echo url('sponsors/billing'); ?>">Check Out</a>
                    </li>
                    <li>
                        <a class="active" href="<?php echo url('sponsors/transactions'); ?>">Transaction History</a>
                    </li>
                </ul>
                
            </nav>
            
        </div>

    
    <div>

        
<br>
<?php 
$str_time = "";
$start_time_am_pm="";

$startTimeStr = explode(":", $transaction["transaction_datetime"]);
$startTimeStr[0] = Functions::string_substr($startTimeStr[0],-2);
if (config('params.CLOCK_TYPE') == '24') {
    $start_time_hour = $startTimeStr[0];
} elseif (config('params.CLOCK_TYPE') == '12') {
    if ($startTimeStr[0] > "12") {
        $start_time_hour = $startTimeStr[0] - 12;
        $start_time_am_pm = "pm";
    } elseif ($startTimeStr[0] == "12") {
        $start_time_hour = 12;
        $start_time_am_pm = "pm";
    } elseif ($startTimeStr[0] == "00") {
        $start_time_hour = 12;
        $start_time_am_pm = "am";
    } else {
        $start_time_hour = $startTimeStr[0];
        $start_time_am_pm = "am";
    }
}
if ($start_time_hour < 10) $start_time_hour = "0".($start_time_hour+0);
$start_time_min = $startTimeStr[1];
$str_time .= $start_time_hour.":".$start_time_min." ".$start_time_am_pm;

$transac_date = explode(" ",$transaction["transaction_datetime"]);
?>

<br />
<?php if ($transaction["system_type"] == "manual") {?>
    <p class="informationMessage">{{ config('params.LANG_TRANSACTION_MANUAL') }}</p>
<?php } ?>

<h2 class="standardSubTitle">Transaction Info</h2>

	<ul class="general-item">
    <li>
        <strong>Payment type</strong>
        <?php
        if (($transaction["system_type"] != "simplepay") && ($transaction["system_type"] != "paypal") && ($transaction["system_type"] != "manual") && ($transaction["system_type"] != "pagseguro")) {
            echo config('params.LANG_CREDITCARD');
        } else {
            echo $transaction["system_type"];
        }
        if ($transaction["recurring"] == "y") {
            echo "&nbsp;<em>".config('params.LANG_MSG_PRICES_AMOUNT_PER_INSTALLMENTS')."</em>";
        }
        ?>
    </li>		
    <li><strong>Id</strong>{{ $transaction["transaction_id"] }}</li>
    <li><strong>Status:</strong>{{ $transaction["transaction_status"] }}</li>
    <li><strong>Date:</strong> {{ $transac_date[0]." - ".$str_time }}</li>
    <li><strong>IP:</strong> {{ $transaction["ip"] }}</li>
    <li><strong>Subtotal:</strong>{{ $transaction["transaction_subtotal"] }} ({{ $transaction["transaction_currency"] }})</li>
    <li><strong>Tax:</strong>{{ $transaction["transaction_tax"]}} ({{ $transaction["transaction_currency"]}})</li>
    <li><strong>Amount:</strong> {{ $transaction["transaction_amount"] }} ({{ $transaction["transaction_currency"] }})</li>
    <li><strong>Notes:</strong> {{ $transaction["notes"] }}</li>
	</ul>


<?php if (isset($transaction_listing_log)) { ?>
     <h2 class="standardSubTitle">Listing</h2>
     <table class="table-itemlist">
        <tbody>
        <tr>
            <th>Title</th>
            <th style="width:100px;">Extra Category</th>
            <th style="width:100px;">Level</th>
            <th style="width:120px;">Promotional Code</th>
             <th style="width:70px;">Renewal</th>
            <th style="width:100px;">Item Price</th>
        </tr>

      @foreach($transaction_listing_log as $each_listing) 

                <tr>
                <td>#{{ $each_listing["listing_id"] }} - {{ $each_listing["listing_address"] }} </td>
                <td style="text-align:center">{{ $each_listing["extra_categories"] }}</td>
                <td>{{ Functions::string_ucwords($each_listing["level_label"]) }}</td>
                <?php if (config('params.PAYMENT_FEATURE') == "on") { ?>
                <?php if ((config('params.CREDITCARDPAYMENT_FEATURE') == "on") || (  config('params.INVOICEPAYMENT_FEATURE')== "on")) { ?>

                <td style="text-align:center">{{ $each_listing["discount_id"] }}</td>
                <?php } ?>
               <?php } ?>     

                <td style="text-align:center">{{ $each_listing["renewal_date"] }}</td>
                <td style="text-align:center">{{ $each_listing["amount"]." (".$transaction["transaction_currency"].")" }}
         </td>
            </tr>
            @endforeach
                 
            </tbody>
            </table>
<?php } 

if (isset($transaction_listingchoice_log)) {
?>
	<h2 class="standardSubTitle">Listing Specials</h2>
	<table class="table-itemlist">
		<tbody><tr>
			<th>Title</th>
			<th style="width:70px;">Renewal</th>
			<th style="width:100px;">Item Price</th>
		</tr>

        @foreach($transaction_listingchoice_log as $each_listingchoice) 

			<tr>
				<td>#{{ $each_listingchoice["listing_id"] }} - {{ $each_listingchoice["badge_name"] }} </td>
				<td style="text-align:center">{{ $each_listingchoice["renewal_date"] }}</td>
				<td style="text-align:center">{{ $each_listingchoice["amount"]." (".$transaction["transaction_currency"].")" }}</td>
			</tr>
      @endforeach
			</tbody>
            </table>

<?php } ?>





    </div>


            </div><!-- Close container-fluid div -->
            
        </div>



		 <script type="text/javascript"> 
        
        if ($("#myChart").length) {
            //This will get the first returned node in the jQuery collection.
            var ctx = $("#myChart").get(0).getContext("2d");
        }

        function initializeDashboard() {
            $(".dial").knob({
                readOnly:   true,
                fgColor:    "#2980b9",
                bgColor:    "#DEE1E3",
                fontWeight: 300,
                thickness:  .2,
                width:      70,
                height:     70
            });

            $(".status, .floating-tip, .alert-new, #item_renewal").tooltip({
                animation: true,
                placement: "top"
            });
             
            if ($("#myChart").length) {
                //Get context with jQuery - using jQuery's .get() method.
                ctx = $("#myChart").get(0).getContext("2d");
                loadChart();
            }
        }

        $(function() {
            $("#alert").fadeOut(5000);
            initializeDashboard();
        });
        
        function showReply(id) {
            $('#review_reply'+id).css('display', '');
            $('#link_reply'+id).css('display', 'none');
            $('#cancel_reply'+id).css('display', '');
        }
        
        function hideReply(id) {
            $('#review_reply'+id).css('display', 'none');
            $('#link_reply'+id).css('display', '');
            $('#cancel_reply'+id).css('display', 'none');
        }
        
        function showLead(id) {
            $('#lead_reply'+id).css('display', '');
            $('#link_lead'+id).css('display', 'none');
            $('#cancel_lead'+id).css('display', '');
        }
        
        function hideLead(id) {
            $('#lead_reply'+id).css('display', 'none');
            $('#link_lead'+id).css('display', '');
            $('#cancel_lead'+id).css('display', 'none');
        }
        
        function saveReply(id) {
            $("#submitReply"+id).css("cursor", "default");
            $("#submitReply"+id).prop("disabled", "disabled");
            $("#submitReply"+id).html('Wait, Loading...');
            
            $.post("https://www.shoresummerrentals.com/sponsors/ajax.php", $("#formReply"+id).serialize(), function(data) {
                if (data == "ok") {
                    $("#msgReviewE"+id).css("display", "none");
                    $("#msgReviewS"+id).css("display", "");
                    $("#msgReviewS"+id).fadeOut(5000);
                } else {
                    $("#msgReviewE"+id).css("display", "");
                    $("#msgReviewS"+id).css("display", "none");
                }
                $("#submitReply"+id).html('Submit');
                $("#submitReply"+id).prop("disabled", "");
                $("#submitReply"+id).css("cursor", "pointer");
            });
        }
        
        function saveLead(id) {
            $("#submitLead"+id).css("cursor", "default");
            $("#submitLead"+id).prop("disabled", "disabled");
            $("#submitLead"+id).html('Wait, Loading...');
            
            $.post("https://www.shoresummerrentals.com/sponsors/ajax.php", $("#formLead"+id).serialize(), function(data) {
                if (data == "ok") {
                    $("#msgLeadE"+id).css("display", "none");
                    $("#msgLeadS"+id).css("display", "");
                    $("#msgLeadS"+id).fadeOut(5000);
                    setTimeout("leadBox('hide', "+id+");", 4000);
                    $("#title_replied"+id).css("display", "none");
                    $("#new_replied"+id).css("display", "");
                } else {
                    $("#msgLeadE"+id).html(data);
                    $("#msgLeadE"+id).css("display", "");
                    $("#msgLeadS"+id).css("display", "none");
                }
                $("#submitLead"+id).html('Submit');
                $("#submitLead"+id).prop("disabled", "");
                $("#submitLead"+id).css("cursor", "pointer");
            });
        }
        
        function reviewBox(option, id) {
            $("#reviews-list").children(".item-review").children(".review-detail").stop(true,true).slideUp();
            $("#reviews-list").children(".item-review").children(".review-summary").stop(true,true).slideDown().removeClass("new");
            if (option == "show") {
                $("#review-summary-"+id).slideUp(); 
                $("#review-detail-"+id).slideDown();
                setItemAsViewed("review", id);
            } else {
                $("#review-summary-"+id).slideDown(); 
                $("#review-detail-"+id).slideUp();
            }            
        }
        
        function leadBox(option, id) {
            $("#leads-list").children(".item-review").children(".review-detail").stop(true,true).slideUp();
            $("#leads-list").children(".item-review").children(".review-summary").stop(true,true).slideDown().removeClass("new");
            if (option == "show") {
                $("#lead-summary-"+id).slideUp(); 
                $("#lead-detail-"+id).slideDown();
                setItemAsViewed("lead", id);
            } else {
                $("#lead-summary-"+id).slideDown(); 
                $("#lead-detail-"+id).slideUp();
            }            
        }
        
        function dealBox(option, id) {
            $("#deals-list").children(".item-review").children(".review-detail").stop(true,true).slideUp();
            $("#deals-list").children(".item-review").children(".review-summary").stop(true,true).slideDown();
            if (option == "show") {
                $("#deal-summary-"+id).slideUp(); 
                $("#deal-detail-"+id).slideDown();
            } else {
                $("#deal-summary-"+id).slideDown(); 
                $("#deal-detail-"+id).slideUp();
            }            
        }
        
        function changeDealStatus(option, id, promocode) {
            $.post("https://www.shoresummerrentals.com/sponsors/deal/deal.php",{action: option, promotion_id: promocode}, function() {
                if (option == "freeUpDeal") {
                    $("#label_used"+id).css("display", "");
                } else {
                    $("#label_used"+id).css("display", "none");
                }
            });
        }
        
        function setItemAsViewed(type, id) {
            $.post("https://www.shoresummerrentals.com/sponsors/ajax.php", {
                ajax_type: 'setItemAsViewed',
                type: type,
                id: id
            }, function () {});
        }
       
        function loadDashboard(item_type, item_id) {
            $.post("<?php echo url('loadDashboard');?>", {
                ajax_type: 'load_dashboard',
                item_type: item_type,
                item_id: item_id,
                _token:'{{ csrf_token() }}'
            }, function (ret) {
                $(".webitem").removeClass("active");
                $("#"+item_type+"_"+item_id).addClass("active");
                scrollPage('#float_layer');
                $("#dashboard").hide().html(ret).fadeIn(800);
                initializeDashboard();
            });
        }

        function selectLegend(option, id, chartdata) {
            var countVisible = 0;
            
            if (option == "viewALL") {
                
                if ( $(".legend-ALL").hasClass("isvisible")) {
//                    $(".legend-ALL").removeClass("isvisible");
//                    $("#optionLegend > li > i").removeClass("checked");
//                    $("#optionLegend > li").removeClass("isvisible");
//                    $("#controlLegend > li").remove();
                } else {
                    countVisible = 2;
                    $("#optionLegend > li > i").addClass("checked");                   
                    $(".legend-ALL").addClass("isvisible");
                    $("#optionLegend > li").not(".isvisible").clone().appendTo("#controlLegend");
                    $("#optionLegend > li").addClass("isvisible");
                }
            } else {
                id: id
                chartdata: chartdata
                $newlegend = $(".legend-"+id).clone();

                if ($(".legend-"+id).hasClass("isvisible")) {
                    
                    //Check if there's at least one other legend selected to prevent empty chart
                    $('#optionLegend li').each(function() {
                        if ($(this).hasClass("isvisible")) {
                            countVisible++;
                        }
                    });

                    if (countVisible > 1) {
                        $(".legend-"+id).children("i").removeClass("checked");
                        $(".legend-"+id).removeClass("isvisible");
                        $("#controlLegend").children(".legend-"+id).remove();
                        $(".legend-ALL").children("i").removeClass("checked");
                        $(".legend-ALL").removeClass("isvisible");
                    }
                } else {
                    countVisible = 2;
                    $(".legend-"+id).children("i").addClass("checked");
                    $(".legend-"+id).addClass("isvisible");
                    $newlegend.appendTo("#controlLegend");
                }
            }
            if (countVisible > 1) {
                controlChart();
            }
        }
                
        function loadChart() {
            var data = {
                labels : chartLabels,
                datasets : initialReport
            };
            var steps = 5;
            var max = maxInitialReport;
            if (max < steps) {
                steps = max;
            }
            var options = {
                bezierCurve : false,
                scaleOverride: true,
                scaleSteps: steps,
                scaleStepWidth: Math.ceil(max / steps),
                scaleStartValue: 0
            };
            ctx = $("#myChart").get(0).getContext("2d");
            new Chart(ctx).Line(data, options);        
        }
       
        function controlChart() {
            
            var datasets = new Array();
            var max = 0;
            var thisHighest = 0;
            $('#optionLegend li').each(function() {
                if ($(this).hasClass("isvisible")) {
                    var reportType = $(this).attr('report');
                    if (reportType) {
                        datasets.push(window[reportType]);
                        thisHighest = Math.max.apply(Math, window[reportType].data);
                        if (thisHighest > max) {
                            max = thisHighest;
                        }
                    }
                }
            });
            
            var steps = 5;
            if (max < steps) {
                steps = max;
            }
            var options = {
                bezierCurve : false,
                scaleOverride: true,
                scaleSteps: steps,
                scaleStepWidth: Math.ceil(max / steps),
                scaleStartValue: 0
            };

            var data = {
                labels : chartLabels,
                datasets : datasets
            };
            ctx = $("#myChart").get(0).getContext("2d");
            new Chart(ctx).Line(data, options);  

        }


function scrollPage(position_id){
    
	if(!position_id){
		$position_id = '#resultsMap';
	}else {
		$position_id = position_id;
	}
	//jQuery('html,body').animate({scrollTop: jQuery($position_id).offset().top},'slow');
    jQuery('html, body').animate({ scrollTop: jQuery('#dashboard').offset().top }, 1500);
}
    </script>
	<script src="{{ asset('/front/js/Chart.js') }}"></script>		
@endsection