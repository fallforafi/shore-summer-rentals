@extends('front')

@section('content')


	
	<section class="inner-page contact-page">
		<div class="container">
			<h1>Privacy Policy</h1>
			<h3>ShoreSummerRentals.com Privacy Policy</h3>

			<p>At ShoreSummerRentals.com, we want you to know how we collect, use, share, and protect information about you. By interacting with the ShoreSummerRentals.com website you consent to the use of information that is collected or submitted as described in this privacy policy. We may change or add to this privacy policy, so we encourage you to review it periodically.</p>
			
			<p>IMPORTANT: We do not sell, rent or otherwise disclose your personal information to any third parties, including but not limited to advertisers, strategic partners or vendors. Again, we take your privacy very seriously.</p>
			
			
			<h4>What Information Is Collected?</h4>
			<p>At ShoreSummerRetals.com, we want you to know how we collect, use, share, and protect information about you.  At various times you may give your personal data to us. For example, in order to send a request or inquiry from you to a property owner or manager, we may need to know all or some of the following: your full name, address, telephone number, e-mail address, your current location, and, in the case of a member's listing, a method of payment. You may also give personal data to us at other times, such as when registering on the Site or when submitting an advertisement for a property.</p>
			
			<p>We also collect IP addresses, mobile device identifier details, your location, navigation and click-stream data, the time of accessing the website, homes you viewed, what you searched for, the duration of your visit, and other  details of your activity on the Site. If you ask us to connect with other sites (for example if you ask us to connect with your Facebook account) we may get information that way too. We may also gather information from Sessions or web beacons.</p>
			
			
			<p>A "Session" is a small file placed on your hard drive by some of our web pages.   We, or third parties we do business with, may use Sessions to help us analyze our web page flow, customize our services, content and advertising, measure promotional effectiveness and promote trust and safety. Sessions are commonly used at most major transactional websites in much the same way we use them here at our Site.</p>
			
			<p>You may delete and block all Sessions from this site, but parts of the site will not work. We want to be open about our Session use.  Even if you are only browsing the Site certain information (including computer and connection information, browser type and version, operating system and platform details, and the time of accessing the Site) is automatically collected about you. This information will be collected every time you access the Site and it will be used for the purposes outlined in this Privacy Policy.</p>
			
			
			<p>You can reduce the information Sessions collect from your device.  An easy way of doing this is often to change the settings in your browser. If you do that you should know that (a) your use of the Site may be adversely affected (and possibly entirely prevented), (b) your experience of this and other sites that use Sessions to enhance or personalize your experience may be adversely affected, and (c) you may not be presented with advertising that reflects the way that you use our and other, sites. You find out how to make these changes to your browser at this site: www.allaboutSessions.org/manage-Sessions/.  Unless you have adjusted your browser setting so that it will refuse Sessions, our system will send Sessions as soon as you visit our site.  By using the site you consent to this, unless you change your browser settings.</p>
			
			<p>We gather and share information concerning the use of the Site by members and travelers with one or more third-party tracking companies for the purpose of reporting statistics. To do this, some of the pages you visit on our Site use electronic images placed in the web page code, called pixel tags (also called "clear GIFs" or "web beacons") that can serve many of the same purposes as Sessions.</p>
			
			
			<p>Web beacons may be used to track the traffic patterns of users from one page to another in order to maximize web traffic flow. Our third-party advertising service providers may also use web beacons to recognize you when you visit the Site and to help determine how you found the Site.</p>
			
			
			
			<h4>Third-Party Automated Collection and Interest-Based Advertising</h4>
			
			<p>ShoreSummerRentals.com participates in interest-based advertising (IBA), also known as Online Behavioral Advertising. We use third-party advertising companies to display ads tailored to your individual interests based on how you browse and shop online. ShoreSummerRentals.com adheres to the Digital Advertising Alliance (DAA) self-regulatory principles for IBA.</p>

			<p>We allow third-party companies to collect certain information when you visit our websites or use our mobile applications. This information is used to serve ads for ShoreSummerRentals.com services when you visit this website or other websites. These companies use non-personally-identifiable information (e.g., click stream information, browser type, time and date, subject of advertisements clicked or scrolled over, hardware/software information, Session and Session ID) and personally identifiable information (e.g., static IP address) during your visits to this and other websites in order to provide advertisements about goods and services likely to be of greater interest to you. These parties typically use a Session, web beacon, or other similar tracking technologies to collect this information.</p>
			

			<p>Our Do Not Track Policy: Some browsers have a �do not track� feature that lets you tell websites that you do not want to have your online activities tracked. To learn more about IBA or to opt-out of this type of advertising, visit the Network Advertising Initiative website and the Digital Advertising Alliance website. Options you select are browser and device-specific.</p>
			
			<h4>Social Media</h4>

			<p>ShoreSummerRentals.com engages with users on multiple social media platforms (e.g., Facebook, Twitter, and Pinterest). If you contact us on one of our social media platforms, or request information via social media we may contact you via direct message. In these instances, your interactions with us are governed by this privacy policy as well as the privacy policy of the social media platform you use.</p>
			
			
			
			<h4>Social Media Widgets</h4>

			<p>Our site includes social media features, such as the Facebook Like button, Google Plus, Pinterest, and Twitter widgets. These features may collect information about your IP address and which page you�re visiting on our site, and they may set a Session or employ other tracking technologies. Social media features and widgets are either hosted by a third party or hosted directly on our site. Your interactions with those features are governed by the privacy policies of the companies that provide them.</p>
			
			
			<h4>Opting Out of Communications</h4>
			<p>We will contact you from time to time for marketing purposes. Unless you have opted out, this could include contacting you by phone or email.</p>
			
			<p>You may opt-out of receiving marketing communications from us by the following means:<br/>
			1.    Contact us at <a href='http://www.shoresummerrentals.com/contactus.php'>http://www.shoresummerrentals.com/contactus.php</a><br/>
			2.    Follow the unsubscribe instructions included in each email or newsletter;<br/>
			3.    Call us at 609-677-1580 <br/>
			</p>
			
		</div>
	</section>
						

					
@endsection