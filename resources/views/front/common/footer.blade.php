<?php 
use App\Functions\Functions;
?>
    <footer>
		<section class="ftr-quote quote-area col-sm-12 bg-orange clrhm p20 white text-center footer-hdr">
				<div class="go2top gotoTop">
                    <a href="#">
                        <i class="fa fa-chevron-up"></i><small>TOP</small></a>
                </div>
			<div class="container">
				<h2 class="h1">"We Bring Owners and Renters Together"</h2>
			</div>
		</section>

		<section class="ftr col-sm-12 bg-blue white links-orange text-center footer-wrapper">
			<div class="container">

				<div class="ftr-logo"><img src="{{ asset('front/images/logo.png') }}" alt="" /></div>
				
				<div class="social-area clrlist icon2x">
					
                    
                     <?php if(Session::get('SESS_ACCOUNT_ID') || isset($login)=='Login') {?>
                    <ul class="social-footer-links">
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                        <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                        <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        <li><a href="#"><i class="fa fa-vimeo-square"></i></a></li>
                    </ul>
                    <?php }else
                    { ?>
                        <ul>
						<li><a href="#"><i class="fa fa-facebook"></i></a></li>
						<li><a href="#"><i class="fa fa-twitter"></i></a></li>
					</ul>
                   <?php } ?>
                					
				</div>
                
                
                
				
				<p>Join <a href="{{url()}}" target="_blank">ShoreSummerRentals.com</a> and <a href="http://www.vacationrentals2u.com/" target="_blank">VacationRentals2U.com</a> Now!</p>
				
				<p>P.O. Box 55, Somers Point, NJ 08244 | Phone: 609-677-1580 | Fax: 609-677-1590</p>
				
				<div class="text-center visa-card-footer"><img src="{{ asset('front/images/credit-cards.png') }}">
                <?php if(Session::get('SESS_ACCOUNT_ID')) {?>
                <script type="text/javascript" src="https://seal.thawte.com/getthawteseal?host_name=www.shoresummerrentals.com&amp;size=S&amp;lang=en" class="__web-inspector-hide-shortcut__"></script>
                <a href="https://sealinfo.thawte.com/thawtesplash?form_file=fdf/thawtesplash.fdf&amp;dn=WWW.SHORESUMMERRENTALS.COM&amp;lang=en" tabindex="-1" onmousedown="return v_mDown(event);" target="THAWTE_Splash" class="__web-inspector-hide-shortcut__"><img name="seal" border="true" src="https://seal.thawte.com/getthawteseal?at=0&amp;sealid=2&amp;dn=WWW.SHORESUMMERRENTALS.COM&amp;lang=en&amp;gmtoff=-300" oncontextmenu="return false;" alt="Click to Verify - This site has chosen a thawte SSL Certificate to improve Web site security"></a>
                <?php } ?>
                </div>
				<?php if(!Session::get('SESS_ACCOUNT_ID') && !isset($login)=='Login')  {
                ?>
				<div class="ftr__nav clrlist mt30 mb20">
					<ul class="listdvr">
						<li><a href="{{ url('advertise') }}">List Your Rental</a></li>
						<li><a href="{{ url('advsearch')}}">Find a Rental</a></li>
                        <?php 
                        $tempPageurl =   url('');
                             
                            $viewUrl = '/view';
                            $featureUrl = '/feature';
                            $specialsUrl = '/specials';
                            if(strpos($tempPageurl, $viewUrl) !== false)
                            {
                                $explodedTemp = explode($viewUrl,$tempPageurl);
                                $tempPageurl = $explodedTemp[0];
                            }
                            elseif(strpos($tempPageurl, $featureUrl) !== false)
                            {
                                $explodedTemp = explode($featureUrl,$tempPageurl);
                                $tempPageurl = $explodedTemp[0];
                            }
                            elseif(strpos($tempPageurl, $specialsUrl) !== false)
                            {
                                $explodedTemp = explode($specialsUrl,$tempPageurl);
                                $tempPageurl = $explodedTemp[0];
                            }
                        $specials = Functions::footerSpecials();
                        if(isset($specials) && !empty($specials))
                        { 
                            foreach($specials as $val){ 
                          $filter_special_name = str_replace(" ", '', strtolower($val->name));
                                 switch ($filter_special_name) {
                                    case 'beachfrontrentals':
                                        
                                            $thisLink = $tempPageurl.'/view-ocean_front';
                                       
                                        break;                                    
                                    case 'bayfrontrentals':
                                        
                                            $thisLink = $tempPageurl.'/view-ocean_front';
                                        
                                        break;
                                    case 'petfriendlyrentals':
                                        
                                             $thisLink = $tempPageurl.'/feature-petsallowed';
                                        
                                        break;
                                    default:
                                        
                                             $thisLink = $tempPageurl.'/specials-'.$filter_special_name;
                                        
                                        break;
                                }   	
                            	
                            	?>


                            <li><a href="<?php echo $thisLink.'/orderby-city'; ?>"><?php echo $val->name; ?></a></li>
					
                       <?php }  }
                        ?>
					
						<!--<li><a href="orderby-city.php#!specials-forsale/orderby-city">Properties for Sale</a></li>
						<li><a href="new-jersey.php">New Jersey Rentals</a></li>
						<li><a href="florida.php">Florida Rentals</a></li> 
						<li><a href="maryland.php">Maryland Rentals</a></li>
						<li><a href="north-carolina.php">North Carolina Rentals</a></li>
						<li><a href="delaware.php">Delaware Rentals</a></li>-->
						<li><a href="{{ url('new-jersey'.'.html') }}">New Jersey Rentals</a></li>

						<li><a href="{{ url('faqs'.'.html') }}">FAQ</a></li>
						<li><a href="http://shoresummerrentals.us2.list-manage.com/subscribe?u=3fd02cb749e6ae50b2a8ebc8a&amp;id=68363bfde1">Submit Rental Request</a></li>
						<li><a href="{{ url('privacy-policy'.'.html') }}">Privacy Policy</a></li>
					</ul>
				</div>
                <?php } ?>
				
				<div class="clearfix"></div>
				
				<p class="powered-by">Powered by <a href="http://www.golpik.com/" target="_blank">Golpik</a></p>
				
				<p class="copyright"><?php echo isset($footer_text[0]->value)?$footer_text[0]->value:'Copyright �  '.date('Y').'  Shore Summer Rentals, LLC, All Rights Reserveds' ?></p>
				<?php if(!isset($login)=='Login') { ?>
				<p><a href="{{ url('terms-of-use'.'.html') }}">Terms of Use</a> &amp; 
				<a href="{{ url('legal-disclaimer'.'.html') }}">Legal Disclaimer</a></p>
				<?php } ?>
    <span style="color: #0d66b2;" ><?php
    
    $time = microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"];
    echo "Created in {$time} seconds";
    ?></span>
				
			</div>
		</section>
	

	</footer>
	<a href="" class="scrollToTop gotoTop"><i class="fa fa-arrow-up"></i></a>