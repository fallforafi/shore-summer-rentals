<body class="old_home">
<header>
	<style>
    
    .notify {
    position: absolute;
    width: 20px;
    height: 20px;
    background-color: #16a085;
    color: #fff;
    font-size: 10px;
    font-style: normal;
    font-weight: 600;
    right: 0;
    top: 0;
    border-radius: 20px;
    pointer-events: none;
}
.dropdown .dropdown-menu .nav-header {
    padding-right: 20px;
    padding-left: 20px;
}
.nav-header {
    padding: 3px 15px;
    font-size: 11px;
    color: #999;
}
</style>

<script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>

<?php 
use App\Functions\Functions;
?>
   
    <section class="hdr-top bg-navy white clrlist listview" >
		<div class="container">
			<a class="navbar-brand" href="{{ url() }}"><img src="{{ asset('front/images/logo.png') }}" alt="logo" /></a>
			
			
			
			 <ul class="login-area pul-rgt ">
            <?php if(Session::get('SESS_ACCOUNT_ID') || Session::get('SESS_LOGIN'))  {

                ?>
                
				<li class="text-right"><h2>"We Bring Owners and Renters Together"</h2></li>
                <?php
            }else
            { ?>
           
                <li><a class="owner-login" href="{{ url('login') }}"><i class="fa fa-chevron-right"></i>OWNERS LOGIN</a></li>
				<li class="text-right"><a class="btn" href="{{ url('advertise') }}">LIST YOUR PROPERTY</a>
				<h2>"We Bring Owners and Renters Together"</h2></li>
                
          <?php   }?>
				
			</ul>
				
		</div>
		<div class="search__hdr">
			<i class="fa fa-search"></i>
		</div>
		</div>
	</section>
<script>
$(document).ready(function(){
    $(".search__hdr").click(function(){
        $(".slider-search-area-toggle").toggle();
    });
});
</script>
	
	<section class="hdr-nav bg-orange white cross-toggle" data-navitemlimit="">
		
        
        
                <div class="container">
			<nav class="navbar navbar-default" role="navigation" id="slide-nav0">
			  <div class="container-fluid">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
				  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				  </button>
				</div>
				<?php if(Session::get('SESS_ACCOUNT_ID') || !Session::get('SESS_LOGIN'))  {
                ?>
				<div class="date pul-lft" id="date_time" >  
				<span id="clock"></span></div>
                <?php } ?>
				
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div id="slidemenu">
					<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        
         <?php if(Session::get('SESS_ACCOUNT_ID')  && Session::get('SESS_LOGIN'))  {


             if(!Session::get('SESS_DASH'))  {   
                ?>
            

                    <ul class="nav navbar-nav" id="ul_main_menu">
                        
                        <li class="menuActived">
                            <a href="{{ url('sponsors') }}">Dashboard</a>
                        </li>

                        <li>
      <a href="{{ url('sponsors/billing') }}">Billing<i class="notify" id="billing_notify"></i></a>
                        </li>

                        <li>
                            <a href="{{ url('sponsors/account') }}">Account</a>
                        </li>

                                           </ul>
                  <?php } ?>

                    <ul class="nav pull-right">
                        
                            <li class="dropdown">
                                <a href="javascript: void(0);" class="sign-up dropdown-toggle">
                                    <i class="fa fa-cogs"></i>
                                </a>

                                <ul class="dropdown-menu">

                                    <li>
                                        <a href="{{ url('profile') }}">Profile</a>
                                    </li>

                                    <li>
                                        <a href="{{ url() }}">Back to Website</a>
                                    </li>

                                    <li>
                                        <a href="{{ url('sponsors/help') }}">Help</a>
                                    </li>

                                    <li>
                                        <a href="{{ url('sponsors/faq') }}">FAQ</a>
                                    </li>

                                    <li>
                                        <a href="{{ url('logout') }}">Sign out</a>
                                    </li>

                                </ul>

                            </li>

                        
                    </ul>

            <?php }?>
            @if(!Session::get('SESS_LOGIN'))
					  <ul class="nav navbar-nav hdr-nav-ul">
						<li class="dropdown">
						  <a href="#!4" class="dropdown-toggle" data-toggle="dropdown">Renters</a>
							<ul class="dropdown-menu">
								<li><a href="{{ url('advsearch') }}"><i class="fa fa-chevron-right"></i> Find a Rental (Advanced)</a></li>
								
                                
                                								<?php 
                                                                
                                                                
                        $specials = Functions::getSpecilas();
                        if(isset($specials) && !empty($specials))
                        { 
                            $tempPageurl =   url('');
                             
                            $viewUrl = '/view';
                            $featureUrl = '/feature';
                            $specialsUrl = '/specials';
                            if(strpos($tempPageurl, $viewUrl) !== false)
                            {
                                $explodedTemp = explode($viewUrl,$tempPageurl);
                                $tempPageurl = $explodedTemp[0];
                            }
                            elseif(strpos($tempPageurl, $featureUrl) !== false)
                            {
                                $explodedTemp = explode($featureUrl,$tempPageurl);
                                $tempPageurl = $explodedTemp[0];
                            }
                            elseif(strpos($tempPageurl, $specialsUrl) !== false)
                            {
                                $explodedTemp = explode($specialsUrl,$tempPageurl);
                                $tempPageurl = $explodedTemp[0];
                            }
                            
                            $thisLink='';
                            foreach($specials as $val){ 
                                 $filter_special_name = str_replace(" ", '', strtolower($val->name));
                                 switch ($filter_special_name) {
                                    case 'beachfrontrentals':
                                        
                                            $thisLink = $tempPageurl.'/view-ocean_front';
                                       
                                        break;                                    
                                    case 'bayfrontrentals':
                                        
                                            $thisLink = $tempPageurl.'/view-ocean_front';
                                        
                                        break;
                                    case 'petfriendlyrentals':
                                        
                                             $thisLink = $tempPageurl.'/feature-petsallowed';
                                        
                                        
                                       
                                        break;
                                    default:
                                        
                                             $thisLink = $tempPageurl.'/specials-'.$filter_special_name;
                                        
                                        
                                        break;
                                }
                                
                             ?>
                            <li><a href="<?php echo $thisLink.'/orderby-city'; ?>"><i class="fa fa-chevron-right"></i><?php echo $val->name; ?></a></li>
					
                       <?php }  }
                        ?>
						    
                                
                                <li><a href="http://shoresummerrentals.us2.list-manage.com/subscribe?u=3fd02cb749e6ae50b2a8ebc8a&amp;id=68363bfde1"><i class="fa fa-chevron-right"></i> Submit Rental Request</a></li>
								<li><a href="{{ url('fraud-alert-renters.html') }}"><i class="fa fa-chevron-right"></i> Rental Fraud Alert</a></li>
							</ul>
						</li>
						
						<li class="dropdown">
						  <a href="#!4" class="dropdown-toggle" data-toggle="dropdown">Owners</a>
							<ul class="dropdown-menu" style="margin:0">
								<li><a href="{{ url('advertise') }}"><i class="fa fa-chevron-right"></i> List Your Rental</a></li>
								<li><a href="{{ url('listing-instructions.html') }}"><i class="fa fa-chevron-right"></i> Listing Instructions</a></li>
								<li><a href="{{ url('guarantee-and-verify.html') }}"><i class="fa fa-chevron-right"></i> Rental Guarantee</a></li>
								<li><a href="{{ url('cash-back-referral.html') }}"><i class="fa fa-chevron-right"></i> Cash Back Referrals</a></li>
								<li><a href="{{ url('login') }}"><i class="fa fa-chevron-right"></i> Login</a></li>
							</ul>
						</li>
						
						<li class="dropdown">
						  <a href="#!4" class="dropdown-toggle" data-toggle="dropdown">Testimonials</a>
							<ul class="dropdown-menu" style="margin:0">
                                <li><a href="{{ url('testimonials-owners.html') }}"><i class="fa fa-chevron-right"></i> Owners Testimonials</a></li>
							</ul>
						</li>
						
						<li class="dropdown">
						  <a href="#!4" class="dropdown-toggle" data-toggle="dropdown">Useful Links</a>
							<ul class="dropdown-menu" style="margin:0">
								<li><a href="{{ url('login') }}"><i class="fa fa-chevron-right"></i> Renew Membership</a></li>
								<li><a href="{{ url('fraud-alert-owner.html') }}"><i class="fa fa-chevron-right"></i> Owner Fraud Alert</a></li>
								<li><a href="{{ url('faqs.html') }}"><i class="fa fa-chevron-right"></i> FAQ's</a></li>
								<li><a href="{{ url('spam-warning.html') }}"><i class="fa fa-chevron-right"></i> Spam Warning</a></li>
								<li><a href="{{ url('for-rent-sign-request.html') }}"><i class="fa fa-chevron-right"></i> Rental Sign Form</a></li>
								<li><a href="{{ url('photography-services.html') }}"><i class="fa fa-chevron-right"></i> Photography Services</a></li>
							</ul>
						</li>
						
						
						<li class="dropdown">
						  <a href="#!4" class="dropdown-toggle" data-toggle="dropdown">About Us</a>
								<ul class="dropdown-menu" style="margin:0">
									<li><a href="{{ url('our-team.html') }}"><i class="fa fa-chevron-right"></i> Our Team</a></li>
									<li><a href="{{ url('why-choose-us.html') }}"><i class="fa fa-chevron-right"></i> Why Choose Us</a></li>
								</ul>
						</li>
						
						<li class="dropdown">
						  <a href="#!4" class="dropdown-toggle" data-toggle="dropdown">Contact Us</a>
							<ul class="dropdown-menu" style="margin:0">
								<li><a href="{{ url('contact-us') }}"><i class="fa fa-chevron-right"></i> Contact Us Form</a></li>
								<li><a href="http://shoresummerrentals.us2.list-manage.com/subscribe?u=3fd02cb749e6ae50b2a8ebc8a&amp;id=68363bfde1"><i class="fa fa-chevron-right"></i> Rental Request Form</a></li>
							</ul>
						</li>
						
						
						<li><a href="https://www.shoresummerrentals.com/blog/">Blog</a></li>
						<li><a href="{{ url() }}">Home</a></li>
					  </ul>
 @endif
@if(Session::get('SESS_ACCOUNT_ID') && !Session::get('SESS_LOGIN'))
           
      
                       <ul class="nav pull-right -nav">
            
            
                <li class="dropdown">
                    
                    <a href="javascript:void(0);" class="sign-up dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-user"></i><b class="caret"></b>
                    </a>
                    
                    <ul class="dropdown-menu">
                        
                        <li class="nav-header">
                            Welcome {{ Session::get('SESS_ACCOUNT_NAME') }}!
                        </li>
                        
                        <li class="divider"></li>
                        
                        <li>
                            <a href="<?php echo url('sponsors'); ?>">Dashboard</a>
                        </li>
                                                  
                                                <li>
                            <a href="<?php echo url('sponsors/accounts'); ?>">Account</a>
                        </li>
                        
                        <li>
                            <a href="<?php echo url('logout'); ?>" class="sign-up">Sign out</a>
                        </li>
                        
                    </ul>
                    
                </li>
 </ul>

@endif

            
					</div><!-- /.navbar-collapse -->
				</div>
			  </div><!-- /.container-fluid -->
			</nav>
		</div>
	</section>
</header>
 <?php if(Session::get('SESS_ACCOUNT_ID') || !Session::get('SESS_LOGIN'))  {
                ?>
<script>

window.onload = date_time('date_time');
function date_time(id)
{
        date = new Date;
        year = date.getFullYear();
        month = date.getMonth();
        months = new Array('January', 'February', 'March', 'April', 'May', 'June', 'Jully', 'August', 'September', 'October', 'November', 'December');
        d = date.getDate();
        day = date.getDay();
        days = new Array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');
        h = date.getHours();
        if(h<10)
        {
                h = "0"+h;
        }
        m = date.getMinutes();
        if(m<10)
        {
                m = "0"+m;
        }
        s = date.getSeconds();
        if(s<10)
        {
                s = "0"+s;
        }
        result = ''+months[month]+' '+d+' '+year+' | <span id="clock" >'+h+':'+m+':'+s+' </span>';
        document.getElementById(id).innerHTML = result;
        setTimeout('date_time("'+id+'");','1000');
        return true;
}
</script>
<?php } ?>