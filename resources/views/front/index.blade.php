@extends('front')

@section('content')


<?php
//d(config('params.imagesPath'),1);
?>

	<section class="slider-area fadeft">
		<div class="container0">
		<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
		  <!-- Indicators -->
		  <ol class="carousel-indicators">
			<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
			<li data-target="#carousel-example-generic" data-slide-to="1"></li>
			<li data-target="#carousel-example-generic" data-slide-to="2"></li>
			<li data-target="#carousel-example-generic" data-slide-to="3"></li>
		  </ol>

		
		   <!-- Wrapper for slides -->
            <div class="carousel-inner">
             @foreach($sliders as $key=>$slider)
             <?php //echo config('params.imagesPath').$slider->prefix.'photo_'.$slider->image_id.".".strtolower($slider->type);?>
             <?php
             
            // d($slider,1);
             ?>
                <div class="item <?php echo ($key==0)?'active':''; ?>">
                  <img src="<?php echo config('params.imagesPath').$slider->prefix.'photo_'.$slider->image_id.".".strtolower($slider->type);?>" alt="<?php echo $slider->alternative_text; ?>" />
                </div>
             
              @endforeach
                
            </div>
			

		  <a class="carousel-control left" href="#carousel-example-generic" data-slide="prev" style="z-index: 1;">‹</a>
		  <a class="carousel-control right" href="#carousel-example-generic" data-slide="next" style="z-index: 1;">›</a>
		</div>
		
		
		
		
		<div class="slider-vdo col-sm-3" data-toggle="modal" data-target="#myModalvdo">
			<img src="{{ asset('front/images/vdo.jpg') }}" alt="">
			<i class="fa fa-play-circle"></i>
		</div>
		<!-- Modal -->
		<div class="modal fade" id="myModalvdo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		  <div class="modal-dialog" role="document">
			<div class="modal-content">
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Media Box Video Title</h4>
			  </div>
			  <div class="modal-body">
				<iframe src="https://player.vimeo.com/video/41307155" width="100%" height="350" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
			  </div>
			</div>
		  </div>
		</div>
		


		</div>
		<div class="search-area slider-search-area slider-search-area-toggle" ng-show="showMe">
			<div class="container">
			<form  class="form" name="search_form" id="search_form" method="get" action="" onSubmit="return CheckForm()"  >
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<div class="form-group col-sm-6">
					<input type="text" name="keyword" class="form-control keyword ac_input" id="keyword_resp" placeholder="Search by address, city, state, zip or rental id" value="" autocomplete="off">
				</div>


				<div class="form-group col-sm-2 search-checkin input-append">
				  <input id="searchCheckin" name="checkin" class="form-control hasDatepicker" type="text" value="" placeholder="check in" data-provide="datepicker">
				  <span class="add-on"><i class="fa fa-calendar"></i></span>
				</div>

				<div class="form-group col-sm-2 search-checkin input-append input-append-two">
				  <input id="searchCheckout" name="checkout" class="form-control hasDatepicker" type="text" value="" placeholder="check out" data-provide="datepicker">
				  <span class="add-on"><i class="fa fa-calendar"></i></span>
				</div>

				<div class="form-group col-sm-2 search-button text-center">
					<button type="submit" class="btn btn-info btn-search form-control" id="search_form_submit"> <i class="fa fa-search"></i> Search</button>
				</div>
			</form>
			<div class="fom-bot">
				<div class="col-sm-6">
				<span class="nj-viewall"><a class="pul-lft" href="new-jersey.html"><i class="fa fa-chevron-right"></i> View all NJ Rentals</a></span>
					&nbsp;
				</div>
				<div class="col-sm-6 fullwidth"> 
					<p class="inline-blok pul-rgt"><a href="advsearch"><i class="fa fa-chevron-right"></i> Advanced search</a></p>
				</div>
			</div>

			</div>
		</div>
	</section>
	
	

    
	<?php //echo isset($summer_sale[0]->content)?$summer_sale[0]->content:''; ?>
	<div class="section-border-top-red"></div>
	<section class="summersale-area bg-orange white links-white clrlist clrhm p20 mb30 summersale-area-section">
			<div class="container">
			<ul>
				<li class="col-sm-6">
					<h1><a href="{{ url('new-jersey'.'.html') }}"><span>VIEW ALL NJ RENTALS - NO FEES<!--JULY 4th DEALS--></span></a></h1>
				</li>
				<li class="col-sm-2 price-starting"><strong>►►►►►►►</strong></li>
				<li class="col-sm-4 view-deals-btn"><strong><a class="btn btn-primary" href="{{ url('advsearch') }}">OR PERFORM ADVANCED SEARCH<!--VIEW DEALS--></a></strong></li>
			</ul>
			</div>
			
	</section>
    <?php /* ?>
    
    
	<section class="summersale-area bg-red white links-white clrlist clrhm p20 mb30">
			<div class="container">
			<ul>
				<li class="col-sm-6">
					<h2><a href="new-jersey.php"><span>VIEW ALL NJ RENTALS - NO FEES</span></a></h2>
				</li>
				<li class="col-sm-2 price-starting"><strong>►►►►►►►</strong></li>
				<li class="col-sm-4"><strong><a class="btn btn-primary" href="advsearch.php">OR PERFORM ADVANCED SEARCH</a></strong></li>
			</ul>
			</div>
			</div>
	</section>
	<?php */ ?>
		<section class="sm-area col-sm-12 p0">
		<div class="container">
<div class="social-media">
    <style>
    .fb-like.fb_iframe_widget {
    position: relative;
    top: -5px;
}
</style>
        <div id="fb-root"></div>
        <script>(function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0";
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>
        <div class="fb-like fb_iframe_widget" data-href="https://www.facebook.com/ShoreSummerRent" data-layout="button_count" data-action="like" data-show-faces="false" data-share="false" fb-xfbml-state="rendered" fb-iframe-plugin-query="action=like&amp;app_id=&amp;container_width=1200&amp;href=https%3A%2F%2Fwww.facebook.com%2FShoreSummerRent&amp;layout=button_count&amp;locale=en_US&amp;sdk=joey&amp;share=false&amp;show_faces=false"><span style="vertical-align: bottom; width: 82px; height: 20px;"><iframe name="f11620613c" width="1000px" height="1000px" frameborder="0" allowtransparency="true" allowfullscreen="true" scrolling="no" title="fb:like Facebook Social Plugin" src="//www.facebook.com/v2.0/plugins/like.php?action=like&amp;app_id=&amp;channel=https%3A%2F%2Fstaticxx.facebook.com%2Fconnect%2Fxd_arbiter.php%3Fversion%3D42%23cb%3Dfff74470c%26domain%3Dwww.shoresummerrentals.com%26origin%3Dhttps%253A%252F%252Fwww.shoresummerrentals.com%252Ff323417c%26relation%3Dparent.parent&amp;container_width=1200&amp;href=https%3A%2F%2Fwww.facebook.com%2FShoreSummerRent&amp;layout=button_count&amp;locale=en_US&amp;sdk=joey&amp;share=false&amp;show_faces=false" style="border: none; visibility: visible; width: 82px; height: 20px;" class=""></iframe></span></div>
        &nbsp;&nbsp;
                <iframe id="twitter-widget-1" scrolling="no" frameborder="0" allowtransparency="true" class="twitter-follow-button twitter-follow-button-rendered" title="Twitter Follow Button" src="//platform.twitter.com/widgets/follow_button.c60d5b0c77548151ea09cd595348dc2d.en.html#dnt=false&amp;id=twitter-widget-1&amp;lang=en&amp;screen_name=shoresummerrent&amp;show_count=false&amp;show_screen_name=false&amp;size=m&amp;time=1457706177900" style="position: static; visibility: visible; width: 62px; height: 20px;" data-screen-name="shoresummerrent"></iframe>
        <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
        &nbsp;&nbsp;
        <!--<span class="PIN_1457706178102_button_pin PIN_1457706178102_white" data-pin-log="button_pinit" data-pin-href="//www.pinterest.com/pin/create/button/?guid=4PwPZs0LkDM4-2&amp;url=http%3A%2F%2Fwww.shoresummerrentals.com&amp;media=http%3A%2F%2Fshoresummerrentals.arcastaging.com%2Fcustom%2Fdomain_1%2Ftheme%2Fdefault%2Fschemes%2Fdefault%2Fimages%2Fimagery%2Fimg-logo.png&amp;description=Shore%20Summer%20Rentals"></span>
        -->
        <a data-pin-do="buttonPin" href="https://www.pinterest.com/pin/create/button/?url=https%3A%2F%2Fwww.shoresummerrentals.com%2F&media=https%3A%2F%2Fwww.shoresummerrentals.com%2F&description=Shore%20Summer%20Rentals"><img src="//assets.pinterest.com/images/pidgets/pinit_fg_en_rect_gray_20.png" /></a>
  <script async defer src="//assets.pinterest.com/js/pinit.js"></script>
        &nbsp;&nbsp;
                <div id="___follow_1" style="text-indent: 0px; margin: 0px; padding: 0px; border-style: none; float: none; line-height: normal; font-size: 1px; vertical-align: baseline; display: inline-block; width: 85px; height: 20px; background: transparent;"><iframe frameborder="0" hspace="0" marginheight="0" marginwidth="0" scrolling="no" style="position: static; top: 0px; width: 85px; margin: 0px; border-style: none; left: 0px; visibility: visible; height: 20px;" tabindex="0" vspace="0" width="100%" id="I1_1457706177228" name="I1_1457706177228" src="https://apis.google.com/_/widget/render/follow?usegapi=1&amp;annotation=none&amp;height=20&amp;rel=publisher&amp;hl=en&amp;origin=https%3A%2F%2Fwww.shoresummerrentals.com&amp;url=https%3A%2F%2Fplus.google.com%2F106846742271038575073&amp;gsrc=3p&amp;jsh=m%3B%2F_%2Fscs%2Fapps-static%2F_%2Fjs%2Fk%3Doz.gapi.en.JWo_OjA4q2A.O%2Fm%3D__features__%2Fam%3DAQ%2Frt%3Dj%2Fd%3D1%2Frs%3DAGLTcCNPhCA-PC_mDXRAihObAM25P7ZD8w#_methods=onPlusOne%2C_ready%2C_close%2C_open%2C_resizeMe%2C_renderstart%2Concircled%2Cdrefresh%2Cerefresh%2Conload&amp;id=I1_1457706177228&amp;parent=https%3A%2F%2Fwww.shoresummerrentals.com&amp;pfname=&amp;rpctoken=32909721" data-gapiattached="true"></iframe></div>
    </div>
	
		</div>
	</section>
	<section class="special-area section-specials-section">
		<div class="container">
		
		<div class="hed crossline"><h2>SPECIALS SECTION</h2><hr/></div>
				
				
			<div class="special-lists-area special-lists-area-main clrlist">
				<ul>
                <?php $tempPageurl =   url('');
                             
                            $viewUrl = '/view';
                            $featureUrl = '/feature';
                            $specialsUrl = '/specials';
                            if(strpos($tempPageurl, $viewUrl) !== false)
                            {
                                $explodedTemp = explode($viewUrl,$tempPageurl);
                                $tempPageurl = $explodedTemp[0];
                            }
                            elseif(strpos($tempPageurl, $featureUrl) !== false)
                            {
                                $explodedTemp = explode($featureUrl,$tempPageurl);
                                $tempPageurl = $explodedTemp[0];
                            }
                            elseif(strpos($tempPageurl, $specialsUrl) !== false)
                            {
                                $explodedTemp = explode($specialsUrl,$tempPageurl);
                                $tempPageurl = $explodedTemp[0];
                            }
					foreach($sp_images as $sp_image)  
                     {
                    $filter_special_name = str_replace(" ", '', strtolower($sp_image->name));
                                 switch ($filter_special_name) {
                                    case 'beachfrontrentals':
                                        
                                            $thisLink = $tempPageurl.'/view-ocean_front';
                                       
                                        break;                                    
                                    case 'bayfrontrentals':
                                        
                                            $thisLink = $tempPageurl.'/view-ocean_front';
                                        
                                        break;
                                    case 'petfriendlyrentals':
                                        
                                             $thisLink = $tempPageurl.'/feature-petsallowed';
                                        
                                        break;
                                    default:
                                        
                                             $thisLink = $tempPageurl.'/specials-'.$filter_special_name;
                                        
                                        
                                        break;
                                }
                    ?>
						<li class="col-sm-3"><a href="<?php echo $thisLink.'/orderby-city'; ?>"><img src="<?php echo config('params.imagesPath').$sp_image->prefix.'photo_'.$sp_image->image_id.".".strtolower($sp_image->type);?>" alt="<?php echo $sp_image->name; ?>" /></a></li>
					<?php } ?>

				</ul>
			</div>
		</div>
	</section>
	

	
	<section class="special-area special-area-two">
		<div class="container">
			
              <div class="vacation-cont col-sm-12 p0">
                    
                    
                        
                            <div class="advert-area list-col-4 col-sm-12 p0">
                                <ul>
                                    @foreach($banner_images as $banner_image)
                                    
                                        <li>
										<a href="<?php echo $banner_image->destination_protocol.$banner_image->destination_url; ?>">
										<img src="<?php echo config('params.imagesPath').$banner_image->prefix.'photo_'.$banner_image->image_id.".".strtolower($banner_image->type);?>" alt="<?php echo $banner_image->caption; ?>" /></a></li>
                                    @endforeach	
                                </ul>
                            </div>
                        
                        
                        
                      
                  
                </div>
                

		</div>
	</section>
	
	<section class="main-area featured-rental">
		<div class="container">
			
			<div class="main__lft col-sm-12 p0 ">
				<div class="featured-area col-sm-12 p0 imgzoom-hover">

					<div class="hed crossline"><h2>FEATURED RENTAL PROPERTIES</h2><hr/></div>
					@foreach($featured_images as $frp_image)

					<div class="featured-box cols-5">
						<div class="featured__inr">
							<div class="featured__img">
								<img src="<?php echo config('params.imagesPath').$frp_image->prefix.'photo_'.$frp_image->image_id.".".strtolower($frp_image->type);?>" alt="<?php echo $frp_image->title; ?>" />
							</div>
							<div class="featured__cont">
								<ul>
									<li><?php echo $frp_image->location_4_title; ?></li>
									<li><?php echo $frp_image->location_3_title; ?></li>
									<li><?php echo $frp_image->bedroom; ?> Beds | Bathrooms <?php echo $frp_image->bathroom; ?> | Sleeps <?php echo $frp_image->sleeps; ?></li>
								</ul>
								<div class="lnk-btn more-btn"><a href="{{ url('listing/'.$frp_image->rental_id.'.html') }}">View Details</a></div>
							</div>
						</div>
					</div>

					@endforeach



				</div>
			
			

				
	
				<div class="vacation-cont col-sm-8 p0">
                <?php 
                    if(isset($bottom_text[0]->content)){ 
                    echo $bottom_text[0]->content;
                    }
                    ?>                
                
                <!--<div class="vacation-cont col-sm-4 p0 mt20 text-right">
                   	<div class="advertisement">
                
				<div class="googleAds">
                    
					
                <script type="text/javascript">
                    google_ad_client	= "pub-0191483045807371";
                    google_ad_width		= 336;
                    google_ad_height	= 280;
                    google_ad_format	= "336x280_as";
                    google_ad_type		= "text_image";
                    google_ad_channel	= "";
                    google_color_border	= "336699";
                    google_color_bg		= "FFFFFF";
                    google_color_link	= "0000FF";
                    google_color_url	= "008000";
                    google_color_text	= "000000";
                </script>

                <script type="text/javascript" src="https://pagead2.googlesyndication.com/pagead/show_ads.js"></script><ins id="aswift_0_expand" style="display:inline-table;border:none;height:280px;margin:0;padding:0;position:relative;visibility:visible;width:336px;background-color:transparent"><ins id="aswift_0_anchor" style="display:block;border:none;height:280px;margin:0;padding:0;position:relative;visibility:visible;width:336px;background-color:transparent"><iframe width="336" height="280" frameborder="0" marginwidth="0" marginheight="0" vspace="0" hspace="0" allowtransparency="true" scrolling="no" allowfullscreen="true" onload="var i=this.id,s=window.google_iframe_oncopy,H=s&amp;&amp;s.handlers,h=H&amp;&amp;H[i],w=this.contentWindow,d;try{d=w.document}catch(e){}if(h&amp;&amp;d&amp;&amp;(!d.body||!d.body.firstChild)){if(h.call){setTimeout(h,0)}else if(h.match){try{h=s.upd(h,i)}catch(e){}w.location.replace(h)}}" id="aswift_0" name="aswift_0" style="left:0;position:absolute;top:0;"></iframe></ins></ins>                    
				</div>
                
			</div>
                </div>-->
			</div>
			
		</div>
	</section>
	<section class="advert-area-three">
		<div class="container">
			<div class="advert-area list-col-4 col-sm-12 p0">
                
				<ul>
					@foreach($second_four_banner_images as $banner_image)
						<li><a href="<?php echo $banner_image->destination_protocol.$banner_image->destination_url; ?>"><img src="<?php echo config('params.imagesPath').$banner_image->prefix.'photo_'.$banner_image->image_id.".".strtolower($banner_image->type);?>" alt="<?php echo $banner_image->caption; ?>" /></a></li>
					@endforeach	
				</ul>
			</div>
		</div>
	</section>
	
	
	
	
	<!--<section class="advertisement-area mt30 advertisement-bottom text-center">
        <div class="container">
				<div class="googleAdsBottom">
					
					<script type="text/javascript">
                    google_ad_client	= "pub-0191483045807371";
                    google_ad_width		= 728;
                    google_ad_height	= 90;
                    google_ad_format	= "728x90_as";
                    google_ad_type		= "text_image";
                    google_ad_channel	= "";
                    google_color_border	= "336699";
                    google_color_bg		= "FFFFFF";
                    google_color_link	= "0000FF";
                    google_color_url	= "008000";
                    google_color_text	= "000000";
                </script>

					<script type="text/javascript" src="https://pagead2.googlesyndication.com/pagead/show_ads.js"></script><ins id="aswift_0_expand" style="display:none;border:none;height:0px;margin:0;padding:0;position:relative;visibility:visible;width:728px;background-color:transparent"><ins id="aswift_0_anchor" style="display:block;border:none;height:90px;margin:0;padding:0;position:relative;visibility:visible;width:728px;background-color:transparent"><iframe width="728" height="0" frameborder="0" marginwidth="0" marginheight="0" vspace="0" hspace="0" allowtransparency="true" scrolling="no" allowfullscreen="true" onload="var i=this.id,s=window.google_iframe_oncopy,H=s&amp;&amp;s.handlers,h=H&amp;&amp;H[i],w=this.contentWindow,d;try{d=w.document}catch(e){}if(h&amp;&amp;d&amp;&amp;(!d.body||!d.body.firstChild)){if(h.call){setTimeout(h,0)}else if(h.match){try{h=s.upd(h,i)}catch(e){}w.location.replace(h)}}" id="aswift_0" name="aswift_0" style="left:0;position:absolute;top:0;"></iframe></ins></ins>                    
				</div>
		</div>
	</section>-->
<script>
function CheckForm(){
		var form_get = jQuery('#search_form').serializeArray();

		jQuery.ajax({
		  url: "{{ url('advancedsearch-checkform') }}",
		  context: document.body,
		  data: form_get,
		  success: function(redirect_url){
			//console.log(redirect_url);
				window.location.href = redirect_url.return_url;
		  }
		});
		return false;
	}
</script>
@endsection
