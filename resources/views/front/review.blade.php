<?php 
use App\Functions\Functions;
?>
@extends('popup')

@section('content')


    <link rel="stylesheet" href="{{ asset('front/css/structure.min.css') }}" />

    <div class="modal-content ">
    
    
        <h2>
            <b>Reviews of {{ $listingData[0]->title.', '.$listingData[0]->address.', '.$listingData[0]->bedroom.' Bedrooms, '.$listingData[0]->bathroom.' Bathrooms, '.$listingData[0]->sleeps.' Sleeps ' }}</b>
          <span>
<a href="javascript:void(0);" onclick="parent.$.fancybox.close();">Close</a>
            </span>
        </h2>
        
        <div class="popup-review">
        
            <div class="info">
     <?php if(session('message_review')) {
                        if (session('success_review')==true) { ?>
                            <br />
                            <p class="successMessage"> <?php  echo session('message_review'); ?></p>

                        <?php } else { ?>
                            <br />
                            <p class="errorMessage"><?php echo session('message_review'); ?></p>

                        <?php }
                    } ?>

            
                <p class="errorMessage" id="JS_errorMessage" style="display:none">&nbsp;</p>
            
            
            </div>
            
            <form name="rate_form" action="{{ url('submit-review') }}" method="post" class="form">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" id="item_type" name="item_type" value="{{ $item_type }}">
                <input type="hidden" id="item_id" name="item_id" value="{{ $listingData[0]->item_id }}">
                 <input type="hidden" name="rating" id="rating" value="1">
                <input type="hidden" id="member_id" name="member_id" value="{{ $member_id }}">
                <input type="hidden" name="pop_type" value="reviewformpage">
                
                
                <div class="rate">
                    <h3>Rate It</h3>
             <img style="cursor:pointer" src="<?php echo url('images/img_rate_star_off.gif'); ?>" alt="star" onclick="setRatingLevel(1)" onmouseout="resetRatingLevel()" onmouseover="setDisplayRatingLevel(1)" name="star1">
                    <img style="cursor:pointer" src="<?php echo url('images/img_rate_star_off.gif'); ?>" alt="star" onclick="setRatingLevel(2)" onmouseout="resetRatingLevel()" onmouseover="setDisplayRatingLevel(2)" name="star2">
                    <img style="cursor:pointer" src="<?php echo url('images/img_rate_star_off.gif'); ?>" alt="star" onclick="setRatingLevel(3)" onmouseout="resetRatingLevel()" onmouseover="setDisplayRatingLevel(3)" name="star3">
                    <img style="cursor:pointer" src="<?php echo url('images/img_rate_star_off.gif'); ?>" alt="star" onclick="setRatingLevel(4)" onmouseout="resetRatingLevel()" onmouseover="setDisplayRatingLevel(4)" name="star4">
                    <img style="cursor:pointer" src="<?php echo url('images/img_rate_star_off.gif'); ?>" alt="star" onclick="setRatingLevel(5)" onmouseout="resetRatingLevel()" onmouseover="setDisplayRatingLevel(5)" name="star5">
                </div>
                
            
               <div class="left">
                
                    <div>
                        <label>* Name:</label>
 <input class="text" type="text" name="reviewer_name" id="reviewer_name" value="<?php echo (Functions::sess_getAccountIdFromSession() && config('params.SOCIALNETWORK_FEATURE') == "on" && $reviewerAcc->getString("has_profile") == "y") ? ($reviewerProfile->getString("nickname")) : ($reviewer_name);?>" maxlength="50" tabindex="1" <?php echo (Functions::sess_getAccountIdFromSession() && config('params.SOCIALNETWORK_FEATURE') == "on" && $reviewerAcc->getString("has_profile") == "y") ? "readonly=\"readonly\"" : "" ?>>
                    </div>
                    <div>
                        <label>* Comment Title:</label>
     <input class="text" type="text" name="review_title" id="review_title" value="" maxlength="50" tabindex="2">
                    </div>
                    <div>
                        <label>* E-mail:</label>
 <input class="text" type="text" name="reviewer_email" id="reviewer_email" value="<?php echo (Functions::sess_getAccountIdFromSession() && config('params.SOCIALNETWORK_FEATURE') == "on" && $reviewerAcc->getString("has_profile") == "y") ? ($reviewerInfo->getString("email")) : ($reviewer_email);?>" maxlength="100" tabindex="3">
                    </div>
                    <div>
                        <label>* City, State:</label>
                        <?php
            if (Functions::sess_getAccountIdFromSession()) {
                unset($rLoc);
                if ($reviewerInfo->getString("city")) $rLoc[] = $reviewerInfo->getString("city");
                if ($reviewerInfo->getString("state")) $rLoc[] = $reviewerInfo->getString("state");
                
                if (is_array($rLoc) && $rLoc[0]) $reviewer_location = implode(", ", $rLoc);
            }
            ?>
                        <input class="text" type="text" name="reviewer_location" id="reviewer_location" value="{{ $reviewer_location }}" maxlength="50" tabindex="4">
                    </div>
                
                </div>
                
               <div class="right">
                
                        <label>* Comment:</label>
                        <textarea class="textarea" name="review" id="review" rows="9" tabindex="5"></textarea>
                
                </div>
                
          <div class="clearfix"></div>
           <div class="review-action">
                
                    <p>Please enter the text you see in the image at the left into the textbox. This is required to prevent automated submission of contact requests.</p>
                    <div class="captcha">
                        <div>

<?php 

print '<img src="data:image/png;base64,'.base64_encode($imagedata).'" border="0" alt="Verification Code image cannot be displayed" title="Verification Code"/>';

?>

       <input type="text" value="" name="captchatext" class="text">
                        </div>

 <input type="hidden" id="captchakey" name="captchakey" value="<?php echo session('SESS_captch_key');?>">
   
                    </div>
                    <button type="submit" name="submit" value="Submit" id="submitReview">Send</button>

                </div>
            </form>
        </div>
    
</div>

	
    
				

<script type="text/javascript" language="javascript">
		
                function setDisplayRatingLevel(level) {
                    for(i = 1; i <= 5; i++) {
                        var starImg = "img_rate_star_off.gif";
                        if( i <= level ) {
                            starImg = "img_rate_star_on.gif";
                        }
                        var imgName = 'star'+i;
                        document.images[imgName].src="<?php echo url('images'); ?>/"+starImg;
                    }
                }
                
                function resetRatingLevel() {
                    setDisplayRatingLevel(document.rate_form.rating.value);
                }
                
                function setRatingLevel(level) {
                    document.rate_form.rating.value = level;
                }
                
                function disabledReviewButton(disable) {
                    if (disable) {
                        $("#submitReview").css("cursor", "default");
                        $("#submitReview").attr("disabled", "disabled");
                        document.getElementById('submitReview').innerHTML = "Wait, Loading...";
                    } else {
                        document.getElementById('submitReview').innerHTML = "Send";
                        $("#submitReview").attr("disabled", "");
                        $("#submitReview").css("cursor", "pointer");
                    }
                }
                
                $('img[name=star]').bind('click', function(){
                    $(this).fadeOut(50);
                    $(this).fadeIn(50);
                });

                $('document').ready(function() {

                    $('form').submit(function() {

                                                var reviewMandatory = "on";
                        var valid_email = new RegExp('^.+@.+\\..+$');
                        var top = 50;
                        var position = 400;

                        $('#JS_errorMessage').empty();
                        $('.errorMessage').css('display', 'none');

                        if ($('#rating').val() == '') {
                            $('#JS_errorMessage').append('Please select a rating for this item<br />\n');
                            position +=15;
                            top -=1;    
                        }
                        if (reviewMandatory == "on") {
                            if ($('#reviewer_name').val() == '' || $('#reviewer_email').val() == '') {
                                $('#JS_errorMessage').append('"Name" and "E-mail" are required to post a comment!<br />\n');
                                position +=15;
                                top -=1;
                            } else if ($('#reviewer_email').val().search(valid_email) == -1) {
                                $('#JS_errorMessage').append('Please type a valid e-mail address!<br />\n');
                            }
                        }
                        if ($('#reviewer_location').val() == '') {
                            $('#JS_errorMessage').append('"City, State" are required to post a comment!<br />\n');
                            position +=15;
                            top -=1;
                        }
                        if ($('#review_title').val() == '' || $('#review').val() == '') {
                            $('#JS_errorMessage').append('"Comment" and "Comment Title" are required to post a comment!<br />\n');
                            position +=15;
                            top -=1;
                        }

                        if ($('#JS_errorMessage').text() == "") {
                            $('#JS_errorMessage').css('display', 'none');    
                        } else {
                            $('#JS_errorMessage').css('display', '');
                            $('#TB_ajaxContent').css('height', position);
                            $('#TB_window').css('top', top+'%');
                            disabledReviewButton(false);
                            return false;
                        }
                        disabledReviewButton(true);
                        return true;

                    });    

                });

            </script>
        
        
@endsection