@extends('front')

@section('content')

<style>

.content-custom h1, .content-custom h2, .content-custom h3{
    line-height: 1;
    text-indent: 0;
	margin:0;
}
.content-custom{
	margin-top:0;
}
.content-custom p{
	font-size:18px;
	line-height: 1.5;
    font-weight: 300;
}
.content-custom a, .content-custom a:visited {
    text-decoration: underline;
}
.content-custom h1, .content-custom h2, .content-custom h3 {
    font-weight: 400;
}


</style>
	
	<div class="photo-services container">

        
		<div class="content content-full margin-top">

			<div class="content-main">
		
				<div class="content-custom"><h1><strong><br>PHOTOGRAPHY SERVICES<br><br></strong><span style="color: #ff6600;"><strong><span style="font-size: x-large;">How do your pictures look? &nbsp;Are they eye-catching<span>&nbsp;or dark, blurry and pixelated?&nbsp;</span></span></strong></span></h1>
<p style="text-align: justify;"><span style="color: #000000;"><br>Eye catching pictures are&nbsp;the biggest difference between an&nbsp;average rental season and a&nbsp;remarkable spectacular rental season&nbsp;including quality tenants. The right&nbsp;camera equipment can make all the&nbsp;difference. Contact us for a quality&nbsp;photo showcasing all of the features of&nbsp;your property that make it special.&nbsp;</span></p>
<p style="text-align: justify; margin-top:10px;"><span style="color: #000000;">Pictures are the first and only impression the renters see. 99% of renters do not drive to the home until check in. Make sure to have several exterior photos, nearby attractions including the beach, bay, amusements, fishing piers, etc. If it does not look appealing, renters will quickly move on to another home. Don't let this happen to you, take great pictures or use a professional service if possible.&nbsp;</span></p>
<p style="text-align: justify; margin-top:10px;"><span style="color: #000000;">Do not have the time, patience or proper equipment to take eye catching&nbsp;photographs&nbsp;of your rental home?&nbsp; No problem, we will do it all for you for only $125 per property (discounts for multiple properties).&nbsp; We know what shots to take to showcase the HIGHLIGHTS of your home.</span>&nbsp;&nbsp;<span style="color: #0000ff;"><a href="https://vimeo.com/145011525"><span style="color: #0000ff;">View our Photo Gallery Portfolio.</span></a></span></p>
<p style="margin-top:10px;"><span style="color: #000000;">We will drive to your home, take photos of ALL the rooms in your home, download them on the web-site and then create a CD for you with all the photos for you to keep. &nbsp;Just&nbsp;<span style="color: #0000ff;"><a href="http://www.shoresummerrentals.com/contactus.php"><span style="color: #0000ff;">contact us</span></a></span>&nbsp;and we can arrange all the details asap!<br>&nbsp;</span></p>
<p style="text-align: justify;"><span style="color: #0000ff;"><a href="https://vimeo.com/145011525"><img src="http://shoresummerrentals.com/custom/domain_1/image_files/Marketing/MARIA_MISCELLANEOUS/sitemgr_Screenshot_2016-01-25_20.16.39.png" alt="" width="500" height="328"></a>&nbsp;&nbsp;</span><a style="font-size: 2em;" href="https://www.youtube.com/watch?v=ig1S373RXEo"><strong><img src="http://shoresummerrentals.com/custom/domain_1/image_files/Marketing/sitemgr_Screenshot_2016-06-27_13.05.00.png" alt="" width="575" height="321"></strong></a></p>
<p style="text-align: justify;"><span style="color: #0000ff;">&nbsp; &nbsp;&nbsp;</span>&nbsp;</p>
<p style="text-align: justify;"><span style="color: #000000;"><strong>TAKING YOUR OWN PHOTOS&nbsp;<br></strong>Please note that you can take your own photos and upload them to your ad on your own. If you are taking photos on your own and do not have a digital camera, you should order a disc along with your paper photos as the photos look much better when uploaded directly from your computer. &nbsp;Please note that if you have paper photos that you need SSR to scan, there will be a nominal fee and the photos are not as clear as they could be.</span></p>
<p style="text-align: justify;"><span style="color: #000000;"><br></span></p></div>			
			</div>
			
					
		</div>

	</div>

					
@endsection