@extends('front')

@section('content')

<style type="text/css">
.sponsors-page .fancybox-inner {
    height: 440px !important;
    
}
.sponsors-page .fancybox-wrap {
    top: 30px !important;
    
}
</style>
	<link rel="stylesheet" href="{{ asset('front/css/structure.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('front/css/members.min.css') }}" />
    <script src="{{ asset('front/js/Chart.js') }}"></script>
    <!--<script src="{{ asset('front/scripts/common.js') }}"></script>-->
     <!--link rel="stylesheet" href="{{ asset('front/css/bootstrap-responsive.min.css') }}" /-->
  
    
    
	<div class="sponsorPage-container container members">

        <div class="sponsors-dashboard container-fluid">


 @if (session('message'))
<div id="alert" class="alert alert-success" style="">
       <?php  echo session('message'); ?>
 </div>
@endif

        



    <div class="row-fluid responsive">
        
            <?php if(isset($spListingData) && !empty($spListingData)){ ?>    
            <div class="col-sm-4 responsive-menu">
                         
    <div class="responsive-scrollmenu">
        
        <div class="scroll-content">

        <?php 
         $listing_id=0; 
        foreach($spListingData as $key=>$val){
           if($key==0)
           {
             $listing_id= $val->id;
           }
            ?>
            <div id="Listing_<?php echo $val->id; ?>" class="webitem <?php echo ($key==0)?'active':''; ?>">
            
                <div class="desc" onclick="loadDashboard('Listing', <?php echo $val->id; ?>)">
                
                    <p class="title">
                    <b><?php echo $val->address; ?></b>
                    <b style="margin-left: 10%;">Rental ID: <?php echo $val->id; ?></b>
                    
                    <span class="status" data-original-title="" title=""> 
                    <?php 
                            if($val->STATUS=='E')
                            {
                                echo '<span class="status-expired">Expired</span>';
                            }
                            elseif($val->STATUS=='P')
                            {
                                echo '<span class="status-pending">Pending</span>';
                            }
                            elseif($val->STATUS=='A')
                            {
                                echo '<span class="status-active">Active</span>';
                            }
                            
                        ?>
                     
                    
                    
                    </span>
                    </p>
                    
                    <p class="simple">
                        Listing (
                        <?php 
                            if($val->LEVEL==10)
                            {
                                echo 'Diamond';
                            }
                            elseif($val->LEVEL==30)
                            {
                                echo 'Gold';
                            }
                            elseif($val->LEVEL==50)
                            {
                                echo 'Silver';
                            }
                            elseif($val->LEVEL==70)
                            {
                                echo 'Bronze';
                            }
                        ?>
                        )                        
                    </p>
                
                </div>
                
                <div class="action">
                
                    <a href="<?php echo url('sponsors/listing/'.$val->id); ?>">Edit</a>
					<a class="iframe fancy_window_preview" href="<?php echo url('sponsors/preview/'.$val->id);  ?>">Preview</a>
                    <a href="<?php echo url('sponsors/rate/'.$val->id);  ?>">Rate</a> 
                    <a href="<?php echo url('sponsors/badges/'.$val->id);  ?>">Specials</a> 
                    <a href="<?php echo url('sponsors/availability/'.$val->id);  ?>">Calendar</a> 
                
                
                
                </div>
            
            </div>
       <?php } ?>        
           
            
         
            
             
        </div>
    </div>
    
    
        <div class="addcontent">
            
            <p>Add a new content</p>
            
            <ul>
                <li>
                    Add <a href="<?php echo url('sponsors/listinglevel'); ?>">Listing</a>
                </li>
                
                                
                                
                                
                            </ul>
            
        </div>

                </div>

<?php } ?>
            <div id="dashboard" class="col-sm-8 responsive-dashboard">            
            
    

    
            </div>
        
             
        
    </div>


            </div><!-- Close container-fluid div -->
            
        </div>

		 <script type="text/javascript">

           if ($("#myChart").length) {
            //This will get the first returned node in the jQuery collection.
            var ctx = $("#myChart").get(0).getContext("2d");
        }

        function initializeDashboard() {
            $(".dial").knob({
                readOnly:   true,
                fgColor:    "#2980b9",
                bgColor:    "#DEE1E3",
                fontWeight: 300,
                thickness:  .2,
                width:      70,
                height:     70
            });

            $(".status, .floating-tip, .alert-new, #item_renewal").tooltip({
                animation: true,
                placement: "top"
            });
             
            if ($("#myChart").length) {
                //Get context with jQuery - using jQuery's .get() method.
                ctx = $("#myChart").get(0).getContext("2d");
                loadChart();
            }
        }

        $(function() {
            $("#alert").fadeOut(5000);
            loadDashboard('Listing',{{ $listing_id }});
        });
        
         
        function showReply(id) {
            $('#review_reply'+id).css('display', '');
            $('#link_reply'+id).css('display', 'none');
            $('#cancel_reply'+id).css('display', '');
        }
        
        function hideReply(id) {
            $('#review_reply'+id).css('display', 'none');
            $('#link_reply'+id).css('display', '');
            $('#cancel_reply'+id).css('display', 'none');
        }
        
        function showLead(id) {
            $('#lead_reply'+id).css('display', '');
            $('#link_lead'+id).css('display', 'none');
            $('#cancel_lead'+id).css('display', '');
        }
        
        function hideLead(id) {
            $('#lead_reply'+id).css('display', 'none');
            $('#link_lead'+id).css('display', '');
            $('#cancel_lead'+id).css('display', 'none');
        }
        
        function saveReply(id) {
            $("#submitReply"+id).css("cursor", "default");
            $("#submitReply"+id).prop("disabled", "disabled");
            $("#submitReply"+id).html('Wait, Loading...');
            
        $.post("<?php echo url('sponsors/review_reply'); ?>", $("#formReply"+id).serialize(), function(data) {
                if (data == "ok") {
                    $("#msgReviewE"+id).css("display", "none");
                    $("#msgReviewS"+id).css("display", "");
                    $("#msgReviewS"+id).fadeOut(5000);
                } else {
                    $("#msgReviewE"+id).css("display", "");
                    $("#msgReviewS"+id).css("display", "none");
                }
                $("#submitReply"+id).html('Submit');
                $("#submitReply"+id).prop("disabled", "");
                $("#submitReply"+id).css("cursor", "pointer");
            });
        }
        
        function saveLead(id) {
            $("#submitLead"+id).css("cursor", "default");
            $("#submitLead"+id).prop("disabled", "disabled");
            $("#submitLead"+id).html('Wait, Loading...');
            
            $.post("<?php echo url('sponsors/lead_reply'); ?> ", $("#formLead"+id).serialize(), function(data) {
                if (data == "ok") {
                    $("#msgLeadE"+id).css("display", "none");
                    $("#msgLeadS"+id).css("display", "");
                    $("#msgLeadS"+id).fadeOut(5000);
                    setTimeout("leadBox('hide', "+id+");", 4000);
                    $("#title_replied"+id).css("display", "none");
                    $("#new_replied"+id).css("display", "");
                } else {
                    $("#msgLeadE"+id).html(data);
                    $("#msgLeadE"+id).css("display", "");
                    $("#msgLeadS"+id).css("display", "none");
                }
                $("#submitLead"+id).html('Submit');
                $("#submitLead"+id).prop("disabled", "");
                $("#submitLead"+id).css("cursor", "pointer");
            });
        }
        
        function reviewBox(option, id) {
            $("#reviews-list").children(".item-review").children(".review-detail").stop(true,true).slideUp();
            $("#reviews-list").children(".item-review").children(".review-summary").stop(true,true).slideDown().removeClass("new");
            if (option == "show") {
                $("#review-summary-"+id).slideUp(); 
                $("#review-detail-"+id).slideDown();
                setItemAsViewed("review", id);
            } else {
                $("#review-summary-"+id).slideDown(); 
                $("#review-detail-"+id).slideUp();
            }            
        }
        
        function leadBox(option, id) {
            $("#leads-list").children(".item-review").children(".review-detail").stop(true,true).slideUp();
            $("#leads-list").children(".item-review").children(".review-summary").stop(true,true).slideDown().removeClass("new");
            if (option == "show") {
                $("#lead-summary-"+id).slideUp(); 
                $("#lead-detail-"+id).slideDown();
                setItemAsViewed("lead", id);
            } else {
                $("#lead-summary-"+id).slideDown(); 
                $("#lead-detail-"+id).slideUp();
            }            
        }
        
        function dealBox(option, id) {
            $("#deals-list").children(".item-review").children(".review-detail").stop(true,true).slideUp();
            $("#deals-list").children(".item-review").children(".review-summary").stop(true,true).slideDown();
            if (option == "show") {
                $("#deal-summary-"+id).slideUp(); 
                $("#deal-detail-"+id).slideDown();
            } else {
                $("#deal-summary-"+id).slideDown(); 
                $("#deal-detail-"+id).slideUp();
            }            
        }
        
        function changeDealStatus(option, id, promocode) {
            $.post("https://www.shoresummerrentals.com/sponsors/deal/deal.php",{action: option, promotion_id: promocode}, function() {
                if (option == "freeUpDeal") {
                    $("#label_used"+id).css("display", "");
                } else {
                    $("#label_used"+id).css("display", "none");
                }
            });
        }
        
        function setItemAsViewed(type, id) {
            $.post("https://www.shoresummerrentals.com/sponsors/ajax.php", {
                ajax_type: 'setItemAsViewed',
                type: type,
                id: id
            }, function () {});
        }
       

           function loadDashboard(item_type, item_id) {
            $.post("<?php echo url('loadDashboard');?>", {
                ajax_type: 'load_dashboard',
                item_type: item_type,
                item_id: item_id,
                _token:'{{ csrf_token() }}'
            }, function (ret) {
                $(".webitem").removeClass("active");
                $("#"+item_type+"_"+item_id).addClass("active");
                scrollPage('#float_layer');
                $("#dashboard").hide().html(ret).fadeIn(800);
                    
                var ctx = $("#myChart").get(0).getContext("2d");
                loadChart();
                initializeDashboard();
            });
        }
       
     

         function selectLegend(option, id, chartdata) {
            var countVisible = 0;
            
            if (option == "viewALL") {
                
                if ( $(".legend-ALL").hasClass("isvisible")) {
//                    $(".legend-ALL").removeClass("isvisible");
//                    $("#optionLegend > li > i").removeClass("checked");
//                    $("#optionLegend > li").removeClass("isvisible");
//                    $("#controlLegend > li").remove();
                } else {
                    countVisible = 2;
                    $("#optionLegend > li > i").addClass("checked");                   
                    $(".legend-ALL").addClass("isvisible");
                    $("#optionLegend > li").not(".isvisible").clone().appendTo("#controlLegend");
                    $("#optionLegend > li").addClass("isvisible");
                }
            } else {
                id: id
                chartdata: chartdata
                $newlegend = $(".legend-"+id).clone();

                if ($(".legend-"+id).hasClass("isvisible")) {
                    
                    //Check if there's at least one other legend selected to prevent empty chart
                    $('#optionLegend li').each(function() {
                        if ($(this).hasClass("isvisible")) {
                            countVisible++;
                        }
                    });

                    if (countVisible > 1) {
                        $(".legend-"+id).children("i").removeClass("checked");
                        $(".legend-"+id).removeClass("isvisible");
                        $("#controlLegend").children(".legend-"+id).remove();
                        $(".legend-ALL").children("i").removeClass("checked");
                        $(".legend-ALL").removeClass("isvisible");
                    }
                } else {
                    countVisible = 2;
                    $(".legend-"+id).children("i").addClass("checked");
                    $(".legend-"+id).addClass("isvisible");
                    $newlegend.appendTo("#controlLegend");
                }
            }
            if (countVisible > 1) {
                controlChart();
            }
        }

                
        function loadChart() {
            var data = {
                labels : chartLabels,
                datasets : initialReport
            };
            var steps = 5;
            var max = maxInitialReport;
            if (max < steps) {
                steps = max;
            }
            var options = {
                bezierCurve : false,
                scaleOverride: true,
                scaleSteps: steps,
                scaleStepWidth: Math.ceil(max / steps),
                scaleStartValue: 0
            };
            ctx = $("#myChart").get(0).getContext("2d");
            new Chart(ctx).Line(data, options);        
        }
       
        function controlChart() {
            
            var datasets = new Array();
            var max = 0;
            var thisHighest = 0;
            $('#optionLegend li').each(function() {
                if ($(this).hasClass("isvisible")) {
                    var reportType = $(this).attr('report');
                    if (reportType) {
                        datasets.push(window[reportType]);
                        thisHighest = Math.max.apply(Math, window[reportType].data);
                        if (thisHighest > max) {
                            max = thisHighest;
                        }
                    }
                }
            });
            
            var steps = 5;
            if (max < steps) {
                steps = max;
            }
            var options = {
                bezierCurve : false,
                scaleOverride: true,
                scaleSteps: steps,
                scaleStepWidth: Math.ceil(max / steps),
                scaleStartValue: 0
            };

            var data = {
                labels : chartLabels,
                datasets : datasets
            };
            ctx = $("#myChart").get(0).getContext("2d");
            new Chart(ctx).Line(data, options);  

        }


function showmore(div_id, link_id, total, block) {
    total = total - 1;
    var lastItemToShow = parseInt($("#"+div_id).val());
    for (i = 0; i < block; i++) {
        lastItemToShow = lastItemToShow + 1;
        $("#"+div_id+lastItemToShow).fadeIn(50);
    }
    if (lastItemToShow >= total) {
        $("#"+link_id).css("display", "none");
    }
    $("#"+div_id).attr("value", lastItemToShow);
}


function scrollPage(position_id){
    
	if(!position_id){
		$position_id = '#resultsMap';
	}else {
		$position_id = position_id;
      
	}
	//jQuery('html,body').animate({scrollTop: jQuery($position_id).offset().top},'slow');
    jQuery('html, body').animate({ scrollTop: jQuery('#float_layer').offset().top }, 1500);
}
    </script>



			
@endsection
