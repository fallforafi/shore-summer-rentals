@extends('popup')

@section('content')
<!--<link href="http://www.shoresummerrentals.com/custom/domain_1/theme/default/detail.min.css" rel="stylesheet" type="text/css" media="all" />-->
 <link rel="stylesheet" href="{{ asset('front/scripts/jquery/jquery_ui.1.9.2/css/ui-lightness/jquery-ui-1.9.2.custom.min.css') }}" />
  <script src="{{ asset('front/scripts/jquery/jquery_ui.1.9.2/js/jquery-ui-1.9.2.custom.min.js') }}"></script>
<div class="modal-content ">
 @if (session('return_email_message'))
       <?php  echo session('return_email_message'); ?>
@endif  

        
            <h2>
    <b>{{ $listingData[0]['title'] }}</b>
                <span>
                    <a href="javascript:void(0);" onclick="parent.$.fancybox.close();">Close</a>
                </span>
            </h2>

            <div class="send-email send-owner-email">

                <div class="info">

                    
       <h4 class="text-center">Contact {{ $listingData[0]['title'] }}</h4><br>

                    
                </div>
                <script type="text/javascript">
                    $(function() {
                        $('#emailForm').submit(function(e) {
                            $('#emailFormSubmitBtn').attr('disabled', 'disabled');
                            return true;
                        })
                    });
                </script>
                <form name="mail" action="<?php echo url('listing_emailformstore'); ?>" method="post" class="form" id="emailForm">
             <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <input type="hidden" name="id" value="{{ $listingData[0]['rental_id'] }}">
                    <input type="hidden" name="receiver" value="owner">
                    <input type="hidden" name="pop_type" value="listing_emailform">

                    
                       <h4>Your information:</h4>
                         <div class="row-fluid">
                        <div class="span6">
                        <label for="first_name">* First Name</label>
                        <input class="text" type="text" name="first_name" id="first_name" tabindex="1" value="">
                                </div>                            
                                <div>
                                    <label for="last_name">* Last Name</label>
                        <input class="text" type="text" name="last_name" id="last_name" tabindex="2" value="">
                                </div>
                                                    </div>

                        <div class="span6">
                            
                                                            <div>
                                    <label for="from">* Your e-mail</label>
                 <input type="email" tabindex="3" name="from" value="" class="text">
                                </div>
                                <div>
                                    <label for="phone_number">* Phone</label>
                  <input type="text" tabindex="4" id="phone_number" name="phone_number" value="" class="text">
                                </div>
                            
                                                                                        
                        </div>
                  

                                                <br>
                        <div class="">
                            <h4>Tell us about your trip:</h4>
                            <div class="row-fluid">
                                <div class="span3">
                                    <label>Arrive</label>
                                    <input type="text" name="arrive" id="arrive" placeholder="Arrive" value="" class="hasDatepicker">
                                </div>
                                <div class="span3">
                                    <label>Depart</label>
                                    <input type="text" name="depart" id="depart" placeholder="Depart" value="" class="hasDatepicker">
                                </div>
                                <div class="span3">
                                    <label># of Adults</label>
                                    <select name="adults">
                                                                                    <option value="0" selected="selected">0</option>
                                                                                    <option value="1">1</option>
                                                                                    <option value="2">2</option>
                                                                                    <option value="3">3</option>
                                                                                    <option value="4">4</option>
                                                                                    <option value="5">5</option>
                                                                                    <option value="6">6</option>
                                                                                    <option value="7">7</option>
                                                                                    <option value="8">8</option>
                                                                                    <option value="9">9</option>
                                                                                    <option value="10">10</option>
                                                                            </select>
                                </div>
                                <div class="span3">
                                    <label># of Children</label>
                                    <select name="children">
                                                                                    <option value="0" selected="selected">0</option>
                                                                                    <option value="1">1</option>
                                                                                    <option value="2">2</option>
                                                                                    <option value="3">3</option>
                                                                                    <option value="4">4</option>
                                                                                    <option value="5">5</option>
                                                                                    <option value="6">6</option>
                                                                                    <option value="7">7</option>
                                                                                    <option value="8">8</option>
                                                                                    <option value="9">9</option>
                                                                                    <option value="10">10</option>
                                                                            </select>
                                </div>
                            </div>
                         <div class="checkbox" style="margin: 10px 0 10px 0;">
                                <label>
                                    <input type="checkbox" style="width: 14px;  top: 8px; position: relative;" class="checkbox" name="flexible_date" value="y" checked="">
                                    My travel dates are flexible                                </label>
                            </div>
                            <div>
                                <label for="body">* Message to owner</label>
                            <textarea name="body" rows="3" cols="0" class="textarea"></textarea>
                            </div>
                        </div>
                        
                 <p>By clicking "Submit" you are agreeing to our <a href="<?php echo url('termsof-use.html'); ?>" target="_blank">Terms of Use</a> and <a href="<?php echo url('legal-disclaimer.html'); ?>" target="_blank">Legal Disclaimer</a></p>
                        <div class="action">
                <button type="submit" value="Send" onclick="pageTracker._trackEvent('button','click','email owner')">Only click SUBMIT one time</button>
                        </div>
                                    </form>
            </div>

            </div>
   </div>

 @endsection           