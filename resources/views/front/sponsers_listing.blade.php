 @extends('front')

@section('content')
<style type="text/css">
    div.googleBase {
    background: #E5E3DF;
    border: 1px solid #BAB5A9!important;
    height: 400px;
    margin: auto;
    width: 491px;
    z-index: 1;
}
</style>


    <link rel="stylesheet" type='text/css' href="{{ asset('front/css/members.min.css') }}" />
    <link rel="stylesheet" type='text/css' href="{{ asset('front/style.css') }}" />
    <link rel="stylesheet" type='text/css' href="{{ asset('front/css/bootstrap.min.css') }}" />
    <link rel="stylesheet" type='text/css' href="{{ asset('plugins/croppic/assets/css/croppic.css') }}"/>
    
  <link rel="stylesheet" type='text/css' href="{{ asset('front/css/structure.min.css') }}" />


    <script src="{{ asset('plugins/croppic/croppic.min.js') }}"></script>

     




                 <div class="listingcontainer-main container members">

                 

        <div class="container-fluid">
    <div class="listingForm">
		
		<a class="bcktoDashboard" href="<?php echo url('sponsors'); ?>"><i class="fa fa-chevron-left"></i> Back to Dashboard</a>
<h2>Listing Information</h2>
        @if(isset($spListingDataId[0]->id))
        

        <nav class="minor-nav">
            <ul>
                <li>
                    <a class="active" href="#">Listing Information</a>
                </li>

                                <li>
               <a href="<?php echo url('sponsors/backlinks/'.$spListingDataId[0]->id.''); ?>">Backlink</a>
                    </li>
                
                
                <li>
                    <a href="<?php echo url('sponsors/rate/'.$spListingDataId[0]->id.''); ?>">Rate</a>
                </li>
                
                <li>
                    <a href="<?php echo url('sponsors/specials/'.$spListingDataId[0]->id.''); ?>">Specials</a>
                </li>
            </ul>
        </nav>

        @endif


<form name="listing" id="listing" action="<?php echo url('sponsors/listing/listingStore'); ?>" method="post" enctype="multipart/form-data">
             <input type="hidden" name="_token" value="{{ csrf_token() }}">
            
            <input type="hidden" name="ieBugFix" value="1">

            
            <input type="hidden" name="process" id="process" value="">
            <input type="hidden" name="id" id="id" value="<?php echo isset($spListingDataId[0]->id)?$spListingDataId[0]->id:0; ?>">
           
            <input type="hidden" name="listingtemplate_id" id="listingtemplate_id" value="0">
            <input type="hidden" name="account_id" id="account_id" value="<?php echo isset($accountId)?$accountId:0; ?>">
            <input type="hidden" name="level" id="level" value="<?php if(isset($spListingDataId[0]->level)){ echo $spListingDataId[0]->level; }elseif(isset($level)){ echo $level; }else{ echo 0; }; ?>">
            <input type="hidden" name="using_package" id="using_package" value="<?php echo (isset($spListingDataId) && $spListingDataId[0]->package_id>0)?'y':'n'; ?>">
            <input type="hidden" name="package_id" id="package_id" value="<?php echo isset($spListingDataId[0]->package_id)?$spListingDataId[0]->package_id:0; ?>">
            <input type="hidden" name="gallery_hash" value="<?php echo (isset($spListingGallery[0]->sess_id) && $spListingGallery[0]->sess_id!=0)?$spListingGallery[0]->sess_id:'';  ?> <?php echo (old('gallery_hash'))?old('gallery_hash'):$gallery_hash;  ?>">
          

            
<script type="text/javascript">
        jQueryForUI = false;

    
    function isInt(elm) {
        if (elm.value == "") {
            return false;
        }
        for (var i = 0; i < elm.value.length; i++) {
            if (elm.value.charAt(i) < "0" || elm.value.charAt(i) > "9") {
                return false;
            }
        }
        return true;
    }

    // ---------------------------------- //




    function JS_addCategory(id) {

        seed = document.listing.seed;
        feed = document.listing.feed;
        cat_add = 1;
        var text = $("#liContent"+id).html();
                    check_cat = 0;
        
        if (feed.length == cat_add && check_cat == 1){
            fancy_alert('Your item has been paid, so you can add a maximum of 1 category free.', 'errorMessage', false, 500, 100, false);
        } else {
            var flag=true;
            for (i=0;i<feed.length;i++)
                if (feed.options[i].value==id)
                    flag=false;

            if(text && id && flag){
                feed.options[feed.length] = new Option(text, id);
                $('#categoryAdd'+id).after("<span class=\"categorySuccessMessage\">Successfully added!</span>").css('display', 'none');
                $('.categorySuccessMessage').fadeOut(5000);
            } else {
                if (!flag) $('#categoryAdd'+id).after("</a> <span class=\"categoryErrorMessage\">Already added</span> </li>");
                else ('#categoryAdd'+id).after("</a> <span class=\"categoryErrorMessage\">Please, select a valid category</span> </li>");
            }
        }

        $('#removeCategoriesButton').show();

    }

    // ---------------------------------- //

    function JS_submit() {

        feed = document.listing.feed;
        return_categories = document.listing.return_categories;
        if(return_categories.value.length > 0) return_categories.value="";

        for (i=0;i<feed.length;i++) {
            if (!isNaN(feed.options[i].value)) {
                if(return_categories.value.length > 0)
                return_categories.value = return_categories.value + "," + feed.options[i].value;
                else
            return_categories.value = return_categories.value + feed.options[i].value;
            }
        }

        document.listing.submit();
    }

    // ---------------------------------- //


    <?php
             $level_id='';
             if(isset($spListingDataId[0]->level))
                { 
                    $level_id.=$spListingDataId[0]->level; 
                }
            elseif(isset($level)){ 
                $level_id.=$level; 
            }
            else{ 
                $level_id.=0; 
            }
             ?> 
    function makeMain(image_id,thumb_id,item_id,temp,item_type) {

        $.get("<?php echo url('sponsors/makemainimage'); ?>", {
            _token:'<?php echo csrf_token(); ?>',
            image_id: image_id,
            thumb_id: thumb_id,
            item_id: item_id,
            temp: temp,
            item_type: item_type,
            gallery_hash: '<?php echo isset($spListingGallery[0]->sess_id)?$spListingGallery[0]->sess_id:'';  ?><?php echo (old('gallery_hash'))?old('gallery_hash'):$gallery_hash;  ?>',
            domain_id: 1        }, function (res) {
                
                            loadGallery(item_id, "y", "sponsors", "", "true");
                    });
    }

    // ---------------------------------- //

    function changeMain(image_id,thumb_id,item_id,temp,gallery_id,item_type) {

        $.get("<?php echo url('sponsors/changemainimage'); ?>", {
            _token:'<?php echo csrf_token(); ?>',
            image_id: image_id,
            thumb_id: thumb_id,
            item_id: item_id,
            gallery_id: gallery_id,
            temp: temp,
            item_type: item_type,
            level: $level_id,
            gallery_hash: '<?php echo isset($spListingGallery[0]->sess_id)?$spListingGallery[0]->sess_id:'';  ?><?php echo (old('gallery_hash'))?old('gallery_hash'):$gallery_hash;  ?>',
            domain_id: 1        }, function (response) {
            if (response == "error"){
                alert('Your item already has the maximum number of images in the gallery. Delete an existing image to save this one.', 'errorMessage', false, 500, 100, false);
            }
                            loadGallery(item_id, "y", "sponsors", "", "true");
                    });
        }

    // ---------------------------------- //




    function loadGallery(id, new_image, sess, del, main) {

        $("#galleryF").fadeIn(0);
        $.post("<?php echo url('sponsors/returngallery'); ?>", {
            sess: sess,
            module: 'listing',
            _token: '<?php echo csrf_token();  ?>',
            id: id,
            new_image: new_image,
            gallery_id: '<?php echo isset($spListingGallery[0]->gallery_id)?$spListingGallery[0]->gallery_id:0;  ?>',
            main: main,
            gallery_hash: '<?php echo isset($spListingGallery[0]->sess_id)?$spListingGallery[0]->sess_id:'';  ?><?php echo (old('gallery_hash'))?old('gallery_hash'):$gallery_hash;  ?>',
            domain_id: 1,
            level: <?php echo $level_id;  ?>        }, function (ret) {
               
            $("#galleryF").html(ret);

            if (del != "edit" && del != "editFe"){
                if (del == "n"){
                    $("#addImage").css("display", "none");
                    $("#galleryF").css("display", "");
                } else {
                                            if (del) {
                            $("#addImage").css("display", "none");
                            $("#galleryF").css("display", "");
                        } else {
                            $("#addImage").css("display", "");
                            $("#galleryF").css("display", "none");
                        }
                                    }
            } else {
                if (del == "edit" || del == "editFe")
                    $("#galleryF").css("display", "");
            }

            if (main == "true"){
                $("#galleryF").css("display", "");
            }
                            $("#galleryF").css("display", "");
            
            if (ret == "no image"){
                $("#galleryF").css("display", "none");
            }

                $("a.fancy_window_imgAdd").fancybox({

                
                'type'                  : 'iframe',
                'width'                 : 785,
                'minHeight'             : 465,
                'closeBtn'              : false,
                'padding'               : 0,
                'margin'                : 0
             });

            $("a.fancy_window_imgCaptions").fancybox({

                
                'type'                  : 'iframe',
                'width'                 : 600,
                'minHeight'             : 235,
                                'closeBtn'              : false,
                                'padding'               : 0,
                'margin'                : 0

                
            });

            $("a.fancy_window_imgDelete").fancybox({

                                'type'                  : 'iframe',
                'width'                 : 300,
                'minHeight'             : 180,
                                'closeBtn'              : false,
                                'padding'               : 0,
                'margin'                : 0
                            });

         
            $("#galleryF ul").sortable({
                tolerance: "pointer",
                start: function(event, ui) {
                    var oldIndex = ui.item.index() + 1;
                    ui.item.data('oldIndex', oldIndex);
                    ui.placeholder.height(ui.item.height());
                },
                update: function(event, ui) {
                    $("#table_gallery .overlay").show();
                    var ids = [];
                    $("#galleryF ul li").each(function(){
                        ids.push($(this).attr("data-image-id"));
                    });
                    var isTemp = ui.item.attr('data-is-temp') === '1' ? 1 : 0;
                    $.post("<?php echo url('setImageGalleryOrder'); ?>", {
                        galleryId: $('#gallery_id').val(),
                        images: ids.toString(),
                        _token: '<?php echo csrf_token();  ?>',
                        isTemp: isTemp
                    }, function (data) {
                        loadGallery('<?php echo isset($spListingDataId[0]->id)?$spListingDataId[0]->id:0; ?>', 'y', 'sponsors', "editFe", '');
                        $("#table_gallery .overlay").hide();
                    }, 'json');
                }
            });
            $("#galleryF ul").disableSelection();
        });


    }
  

    function changePosSelect(location_4) {

        $("#search_pos_div").hide();
        $("#search_pos_div2").hide();
        $("#div_img_loading_pos").show();

        $.post("<?php echo url('get-search-pos'); ?>", {
            location_4: location_4,
            search_pos: "<?php echo isset($spListingDataId[0]->search_pos)?$spListingDataId[0]->search_pos:0; ?>"
        }, function (ret) {
            $("#search_pos_div").html(ret);
            $("#div_img_loading_pos").hide();
            $("#search_pos_div").show();
            $("#search_pos_div2").show();
        });
    }

     

    </script>


 @if ($errors->has())
 <p class="errorMessage" > 
  @foreach ($errors->all() as $error)
    {!! $error !!}<br />		
  @endforeach
</p>
@endif

 @if (session('message'))
 <p class="errorMessage" > 
       <?php  echo session('message'); ?>
 </p>
@endif
<table class="standard-table onebackground">

    <tbody><tr class="bigger">
        <th class="wrap">
            <i><span>Required field</span></i>
            Listing Title:        </th>
        <td colspan="2">
        <!--$title-->
            <input type="text" name="title" value="<?php echo isset($spListingDataId[0]->title)?$spListingDataId[0]->title:old("title"); ?>" maxlength="100">
            <input type="hidden" name="friendly_url" id="friendly_url" value="<?php echo isset($spListingDataId[0]->friendly_url)?$spListingDataId[0]->friendly_url:''; ?>" maxlength="150">
                    </td>
    </tr>

                <tr style="display:none;">
            <td>
                <input type="hidden" id="is_featured" name="is_featured" value="<?php echo isset($spListingDataId[0]->is_featured)?$spListingDataId[0]->is_featured:''; ?>">
                <input type="hidden" id="featured_date" name="featured_date" value="<?php echo isset($spListingDataId[0]->featured_date)?$spListingDataId[0]->featured_date:''; ?>">
            </td>
        </tr>
    </tbody></table>


    <table class="standard-table">
        <tbody><tr>
            <th>Description</th>
            <td>
<textarea name="long_description" rows="5"><?php echo isset($spListingDataId[0]->long_description)?$spListingDataId[0]->long_description:old("long_description"); ?></textarea>
            </td>
        </tr>
    </tbody></table>
    


<table class="standard-table">
    <tbody><tr>
        <th colspan="2" class="standard-tabletitle">Contact Information</th>
    </tr>

    <tr>
        <th>First Name:</th>
        <td>
            <input type="text" name="contact_fname" value="<?php echo isset($spListingDataId[0]->contact_fname)?$spListingDataId[0]->contact_fname:old("contact_fname"); ?>">
        </td>
    </tr>

    <tr>
        <th>Last Name:</th>
        <td>
            <input type="text" name="contact_lname" value="<?php echo isset($spListingDataId[0]->contact_lname)?$spListingDataId[0]->contact_lname:old("contact_lname"); ?>">
        </td>
    </tr>

            <tr>
            <th>Renter Inquiry Email:</th>
            <td>
                <input type="text" name="email" value="<?php echo isset($spListingDataId[0]->email)?$spListingDataId[0]->email:old("email"); ?>" maxlength="50">
                <span>This will be the email that you want potential renters to send rental requests.</span>
            </td>
        </tr>
    
    
        <tr>
        <th>Phone:</th>
        <td>
            <input type="text" name="phone" value="<?php echo isset($spListingDataId[0]->phone)?$spListingDataId[0]->phone:old("phone"); ?>">
            <span>This will be the phone number that you want potential renters to call you on.</span>
        </td>
    </tr>
    
            <tr>
            <th>Fax:</th>
            <td><input type="text" name="fax" value="<?php echo isset($spListingDataId[0]->fax)?$spListingDataId[0]->fax:old("fax"); ?>"></td>
        </tr>
    
    
    </tbody></table>

<table class="standard-table noMargin">
    <tbody class="vac-rental-loc"><tr>
        <th colspan="2" class="standard-tabletitle">Vacation Rental Location</th>
    </tr>
    <tr>
        <th class="wrap">
            Property Address Line 1:        </th>
        <td>
            <input type="text" name="address" id="address" onblur="loadMap(document.listing);" value="<?php echo isset($spListingDataId[0]->address)?$spListingDataId[0]->address:old("address"); ?>" maxlength="50">
            <span>
                Street Address, P.O. box </span>
        </td>
    </tr>
    <tr>
        <th class="wrap">
            Property Address Line 2:        </th>
        <td>
            <input type="text" name="address2" value="<?php echo isset($spListingDataId[0]->address2)?$spListingDataId[0]->address2:old("address2"); ?>" maxlength="50">
            <span>Apartment, suite, unit, building, floor, etc.</span>
        </td>
    </tr>

    <tr>
        <th>Property Zip:</th>
        <td><input type="text" name="zip_code" id="zip_code" onblur="loadMap(document.listing);" value="<?php echo isset($spListingDataId[0]->zip_code)?$spListingDataId[0]->zip_code:old("zip_code"); ?>" maxlength="10"></td>
    </tr>

    
    
    <tr style="display: none">
        <th>Latitude:</th>
        <td>
    <input type="text" name="latitude" id="latitude" onblur="loadMap(document.listing, true);" value="<?php echo isset($spListingDataId[0]->latitude)?$spListingDataId[0]->latitude:old("latitude"); ?>" maxlength="10">
            <span>Ex: 38.830391</span>
        </td>
    </tr>

    <tr style="display: none">
        <th>Longitude:</th>
        <td>
            <input type="text" name="longitude" id="longitude" onblur="loadMap(document.listing, true);" value="<?php echo isset($spListingDataId[0]->longitude)?$spListingDataId[0]->longitude:old("longitude"); ?>" maxlength="10">
            <span>Ex: -77.196370</span>
        </td>
    </tr>

    </tbody></table>
    
		<div id="formsLocation" class="form-location vac-location-place">
			<table cellpadding="0" cellspacing="0" border="0" class="standard-table standardSIGNUPTable "> 							<tbody><tr>
								<th> <label for="location_1">Country:</label></th>
								<td colspan="2"> United States </td>
								<td>&nbsp;</td>
							</tr>
					<input type="hidden" name="location_1" value="<?php echo isset($spListingDataId[0]->location_1)?$spListingDataId[0]->location_1:'1'; ?>">
						<tr id="div_location_3">
							<th>
							<label for="location_3">Property State:</label>
							</th>
							<td class="field" id="div_img_loading_3" style="display:none;">
								<img src="<?php echo url(); ?>/images/img_loading_bar.gif" alt="Wait, Loading...">
							</td>
                            <td id="div_select_3" class="field locationSelect">
                            
        <select class="select " name="location_3" id="location_3" onchange="loadLocationSitemgrMembers('<?php echo url(); ?>', '1,3,4', 3, 4, this.value,'<?php echo (isset($spListingDataId[0]->location_4) && $spListingDataId[0]->location_4>0)?$spListingDataId[0]->location_4:''; ?><?php echo (old("location_4"))?old("location_4"):''; ?>'); loadMap(document.listing)">
                                
                            <option id="l_location_3" value="">Select a State</option>
                            @foreach($locations_3 as $location)

      <option <?php echo (isset($spListingDataId[0]->location_3) && $spListingDataId[0]->location_3==$location->id)?'SELECTED':''; ?> <?php echo (old('location_3')==$location->id)?'SELECTED':''; ?>  value="{{ $location->id }}">{{ $location->name }}</option>

                            @endforeach

                                   
                           <!--<option <?php echo (isset($spListingDataId[0]->location_3) && $spListingDataId[0]->location_3==3)?'SELECTED':''; ?> value="3">Florida</option>
                                    <option <?php echo (isset($spListingDataId[0]->location_3) && $spListingDataId[0]->location_3==11)?'SELECTED':''; ?> value="11">Maryland</option>
                                    <option <?php echo (isset($spListingDataId[0]->location_3) && $spListingDataId[0]->location_3==35)?'SELECTED':''; ?> value="35">Nayarit</option>
                                    <option <?php echo (isset($spListingDataId[0]->location_3) && $spListingDataId[0]->location_3==6)?'SELECTED':''; ?> value="6">New Jersey</option>
                                    <option <?php echo (isset($spListingDataId[0]->location_3) && $spListingDataId[0]->location_3==10)?'SELECTED':''; ?> value="10">North Carolina</option>		-->						
                                </select>
                                <div class="field" id="box_no_location_found_3" style="display:none;">No locations found..</div>
                            </td>						
                            <td class="field">
                                <div id="div_new_location3_link">
                                    <a href="javascript:void(0);" onclick="showNewLocationField('3', '1,3,4', true);" style=" cursor: pointer">+ Add a new state</a>
                                </div>								
                            </td>						
						</tr>

						
                        <tr id="div_new_location3_field" style="display:none;">
                            <th>
                                <label for="newlocation">Add a new state:</label>
                            </th>
                            <td class="field">
                                <input type="text" value="" name="new_location3_field" 
                                id="new_location3_field"  onfocus="showNewLocationField('4', '1,3,4', false);" onblur="easyFriendlyUrl(this.value, 'new_location3_friendly', 'a-zA-Z0-9', '-');  loadMap(document.listing); ">
                                <input type="hidden" name="new_location3_friendly" id="new_location3_friendly" value="">
                            </td>
                            <td class="field" colspan="2">
                                <div id="div_new_location3_back">
                                    <a href="javascript:void(0);" onclick="hideNewLocationField('3', '1,3,4',true);" style=" cursor: pointer">- Choose an existing state</a>
                                </div>
                            </td>
                        </tr>
						
                        <tr id="div_location_4" style="display:none;">
                            <th>
                                <label for="location_4">Property City:</label>
                            </th>
                            <td class="field" id="div_img_loading_4" style="display:none;">
                                <img src="<?php echo url(); ?>/images/img_loading_bar.gif" alt="Wait, Loading...">
                            </td>
                            <td id="div_select_4" class="field locationSelect">
                                <select class="select "  name="location_4" id="location_4" onchange="loadMap(document.listing);changePosSelect(this.value);">
                                
                                </select>
                                <div class="field" id="box_no_location_found_4" style="display:none;">No locations found..</div>
                            </td>						
                            <td class="field">
                                <div id="div_new_location4_link">
                                    <a href="javascript:void(0);" onclick="showNewLocationField('4', '1,3,4', true);" style=" cursor: pointer">+ Add a new city</a>
                                </div>								
                            </td>						
                        </tr>

						
                        <tr id="div_new_location4_field" style="display:none;">
                            <th>
                                <label for="newlocation">Add a new city:</label>
                            </th>
                            <td class="field">
                                <input type="text" value="" name="new_location4_field" id="new_location4_field" onblur="easyFriendlyUrl(this.value, 'new_location4_friendly', 'a-zA-Z0-9', '-');  loadMap(document.listing); ">
                                <input type="hidden" name="new_location4_friendly" id="new_location4_friendly" value="{{ old('new_location4_friendly') }}">
                            </td>
                            <td class="field" colspan="2">
                                <div id="div_new_location4_back">
                                    <a href="javascript:void(0);" onclick="hideNewLocationField('4', '1,3,4');" style=" cursor: pointer">- Choose an existing city</a>
                                </div>
                            </td>
                        </tr>
									</tbody></table>
                </div>         
                <table class="standard-table" style="display:none;">
                    <tbody>
                        <tr>
                            <td>
                                <input type="hidden" id="search_pos" name="search_pos" value="<?php echo isset($spListingDataId[0]->search_pos)?$spListingDataId[0]->search_pos:0; ?>">
                                <input type="hidden" id="search_pos_date" name="search_pos_date" value="<?php echo isset($spListingDataId[0]->search_pos_date)?$spListingDataId[0]->search_pos_date:0; ?>">
                            </td>
                        </tr>
                    </tbody>
                </table>
    
    <script src="https://maps.google.com/maps/api/js?key=AIzaSyDMpUvGopK6RLmIqobIviCo4tWLMQXj8N4" type="text/javascript"></script>
    <script type="text/javascript">

        var geocoder;
        var map;
        
        function displayMapForm(form, show) {
            var use_lat_long = false;
            if ($.cookie('showMapForm') == 1 || show) {
                $('#trMap').css('display', '');
                $('#map').css('display', '');
                $('#tipsMap').css('display', '');
                $('#linkDisplayMap').text('' + showText(LANG_JS_LABEL_HIDEMAP) + '');
                $.cookie('showMapForm', '0', {expires: 7, path: '/'});
                if (!show){
                    
                    if (document.getElementById('myLatitudeLongitude').value) {
                        use_lat_long = true;
                    }
                    
                    loadMap(form, use_lat_long);
                }
            } else {
                $('#map').css('display', 'none');
                $('#trMap').css('display', 'none');
                $('#tipsMap').css('display', 'none');
                $('#linkDisplayMap').text('' + showText(LANG_JS_LABEL_SHOWMAP) + '');
                $.cookie('showMapForm', '1', {expires: 7, path: '/'});
            }
        }
        
        function setCoordinates(coord) {
            var new_lat;
            var new_long;
            var aux_latlong;

            document.getElementById('myLatitudeLongitude').value = coord;
            if (document.getElementById('maptuning_done')) {
                document.getElementById('maptuning_done').value = "y";
            }
            aux_latlong = document.getElementById('myLatitudeLongitude').value;
            aux_latlong = aux_latlong.replace("(", "").replace(")", "").replace(" ", "").split(',');
            new_lat = aux_latlong[0]; 
            new_long  = aux_latlong[1];
            
            var num_lat = new Number(new_lat);
            var num_long = new Number(new_long);
            
            document.getElementById('latitude').value = num_lat.toFixed(6);
            document.getElementById('longitude').value = num_long.toFixed(6);
        }
        
        function initialize(map_zoom, google_location, latitude, longitude, use_lat_long) {
            
            geocoder = new google.maps.Geocoder();
            var myOptions = {
              zoom: map_zoom,
              scrollwheel: false,
              mapTypeId: google.maps.MapTypeId.ROADMAP
            }
            map = new google.maps.Map(document.getElementById("map"), myOptions);
            codeAddress(google_location, latitude, longitude, use_lat_long); 
            
            function codeAddress(google_location, latitude, longitude, use_lat_long) {
                var address = google_location;
                var marker = new google.maps.Marker({
                    map: map, 
                    draggable: true
                });
                
                if (use_lat_long && latitude && longitude){
                    var latlng = new google.maps.LatLng(latitude, longitude);
                    marker.setPosition(latlng);
                    map.setCenter(latlng);
                } else {
                    if (geocoder) {
                        geocoder.geocode( { 'address': address}, function(results, status) {
                            if (status != google.maps.GeocoderStatus.OK) {
                                fancy_alert("Not found. Please, try to specify better your location.", "errorMessage", false, 300, 90, false);
                            }
                            if (status == google.maps.GeocoderStatus.OK) {
                                map.setCenter(results[0].geometry.location);
                                marker.setPosition(results[0].geometry.location);
                                setCoordinates(results[0].geometry.location);
                            }
                        });
                    }
                }
                
                google.maps.event.addListener(marker, 'dragend', function(event) {
                    setCoordinates(event.latLng);                    
                });
                
                google.maps.event.addListener(map, 'zoom_changed', function() {
                    document.getElementById('map_zoom').value = map.getZoom();
                });
                
            }   
        }
        
        function loadMap(form, use_lat_long){
            var address = document.getElementById('address').value;
            var zip = document.getElementById('zip_code').value;
            var location_3 = '';
            var location_4 = '';
            var latitude = document.getElementById('latitude').value; 
            var longitude = document.getElementById('longitude').value; 
            var index;
            var google_location = '';
            var locations = new Array();
            var array_index = 0;
            var callMap = false;
            var valid_coord = true;
            var msg_invalid = '';
            
            if (use_lat_long && latitude && longitude){
                
                if (!isFinite(latitude) || !isFinite(longitude) || latitude < -90 || latitude > 90 || longitude < -180 || longitude > 180){
                    
                    msg_invalid = "<b>The following fields contain errors:</b><br />";
                    
                    if (!isFinite(latitude) || latitude < -90 || latitude > 90){
                        valid_coord = false;
                        msg_invalid = msg_invalid + "&#149;&nbsp;Latitude must be a number between -90 and 90.<br />";
                    }
                    if (!isFinite(longitude) || longitude < -180 || longitude > 180){
                        valid_coord = false;
                        msg_invalid = msg_invalid + "&#149;&nbsp;Longitude must be a number between -180 and 180.<br />";
                    }
                }
                
                if (valid_coord){
                   callMap = true; 
                } else {
                   fancy_alert(msg_invalid, "errorMessage", false, 450, 150, false); 
                }
                
            } else {
                if (address){
                    locations[array_index] = address;
                    array_index++;
                    callMap = true;
                }

                if (zip){
                    locations[array_index] = zip;
                    array_index++;
                    callMap = true;
                }

                 

         
  if (document.getElementById('new_location4_field') && document.getElementById('new_location4_field').value) {
                        location_4 = document.getElementById('new_location4_field').value;
                        locations[array_index] = location_4;
                        array_index++;
                        callMap = true;
                }

 if (document.getElementById('location_4') && document.getElementById('location_4').value) {
                    index = form.location_4.selectedIndex;
                    if (document.getElementById('location_4').options[index].value) {
                        location_4 = document.getElementById('location_4').options[index].text;
                        locations[array_index] = location_4;
                        array_index++;
                        callMap = true;
                    }
                } else if (document.getElementById('city')) {
                    location_4 = document.getElementById('city').value;
                    locations[array_index] = location_4;
                    array_index++;
                    callMap = true;
                }

                if (document.getElementById('location_3')) {
                    index = form.location_3.selectedIndex;
                    if (document.getElementById('location_3').options[index].value) {
                        location_3 = document.getElementById('location_3').options[index].text;
                        locations[array_index] = location_3;
                        array_index++;
                        callMap = true;
                    }
                } else if (document.getElementById('state')) {
                    location_3 = document.getElementById('state').value;
                    locations[array_index] = location_3;
                    array_index++;
                    callMap = true;
                }

                google_location = locations.join(", ");
            }

            if (callMap){
                $("#tableMapTuning").css("display", "");
                $("#divDisplayMap").css("display", "block");
                $('#linkDisplayMap').text('' + showText('Yes') + '');
                //$.cookie('showMapForm', '0', {expires: 7, path: '/'});
                initialize(15, google_location, latitude, longitude, use_lat_long);
            } else {
                if (!latitude && !longitude){
                    $("#tableMapTuning").css("display", "none");
                    $("#divDisplayMap").css("display", "none");
                }
            }
        }

    </script>   
    <table class="standard-table noMargin" id="tableMapTuning" style="display:none;">
        <tbody>
        <tr>
            <th colspan="2" class="standard-tabletitle">
                Map Tuning                <div id="tipsMap">
                    <span style="text-align: justify;">Use the controls "+" and "-" to adjust the map zoom.</span>
                    <br>
                    <span style="text-align: justify;">Use the arrows to navigate on map.</span>
                    <br>
                    <span style="text-align: justify;">Drag-and-Drop the marker to adjust the location.</span>
                </div>
            </th>
        </tr>

        <tr>
            <td>
            <div id="map" class="googleBase" style="border: 1px solid rgb(0, 0, 0);" ></div>
                <input type="hidden" name="latitude_longitude" id="myLatitudeLongitude" value="">
                <input type="hidden" name="map_zoom" id="map_zoom" value="0">
                <input type="hidden" name="maptuning_done" id="maptuning_done" value="n">
            </td>
        </tr>

    </tbody></table>


        <table class="standard-table images-table-main">
            <tbody>
                <tr>
                    <th colspan="3" class="standard-tabletitle">Images <span>(890px x 582px) (JPG, GIF or PNG)</span></th>
                </tr>
                
                <tr id="table_gallery">
                    <th colspan="3" class="Full">
                        <div class="overlay spinner"></div>
                        <input type="hidden" name="gallery_id" id="gallery_id" value="<?php echo isset($spListingGallery[0]->gallery_id)?$spListingGallery[0]->gallery_id:0;  ?>">
                        
                        <div id="galleryF"></div>
                    </th>
                </tr>
               
                <tr>
                    <td class="alignTop width100">
             <?php
             $level_id='';
             if(isset($spListingDataId[0]->level))
                { 
                    $level_id.=$spListingDataId[0]->level; 
                }
            elseif(isset($level)){ 
                $level_id.=$level; 
            }
            else{ 
                $level_id.=0; 
            }
             ?>       

          <script type="text/javascript">
         function fancyclose()
        {

        setTimeout(function(){
               loadGallery(<?php echo isset($spListingDataId[0]->id)?$spListingDataId[0]->id:0 ?>, 'y', 'sponsors', '', '');
          }, 1500)
       

        setTimeout(function(){
              parent.$.fancybox.close();
          }, 500)

        }
          </script>
 
                        <!--                    <p class="successMessage highlight">Add some images to your listing and get more visibility for you business!</p>-->
            <a id="uploadimage" href="<?php echo url('sponsors/popupUploadImage?domain_id=1&pop_type=uploadimage&gallery_hash='.(isset($spListingGallery[0]->sess_id)?$spListingGallery[0]->sess_id:$gallery_hash).'&item_type=listing&item_id='.(isset($spListingDataId[0]->id)?$spListingDataId[0]->id:0).'&gallery_id='.(isset($spListingGallery[0]->gallery_id)?$spListingGallery[0]->gallery_id:0).'&photos=31&level='.$level_id); ?>" class="addImageForm input-button-form iframe fancy_window_imgAdd">Add Image</a>
                        <span class="errMaxsize">Maximum file size 8 MB. Animated .gif isn't supported.</span>
                        <p class="informationMessage">This listing will show the maximum of 31 images in the gallery  plus one main image.</p>
                    </td>
                </tr>
            </tbody>
        </table>

    
            <table class="standard-table">
        <tbody><tr>
            <th colspan="2" class="standard-tabletitle">Details</th>
        </tr>

        <tr>
            <th>
                Bedroom(s)            </th>
            <td>

                <select name="bedroom">
                    <option value="">Select...</option>
                    <option <?php echo (isset($spListingDataId[0]->bedroom) && $spListingDataId[0]->bedroom=='studio')?'SELECTED':''; ?>  <?php echo (old('bedroom')=='studio')?'SELECTED':''; ?> value="studio">Studio</option>
                    <option <?php echo (isset($spListingDataId[0]->bedroom) && $spListingDataId[0]->bedroom=='1')?'SELECTED':''; ?> <?php echo (old('bedroom')=='1')?'SELECTED':''; ?> value="1">1 Bedroom</option>
                    <option <?php echo (isset($spListingDataId[0]->bedroom) && $spListingDataId[0]->bedroom=='2')?'SELECTED':''; ?> <?php echo (old('bedroom')=='2')?'SELECTED':''; ?> value="2">2 Bedrooms</option>
                    <option <?php echo (isset($spListingDataId[0]->bedroom) && $spListingDataId[0]->bedroom=='3')?'SELECTED':''; ?> <?php echo (old('bedroom')=='3')?'SELECTED':''; ?> value="3">3 Bedrooms</option>
                    <option <?php echo (isset($spListingDataId[0]->bedroom) && $spListingDataId[0]->bedroom=='4')?'SELECTED':''; ?> <?php echo (old('bedroom')=='4')?'SELECTED':''; ?> value="4">4 Bedrooms</option>
                    <option <?php echo (isset($spListingDataId[0]->bedroom) && $spListingDataId[0]->bedroom=='5')?'SELECTED':''; ?> <?php echo (old('bedroom')=='5')?'SELECTED':''; ?> value="5">5 Bedrooms</option>
                    <option <?php echo (isset($spListingDataId[0]->bedroom) && $spListingDataId[0]->bedroom=='6')?'SELECTED':''; ?> <?php echo (old('bedroom')=='6')?'SELECTED':''; ?> value="6">6 Bedrooms</option>
                    <option <?php echo (isset($spListingDataId[0]->bedroom) && $spListingDataId[0]->bedroom=='7')?'SELECTED':''; ?> <?php echo (old('bedroom')=='7')?'SELECTED':''; ?> value="7">7 Bedrooms</option>
                    <option <?php echo (isset($spListingDataId[0]->bedroom) && $spListingDataId[0]->bedroom=='8')?'SELECTED':''; ?> <?php echo (old('bedroom')=='8')?'SELECTED':''; ?> value="8">8 Bedrooms</option>
                    <option <?php echo (isset($spListingDataId[0]->bedroom) && $spListingDataId[0]->bedroom=='9')?'SELECTED':''; ?> <?php echo (old('bedroom')=='9')?'SELECTED':''; ?> value="9">9 Bedrooms</option>
                    <option <?php echo (isset($spListingDataId[0]->bedroom) && $spListingDataId[0]->bedroom=='10')?'SELECTED':''; ?> <?php echo (old('bedroom')=='10')?'SELECTED':''; ?> value="10">10 Bedrooms</option>
                    <option <?php echo (isset($spListingDataId[0]->bedroom) && $spListingDataId[0]->bedroom=='11')?'SELECTED':''; ?> <?php echo (old('bedroom')=='11')?'SELECTED':''; ?> value="11">11 Bedrooms</option>
                    <option <?php echo (isset($spListingDataId[0]->bedroom) && $spListingDataId[0]->bedroom=='12')?'SELECTED':''; ?> <?php echo (old('bedroom')=='12')?'SELECTED':''; ?> value="12">12 Bedrooms</option>
                    <option <?php echo (isset($spListingDataId[0]->bedroom) && $spListingDataId[0]->bedroom=='13+')?'SELECTED':''; ?> <?php echo (old('bedroom')=='13+')?'SELECTED':''; ?> value="13+">13+ Bedrooms</option>
                </select>
            </td>
        </tr>
        <tr>
            <th>
                Bedding size 
            </th>
            <td>
                <input type="text" name="bedsize" value="<?php echo isset($spListingDataId[0]->bedsize)?$spListingDataId[0]->bedsize:old("bedsize"); ?>">
            </td>
        </tr>
        <tr>
            <th>
                Bathroom(s)            </th>
            <td>
                <input type="text" name="bathroom" size="4" maxlength="4" value="<?php echo isset($spListingDataId[0]->bathroom)?$spListingDataId[0]->bathroom:old("bathroom"); ?>" style="width:40px">
            </td>
        </tr>
        <tr>
            <th>
                Sleeps            </th>
            <td>
                <input type="text" name="sleeps" class="number_field" size="4" maxlength="3" value="<?php echo isset($spListingDataId[0]->sleeps)?$spListingDataId[0]->sleeps:old("sleeps"); ?>"  style="width:40px">
            </td>
        </tr>
        <tr>
            <th>
                Property Type            
            </th>
            <td>
            <select id="property_type" name="property_type">
            <option <?php echo (isset($spListingDataId[0]->property_type) && $spListingDataId[0]->property_type=='')?'SELECTED':''; ?>  value="">All</option>
            <option <?php echo (isset($spListingDataId[0]->property_type) && $spListingDataId[0]->property_type=='condominium')?'SELECTED':''; ?> <?php echo (old('property_type')=='condominium')?'SELECTED':''; ?> value="condominium">Condominium</option>
            <option <?php echo (isset($spListingDataId[0]->property_type) && $spListingDataId[0]->property_type=='single_family')?'SELECTED':''; ?> <?php echo (old('property_type')=='single_family')?'SELECTED':''; ?> value="single_family">Single Family</option>
            <option <?php echo (isset($spListingDataId[0]->property_type) && $spListingDataId[0]->property_type=='studio')?'SELECTED':''; ?> <?php echo (old('property_type')=='studio')?'SELECTED':''; ?> value="studio">Studio</option>
            <option <?php echo (isset($spListingDataId[0]->property_type) && $spListingDataId[0]->property_type=='townhome')?'SELECTED':''; ?> <?php echo (old('property_type')=='townhome')?'SELECTED':''; ?> value="townhome">Townhome</option>
            </select>
            </td>
        </tr>
        <tr>
            <th>
                View            </th>
            <td>
                <select id="view" name="view">
                    <option <?php echo (isset($spListingDataId[0]->view) && $spListingDataId[0]->view=='')?'SELECTED':''; ?> value="">Not Applicable</option>
                    <option <?php echo (isset($spListingDataId[0]->view) && $spListingDataId[0]->view=='ocean')?'SELECTED':''; ?> <?php echo (old('view')=='ocean')?'SELECTED':''; ?>  value="ocean">Ocean View</option>
                    <option <?php echo (isset($spListingDataId[0]->view) && $spListingDataId[0]->view=='ocean_front')?'SELECTED':''; ?> <?php echo (old('view')=='ocean_front')?'SELECTED':''; ?> value="ocean_front">Ocean Front View</option>
                    <option <?php echo (isset($spListingDataId[0]->view) && $spListingDataId[0]->view=='bay')?'SELECTED':''; ?> <?php echo (old('view')=='bay')?'SELECTED':''; ?> value="bay">Bay View</option>
                    <option <?php echo (isset($spListingDataId[0]->view) && $spListingDataId[0]->view=='bay_front')?'SELECTED':''; ?> <?php echo (old('view')=='bay_front')?'SELECTED':''; ?> value="bay_front">Bay Front View</option>
                    <option <?php echo (isset($spListingDataId[0]->view) && $spListingDataId[0]->view=='bay_ocean_front')?'SELECTED':''; ?> <?php echo (old('view')=='bay_ocean_front')?'SELECTED':''; ?> value="bay_ocean_front">Bay &amp; Ocean View</option>
                </select>
            </td>
        </tr>
        <tr>
            <th>
                Distance to Beach            
            </th>
            <td>
            <select id="distance_beach" name="distance_beach">
                <option <?php echo (isset($spListingDataId[0]->distance_beach) && $spListingDataId[0]->distance_beach=='')?'SELECTED':''; ?> value="">No Preference</option>
                <option <?php echo (isset($spListingDataId[0]->distance_beach) && $spListingDataId[0]->distance_beach=='less1block')?'SELECTED':''; ?> <?php echo (old('distance_beach')=='less1block')?'SELECTED':''; ?> value="less1block">&lt; 1 block</option>
                <option <?php echo (isset($spListingDataId[0]->distance_beach) && $spListingDataId[0]->distance_beach=='1block')?'SELECTED':''; ?> <?php echo (old('distance_beach')=='1block')?'SELECTED':''; ?> value="1block">1 block</option>
                <option <?php echo (isset($spListingDataId[0]->distance_beach) && $spListingDataId[0]->distance_beach=='2blocks')?'SELECTED':''; ?> <?php echo (old('distance_beach')=='2blocks')?'SELECTED':''; ?> value="2blocks">2 blocks</option>
                <option <?php echo (isset($spListingDataId[0]->distance_beach) && $spListingDataId[0]->distance_beach=='3blocks')?'SELECTED':''; ?> <?php echo (old('distance_beach')=='3blocks')?'SELECTED':''; ?> value="3blocks">3 blocks</option>
                <option <?php echo (isset($spListingDataId[0]->distance_beach) && $spListingDataId[0]->distance_beach=='4blocks')?'SELECTED':''; ?> <?php echo (old('distance_beach')=='4blocks')?'SELECTED':''; ?> value="4blocks">4 blocks</option>
                <option <?php echo (isset($spListingDataId[0]->distance_beach) && $spListingDataId[0]->distance_beach=='5blocks')?'SELECTED':''; ?> <?php echo (old('distance_beach')=='5blocks')?'SELECTED':''; ?> value="5blocks">5 blocks</option>
                <option <?php echo (isset($spListingDataId[0]->distance_beach) && $spListingDataId[0]->distance_beach=='6to10blocks')?'SELECTED':''; ?> <?php echo (old('distance_beach')=='6to10blocks')?'SELECTED':''; ?> value="6to10blocks">6-10 blocks</option>
                <option <?php echo (isset($spListingDataId[0]->distance_beach) && $spListingDataId[0]->distance_beach=='1mile')?'SELECTED':''; ?> value="1mile">1 mile</option>
                <option <?php echo (isset($spListingDataId[0]->distance_beach) && $spListingDataId[0]->distance_beach=='more1mile')?'SELECTED':''; ?> value="more1mile">&gt; 1 mile</option>
            </select>
            </td>
        </tr>
        <tr>
            <th>
                <div class="development_id">
                    Development Name                
                </div>
                <div class="development_name" style="display: none;">
                    Add new development Name                
                </div>
            </th>
            <td>
                                <table class="noMargin" cellpadding="0" cellspacing="0" border="0" width="100%">
                    <tbody><tr>
                        <td width="470">
                            <?php if(isset($developmentData) && !empty($developmentData)){ ?>
                                <select name="development_id" id="development_id" class="development_id">
                                    <option value="">Select Development Name</option>
                                    <?php foreach($developmentData as $key=>$val){
                                    ?>
                                    <option <?php echo (isset($spListingDataId[0]->development_id) && $spListingDataId[0]->development_id==$val->id)?'SELECTED':''; ?> <?php echo (old('development_id')==$val->id)?'SELECTED':''; ?> value="<?php echo $val->id; ?>"><?php echo $val->name; ?></option>
                                    <?php }?>
                                    
                                
                                </select>
                            <?php } ?>
                            <input type="text" name="development_name" value="{{ old('development_name') }}" class="development_name" style="display: none;">
                        </td>
                        <td class="field">
                            <a href="" class="development_id developmentNameToggleInput">+ Add new development name</a>
                            <a href="" class="development_name developmentNameToggleInput" style="display: none;">+ Use existing development name</a>
                        </td>
                    </tr>
                </tbody></table>
                <script>

                    $(document).ready(function(){
                        $(".developmentNameToggleInput").click(function(e){
                            e.preventDefault();
                            if($("#development_id").is(":visible")){
                                $(".development_id").each(function(){
                                    if($(this).is(":input")){
                                        $(this).val("");
                                    }
                                    $(this).css("display", "none");
                                })
                                $(".development_name").each(function(){
                                    $(this).css("display", "");
                                })
                            }else{
                                $(".development_name").each(function(){
                                    if($(this).is(":input")){
                                        $(this).val("");
                                    }
                                    $(this).css("display", "none");
                                })
                                $(".development_id").each(function(){
                                    $(this).css("display", "");
                                })
                            }
                        });
                    })

                </script>


            </td>
        </tr>
        <tr>
            <th>Required Stay</th>
            <td><input type="text" name="required_stay" value="<?php echo isset($spListingDataId[0]->required_stay)?$spListingDataId[0]->required_stay:old('required_stay'); ?>"></td>
        </tr>

        <tr>
            <th>
                Airport Abbreviation            </th>
            <td>
                <input type="text" name="airport_abbreviation" value="<?php echo isset($spListingDataId[0]->airport_abbreviation)?$spListingDataId[0]->airport_abbreviation:old('airport_abbreviation'); ?>" maxlength="5" style="width: 50px;">
            </td>
        </tr>

        <tr>
            <th>
                Airport Distance            </th>
            <td>
                <input type="text" name="airport_distance" value="<?php echo isset($spListingDataId[0]->airport_distance)?$spListingDataId[0]->airport_distance:old('airport_distance'); ?>" maxlength="7" style="width: 50px;"> &nbsp; miles            </td>
        </tr>

    </tbody></table>

    <table class="standard-table">
        <tbody><tr>
            <th colspan="2" class="standard-tabletitle">Links to more information</th>
        </tr>

        <tr>
            <th>
                External Link Text 1            </th>
            <td>
                <input type="text" name="external_link_text1" value="<?php echo isset($spListingDataId[0]->external_link_text1)?$spListingDataId[0]->external_link_text1:old('external_link_text1'); ?>">
            </td>
        </tr>
        <tr>
            <th>
                External Link 1            </th>
            <td>
                <input type="text" name="external_link1" value="<?php echo isset($spListingDataId[0]->external_link1)?$spListingDataId[0]->external_link1:old('external_link1'); ?>">
                <span>http://exemple1.com</span>
            </td>
        </tr>
        <tr>
            <th>
                External Link Text 2            </th>
            <td>
                <input type="text" name="external_link_text2" value="<?php echo isset($spListingDataId[0]->external_link_text2)?$spListingDataId[0]->external_link_text2:old('external_link_text2'); ?>">
            </td>
        </tr>
        <tr>
            <th>
                External Link 2            </th>
            <td>
                <input type="text" name="external_link2" value="<?php echo isset($spListingDataId[0]->external_link2)?$spListingDataId[0]->external_link2:old('external_link2'); ?>" >
                <span>http://exemple2.com</span>
            </td>
        </tr>
        <tr>
            <th>
                External Link Text 3            </th>
            <td>
                <input type="text" name="external_link_text3" value="<?php echo isset($spListingDataId[0]->external_link_text3)?$spListingDataId[0]->external_link_text3:old('external_link_text3'); ?>" >
            </td>
        </tr>
        <tr>
            <th>
                External Link 3            </th>
            <td>
                <input type="text" name="external_link3" value="<?php echo isset($spListingDataId[0]->external_link3)?$spListingDataId[0]->external_link3:old('external_link3'); ?>" >
                <span>http://exemple3.com</span>
            </td>
        </tr>

    </tbody>
    </table>

    <table class="standard-table amenities-tab-Main">
    <tbody>
    <tr>
        <th colspan="6" class="standard-tabletitle">Amenities</th>
    </tr>
    <tr>
    <th>
    <input type="checkbox" name="amenity_airhockey" id="amenity_airhockey" <?php echo (isset($spListingDataId[0]->amenity_airhockey) && $spListingDataId[0]->amenity_airhockey=='y')?'CHECKED':''; ?> <?php echo (old('amenity_airhockey')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
    </th>
    <td>
    <label for="amenity_airhockey">Air Hockey</label>
    </td>
    <th>
    <input type="checkbox" name="amenity_alarmclock" id="amenity_alarmclock" 
    <?php echo (isset($spListingDataId[0]->amenity_alarmclock) && $spListingDataId[0]->amenity_alarmclock=='y')?'CHECKED':''; ?> <?php echo (old('amenity_alarmclock')=='y')?'CHECKED':''; ?>  value="y" class="inputCheck">
    </th>
    <td>
    <label for="amenity_alarmclock">Alarm Clock</label>
    </td>
    <th>
    <input type="checkbox" name="amenity_answeringmachine" id="amenity_answeringmachine" 
    <?php echo (isset($spListingDataId[0]->amenity_answeringmachine) && $spListingDataId[0]->amenity_answeringmachine=='y')?'CHECKED':''; ?> <?php echo (old('amenity_answeringmachine')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
    </th>
    <td>
    <label for="amenity_answeringmachine">Answering Machine</label>
    </td>
    </tr>
    <tr>
    <th>
    <input type="checkbox" name="amenity_arcadegames" id="amenity_arcadegames" 
    <?php echo (isset($spListingDataId[0]->amenity_arcadegames) && $spListingDataId[0]->amenity_arcadegames=='y')?'CHECKED':''; ?>  <?php echo (old('amenity_arcadegames')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
    </th>
    <td>
    <label for="amenity_arcadegames">Arcade Games</label>
    </td>
    <th>
    <input type="checkbox" name="amenity_billiards" id="amenity_billiards" 
    <?php echo (isset($spListingDataId[0]->amenity_billiards) && $spListingDataId[0]->amenity_billiards=='y')?'CHECKED':''; ?> <?php echo (old('amenity_billiards')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
    </th>
    <td>
    <label for="amenity_billiards">Billiards</label>
    </td>
    <th>
    <input type="checkbox" name="amenity_blender" id="amenity_blender"
     <?php echo (isset($spListingDataId[0]->amenity_blender) && $spListingDataId[0]->amenity_blender=='y')?'CHECKED':''; ?> <?php echo (old('amenity_blender')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
    </th>
    <td>
    <label for="amenity_blender">Blender</label>
    </td>
    </tr>
    <tr>
    <th>
    <input type="checkbox" name="amenity_blurayplayer" id="amenity_blurayplayer"
     <?php echo (isset($spListingDataId[0]->amenity_blurayplayer) && $spListingDataId[0]->amenity_blurayplayer=='y')?'CHECKED':''; ?> <?php echo (old('amenity_blurayplayer')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
    </th>
    <td>
    <label for="amenity_blurayplayer">Blu Ray Player</label>
    </td>
    <th>
    <input type="checkbox" name="amenity_books" id="amenity_books" 
    <?php echo (isset($spListingDataId[0]->amenity_books) && $spListingDataId[0]->amenity_books=='y')?'CHECKED':''; ?> <?php echo (old('amenity_books')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
    </th>
    <td>
    <label for="amenity_books">Books</label>
    </td>
    <th><input type="checkbox" name="amenity_casetteplayer" id="amenity_casetteplayer" 
    <?php echo (isset($spListingDataId[0]->amenity_casetteplayer) && $spListingDataId[0]->amenity_casetteplayer=='y')?'CHECKED':''; ?> <?php echo (old('amenity_casetteplayer')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
    </th>
    <td><label for="amenity_casetteplayer">Cassette Player</label></td>
    </tr>
    <tr>
    <th>
    <input type="checkbox" name="amenity_cdplayer" id="amenity_cdplayer" 
    <?php echo (isset($spListingDataId[0]->amenity_cdplayer) && $spListingDataId[0]->amenity_cdplayer=='y')?'CHECKED':''; ?> <?php echo (old('amenity_cdplayer')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
    </th>
    <td>
    <label for="amenity_cdplayer">CD Player</label>
    </td>
    <th>
    <input type="checkbox" name="amenity_ceilingfan" id="amenity_ceilingfan"
     <?php echo (isset($spListingDataId[0]->amenity_ceilingfan) && $spListingDataId[0]->amenity_ceilingfan=='y')?'CHECKED':''; ?> <?php echo (old('amenity_ceilingfan')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
    </th>
    <td>
    <label for="amenity_ceilingfan">Ceiling Fan</label>
    </td>
    <th>
    <input type="checkbox" name="amenity_childshighchair" id="amenity_childshighchair" 
    <?php echo (isset($spListingDataId[0]->amenity_childshighchair) && $spListingDataId[0]->amenity_childshighchair=='y')?'CHECKED':''; ?> <?php echo (old('amenity_childshighchair')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
    </th>
    <td>
    <label for="amenity_childshighchair">Child's Highchair</label>
    </td>
    </tr>
    <tr>
    <th>
    <input type="checkbox" name="amenity_coffeemaker" id="amenity_coffeemaker"
    <?php echo (isset($spListingDataId[0]->amenity_coffeemaker) && $spListingDataId[0]->amenity_coffeemaker=='y')?'CHECKED':''; ?> <?php echo (old('amenity_coffeemaker')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
    </th>
    <td>
    <label for="amenity_coffeemaker">Coffee Maker</label>
    </td>
    <th>
    <input type="checkbox" name="amenity_communalpool" id="amenity_communalpool" 
    <?php echo (isset($spListingDataId[0]->amenity_communalpool) && $spListingDataId[0]->amenity_communalpool=='y')?'CHECKED':''; ?> <?php echo (old('amenity_communalpool')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
    </th>
    <td>
    <label for="amenity_communalpool">Communal Pool</label>
    </td>
    <th>
    <input type="checkbox" name="amenity_computer" id="amenity_computer" 
    <?php echo (isset($spListingDataId[0]->amenity_computer) && $spListingDataId[0]->amenity_computer=='y')?'CHECKED':''; ?> <?php echo (old('amenity_computer')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
    </th>
    <td>
    <label for="amenity_computer">Computer</label>
    </td>
    </tr>
    <tr>
    <th>
    <input type="checkbox" name="amenity_cookingrange" id="amenity_cookingrange"
     <?php echo (isset($spListingDataId[0]->amenity_cookingrange) && $spListingDataId[0]->amenity_cookingrange=='y')?'CHECKED':''; ?> <?php echo (old('amenity_cookingrange')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
    </th>
    <td>
    <label for="amenity_cookingrange">Cooking Range</label>
    </td>
    <th>
    <input type="checkbox" name="amenity_cookware" id="amenity_cookware"
     <?php echo (isset($spListingDataId[0]->amenity_cookware) && $spListingDataId[0]->amenity_cookware=='y')?'CHECKED':''; ?> <?php echo (old('amenity_cookware')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
    </th>
    <td>
    <label for="amenity_cookware">Cookware</label>
    </td><th>
    <input type="checkbox" name="amenity_deckfurniture" id="amenity_deckfurniture" 
    <?php echo (isset($spListingDataId[0]->amenity_deckfurniture) && $spListingDataId[0]->amenity_deckfurniture=='y')?'CHECKED':''; ?> <?php echo (old('amenity_deckfurniture')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
    </th>
    <td>
    <label for="amenity_deckfurniture">Deck Furniture</label>
    </td>
    </tr>
    <tr>
    <th>
    <input type="checkbox" name="amenity_dishes" id="amenity_dishes" 
    <?php echo (isset($spListingDataId[0]->amenity_dishes) && $spListingDataId[0]->amenity_dishes=='y')?'CHECKED':''; ?> <?php echo (old('amenity_dishes')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
    </th>
    <td>
    <label for="amenity_dishes">Dishes</label>
    </td>
    <th>
    <input type="checkbox" name="amenity_dishwasher" id="amenity_dishwasher"
     <?php echo (isset($spListingDataId[0]->amenity_dishwasher) && $spListingDataId[0]->amenity_dishwasher=='y')?'CHECKED':''; ?> <?php echo (old('amenity_dishwasher')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
    </th>
    <td>
    <label for="amenity_dishwasher">Dishwasher</label>
    </td>
    <th>
    <input type="checkbox" name="amenity_dvdplayer" id="amenity_dvdplayer" 
    <?php echo (isset($spListingDataId[0]->amenity_dvdplayer) && $spListingDataId[0]->amenity_dvdplayer=='y')?'CHECKED':''; ?> <?php echo (old('amenity_dvdplayer')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
    </th>
    <td>
    <label for="amenity_dvdplayer">DVD Player</label>
    </td>
    </tr>
    <tr>
    <th>
    <input type="checkbox" name="amenity_exercisefacilities" id="amenity_exercisefacilities"
     <?php echo (isset($spListingDataId[0]->amenity_exercisefacilities) && $spListingDataId[0]->amenity_exercisefacilities=='y')?'CHECKED':''; ?> <?php echo (old('amenity_exercisefacilities')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
    </th>
    <td>
    <label for="amenity_exercisefacilities">Exercise Facilities</label>
    </td>
    <th>
    <input type="checkbox" name="amenity_foosball" id="amenity_foosball" 
    <?php echo (isset($spListingDataId[0]->amenity_foosball) && $spListingDataId[0]->amenity_foosball=='y')?'CHECKED':''; ?> <?php echo (old('amenity_foosball')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
    </th>
    <td>
    <label for="amenity_foosball">Foosball</label>
    </td>
    <th>
    <input type="checkbox" name="amenity_games" id="amenity_games"
     <?php echo (isset($spListingDataId[0]->amenity_games) && $spListingDataId[0]->amenity_games=='y')?'CHECKED':''; ?> <?php echo (old('amenity_games')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
    </th>
    <td>
    <label for="amenity_games">Games</label>
    </td>
    </tr>
    <tr>
    <th>
    <input type="checkbox" name="amenity_gametable" id="amenity_gametable"
     <?php echo (isset($spListingDataId[0]->amenity_gametable) && $spListingDataId[0]->amenity_gametable=='y')?'CHECKED':''; ?> <?php echo (old('amenity_gametable')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
    </th>
    <td>
    <label for="amenity_gametable">Game Table</label>
    </td>
    <th>
    <input type="checkbox" name="amenity_grill" id="amenity_grill" 
    <?php echo (isset($spListingDataId[0]->amenity_grill) && $spListingDataId[0]->amenity_grill=='y')?'CHECKED':''; ?> <?php echo (old('amenity_grill')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
    </th>
    <td>
    <label for="amenity_grill">Grill</label>
    </td>
    <th>
    <input type="checkbox" name="amenity_hairdryer" id="amenity_hairdryer" 
    <?php echo (isset($spListingDataId[0]->amenity_hairdryer) && $spListingDataId[0]->amenity_hairdryer=='y')?'CHECKED':''; ?> <?php echo (old('amenity_hairdryer')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
    </th>
    <td>
    <label for="amenity_hairdryer">Hair Dryer</label>
    </td>
    </tr>
    <tr>
    <th>
    <input type="checkbox" name="amenity_icemaker" id="amenity_icemaker" 
    <?php echo (isset($spListingDataId[0]->amenity_icemaker) && $spListingDataId[0]->amenity_icemaker=='y')?'CHECKED':''; ?> <?php echo (old('amenity_icemaker')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
    </th>
    <td>
    <label for="amenity_icemaker">Ice Maker</label>
    </td>
    <th>
    <input type="checkbox" name="amenity_internet" id="amenity_internet" 
    <?php echo (isset($spListingDataId[0]->amenity_internet) && $spListingDataId[0]->amenity_internet=='y')?'CHECKED':''; ?> <?php echo (old('amenity_internet')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
    </th>
    <td>
    <label for="amenity_internet">Internet</label>
    </td>
    <th>
    <input type="checkbox" name="amenity_ironandboard" id="amenity_ironandboard"
     <?php echo (isset($spListingDataId[0]->amenity_ironandboard) && $spListingDataId[0]->amenity_ironandboard=='y')?'CHECKED':''; ?> <?php echo (old('amenity_ironandboard')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
    </th>
    <td>
    <label for="amenity_ironandboard">Iron and Board</label>
    </td>
    </tr>
    <tr>
    <th>
    <input type="checkbox" name="amenity_kidsgames" id="amenity_kidsgames"
     <?php echo (isset($spListingDataId[0]->amenity_kidsgames) && $spListingDataId[0]->amenity_kidsgames=='y')?'CHECKED':''; ?> <?php echo (old('amenity_kidsgames')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
    </th>
    <td>
    <label for="amenity_kidsgames">Kid's Games</label>
    </td>
    <th>
    <input type="checkbox" name="amenity_linensprovided" id="amenity_linensprovided" 
    <?php echo (isset($spListingDataId[0]->amenity_linensprovided) && $spListingDataId[0]->amenity_linensprovided=='y')?'CHECKED':''; ?> <?php echo (old('amenity_linensprovided')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
    </th>
    <td>
    <label for="amenity_linensprovided">Linens Provided</label>
    </td>
    <th>
    <input type="checkbox" name="amenity_lobsterpot" id="amenity_lobsterpot" 
    <?php echo (isset($spListingDataId[0]->amenity_lobsterpot) && $spListingDataId[0]->amenity_lobsterpot=='y')?'CHECKED':''; ?> <?php echo (old('amenity_lobsterpot')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
    </th>
    <td>
    <label for="amenity_lobsterpot">Lobster Pot</label>
    </td>
    </tr>
    <tr>
    <th>
    <input type="checkbox" name="amenity_microwave" id="amenity_microwave"
     <?php echo (isset($spListingDataId[0]->amenity_microwave) && $spListingDataId[0]->amenity_microwave=='y')?'CHECKED':''; ?> <?php echo (old('amenity_microwave')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
    </th>
    <td>
    <label for="amenity_microwave">Microwave</label>
    </td>
    <th>
    <input type="checkbox" name="amenity_minirefrigerator" id="amenity_minirefrigerator" 
    <?php echo (isset($spListingDataId[0]->amenity_minirefrigerator) && $spListingDataId[0]->amenity_minirefrigerator=='y')?'CHECKED':''; ?> <?php echo (old('amenity_minirefrigerator')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
    </th>
    <td>
    <label for="amenity_minirefrigerator">Mini Refrigerator</label>
    </td>
    <th>
    <input type="checkbox" name="amenity_mp3radiodock" id="amenity_mp3radiodock"
     <?php echo (isset($spListingDataId[0]->amenity_mp3radiodock) && $spListingDataId[0]->amenity_mp3radiodock=='y')?'CHECKED':''; ?> <?php echo (old('amenity_mp3radiodock')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
    </th>
    <td>
    <label for="amenity_mp3radiodock">MP3 Radio Dock</label>
    </td>
    </tr>
    <tr>
    <th>
    <input type="checkbox" name="amenity_outsideshower" id="amenity_outsideshower"
     <?php echo (isset($spListingDataId[0]->amenity_outsideshower) && $spListingDataId[0]->amenity_outsideshower=='y')?'CHECKED':''; ?> <?php echo (old('amenity_outsideshower')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
    </th>
    <td>
    <label for="amenity_outsideshower">Outside Shower</label>
    </td>
    <th>
    <input type="checkbox" name="amenity_oven" id="amenity_oven" 
    <?php echo (isset($spListingDataId[0]->amenity_oven) && $spListingDataId[0]->amenity_oven=='y')?'CHECKED':''; ?> <?php echo (old('amenity_oven')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
    </th>
    <td>
    <label for="amenity_oven">Oven</label>
    </td>
    <th>
    <input type="checkbox" name="amenity_pinball" id="amenity_pinball"
     <?php echo (isset($spListingDataId[0]->amenity_pinball) && $spListingDataId[0]->amenity_pinball=='y')?'CHECKED':''; ?> <?php echo (old('amenity_pinball')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
    </th>
    <td>
    <label for="amenity_pinball">Pinball</label>
    </td>
    </tr>
    <tr>
    <th>
    <input type="checkbox" name="amenity_pingpong" id="amenity_pingpong" 
    <?php echo (isset($spListingDataId[0]->amenity_pingpong) && $spListingDataId[0]->amenity_pingpong=='y')?'CHECKED':''; ?> <?php echo (old('amenity_pingpong')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
    </th>
    <td>
    <label for="amenity_pingpong">Ping Pong</label>
    </td>
    <th>
    <input type="checkbox" name="amenity_privatepool" id="amenity_privatepool"
     <?php echo (isset($spListingDataId[0]->amenity_privatepool) && $spListingDataId[0]->amenity_privatepool=='y')?'CHECKED':''; ?> <?php echo (old('amenity_privatepool')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
    </th>
    <td>
    <label for="amenity_privatepool">Private Pool</label>
    </td>
    <th>
    <input type="checkbox" name="amenity_radio" id="amenity_radio"
     <?php echo (isset($spListingDataId[0]->amenity_radio) && $spListingDataId[0]->amenity_radio=='y')?'CHECKED':''; ?> <?php echo (old('amenity_radio')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
    </th>
    <td>
    <label for="amenity_radio">Radio</label>
    </td>
    </tr>
    <tr>
    <th>
    <input type="checkbox" name="amenity_refrigeratorfreezer" id="amenity_refrigeratorfreezer" 
    <?php echo (isset($spListingDataId[0]->amenity_refrigeratorfreezer) && $spListingDataId[0]->amenity_refrigeratorfreezer=='y')?'CHECKED':''; ?> <?php echo (old('amenity_refrigeratorfreezer')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
    </th>
    <td>
    <label for="amenity_refrigeratorfreezer">Refrigerator/Freezer</label>
    </td>
    <th>
    <input type="checkbox" name="amenity_sofabed" id="amenity_sofabed"
     <?php echo (isset($spListingDataId[0]->amenity_sofabed) && $spListingDataId[0]->amenity_sofabed=='y')?'CHECKED':''; ?> <?php echo (old('amenity_sofabed')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
    </th>
    <td>
    <label for="amenity_sofabed">Sofa Bed</label>
    </td>
    <th>
    <input type="checkbox" name="amenity_stereo" id="amenity_stereo" 
    <?php echo (isset($spListingDataId[0]->amenity_stereo) && $spListingDataId[0]->amenity_stereo=='y')?'CHECKED':''; ?> <?php echo (old('amenity_stereo')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
    </th>
    <td>
    <label for="amenity_stereo">Stereo</label>
    </td>
    </tr>
    <tr>
    <th>
    <input type="checkbox" name="amenity_telephone" id="amenity_telephone"
     <?php echo (isset($spListingDataId[0]->amenity_telephone) && $spListingDataId[0]->amenity_telephone=='y')?'CHECKED':''; ?> <?php echo (old('amenity_telephone')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
    </th>
    <td>
    <label for="amenity_telephone">Telephone</label>
    </td>
    <th>
    <input type="checkbox" name="amenity_television" id="amenity_television"
     <?php echo (isset($spListingDataId[0]->amenity_television) && $spListingDataId[0]->amenity_television=='y')?'CHECKED':''; ?> <?php echo (old('amenity_television')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
    </th>
    <td>
    <label for="amenity_television">Television</label>
    </td>
    <th>
    <input type="checkbox" name="amenity_toaster" id="amenity_toaster"
     <?php echo (isset($spListingDataId[0]->amenity_toaster) && $spListingDataId[0]->amenity_toaster=='y')?'CHECKED':''; ?> <?php echo (old('amenity_toaster')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
    </th>
    <td>
    <label for="amenity_toaster">Toaster</label>
    </td>
    </tr>
    <tr>
    <th>
    <input type="checkbox" name="amenity_toasteroven" id="amenity_toasteroven"
     <?php echo (isset($spListingDataId[0]->amenity_toasteroven) && $spListingDataId[0]->amenity_toasteroven=='y')?'CHECKED':''; ?> <?php echo (old('amenity_toasteroven')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
    </th>
    <td>
    <label for="amenity_toasteroven">Toaster Oven</label>
    </td>
    <th>
    <input type="checkbox" name="amenity_towelsprovided" id="amenity_towelsprovided"
     <?php echo (isset($spListingDataId[0]->amenity_towelsprovided) && $spListingDataId[0]->amenity_towelsprovided=='y')?'CHECKED':''; ?> <?php echo (old('amenity_towelsprovided')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
    </th>
    <td>
    <label for="amenity_towelsprovided">Towels Provided</label>
    </td>
    <th>
    <input type="checkbox" name="amenity_toys" id="amenity_toys"
     <?php echo (isset($spListingDataId[0]->amenity_toys) && $spListingDataId[0]->amenity_toys=='y')?'CHECKED':''; ?> <?php echo (old('amenity_toys')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
    </th>
    <td>
    <label for="amenity_toys">Toys</label>
    </td>
    </tr>
    <tr>
    <th>
    <input type="checkbox" name="amenity_utensils" id="amenity_utensils"
     <?php echo (isset($spListingDataId[0]->amenity_utensils) && $spListingDataId[0]->amenity_utensils=='y')?'CHECKED':''; ?> <?php echo (old('amenity_utensils')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
    </th>
    <td>
    <label for="amenity_utensils">Utensils</label>
    </td>
    <th>
    <input type="checkbox" name="amenity_vacuum" id="amenity_vacuum" 
    <?php echo (isset($spListingDataId[0]->amenity_vacuum) && $spListingDataId[0]->amenity_vacuum=='y')?'CHECKED':''; ?> <?php echo (old('amenity_vacuum')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
    </th>
    <td>
    <label for="amenity_vacuum">Vacuum</label>
    </td>
    <th>
    <input type="checkbox" name="amenity_vcr" id="amenity_vcr" 
    <?php echo (isset($spListingDataId[0]->amenity_vcr) && $spListingDataId[0]->amenity_vcr=='y')?'CHECKED':''; ?> <?php echo (old('amenity_vcr')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
    </th>
    <td>
    <label for="amenity_vcr">VCR</label>
    </td>
    </tr>
    <tr>
    <th>
    <input type="checkbox" name="amenity_videogameconsole" id="amenity_videogameconsole"
     <?php echo (isset($spListingDataId[0]->amenity_videogameconsole) && $spListingDataId[0]->amenity_videogameconsole=='y')?'CHECKED':''; ?> <?php echo (old('amenity_videogameconsole')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
    </th>
    <td>
    <label for="amenity_videogameconsole">Video Game Console</label>
    </td>
    <th>
    <input type="checkbox" name="amenity_videogames" id="amenity_videogames"
     <?php echo (isset($spListingDataId[0]->amenity_videogames) && $spListingDataId[0]->amenity_videogames=='y')?'CHECKED':''; ?> <?php echo (old('amenity_videogames')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
    </th>
    <td>
    <label for="amenity_videogames">Video Games</label>
    </td>
    <th>
    <input type="checkbox" name="amenity_washerdryer" id="amenity_washerdryer" 
    <?php echo (isset($spListingDataId[0]->amenity_washerdryer) && $spListingDataId[0]->amenity_washerdryer=='y')?'CHECKED':''; ?> <?php echo (old('amenity_washerdryer')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
    </th>
    <td>
    <label for="amenity_washerdryer">Washer/Dryer</label>
    </td>
    </tr>
    <tr>
    <th><input type="checkbox" name="amenity_wifi" id="amenity_wifi"
     <?php echo (isset($spListingDataId[0]->amenity_wifi) && $spListingDataId[0]->amenity_wifi=='y')?'CHECKED':''; ?> <?php echo (old('amenity_wifi')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
    </th>
    <td>
    <label for="amenity_wifi">WiFi</label>
    </td>
    </tr>
    </tbody>
    </table> 
       <table class="standard-table features-tab-Main">
       <tbody>
       <tr>
       <th colspan="6" class="standard-tabletitle">Features</th>
       </tr>
       <tr>
       <th>
       <input type="checkbox" name="feature_airconditioning" id="feature_airconditioning" 
       <?php echo (isset($spListingDataId[0]->feature_airconditioning) && $spListingDataId[0]->feature_airconditioning=='y')?'CHECKED':''; ?> <?php echo (old('feature_airconditioning')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
       </th>
       <td>
       <label for="feature_airconditioning">Air Conditioning</label>
       </td>
       <th>
       <input type="checkbox" name="feature_balcony" id="feature_balcony"
        <?php echo (isset($spListingDataId[0]->feature_balcony) && $spListingDataId[0]->feature_balcony=='y')?'CHECKED':''; ?> <?php echo (old('feature_balcony')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
       </th>
       <td>
       <label for="feature_balcony">Balcony</label>
       </td>
       <th>
       <input type="checkbox" name="feature_barbecuecharcoal" id="feature_barbecuecharcoal"
        <?php echo (isset($spListingDataId[0]->feature_barbecuecharcoal) && $spListingDataId[0]->feature_barbecuecharcoal=='y')?'CHECKED':''; ?> <?php echo (old('feature_barbecuecharcoal')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
       </th>
       <td>
       <label for="feature_barbecuecharcoal">Barbecue Charcoal</label>
       </td>
       </tr>
       <tr>
       <th>
       <input type="checkbox" name="feature_barbecuegas" id="feature_barbecuegas"
        <?php echo (isset($spListingDataId[0]->feature_barbecuegas) && $spListingDataId[0]->feature_barbecuegas=='y')?'CHECKED':''; ?> <?php echo (old('feature_barbecuegas')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
       </th>
       <td>
       <label for="feature_barbecuegas">Barbecue Gas</label>
       </td>
       <th>
       <input type="checkbox" name="feature_boatslip" id="feature_boatslip"
        <?php echo (isset($spListingDataId[0]->feature_boatslip) && $spListingDataId[0]->feature_boatslip=='y')?'CHECKED':''; ?> <?php echo (old('feature_boatslip')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
       </th>
       <td>
       <label for="feature_boatslip">Boat Slip</label>
       </td>
       <th>
       <input type="checkbox" name="feature_cablesatellitetv" id="feature_cablesatellitetv"
        <?php echo (isset($spListingDataId[0]->feature_cablesatellitetv) && $spListingDataId[0]->feature_cablesatellitetv=='y')?'CHECKED':''; ?> <?php echo (old('feature_cablesatellitetv')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
       </th>
       <td>
       <label for="feature_cablesatellitetv">Cable/Satellite TV</label>
       </td>
       </tr>
       <tr>
       <th>
       <input type="checkbox" name="feature_clubhouse" id="feature_clubhouse"
        <?php echo (isset($spListingDataId[0]->feature_clubhouse) && $spListingDataId[0]->feature_clubhouse=='y')?'CHECKED':''; ?> <?php echo (old('feature_clubhouse')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
       </th>
       <td>
       <label for="feature_clubhouse">Clubhouse</label>
       </td>
       <th>
       <input type="checkbox" name="feature_coveredparking" id="feature_coveredparking" 
       <?php echo (isset($spListingDataId[0]->feature_coveredparking) && $spListingDataId[0]->feature_coveredparking=='y')?'CHECKED':''; ?> <?php echo (old('feature_coveredparking')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
       </th>
       <td>
       <label for="feature_coveredparking">Off Street Parking</label>
       </td>
       <th>
       <input type="checkbox" name="feature_deck" id="feature_deck" 
       <?php echo (isset($spListingDataId[0]->feature_deck) && $spListingDataId[0]->feature_deck=='y')?'CHECKED':''; ?> <?php echo (old('feature_deck')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
       </th>
       <td>
       <label for="feature_deck">Deck</label>
       </td>
       </tr>
       <tr>
       <th>
       <input type="checkbox" name="feature_diningroom" id="feature_diningroom" 
       <?php echo (isset($spListingDataId[0]->feature_diningroom) && $spListingDataId[0]->feature_diningroom=='y')?'CHECKED':''; ?> <?php echo (old('feature_diningroom')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
       </th>
       <td>
       <label for="feature_diningroom">Dining Room</label>
       </td>
       <th>
       <input type="checkbox" name="feature_elevator" id="feature_elevator" 
       <?php echo (isset($spListingDataId[0]->feature_elevator) && $spListingDataId[0]->feature_elevator=='y')?'CHECKED':''; ?> <?php echo (old('feature_elevator')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
       </th>
       <td>
       <label for="feature_elevator">Elevator</label>
       </td>
       <th>
       <input type="checkbox" name="feature_familyroom" id="feature_familyroom" 
       <?php echo (isset($spListingDataId[0]->feature_familyroom) && $spListingDataId[0]->feature_familyroom=='y')?'CHECKED':''; ?> <?php echo (old('feature_familyroom')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
       </th>
       <td>
       <label for="feature_familyroom">Family Room</label>
       </td>
       </tr>
       <tr>
       <th>
       <input type="checkbox" name="feature_fullkitchen" id="feature_fullkitchen" 
       <?php echo (isset($spListingDataId[0]->feature_fullkitchen) && $spListingDataId[0]->feature_fullkitchen=='y')?'CHECKED':''; ?> <?php echo (old('feature_fullkitchen')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
       </th>
       <td>
       <label for="feature_fullkitchen">Full Kitchen</label>
       </td>
       <th>
       <input type="checkbox" name="feature_garage" id="feature_garage" 
       <?php echo (isset($spListingDataId[0]->feature_garage) &&  $spListingDataId[0]->feature_garage=='y')?'CHECKED':''; ?> <?php echo (old('feature_garage')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
       </th>
       <td>
       <label for="feature_garage">Garage</label>
       </td>
       <th>
       <input type="checkbox" name="feature_gasfireplace" id="feature_gasfireplace" 
       <?php echo (isset($spListingDataId[0]->feature_gasfireplace) && $spListingDataId[0]->feature_gasfireplace=='y')?'CHECKED':''; ?> <?php echo (old('feature_gasfireplace')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
       </th>
       <td>
       <label for="feature_gasfireplace">Gas Fireplace</label>
       </td>
       </tr>
       <tr>
       <th>
       <input type="checkbox" name="feature_gatedcommunity" id="feature_gatedcommunity" 
       <?php echo (isset($spListingDataId[0]->feature_gatedcommunity) && $spListingDataId[0]->feature_gatedcommunity=='y')?'CHECKED':''; ?> <?php echo (old('feature_gatedcommunity')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
       </th>
       <td>
       <label for="feature_gatedcommunity">Gated Community</label>
       </td>
       <th>
       <input type="checkbox" name="feature_heated" id="feature_heated"
        <?php echo (isset($spListingDataId[0]->feature_heated) && $spListingDataId[0]->feature_heated=='y')?'CHECKED':''; ?> <?php echo (old('feature_heated')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
       </th>
       <td>
       <label for="feature_heated">Heated</label>
       </td>
       <th>
       <input type="checkbox" name="feature_heatedpool" id="feature_heatedpool" 
       <?php echo (isset($spListingDataId[0]->feature_heatedpool) && $spListingDataId[0]->feature_heatedpool=='y')?'CHECKED':''; ?> <?php echo (old('feature_heatedpool')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
       </th>
       <td>
       <label for="feature_heatedpool">Heated Pool</label>
       </td>
       </tr>
       <tr>
       <th>
       <input type="checkbox" name="feature_hottubjacuzzi" id="feature_hottubjacuzzi" 
       <?php echo (isset($spListingDataId[0]->feature_hottubjacuzzi) && $spListingDataId[0]->feature_hottubjacuzzi=='y')?'CHECKED':''; ?> <?php echo (old('feature_hottubjacuzzi')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
       </th>
       <td>
       <label for="feature_hottubjacuzzi">Hot Tub/Jacuzzi</label>
       </td>
       <th>
       <input type="checkbox" name="feature_internetaccess" id="feature_internetaccess" 
       <?php echo (isset($spListingDataId[0]->feature_internetaccess) && $spListingDataId[0]->feature_internetaccess=='y')?'CHECKED':''; ?> <?php echo (old('feature_internetaccess')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
       </th>
       <td>
       <label for="feature_internetaccess">Internet Access</label>
       </td>
       <th>
       <input type="checkbox" name="feature_kitchenette" id="feature_kitchenette"
        <?php echo (isset($spListingDataId[0]->feature_kitchenette) && $spListingDataId[0]->feature_kitchenette=='y')?'CHECKED':''; ?> <?php echo (old('feature_kitchenette')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
       </th>
       <td>
       <label for="feature_kitchenette">Kitchenette</label>
       </td>
       </tr>
       <tr>
       <th>
       <input type="checkbox" name="feature_livingroom" id="feature_livingroom"
        <?php echo (isset($spListingDataId[0]->feature_livingroom) && $spListingDataId[0]->feature_livingroom=='y')?'CHECKED':''; ?> <?php echo (old('feature_livingroom')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
       </th>
       <td>
       <label for="feature_livingroom">Living Room</label>
       </td>
       <th>
       <input type="checkbox" name="feature_loft" id="feature_loft" 
       <?php echo (isset($spListingDataId[0]->feature_loft) && $spListingDataId[0]->feature_loft=='y')?'CHECKED':''; ?> <?php echo (old('feature_loft')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
       </th>
       <td>
       <label for="feature_loft">Loft</label>
       </td>
       <th>
       <input type="checkbox" name="feature_onsitesecurity" id="feature_onsitesecurity"
        <?php echo (isset($spListingDataId[0]->feature_onsitesecurity) && $spListingDataId[0]->feature_onsitesecurity=='y')?'CHECKED':''; ?> <?php echo (old('feature_onsitesecurity')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
       </th>
       <td>
       <label for="feature_onsitesecurity">On-Site Security</label>
       </td>
       </tr>
       <tr>
       <th>
       <input type="checkbox" name="feature_patio" id="feature_patio" 
       <?php echo (isset($spListingDataId[0]->feature_patio) && $spListingDataId[0]->feature_patio=='y')?'CHECKED':''; ?> <?php echo (old('feature_patio')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
       </th>
       <td>
       <label for="feature_patio">Patio</label>
       </td>
       <th>
       <input type="checkbox" name="feature_petsallowed" id="feature_petsallowed" 
       <?php echo (isset($spListingDataId[0]->feature_petsallowed) && $spListingDataId[0]->feature_petsallowed=='y')?'CHECKED':''; ?> <?php echo (old('feature_petsallowed')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
       </th>
       <td>
       <label for="feature_petsallowed">Pets Allowed</label>
       </td>
       <th>
       <input type="checkbox" name="feature_playroom" id="feature_playroom"
        <?php echo (isset($spListingDataId[0]->feature_playroom) && $spListingDataId[0]->feature_playroom=='y')?'CHECKED':''; ?> <?php echo (old('feature_playroom')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
       </th>
       <td>
       <label for="feature_playroom">Playroom</label>
       </td>
       </tr>
       <tr>
       <th>
       <input type="checkbox" name="feature_pool" id="feature_pool" 
       <?php echo (isset($spListingDataId[0]->feature_pool) && $spListingDataId[0]->feature_pool=='y')?'CHECKED':''; ?> <?php echo (old('feature_pool')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
       </th>
       <td>
       <label for="feature_pool">Pool</label>
       </td>
       <th>
       <input type="checkbox" name="feature_porch" id="feature_porch" 
       <?php echo (isset($spListingDataId[0]->feature_porch) && $spListingDataId[0]->feature_porch=='y')?'CHECKED':''; ?> <?php echo (old('feature_porch')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
       </th>
       <td>
       <label for="feature_porch">Porch</label>
       </td>
       <th>
       <input type="checkbox" name="feature_rooftopdeck" id="feature_rooftopdeck"
        <?php echo (isset($spListingDataId[0]->feature_rooftopdeck) && $spListingDataId[0]->feature_rooftopdeck=='y')?'CHECKED':''; ?> <?php echo (old('feature_rooftopdeck')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
       </th>
       <td>
       <label for="feature_rooftopdeck">Rooftop Deck</label>
       </td>
       </tr>
       <tr>
       <th>
       <input type="checkbox" name="feature_sauna" id="feature_sauna" 
       <?php echo (isset($spListingDataId[0]->feature_sauna) && $spListingDataId[0]->feature_sauna=='y')?'CHECKED':''; ?> <?php echo (old('feature_sauna')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
       </th>
       <td>
       <label for="feature_sauna">Sauna</label>
       </td>
       <th>
       <input type="checkbox" name="feature_smokingpermitted" id="feature_smokingpermitted"
        <?php echo (isset($spListingDataId[0]->feature_smokingpermitted) && $spListingDataId[0]->feature_smokingpermitted=='y')?'CHECKED':''; ?> <?php echo (old('feature_smokingpermitted')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
       </th>
       <td>
       <label for="feature_smokingpermitted">Smoking Permitted</label>
       </td>
       <th>
       <input type="checkbox" name="feature_wheelchairaccess" id="feature_wheelchairaccess" 
       <?php echo (isset($spListingDataId[0]->feature_wheelchairaccess) && $spListingDataId[0]->feature_wheelchairaccess=='y')?'CHECKED':''; ?> <?php echo (old('feature_wheelchairaccess')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
       </th>
       <td>
       <label for="feature_wheelchairaccess">Wheelchair Access</label>
       </td>
       </tr>
       <tr>
       <th>
       <input type="checkbox" name="feature_woodfireplace" id="feature_woodfireplace"
        <?php echo (isset($spListingDataId[0]->feature_woodfireplace) && $spListingDataId[0]->feature_woodfireplace=='y')?'CHECKED':''; ?> <?php echo (old('feature_woodfireplace')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
       </th>
       <td>
       <label for="feature_woodfireplace">Wood Fireplace</label>
       </td>
       </tr>
       </tbody>
       </table> 
          <table class="standard-table activities-tab-Main">
          <tbody>
          <tr>
          <th colspan="6" class="standard-tabletitle">Activities</th>
          </tr>
          <tr>
          <th>
          <input type="checkbox" name="activity_antiquing" id="activity_antiquing"
           <?php echo (isset($spListingDataId[0]->activity_antiquing) && $spListingDataId[0]->activity_antiquing=='y')?'CHECKED':''; ?> <?php echo (old('activity_antiquing')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
          </th>
          <td>
          <label for="activity_antiquing">Antiquing</label>
          </td>
          <th>
          <input type="checkbox" name="activity_basketballcourt" id="activity_basketballcourt"
           <?php echo (isset($spListingDataId[0]->activity_basketballcourt) && $spListingDataId[0]->activity_basketballcourt=='y')?'CHECKED':''; ?> <?php echo (old('activity_basketballcourt')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
          </th>
          <td>
          <label for="activity_basketballcourt">Basketball Court</label>
          </td>
          <th>
          <input type="checkbox" name="activity_bayfishing" id="activity_bayfishing" 
          <?php echo (isset($spListingDataId[0]->activity_bayfishing) && $spListingDataId[0]->activity_bayfishing=='y')?'CHECKED':''; ?> <?php echo (old('activity_bayfishing')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
          </th>
          <td>
          <label for="activity_bayfishing">Bay Fishing</label>
          </td>
          </tr>
          <tr>
          <th>
          <input type="checkbox" name="activity_beachcombing" id="activity_beachcombing"
           <?php echo (isset($spListingDataId[0]->activity_beachcombing) && $spListingDataId[0]->activity_beachcombing=='y')?'CHECKED':''; ?> <?php echo (old('activity_beachcombing')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
          </th>
          <td>
          <label for="activity_beachcombing">Beachcombing</label>
          </td>
          <th>
          <input type="checkbox" name="activity_bicycling" id="activity_bicycling"
           <?php echo (isset($spListingDataId[0]->activity_bicycling) && $spListingDataId[0]->activity_bicycling=='y')?'CHECKED':''; ?> <?php echo (old('activity_bicycling')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
          </th>
          <td>
          <label for="activity_bicycling">Bicycling</label>
          </td>
          <th>
          <input type="checkbox" name="activity_bikerentals" id="activity_bikerentals" 
          <?php echo (isset($spListingDataId[0]->activity_bikerentals) && $spListingDataId[0]->activity_bikerentals=='y')?'CHECKED':''; ?> <?php echo (old('activity_bikerentals')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
          </th>
          <td>
          <label for="activity_bikerentals">Bike Rentals</label>
          </td>
          </tr>
          <tr>
          <th>
          <input type="checkbox" name="activity_bikesprovided" id="activity_bikesprovided"
           <?php echo (isset($spListingDataId[0]->activity_bikesprovided) && $spListingDataId[0]->activity_bikesprovided=='y')?'CHECKED':''; ?> <?php echo (old('activity_bikesprovided')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
          </th>
          <td>
          <label for="activity_bikesprovided">Bikes Provided</label>
          </td>
          <th>
          <input type="checkbox" name="activity_birdwatching" id="activity_birdwatching"
           <?php echo (isset($spListingDataId[0]->activity_birdwatching) && $spListingDataId[0]->activity_birdwatching=='y')?'CHECKED':''; ?> <?php echo (old('activity_birdwatching')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
          </th>
          <td>
          <label for="activity_birdwatching">Bird Watching</label>
          </td>
          <th>
          <input type="checkbox" name="activity_boating" id="activity_boating" 
          <?php echo (isset($spListingDataId[0]->activity_boating) && $spListingDataId[0]->activity_boating=='y')?'CHECKED':''; ?> <?php echo (old('activity_boating')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
          </th>
          <td>
          <label for="activity_boating">Boating</label>
          </td>
          </tr>
          <tr>
          <th>
          <input type="checkbox" name="activity_boatrentals" id="activity_boatrentals" 
          <?php echo (isset($spListingDataId[0]->activity_boatrentals) && $spListingDataId[0]->activity_boatrentals=='y')?'CHECKED':''; ?> <?php echo (old('activity_boatrentals')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
          </th>
          <td>
          <label for="activity_boatrentals">Boat Rentals</label>
          </td>
          <th>
          <input type="checkbox" name="activity_botanicalgarden" id="activity_botanicalgarden" 
          <?php echo (isset($spListingDataId[0]->activity_botanicalgarden) && $spListingDataId[0]->activity_botanicalgarden=='y')?'CHECKED':''; ?> <?php echo (old('activity_botanicalgarden')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
          </th>
          <td>
          <label for="activity_botanicalgarden">Botanical Garden</label>
          </td>
          <th>
          <input type="checkbox" name="activity_canoe" id="activity_canoe" 
          <?php echo (isset($spListingDataId[0]->activity_canoe) && $spListingDataId[0]->activity_canoe=='y')?'CHECKED':''; ?> <?php echo (old('activity_canoe')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
          </th>
          <td>
          <label for="activity_canoe">Canoe</label>
          </td>
          </tr>
          <tr>
          <th>
          <input type="checkbox" name="activity_churches" id="activity_churches" 
          <?php echo (isset($spListingDataId[0]->activity_churches) && $spListingDataId[0]->activity_churches=='y')?'CHECKED':''; ?> <?php echo (old('activity_churches')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
          </th>
          <td>
          <label for="activity_churches">Churches</label>
          </td>
          <th>
          <input type="checkbox" name="activity_cinemas" id="activity_cinemas" 
          <?php echo (isset($spListingDataId[0]->activity_cinemas) && $spListingDataId[0]->activity_cinemas=='y')?'CHECKED':''; ?> <?php echo (old('activity_cinemas')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
          </th>
          <td>
          <label for="activity_cinemas">Cinemas</label>
          </td>
          <th>
          <input type="checkbox" name="activity_deepseafishing" id="activity_deepseafishing"
           <?php echo (isset($spListingDataId[0]->activity_deepseafishing) && $spListingDataId[0]->activity_deepseafishing=='y')?'CHECKED':''; ?> <?php echo (old('activity_deepseafishing')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
          </th>
          <td>
          <label for="activity_deepseafishing">Deepsea Fishing</label>
          </td>
          </tr>
          <tr>
          <th>
          <input type="checkbox" name="activity_fishing" id="activity_fishing"
           <?php echo (isset($spListingDataId[0]->activity_fishing) && $spListingDataId[0]->activity_fishing=='y')?'CHECKED':''; ?> <?php echo (old('activity_fishing')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
          </th>
          <td>
          <label for="activity_fishing">Fishing</label>
          </td>
          <th>
          <input type="checkbox" name="activity_fitnesscenter" id="activity_fitnesscenter" 
          <?php echo (isset($spListingDataId[0]->activity_fitnesscenter) && $spListingDataId[0]->activity_fitnesscenter=='y')?'CHECKED':''; ?>  <?php echo (old('activity_fitnesscenter')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
          </th>
          <td>
          <label for="activity_fitnesscenter">Fitness Center</label>
          </td>
          <th>
          <input type="checkbox" name="activity_golf" id="activity_golf"
           <?php echo (isset($spListingDataId[0]->activity_golf) && $spListingDataId[0]->activity_golf=='y')?'CHECKED':''; ?> <?php echo (old('activity_golf')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
          </th>
          <td>
          <label for="activity_golf">Golf</label>
          </td>
          </tr>
          <tr>
          <th>
          <input type="checkbox" name="activity_healthbeautyspa" id="activity_healthbeautyspa" 
          <?php echo (isset($spListingDataId[0]->activity_healthbeautyspa) && $spListingDataId[0]->activity_healthbeautyspa=='y')?'CHECKED':''; ?> <?php echo (old('activity_healthbeautyspa')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
          </th>
          <td>
          <label for="activity_healthbeautyspa">Health/Beauty Spa</label>
          </td>
          <th>
          <input type="checkbox" name="activity_hiking" id="activity_hiking" 
          <?php echo (isset($spListingDataId[0]->activity_hiking) && $spListingDataId[0]->activity_hiking=='y')?'CHECKED':''; ?> <?php echo (old('activity_hiking')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
          </th>
          <td>
          <label for="activity_hiking">Hiking</label>
          </td>
          <th>
          <input type="checkbox" name="activity_horsebackriding" id="activity_horsebackriding"
           <?php echo (isset($spListingDataId[0]->activity_horsebackriding) && $spListingDataId[0]->activity_horsebackriding=='y')?'CHECKED':''; ?> <?php echo (old('activity_horsebackriding')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
          </th>
          <td>
          <label for="activity_horsebackriding">Horseback Riding</label>
          </td>
          </tr>
          <tr>
          <th>
          <input type="checkbox" name="activity_horseshoes" id="activity_horseshoes" 
          <?php echo (isset($spListingDataId[0]->activity_horseshoes) && $spListingDataId[0]->activity_horseshoes=='y')?'CHECKED':''; ?> <?php echo (old('activity_horseshoes')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
          </th>
          <td>
          <label for="activity_horseshoes">Horseshoes</label>
          </td>
          <th>
          <input type="checkbox" name="activity_hotairballooning" id="activity_hotairballooning" 
          <?php echo (isset($spListingDataId[0]->activity_hotairballooning) && $spListingDataId[0]->activity_hotairballooning=='y')?'CHECKED':''; ?> <?php echo (old('activity_hotairballooning')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
          </th>
          <td>
          <label for="activity_hotairballooning">Hot Air Ballooning</label>
          </td>
          <th>
          <input type="checkbox" name="activity_iceskating" id="activity_iceskating"
           <?php echo (isset($spListingDataId[0]->activity_iceskating) && $spListingDataId[0]->activity_iceskating=='y')?'CHECKED':''; ?> <?php echo (old('activity_iceskating')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
          </th>
          <td>
          <label for="activity_iceskating">Ice Skating</label>
          </td>
          </tr>
          <tr>
          <th>
          <input type="checkbox" name="activity_jetskiing" id="activity_jetskiing" 
          <?php echo (isset($spListingDataId[0]->activity_jetskiing) && $spListingDataId[0]->activity_jetskiing=='y')?'CHECKED':''; ?> <?php echo (old('activity_jetskiing')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
          </th>
          <td>
          <label for="activity_jetskiing">Jet Skiing</label>
          </td>
          <th>
          <input type="checkbox" name="activity_kayaking" id="activity_kayaking" 
          <?php echo (isset($spListingDataId[0]->activity_kayaking) && $spListingDataId[0]->activity_kayaking=='y')?'CHECKED':''; ?> <?php echo (old('activity_kayaking')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
          </th>
          <td>
          <label for="activity_kayaking">Kayaking</label>
          </td>
          <th>
          <input type="checkbox" name="activity_livetheater" id="activity_livetheater" 
          <?php echo (isset($spListingDataId[0]->activity_livetheater) && $spListingDataId[0]->activity_livetheater=='y')?'CHECKED':''; ?> <?php echo (old('activity_livetheater')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
          </th>
          <td>
          <label for="activity_livetheater">Live Theater</label>
          </td>
          </tr>
          <tr>
          <th>
          <input type="checkbox" name="activity_marina" id="activity_marina" 
          <?php echo (isset($spListingDataId[0]->activity_marina) && $spListingDataId[0]->activity_marina=='y')?'CHECKED':''; ?> <?php echo (old('activity_marina')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
          </th>
          <td>
          <label for="activity_marina">Marina</label>
          </td>
          <th>
          <input type="checkbox" name="activity_miniaturegolf" id="activity_miniaturegolf" 
          <?php echo (isset($spListingDataId[0]->activity_miniaturegolf) && $spListingDataId[0]->activity_miniaturegolf=='y')?'CHECKED':''; ?> <?php echo (old('activity_miniaturegolf')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
          </th>
          <td>
          <label for="activity_miniaturegolf">Miniature Golf</label>
          </td>
          <th>
          <input type="checkbox" name="activity_mountainbiking" id="activity_mountainbiking"
           <?php echo (isset($spListingDataId[0]->activity_mountainbiking) && $spListingDataId[0]->activity_mountainbiking=='y')?'CHECKED':''; ?> <?php echo (old('activity_mountainbiking')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
          </th>
          <td>
          <label for="activity_mountainbiking">Mountain Biking</label>
          </td>
          </tr>
          <tr>
          <th>
          <input type="checkbox" name="activity_moviecinemas" id="activity_moviecinemas"
           <?php echo (isset($spListingDataId[0]->activity_moviecinemas) && $spListingDataId[0]->activity_moviecinemas=='y')?'CHECKED':''; ?> <?php echo (old('activity_moviecinemas')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
          </th>
          <td>
          <label for="activity_moviecinemas">Movie Cinemas</label>
          </td>
          <th>
          <input type="checkbox" name="activity_museums" id="activity_museums"
           <?php echo (isset($spListingDataId[0]->activity_museums) && $spListingDataId[0]->activity_museums=='y')?'CHECKED':''; ?> <?php echo (old('activity_museums')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
          </th>
          <td>
          <label for="activity_museums">Museums</label>
          </td>
          <th>
          <input type="checkbox" name="activity_paddleboating" id="activity_paddleboating" 
          <?php echo (isset($spListingDataId[0]->activity_paddleboating) &&  $spListingDataId[0]->activity_paddleboating=='y')?'CHECKED':''; ?> <?php echo (old('activity_paddleboating')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
          </th>
          <td>
          <label for="activity_paddleboating">Paddle Boating</label>
          </td>
          </tr>
          <tr>
          <th>
          <input type="checkbox" name="activity_paragliding" id="activity_paragliding" 
          <?php echo (isset($spListingDataId[0]->activity_paragliding) && $spListingDataId[0]->activity_paragliding=='y')?'CHECKED':''; ?> <?php echo (old('activity_paragliding')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
          </th>
          <td>
          <label for="activity_paragliding">Paragliding</label>
          </td>
          <th>
          <input type="checkbox" name="activity_parasailing" id="activity_parasailing" 
          <?php echo (isset($spListingDataId[0]->activity_parasailing) && $spListingDataId[0]->activity_parasailing=='y')?'CHECKED':''; ?> <?php echo (old('activity_parasailing')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
          </th>
          <td>
          <label for="activity_parasailing">Parasailing</label>
          </td>
          <th>
          <input type="checkbox" name="activity_playground" id="activity_playground"
           <?php echo (isset($spListingDataId[0]->activity_playground) && $spListingDataId[0]->activity_playground=='y')?'CHECKED':''; ?> <?php echo (old('activity_playground')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
          </th>
          <td>
          <label for="activity_playground">Playground</label>
          </td>
          </tr>
          <tr>
          <th>
          <input type="checkbox" name="activity_recreationcenter" id="activity_recreationcenter"
           <?php echo (isset($spListingDataId[0]->activity_recreationcenter) && $spListingDataId[0]->activity_recreationcenter=='y')?'CHECKED':''; ?> <?php echo (old('activity_recreationcenter')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
          </th>
          <td>
          <label for="activity_recreationcenter">Recreation Center</label>
          </td>
          <th>
          <input type="checkbox" name="activity_restaurants" id="activity_restaurants" 
          <?php echo (isset($spListingDataId[0]->activity_restaurants) && $spListingDataId[0]->activity_restaurants=='y')?'CHECKED':''; ?> <?php echo (old('activity_restaurants')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
          </th>
          <td>
          <label for="activity_restaurants">Restaurants</label>
          </td>
          <th>
          <input type="checkbox" name="activity_rollerblading" id="activity_rollerblading" 
          <?php echo (isset($spListingDataId[0]->activity_rollerblading) && $spListingDataId[0]->activity_rollerblading=='y')?'CHECKED':''; ?> <?php echo (old('activity_rollerblading')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
          </th>
          <td>
          <label for="activity_rollerblading">Roller Blading</label>
          </td>
          </tr>
          <tr>
          <th>
          <input type="checkbox" name="activity_sailing" id="activity_sailing" 
          <?php echo (isset($spListingDataId[0]->activity_sailing) && $spListingDataId[0]->activity_sailing=='y')?'CHECKED':''; ?> <?php echo (old('activity_sailing')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
          </th>
          <td>
          <label for="activity_sailing">Sailing</label>
          </td>
          <th>
          <input type="checkbox" name="activity_shelling" id="activity_shelling" 
          <?php echo (isset($spListingDataId[0]->activity_shelling) && $spListingDataId[0]->activity_shelling=='y')?'CHECKED':''; ?> <?php echo (old('activity_shelling')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
          </th>
          <td>
          <label for="activity_shelling">Shelling</label>
          </td>
          <th>
          <input type="checkbox" name="activity_shopping" id="activity_shopping" 
          <?php echo (isset($spListingDataId[0]->activity_shopping) && $spListingDataId[0]->activity_shopping=='y')?'CHECKED':''; ?> <?php echo (old('activity_shopping')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
          </th>
          <td>
          <label for="activity_shopping">Shopping</label>
          </td>
          </tr>
          <tr>
          <th>
          <input type="checkbox" name="activity_sightseeing" id="activity_sightseeing" 
          <?php echo (isset($spListingDataId[0]->activity_sightseeing) && $spListingDataId[0]->activity_sightseeing=='y')?'CHECKED':''; ?> <?php echo (old('activity_sightseeing')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
          </th>
          <td>
          <label for="activity_sightseeing">Sightseeing</label>
          </td>
          <th>
          <input type="checkbox" name="activity_skiing" id="activity_skiing" 
          <?php echo (isset($spListingDataId[0]->activity_skiing) && $spListingDataId[0]->activity_skiing=='y')?'CHECKED':''; ?> <?php echo (old('activity_skiing')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
          </th>
          <td>
          <label for="activity_skiing">Skiing</label>
          </td>
          <th>
          <input type="checkbox" name="activity_spa" id="activity_spa" 
          <?php echo (isset($spListingDataId[0]->activity_spa) && $spListingDataId[0]->activity_spa=='y')?'CHECKED':''; ?> <?php echo (old('activity_spa')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
          </th>
          <td>
          <label for="activity_spa">Spa</label>
          </td>
          </tr>
          <tr>
          <th>
          <input type="checkbox" name="activity_surffishing" id="activity_surffishing" 
          <?php echo (isset($spListingDataId[0]->activity_surffishing) && $spListingDataId[0]->activity_surffishing=='y')?'CHECKED':''; ?> <?php echo (old('activity_surffishing')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
          </th>
          <td>
          <label for="activity_surffishing">Surf Fishing</label>
          </td>
          <th>
          <input type="checkbox" name="activity_surfing" id="activity_surfing" 
          <?php echo (isset($spListingDataId[0]->activity_surfing) && $spListingDataId[0]->activity_surfing=='y')?'CHECKED':''; ?> <?php echo (old('activity_surfing')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
          </th>
          <td>
          <label for="activity_surfing">Surfing</label>
          </td>
          <th>
          <input type="checkbox" name="activity_swimming" id="activity_swimming" 
          <?php echo (isset($spListingDataId[0]->activity_swimming) && $spListingDataId[0]->activity_swimming=='y')?'CHECKED':''; ?> <?php echo (old('activity_swimming')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
          </th>
          <td>
          <label for="activity_swimming">Swimming</label>
          </td>
          </tr>
          <tr>
          <th>
          <input type="checkbox" name="activity_tennis" id="activity_tennis" 
          <?php echo (isset($spListingDataId[0]->activity_tennis) && $spListingDataId[0]->activity_tennis=='y')?'CHECKED':''; ?> <?php echo (old('activity_tennis')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
          </th>
          <td>
          <label for="activity_tennis">Tennis</label>
          </td>
          <th>
          <input type="checkbox" name="activity_themeparks" id="activity_themeparks" 
          <?php echo (isset($spListingDataId[0]->activity_themeparks) && $spListingDataId[0]->activity_themeparks=='y')?'CHECKED':''; ?> <?php echo (old('activity_themeparks')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
          </th>
          <td>
          <label for="activity_themeparks">Theme Parks</label>
          </td>
          <th>
          <input type="checkbox" name="activity_walking" id="activity_walking"
           <?php echo (isset($spListingDataId[0]->activity_walking) && $spListingDataId[0]->activity_walking=='y')?'CHECKED':''; ?> <?php echo (old('activity_walking')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
          </th>
          <td>
          <label for="activity_walking">Walking</label>
          </td>
          </tr>
          <tr>
          <th>
          <input type="checkbox" name="activity_waterparks" id="activity_waterparks"
           <?php echo (isset($spListingDataId[0]->activity_waterparks) && $spListingDataId[0]->activity_waterparks=='y')?'CHECKED':''; ?> <?php echo (old('activity_waterparks')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
          </th>
          <td>
          <label for="activity_waterparks">Water Parks</label>
          </td>
          <th>
          <input type="checkbox" name="activity_waterskiing" id="activity_waterskiing" 
          <?php echo (isset($spListingDataId[0]->activity_waterskiing) && $spListingDataId[0]->activity_waterskiing=='y')?'CHECKED':''; ?> <?php echo (old('activity_waterskiing')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
          </th>
          <td>
          <label for="activity_waterskiing">Water Skiing</label>
          </td>
          <th>
          <input type="checkbox" name="activity_watertubing" id="activity_watertubing" 
          <?php echo (isset($spListingDataId[0]->activity_watertubing) && $spListingDataId[0]->activity_watertubing=='y')?'CHECKED':''; ?> <?php echo (old('activity_watertubing')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
          </th>
          <td>
          <label for="activity_watertubing">Water Tubing</label>
          </td>
          </tr>
          <tr>
          <th>
          <input type="checkbox" name="activity_wildlifeviewing" id="activity_wildlifeviewing"
           <?php echo (isset($spListingDataId[0]->activity_wildlifeviewing) && $spListingDataId[0]->activity_wildlifeviewing=='y')?'CHECKED':''; ?> <?php echo (old('activity_wildlifeviewing')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
          </th>
          <td>
          <label for="activity_wildlifeviewing">Wildlife Viewing</label>
          </td>
          <th>
          <input type="checkbox" name="activity_zoo" id="activity_zoo"
           <?php echo (isset($spListingDataId[0]->activity_zoo) && $spListingDataId[0]->activity_zoo=='y')?'CHECKED':''; ?> <?php echo (old('activity_zoo')=='y')?'CHECKED':''; ?> value="y" class="inputCheck">
          </th>
          <td>
          <label for="activity_zoo">Zoo</label>
          </td>
          </tr>
          </tbody>
          </table>
    
<table class="standard-table">
    <tbody><tr>
        <th colspan="2" class="standard-tabletitle">
            Categories and sub-categories                    </th>
    </tr>
                <input type="hidden" name="return_categories" value="">
        <tr>
            <td class="treeView">
           <ul id="listing_categorytree_id_0" class="categoryTreeview">
                                    <li>&nbsp;</li>
             </ul>

                <table width="100%" border="0" cellpadding="2" class="tableCategoriesADDED" cellspacing="0">
                    <tbody><tr>
                        <th colspan="2" class="tableCategoriesTITLE alignLeft"><i> * <span>Required field</span></i> <strong>Listing Categories:</strong></th>
                    </tr>
                    <tr id="msgback2" class="informationMessageShort">
                        <td colspan="2" style="width: auto;"><p><img width="16" height="14" src="https://www.shoresummerrentals.com/images/icon_atention.gif" alt="Attention" title="Attention"> Click on "Add" to select categories</p></td>
                    </tr>
                    <tr id="msgback" class="informationMessageShort" style="display:none">
                        <td colspan="2"><i> * <span>Required field</span></i> <div><img width="16" height="14" src="https://www.shoresummerrentals.com/images/icon_atention.gif" alt="Attention" title="Attention"></div><p>Click at "Add main category" or "Add sub-category" to type your new categories</p></td>
                    </tr>
                    <tr>
       <td colspan="2" class="tableCategoriesCONTENT"><?php echo $feedDropDown; ?></td>
                    </tr>
                    <tr id="removeCategoriesButton" style="display:none;">
                        <td class="tableCategoriesBUTTONS" colspan="2">
                            <center>
                                <button class="input-button-form" type="button" value="Remove Selected Category" onclick="JS_removeCategory(document.listing.feed, false);">Remove Selected Category</button>
                            </center>
                        </td>
                    </tr>
                </tbody></table>
            </td>
        </tr>
    </tbody></table>

                        <table class="standard-table">
                <tbody><tr>
                    <th colspan="2" class="standard-tabletitle">Promotional Code</th>
                </tr>
                <tr>
                    <th>Code:</th>
 <td><input type="text" name="discount_id" value="<?php echo isset($spListingDataId[0]->discount_id)?$spListingDataId[0]->discount_id:$discount_id; ?><?php echo old('discount_id')?old('discount_id'):''; ?>" maxlength="10"></td>
                </tr>
            </tbody>
            </table>
            
  <script language="javascript" type="text/javascript">
    function loadCategoryTree(action, prefix, category, category_id, template_id, path, domain_id) {

    var ajax_categories = 0;
    
    if (document.getElementById("feed")) {
        feed = document.getElementById("feed");
    } else if (document.getElementById("feed_listing") && prefix == "listing_") {
        feed = document.getElementById("feed_listing");
    } else if (document.getElementById("feed_event") && prefix == "event_") {
        feed = document.getElementById("feed_event");
    } else if (document.getElementById("feed_classified") && prefix == "classified_") {
        feed = document.getElementById("feed_classified");
    } else if (document.getElementById("feed_article") && prefix == "article_") {
        feed = document.getElementById("feed_article");
    }

    if (feed) {
        
        var categories_aux = new Array();
        
        if (feed.length > 0) {
            for(i =0 ; i < feed.length ; i++){
              categories_aux[i] = feed.options[i].value;
            }
            var categories = categories_aux.join(",");
            ajax_categories = categories;
        }
    }

    var xmlhttp;
    try {
        xmlhttp = new XMLHttpRequest();
    } catch (e) {
        try {
            xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
        } catch (e) {
            try {
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            } catch (e) {
                xmlhttp = false;
            }
        }
    }
    document.getElementById(prefix+"categorytree_id_"+category_id).style.display = "";
    document.getElementById(prefix+"categorytree_id_"+category_id).innerHTML = "<li class='loading'>Wait, Loading...</li>";
    if (xmlhttp) {
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == 4) {
                if (xmlhttp.status == 200) {
                    if (category_id > 0) document.getElementById(prefix+"opencategorytree_id_"+category_id).style.display = "none";
                    if (category_id > 0) document.getElementById(prefix+"opencategorytree_title_id_"+category_id).style.display = "none";
                    if (category_id > 0) document.getElementById(prefix+"closecategorytree_id_"+category_id).style.display = "";
                    if (category_id > 0) document.getElementById(prefix+"closecategorytree_title_id_"+category_id).style.display = "";
                    document.getElementById(prefix+"categorytree_id_"+category_id).innerHTML = xmlhttp.responseText;
                }
            }
        }
        
        xmlhttp.open("GET", "<?php echo url('loadcategorytree'); ?>?action=" + action + "&prefix=" + prefix + "&category=" + category + "&category_id=" + category_id + "&template_id=" + template_id + "&ajax_categories=" + ajax_categories + "&path=" + path, true);
        xmlhttp.send(null);
    }
}

function closeCategoryTree(prefix, category, category_id, path) {
    if (category_id > 0) document.getElementById(prefix+"closecategorytree_id_"+category_id).style.display = "none";
    if (category_id > 0) document.getElementById(prefix+"closecategorytree_title_id_"+category_id).style.display = "none";
    if (category_id > 0) document.getElementById(prefix+"opencategorytree_id_"+category_id).style.display = "";
    if (category_id > 0) document.getElementById(prefix+"opencategorytree_title_id_"+category_id).style.display = "";
    document.getElementById(prefix+"categorytree_id_"+category_id).innerHTML = "";
    document.getElementById(prefix+"categorytree_id_"+category_id).style.display = "none";
}
function JS_removeCategory(feed, order) {
    if (feed.selectedIndex >= 0) {
        $('#categoryAdd'+feed.options[feed.selectedIndex].value).after($('.categorySuccessMessage').empty());
        $('#categoryAdd'+feed.options[feed.selectedIndex].value ).fadeIn(500);
        feed.remove(feed.selectedIndex);
        $('#removeCategoriesButton').hide();
        if (order){
            orderCalculate();
        }
    }
    
    if(feed.length == 0){
        $('#removeCategoriesButton').hide();
    }

}

</script>

    <script language="javascript" type="text/javascript">
    loadCategoryTree('all', 'listing_', 'ListingCategory', 0, 0, '<?php echo url(); ?>',1);
            </script>

<script language="javascript" type="text/javascript">
    $(document).ready(function() {

        // load location 
        location3Val = $('#location_3').val();
        loadLocationSitemgrMembers('<?php echo url(); ?>', '1,3,4', 3, 4,location3Val,'<?php echo (isset($spListingDataId[0]->location_4) && $spListingDataId[0]->location_4>0)?$spListingDataId[0]->location_4:0; ?><?php echo (old("location_4"))?old("location_4"):''; ?>');

        loadGallery('<?php echo isset($spListingDataId[0]->id)?$spListingDataId[0]->id:0; ?>', 'y', 'sponsors', "editFe", '');

                loadMap(document.listing, true);
        
        function checkIfNumber(e){

        }

        $(".number_field").keydown(function (e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                 // Allow: Ctrl+A
                (e.keyCode == 65 && e.ctrlKey === true) ||
                 // Allow: home, end, left, right
                (e.keyCode >= 35 && e.keyCode <= 39)) {
                     // let it happen, don't do anything
                     return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });

    });
</script>

<script type="text/javascript" language="javascript">
    $(document).ready(function() {
        if ($("#isppilisting").attr("checked")) {
            $("#deposit_amount").show();
            $("#tax_per_inquiry").show();
            $("#unique_email").show();
        }
        $('#isppilisting').click(function(){
            if ($("#isppilisting").attr("checked")) {
                $("#deposit_amount").show();
                $("#tax_per_inquiry").show();
                $("#unique_email").show();
            }
            else {
                $("#deposit_amount").hide();
                $("#tax_per_inquiry").hide();
                $("#unique_email").hide();
            }
        })
    })
    
    
    
    
    function loadLocationSitemgrMembers(url, edir_locations, level, childLevel, id,loc4Id) {

	var edir_locations = edir_locations.split(',');
	
	if (!isNaN(id)) {		

		for (i=0; i<edir_locations.length; i++) {
			if (edir_locations[i]>level) {
				text = $("#l_location_"+edir_locations[i]).attr("text");
				$("#location_"+edir_locations[i]).html("<option id=\"l_location_"+edir_locations[i]+"\" value=\"\">"+text+"</option>");
				$('#div_location_'+edir_locations[i]).css('display', 'none');
				$('#new_location'+edir_locations[i]+'_field').attr('value', '');
				$('#div_new_location'+edir_locations[i]+'_field').css('display', 'none');				
			}
		}	

		$("#div_location_"+childLevel).css("display","");
		$('#location_'+childLevel).css('display', 'none');
		$('#div_img_loading_'+childLevel).css('display', '');
		$('#box_no_location_found_'+childLevel).css('display', 'none');
                try{
                    $('#div_select_'+childLevel).css('display', 'none');
                } catch(e){}

		$.get("<?php echo url('sponsors/locations'); ?>",{id: id, level: level, childLevel: childLevel, type:'byId',loc4Id:loc4Id},function(location){
			
			if (location!="empty"){
				var text = $("#l_location_"+childLevel).attr("text");			
				$("#location_"+childLevel).html(location);
				$("#l_location_"+childLevel).html(text);
				$('#location_'+childLevel).css('display', '');
                try{
                    $('#div_select_'+childLevel).css('display', '');
                } catch(e){}
				display_level_limit = childLevel;
			} else {
				if (!id) 
					$("#div_location_"+childLevel).css("display", 'none');
				else {
                    try{
                        $('#div_select_'+childLevel).css('display', '');
                    } catch(e){}
					$('#box_no_location_found_'+childLevel).css('display', '');
                }
			}
				
			if (childLevel && id)
				$('#div_new_location'+childLevel+'_link').css('display', '');
			else
				$('#div_new_location'+childLevel+'_link').css('display', 'none');			

			$('#div_img_loading_'+childLevel).css('display', 'none');	
			
		});
	}	
	//containerReload();
}

function easyFriendlyUrl(strToReplace, target, validchars, separator) {
    var str = "";
    var i;
    var str_accent = '';
    var str_no_accent = '';
    var str_new = "";
    
    for (i = 0; i < strToReplace.length; i++) {
        if (str_accent.indexOf(strToReplace.charAt(i)) != -1) {
            str_new += str_no_accent.substr(str_accent.search(strToReplace.substr(i,1)),1);
        } else {
            str_new += strToReplace.substr(i,1);
        }
    }

    var exp_reg = new RegExp("[" + validchars + separator + "]");
    var exp_reg_space = new RegExp("[ ]");
    name2friendlyurl = str_new;
    name2friendlyurl.toString(); 
    name2friendlyurl = name2friendlyurl.replace(/^ +/, "");
    
    for (i = 0 ; i < name2friendlyurl.length; i++) {
        if (exp_reg.test(name2friendlyurl.charAt(i))) { 
            str = str+name2friendlyurl.charAt(i);
        } else {
            if (exp_reg_space.test(name2friendlyurl.charAt(i))) {
                if (str.charAt(str.length-1) != separator) { 
                    str = str + separator;
                }
            }
        }
    }

    if (str.charAt(str.length-1) == separator) str = str.substr(0, str.length-1);
    if (document.getElementById(target))
    document.getElementById(target).value = str.toLowerCase();
}

function showText(text) {
    return unescape(text);
}
function displayMap() {
    var index, totalamount=10;
    var classname;
    if ($.cookie('showMap') == 1) {
        $('#resultsMap').css('display', '');
        $('#linkDisplayMap').text('' + showText(LANG_JS_LABEL_HIDEMAP) + '');
        for (index=1; index<=totalamount; index++) {
            if (document.getElementById('summaryNumberID'+index)) {
                classname=document.getElementById('summaryNumberID'+index).className;
                if (classname=='summaryNumberSC isHidden')
                    document.getElementById('summaryNumberID'+index).className = 'summaryNumberSC show-inline';
                else
                    document.getElementById('summaryNumberID'+index).className = 'summaryNumber show-inline';

            }
        }
        $.cookie('showMap', '0', {expires: 7, path: '/'});
        initialize();
    } else {
        $('#resultsMap').css('display', 'none');
        $('#linkDisplayMap').text('' + showText(LANG_JS_LABEL_SHOWMAP) + '');
        for (index=1; index<=totalamount; index++) {
            if (document.getElementById('summaryNumberID'+index)) {
                classname=document.getElementById('summaryNumberID'+index).className;
                if (classname=='summaryNumber isVisible')
                    document.getElementById('summaryNumberID'+index).className = 'summaryNumber hide';
                else
                    document.getElementById('summaryNumberID'+index).className = 'summaryNumberSC hide';
            }
        }
        $.cookie('showMap', '1', {expires: 7, path: '/'});
    }
}

function showNewLocationField(level, edir_locations, back, text) {

	var edir_locations = edir_locations.split(',');

	for (i=0; i<edir_locations.length; i++) {
		if (edir_locations[i]>=level) {
		  
			$('#location_'+edir_locations[i]).attr('selected',true);
			$('#div_location_'+edir_locations[i]).css('display', 'none');
			$('#new_location'+edir_locations[i]+'_field').val("");
			$('#div_new_location'+edir_locations[i]+'_field').css('display', 'none');
		}
	}		
	$('#div_new_location'+level+'_field').css('display', '');	
	$('#div_new_location'+level+'_link').css('display', 'none');
	if (!back)
		$('#div_new_location'+level+'_back').css('display', 'none');
	else
		$('#div_new_location'+level+'_back').css('display', '');

	if (text) {
		$('#new_location'+level+'_field').val(text);
	}

}

  
function hideNewLocationField(level, edir_locations) {

    var edir_locations = edir_locations.split(',');

    for (i=0; i<edir_locations.length; i++) {
        if (edir_locations[i]>=level) {
            
            $('#new_location'+edir_locations[i]+'_field').val("");
            $('#div_new_location'+edir_locations[i]+'_field').css('display', 'none');
            $('#location_'+edir_locations[i]+'option[value=""]').attr('selected',true);

        }
    }   
    $('#div_location_'+level).css('display', '');
    $('#div_new_location'+level+'_link').css('display', '');
    if (!$("#location_"+level).is(":visible")) {
        $('#box_no_location_found_'+level).css('display', '');
    }
}  



</script>
            
            <input type="hidden" name="ieBugFix2" value="1">

            
        </form>
        <br>
        <form action="<?php echo url('sponsors'); ?>" >

            <div class="baseButtons">

                <p class="standardButton">
                    <button type="button" onclick="JS_submit()">Submit</button>
                </p>
                <p class="standardButton">
                    <button type="submit" value="Cancel">Cancel</button>
                </p>

            </div>

        </form>

    </div>


            </div><!-- Close container-fluid div -->
            
        </div>
@endsection