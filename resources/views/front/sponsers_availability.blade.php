
@extends('front')

@section('content')
<?php 

use App\Functions\Functions;
use App\Models\Domain\Sql;
?>

	<link rel="stylesheet" href="{{ asset('front/css/bootstrap.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('front/css/members.min.css') }}" />
   <link rel="stylesheet" href="{{ asset('front/css/structure.min.css') }}" />

	<link href="https://www.shoresummerrentals.com/custom/domain_1/theme/default/content_custom.min.css" rel="stylesheet" type="text/css" media="all">
    <!--link rel="stylesheet" href="{{ asset('front/css/bootstrap-responsive.min.css') }}" /-->
 <style type="text/css">
     .calendar-members .calendar-table .listing_booked_booked, .calendar-members .calendar-table .listing_booked_booked_1 {
    background: #fc6a31 !important;
    color: #000;
}

.calendar-members .calendar-table .listing_booked_available_am ,.calendar-members .calendar-table .listing_booked_available_am_1 {
    background: url(https://www.shoresummerrentals.com/custom/domain_1/images/design/bg-listing_booked_available_am.png) no-repeat #fc6a31 !important;
    color: #000;
}


.calendar-members .calendar-table .listing_booked_available_pm, .calendar-members .calendar-table .listing_booked_available_pm_1 {
    background: url(https://www.shoresummerrentals.com/custom/domain_1/images/design/bg-listing_booked_available_pm_2.png) no-repeat #AAC7F3 !important;
    color: #000;
}

 </style>    
     
    
    
	 <div class="availability-page-container container members ">

        <div class="container-fluid">
			<style type="text/css">
			</style>
			<div >

												
				<h2>Listing - testing shore</h2>

				<ul class="list-view">
					<li class="list-back"><a href="<?php echo url('sponsors'); ?>">Back</a></li>
				</ul>

				
                    <div class="baseForm">    
                        

@if (session('message'))
  <p class='successMessage'>{{ session('message') }}</p>
@endif

@if (session('message_listing_availability'))
    <p class='errorMessage'>{{ session('message_listing_availability') }}</p>
@endif
<script type="text/javascript">
    $(document).ready(function() {
        //DATE PICKER
        
        $("#start_date").datepicker({
            minDate: 0,
            onSelect: function(dateText, inst) {
                var actualDate = new Date(dateText);
                var newDate = new Date(actualDate.getFullYear(), actualDate.getMonth(), actualDate.getDate()+1);
                $("#end_date").datepicker("option", "minDate", newDate);
            },
            onClose: function(dateText, inst) {
                setTimeout(function() {
                    $("#end_date").focus();
                }, 200);
            }
        });

        $("#end_date").datepicker();

        /*$('#start_date').datepicker({
            dateFormat: 'mm/dd/yy',
            changeMonth: true,
            changeYear: true
        });
        $('#end_date').datepicker({
            dateFormat: 'mm/dd/yy',
            changeMonth: true,
            changeYear: true
        });*/
    });
</script>

<form name="listing_availability" id="listing_availability" action="<?php echo url('sponsors/storeAvailability');?>" method="post">


    <!-- Microsoft IE Bug (When the form contain a field with a special char like &#8213; and the enctype is multipart/form-data and the last textfield is empty the first transmitted field is corrupted)-->
    <input type="hidden" name="ieBugFix" value="1" />
    <!-- Microsoft IE Bug -->

    <input type="hidden" name="_token" value="{{ csrf_token() }}">

    <input type="hidden" name="id" id="id" value="{{ $listing_id }}" />
    <input type="hidden" name="availability_id" id="availability_id" value="<?php echo isset($availability_id)?$availability_id : '' ?>" />
    <input type="hidden" name="letter" value="" />
    <input type="hidden" name="screen" value="" />
    <input type="hidden" name="form" value="listing_availability_booking" />
    <input type="hidden" name="delete_availability" id="delete_availability" value="<?php echo isset($delete_availability)?$delete_availability : '' ?>" />
    <?php if(!(isset($availability_id) && isset($delete_availability)))
        { ?>
      <table cellpadding="0" cellspacing="0" border="0" class="standard-table">
            <tr>
                <th colspan="2" class="standard-tabletitle">Add the Booking Date for the Listing</th>
            </tr>
            
            <tr>
                <th class="alignTop">* Check in:</th>
                <td>
                    <input type="text" name="start_date" id="start_date" value="<?php echo isset($spAvailabeId[0]->start_date)?Functions::format_date($spAvailabeId[0]->start_date):''; ?>" style="width:85px" />

                  <?php $start_date_period=isset($spAvailabeId[0]->start_date_period)?$spAvailabeId[0]->start_date_period:'';
                  $end_date_period=isset($spAvailabeId[0]->end_date_period)?$spAvailabeId[0]->end_date_period:'';
                  ?>
                  <select name="start_date_period" style="padding:2px;width:50px">
                        <option value="1" <?php echo $start_date_period==1? "selected='selected'" : "";?>>AM</option>
                        <option value="2" <?php echo $start_date_period==2 ? "selected='selected'" : "";?>>PM</option>
                    </select>
                    <span> (mm/dd/yyyy)</span>
                </td>
            </tr>
            
            <tr>
                <th class="alignTop">* Check Out:</th>
                <td>
                    <input type="text" name="end_date" id="end_date" value="<?php echo isset($spAvailabeId[0]->end_date)?Functions::format_date($spAvailabeId[0]->end_date):''; ?>" style="width:85px" />
                  <select name="end_date_period" style="padding:2px;width:50px">
                        <option value="1" <?php echo $end_date_period==1 ? "selected='selected'" : "";?>>AM</option>
                        <option value="2" <?php echo $end_date_period==2 ? "selected='selected'" : "";?>>PM</option>
                    </select>
                    <span> (mm/dd/yyyy)</span>
                </td>
            </tr>    
            <tr>
            <th>* Customer Name:</th>
                <td colspan="4"><input type="text" name="customer_name" value="<?php echo isset($spAvailabeId[0]->customer_name)?$spAvailabeId[0]->customer_name:''; ?>" /></td>
            </tr> 
        </table>
        <?php } 

        else { ?>

              <p class="informationMessage">Delete Booking  {{ Functions::format_date($start_date) }}
              {{ $start_date_period == 1 ? "Am" : "Pm" }} To {{ Functions::format_date($end_date) }}  {{ $end_date_period == 1 ? "Am" : "Pm" }} ?</p>

        <div class="header-form">
            
        </div>
        
     <?php 
        }
        ?>
        
    <div class="baseButtons">
    <?php  if(isset($delete_availability))

     {
        ?>
            <button type="button" name="submit_button" class="input-button-form" value="Submit" onclick="JS_submit(this.form.name,'','2');">Submit</button>
     
        <?php }
        elseif(!isset($delete_availability)) {?>  
            <button type="button" name="submit_button" class="input-button-form" value="Submit" onclick="JS_submit(this.form.name,'','');">Submit</button>
       <?php     
        }
        if(isset($availability_id) && !isset($delete_availability))
        {
         ?>  

          <button type="button" name="submit_button_delete" class="input-button-form" value="Submit" onclick="JS_submit(this.form.name,'','1');">Delete</button>
       
       <?php   } ?>    


         

      <button type="button" name="cancel" class="input-button-form" value="Cancel" onclick="document.getElementById('formlistingcancel').submit();">Cancel</button>
    
    </div>
    
    

    
</form>

<form id="formlistingcancel" action="<?php echo url('sponsors'); ?>" method="get">

        <input type="hidden" name="letter" value="" />
    <input type="hidden" name="screen" value="" />

</form>
                    </div>
                    
<script type="text/javascript">
    function JS_submit(form,year,delete_) {
        if(delete_) $('#delete_availability').val(delete_);
        if(year) $('#year').val(year);
        $('#'+form).submit();
    }
</script>

   <?php
   $year_current =  date("Y");

    if(session('year')) { $year  = session('year'); }
    else { $year  = date("Y"); }

$show_listing_availability_calendar = "";
 
 if($show_listing_availability_calendar){
     ?>
     <br clear="all">
     <p class="informationMessage">
        Please <a onclick="scrollPage('#contact-formScroll');" href="javascript: void(0);">contact the owner</a> for current availability as their booking calendar is not available at present
     </p>
     <?php
 }

$month  = date("m");
$day  = date("d");
$count = 12;
$class_calendar_aux1 = "listing_booked_booked";
$class_calendar_aux2 = "listing_booked_available_am";
$class_calendar_aux3 = "listing_booked_available_pm";
$last_availability_id='';

    if(!(isset($availability_id) && isset($delete_availability)))
        { ?>
          
                
            <table class="calendar-availability-table" width="100%" cellspacing="0" cellpadding="0" border="0">
                <tr>
                    <th>CheckIn Date</th>
                    <th>CheckOut Date</th>
                    <th>Customer Name</th>
                    <th>Edit/Delete</th>
                </tr>
                       
                @foreach($table_availability as $table_availability_item)
                <?php
                    $start_date_table = $table_availability_item->start_date;
                    $start_date_table_array = explode("-", $table_availability_item->start_date);
                    $day_start_date_table = $start_date_table_array[2];
                    $month_start_date_table = $start_date_table_array[1];
                    $year_start_date_table = $start_date_table_array[0];
                    
                    $end_date_table = $table_availability_item->end_date;
                    $end_date_table_array = explode("-",$table_availability_item->end_date);
                    $day_end_date_table = $end_date_table_array[2];
                    $month_end_date_table = $end_date_table_array[1];
                    $year_end_date_table = $end_date_table_array[0];
                ?>
                    <tr>
                        <td><?php echo $day_start_date_table; ?> <?php echo Functions::system_getMonthByNumber($month_start_date_table); ?> <?php echo $year_start_date_table; ?> <?php echo $table_availability_item->start_date_period==1 ? "AM" : "PM"; ?></td>
                          <td><?php echo $day_end_date_table; ?> <?php echo Functions::system_getMonthByNumber($month_end_date_table); ?> <?php echo $year_end_date_table; ?> <?php echo $table_availability_item->end_date_period==1 ? "AM" : "PM"; ?></td>
                        <td><?php echo $table_availability_item->customer_name; ?></td>
                        <td><a href="<?php echo url('sponsors/availability/'.$table_availability_item->listing_id.'/'.$table_availability_item->id); ?>">Edit/Delete</a></td>
                    </tr>
                @endforeach                                         
                  
                                
            </table>
           
         <form name="listing_availability_year" id="listing_availability_year" 
         action="<?php echo url('sponsors/years'); ?>" method="post">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="id" id="id" value="{{ $listing_id }}" />
            <input type="hidden" name="form" value="listing_availability_year" />
        

        <div class="calendar-members" >
        
            <div class="calendar-members-nav">
          <center>
                    <?php
                    $k_select = -1;
                    if(strpos($_SERVER["PHP_SELF"],"detail.php") !== false) $k_select++;
                    $k_select_control = 3;
                    ?>
                    <select name="year" onchange="JS_submit('listing_availability_year',this.value,'')">
                        <?php for($k=$k_select;$k<$k_select_control;$k++){?>
                            <option value="<?php echo $year_current+$k?>" <?php echo $year==$year_current+$k ? "selected=\"selected\";" : ""?>><?php echo $year_current+$k?></option>
                        <?php } ?>
                    </select>
                </center>
                            </div>  
            
            <ul class="legend">
            
            	<li><strong>Colour key:</strong></li>
                <li class="available">Available</li>
                <li class="booked">Booked</li>
                <li class="changeover-day">Changeover Day</li>
            
            </ul>     
          <?php for($month_count=1;$month_count<=$count;$month_count++){?>

                <?php 
                if($month_count < 10) $month_function = "0".$month_count;
                else $month_function = $month_count;
                $year_function = $year;
                $year_ = substr($year_function,-2);
                
                if($month_function>12) {
                    $month_function = $month_function - 12;
                    $year_function = $year +1;
                    $year_ = substr($year_function,-2);
                }
                
                ?>
            
                <table class="calendar-table" width="100%" cellspacing="0" cellpadding="0" border="0">
                    <tbody>
                        <tr>
                            <td class="calendar-header" colspan="7">
                                <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                    <tbody>
                                        <tr>
                                            <th><?php echo "".Functions::system_getMonthByNumber($month_function)." ".$year_function.""; ?></th>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                             <th abbr="sunday" title="sunday" class="calendar-weekday"><b>S</b></th>
                             <th abbr="monday" title="monday" class="calendar-weekday"><b>M</b></th>
                             <th abbr="tuesday" title="tuesday" class="calendar-weekday"><b>T</b></th>
                             <th abbr="wednesday" title="wednesday" class="calendar-weekday"><b>W</b></th>
                             <th abbr="thursday" title="thursday" class="calendar-weekday"><b>T</b></th>
                             <th abbr="friday" title="friday" class="calendar-weekday"><b>F</b></th>
                             <th abbr="saturday" title="saturday" class="calendar-weekday"><b>S</b></th>
                         </tr>
                     <?php
                          $Date = strtotime($month_function."/".$day."/".$year_);
                          $Day  = date('w',strtotime(date('n/\0\1\/Y',$Date)));
                          $Days = date('t',$Date);
                          

                          for ($i=1,$d=1;$d<=$Days;) {
                               echo("<tr>");
                               for ($x=1;$x<=7 && $d <= $Days;$x++,$i++) {
                                    if ($i > $Day) {
                                       
                                        $newdate=$month_function."/".$d."/".$year_function;
                                        $check_date = "";
                                        $class_calendar_day = "";
                                        $check_date = Sql::system_CheckDateListingAvailabilityBooking($listing_id,$newdate,$availability_id_aux_color);
                                        if($availability_id_aux_color){
                                            if($last_availability_id != $availability_id_aux_color){
                                                if($last_availability_id && $availability_id_aux_color){
                                                    $class_calendar_aux1 = Functions::system_ChangeCalendarColor($class_calendar_aux1);
                                                    $class_calendar_aux2 = Functions::system_ChangeCalendarColor($class_calendar_aux2);
                                                    $class_calendar_aux3 = Functions::system_ChangeCalendarColor($class_calendar_aux3);
                                                }
                                                $last_availability_id = $availability_id_aux_color;       
                                            }
                                        }
                                        
                                        switch ($check_date) {
                                            case 'available': $class_calendar_day = "listing_booked_available"; break;
                                            case 'booked': $class_calendar_day = $class_calendar_aux1; break;
                                            case 'booked_pm': $class_calendar_day = $class_calendar_aux2; break;
                                            case 'booked_am': $class_calendar_day = $class_calendar_aux3; break;
                                        }

                                        echo("<td  class='calendar-day ".$class_calendar_day."' width='22' align='center'>".$d++."</td>");
                                    }
                                    else { echo("<td> </td>"); }
                               }
                               for (;$x<=7;$x++) { echo("<td> </td>"); }
                               echo("</tr>");
                           }
                     ?>
                     </tbody>
                </table>
        
            <?php }?>
        
		                    

        </div>
        
     </form>
      <?php } ?>
    
    
				
			</div>


            </div><!-- Close container-fluid div -->
            
        </div><!-- Close container div -->



		 <script type="text/javascript">
        
        if ($("#myChart").length) {
            //This will get the first returned node in the jQuery collection.
            var ctx = $("#myChart").get(0).getContext("2d");
        }

        function initializeDashboard() {
            $(".dial").knob({
                readOnly:   true,
                fgColor:    "#2980b9",
                bgColor:    "#DEE1E3",
                fontWeight: 300,
                thickness:  .2,
                width:      70,
                height:     70
            });

            $(".status, .floating-tip, .alert-new, #item_renewal").tooltip({
                animation: true,
                placement: "top"
            });
             
            if ($("#myChart").length) {
                //Get context with jQuery - using jQuery's .get() method.
                ctx = $("#myChart").get(0).getContext("2d");
                loadChart();
            }
        }

        $(function() {
            $("#alert").fadeOut(5000);
            initializeDashboard();
        });
        
        function showReply(id) {
            $('#review_reply'+id).css('display', '');
            $('#link_reply'+id).css('display', 'none');
            $('#cancel_reply'+id).css('display', '');
        }
        
        function hideReply(id) {
            $('#review_reply'+id).css('display', 'none');
            $('#link_reply'+id).css('display', '');
            $('#cancel_reply'+id).css('display', 'none');
        }
        
        function showLead(id) {
            $('#lead_reply'+id).css('display', '');
            $('#link_lead'+id).css('display', 'none');
            $('#cancel_lead'+id).css('display', '');
        }
        
        function hideLead(id) {
            $('#lead_reply'+id).css('display', 'none');
            $('#link_lead'+id).css('display', '');
            $('#cancel_lead'+id).css('display', 'none');
        }
        
        function saveReply(id) {
            $("#submitReply"+id).css("cursor", "default");
            $("#submitReply"+id).prop("disabled", "disabled");
            $("#submitReply"+id).html('Wait, Loading...');
            
            $.post("https://www.shoresummerrentals.com/sponsors/ajax.php", $("#formReply"+id).serialize(), function(data) {
                if (data == "ok") {
                    $("#msgReviewE"+id).css("display", "none");
                    $("#msgReviewS"+id).css("display", "");
                    $("#msgReviewS"+id).fadeOut(5000);
                } else {
                    $("#msgReviewE"+id).css("display", "");
                    $("#msgReviewS"+id).css("display", "none");
                }
                $("#submitReply"+id).html('Submit');
                $("#submitReply"+id).prop("disabled", "");
                $("#submitReply"+id).css("cursor", "pointer");
            });
        }
        
        function saveLead(id) {
            $("#submitLead"+id).css("cursor", "default");
            $("#submitLead"+id).prop("disabled", "disabled");
            $("#submitLead"+id).html('Wait, Loading...');
            
            $.post("https://www.shoresummerrentals.com/sponsors/ajax.php", $("#formLead"+id).serialize(), function(data) {
                if (data == "ok") {
                    $("#msgLeadE"+id).css("display", "none");
                    $("#msgLeadS"+id).css("display", "");
                    $("#msgLeadS"+id).fadeOut(5000);
                    setTimeout("leadBox('hide', "+id+");", 4000);
                    $("#title_replied"+id).css("display", "none");
                    $("#new_replied"+id).css("display", "");
                } else {
                    $("#msgLeadE"+id).html(data);
                    $("#msgLeadE"+id).css("display", "");
                    $("#msgLeadS"+id).css("display", "none");
                }
                $("#submitLead"+id).html('Submit');
                $("#submitLead"+id).prop("disabled", "");
                $("#submitLead"+id).css("cursor", "pointer");
            });
        }
        
        function reviewBox(option, id) {
            $("#reviews-list").children(".item-review").children(".review-detail").stop(true,true).slideUp();
            $("#reviews-list").children(".item-review").children(".review-summary").stop(true,true).slideDown().removeClass("new");
            if (option == "show") {
                $("#review-summary-"+id).slideUp(); 
                $("#review-detail-"+id).slideDown();
                setItemAsViewed("review", id);
            } else {
                $("#review-summary-"+id).slideDown(); 
                $("#review-detail-"+id).slideUp();
            }            
        }
        
        function leadBox(option, id) {
            $("#leads-list").children(".item-review").children(".review-detail").stop(true,true).slideUp();
            $("#leads-list").children(".item-review").children(".review-summary").stop(true,true).slideDown().removeClass("new");
            if (option == "show") {
                $("#lead-summary-"+id).slideUp(); 
                $("#lead-detail-"+id).slideDown();
                setItemAsViewed("lead", id);
            } else {
                $("#lead-summary-"+id).slideDown(); 
                $("#lead-detail-"+id).slideUp();
            }            
        }
        
        function dealBox(option, id) {
            $("#deals-list").children(".item-review").children(".review-detail").stop(true,true).slideUp();
            $("#deals-list").children(".item-review").children(".review-summary").stop(true,true).slideDown();
            if (option == "show") {
                $("#deal-summary-"+id).slideUp(); 
                $("#deal-detail-"+id).slideDown();
            } else {
                $("#deal-summary-"+id).slideDown(); 
                $("#deal-detail-"+id).slideUp();
            }            
        }
        
        function changeDealStatus(option, id, promocode) {
            $.post("https://www.shoresummerrentals.com/sponsors/deal/deal.php",{action: option, promotion_id: promocode}, function() {
                if (option == "freeUpDeal") {
                    $("#label_used"+id).css("display", "");
                } else {
                    $("#label_used"+id).css("display", "none");
                }
            });
        }
        
        function setItemAsViewed(type, id) {
            $.post("https://www.shoresummerrentals.com/sponsors/ajax.php", {
                ajax_type: 'setItemAsViewed',
                type: type,
                id: id
            }, function () {});
        }
       
        function loadDashboard(item_type, item_id) {
            $.post("<?php echo url('loadDashboard');?>", {
                ajax_type: 'load_dashboard',
                item_type: item_type,
                item_id: item_id,
                _token:'{{ csrf_token() }}'
            }, function (ret) {
                $(".webitem").removeClass("active");
                $("#"+item_type+"_"+item_id).addClass("active");
                scrollPage('#float_layer');
                $("#dashboard").hide().html(ret).fadeIn(800);
                initializeDashboard();
            });
        }

        function selectLegend(option, id, chartdata) {
            var countVisible = 0;
            
            if (option == "viewALL") {
                
                if ( $(".legend-ALL").hasClass("isvisible")) {
//                    $(".legend-ALL").removeClass("isvisible");
//                    $("#optionLegend > li > i").removeClass("checked");
//                    $("#optionLegend > li").removeClass("isvisible");
//                    $("#controlLegend > li").remove();
                } else {
                    countVisible = 2;
                    $("#optionLegend > li > i").addClass("checked");                   
                    $(".legend-ALL").addClass("isvisible");
                    $("#optionLegend > li").not(".isvisible").clone().appendTo("#controlLegend");
                    $("#optionLegend > li").addClass("isvisible");
                }
            } else {
                id: id
                chartdata: chartdata
                $newlegend = $(".legend-"+id).clone();

                if ($(".legend-"+id).hasClass("isvisible")) {
                    
                    //Check if there's at least one other legend selected to prevent empty chart
                    $('#optionLegend li').each(function() {
                        if ($(this).hasClass("isvisible")) {
                            countVisible++;
                        }
                    });

                    if (countVisible > 1) {
                        $(".legend-"+id).children("i").removeClass("checked");
                        $(".legend-"+id).removeClass("isvisible");
                        $("#controlLegend").children(".legend-"+id).remove();
                        $(".legend-ALL").children("i").removeClass("checked");
                        $(".legend-ALL").removeClass("isvisible");
                    }
                } else {
                    countVisible = 2;
                    $(".legend-"+id).children("i").addClass("checked");
                    $(".legend-"+id).addClass("isvisible");
                    $newlegend.appendTo("#controlLegend");
                }
            }
            if (countVisible > 1) {
                controlChart();
            }
        }
                
        function loadChart() {
            var data = {
                labels : chartLabels,
                datasets : initialReport
            };
            var steps = 5;
            var max = maxInitialReport;
            if (max < steps) {
                steps = max;
            }
            var options = {
                bezierCurve : false,
                scaleOverride: true,
                scaleSteps: steps,
                scaleStepWidth: Math.ceil(max / steps),
                scaleStartValue: 0
            };
            ctx = $("#myChart").get(0).getContext("2d");
            new Chart(ctx).Line(data, options);        
        }
       
        function controlChart() {
            
            var datasets = new Array();
            var max = 0;
            var thisHighest = 0;
            $('#optionLegend li').each(function() {
                if ($(this).hasClass("isvisible")) {
                    var reportType = $(this).attr('report');
                    if (reportType) {
                        datasets.push(window[reportType]);
                        thisHighest = Math.max.apply(Math, window[reportType].data);
                        if (thisHighest > max) {
                            max = thisHighest;
                        }
                    }
                }
            });
            
            var steps = 5;
            if (max < steps) {
                steps = max;
            }
            var options = {
                bezierCurve : false,
                scaleOverride: true,
                scaleSteps: steps,
                scaleStepWidth: Math.ceil(max / steps),
                scaleStartValue: 0
            };

            var data = {
                labels : chartLabels,
                datasets : datasets
            };
            ctx = $("#myChart").get(0).getContext("2d");
            new Chart(ctx).Line(data, options);  

        }


function scrollPage(position_id){
    
	if(!position_id){
		$position_id = '#resultsMap';
	}else {
		$position_id = position_id;
	}
	//jQuery('html,body').animate({scrollTop: jQuery($position_id).offset().top},'slow');
    jQuery('html, body').animate({ scrollTop: jQuery('#dashboard').offset().top }, 1500);
}
    </script>
	<script src="{{ asset('/front/js/Chart.js') }}"></script>		
@endsection