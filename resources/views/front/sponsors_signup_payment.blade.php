<?php
use App\Functions\Functions;

 $max_item_sum = 20;
 $stop_payment = false;
?>  
@extends('front')

@section('content')

	<link rel="stylesheet" href="{{ asset('front/css/bootstrap.min.css') }}" />
	<link rel="stylesheet" href="{{ asset('front/css/content_custom.min.css') }}" />
	<link href="https://www.shoresummerrentals.com/custom/domain_1/theme/default/content_custom.min.css" rel="stylesheet" type="text/css" media="all">
    <!--link rel="stylesheet" href="{{ asset('front/css/bootstrap-responsive.min.css') }}" /-->
 
 <div class="container">
<div class="content-main" id="screen5" style="">

            <div class="order-head">
                <ul class="standardStep steps-3">
                    <li class="steps-ui stepLast"><span>3</span>&nbsp;Confirmation</li>
                    <li class="steps-ui stepActived"><span>2</span>&nbsp;Check Out</li>
                    <li class="steps-ui"><span>1</span>&nbsp;Identification</li>
                </ul>
            </div>


<div class="order">

                <div id="billing-detail">

                    <div class="left textright">
                        <h3>Billing Detail</h3>
                        <p>A quick overview of how much you will be charged.</p>
                    </div>

                    <div class="right">
                        
 @if (isset($bill_info["listings"])): 
     <?php foreach($bill_info["listings"] as $id => $info) { ?>                        
    <table class="standard-tableTOPBLUE">
                            <tbody><tr>
                                <th>Listing </th>
                                <th>Level</th>
                                  <?php if (($bill_info["listings"]) && $info["extra_category_amount"] > 0) { ?>
                                        <th>Extra Category</th>
                                    <?php } ?>

                             <?php if ((config('params.PAYMENT_FEATURE') == "on") && $info["discount_id"] && ((config('params.CREDITCARDPAYMENT_FEATURE') == "on") || (config('INVOICEPAYMENT_FEATURE') == "on"))) { ?>
                               <th>Promotional Code</th>
                                    <?php } ?>
                                
                               <?php /* if ($payment_tax_status == "on" || ($ispackage == "true" && $auxitem_name)) { ?>
                                        <th>{{ ($ispackage == "true" && $auxitem_name) ? system_showText(LANG_LABEL_PRICE_PLURAL) : system_showText(LANG_SUBTOTAL)) }}</th>
                                    <?php } ?>

                                    <?php if ($payment_tax_status == "on" && $ispackage != "true") { ?>
                                        <th><?=$payment_tax_label." (".$payment_tax_value."%)";?></th>
                                    <?php } */?>
                                <th>Total</th>
                                                                
                            </tr>
                         
                            <tr>
					
                    <td>{{ $info["title"] }} </td>
                    <?php if (($bill_info["listings"]) && $info["extra_category_amount"] > 0) { ?>
                            <td>{{ $info["extra_category_amount"] }}</td>
                    <?php } ?>
                    <td>{{ Functions::string_ucwords($info["level"])  }}</td>
                     <?php if ((config('params.PAYMENT_FEATURE') == "on") && $info["discount_id"] && ((config('params.CREDITCARDPAYMENT_FEATURE') == "on") || (config('INVOICEPAYMENT_FEATURE') == "on"))) { ?>
                <td>{{ ($info["discount_id"]) ? $info["discount_id"] : "N/A" }}</td>
                <?php } ?>

                <?php /*if ($payment_tax_status == "on" || ($ispackage == "true" && $auxitem_name)) { ?>
                <td><?=config('params.CURRENCY_SYMBOL')." ".($aux_package_total > 0 ? format_money($bill_info["total_bill"]-$aux_package_total) : $bill_info["total_bill"]);?></td>
                  <? } ?>

                   <? if ($payment_tax_status == "on" && $ispackage != "true" ) { ?>
               <td><?=config('params.CURRENCY_SYMBOL')." ".Functions::payment_calculateTax($bill_info["total_bill"], $payment_tax_value, true, false); */?></td>

               <td>{{ config('params.CURRENCY_SYMBOL')." ".$info["total_fee"] }}   </td>
                                                                    
                            </tr>
                            
           <?php } ?>                                                   
                        </tbody></table>
                        
        @endif                                           
                        
                    </div>

                </div>
                
                
                <div id="payment-method">


                @if($payment_method!="paypal")
                    <div class="left textright">
                        <h3>Payment</h3>
                        <p>Make your payment and finish your order.</p>
                    </div>
                    @endif
                    <div class="right">
                        <div class="option">
                            
                   

                            
                            <?php
                            if ($bill_info["listings"]) {
                                $thisListingID = array_keys($bill_info["listings"]);
                    $next = url('sponsors/listing/'.$thisListingID[0].'/signup');
                            } 

                            ?>
                            <script language="javascript" type="text/javascript">
                                <!--

                                function next() {
                                    if (document.getElementById("terms").checked){
                                        document.location="{{ $next }}";
                                    } else {
                                        fancy_alert('Please check the agreement terms.', 'informationMessage', false, 500, 100, false);
                                    }
                                }

                                //-->
                            </script>
                        </div>
                    </div>

                </div>
          <?php      
          $payment_process = "signup";

            ?>
            @include('front/form_billing_'.$payment_method)
           

            </div>
              
		  </div>
    

</div>



	<script src="{{ asset('/front/js/Chart.js') }}"></script>		
@endsection