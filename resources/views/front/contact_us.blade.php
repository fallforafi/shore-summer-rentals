@extends('front')

@section('content')


	
	<section class="contact-page">
		<div class="contactUs__container container">
			<h1><strong>CONTACT US</strong></h1>
			<h3>Reach ShoreSummerRentals.com</h3>
			<p>This form is for general inquiries to the <a href="http://www.shoresummerrentals.com">ShoreSummerRentals</a>&nbsp;</span>staff, not for rental requests. &nbsp;<br>If you tried our <a href="http://www.shoresummerrentals.com/advsearch">Advanced Search</a> and still cannot find a rental that suits your needs, please&nbsp; fill out this&nbsp; <a href="http://shoresummerrentals.us2.list-manage.com/subscribe?u=3fd02cb749e6ae50b2a8ebc8a&amp;id=68363bfde1">Rental Request Form</span>.</a> &nbsp; Thank you!</p>
			
			
			@if (count($errors) > 0)
				<div class="alert alert-danger">
					<strong>Whoops!</strong> There were some problems with your input.<br><br>
					<ul>
						@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
			@endif
			<div class="contact-form col-sm-4">
			
			<form name="contactusForm" id="contactusForm" action="{{ url('contact-send') }}" method="post" class="form">
               <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group">
                    <label for="name">* Name</label>
                    <input id="name" name="name" value="" type="text" class="form-control text col-sm-12" required >
                </div>

                <div class="form-group">
                    <label for="email">* E-mail</label>
                    <input id="email" name="email" value="" type="email" class="form-control text col-sm-12" required>
                </div>

                <div class="form-group">
                    <label for="name">* Phone</label>
                    <input id="name" name="phone" value="" type="text" class="form-control text col-sm-12" required>
                </div>

                <div class="form-group">
                    <label for="title">* Subject</label>
                    <input id="title" name="title" value="" type="text" class="form-control text col-sm-12" required>
                </div>
                
				<div class="form-group">
					<label for="message">* Message</label>
					<textarea id="message" name="messageBody" rows="5" cols="30" class="form-control textarea col-sm-12" required></textarea>
				</div>
            
				<div class="form-group contact-form__submit">
                    <button class="form-control contact-form__submitBtn col-sm-12" type="submit">ONLY CLICK SEND ONE TIME</button>
				</div>

        </form>

    </div>
	
			<div class="col-sm-8 contactus">
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3082.055778393695!2d-74.62588968474441!3d39.42286312340406!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c0c2e9ebebfd4b%3A0xfb96c044e8b82a8e!2sShore+Summer+Rentals!5e0!3m2!1sen!2s!4v1456928760709" width="100%" height="240" frameborder="0" style="border:0" allowfullscreen></iframe>
			
			
				<div class="contact__inr col-sm-6 p0">
					<address>
						<b>Address</b>
						<p>Shore Summer Rentals, P.O. Box 55</p>
						<p>Somers Point, New Jersey 08244</p>
						<p>USA</p>
					</address>
				</div>
            
				<div class="contact__inr col-sm-6 p0">      
					<address>
						<b>E-mail</b>
						<p id="contactSidebarInfo_noicon">info@shoresummerrentals.com</p>
					</address>
					
					<address>
						<b>Phone</b>
						<p>877- SHORE-4U</p>
					</address>
				</div>

			</div>
	
		</div>
	</section>
						

					
@endsection