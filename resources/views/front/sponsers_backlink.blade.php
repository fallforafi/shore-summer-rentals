@extends('front')

@section('content')

	<link rel="stylesheet" href="{{ asset('front/css/bootstrap.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('front/css/members.min.css') }}" />
	<link href="{{ asset('front/css/content_custom.min.css') }}" rel="stylesheet" type="text/css" media="all">
        <link rel="stylesheet" href="{{ asset('front/css/structure.min.css') }}" />

<style type="text/css">
.backlinks-page .fancybox-inner {
    height: auto !important;
}
</style>
    <!--link rel="stylesheet" href="{{ asset('front/css/bootstrap-responsive.min.css') }}" /-->
     
    
	 <div class="backlink-page-container container members ">

        <div class="container-fluid">

		
		  <nav class="minor-nav">
        <ul>
            <li>
                <a  href="<?php echo url('sponsors/listing/'.$listing_id.''); ?>">Listing Information</a>
            </li>

                            <li >
               <a class="active" href="<?php echo url('sponsors/backlinks/'.$listing_id.''); ?>">Backlink</a>
                </li>
            
            
            <li>
                <a href="<?php echo url('sponsors/rate/'.$listing_id.''); ?>">Rate</a>
            </li>
            
            <li>
                <a href="<?php echo url('sponsors/specials/'.$listing_id.''); ?>">Specials</a>
            </li>
        </ul>
    </nav>
  


	<div>

						        
        <div class="package">

    <form name="backlinks" id="backlinks" method="post" action="<?php echo url('store/backlinks'); ?>">
                
                <input type="hidden" name="id" value="{{ $listing_id }}" />
                <input type="hidden" id="backlinkValid" name="backlinkValid" value="0" />
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                
	<h2>Increase your Visibility</h2>
        
    
	<div class="backlink">

		<img class="pull-left" height="232" width="244" src="<?php echo url().'/images/img-backlink-add.gif'; ?>">

		<div class="content-custom">
			<p>
				<h4>Why add a Backlink?</h4>
				Adding a link to your website pointing to this one, increases the relevance of this site and in turn the relevance of your listing.<br /> <br />
				<h4>What's in it for me?</h4>
				By giving us a link on the homepage of your site, you help us with our ranking and hence your results. But as well as helping us, we will go the extra mile and help you. If you add a link, once we have verified it exists, we will show your listing with a special style on the results page, so you really get some extra exposure in the directory - it's a win / win situation for us both.<br /> <br />
				<h4>How can I do this?</h4>
				Simple really, copy the code above into the code of your website, so that it shows up somewhere prominent on the home page, give us the URL of your website (where the link is) and we will verify it after you hit the "Verify" button - then just continue on.... super simple.<br />
                <strong>Please, notice that you can change this code as you want, since you keep the URL exactly like shown here, otherwise your backlink will not be validated.</strong>
			</p>
		</div>		

	</div>

	<div class="row-fluid well-steps">
		<div class="col-sm-4">
			<p>Put this on your Site (HTML Code):</p>
			<textarea readonly><?php echo $backlinks; ?></textarea>
		</div>
		<div class="col-sm-4">
			<p>Enter the URL of your Site: 
			<br /><small>Ex: http://www.mywebsite.com</small></p>
			<input type="text" id="website_url" name="backlink_url" value="" />
			
		</div>
		<div class="col-sm-4">
			<p>Click the Button to verify your Backlink</p> 

			<button type="button" class="btn btn-info verifyBtnBacklink" onclick="checkWebsite();">Verify</button>
            <p style="display:none" id="imgLoading"><img src="<?php echo url().'/images/icon-loading-content.gif'; ?>" /></p>
			
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="clearfix"></div>

	<div class="row-fluid text-center">
		<br />
		<button type="button" class="btn btn-info inactive addBacklinkBtn" disabled="disabled" id="continue" style="cursor: default;" onclick="addBacklink();">Yes, add Backlink</button>
	</div>
	
	<script type="text/javascript">
        function addBacklink(){
			$('#backlinks').submit();
		}
		
		function checkWebsite(){
			
			var url = "";
			var listingId = <?php echo $listing_id; ?>;
			
			url = $('#website_url').val();
			
			if (url){
                $("#imgLoading").css("display", "");
				$.post("<?php echo url('check_website'); ?>", {
                    url: url,
                     _token:'<?php echo csrf_token(); ?>',
                    id: listingId
				}, function (response) {
                    $("#imgLoading").css("display", "none");
					if (response == "OK"){
						$('#continue').prop("disabled", "");
						$('#continue').removeClass("inactive");
						$('#continue').css("cursor" , "pointer");
                        $('#backlinkValid').val("1");
                        fancy_alert('Site successfully validated!', 'successMessage', false, 350, 'auto', false);
					} else {
                        $('#continue').prop("disabled", "disabled");
						$('#continue').addClass("inactive");
						$('#continue').css("cursor" , "default");
                        $('#backlinkValid').val("0");
                        fancy_alert('Validation failed! Please try again.', 'errorMessage', false, 350, 'auto', false);
					}
				});	
			} else {
                fancy_alert('Please, type your site URL first.', 'informationMessage', false, 350, 'auto', false);
			}
		}
		
	</script>            </form>

        </div>

	</div>


            </div><!-- Close container-fluid div -->
            
        </div><!-- Close container div -->



		 <script type="text/javascript">
   function fancy_alert(msg, msgStyle, auto, width, height, modal) {
    if (typeof THEME_FLAT_FANCYBOX == "undefined") {
        $.fancybox(
            "<p class=\""+msgStyle+"\">"+msg+"</p>",
                {
                    'modal'             : modal,
                    'autoDimensions'    : auto,
                    'width'             : width,
                    'height'            : height,
                    'titleShow'         : false
                }
        );
    } else {
        if (THEME_FLAT_FANCYBOX && THEME_FLAT_FANCYBOX != "undefined") {
            $.fancybox(
                "<div class=\"modal-content-mini color-4\"><h2><span><a href=\"javascript:void(0);\" onclick=\"parent.$.fancybox.close();\">Close</a></span></h2><p class=\""+msgStyle+"\">"+msg+"</p></div>",
                    {
                        'maxWidth'          : 475,
                        'padding'           : 0,
                        'margin'            : 0,
                        'closeBtn'          : false,
                        'titleShow'         : false,
                        'showCloseButton'   : false
                    }
            );
        } else {
            $.fancybox(
                "<p class=\""+msgStyle+"\">"+msg+"</p>",
                    {
                        'modal'             : modal,
                        'autoDimensions'    : auto,
                        'width'             : width,
                        'height'            : height,
                        'titleShow'         : false
                    }
            );
        }
    }
} 
function scrollPage(position_id){
    
	if(!position_id){
		$position_id = '#resultsMap';
	}else {
		$position_id = position_id;
	}
	//jQuery('html,body').animate({scrollTop: jQuery($position_id).offset().top},'slow');
    jQuery('html, body').animate({ scrollTop: jQuery('#dashboard').offset().top }, 1500);
}
    </script>
	<script src="{{ asset('/front/js/Chart.js') }}"></script>		
@endsection