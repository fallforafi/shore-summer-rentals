<?php 
use App\Models\Domain\Sql;
use App\Functions\Functions;
use App\Functions\Listing;
use App\Functions\Review;
use App\Functions\Lead;
use App\Functions\Profile;


        

    $item_phoneviews = 0;
    $item_websiteviews = 0;
    $item_faxviews = 0;
    $item_leads = 0;
    $banner_views = 0;
    $banner_clicks = 0;
    $item_hasemail=true;

    //if (is_array($array_fields) && (in_array("email", $array_fields) || in_array("contact_email", $array_fields))) {
        //$item_hasemail = true;
    //}        


        $reports = Sql::retrieveListingReport($item_id);
        $itemObj=new Listing();
        $itemObj->Listing($item_id);
        $item_status=$itemObj->getString("status");
        //$item_dateCreated = $itemObj->getString("entered");
        $item_dateCreated = $itemObj->getString("entered");
        $auxCreated = explode(" ", $item_dateCreated);
        $auxCreatedDate = explode("-", $auxCreated[0]);
        
        $report_startYear = (int)$auxCreatedDate[0];
        $report_startMonth = (int)$auxCreatedDate[1];
        
        $report_endYear = (int)date("Y");
        $report_endMonth = (int)date("m");
       
        if ($report_startYear < $report_endYear) {
            $report_startYear = $report_endYear;
            $report_startMonth = 1;
        }

        //Available Reports according to each module/level
        $avReports = array();
        $avReportsLabels = array();
        $avReportsColors = array();
        $graphic = array();

        if ($item_type== "Listing") {

          

            $avReports[] = "email";
            $avReportsLabels[] = "Leads";
            $avReportsColors[] = "106,176,222";

             $avReports[] = "detail";
            $avReportsLabels[] = "Detail Views";
            $avReportsColors[] = "39,174,96";

             $avReports[] = "summary";
        $avReportsLabels[] = "Summary View";
        $avReportsColors[] = "211,84,0";

             $avReports[] = "phone";
             $avReportsLabels[] = "Phone Views";
              $avReportsColors[] = "26,188,156";

               $avReports[] = "fax";
                $avReportsLabels[] = "Fax Views";
                $avReportsColors[] = "217,83,79";

               

             $avReports[] = "sms";
             $avReportsLabels[] = "Send to Phone";
              $avReportsColors[] = "50,209,117";

        }

          

        if ($reports) {


        foreach ($reports as $key => $report) {
            //Total Stats for box "Activity Report"
            $item_phoneviews += $report["phone"];
            $item_websiteviews += $report["click"];
            $item_faxviews += $report["fax"];
            $item_leads += $report["email"];
             //$banner_views = $report["view"];
           // $banner_clicks = $report["click_thru"];


           list($year, $month) = explode("-", $key);

            //Prepare reports to build the chart
            foreach($avReports as $avReport) {
                $graphic[$year][$avReport][$month] = $report[($avReport == "websiteclick") ? "click" : $avReport];
            
             

            }

        }


    }

      //Chart code
    $months = explode(",", config('params.LANG_DATE_MONTHS'));
    
    $strLabel = "";
    for ($i = $report_startMonth; $i <= $report_endMonth; $i++) {
        $strLabel .= "\"".ucfirst(Functions::string_substr($months[$i-1], 0, 3))."\",";
    }

    $strLabel = Functions::string_substr($strLabel, 0, -1);
    $count = 1;
    $initialReport = "";
    foreach ($avReports as $avReport) {
        if ($count <= 2) {
            $initialReport .= $avReport.",";
        }
        ${"data_".$avReport} = "[";
        $count++;
    }


    $initialReport = Functions::string_substr($initialReport, 0, -1);
    $maxInitialReport = 0;
    $showChart = false;
    $counters=0;
    for ($i = $report_startMonth; $i <= $report_endMonth; $i++) {
       
     if($counters==4) 
            {
               break;
            }

        foreach ($avReports as $avReport) {


            ${"value_".$avReport} = (isset( $graphic[$report_endYear][$avReport][$i]) ?  $graphic[$report_endYear][$avReport][$i] : '');
            ${"data_".$avReport} .= (${"value_".$avReport} ? ${"value_".$avReport} : "0").",";


            //Enable chart if there's at least one report to be shown
            if (${"value_".$avReport}) {
                $showChart = true;
            }
                    
            //Define max value for initial report
            if (Functions::string_strpos($initialReport, $avReport) !== false) {
                if (${"value_".$avReport} > $maxInitialReport) {
                    $maxInitialReport = ${"value_".$avReport};
                }
            }
 
            
    }

    $counters++;


      }


    foreach ($avReports as $avReport) {
        ${"data_".$avReport} = Functions::string_substr(${"data_".$avReport}, 0, -1)."]";



    }
    
  $item_status = "A";
//Review
    //setting_get("commenting_edir", $commenting_edir);    
    $review_enabled ="on";
    $item_hasreview = false;

    if ($review_enabled == "on") {

   $reviewsArrTmp=Sql::getReviews($item_type,$item_id);


        if(count($reviewsArrTmp)>0)
        {
            $item_hasreview = true;
        //Avg review
        $item_avgreview = $itemObj->getNumber("avg_review");
        

       $newReviews = 0;
       $reviewsArr=array();
        if ($reviewsArrTmp) foreach ($reviewsArrTmp as $each_reviewsArrTmp) {
           

           $reviewsArr[]= $reviewsArrs= new Review();
           $reviewsArrs->Review($each_reviewsArrTmp->id);
                     
            if ($each_reviewsArrTmp->new == "y") {
                $newReviews++;
            }


        }
         
        
    if ($newReviews == 1) {
            $newReviewsTip = str_replace("[x]", $newReviews, "You received [x] new review");
        } else {
            $newReviewsTip = str_replace("[x]", $newReviews, "You received [x] new reviews");
        }
        }
      
        
    }


     //Leads
    if ($item_hasemail) {

        //$where = " Leads.type = '".strtolower($item_type)."' AND Leads.item_id = '$item_id' AND Leads.item_id = $leadTable.id AND $leadTable.account_id = '$acctId'";

        $leadsArrTmp=Sql::getLeads($item_type,$item_id);
        
        $newLeads = 0;
        //$leadsArr=array();
        if (count($leadsArrTmp)>0)
        {
            foreach ($leadsArrTmp as $each_leadssArrTmp) {
            $leadsArr[] = $auxLeadObj = new Lead();
             $auxLeadObj->Lead($each_leadssArrTmp->id);
            //$leadsArr[] = $auxLeadObj->data_in_array;

            if ($each_leadssArrTmp->new == "y") {
                $newLeads++;
            }
        }
        
        if ($newLeads == 1) {
            $newLeadsTip = str_replace("[x]", $newLeads, "You received [x] new lead");
        } else {
            $newLeadsTip = str_replace("[x]", $newLeads, "You received [x] new leads");
        }
        } 
        else
        {
            $item_hasemail=false;
        }
     

    }

     //$shareFacebook    = "href=\"http://www.facebook.com/sharer.php?u=".$linkFacebook."&amp;t=".urlencode($itemObj->getString("title"))."\" target=\"_blank\"";
       // $shareTwitter     = "href=\"http://twitter.com/?status=".$linkTwitter."\" target=\"_blank\"";


     $shareFacebook    = "http://www.facebook.com/sharer.php";
        $shareTwitter     = "http://twitter.com/?status=";
 //Max reviews/leads shown per block
    $maxItems = 3;

 ?>

	<div class="dashboard">
        <header>
             
      <?php if($spListingDataId[0]->status!='A'){ ?>
        <a href="<?php echo url('sponsors/listinglevel?id='.$spListingDataId[0]->id); ?>" class="btn btn-primary">Choose Membership</a> 
                                  <?php } ?>
                                    <h1><?php echo $spListingDataId[0]->email; ?><span style="margin-left: 36%;">Rental ID: <?php echo $spListingDataId[0]->id; ?></span></h1>
            
                            <p><a class="floating-tip" href="<?php echo url('sponsors/billing'); ?>" data-original-title="" title="">Click here to pay for your listing</a></p>
            
            <h5><a href="<?php echo url('advertise'); ?>" target="_blank" class="">View Membership Details</a></h5>
            <h5><a href="<?php echo url('contact-us'); ?>" target="_blank" class="">Contact us to downgrade and/or for multiple property discounts</a></h5>

        </header>

        <?php if($showChart){ ?>
        <section class="stats-complete">
            
            <h2>Statistics</h2>
            
            <div class="chart-legends">
                               
                <div class="hidden-legends {{ (count($avReports) <= 2 ? 'hidden-desktop' : '') }}">

                    <span>View more stats &raquo;</span>
                    
                    <ul id="optionLegend">
                        
                           <?php 
                        $countReport = 1;
                        foreach ($avReports as $avReport) { ?>
                        
                            <li class="legend-{{ $countReport }} {{ ($countReport <= 2 ? 'svisible' : '') }}" report="{{ $avReport }}" onclick="selectLegend('select', {{ $countReport }}, {{ $avReport }})">
                                <i class=<?php echo ($countReport <= 2) ? "checked" : ""; ?>></i>
                                <b style="background-color: rgb( <?php echo $avReportsColors[($countReport-1)]; ?>)"></b>
                                {{ $avReportsLabels[($countReport-1)] }}
                            </li>
                        
                        <?php
                        $countReport++;
                        }
                        
                        if (count($avReports) > 2) { ?>
                        <li class="legend-ALL" onclick="selectLegend('viewALL', {{ $countReport }})">
                            <i></i>
                            <b></b>
                           View all
                        </li>
                        <?php } ?>          


                         
                                                

                    </ul>
                        
                </div>

                   <ul id="controlLegend">
                    
                    <li class="legend-1 isvisible" <?php echo(count($avReports) > 1 ? "onclick=\"selectLegend('select', 1, ".$avReports[0].")\"" : ""); ?> >
                        <i class="checked"></i>
                        <b style="background-color: rgb(<?php echo $avReportsColors[0]; ?>)"></b>
                        {{ $avReportsLabels[0] }}
                    </li>
                    
                    <li class="legend-2 isvisible" <?php echo (count($avReports) > 1 ? "onclick=\"selectLegend('select', 2, ".$avReports[1].")\"" : "") ?> >
                        <i class="checked"></i>
                        <b style="background-color: rgb(<?php echo $avReportsColors[1]; ?>)"></b>
                        {{ $avReportsLabels[1] }}
                    </li>              
                    
                </ul>
                
               
                
            </div>
            
            <canvas id="myChart" width="580" height="200" style="width: 580px; height: 200px;"></canvas>
            
        </section>
     <?php } ?>
                
        <section class="stats-summary">
            
            <h2>Activity Report</h2>

            <div class="row-fluid">
                
                                
                <div class="col-sm-3">
                                 <p>{{ $item_phoneviews }} Phone Views</p>
                                         
                                 <p><?php echo  $spListingDataId[0]->number_views; ?> Page Views</p>
                                        
                                 <p>{{ $item_faxviews }} Fax Views</p>
                                    </div>
                
               
 <?php if ($item_hasreview && $item_status == 'A' && isset($leadsArr)) { ?>                  
                <div class="col-sm-3">
                    
                 <h5>{{ count($leadsArr) }}</h5>
                    
                    <p>Total Leads</p>

                          <?php if ($item_leads) { ?>
                        <p>
                     <a href="javascript:void(0);" onclick="scrollPage('#leads-list');">See all unread leads</a>
                            <?php if ($newLeads) { ?>
                            <em class="alert-new" title="{{ $newLeadsTip }}">{{ ($newLeads > 10 ? "9+" : $newLeads) }}</em>
                            <?php } ?>
                        </p>
                    <?php } ?>
                    
                                        
                </div>
                 <?php } ?>



                <?php if ($item_hasreview) { ?>                  
                <div class="col-sm-3">
                    
                    <div class="large-rating">
                        <div class="stars-rating">
                            <div class="rate-{{ $item_avgreview }}"></div>
                        </div>
                    </div>
                    
                    <p>Based on {{ count($reviewsArr) }} Reviews</p>

                        <?php if (count($reviewsArr)) { ?>
                        <p>
           <a href="javascript:void(0);" onclick="scrollPage('#reviews-list');">See all reviews</a>
                            <? if ($newReviews) { ?>
  <em class="alert-new" title="{{ $newReviewsTip }}">{{ ($newReviews > 10 ? "9+" : $newReviews) }}</em>
                            <? } ?>
                        </p>
                    <?php } ?>
                    
                                        
                </div>
                 <?php } ?>
                                
            </div>
            
        </section>
        
        <?php if ($item_hasreview) {     ?>  
        <section class="reviews-list" id="reviews-list">
            
            <h2>Reviews</h2>

     <div class="row-fluid head">
            <?php if ($item_status == "A") { ?>
                
                <div class="span9">
                    <p>
                        <a href="<?php echo $shareFacebook; ?>" target="_blank"><i class="socialicon social-facebook"></i></a>
                        <a href="<?php echo $shareTwitter; ?>" target="_blank"><i class="socialicon social-twitter"></i></a>
                        Share and get more reviews
                    </p>
                </div>
                
                <?php } ?>
            
       
                
                           

                   <div class='span{{ ($item_status == "A" ? "3" : "12") }}'>
 <p class="paging">{{ count($reviewsArr) }} {{ (count($reviewsArr) == 1 ? "Review" : "Reviews") }}</p>
                </div>                
              
                
            </div>



            <?php
            $countReview = 1;
            
            if ($reviewsArr) foreach($reviewsArr as $each_rate) {

                //Review Title
                    if ($each_rate->getString("review_title")) {
                        $review_title = $each_rate->getString("review_title");
                    } else {
                        $review_title ="N/A";
                    }

                    //Reviewer Name
                    if ($each_rate->getString("reviewer_name")) {
                        $reviewer_name = $each_rate->getString("reviewer_name");
                    } else {
                        $reviewer_name = "N/A";
                    }
                    
                    //Reviewer Image                    
                    /*$imgTag = "";
                    if ($each_rate->getNumber("member_id")) {
                        $profile = new Profile($each_rate->getNumber("member_id"));
                        $imgTag = socialnetwork_writeLink($each_rate->getNumber("member_id"), "", "", $profile->getNumber("image_id"), false, false, "", true, "user-profile", true);
                    } else {
                        $imgTag = "<img src=\"/images/profile_noimage.gif\" alt=\"$reviewer_name\">";
                    }*/

                     //Review Status
                    $pending = true;
                    if ($each_rate->getNumber("approved") == 0) {
                        
                        if (Functions::string_strlen(trim($each_rate->getString("response"))) > 0) {
                            
                            //Pending Review and Pending Reply
                            if ($each_rate->getNumber("responseapproved") == 0) {
                                $reviewStatus = config('params.LANG_MSG_WAITINGSITEMGRAPPROVE_REVIEW_REPLY');
                            } else {
                                //Pending Review
                            $reviewStatus = config('params.LANG_MSG_WAITINGSITEMGRAPPROVE_REVIEW');
                            }
                            
                        } else {
                            //Pending Review
                            $reviewStatus =config('params.LANG_MSG_WAITINGSITEMGRAPPROVE_REVIEW');
                        }
                        
                    } elseif ($each_rate->getNumber("approved") == 1) {
                        
                        if (Functions::string_strlen(trim($each_rate->getString("response"))) == 0) {
                            
                            //Review approved
                            $pending = false;
                            $reviewStatus = config('params.LANG_MSG_REVIEW_ALREADY_APPROVED');
                            
                        } elseif (Functions::string_strlen($each_rate->getString("response")) > 0) {
                            
                            //Reply pending
                            if ($each_rate->getNumber("responseapproved") == 0) {
                                $reviewStatus = config('params.LANG_MSG_WAITINGSITEMGRAPPROVE_REPLY');
                            } else {
                                //Review and reply approved
                                $pending = false;
                                $reviewStatus = config('params.LANG_MSG_REVIEWANDREPLY_ALREADY_APPROVED');
                            }
                            
                        }
                        
                    }
                    ?>

           <div class="item-review" id="item-review_{{ $countReview }}" style=<?php echo $countReview > $maxItems ? "display:none;" : "" ?> >
                
                <div class="review-summary <?php echo ($each_rate->getString("new") == "y" ? "new" : ""); ?>" onclick="reviewBox('show', {{ $each_rate->id }});" id="review-summary-{{ $each_rate->getNumber("id") }}">
                    
                    <span class="pull-right">
                        <a href="javascript:void(0);">View </a>
                |<a href="javascript:void(0);" onclick="showReply(<?php echo $each_rate->id; ?>)"> <?php echo ($each_rate->getString("response") ? "Edit Reply" : " Reply") ;?></a>

                    </span>
                    
                    <b>{{ $review_title }}</b>
                    
                    <br>
                    
                    <div class="stars-rating">
                        <div class="rate-<?php echo $each_rate->getString("rating"); ?>"></div>
                    </div>
                    
                    <time> {{ ($each_rate->getString("added")) ? Functions::format_date($each_rate->getString("added"), config('params.DEFAULT_DATE_FORMAT'), "datestring") : "N/A" }}</time>
               <!--datestring

               Functions::system_showTruncatedText(-->
                    <p>{{ $each_rate->getString("review") }} - <em>{{ $reviewer_name }}</em></p>
                                        
                </div>
                
     <div class="review-detail" id='review-detail-{{ $each_rate->getNumber("id") }}' style="display:none">
                    
       <a href="javascript:void(0);" onclick="reviewBox('hide', <?php echo $each_rate->getNumber("id"); ?>);" class="pull-right">Hide</a>
                    
                    <?php if ($pending) { ?>
                        <small class="text-warning">{{ $reviewStatus }}</small>
                    <?php } ?>
                    <div>
                        <?php //echo $imgTag; ?>
                        
                        <p>Review By <b>{{ $reviewer_name }}</b></p>
                        
                        <time><?php echo ($each_rate->getString("added")) ? Functions::format_date($each_rate->getString("added"), config('params.DEFAULT_DATE_FORMAT'), "datestring") : "N/A";?></time>
                        
                        <div class="large-rating">
                            <div class="stars-rating">
                                <div class="rate-<?php echo $each_rate->getString("rating");?>"></div>
                            </div>
                        </div>
                    </div>
                    
                    <q class="review-text">{{ $each_rate->getString("review", true) }}</q>

                     <q class="review-reply">
                        
                        <?php if (Functions::string_strlen(trim($each_rate->getString("response"))) > 0) { ?>
                            <span>Reply:</span>
                            {{ nl2br($each_rate->getString("response")) }}
                        <?php } ?>
                            
                        <span class="text-right">
                            <a href="javascript:void(0);" onclick="showReply(<?php echo $each_rate->id; ?>);" id="link_reply{{ $each_rate->getNumber("id") }}">
                           <?php echo ($each_rate->getString("response") ? "Edit Reply" : " Reply") ;?></a>
                 <a href="javascript:void(0);" onclick="hideReply({{ $each_rate->getNumber("id") }});" id="cancel_reply<?php echo $each_rate->getNumber("id"); ?>" style="display:none;">Cancel</a>
                        </span>
                            
                    </q>
                    
                    <div class="replythis" style="display:none;" id="review_reply<?php echo $each_rate->getNumber("id"); ?>">
                        
             <form name="formReply<?php echo $each_rate->getNumber("id"); ?>" id="formReply<?php echo $each_rate->getNumber("id"); ?>" method="post" action="javascript:void(0);">
                            
         <p class="successMessage" id="msgReviewS<?php echo $each_rate->getNumber("id"); ?>" style="display:none">Reply successfully sent!</p>
                            <p class="errorMessage" id="msgReviewE<?php echo $each_rate->getNumber("id"); ?>" style="display:none">Please type a valid reply!</p>
                            
                             <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="item_id" value="<?php echo $each_rate->getNumber("item_id"); ?>" />
                            <input type="hidden" name="item_type" value="  <?php echo $each_rate->getNumber("item_type"); ?>" />
                            <input type="hidden" name="idReview" value="<?php echo $each_rate->getNumber("id"); ?>" />
                            <input type="hidden" name="ajax_type" value="review_reply" />
                            
                            <div>
                                <label for="textarea">Write Reply</label>
 <textarea cols="40" rows="8" name="reply" id="reply<?php echo $each_rate->getNumber("id"); ?>"><?php echo $each_rate->getString("response"); ?></textarea>
                            </div>
                            
                            <div class="text-right">
     <button type="button" name="submit" id="submitReply<?php echo $each_rate->getNumber("id"); ?>"" onclick="saveReply(<?php echo $each_rate->getNumber("id"); ?>);" class="btn btn-success">Submit</button>
                            </div>
                            
                        </form>
                        
                    </div>
                   
                    


                </div>

            </div>              



<?php
  $countReview++;
            }

  if ($countReview > ($maxItems + 1)) { ?>
                
                <div class="viewmore">
                    <a id="linkMorereviews" href="javascript:void(0);" onclick="showmore('item-review_', 'linkMorereviews', {{ $countReview }}, {{ $maxItems }});">View More</a>
                    <input type="hidden" id="item-review_" value="{{ $maxItems }}" />
                </div>
                
            <?php } ?>

 
        </section>

        <?php } 

          if ($item_hasemail && $item_status == "A") { ?>
        
        <section class="reviews-list" id="leads-list">

         <h2>Leads</h2>

     <div class="row-fluid head">                
               
                <div class="span9">
                  <p>
                        <a href="<?php echo $shareFacebook; ?>" target="_blank"><i class="socialicon social-facebook"></i></a>
                        <a href="<?php echo $shareTwitter; ?>" target="_blank"><i class="socialicon social-twitter"></i></a>
                        Share and get more leads
                    </p>
                </div>

                  <div class='span{{ ($item_status == "A" ? "3" : "12") }}'>
 <p class="paging">{{ count($leadsArr) }} Total {{ (count($leadsArr) == 1 ? "Lead" : "Leads") }}</p>
                </div>  
                 
            </div>

            <?php
             $countLead = 1;
            if ($leadsArr) foreach($leadsArr as $each_lead) {

                            //d($each_lead->reply_date);die;


                $auxMessage = @unserialize($each_lead->message);
                if (is_array($auxMessage)) {
                    $each_lead->message = "";
                    foreach ($auxMessage as $key => $value) {
                        $each_lead->message .= (defined($key) ? config('params'.$key) : $key).($value ? ": ".$value : "")."\n";
                    }
                }
                
                $replied = false;
                if ($each_lead->reply_date != "0000-00-00 00:00:00") {
                    $replied = true;
                   $titleIco = "Replied"." (".Functions::format_date($each_lead->reply_date, config('params.DEFAULT_DATE_FORMAT'), "datestring").")";//datestring
                }
                $titleIcoToday = "Replied"."  (".Functions::format_date(date("Y")."-".date("m")."-".date("d"), config('params.DEFAULT_DATE_FORMAT'), "date").")";//datestring

                $lead_name = $each_lead->first_name.($each_lead->last_name ? " ".$each_lead->last_name : "");


                ?>

  <div class="item-review" id="item-lead_{{ $countLead }}" style=<?php echo $countLead > $maxItems ? "display:none;" : "" ?>>
                
                <div class="review-summary <?php echo ($each_lead->new == "y" ? "new" : "");?>" onclick="leadBox('show', {{ $each_lead->id }});" id="lead-summary-{{ $each_lead->id}}">
                    
                    <a href="javascript:void(0);" class="pull-right">View</a>  
                    
                    <b>{{ $lead_name }}</b>
                                        
                    <time> {{ ($each_lead->entered) ? Functions::format_date($each_lead->entered, config('params.DEFAULT_DATE_FORMAT'), "datestring") : "N/A" }}</time>
               
                    <span class="text-highlight">{{ $each_lead->subject }}</span>
                    
                </div>
                
                <div class="review-detail" id="lead-detail-{{ $each_lead->id }}" style="display:none">
                    
                    <a href="javascript:void(0);" onclick="leadBox('hide', {{ $each_lead->id }});" class="pull-right">Hide</a>
                                       
                    <?php if ($replied) { ?>
                        <p id="title_replied{{ $each_lead->id }}" class="text-success">{{ Functions::system_showText($titleIco) }}</p>
                    <?php } ?>

                    <p id="new_replied{{ $each_lead->id }}" style="display:none;" class="text-success">{{ $titleIcoToday }}</p>
                    
                    <p>From <b>{{ $lead_name }}</b></p>
                    
                    <time>{{ ($each_lead->entered) ? Functions::format_date($each_lead->entered, config('params.DEFAULT_DATE_FORMAT'), "date") : "N/A" }}</time>
                    
                    
                    <p class="review-text"><?php
                    $message=$each_lead->message;
                    $message = str_replace("LANG_LABEL_MESSAGE",'Message',$message);
                     echo nl2br($message); ?></p>

                    <q class="review-reply">
                      
                        <span class="text-right">
                            <a href="javascript:void(0);" onclick="showLead({{ $each_lead->id }});" id="link_lead{{ $each_lead->id }}">Reply</a>
                            <a href="javascript:void(0);" onclick="hideLead({{ $each_lead->id }});" id="cancel_lead{{ $each_lead->id }}" style="display:none;">Cancel</a>
                        </span>
                            
                    </q>
                    
                    <div class="replythis" style="display:none;" id="lead_reply{{ $each_lead->id }}">
                        
                        <form name="formLead{{ $each_lead->id }}" id="formLead{{ $each_lead->id }}" method="post" action="javascript:void(0);">
                            
                            <p class="successMessage" id="msgLeadS{{ $each_lead->id }}" style="display:none">Replied</p>
                            <p class="errorMessage" id="msgLeadE{{ $each_lead->id }}" style="display:none"></p>
                        
                             <input type="hidden" name="_token" value="{{ csrf_token() }}">
   
                            <input type="hidden" name="item_id" value="{{ $item_id }}" />
                            <input type="hidden" name="item_type" value="{{ $item_type }}" />
                            <input type="hidden" name="type" value="{{ $item_type }}" />
                            <input type="hidden" name="idLead" value="{{ $each_lead->id }}" />
                            <input type="hidden" name="action" value="reply" />
                            <input type="hidden" name="ajax_type" value="lead_reply" />

                            <label>
                                <p>To </p>
          <input type="email" name="to" value="<?php echo (isset($to) && isset($idLead) == $each_lead->id ? $to : $each_lead->email);?>" />
                            </label>

                            <label>
                                <p>Message:</p>
                                <textarea name="message" rows="5"><?php echo  (isset($message)  && isset($idLead) == $each_lead->id ? $message : "");?></textarea>
                            </label>

                            <div class="admin-actions">
                                <button type="button" name="submit" id="submitLead{{ $each_lead->id }}" onclick="saveLead({{ $each_lead->id }});" class="btn btn-success">Submit</button>
                            </div>

                        </form>
                        
                    </div>
                    
                </div>

            </div>

   <?php  $countLead++; 
    } 

 if ($countLead > ($maxItems + 1)) {
    ?>
      <div class="viewmore">
    <a id="linkMoreleads" href="javascript:void(0);" onclick="showmore('item-lead_', 'linkMoreleads', {{ $countLead }}, {{ $maxItems }});">View More</a>
                    <input type="hidden" id="item-lead_" value="{{ $maxItems }}" />
                </div>
                
            <?php } ?>

            <?php if ($item_status == "A") { ?>            
                <h5><a href="<?php echo url('rentalleadlist'); ?>" target="_blank">Access Rental Lead List</a></h5>
            <?php } ?>

         
                </section>

        <?php } ?>
        
        
    </div>
<script type="text/javascript">

 
     <?php 
        $count = 0;
        foreach ($avReports as $avReport) { ?>

            var {{ $avReport }} = {
                    fillColor : "rgba(<?php echo $avReportsColors[$count]; ?>,0.1)",
                    strokeColor : "rgba(<?php echo $avReportsColors[$count]; ?>,0.3)",
                    pointColor : "rgba(<?php echo $avReportsColors[$count]; ?>,1)",
                    pointStrokeColor : "#fff",

                    data : <?php echo ${"data_".$avReport}; ?>
                };

        <?php
        $count++;
        } ?>
               
       var chartLabels = [<?php echo $strLabel; ?>];
        var initialReport = [<?php echo $initialReport; ?>];
        var maxInitialReport =<?php echo $maxInitialReport; ?>;


        
    </script>