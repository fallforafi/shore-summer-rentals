<?php
if (config('params.PAYMENT_FEATURE') != "on") { exit; }
	if ((config('params.CREDITCARDPAYMENT_FEATURE') != "on") && (config('params.INVOICEPAYMENT_FEATURE') != "on")) { exit; }

?>
@extends('front')

@section('content')

	<link rel="stylesheet" href="{{ asset('front/css/bootstrap.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('front/css/members.min.css') }}" />
	<link href="https://www.shoresummerrentals.com/custom/domain_1/theme/default/content_custom.min.css" rel="stylesheet" type="text/css" media="all">
    <!--link rel="stylesheet" href="{{ asset('front/css/bootstrap-responsive.min.css') }}" /-->
     
<?php
use App\Functions\Functions;
use App\Functions\Listing;
use App\Functions\ListingLevel;
use App\Models\Domain\Sql;


?>     
    
    
	 <div class="billing-page-container container members ">

        <div class="container-fluid">

        <div class="row-fluid">
            
            <nav class="minor-nav">
                
                <ul>
                    <li>
                        <a class="active"  href="<?php echo url('sponsors/billing'); ?>">Check Out</a>
                    </li>
                    <li>
                        <a href="<?php echo url('sponsors/transactions'); ?>">Transaction History</a>
                    </li>
                </ul>
                
            </nav>
            
        </div>

    
	<div class="row-fluid">
		<h5 class="text-right"><a href="<?php echo url('advertise'); ?>" target="_blank" class="">View Membership Details</a></h5>		
		<h5 class="text-right"><a href="<?php echo url('contact-us'); ?>" target="_blank" class="">Contact us to downgrade and/or for multiple property discounts</a></h5>

								
		
  <form name="pay" id="pay"  action="<?php echo url('sponsors/billing/pay'); ?>" method="post" enctype="multipart/form-data">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">	


		<script>

			function toggleAll(obj, lnk){

				var value = obj.checked;
				var trElements = document.getElementsByTagName('tr');

				if(lnk == true){
					if(value == true){
						obj.checked = false;
						value = false;
					} else {
						obj.checked = true;
						value = true;
					}
				}

				for(i=0; i < trElements.length ; i++){
					for(j=0; j < trElements.item(i).childNodes.length; j++){
						if(trElements.item(i).childNodes[j].firstChild) {
							if(trElements.item(i).childNodes[j].firstChild.id == "listing_id[]"){
								if(value == true) {
									trElements.item(i).childNodes[j].firstChild.checked = true;
									trElements.item(i).childNodes[j].firstChild.parentNode.parentNode.className = 'bg-tablebilling-active';
									for(x=0; x < trElements.item(i).childNodes[j].firstChild.parentNode.parentNode.childNodes.length; x++) {
										if(trElements.item(i).childNodes[j].firstChild.parentNode.parentNode.childNodes[x].firstChild){
											if(trElements.item(i).childNodes[j].firstChild.parentNode.parentNode.childNodes[x].firstChild.name == "discountlisting_id[]"){
												trElements.item(i).childNodes[j].firstChild.parentNode.parentNode.childNodes[x].firstChild.disabled= false;
											}
										}
									}
								} else {
									trElements.item(i).childNodes[j].firstChild.checked = false;
									trElements.item(i).childNodes[j].firstChild.parentNode.parentNode.className = 'bg-tablebilling-inactive';
									for(x=0; x < trElements.item(i).childNodes[j].firstChild.parentNode.parentNode.childNodes.length; x++) {
										if(trElements.item(i).childNodes[j].firstChild.parentNode.parentNode.childNodes[x].firstChild){
											if(trElements.item(i).childNodes[j].firstChild.parentNode.parentNode.childNodes[x].firstChild.name == "discountlisting_id[]"){
												trElements.item(i).childNodes[j].firstChild.parentNode.parentNode.childNodes[x].firstChild.disabled= true;
											}
										}
									}
								}
							}
							if(trElements.item(i).childNodes[j].firstChild.id == "badge_id[]"){
								if(value == true) {
									trElements.item(i).childNodes[j].firstChild.checked = true;
									trElements.item(i).childNodes[j].firstChild.parentNode.parentNode.className = 'bg-tablebilling-active';
								} else {
									trElements.item(i).childNodes[j].firstChild.checked = false;
									trElements.item(i).childNodes[j].firstChild.parentNode.parentNode.className = 'bg-tablebilling-inactive';
								}
							}
							if(trElements.item(i).childNodes[j].firstChild.id == "event_id[]"){
								if(value == true) {
									trElements.item(i).childNodes[j].firstChild.checked = true;
									trElements.item(i).childNodes[j].firstChild.parentNode.parentNode.className = 'bg-tablebilling-active';
									for(x=0; x < trElements.item(i).childNodes[j].firstChild.parentNode.parentNode.childNodes.length; x++) {
										if(trElements.item(i).childNodes[j].firstChild.parentNode.parentNode.childNodes[x].firstChild){
											if(trElements.item(i).childNodes[j].firstChild.parentNode.parentNode.childNodes[x].firstChild.name == "discountevent_id[]"){
												trElements.item(i).childNodes[j].firstChild.parentNode.parentNode.childNodes[x].firstChild.disabled= false;
											}
										}
									}
								} else {
									trElements.item(i).childNodes[j].firstChild.checked = false;
									trElements.item(i).childNodes[j].firstChild.parentNode.parentNode.className = 'bg-tablebilling-inactive';
									for(x=0; x < trElements.item(i).childNodes[j].firstChild.parentNode.parentNode.childNodes.length; x++) {
										if(trElements.item(i).childNodes[j].firstChild.parentNode.parentNode.childNodes[x].firstChild){
											if(trElements.item(i).childNodes[j].firstChild.parentNode.parentNode.childNodes[x].firstChild.name == "discountevent_id[]"){
												trElements.item(i).childNodes[j].firstChild.parentNode.parentNode.childNodes[x].firstChild.disabled= true;
											}
										}
									}
								}
							}
							if(trElements.item(i).childNodes[j].firstChild.id == "banner_id[]"){
								if(value == true) {
									trElements.item(i).childNodes[j].firstChild.checked = true;
									trElements.item(i).childNodes[j].firstChild.parentNode.parentNode.className = 'bg-tablebilling-active';
									for(x=0; x < trElements.item(i).childNodes[j].firstChild.parentNode.parentNode.childNodes.length; x++) {
										if(trElements.item(i).childNodes[j].firstChild.parentNode.parentNode.childNodes[x].firstChild){
											if(trElements.item(i).childNodes[j].firstChild.parentNode.parentNode.childNodes[x].firstChild.name == "discountbanner_id[]"){
												trElements.item(i).childNodes[j].firstChild.parentNode.parentNode.childNodes[x].firstChild.disabled= false;
											}
										}
									}
								} else {
									trElements.item(i).childNodes[j].firstChild.checked = false;
									trElements.item(i).childNodes[j].firstChild.parentNode.parentNode.className = 'bg-tablebilling-inactive';
									for(x=0; x < trElements.item(i).childNodes[j].firstChild.parentNode.parentNode.childNodes.length; x++) {
										if(trElements.item(i).childNodes[j].firstChild.parentNode.parentNode.childNodes[x].firstChild){
											if(trElements.item(i).childNodes[j].firstChild.parentNode.parentNode.childNodes[x].firstChild.name == "discountbanner_id[]"){
												trElements.item(i).childNodes[j].firstChild.parentNode.parentNode.childNodes[x].firstChild.disabled= true;
											}
										}
									}
								}
							}
							if(trElements.item(i).childNodes[j].firstChild.id == "classified_id[]"){
								if(value == true) {
									trElements.item(i).childNodes[j].firstChild.checked = true;
									trElements.item(i).childNodes[j].firstChild.parentNode.parentNode.className = 'bg-tablebilling-active';
									for(x=0; x < trElements.item(i).childNodes[j].firstChild.parentNode.parentNode.childNodes.length; x++) {
										if(trElements.item(i).childNodes[j].firstChild.parentNode.parentNode.childNodes[x].firstChild){
											if(trElements.item(i).childNodes[j].firstChild.parentNode.parentNode.childNodes[x].firstChild.name == "discountclassified_id[]"){
												trElements.item(i).childNodes[j].firstChild.parentNode.parentNode.childNodes[x].firstChild.disabled= false;
											}
										}
									}
								} else {
									trElements.item(i).childNodes[j].firstChild.checked = false;
									trElements.item(i).childNodes[j].firstChild.parentNode.parentNode.className = 'bg-tablebilling-inactive';
									for(x=0; x < trElements.item(i).childNodes[j].firstChild.parentNode.parentNode.childNodes.length; x++) {
										if(trElements.item(i).childNodes[j].firstChild.parentNode.parentNode.childNodes[x].firstChild){
											if(trElements.item(i).childNodes[j].firstChild.parentNode.parentNode.childNodes[x].firstChild.name == "discountclassified_id[]"){
												trElements.item(i).childNodes[j].firstChild.parentNode.parentNode.childNodes[x].firstChild.disabled= true;
											}
										}
									}
								}
							}
							if(trElements.item(i).childNodes[j].firstChild.id == "article_id[]"){
								if(value == true) {
									trElements.item(i).childNodes[j].firstChild.checked = true;
									trElements.item(i).childNodes[j].firstChild.parentNode.parentNode.className = 'bg-tablebilling-active';
									for(x=0; x < trElements.item(i).childNodes[j].firstChild.parentNode.parentNode.childNodes.length; x++) {
										if(trElements.item(i).childNodes[j].firstChild.parentNode.parentNode.childNodes[x].firstChild){
											if(trElements.item(i).childNodes[j].firstChild.parentNode.parentNode.childNodes[x].firstChild.name == "discountarticle_id[]"){
												trElements.item(i).childNodes[j].firstChild.parentNode.parentNode.childNodes[x].firstChild.disabled= false;
											}
										}
									}
								} else {
									trElements.item(i).childNodes[j].firstChild.checked = false;
									trElements.item(i).childNodes[j].firstChild.parentNode.parentNode.className = 'bg-tablebilling-inactive';
									for(x=0; x < trElements.item(i).childNodes[j].firstChild.parentNode.parentNode.childNodes.length; x++) {
										if(trElements.item(i).childNodes[j].firstChild.parentNode.parentNode.childNodes[x].firstChild){
											if(trElements.item(i).childNodes[j].firstChild.parentNode.parentNode.childNodes[x].firstChild.name == "discountarticle_id[]"){
												trElements.item(i).childNodes[j].firstChild.parentNode.parentNode.childNodes[x].firstChild.disabled= true;
											}
										}
									}
								}
							}
							if(trElements.item(i).childNodes[j].firstChild.id == "custom_invoice_id[]"){
								if(value == true) {
									trElements.item(i).childNodes[j].firstChild.checked = true;
									trElements.item(i).childNodes[j].firstChild.parentNode.parentNode.className = 'bg-tablebilling-active';
								} else {
									trElements.item(i).childNodes[j].firstChild.checked = false;
									trElements.item(i).childNodes[j].firstChild.parentNode.parentNode.className = 'bg-tablebilling-inactive';
								}
							}
						}
					}
				}
			}

			function toggleLinebyChkBox(obj){
				if (obj.checked == true){
					obj.parentNode.parentNode.className = 'bg-tablebilling-active';
					for(x=0; x < obj.parentNode.parentNode.childNodes.length; x++){
						if(obj.parentNode.parentNode.childNodes[x].firstChild){
                            console.log(obj.parentNode.parentNode.childNodes[x].firstChild.name+" checked");
							if ((obj.parentNode.parentNode.childNodes[x].firstChild.name == "discountlisting_id[]") || (obj.parentNode.parentNode.childNodes[x].firstChild.name == "discountevent_id[]") || (obj.parentNode.parentNode.childNodes[x].firstChild.name == "discountbanner_id[]") || (obj.parentNode.parentNode.childNodes[x].firstChild.name == "discountclassified_id[]") || (obj.parentNode.parentNode.childNodes[x].firstChild.name == "discountarticle_id[]") || (obj.parentNode.parentNode.childNodes[x].firstChild.name == "discountbadge_id[]")) {
								obj.parentNode.parentNode.childNodes[x].firstChild.disabled= false;
							}
						}
					}
				} else {
					obj.parentNode.parentNode.className = 'bg-tablebilling-inactive';
					for(x=0; x < obj.parentNode.parentNode.childNodes.length; x++){
						if(obj.parentNode.parentNode.childNodes[x].firstChild){
                            console.log(obj.parentNode.parentNode.childNodes[x].firstChild.name+" unchecked");
							if ((obj.parentNode.parentNode.childNodes[x].firstChild.name == "discountlisting_id[]") || (obj.parentNode.parentNode.childNodes[x].firstChild.name == "discountevent_id[]") || (obj.parentNode.parentNode.childNodes[x].firstChild.name == "discountbanner_id[]") || (obj.parentNode.parentNode.childNodes[x].firstChild.name == "discountclassified_id[]") || (obj.parentNode.parentNode.childNodes[x].firstChild.name == "discountarticle_id[]") || (obj.parentNode.parentNode.childNodes[x].firstChild.name == "discountbadge_id[]")) {
								obj.parentNode.parentNode.childNodes[x].firstChild.disabled= true;
							}
						}
					}
				}
			}

			function toggleLine(obj){
				for(i=0; i < obj.childNodes.length; i++){
					if(obj.childNodes[i].firstChild) {
						if(obj.childNodes[i].firstChild.id == "badge_id[]") {
							if(obj.childNodes[i].firstChild.checked == true) {
								obj.className = 'bg-tablebilling-inactive';
								obj.childNodes[i].firstChild.checked = false;
							} else {
								obj.childNodes[i].firstChild.checked = true;
								obj.className = 'bg-tablebilling-active';
							}
						}
						if(obj.childNodes[i].firstChild.id == "listing_id[]") {
							if(obj.childNodes[i].firstChild.checked == true) {
								obj.className = 'bg-tablebilling-inactive';
								obj.childNodes[i].firstChild.checked = false;
								for(x=0; x < obj.childNodes.length; x++){
									if(obj.childNodes[x].firstChild){
										if(obj.childNodes[x].firstChild.name == "discountlisting_id[]"){
											obj.childNodes[x].firstChild.disabled= true;
										}
									}
								}
							} else {
								obj.childNodes[i].firstChild.checked = true;
								obj.className = 'bg-tablebilling-active';
								for(x=0; x < obj.childNodes.length; x++){
									if(obj.childNodes[x].firstChild){
										if(obj.childNodes[x].firstChild.name == "discountlisting_id[]"){
											obj.childNodes[x].firstChild.disabled= false;
										}
									}
								}
							}
						}
						if(obj.childNodes[i].firstChild.id == "event_id[]") {
							if(obj.childNodes[i].firstChild.checked == true) {
								obj.className = 'bg-tablebilling-inactive';
								obj.childNodes[i].firstChild.checked = false;
								for(x=0; x < obj.childNodes.length; x++){
									if(obj.childNodes[x].firstChild){
										if(obj.childNodes[x].firstChild.name == "discountevent_id[]"){
											obj.childNodes[x].firstChild.disabled= true;
										}
									}
								}
							} else {
								obj.childNodes[i].firstChild.checked = true;
								obj.className = 'bg-tablebilling-active';
								for(x=0; x < obj.childNodes.length; x++){
									if(obj.childNodes[x].firstChild){
										if(obj.childNodes[x].firstChild.name == "discountevent_id[]"){
											obj.childNodes[x].firstChild.disabled= false;
										}
									}
								}
							}
						}
						if(obj.childNodes[i].firstChild.id == "banner_id[]") {
							if(obj.childNodes[i].firstChild.checked == true) {
								obj.className = 'bg-tablebilling-inactive';
								obj.childNodes[i].firstChild.checked = false;
								for(x=0; x < obj.childNodes.length; x++){
									if(obj.childNodes[x].firstChild){
										if(obj.childNodes[x].firstChild.name == "discountbanner_id[]"){
											obj.childNodes[x].firstChild.disabled= true;
										}
									}
								}
							} else {
								obj.childNodes[i].firstChild.checked = true;
								obj.className = 'bg-tablebilling-active';
								for(x=0; x < obj.childNodes.length; x++){
									if(obj.childNodes[x].firstChild){
										if(obj.childNodes[x].firstChild.name == "discountbanner_id[]"){
											obj.childNodes[x].firstChild.disabled= false;
										}
									}
								}
							}
						}
						if(obj.childNodes[i].firstChild.id == "classified_id[]") {
							if(obj.childNodes[i].firstChild.checked == true) {
								obj.className = 'bg-tablebilling-inactive';
								obj.childNodes[i].firstChild.checked = false;
								for(x=0; x < obj.childNodes.length; x++){
									if(obj.childNodes[x].firstChild){
										if(obj.childNodes[x].firstChild.name == "discountclassified_id[]"){
											obj.childNodes[x].firstChild.disabled= true;
										}
									}
								}
							} else {
								obj.childNodes[i].firstChild.checked = true;
								obj.className = 'bg-tablebilling-active';
								for(x=0; x < obj.childNodes.length; x++){
									if(obj.childNodes[x].firstChild){
										if(obj.childNodes[x].firstChild.name == "discountclassified_id[]"){
											obj.childNodes[x].firstChild.disabled= false;
										}
									}
								}
							}
						}
						if(obj.childNodes[i].firstChild.id == "article_id[]") {
							if(obj.childNodes[i].firstChild.checked == true) {
								obj.className = 'bg-tablebilling-inactive';
								obj.childNodes[i].firstChild.checked = false;
								for(x=0; x < obj.childNodes.length; x++){
									if(obj.childNodes[x].firstChild){
										if(obj.childNodes[x].firstChild.name == "discountarticle_id[]"){
											obj.childNodes[x].firstChild.disabled= true;
										}
									}
								}
							} else {
								obj.childNodes[i].firstChild.checked = true;
								obj.className = 'bg-tablebilling-active';
								for(x=0; x < obj.childNodes.length; x++){
									if(obj.childNodes[x].firstChild){
										if(obj.childNodes[x].firstChild.name == "discountarticle_id[]"){
											obj.childNodes[x].firstChild.disabled= false;
										}
									}
								}
							}
						}
						if(obj.childNodes[i].firstChild.id == "custom_invoice_id[]") {
							if(obj.childNodes[i].firstChild.checked == true) {
								obj.className = 'bg-tablebilling-inactive';
								obj.childNodes[i].firstChild.checked = false;
							} else {
								obj.childNodes[i].firstChild.checked = true;
								obj.className = 'bg-tablebilling-active';
							}
						}
					}
				}
			}

		</script>
		

		<?php
		if (isset($bill_info["listings"])) {
		?>
			<table border="0" cellpadding="2" cellspacing="2" class="standard-tableTOPBLUE">

				<tr>
					<th width="30">Pay</th>
                    <th>Listing Name</th>
                    <th width="100">Level</th>
                    <th width="100">Cost</th>
					<th width="140">Promotional Code</th>
					<th width="100">Promo credit</th>
					<th width="70">Renewal</th>
				</tr>

				<?php
				foreach($bill_info['listings'] as $id=>$info){
				$renewal_date_style = ($info["needtocheckout"] == "y") ? "color: #e67e22" : "";		

				?>
				<tr class="bg-tablebilling-inactive">

					<td><input type="checkbox" id="listing_id[]" name="listing_id[]" value="{{ $id }} "  onclick="toggleLinebyChkBox(this)"   class="inputCheck" /></td>
					 <td style="text-align: left; cursor: pointer; {{ $renewal_date_style }}" onclick="toggleLine(this.parentNode)"># {{ $id }} - {{ $info["address"] }} </td>

					 <td style="cursor: pointer; {{ $renewal_date_style }}" onclick="toggleLine(this.parentNode)">{{ Functions::string_ucwords($info["level"]) }} </td>


				   <td style="cursor: pointer;  {{ $renewal_date_style }}" onclick="toggleLine(this.parentNode)">{{config('params.CURRENCY_SYMBOL').$info["total_fee"] }}</td>

					<?php if (config('params.PAYMENT_FEATURE') == "on") { ?>
							<?php if ((config('params.CREDITCARDPAYMENT_FEATURE') == "on") || (  config('params.INVOICEPAYMENT_FEATURE')== "on")) { ?>
							
						 <td style="cursor: pointer; color:  {{ $renewal_date_style }}"><input type="text" class="discountField" id="listing_{{ $id }}" name="discountlisting_id[]" value="{{ $info['discount_id']}}" disabled style="width:60px;font-size:10px" />		


							<?php } ?>
						<?php } ?>	


					 <td style="cursor: pointer; {{ $renewal_date_style }}" onclick="toggleLine(this.parentNode)" class="discount_amount_listing_{{ $id }}">
                            <?php if($info["discount_type"] === 'monetary value'):?>
                                - {{ config('params.CURRENCY_SYMBOL').$info["discount_amount"] }}
                            <?php elseif($info["discount_type"] === 'percentage'):?>
								<?php
								$price = floatval($info["total_fee"])/((100-floatval($info["discount_amount"]))/100)-$info['total_fee'];
								?>
								-{{config('params.CURRENCY_SYMBOL').number_format($price, 2) }}

                            <?php endif;?>
                        </td>


						<td style="cursor: pointer; {{ $renewal_date_style }}" onclick="toggleLine(this.parentNode)">{{ ($info["renewal_date"]== "0000-00-00")?'New':Functions::format_date($info["renewal_date"]) }}</td>
					</tr>
					<?php }	?>	
				</table>
				<?php }	?>
						

		<?php
		if (isset($bill_info["badges"])) {
		?>		
		<table border="0" cellpadding="2" cellspacing="2" class="standard-tableTOPBLUE">

		<table border="0" cellpadding="2" cellspacing="2" class="standard-tableTOPBLUE listingSpecials-table">
				<tr>
					<th width="30">Pay</th>
					<th>Listing Specials</th>
                    <th width="100">Cost</th>
                   	<th width="140">Promotional Code</th>
                   	<th width="100">Promo credit</th>
					<th width="70">Renewal</th>
				</tr>

				<?php
				foreach($bill_info["badges"] as $id => $info){
				$renewal_date_style = ($info["needtocheckout"] == "y") ? "color: #e67e22" : "";		
					//$checked = ($info["needtocheckout"] == "y") ? "checked" : "";

					// Kash changes//15_07_2016
					//free badges should not display request by maria Krik
					$temp_badge_array = array ("1", "8", "10");
					if(in_array($info['editor_id'],$temp_badge_array))
					{
							continue;
					}
					?>
			<tr class="bg-tablebilling-inactive">

						<td><input type="checkbox" id="badge_id[]" name="badge_id[]" value="{{ $id }}"  onclick="toggleLinebyChkBox(this)" class="inputCheck" /></td>
						<td style="text-align: left; cursor: pointer;{{ $renewal_date_style }} " onclick="toggleLine(this.parentNode)">{{ $info["listing_id"]  }} - {{ $info["name"] }}</td>

						  <td style="cursor: pointer;{{ $renewal_date_style }} " onclick="toggleLine(this.parentNode)">{{config('params.CURRENCY_SYMBOL').$info["total_fee"] }}</td>

						  <?php if (config('params.PAYMENT_FEATURE') == "on") { ?>
							<?php if ((config('params.CREDITCARDPAYMENT_FEATURE') == "on") || (  config('params.INVOICEPAYMENT_FEATURE')== "on")) { ?>
							
						 <td style="cursor: pointer; {{ $renewal_date_style }}"><input type="text" class="discountField" id="ListingChoice_{{ $id }}" name="discountbadge_id[]" value="{{ $info['discount_id']}}" disabled style="width:60px;font-size:10px" />		


							<?php } ?>
						<?php } ?>	

							 <td style="cursor: pointer; {{ $renewal_date_style }}" onclick="toggleLine(this.parentNode)" class="discount_amount_listing_{{ $id }}">
                            <?php if($info["discount_type"] === 'monetary value'):?>
                                - {{ config('params.CURRENCY_SYMBOL').$info["discount_amount"] }}
                            <?php elseif($info["discount_type"] === 'percentage'):?>
								<?php
								$price = floatval($info["total_fee"])/((100-floatval($info["discount_amount"]))/100)-$info['total_fee'];
								?>
								-{{config('params.CURRENCY_SYMBOL').number_format($price, 2) }}

                            <?php endif;?>
                        </td>


					

						<td style="cursor: pointer; {{ $renewal_date_style }}" onclick="toggleLine(this.parentNode)">{{ ($info["renewal_date"]== "0000-00-00")?'New':Functions::format_date($info["renewal_date"]) }}</td>
					</tr>
					<?php }	?>	
				</table>
				<?php }	?>	

				<?php
		if (isset($bill_info["custominvoices"])) {
		?>		
		<table border="0" cellpadding="2" cellspacing="2" class="standard-table">
				<tr>
					<th class="standard-tabletitle">Pay for outstanding invoices</th>
				</tr>
			</table>

			<table border="0" cellpadding="2" cellspacing="2" class="standard-tableTOPBLUE">
			<tr>
					<th width="30">Pay</th>
					<th>Title</th>
					<th width="100">Items</th>
					<th width="140">Amount</th>
					<th width="70">Date</th>
				</tr>

				<?php
				/* all checked by default */
				$checked = true;

				foreach($bill_info["custominvoices"] as $id => $info){
					?>

				<tr class="<?php echo (($checked) ? ("bg-tablebilling-active"): ("bg-tablebilling-inactive"))?>">
				<td><input type="checkbox" id="custom_invoice_id[]" name="custom_invoice_id[]" value="{{ $id }}" checked="checked" onclick="toggleLinebyChkBox(this)" class="inputCheck" /></td>
						<td style="text-align: left; cursor: pointer;" onclick="toggleLine(this.parentNode)">{{ $info["title"] }}</td>
		<td><a href="<?php echo url('custominvoice_items/'.$info["id"]); ?>" class="link-table iframe fancy_window_custom" style="text-decoration: underline;">View Items</a></td>
			<td style="cursor: pointer;" onclick="toggleLine(this.parentNode)">{{ $info["subtotal"] }}</td>
			<td style="cursor: pointer;" onclick="toggleLine(this.parentNode)">{{ Functions::format_date($info["date"]) }}</td>
					</tr>			
		
					<?php }	?>	
				</table>
				<?php }	?>	










								</table>

			
		<div class="payment-options">
			<div>
			<input type="radio" name="payment_method" value="authorize" id="radio1"  /><label for="radio1">By Credit Card</label>
			</div>

			<div>
			<input type="radio" name="payment_method" value="paypal" id="radio2"><label for="radio2">By PayPal</label>
			</div>

			<div>
			<input type="radio" name="payment_method" value="invoice" id="radio3"  /><label for="radio3">Print Invoice and Mail a Check</label>
			</div>		
		</div>

		<br />
        
        <div class="baseButtons">
			<p class="standardButton">
				<input type="hidden" name="second_step" id="second_step" value="1" style="display: none" />
				<button type="submit">Next</button>
			</p>
			<br />
			<br />
            <script type="text/javascript" src="https://seal.thawte.com/getthawteseal?host_name=www.shoresummerrentals.com&amp;size=S&amp;lang=en"></script>
        </div>
		<script>

			function Discount() {
				this.id = '';
				this.target = '';
				this.code = '';
				this.selector = ".discountField";
				this.timeout = '';
				this.requestId = '';
			}
			Discount.prototype = {
				init: function () {
					var self = this;
					$(this.selector).bind('change keyup input propertychange', function (event) {
						var $element = $(this);
						$(".discount_amount_" + $element.attr("id")).html("");
						if (self.timeout != '') {
							clearTimeout(self.timeout);
							self.timeout = '';
						}
						if ($element.val().length > 0) {
							self.timeout = setTimeout(function () {
								self.id = $element.attr("id");
								self.target = ".discount_amount_" + self.id;
								self.code = $element.val();
								self.requestId = new Date().getTime();
								self.loadData();
							}, 1500);
						}

					});
				},

				loadData: function () {
					if (this.code.length > 0) {
						this.requestInProgress = true;
						var self = this;
						$.post(
							"<?php echo url('sponsors/billing/check_promo'); ?>",
							{
								_token: '<?php echo csrf_token();  ?>',
								code: this.code,
								target: this.id,
								requestId: this.requestId
							},
							function (response) {
								if(response.requestId==self.requestId) {
									if (typeof response.message != 'undefined') {
										$(self.target).html(response.message);
									}
									if (typeof response.amount != 'undefined') {
										$(self.target).html(response.amount);
									}
								}
							},
							'json');} else {
						$(this.target).html("");
					}
				}
			};

			$(document).ready(function(){
				var discount = new Discount();
				discount.init();
			})
		</script>

				</form>
	</div>


            </div><!-- Close container-fluid div -->
            
        </div><!-- Close container div -->




		 <script type="text/javascript">
        
        if ($("#myChart").length) {
            //This will get the first returned node in the jQuery collection.
            var ctx = $("#myChart").get(0).getContext("2d");
        }

        function initializeDashboard() {
            $(".dial").knob({
                readOnly:   true,
                fgColor:    "#2980b9",
                bgColor:    "#DEE1E3",
                fontWeight: 300,
                thickness:  .2,
                width:      70,
                height:     70
            });

            $(".status, .floating-tip, .alert-new, #item_renewal").tooltip({
                animation: true,
                placement: "top"
            });
             
            if ($("#myChart").length) {
                //Get context with jQuery - using jQuery's .get() method.
                ctx = $("#myChart").get(0).getContext("2d");
                loadChart();
            }
        }

        $(function() {
            $("#alert").fadeOut(5000);
            initializeDashboard();
        });
        
        function showReply(id) {
            $('#review_reply'+id).css('display', '');
            $('#link_reply'+id).css('display', 'none');
            $('#cancel_reply'+id).css('display', '');
        }
        
        function hideReply(id) {
            $('#review_reply'+id).css('display', 'none');
            $('#link_reply'+id).css('display', '');
            $('#cancel_reply'+id).css('display', 'none');
        }
        
        function showLead(id) {
            $('#lead_reply'+id).css('display', '');
            $('#link_lead'+id).css('display', 'none');
            $('#cancel_lead'+id).css('display', '');
        }
        
        function hideLead(id) {
            $('#lead_reply'+id).css('display', 'none');
            $('#link_lead'+id).css('display', '');
            $('#cancel_lead'+id).css('display', 'none');
        }
        
        function saveReply(id) {
            $("#submitReply"+id).css("cursor", "default");
            $("#submitReply"+id).prop("disabled", "disabled");
            $("#submitReply"+id).html('Wait, Loading...');
            
            $.post("https://www.shoresummerrentals.com/sponsors/ajax.php", $("#formReply"+id).serialize(), function(data) {
                if (data == "ok") {
                    $("#msgReviewE"+id).css("display", "none");
                    $("#msgReviewS"+id).css("display", "");
                    $("#msgReviewS"+id).fadeOut(5000);
                } else {
                    $("#msgReviewE"+id).css("display", "");
                    $("#msgReviewS"+id).css("display", "none");
                }
                $("#submitReply"+id).html('Submit');
                $("#submitReply"+id).prop("disabled", "");
                $("#submitReply"+id).css("cursor", "pointer");
            });
        }
        
        function saveLead(id) {
            $("#submitLead"+id).css("cursor", "default");
            $("#submitLead"+id).prop("disabled", "disabled");
            $("#submitLead"+id).html('Wait, Loading...');
            
            $.post("https://www.shoresummerrentals.com/sponsors/ajax.php", $("#formLead"+id).serialize(), function(data) {
                if (data == "ok") {
                    $("#msgLeadE"+id).css("display", "none");
                    $("#msgLeadS"+id).css("display", "");
                    $("#msgLeadS"+id).fadeOut(5000);
                    setTimeout("leadBox('hide', "+id+");", 4000);
                    $("#title_replied"+id).css("display", "none");
                    $("#new_replied"+id).css("display", "");
                } else {
                    $("#msgLeadE"+id).html(data);
                    $("#msgLeadE"+id).css("display", "");
                    $("#msgLeadS"+id).css("display", "none");
                }
                $("#submitLead"+id).html('Submit');
                $("#submitLead"+id).prop("disabled", "");
                $("#submitLead"+id).css("cursor", "pointer");
            });
        }
        
        function reviewBox(option, id) {
            $("#reviews-list").children(".item-review").children(".review-detail").stop(true,true).slideUp();
            $("#reviews-list").children(".item-review").children(".review-summary").stop(true,true).slideDown().removeClass("new");
            if (option == "show") {
                $("#review-summary-"+id).slideUp(); 
                $("#review-detail-"+id).slideDown();
                setItemAsViewed("review", id);
            } else {
                $("#review-summary-"+id).slideDown(); 
                $("#review-detail-"+id).slideUp();
            }            
        }
        
        function leadBox(option, id) {
            $("#leads-list").children(".item-review").children(".review-detail").stop(true,true).slideUp();
            $("#leads-list").children(".item-review").children(".review-summary").stop(true,true).slideDown().removeClass("new");
            if (option == "show") {
                $("#lead-summary-"+id).slideUp(); 
                $("#lead-detail-"+id).slideDown();
                setItemAsViewed("lead", id);
            } else {
                $("#lead-summary-"+id).slideDown(); 
                $("#lead-detail-"+id).slideUp();
            }            
        }
        
        function dealBox(option, id) {
            $("#deals-list").children(".item-review").children(".review-detail").stop(true,true).slideUp();
            $("#deals-list").children(".item-review").children(".review-summary").stop(true,true).slideDown();
            if (option == "show") {
                $("#deal-summary-"+id).slideUp(); 
                $("#deal-detail-"+id).slideDown();
            } else {
                $("#deal-summary-"+id).slideDown(); 
                $("#deal-detail-"+id).slideUp();
            }            
        }
        
        function changeDealStatus(option, id, promocode) {
            $.post("https://www.shoresummerrentals.com/sponsors/deal/deal.php",{action: option, promotion_id: promocode}, function() {
                if (option == "freeUpDeal") {
                    $("#label_used"+id).css("display", "");
                } else {
                    $("#label_used"+id).css("display", "none");
                }
            });
        }
        
        function setItemAsViewed(type, id) {
            $.post("https://www.shoresummerrentals.com/sponsors/ajax.php", {
                ajax_type: 'setItemAsViewed',
                type: type,
                id: id
            }, function () {});
        }
       
        function loadDashboard(item_type, item_id) {
            $.post("<?php echo url('loadDashboard');?>", {
                ajax_type: 'load_dashboard',
                item_type: item_type,
                item_id: item_id,
                _token:'{{ csrf_token() }}'
            }, function (ret) {
                $(".webitem").removeClass("active");
                $("#"+item_type+"_"+item_id).addClass("active");
                scrollPage('#float_layer');
                $("#dashboard").hide().html(ret).fadeIn(800);
                initializeDashboard();
            });
        }

        function selectLegend(option, id, chartdata) {
            var countVisible = 0;
            
            if (option == "viewALL") {
                
                if ( $(".legend-ALL").hasClass("isvisible")) {
//                    $(".legend-ALL").removeClass("isvisible");
//                    $("#optionLegend > li > i").removeClass("checked");
//                    $("#optionLegend > li").removeClass("isvisible");
//                    $("#controlLegend > li").remove();
                } else {
                    countVisible = 2;
                    $("#optionLegend > li > i").addClass("checked");                   
                    $(".legend-ALL").addClass("isvisible");
                    $("#optionLegend > li").not(".isvisible").clone().appendTo("#controlLegend");
                    $("#optionLegend > li").addClass("isvisible");
                }
            } else {
                id: id
                chartdata: chartdata
                $newlegend = $(".legend-"+id).clone();

                if ($(".legend-"+id).hasClass("isvisible")) {
                    
                    //Check if there's at least one other legend selected to prevent empty chart
                    $('#optionLegend li').each(function() {
                        if ($(this).hasClass("isvisible")) {
                            countVisible++;
                        }
                    });

                    if (countVisible > 1) {
                        $(".legend-"+id).children("i").removeClass("checked");
                        $(".legend-"+id).removeClass("isvisible");
                        $("#controlLegend").children(".legend-"+id).remove();
                        $(".legend-ALL").children("i").removeClass("checked");
                        $(".legend-ALL").removeClass("isvisible");
                    }
                } else {
                    countVisible = 2;
                    $(".legend-"+id).children("i").addClass("checked");
                    $(".legend-"+id).addClass("isvisible");
                    $newlegend.appendTo("#controlLegend");
                }
            }
            if (countVisible > 1) {
                controlChart();
            }
        }
                
        function loadChart() {
            var data = {
                labels : chartLabels,
                datasets : initialReport
            };
            var steps = 5;
            var max = maxInitialReport;
            if (max < steps) {
                steps = max;
            }
            var options = {
                bezierCurve : false,
                scaleOverride: true,
                scaleSteps: steps,
                scaleStepWidth: Math.ceil(max / steps),
                scaleStartValue: 0
            };
            ctx = $("#myChart").get(0).getContext("2d");
            new Chart(ctx).Line(data, options);        
        }
       
        function controlChart() {
            
            var datasets = new Array();
            var max = 0;
            var thisHighest = 0;
            $('#optionLegend li').each(function() {
                if ($(this).hasClass("isvisible")) {
                    var reportType = $(this).attr('report');
                    if (reportType) {
                        datasets.push(window[reportType]);
                        thisHighest = Math.max.apply(Math, window[reportType].data);
                        if (thisHighest > max) {
                            max = thisHighest;
                        }
                    }
                }
            });
            
            var steps = 5;
            if (max < steps) {
                steps = max;
            }
            var options = {
                bezierCurve : false,
                scaleOverride: true,
                scaleSteps: steps,
                scaleStepWidth: Math.ceil(max / steps),
                scaleStartValue: 0
            };

            var data = {
                labels : chartLabels,
                datasets : datasets
            };
            ctx = $("#myChart").get(0).getContext("2d");
            new Chart(ctx).Line(data, options);  

        }


function scrollPage(position_id){
    
	if(!position_id){
		$position_id = '#resultsMap';
	}else {
		$position_id = position_id;
	}
	//jQuery('html,body').animate({scrollTop: jQuery($position_id).offset().top},'slow');
    jQuery('html, body').animate({ scrollTop: jQuery('#dashboard').offset().top }, 1500);
}
    </script>
	<script src="{{ asset('/front/js/Chart.js') }}"></script>		
@endsection