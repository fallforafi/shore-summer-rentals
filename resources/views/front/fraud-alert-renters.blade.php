@extends('front')

@section('content')

<style>
.fraudAlertRenters h2{
	font-weight:600;
	font-size:42px;
	line-height: 0.5;
}
.fraudAlertRenters a, .fraudAlertRenters a:visited {
    text-decoration: underline;
}
.content-custom h2, .content-custom p {
    margin-bottom: 10px;
}
.fraudAlertRenters p {
    font-size: 18px !important;
    line-height: 1.5 !important;
    font-weight: 300 !important;
	margin:12px 0 0 !important;
}
.abtShoreSummertestimonial{
	margin-top:40px !important;
}

</style>

	
	<div class="fraudAlertRenters container">

        
		<div class="content content-full margin-top">

			<div class="content-main">
		
				<div class="content-custom"><h1><strong><br>RENTER FRAUD ALERT<br><br></strong></h1>
<h2><strong><span style="color: #ff6600; font-size: x-large;">Avoid Vacation Rental Scams</span></strong></h2>
<p style="text-align: left;"><strong><span style="color: #000000;"><br>SUGGEST TO MEET THE OWNER IN PERSON</span></strong><span style="color: #000000;"> to view the property before any money is exchanged. Following this one simple rule will eliminate 99% of fraud.</span></p>
<p style="text-align: left;"><span style="color: #000000;"><strong><span style="color: #000000;">NEVER WIRE FUNDS VIA WESTERN</span> UNION, MONEYGRAM</strong> or any other wire service – most likely, anyone who asks you to do so is a scammer.</span></p>
<p style="text-align: left;"><span style="color: #000000;"><strong>SHORESUMMERRENTALS.COM AND VACATIONRENTALS2U.COM IS NOT INVOLVED IN ANY TRANSACTION</strong>. We do not handle payments, guarantee transactions, provide escrow services, or offer "buyer protection" or "seller certification". It is the renter’s responsibility to use good judgment and rent at your own risk.</span></p>
<p style="text-align: left;"><span style="color: #000000;"><strong>NEVER GIVE OUT FINANCIAL INFORMATION</strong> (bank account number, social security number, eBay/PayPal info, etc.)</span></p>
<p style="text-align: left;"><span style="color: #000000;"><strong>TOO GOOD TO BE TRUE</strong>. If a rental seems suspicious or the offer is too good to be true, it probably is, please <a href="http://www.shoresummerrentals.com/contactus.php">contact us </a>for us to investigate before any money is exchanged.</span></p>
<p style="text-align: left;"><strong><span style="color: #000000;">WHO TO NOTIFY ABOUT FRAUD OR SCAM ATTEMPTS?</span></strong></p>
<p style="text-align: left;"><span style="color: #0000ff;"><a href="http://ftc.gov/multimedia/video/scam-watch/file-a-complaint.shtm"><span style="color: #0000ff;">FTC Video: How to report scams to the FTC</span></a></span></p>
<p style="text-align: left;"><span style="color: #000000;">FTC toll free hotline: 877-FTC-HELP (877-382-4357)</span></p>
<p style="text-align: left;"><span style="color: #0000ff;"><a href="http://www.ftc.gov/"><span style="color: #0000ff;">FTC online complaint form</span></a></span></p>
<p style="text-align: left;"><span style="color: #000000;">Canadian PhoneBusters hotline: 888-495-8501</span></p>
<p style="text-align: left;"><span style="color: #000000;"><span style="color: #0000ff;"><a href="http://www.competitionbureau.gc.ca/"><span style="color: #0000ff;">Compet</span></a><a href="http://www.competitionbureau.gc.ca/"><span style="color: #0000ff;">ition Bureau Canada</span></a></span>: 800-348-5358</span></p>
<p style="text-align: left;"><span style="color: #0000ff;"><a href="http://www.ic3.gov/"><span style="color: #0000ff;">Internet Fraud Complaint Center</span></a></span></p>
<p style="text-align: left;"><span style="color: #0000ff;"><span style="color: #0000ff;"><br></span></span></p></div>			
			</div>
			
					
		</div>

	</div>
						

					
@endsection