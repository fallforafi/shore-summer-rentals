
@extends('front')

@section('content')

<?php 
use App\Functions\Functions;
//extract($_POST);

            

?>

<style type="text/css">
    
.no-image{
    background-image: url('https://www.shoresummerrentals.com/custom/domain_1/content_files/noimage.gif') !important;
}
.ie .no-image { 
  filter: progid:DXImageTransform.Microsoft.AlphaImageLoader( 
src='https://www.shoresummerrentals.com/custom/domain_1/content_files/noimage.gif', sizingMethod='scale');
 background-image : none !important;
 }

</style>
  <link rel="stylesheet" href="{{ asset('front/css/members.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('front/style.css') }}" />
    <link rel="stylesheet" href="{{ asset('front/css/bootstrap.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('plugins/croppic/assets/css/croppic.css') }}"/>
    
  <link rel="stylesheet" href="{{ asset('front/css/structure.min.css') }}" />


    <script src="{{ asset('plugins/croppic/croppic.min.js') }}"></script>
      <script src="{{ asset('front/scripts/jquery/jquery_ui.1.9.2/js/jquery-ui-1.9.2.custom.min.js') }}" type="text/javascript"></script>
<script type="text/javascript" src="{{ asset('front/scripts/jquery/jquery.textareaCounter.plugin.js') }}"></script>

    <!--link rel="stylesheet" href="{{ asset('front/css/bootstrap-responsive.min.css') }}" /-->
  
	<div class="editProfile-page">
               
                
    <div class="container">

    	
	    <div class="row-fluid">

	        
     
 @if (session('message'))
<div id="alert" class="alert alert-success" style="">
       <?php  echo session('message'); ?>
 </div>
@endif       
    <p class="successMessage" id="messageEmail" style="display:none">Your activation e-mail has been sent. Please, check your inbox.</p>
    <p class="errorMessage" id="messageEmailError" style="display:none"></p>
    
    
    
	        <div class="member-form">
	            
	<form name="account" id="account" method="post" action="<?php echo url('profile/update');?>" enctype="multipart/form-data">

    <input type="hidden" name="_token" value="{{ csrf_token() }}">

		<input type="hidden" name="tab" id="tab" value="" />
		<input type="hidden" name="account_id" value="{{ $userAccountId }}" />

    <div id="cont_tab_1" style="">
            
			
    <script type="text/javascript">
        //<![CDATA[       

        function getFacebookImage() {
            
            $("#image_fb").html("<img src=\"" + DEFAULT_URL + "/images/img_loading_big.gif\" alt=\"\" />");
            
            $.post(DEFAULT_URL + "/sponsors/ajax.php", {
                ajax_type: 'getFacebookImage',
                id: '5528'
            }, function(newImage) {
                var eURL = /http(s)?:\/\/([\w-]+\.)+[\w-]+(\/[\w- ./?%&=]*)?/
                var arrInfo = newImage.split("[FBIMG]");
                var imgSize = "";
                
                if (arrInfo[0] && eURL.exec(arrInfo[0])) {
                    $("#facebook_image").val(arrInfo[0]);
                    if (arrInfo[1] && arrInfo[2]) {
                        var w = parseInt(arrInfo[1]);
                        var h = parseInt(arrInfo[2]);
                        $("#facebook_image_height").val(h);
                        $("#facebook_image_width").val(w);

                        imgSize = " width=\"" + w + "\" ";
                        imgSize += " height=\"" + h + "\" ";
                    } else {
                        $("#facebook_image_height").val("130");
                        $("#facebook_image_widht").val("130");
                        imgSize = " width=\"130\" ";
                        imgSize += " height=\"130\" ";
                    }
                    $("#image_fb").html("<img src=\"" + arrInfo[0] + "\" " + imgSize + " alt=\"\" />");
                    if ($("#message").text() == "Error uploading image. Please try again.") {
                        $("#message").removeClass("errorMessage");
                        $("#message").text("");
                    }
                } else if (!eURL.exec(arrInfo[0])) {
                    $("#facebook_image").val("");
                    $("#image_fb").html("<img src=\"https://www.shoresummerrentals.com/images/profile_noimage.gif\" width=\"130\" height=\"130\" alt=\"No Image\" />");
                    $("#message").removeClass("successMessage");
                    $("#message").removeClass("informationMessage");
                    $("#message").addClass("errorMessage");
                    $("#message").text("Error uploading image. Please try again.");
                }
            });
        }

        function profileStatus(check_id) {
            var check = $("#" + check_id).attr("checked");
            $.post("<?php echo url('profile/ajax'); ?>", {
                action: "changeStatus",
                has_profile: check,
                account_id: '{{ Functions::sess_getAccountIdFromSession() }}',
                 _token: '<?php echo csrf_token(); ?>',
                ajax: true
            });
            
        }

        function validateFriendlyURL(friendly_url, current_acc) {
        
            $("#URL_ok").css("display", "none");
            $("#URL_notok").css("display", "none");
        
            if (friendly_url) {

                $("#loadingURL").css("display", "");

                $.get("<?php echo url('check_friendlyurl'); ?>", {
                    type: "profile",
                    friendly_url: friendly_url,
                    current_acc : current_acc
                }, function (response) {
                    if (response == "ok") {
                        $("#urlSample").html(friendly_url);
                        $("#URL_ok").css("display", "");
                        $("#URL_notok").css("display", "none");
                    } else {
                        $("#URL_ok").css("display", "none");
                        $("#URL_notok").css("display", "");
                    }
                    $("#loadingURL").css("display", "none");
                });
            } else {
                $("#URL_ok").css("display", "none");
                $("#URL_notok").css("display", "none");
            }
        }
        
        function removePhoto() {
            $.post("<?php echo url('profile/ajax'); ?>", {
                action: "removePhoto",
                account_id: '{{ Functions::sess_getAccountIdFromSession() }}',
                 _token: '<?php echo csrf_token(); ?>',
                ajax: true
            }, function(){
                $("#facebook_image").attr("value", "");
                $("#facebook_image_height").attr("value", "");
                $("#facebook_image_width").attr("value", "");
                $("#linkRemovePhoto").css("display", "none");
                $("#image_fb").html("<img src=\"https://www.shoresummerrentals.com/images/profile_noimage.gif\" width=\"130\" height=\"130\" alt=\"No Image\" />");
            });
        }
        //]]>
    </script>

<?php
if(isset($return))
{
    echo $return;
}

    ?>

<a href="#" id="crop_window" class="fancy_window_crop" style="display:none"></a>

    <script type="text/javascript">
	//<![CDATA[
    
        $(document).ready(function() {
            
                                
            //use fancybox 2.0
            $("a.fancy_window_crop").fancybox({
                modal                   : true,
                width                   : 500,
                height                  : 460,
                type                    : 'ajax',
                
                                                                
                padding             : 0,
                margin              : 0,
                closeBtn            : false,

                                
                autoSize                : false
            });
                    
                    });
    
        // creating an iframe to post the image to generate a preview
        $('document').ready(function() { 
  $("<iframe  name='img_frame' style='display:none;'></iframe>").appendTo('body'); });

        // description : upload the image to the server
        // notes : 1. send to the iframe created before
        // 		   2. submit the form to it
        function UploadImage(form, multi) {

        	if ( multi ) {
        		var counter = multi;
        		var subcrop = multi;
        	}
        	else {
        		var counter = '';
        		var subcrop = true;
        	}
            $("#crop_window").attr("href", '<?php echo url('form_crop'); ?>?multi=' + counter + '&sitemgrLang=0');
            $("#crop_window").trigger('click');

        	// clearing error message and removing thumb preview
        	$('#noImageSpan'+counter).css('display', 'none');
            // removing pre-cropped images if any
            if ( '.jcrop-holder' ) {
	            $('.jcrop-holder').remove();
	            $('.jcrop-hline').remove();
	            $('.jcrop-vline').remove();
	            $('.jcrop-tracker').remove();
	            $('.jcrop-handle').remove();
			}
            // sending image
            $('#crop_submit').val(subcrop);
            $('#'+form).attr('target', 'img_frame');
            $('#'+form).submit();
            $('#'+form).attr('target', '_self');
            $('#crop_submit').val(0);
        }

           // placing an error message if the image format is not allowed
        function noImage(message, multi) {
            if ( multi ) var counter = multi;
            else var counter = '';
            $('#loadingBar').remove();
            if(message == "type"){
                $('#errorType').css('display', '');
            } else if(message == "size") {
                $('#errorSize').css('display', '');
            }
            $('#noImageSpan'+counter).css('display', '');
        }

        // set the src of the image to the uploaded one
        function SetImageFile( pImgSrc, imgWidth, imgHeight, imgType, multi , aspectRat ) {
            if ( multi ) var counter = multi;
            else var counter = '';
            $('#loadingBar').remove();
            $('#ButtonCropSubmit').css('display', '');
            $('#imgUpload'+counter).attr('src', pImgSrc);
            $('#imgUpload'+counter).css('display', '');
            $('#crop').css('display', '');

            setJcrop(imgWidth, imgHeight, imgType, counter, aspectRat);
        }

        // creating the Jcrop
        function setJcrop(imgWidth, imgHeight, imgType, multi, aspectRat) {
            if ( multi ) var counter = multi;
            else var counter = '';
            $('#imgUpload'+counter).Jcrop({
                onChange: showCoords,
                onSelect: showCoords,
                setSelect:   [ (imgWidth/4), (imgHeight/4), (imgWidth/4*3), (imgHeight/4*3) ],
                aspectRatio: aspectRat,
                boxWidth: 400,
                boxHeight: 400,
                bgColor: 'transparent',
                fullImageWidth: imgWidth,
                fullImageHeight: imgHeight
            });
            function showCoords(c) {
                $('#x'+counter).val(c.x);
                $('#y'+counter).val(c.y);
                $('#x2'+counter).val(c.x2);
                $('#y2'+counter).val(c.y2);
                $('#w'+counter).val(c.w);
                $('#h'+counter).val(c.h);
                $('#image_width'+counter).val(imgWidth);
                $('#image_height'+counter).val(imgHeight);
                $('#image_type'+counter).val(imgType);
            };
        }
		//]]>
    </script>        
    <div id="hiddenFields" style="display: none;">
        <input type="hidden" id="facebook_image" name="facebook_image" value="" />
        <input type="hidden" id="facebook_image_height" name="facebook_image_height" value="0" />
        <input type="hidden" id="facebook_image_width" name="facebook_image_width" value="0" />
        <input type="hidden" name="image_id" value="{{ $image_id }}" />

        <!--Crop Tool Inputs-->
        <input type="hidden" name="x" id="x" />
        <input type="hidden" name="y" id="y" />
        <input type="hidden" name="x2" id="x2" />
        <input type="hidden" name="y2" id="y2" />
        <input type="hidden" name="w" id="w" />
        <input type="hidden" name="h" id="h" />
        <input type="hidden" name="image_width" id="image_width" />
        <input type="hidden" name="image_height" id="image_height" />
        <input type="hidden" name="image_type" id="image_type" />
        <input type="hidden" name="crop_submit" id="crop_submit" />
    </div>
    
    <div id="personal-info">
                    
        <div class="left textright">

            <h2>Profile Information</h2>
            
            <div id="image_fb">
              <?php echo $imgTag; ?>
                           </div>           
            
            <div class="hiddenFile-box">
                                                       
                <span class="hiddenFile">
                    <button type="button" id="buttonfile">Change photo</button>
                    <input type="file" name="image" id="image" size="1" onchange="UploadImage('account');"/>
                </span>

            </div>
            
            <br />
            
                            <div id="linkRemovePhoto">
                    <a href="javascript: void(0);" onclick="removePhoto();">Remove photo</a>
                    <br />
                </div>
                        
            
        </div>

        <div class="right">
            
            <div class="cont_70">
                <label>Your Name <a href="javascript: void(0);">* <span>Required field</span></a></label>
 <input type="text" name="nickname" value="<?php echo $profileSp[0]->nickname; ?>" />	
            </div>

   
            <div class="cont_100">
                <label>About Me</label>
    <textarea id="personal_message" name="personal_message" rows="7" cols="1"><?php echo $profileSp[0]->personal_message; ?></textarea>	
					
            </div>


<script language="javascript" type="text/javascript">

        $(document).ready(function() {
            
            var field_name = 'personal_message';
            var count_field_name = 'personal_message_remLen';

            var options = {
                        'maxCharacterSize': 250,
                        'originalStyle': 'originalTextareaInfo',
                        'warningStyle' : 'warningTextareaInfo',
                        'warningNumber': 40,
                        'displayFormat' : '<span><input readonly="readonly" type="text" id="'+count_field_name+'" name="'+count_field_name+'" size="3" maxlength="3" value="#left" class="textcounter" disabled="disabled" /> characters left (including spaces and line breaks)</span>' 
                };
            $('#'+field_name).textareaCount(options);

        });
    </script>
        </div>

    </div>
        
    <div id="personal-page">

        <div class="left textright">

            <h2>Personal Page</h2> 				
            <span>This is the URL where visitors can see your public profile page</span>

        </div>

        <div class="right">
            
                            <div class="cont_checkbox">
 <input type="checkbox" id="has_profile" name="has_profile" <?php echo ($profileSp[0]->has_profile == "y") ? "checked=\"checked\"": ""; ?> onclick="profileStatus(this.id);" />
                    <label for="has_profile">Create my Personal Page</label>
                </div>
            
            <div class="cont_100">
                <label>Your URL <a href="javascript: void(0);">* <span>Required field</span></a></label>
                
                <div class="checking"><!--<?php echo $profileSp[0]->friendly_url; ?>-->
       <input type="text" name="friendly_url" id="friendly_url" value="<?php echo $profileSp[0]->friendly_url; ?>" onblur="validateFriendlyURL(this.value, {{ $userAccountId }});easyFriendlyUrl(this.value, 'friendly_url', 'a-zA-Z0-9', '-'); " />
                   
                    <img id="loadingURL" src="https://www.shoresummerrentals.com/images/img_loading.gif" width="15px;" style="display: none;" />
                    <span id="URL_ok" class="positive" style="display: none;"> <img src="https://www.shoresummerrentals.com/images/ico-approve.png" border="0" alt="" /> Available URL</span>
                    <span id="URL_notok" class="negative" style="display: none;"> <img src="https://www.shoresummerrentals.com/images/ico-deny.png" border="0" alt="" /> Unavailable URL</span>
                </div>	 					 				
            </div>

            <div class="cont_100">
                <label class="label_100">{{ $domain_url }}/<b id="urlSample"><?php echo $profileSp[0]->friendly_url; ?></b>/</label>
            </div>

        </div>

    </div>


			<div class="btAdd">
				<p class="standardButton">
					<button type="submit" name="profile_save" value="Submit">Submit</button>
				</p>
				<p class="standardButton">
	<button type="reset" onclick="redirect('<?php echo url('profile'); ?>');">Cancel</button>
				</p>
			</div>
		</div>
        
        
	</form>

	<script language="javascript" type="text/javascript">
		function redirect (url) {
			window.location = url;
		}
	</script>	        </div>

		</div>

		
	</div>
                </div><!-- Close container-fluid div -->
        </div><!-- Close container div -->



		 <script type="text/javascript"> 
     
    </script>


@endsection