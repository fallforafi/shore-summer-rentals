<?php  /*Js Function For Compare Listing*/  ?>

<script>
  function view_quickInfo(row,resource_id)
  {
    var TOKEN = '{{ csrf_token() }}';
     $.ajax({
        url: "{{ url('quick-info') }}",
        type: 'POST',
        data: {resource_id: resource_id,_token:TOKEN},
        dataType: 'JSON',
        success: function (data) {
            var obj = (data);
            var newContent='';
            if(!$.isEmptyObject(obj))
            {
            $.each( obj, function( key, value ) {
                    if(value.type==='' || value.type=== null) 
                    {
                        $('#quick_info_image').attr('src','{{ asset("images/noimage.jpg") }}' );
                    }
                    else
                    {
                        $('#quick_info_image').attr('src','<?php echo config('params.imagesPath');?>'+value.prefix+'photo_'+value.image_id+'.'+value.type.toLowerCase());    
    
                    }
                    
                    $('#quick_info_title').html(value.title);
                    $('#quick_info_location').html('Location: '+value.address+' '+value.address2+', '+value.location_4_title+', '+value.location_3_title+', '+value.zip_code)
                    $('#quick_info_rentalId').html(resource_id);
                    
                    if(value.min_week===null || value.min_week==='')
                    {
                        $('#quick_info_rate').html('');
                        
                    }
                    else
                    {
                        $('#quick_info_rate').html("<strong> "+'<?php echo $currency[Config::get('params.currency_default')]['symbol']; ?>'+value.min_week+"- "+'<?php echo $currency[Config::get('params.currency_default')]['symbol']; ?>'+value.max_week+"</strong><br> per week");
                    }
                    
                    if(value.required_stay===null || value.required_stay==='')
                    {
                        $('#quick_info_stay').html('');
                    }
                    else
                    {
                        $('#quick_info_stay').html(value.required_stay+' minimum stay');
                    }
                });
                
               
                
//                
//                $("#location_4").html(newContent);
            
            }
            else if($.isEmptyObject(obj))
            { 
              //  newContent='<option value="">Choose state first</option>';
//                $("#location_4").html(newContent);
                return false;
            }
            else if(obj==0)
            { 
              //  newContent='<option value="">Choose state first</option>';
//                $("#location_4").html(newContent);
                return false;
            }
        }
    });
    $('#quickInfo').modal('show');
  }
  
  function compare_listing(result_count,row,resource_id,title)
  {
  
        
        setCookie('result_count',result_count);
        if($("#compare_"+row).prop('checked') == true){
            //alert(getCookie('title_'+row));
            
            
            setCookie('row_'+row,row);
            setCookie('title_'+row,title);
            setCookie('resource_id_'+row,resource_id);
            
            //alert(resource_id);
            // truncate title 
            var title_temp = title;
            if(title_temp.length>25)
            {
               var shortText = jQuery.trim(title_temp).substring(0, 25)
                .trim(this) + "..."; 
            }
            else
            {
                var shortText = title_temp;
            }
            
            
            
            
            var html_temp = ' <li id="compare_li_'+row+'" >'+
                                '<b>'+shortText+'</b>'+
                                '<span>'+
                                    '<a onclick="remove_signle_item('+row+')" rel="nofollow" href="JAVASCRIPT:void(0);">x</a>'+
                                '</span>'+
                            '</li>';
            $('#compare_ul').append(html_temp);
            // get total chiuld of ul compare list
            var count = parseInt($("#compare_ul").children().length);
            setCookie('total_list',count);
            if(count>0)
            {
                $('#comparsion_box').css('display','block');
            }
            else
            {
                $('#comparsion_box').css('display','none');
            }
            if(count>1)
            {
                $('#compare_button').css('display','block');
            }
            else if(count<=1)
            {
               $('#compare_button').css('display','none');  
            } 
            var total_list = getCookie('total_list');
            
            if(total_list>5)
            {
                setCookie('row_'+row,'');
                setCookie('title_'+row,'');
                setCookie('resource_id_'+row,'');
                $('#compare_li_'+row).remove();
                setCookie('total_list',(count-1));
                $("#compare_"+row).prop('checked',false);
            }
            if(total_list>5)
            {
                
                row_number  = '<?php echo isset($result_count)?$result_count:0; ?>';
                //alert(row_number);
                
                for(var i=0;i<row_number;i++)
                {
                    if($("#compare_"+i).prop('checked') == false)
                    {
                        $("#compare_"+i).attr('disabled',true);
                    }
                }
                
                
                
            }
            else
            {
                row_number  = '<?php echo isset($result_count)?$result_count:0; ?>';
                 
                for(var i=0;i<row_number;i++)
                {
                    if($("#compare_"+i).prop('checked') == false)
                    {
                        $("#compare_"+i).attr('disabled',false);
                    }
                }
              
            }
                       
        }
        else
        {
            if(checkCookie(row,resource_id,title))
            {
                
                count = parseInt($("#compare_ul").children().length);
                $('#compare_li_'+row).remove();
                
                count = count-1;
                setCookie('total_list',count);
                row_number  = '<?php echo isset($result_count)?$result_count:0; ?>';
                for(var i=0;i<row_number;i++)
                {
                    if($("#compare_"+i).prop('checked') == false)
                    {
                        $("#compare_"+i).attr('disabled',false);
                    }
                }
                if(count>1)
                {
                    $('#compare_button').css('display','block');
                }
                else if(count<=1)
                {
                   $('#compare_button').css('display','none');  
                } 
                if(count>0)
                {
                    $('#comparsion_box').css('display','block');
                }
                else
                {
                    $('#comparsion_box').css('display','none');
                }
            }
           
        }
       

    
         return true;
    
  }
  
  function checkTotalList()
  {
        var total_list = getCookie('total_list');
       if(total_list>5)
        {
            alert('exceed limit '+total_list);
            row_number  = '<?php echo isset($result_count)?$result_count:0; ?>';
            for(var i=0;i<row_number;i++)
            {
                if($("#compare_"+i).prop('checked') == false)
                {
                    $("#compare_"+i).attr('disabled',true);
                }
            }
            
           return true; 
        }
        else
        {
            row_number  = '<?php echo isset($result_count)?$result_count:0; ?>';
            for(var i=0;i<row_number;i++)
            {
                if($("#compare_"+i).prop('checked') == false)
                {
                    $("#compare_"+i).attr('disabled',false);
                }
            }
            return true;
        }
  }

function setCookie(cname,cvalue) {
    
   
    document.cookie = cname+"="+cvalue+"; ";
}
function expireCookie(cname,cvalue) {
    
   
    document.cookie = cname+"="+cvalue+";expires=Thu, 01 Jan 1970 00:00:00 UTC ";
}



function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function checkCookie(row,resource_id,title) {
    var row_temp=getCookie("row_"+row);
    if (row_temp != "") {
        setCookie('row_'+row,'');
        
    }
    
    var resource_id_temp=getCookie("resource_id_"+row);
    if (resource_id_temp != "") {
        setCookie('resource_id_'+row,'');
        
    }
    
    var title_temp=getCookie("title_"+row);
    if (title_temp != "") {
        setCookie('title_'+row,'');
        
    }
    return true;
}

function listCookiesCompare() {
   row_number  = '<?php echo isset($result_count)?$result_count:0; ?>';
   for(var i=0;i<row_number;i++)
   {
        if(getCookie('row_'+i)!='')
        {
            
           var resource_id_temp =  getCookie('resource_id_'+i);
            
            // truncate title 
            var title_temp = getCookie('title_'+i);
            if(title_temp.length>20)
            {
               var shortText = jQuery.trim(title_temp).substring(0, 20)
                .trim(this) + "..."; 
            }
            else
            {
                var shortText = title_temp;
            }
           
            if(resource_id_temp==$('#compare_'+getCookie('row_'+i)).attr('value'))
            {
                $('#compare_'+getCookie('row_'+i)).attr('checked','checked');
               
            }
             var html_temp = ' <li id="compare_li_'+i+'" >'+
                '<b>'+shortText+'</b>'+
                '<span>'+
                '<a onclick="remove_signle_item('+i+')" rel="nofollow" href="JAVASCRIPT:void(0);">x</a>'+
                '</span>'+
                '</li>';
                $('#compare_ul').append(html_temp);
            
        }
        else
        {
           //$('#compare_'+getCookie('row_'+i)).attr('checked','');
           continue;
        }
        
   }
   return true;
    
}
function remove_signle_item(row)
{
        count = parseInt($("#compare_ul").children().length);
        $('#compare_li_'+row).remove();
        if(count>-1)
        {
            count = count-1;
        }
         if(count>0)
        {
            $('#comparsion_box').css('display','block');
        }
        else
        {
            $('#comparsion_box').css('display','none');
        }
        if(count>1)
        {
            $('#compare_button').css('display','block');
        }
        else if(count<=1)
        {
           $('#compare_button').css('display','none');  
        } 
    
        setCookie('row_'+row,'');
        setCookie('title_'+row,'');
        setCookie('resource_id_'+row,'');
        setCookie('total_list',(count-1));
        $("#compare_"+row).prop('checked',false);
}
function empty_list_compare()
{
    
    row_number  = '<?php echo isset($result_count)?$result_count:0; ?>';
     for(var i=0;i<row_number;i++)
     {
        var row_temp=getCookie("row_"+i);
        if (row_temp != "") {
            expireCookie('row_'+i,'');

        }
        
        var resource_id_temp=getCookie("resource_id_"+i);
        if (resource_id_temp != "") {
            expireCookie('resource_id_'+i,'');
            
        }
        
        var title_temp=getCookie("title_"+i);
        if (title_temp != "") {
            expireCookie('title_'+i,'');
            
        }
        
        if($("#compare_"+i).prop('checked') == true)
        {
            $("#compare_"+i).attr('checked',false);
        }
     }
    $('#compare_ul').empty();
    $('#comparsion_box').css('display','none');


}
// A $( document ).ready() block.
$( document ).ready(function() {
    listCookiesCompare();
    
    var total_list = parseInt($("#compare_ul").children().length);
    if(total_list>0)
    {
        $('#comparsion_box').css('display','block');
    }
    else
    {
        $('#comparsion_box').css('display','none');
    }
    if(total_list>1)
    {
        $('#compare_button').css('display','block');
    }
    else if(total_list<=1)
    {
       $('#compare_button').css('display','none');  
    } 
    if(total_list>4)
    {
     
        row_number  = '<?php echo isset($result_count)?$result_count:0; ?>';
        for(var i=0;i<row_number;i++)
        {
            if($("#compare_"+i).prop('checked') == false)
            {
                $("#compare_"+i).attr('disabled',true);
            }
        }
        
        
    }
    else
    {
        row_number  = '<?php echo isset($result_count)?$result_count:0; ?>';
        for(var i=0;i<row_number;i++)
        {
            if($("#compare_"+i).prop('checked') == false)
            {
                $("#compare_"+i).attr('disabled',false);
            }
        }
      
    }
});

  </script>
<script>
function CheckForm(){
        var form_get = jQuery('#search_form').serializeArray();

        jQuery.ajax({
          url: "{{ url('advancedsearch-checkform') }}",
          context: document.body,
          data: form_get,
          success: function(redirect_url){
            //console.log(redirect_url);
                window.location.href = redirect_url.return_url;
          }
        });
        return false;
    }



</script>