
@extends('front')

@section('content')

    <link rel="stylesheet" href="{{ asset('front/css/members.min.css') }}" />
	<link rel="stylesheet" href="{{ asset('front/css/content_custom.min.css') }}" />
	<link href="https://www.shoresummerrentals.com/custom/domain_1/theme/default/content_custom.min.css" rel="stylesheet" type="text/css" media="all">
    <!--link rel="stylesheet" href="{{ asset('front/css/bootstrap-responsive.min.css') }}" /-->
     
     
    
    
<style>

.content-custom h1, .content-custom h2, .content-custom h3{
    line-height: 1;
    text-indent: 0;
	margin:0;
}
.content-custom{
	margin-top:0;
}
.content-custom p{
	font-size:18px;
	line-height: 1.5;
    font-weight: 300;
}
.content-custom a, .content-custom a:visited {
    text-decoration: underline;
}
</style>	  

               
                
    
   <div class="contentFaqs__container container">

        
		<div class="content content-full margin-top">

			<div class="content-main">
		
				<div class="content-custom"><h1><strong><br>FREQUENTLY ASKED QUESTIONS</strong></h1>
<h2><strong><span style="font-size: x-large; color: #ff6600;"><br>Learn More About Listing Your Property</span></strong></h2>
<p style="margin-top:10px; margin-bottom:10px;"><strong><span style="color: #000000;"><br>General</span></strong></p>
<p><span style="color: #3366ff;"><a href="#Q1"><span style="color: #3366ff;">Q1: How can I be confident that the rental properties on&nbsp;Shore SummerRentals.com are legitimate?</span></a></span></p>
<div align="center"><hr align="center" size="1" width="100%"></div>
<p><a href="#Q2">Q2: Can you help create or modify my ad?</a></p>
<div align="center"><hr align="center" size="1" width="100%"></div>
<p><a href="#Q3">Q3: I cannot see changes that I made to my ad.</a></p>
<div align="center"><hr align="center" size="1" width="100%"></div>
<p><a href="#Q4">Q4: My new ad is not showing on the website.</a></p>
<div align="center"><hr align="center" size="1" width="100%"></div>
<p><a href="#Q5">Q5: Which spot will my ad be displayed?</a></p>
<div align="center"><hr align="center" size="1" width="100%"></div>
<p><a href="#Q6">Q6: What are the differences in your&nbsp;Main Membership levels?</a></p>
<div align="center"><hr align="center" size="1" width="100%"></div>
<p><a href="#Q7"><span style="background-color: #ffffff;"><span style="background-color: #ffffff;">Q7: I have more than one property in the same location, am I eligible for a discount?</span></span></a></p>
<div align="center"><hr align="center" size="1" width="100%"></div>
<p><a href="#Q8">Q8: Do you offer discounts for more than one property in separate locations?</a></p>
<div align="center"><hr align="center" size="1" width="100%"></div>
<p><a href="#Q9">Q9: What is an Add-On and&nbsp;is it included in my Main Membership?</a></p>
<div align="center"><hr align="center" size="1" width="100%"></div>
<p><a href="#Q10">Q10: How do I Prove Property Owmership?</a></p>
<div align="center"><hr align="center" size="1" width="100%"></div>
<p><a href="#Q11">Q11: I am Having Trouble Uploading Photos</a>.</p>
<div align="center"><hr align="center" size="1" width="100%"></div>
<p><a href="#Q12">Q12: How Do I&nbsp;Edit My Calendar?</a></p>
<div align="center"><hr align="center" size="1" width="100%"></div>
<p><a href="#Q13">Q13: Can I Switch Out Properties During the Year?</a></p>
<div align="center"><hr align="center" size="1" width="100%"></div>
<p><a href="#Q14">Q14: I Sold My Property, Can I Receive a Credit or Refund?</a></p>
<div align="center"><hr align="center" size="1" width="100%"></div>
<p><a href="#Q15">Q15: Can You Renew My Listing With Last Years Credit Card Information?</a></p>
<div align="center"><hr align="center" size="1" width="100%"></div>
<p><a href="#Q16">Q16: Can I Reserve a Vacation Rental Property through Vacation Rentals 2U.com and ShoreSummerRentals.com?</a></p>
<div align="center"><hr align="center" size="1" width="100%"></div>
<p><span style="color: #000000;"><strong>Membership Prices, Details and Payments</strong></span></p>
<p><a href="#Q17">Q17: One Year Membership Term Only</a></p>
<div align="center"><hr align="center" size="1" width="100%"></div>
<p><a href="#Q18">Q18: Is there setup fees involved to create my vacation rental property advertisement?</a></p>
<div align="center"><hr align="center" size="1" width="100%"></div>
<p><a href="#Q19">Q19: Are there management or referral fees involved with VacationRentals2U.com and ShoreSummerRentals.com</a></p>
<div align="center"><hr align="center" size="1" width="100%"></div>
<p><a href="#Q20">Q20: Do I have to have a credit card to sign up?</a></p>
<div align="center"><hr align="center" size="1" width="100%"></div>
<p><a href="#Q21">Q21: Main Membership Details and Pricing</a></p>
<div align="center"><hr align="center" size="1" width="100%"></div>
<p><a href="#Q22">Q22: Add-On Package Details and Pricing</a></p>
<div align="center"><hr align="center" size="1" width="100%"></div>
<p><a href="#Q23">Q23: Making Payments</a></p>
<div align="center"><hr align="center" size="1" width="100%"></div>
<p><a href="#Q24">Q24: Do You Have Auto Renewal Billing?</a></p>
<div align="center"><hr align="center" size="1" width="100%"></div>
<p><a href="#Q25">Q25: Rental Guarantee </a></p>
<div align="center"><hr align="center" size="1" width="100%"></div>
<p><a href="#Q26">Q26: My Property is Booked,&nbsp;I Decided Not to Rent My Property or I Sold my Property Before My Subscription Expired, Can I Get a Refund for the Time Not Used? </a></p>
<div align="center"><hr align="center" size="1" width="100%"></div>
<p><a href="#Q27">Q27: Payment Address</a></p>
<div align="center"><hr align="center" size="1" width="100%"></div>
<p><span style="color: #000000;"><strong>Security</strong></span></p>
<p><a href="#Q28">Q28: Is ShoreSummerRentals.com a secure site?</a></p>
<div align="center"><hr align="center" size="1" width="100%"></div>
<p><span style="color: #000000;"><strong>Exposure</strong></span></p>
<p><a href="#Q29">Q29: How much web traffic does ShoreSummerRentals.com get?</a></p>
<div align="center"><hr align="center" size="1" width="100%"></div>
<p><a href="#Q30">Q30: How will I know if people are looking at my vacation rental listing?</a></p>
<div align="center"><hr align="center" size="1" width="100%"></div>
<p><a href="#Q31">Q31: How will I receive vacation property rental inquiries?</a></p>
<div align="center"><hr align="center" size="1" width="100%"></div>
<p><a href="#Q32">Q32: How do you promote the VacationRentals2U.com and ShoreSummerRentals.com website?</a></p>
<div align="center"><hr align="center" size="1" width="100%"></div>
<p><a href="#Q33">Q33: What are the advantages of listing my vacation rental property on the VacationRentals2U.com and ShoreSummerRentals.com website?</a></p>
<div align="center"><hr align="center" size="1" width="100%"></div>
<p><span style="color: #000000;"><strong>Location</strong></span></p>
<p><a href="#Q34">Q34: What shore vacation towns are included for rental properties on the VacationRentals2U.com and ShoreSummerRentals.com website?</a></p>
<div align="center"><hr align="center" size="1" width="100%"></div>
<p><a href="#Q35">Q35: Besides Jersey shore rental properties, what other vacation rental destinations are featured on our site?</a></p>
<div align="center"><hr align="center" size="1" width="100%"></div>
<p><a href="#Q36">Q36: Do the VacationRentals2U.com and ShoreSummerRentals.com websites aalso promote winter vacation rental properties?</a></p>
<div align="center"><hr align="center" size="1" width="100%"></div>
<p><span style="color: #000000;"><strong>Ad and Account Management</strong></span></p>
<p><a href="#Q37">Q37: Listing Title Tips</a></p>
<div align="center"><hr align="center" size="1" width="100%"></div>
<p><a href="#Q38">Q38: Listing Guidelines</a></p>
<div align="center"><hr align="center" size="1" width="100%"></div>
<p><a href="#Q39">Q39: Forgot Username or Password</a></p>
<div align="center"><hr align="center" size="1" width="100%"></div>
<p><a href="#Q40">Q40: Transfer a listing</a></p>
<div align="center"><hr align="center" size="1" width="100%"></div>
<p><a href="#Q41">Q41: Change Account Holder and Contact Owner&nbsp;Email Address</a></p>
<div align="center"><hr align="center" size="1" width="100%"></div>
<p><a href="#Q42">Q42: Modify/Delete Listings</a></p>
<div align="center"><hr align="center" size="1" width="100%"></div>
<p><a href="#Q43">Q43: Having Trouble Registering on Website</a></p>
<div align="center"><hr align="center" size="1" width="100%"></div>
<p><a href="#Q44">Q44: What Web Browsers are Supported by the Website?</a></p>
<div align="center"><hr align="center" size="1" width="100%"></div>
<p><a href="#Q45">Q45: Change Password</a></p>
<div align="center"><hr align="center" size="1" width="100%"></div>
<p><a href="#Q46">Q46: Property Address Visible on Ad</a></p>
<div align="center"><hr align="center" size="1" width="100%"></div>
<p><a href="#Q47">Q47: </a></p>
<div align="center"><hr align="center" size="1" width="100%"></div>
<p><a href="#Q48">Q48: Do I Need to Know HTML or How to Design my Summer Vacation Rental Page?</a></p>
<div align="center"><hr align="center" size="1" width="100%"></div>
<p><a href="#Q49">Q49: Do I Need to Create 2&nbsp;Listings&nbsp;for my home to appear on ShoreSummerRentals.com and VacationRentals2U.com? </a></p>
<div align="center"><hr align="center" size="1" width="100%"></div>
<p><a href="#Q50">Q50: Who Updates my Listings?</a></p>
<div align="center"><hr align="center" size="1" width="100%"></div>
<p><a href="#Q51">Q51: May I provide links to other web pages external to my ad? </a></p>
<div align="center"><hr align="center" size="1" width="100%"></div>
<p><span style="color: #000000;"><strong>Google Maps</strong></span></p>
<p><a href="#Q52">Q52: Google Maps</a></p>
<div align="center"><hr align="center" size="1" width="100%"></div>
<p><a href="#Q53">Q53: Map is Not Showing My Location Accurately </a></p>
<div align="center"><hr align="center" size="1" width="100%"></div>
<p><a href="#Q54">Q54: Can I Disable the Map?</a></p>
<div align="center"><hr align="center" size="1" width="100%"></div>
<p><span style="color: #000000;"><strong>Photos</strong></span></p>
<p><a href="#Q55">Q55: How do I add and change photographs to my vacation rental listing?</a></p>
<div align="center"><hr align="center" size="1" width="100%"></div>
<p><a href="#Q56">Q56: Additional Photo Tips</a></p>
<div align="center"><hr align="center" size="1" width="100%"></div>
<p><a href="#Q57">Q57: </a></p>
<div align="center"><hr align="center" size="1" width="100%"></div>
<p><a href="#Q58">Q58: Can I Copy Photos from Another Website?</a></p>
<div align="center"><hr align="center" size="1" width="100%"></div>
<p><a href="#Q59">Q59: Do You Charge to Update Photos or Make Changes to My Property Listings?</a></p>
<div align="center"><hr align="center" size="1" width="100%"></div>
<p><a href="#Q60">Q60: My Text and Photos Were Copied</a></p>
<div align="center"><hr align="center" size="1" width="100%"></div>
<p><a href="#Q61">Q61: </a></p>
<div align="center"><hr align="center" size="1" width="100%"></div>
<p><span style="color: #000000;"><strong>Calendar</strong></span></p>
<p><a href="#Q62">Q62: Do You Offer a Rental Property Availability Calendar?</a></p>
<div align="center"><hr align="center" size="1" width="100%"></div>
<p><a href="#Q63">Q63: Where Do I Update My Calendar?</a></p>
<div align="center"><hr align="center" size="1" width="100%"></div>
<p><a href="#Q64">Q64: Do I Have to Update My Calendar?</a></p>
<div align="center"><hr align="center" size="1" width="100%"></div>
<p><a href="#Q65">Q65: Can I Link Another Calendar to My Ad?</a></p>
<div align="center"><hr align="center" size="1" width="100%"></div>
<p><span style="color: #000000;"><strong>Rental Property Rates</strong></span></p>
<p><a href="#Q66">Q66: Where Do I Post My Rates?</a></p>
<div align="center"><hr align="center" size="1" width="100%"></div>
<p><a href="#Q67">Q67: Do I Have to Post Rates?</a></p>
<div align="center"><hr align="center" size="1" width="100%"></div>
<p><span style="color: #000000;"><strong>Rental Coordination and Forms</strong></span></p>
<p><a href="#Q68">Q68: Who is responsible for writing the rental lease agreement?</a></p>
<div align="center"><hr align="center" size="1" width="100%"></div>
<p><a href="#Q69">Q69: Who determines the price I charge for my vacation rental property?</a></p>
<div align="center"><hr align="center" size="1" width="100%"></div>
<p><a href="#Q70">Q70: How Do I Get the Renter the Key?</a></p>
<div align="center"><hr align="center" size="1" width="100%"></div>
<p><a href="#Q71">Q71: </a></p>
<div align="center"><hr align="center" size="1" width="100%"></div>
<p><span style="color: #000000;"><strong>Cancellation Policy</strong></span></p>
<p><a href="#Q72">Q72: May I cancel my vacation rental listing at any time?</a></p>
<div align="center"><hr align="center" size="1" width="100%"></div>
<p><a href="#Q73">Q73: Can I delete my advertisement if I am no longer interested in advertising?</a></p>
<div align="center"><hr align="center" size="1" width="100%"></div>
<p><span style="color: #000000;"><strong>Miscellaneous Questions</strong></span></p>
<p><a href="#Q74">Q74: Can I find a listing using the listing number?</a></p>
<div align="center"><hr align="center" size="1" width="100%"></div>
<p><a href="#Q75">Q75: Is There a Direct Link to My Listing?</a></p>
<div align="center"><hr align="center" size="1" width="100%"></div>
<p><a href="#Q76">Q76: My City is Not Listed on Your Website.</a></p>
<div align="center"><hr align="center" size="1" width="100%"></div>
<p><a href="#Q77">Q77: Featured Properties.</a></p>
<div align="center"><hr align="center" size="1" width="100%"></div>
<p><a href="#Q78">Q78: Cash Back Referral Program</a></p>
<div align="center"><hr align="center" size="1" width="100%"></div>
<p><a href="#Q79">Q79: Can I Restore an Expired Listing?</a></p>
<div align="center"><hr align="center" size="1" width="100%"></div>
<p><a href="#Q80">Q80: Can I Restore a Deleted Listing?</a></p>
<div align="center"><hr align="center" size="1" width="100%"></div>
<p><a href="#Q81">Q81: Renters: Bounced Email</a></p>
<div align="center"><hr align="center" size="1" width="100%"></div>
<p><a href="#Q82">Q82: Owners: Bounced Email</a></p>
<div align="center"><hr align="center" size="1" width="100%"></div>
<p><a href="#Q83">Q83: Someone copied my listing text</a></p>
<div align="center"><hr align="center" size="1" width="100%"></div>
<p><strong>Spam, Scams, Complaints and Abuse</strong></p>
<p><a href="#Q84">Q84: Spam Emails</a></p>
<div align="center"><hr align="center" size="1" width="100%"></div>
<p><a href="#Q85">Q85: Suspicious Emai</a></p>
<div align="center"><hr align="center" size="1" width="100%"></div>
<p><a href="#Q86">Q86: Renter Overpayment Scams</a></p>
<div align="center"><hr align="center" size="1" width="100%"></div>
<p><a href="#Q87">Q87: File a Complaint about a Property Owner.</a></p>
<div align="center"><hr align="center" size="1" width="100%"></div>
<p><a href="#Q88">Q88: Protection against Abuse/Fraud.</a></p>
<div align="center"><hr align="center" size="1" width="100%"></div>
<p><a href="#Q89">Q89: Report Abuse/Fraud</a></p>
<div align="center"><hr align="center" size="1" width="100%"></div>
<p style="text-align: justify;"><span style="color: #000000;"><strong>General</strong></span></p>
<p style="text-align: justify;"><span style="color: #000000;"><strong><a name="Q1"></a>Q1: How can I be confident that the rental properties on Shore SummerRentals.com are legitimate?</strong></span></p>
<p style="text-align: justify;"><span style="color: #000000;">We put our owners through a thorough&nbsp;verification process in order to prove ownership before allowing their ads to display live on our websites.&nbsp; That being said, since the online service we provide for vacationers is comparable to that offered by a newspaper or magazine through which vacation homes are advertised, we still recommend that you take steps to protect yourself by requesting references and a list of past renters from the homeowners before agreeing to rent. If possible, we also suggest that you visit the property in advance.</span></p>
<p style="text-align: justify;"><a href="#top">Back to Top</a></p>
<div style="text-align: justify;" align="center"><hr align="center" size="1" width="100%"></div>
<p style="text-align: justify;"><span style="color: #000000;"><strong><a name="Q2"></a>Q2: Can you help create or modify my ad?</strong></span></p>
<p style="text-align: justify;"><span style="color: #000000;">Of course,&nbsp;we are here to help!&nbsp; <a href="mailto:info@shoresummerrentals.com">E-mail</a> us for help in&nbsp;creating or modifying your ad.&nbsp; To edit your listing on your own, simply <a href="http://shoresummerrentals.com/sponsors/login.php">login</a> to your account and click on "Edit". Click here for&nbsp;<a href="http://shoresummerrentals.com/content/listing-instructions.html">Creating and/or Editing&nbsp;Instructions</a>.</span></p>
<p style="text-align: justify;"><a href="#top">Back to Top</a></p>
<div style="text-align: justify;" align="center"><hr align="center" size="1" width="100%"></div>
<p style="text-align: justify;"><strong><span style="color: #000000;"><a name="Q3"></a>Q3: I cannot see changes that I made to my ad</span></strong></p>
<p style="text-align: justify;"><span style="color: #000000;">All edits and changes are live and&nbsp;effective immediately. Since most browser&nbsp;pages are cached, you may&nbsp;need to click refresh your screen.&nbsp;&nbsp;Your listing changes will appear for everyone else who is visiting the page for the first time. If you see the correct information in your Account Center when logged into your account, your listing has been changed.</span></p>
<p style="text-align: justify;"><a href="#top">Back to Top</a></p>
<div style="text-align: justify;" align="center"><hr align="center" size="1" width="100%"></div>
<p style="text-align: justify;"><span style="color: #000000;"><strong><a name="Q4"></a>Q4: My new ad is not showing on the website</strong></span></p>
<p style="text-align: justify;"><span style="color: #000000;">All&nbsp;new rental listings need to be approved and activated by ShoreSummerRentals.com before it appears lives on our websites.&nbsp; All edits and changes to an already approved rental listing are&nbsp;effective immediately. </span></p>
<p style="text-align: justify;"><a href="#top">Back to Top</a></p>
<div style="text-align: justify;" align="center"><hr align="center" size="1" width="100%"></div>
<p style="text-align: justify;"><strong><span style="color: #000000;"><a name="Q5"></a>Q5: Which spot will my ad be displayed</span></strong></p>
<p style="text-align: justify;"><span style="color: #000000;">It all depends on the Main Membership that you purchase. We offer 4 Main Membership levels - <a href="http://shoresummerrentals.com/advertise.php">Bronze, Silver, Gold and Diamond</a>.&nbsp; The higher the membership level you purchase, the higher your rental listing will appear in the search results.&nbsp; <a href="http://shoresummerrentals.com/advertise.php">View Membership Details</a>.</span></p>
<p style="text-align: justify;"><a href="#top">Back to Top</a></p>
<div style="text-align: justify;" align="center"><hr align="center" size="1" width="100%"></div>
<p style="text-align: justify;"><strong><span style="color: #000000;"><a name="Q6"></a>Q6: What are the&nbsp;differences in your&nbsp;Main Membership types?</span></strong></p>
<p style="text-align: justify;"><span style="color: #000000;"><a href="http://shoresummerrentals.com/advertise.php">View Membership Details</a></span></p>
<p style="text-align: justify;"><a href="#top">Back to Top</a></p>
<div style="text-align: justify;" align="center"><hr align="center" size="1" width="100%"></div>
<p style="text-align: justify;"><strong><span style="color: #000000;"><a name="Q7"></a>Q7: I have more than one property in the same location, am I eligible for a discount?</span></strong></p>
<p style="text-align: justify;"><span style="color: #000000;">Yes, <a href="mailto:info@shoresummerrentals.com">e-mail</a> us for pricing.&nbsp; </span></p>
<p style="text-align: justify;"><a href="#top">Back to Top</a></p>
<div style="text-align: justify;" align="center"><hr align="center" size="1" width="100%"></div>
<p style="text-align: justify;"><strong><span style="color: #000000;"><a name="Q8"></a>Q8: Do you offer discounts for more than one property in separate locations?</span></strong></p>
<p style="text-align: justify;"><span style="color: #000000;">If you are listing multiple summer vacation rental properties at different locations, certain discounts may apply.&nbsp; Please <a href="mailto:info@shoresummerrentals.com">e-mail</a> us for more information about multiple property listing pricing.</span></p>
<p style="text-align: justify;"><a href="#top">Back to Top</a></p>
<div style="text-align: justify;" align="center"><hr align="center" size="1" width="100%"></div>
<p style="text-align: justify;"><span style="color: #000000;"><strong><a name="Q9"></a>Q9: What is an Add-On&nbsp;and is it included in my Main Membership?</strong></span></p>
<p style="text-align: justify;"><span style="color: #000000;">Add-On's&nbsp;are NOT included in ANY of our <a href="http://shoresummerrentals.com/advertise.php">Main Memberships</a>. It is an optional Add-On to enhance your rental listing and increase your exposire and rental income.&nbsp; We offer <a href="http://shoresummerrentals.com/advertise.php">3&nbsp;Add-On&nbsp;Packages </a>- Specials, Featured Property and&nbsp;Top Ranking.&nbsp; The Special's Add-On can be purchased online by logging into your account and clicking on Specials.&nbsp;&nbsp;You will need to <a href="mailto:info@shoresummerrentals.com">e-mail</a> us to purchase the Featured Property and/or one of the Top Ranking Add-On.&nbsp;</span></p>
<p style="text-align: justify;"><a href="#top">Back to Top</a></p>
<div style="text-align: justify;" align="center"><hr align="center" size="1" width="100%"></div>
<p style="text-align: justify;"><span style="color: #000000;"><strong><a name="Q10"></a>Q10: How Do I Prove Property Ownership?</strong></span></p>
<p style="text-align: justify;">We will contact you to request&nbsp;documentation if we cannot verify ownership on our own.</p>
<p style="text-align: justify;"><a href="#top">Back to Top</a></p>
<div style="text-align: justify;" align="center"><hr align="center" size="1" width="100%"></div>
<p style="text-align: justify;"><strong><span style="color: #000000;"><a name="Q11"></a>Q11: I am Having Trouble Uploading Photos</span></strong></p>
<p style="text-align: justify;"><span style="color: #000000;">If you have followed the directions exactly&nbsp;and nothing is happening or the system kicks you out, your photos may be too large. Please <a href="mailto:info@shoresummerrentals.com">e-mail</a>&nbsp;us the photos&nbsp;for us to resize and&nbsp;them into your rental listing.</span></p>
<p style="text-align: justify;"><a href="#top">Back to Top</a></p>
<div style="text-align: justify;" align="center"><hr align="center" size="1" width="100%"></div>
<p style="text-align: justify;"><span style="color: #000000;"><strong><a name="Q12"></a>Q12: How Do I&nbsp;Edit My Calendar?</strong></span></p>
<p style="text-align: justify;"><span style="color: #000000;">Login to your account and click on the Calendar tab.&nbsp; </span></p>
<p style="text-align: justify;"><a href="#top">Back to Top</a></p>
<div style="text-align: justify;" align="center"><hr align="center" size="1" width="100%"></div>
<p style="text-align: justify;"><span style="color: #000000;"><strong><a name="Q13"></a>Q13: Can I Switch Out Properties During the Year?</strong></span></p>
<p style="text-align: justify;"><span style="color: #000000;">Each membership is property specific and cannot be swapped with other properties once they sell or book up. You will need to purchase a separate listing for each property.</span></p>
<p style="text-align: justify;"><a href="#top">Back to Top</a></p>
<div style="text-align: justify;" align="center"><hr align="center" size="1" width="100%"></div>
<p style="text-align: justify;"><span style="color: #000000;"><strong><a name="Q14"></a>Q14: I Sold My Property, Can I Receive a Credit or Refund?</strong></span></p>
<p style="text-align: justify;"><span style="color: #000000;">No. There are no refunds for properties that are sold during their current membership. All listings are sold to run the full term that is chosen by the member.&nbsp; If you sell your property and no longer wish for the listing to remain online, please <a href="mailto:info@shoresummerrentals.com">e-mail</a>&nbsp;us and we will&nbsp;remove&nbsp;the listing without refund.</span></p>
<p style="text-align: justify;"><span style="color: #000000;">In the event you wish to remove your listing temporarily, we can inactivate the listing&nbsp;and reactivate&nbsp;it at a future date during your current subscription term.&nbsp; Your&nbsp;listing will not&nbsp;receive credit for the&nbsp;inactive time period.&nbsp; <br></span><br><a href="#top">Back to Top</a></p>
<div style="text-align: justify;" align="center"><hr align="center" size="1" width="100%"></div>
<p style="text-align: justify;"><span style="color: #000000;"><strong><a name="Q15"></a>Q15: Can You Renew My Listing With Last Years Credit Card Information?</strong></span></p>
<p style="text-align: justify;"><span style="color: #000000;">No. For your security, we do not save credit card information on file. Your listing will NOT automatically renew. You will need to login to your account, click on Renew Listing and enter updated credit card information to renew. You can also mail a check to P.O. Box 55, Somers Point, NJ 08244 or call 877-SHORE-4U (746-7348) with your credit card number for us to manual process it for you.</span></p>
<p style="text-align: justify;"><a href="#top">Back to Top</a></p>
<div style="text-align: justify;" align="center"><hr align="center" size="1" width="100%"></div>
<p style="text-align: justify;"><span style="color: #000000;"><strong><a name="Q16"></a>Q16: Can I Reserve a Vacation Rental Property through ShoreSummerRentals.com?</strong></span></p>
<p style="text-align: justify;"><span style="color: #000000;">No. We are strictly an advertising tool so all transactions must be coordinated through the renter and the property owner directly. <a href="http://shoresummerrentals.com/advsearch">Click here</a> to search for the perfect vacation rental property.&nbsp; Once you find homes you like,&nbsp;click on the Contact Owner tab.</span></p>
<p style="text-align: justify;"><a href="#top">Back to Top</a></p>
<div style="text-align: justify;" align="center"><hr align="center" size="1" width="100%"></div>
<p style="text-align: justify;"><strong><span style="color: #000000;">Membership Prices, Details and Payments</span></strong></p>
<p style="text-align: justify;"><span style="color: #000000;"><a name="Q17"></a>Q17: <strong>One Year Membership Term Only</strong></span></p>
<p style="text-align: justify;"><span style="color: #000000;">We offer ONE year <a href="http://shoresummerrentals.com/advertise.php">memberships</a> only.</span></p>
<p style="text-align: justify;"><a href="#top">Back to Top</a></p>
<div style="text-align: justify;" align="center"><hr align="center" size="1" width="100%"></div>
<p style="text-align: justify;"><strong><span style="color: #000000;"><a name="Q18"></a>Q18: Are there setup fees involved to create my vacation rental property advertisement?</span></strong></p>
<p style="text-align: justify;"><span style="color: #000000;">No. We are here to help! If you are not computer savvy, we will be happy to create your vacation rental property listing(s) at no additional cost.&nbsp; <a href="mailto:info@shoresummerrentals.com">E-mail</a>&nbsp;us to request a&nbsp;Property Detail Form.&nbsp; We will need you to fill it out in it's entirety and mail or fax it to us with payment along with proof of ownership.&nbsp; Once this information is received and verified, we will have your rental listing up and running within 24 hours.&nbsp;&nbsp;We will contact you at that point&nbsp;for you to review for accuracy.</span></p>
<p style="text-align: justify;"><a href="#top">Back to Top</a></p>
<div style="text-align: justify;" align="center"><hr align="center" size="1" width="100%"></div>
<p style="text-align: justify;"><strong><span style="color: #000000;"><a name="Q19"></a>Q19: Are there management or referral fees involved with ShoreSummerRentals.com?</span></strong></p>
<p style="text-align: justify;"><span style="color: #000000;">No. There are NO management or referral fees involved with Shore SummerRentals.com. Your only fee is the&nbsp;Main Membership cost and Add-On Package (optional) cost for a one-year listing of your vacation rental property. There are no hidden charges or unexpected terms.</span></p>
<p style="text-align: justify;"><a href="#top">Back to Top</a></p>
<div style="text-align: justify;" align="center"><hr align="center" size="1" width="100%"></div>
<p style="text-align: justify;"><strong><span style="color: #000000;"><a name="Q20"></a>Q20: Do I have to have a credit card to sign up?</span></strong></p>
<p style="text-align: justify;"><span style="color: #000000;">No. You can mail a check made payable to ShoreSummerRentals.com, P.O. Box 55, Somers Point, NJ 08244. Before mailing check, please register on the site so that I have your contact information or include your contact information with check. Once the check is received, I will create a blank ad for you to update at your convenience. If you would like us to prepare the rental listing&nbsp;for you, <a href="mailto:info@shoresummerrentals.com">e-mail</a>&nbsp;us to request a&nbsp;Property Detail Form.&nbsp; We will need you to fill it out in it's entirety and mail or fax it to us with payment along with proof of ownership.&nbsp; Once this information is received and verified, we will have your rental listing up and running within 24 hours.&nbsp;&nbsp;We will contact you at that point&nbsp;for you to review for accuracy.</span></p>
<p style="text-align: justify;"><a href="#top">Back to Top</a></p>
<div style="text-align: justify;" align="center"><hr align="center" size="1" width="100%"></div>
<p style="text-align: justify;"><span style="color: #000000;"><strong><a name="Q21"></a>Q21: Main Membership Details and Pricing</strong></span></p>
<p style="text-align: justify;"><a href="http://shoresummerrentals.com/advertise.php">Click here to view details and pricing</a>.</p>
<p style="text-align: justify;"><a href="#top">Back to Top</a></p>
<div style="text-align: justify;" align="center"><hr align="center" size="1" width="100%"></div>
<p style="text-align: justify;"><strong><span style="color: #000000;"><a name="Q22"></a>Q22: Add-On Package Details and Pricing</span></strong></p>
<p style="text-align: justify;"><a href="http://shoresummerrentals.com/advertise.php">Click here to view details and pricing</a>.</p>
<p style="text-align: justify;"><a href="#top">Back to Top</a></p>
<div style="text-align: justify;" align="center"><hr align="center" size="1" width="100%"></div>
<p style="text-align: justify;"><strong><span style="color: #000000;"><a name="Q23"></a>Q23: Making Payments</span></strong></p>
<p style="text-align: justify;"><span style="color: #000000;"><span style="text-decoration: underline;">By Check</span>: please allow at least two weeks from the time it is mailed before it is processed. A notification by email is sent after each payment is processed. If you do not receive an email from us after 2 weeks, please <a href="http://www.shoresummerrentals.com/contactus.php">contact us</a>. </span><br><br><span style="color: #000000;"><span style="text-decoration: underline;">By Credit Card</span>: your credit card payment is usually processed immediately, but may take up to 24 hours. Upon receipt of payment, you will receive an email confirmation.&nbsp; If you did not receive an email confirmation, your payment was most likely not processed.&nbsp; To be sure,&nbsp;<a href="http://shoresummerrentals.com/sponsors/login.php">login</a>&nbsp;to your account to&nbsp;check the status of your listing. If original purchase, you will see a property in your account center that needs to be created with an expiration date a year from the current date. If it is a renewal, please check your listing expiration date to see if it extended another year from your last expiration date. </span></p>
<p style="text-align: justify;"><a href="#top">Back to Top</a></p>
<div style="text-align: justify;" align="center"><hr align="center" size="1" width="100%"></div>
<p style="text-align: justify;"><strong><span style="color: #000000;"><a name="Q24"></a>Q24: Do You Have Auto Renewal Billing?</span></strong></p>
<p style="text-align: justify;"><span style="color: #000000;">No. For your security, we do NOT save credit card information on file. A renewal needs to be initiated by you by logging into your account, clicking on Renew Listing and entering your up to date credit card information. You can also mail a check made payable to ShoreSummerRental.com and mailed to P.O. Box 55, Somers Point, NJ 08244.</span></p>
<p style="text-align: justify;"><a href="#top">Back to Top</a></p>
<div style="text-align: justify;" align="center"><hr align="center" size="1" width="100%"></div>
<p style="text-align: justify;"><span style="color: #000000;"><strong><a name="Q25"></a>Q25: Rental Guarantee</strong></span></p>
<p style="text-align: justify;">View <a href="http://shoresummerrentals.com/content/guarantee-and-verify.html">Rental Guarantee Details</a>.</p>
<p style="text-align: justify;"><a href="#top">Back to Top</a></p>
<div style="text-align: justify;" align="center"><hr align="center" size="1" width="100%"></div>
<p style="text-align: justify;"><strong><span style="color: #000000;"><a name="Q26"></a>Q26:&nbsp;My Property is Booked,&nbsp;I Decided Not to Rent My Property or I Sold my Property Before My Subscription Expired, Can I Get a Refund for the Time Not Used?</span></strong></p>
<p style="text-align: justify;"><span style="color: #000000;">No.&nbsp; Cancelling a subscription before the maximum duration does not result or entitle the property owner to a refund for any unused listing time.&nbsp; The sale of your property, securing a full time renter or property removal from rental market does not constitute a refund.</span><span style="color: #000000;"> All listings are sold to run the full term that is chosen by the member.&nbsp; </span></p>
<p style="text-align: justify;"><span style="color: #000000;">If you sell your property and no longer wish for the listing to remain online, please <a href="mailto:info@shoresummerrentals.com">e-mail</a>&nbsp;us and we will&nbsp;remove&nbsp;the listing without refund.&nbsp; </span><span style="color: #000000;">In the event you wish to remove your listing temporarily, we can inactivate the listing&nbsp;and reactivate&nbsp;it at a future date during your current subscription term.&nbsp; Your&nbsp;listing will not&nbsp;receive credit for the&nbsp;inactive time period.&nbsp; Natural disasters or sale of a home does not constitute a refund. </span></p>
<p style="text-align: justify;"><a href="#top">Back to Top</a></p>
<div style="text-align: justify;" align="center"><hr align="center" size="1" width="100%"></div>
<p style="text-align: justify;"><strong><span style="color: #000000;"><a name="Q27"></a>Q27: Payment Address</span></strong></p>
<p style="text-align: justify;"><span style="color: #000000;">Please make checks payable to:</span><br><br><span style="color: #000000;">ShoreSummerRentals.com </span><br><span style="color: #000000;">P.O. Box 55</span><br><span style="color: #000000;">Somers Point, NJ 08244</span></p>
<p style="text-align: justify;"><a href="#top">Back to Top</a></p>
<div style="text-align: justify;" align="center"><hr align="center" size="1" width="100%"></div>
<p style="text-align: justify;"><strong><span style="color: #000000;">Security</span></strong></p>
<p style="text-align: justify;"><strong><span style="color: #000000;"><a name="Q28"></a>Q28: Is&nbsp;the ShoreSummerRentals.com site secure?</span></strong></p>
<p style="text-align: justify;"><span style="color: #000000;">Shore SummerRentals.com have employed sophisticated technology to ensure the most secure website for our advertisers and visitors. Our website was designed with your security in mind.</span></p>
<p style="text-align: justify;"><a href="#top">Back to Top</a></p>
<div style="text-align: justify;" align="center"><hr align="center" size="1" width="100%"></div>
<p style="text-align: justify;"><strong><span style="color: #000000;">Exposure</span></strong></p>
<p style="text-align: justify;"><strong><span style="color: #000000;"><a name="Q29"></a>Q29: How much web traffic does ShoreSummerRentals.com get?</span></strong></p>
<p style="text-align: justify;"><span style="color: #000000;">The Shore SummerRentals.com website has been a high traffic resource for the past 12 years&nbsp;receiving millions&nbsp;of hits per month.&nbsp; Traffic, however, is a relative measure. What counts is the quality of traffic received on the website, not just the quantity. We pride ourselves in the volume of quality inquiries received on a regular basis, i.e., serious clients looking to rent your vacation rental property. We will strive daily to provide the same results with ShoreSummerRentals.com.</span></p>
<p style="text-align: justify;"><a href="#top">Back to Top</a></p>
<div style="text-align: justify;" align="center"><hr align="center" size="1" width="100%"></div>
<p style="text-align: justify;"><strong><span style="color: #000000;"><a name="Q30"></a>Q30: How will I know if people are looking at my vacation rental listing?</span></strong></p>
<p style="text-align: justify;"><span style="color: #000000;">Once your vacation rental property is in our index and published on the website, you can monitor the visits to your vacation rental property page.&nbsp; </span><span style="color: #000000;">Email inquiries from potential property renters are stored on your account for easy reference. We provide you with a real-time page counter that keeps track of all your hits. To view the page counter, just click on your vacation rental property details and look at the counter located at the bottom of the page. </span></p>
<p style="text-align: justify;"><a href="#top">Back to Top</a></p>
<div style="text-align: justify;" align="center"><hr align="center" size="1" width="100%"></div>
<p style="text-align: justify;"><span style="color: #000000;"><strong><a name="Q31"></a>Q31: How will I receive vacation property rental inquiries?</strong></span></p>
<p style="text-align: justify;"><span style="color: #000000;">Once a potential renter finds your vacation rental property, they will complete a simple inquiry form and the information will be sent to your chosen email account.&nbsp;&nbsp; You will also receive a text message (optional) to alert you that someone emailed you about renting your home.&nbsp; The quicker&nbsp;the owners respond, the more rentals they secure.&nbsp; </span><span style="color: #000000;">Additionally, your telephone number will be available for people who prefer to speak with a live person.</span></p>
<p style="text-align: justify;"><a href="#top">Back to Top</a></p>
<div style="text-align: justify;" align="center"><hr align="center" size="1" width="100%"></div>
<p style="text-align: justify;"><strong><span style="color: #000000;"><a name="Q32"></a>Q32: How do you promote the ShoreSummerRentals.com website?</span></strong></p>
<p style="text-align: justify;"><span style="color: #000000;">Results are key, and to that end we have gone to great lengths to make sure that your summer vacation rental property listings on the ShoreSummerRentals.com website receive maximum exposure. With extensive advertising, paid search engine placement, and a search engine optimization, we make sure that you get the most value for your listings. </span><br><br><span style="color: #000000;">Paid Search Engine Placement </span><br><span style="color: #000000;">We partner with top search engines and directories so that our website and your listings receive the most exposure possible. </span><br><br><span style="color: #000000;">Search Engine Optimized Website</span><br><span style="color: #000000;">By making sure that our site is optimized for search engine indexing and is search engine friendly, we have been able to achieve phenomenal results with many of the top search engines and indexes on the Internet. We continually strive to make our site as easily accessible and well placed as possible.</span></p>
<p style="text-align: justify;"><a href="#top">Back to Top</a></p>
<div style="text-align: justify;" align="center"><hr align="center" size="1" width="100%"></div>
<p style="text-align: justify;"><span style="color: #000000;"><strong><a name="Q33"></a>Q33: What are the advantages of listing my vacation rental property on the&nbsp;ShoreSummerRentals.com website?</strong></span></p>
<p style="text-align: justify;"><span style="color: #000000;">The most important advantage is value. We offer you exceptional value when you advertise your vacation rental on ShoreSummerRentals.com. We are also local and very accessible.&nbsp; We have incorporated extensive techniques to make our site user-friendly for both potential renters and paid advertisers alike.</span></p>
<p style="text-align: justify;"><a href="#top">Back to Top</a></p>
<div style="text-align: justify;" align="center"><hr align="center" size="1" width="100%"></div>
<p style="text-align: justify;"><strong><span style="color: #000000;">Location</span></strong></p>
<p style="text-align: justify;"><strong><span style="color: #000000;"><a name="Q34"></a>Q34: What shore vacation towns are included for rental properties on the ShoreSummerRentals.com website?</span></strong></p>
<p style="text-align: justify;"><span style="color: #000000;">ShoreSummerRentals.com now accommodates vacation rental properties in all the United States. We are currently looking for properties in all US towns.</span></p>
<p style="text-align: justify;"><a href="#top">Back to Top</a></p>
<div style="text-align: justify;" align="center"><hr align="center" size="1" width="100%"></div>
<p style="text-align: justify;"><strong><span style="color: #000000;"><a name="Q35"></a>Q35: Besides Jersey Shore rental properties, what other vacation rental destinations are featured on our site?</span></strong></p>
<p style="text-align: justify;"><span style="color: #000000;">ShoreSummerRentals.com also has a sister site, <a href="http://www.VacationRentals2U.com">VacationRentals2U.com&nbsp;</a>which now accommodates all vacation rental types such as the grand canyon, ski resorts, Disney, etc.&nbsp; We are currently looking for properties in all United Stated towns.</span></p>
<p style="text-align: justify;"><a href="#top">Back to Top</a></p>
<div style="text-align: justify;" align="center"><hr align="center" size="1" width="100%"></div>
<p style="text-align: justify;"><strong><span style="color: #000000;"><a name="Q36"></a>Q36: Does the&nbsp;ShoreSummerRentals.com website also promote winter vacation rental properties?</span></strong></p>
<p style="text-align: justify;"><span style="color: #000000;">Yes. We can accommodate vacation rentals throughout the entire year - Spring, Summer, Fall and Winter getaways right at your fingertips. This also includes popular East Coast vacation destinations, such as Disney World in Florida all the way to the Grand Canyon. Please bookmark&nbsp;Shore SummerRentals.com or <a href="http://www.shoresummerrentals.com/contactus.php">contact us</a> for additional information.</span></p>
<p style="text-align: justify;"><a href="#top">Back to Top</a></p>
<div style="text-align: justify;" align="center"><hr align="center" size="1" width="100%"></div>
<p style="text-align: justify;"><strong><span style="color: #000000;">Ad and Account Management</span></strong></p>
<p style="text-align: justify;"><strong><span style="color: #000000;"><a name="Q37"></a>Q37: Listing Title Tips</span></strong></p>
<p style="text-align: justify;"><span style="color: #000000;">The Listing Title is very important. It is the first things renters see and needs to grab their attention. We have a few tips and suggestions:</span></p>
<p style="text-align: justify;"><span style="color: #000000;">Check that you use correct spelling</span></p>
<p style="text-align: justify;"><span style="color: #000000;">The use of symbols, numbers, or letters must adhere to the true meaning of the symbol.</span></p>
<p style="text-align: justify;"><span style="color: #000000;">Avoid excessive capitalization, repeated and unnecessary punctuation or symbols, and repetition Change your title often to keep your listing fresh.</span></p>
<p style="text-align: justify;"><a href="#top">Back to Top</a></p>
<div style="text-align: justify;" align="center"><hr align="center" size="1" width="100%"></div>
<p style="text-align: justify;"><strong><span style="color: #000000;"><a name="Q38"></a>Q38: Listing Guidelines</span></strong></p>
<p style="text-align: justify;"><span style="color: #000000;"><strong>We have certain guidelines that need to be adhered to when creating your ad.</strong> </span><br><br><span style="color: #000000;"><strong>External URLs</strong> </span><br><span style="color: #000000;">Shore SummerRentals.com has a dedicated area in our listings to place up to 3 external URL's that pertain to your property such as a personal website you created for your property with more photos, descriptions, etc.&nbsp; Links to any of our competitor's site is strictly PROHIBITED and will be removed from your ad. You will be notified that this was done and warned that if it is placed on the ad again, your rental listing&nbsp;will be removed from our websites without refund. </span><br><br><span style="color: #000000;"><strong>Email Address in Body of Ad Prohibited</strong> </span><br><span style="color: #000000;">Listings that contain email addresses are prime targets for email collecting spiders that look for email addresses for mailing lists that they sell to spammers. This is how SPAM is sent. If email addresses are present in our listings we will get spidered by these email collectors which will slow down the site, lower the listing views and inquires, and cause property owners to be spammed. </span><br><br><span style="color: #000000;"><strong>Each Listing is for ONE Property in ONE Location.<br></strong> Ads for multiple homes are prohibited.&nbsp;&nbsp;ShoreSummerRentals.com listings are designed to advertise ONE PROPERTY in ONE LOCATION. You may NOT list more than one property per listing. If this is observed, you will be warned to remove the additional property advertisement, photos, etc. If you do not remove the information, ShoreSummerRentals.com has the right to deactivate your listing with no refund. </span></p>
<p style="text-align: justify;"><span style="color: #000000;"><strong>Closest Appropriate Region.<br></strong> Your listing must be placed in the closest appropriate region to your property. <br><br><strong>Accurate Property Information</strong></span><br><span style="color: #000000;">Your listing must reflect accurate information about the property. </span><br><br><strong><span style="color: #000000;">Plagiarizing Listings Prohibited</span></strong><br><span style="color: #000000;">Please do not copy information from another listing. </span><br><br><span style="color: #000000;"><strong>Review and Approval of Ads</strong> </span><br><span style="color: #000000;">All listings must be reviewed by&nbsp;ShoreSummerRentals.com to ensure that they adhere to the guidelines specified in each section above. When building your listing, please double-check every field to ensure they don't include such items as, incorrect information, the characters &lt; or &gt;, email addresses, phone numbers or websites. These items will cause your listing to be rejected now or possibly deleted at a later time.&nbsp;Shore SummerRentals.com reserves the right to alter or permanently remove any listing that violates our guidelines or we feel could compromise the potential renter and/or our site's integrity without warning or notice.</span></p>
<p style="text-align: justify;"><a href="#top">Back to Top</a></p>
<div style="text-align: justify;" align="center"><hr align="center" size="1" width="100%"></div>
<p style="text-align: justify;"><strong><span style="color: #000000;"><a name="Q39"></a>Q39: Forgot Username or Password</span></strong></p>
<p style="text-align: justify;"><span style="color: #000000;">If you forget your username or password, you may obtain it by going to the&nbsp;<a href="http://shoresummerrentals.com/sponsors/login.php">login</a> page and&nbsp;clicking on the Forgot Your Password link.&nbsp; Enter your email address and your password will then be mailed to your email address, assuming you have previously registered with us, and the email address you enter is the same one you registered with.</span></p>
<p style="text-align: justify;"><a href="#top">Back to Top</a></p>
<div style="text-align: justify;" align="center"><hr align="center" size="1" width="100%"></div>
<p style="text-align: justify;"><strong><span style="color: #000000;"><a name="Q40"></a>Q40: Transfer a listing</span></strong></p>
<p style="text-align: justify;"><span style="color: #000000;">Shore SummerRentals.com listings are transferable in the event the property was sold.&nbsp;&nbsp;&nbsp;We can transfer the rental listing to the new owner's account with the permission of the original owner of the home and rental listing.&nbsp;&nbsp;They have to be advertising the same property.&nbsp; There is a $25.00 transfer fee.&nbsp; <a href="http://www.shoresummerrentals.com/contactus.php">Contact us</a>&nbsp;for details.&nbsp; If you wish to cancel your service due to a sale or transfer, the cancellation policy above will apply. No pro-rated refunds will be offered to terminate your listing due to the sale of a property or if taken off of the rental market.</span></p>
<p style="text-align: justify;"><a href="#top">Back to Top</a></p>
<div style="text-align: justify;" align="center"><hr align="center" size="1" width="100%"></div>
<p style="text-align: justify;"><strong><span style="color: #000000;"><a name="Q41"></a>Q41: Change Account Holder and Contact Owner Email Address</span></strong><br><br><span style="color: #000000;">Please note that there are two locations to enter email addresses.&nbsp; </span></p>
<p style="text-align: justify;"><span style="color: #000000;">There is an account holder email address which is the email of the person in charge of the account.&nbsp; This&nbsp;email is&nbsp;used to login to your account and where correspondence from management such as invoices, alerts, tips, etc.&nbsp; will be sent.&nbsp;&nbsp;</span></p>
<p style="text-align: justify;"><strong><span style="color: #000000;"><span style="text-decoration: underline;">Update Account Holder Email</span>:</span></strong></p>
<p style="text-align: justify;"><span style="color: #000000;">Once <a href="http://shoresummerrentals.com/sponsors/login.php">logged in</a>&nbsp;to your dashboard, click on "Account".&nbsp; This is where you&nbsp;edit all the information for the account holder such as email,&nbsp;password, mailing address,&nbsp;phone numbers, etc. &nbsp;Make sure to save all changes.</span></p>
<p style="text-align: justify;"><span style="color: #000000;">There is also an email address for the person who would like to receive the email inquiries from the renters.&nbsp; The email addresses can&nbsp;be the same or different.&nbsp; </span></p>
<p style="text-align: justify;"><strong><span style="color: #000000;"><span style="text-decoration: underline;">Update Contact Owner&nbsp;Email</span>:</span></strong></p>
<p style="text-align: justify;"><span style="color: #000000;">Once <a href="http://shoresummerrentals.com/sponsors/login.php">logged in</a>&nbsp;to your dashboard, click on "Edit".&nbsp; This is where you&nbsp;edit some of the property details and the email of the person you want to receive renter email inquiries.&nbsp; Make sure to save all changes.</span></p>
<p style="text-align: justify;"><a href="#top">Back to Top</a></p>
<div style="text-align: justify;" align="center"><hr align="center" size="1" width="100%"></div>
<p style="text-align: justify;"><strong><span style="color: #000000;"><a name="Q42"></a>Q42: Modify/Delete Listings</span></strong></p>
<p style="text-align: justify;"><span style="color: #000000;">You have full access to your listings 24 hours a day. You can modify any portion of your listing at any time, including the pictures.&nbsp;Great flexibility and you control all of it.&nbsp; View <a href="http://www.shoresummerrentals.com/content/listing-instructions.html">editing instructions</a>.</span></p>
<p style="text-align: justify;"><a href="#top">Back to Top</a></p>
<div style="text-align: justify;" align="center"><hr align="center" size="1" width="100%"></div>
<p style="text-align: justify;"><strong><span style="color: #000000;"><a name="Q43"></a>Q43: Having Trouble Registering on Website</span></strong></p>
<p style="text-align: justify;"><span style="color: #000000;">If you attempt to register on the site&nbsp;and receive a message saying "the&nbsp;current user is already in the database".&nbsp; This means you are already registered with us with that same email address.&nbsp;&nbsp;If this is the case, please <a href="http://www.shoresummerrentals.com/contactus.php">contact us</a>&nbsp;to request your login information.</span></p>
<p style="text-align: justify;"><a href="#top">Back to Top</a></p>
<div style="text-align: justify;" align="center"><hr align="center" size="1" width="100%"></div>
<p style="text-align: justify;"><strong><span style="color: #000000;"><a name="Q44"></a>Q44: What Web Browsers are Supported by Your Websites?<br></span></strong></p>
<p style="text-align: justify;"><span style="color: #000000;">At ShoreSummerRentals, we strive to provide our customers with a modern, fast, safe and secure web browsing experience.<br><br></span><strong><span style="color: #000000;">In order to provide you with a great user experience, we officially support the following browsers:<br></span></strong><span style="color: #000000;">* Internet Explorer 10 or higher</span><span style="color: #000000;"><br>* Firefox 32 or higher</span><span style="color: #000000;"><br>* Chrome 38 or higher</span><span style="color: #000000;"><br>* Safari 7 or higher</span><br><br><strong><span style="color: #000000;">On mobile devices, we officially support the following:<br></span></strong><span style="color: #000000;"><strong>* </strong>iOS Safari 7 or higher (iPhone and iPad)</span><span style="color: #000000;"><br>* Android Chrome 38 or higher (Android Phones and Tablets)<br><br>While our sites may continue to function on other devices and in other browsers than those listed, occasionally you may see a warning if you are using a browser or device where ShoreSummerRentals has known issues. &nbsp;While we do our best to support browsers that are most commonly in use, it is not possible to support all, and therefore we recommend upgrading to one of the above mentioned browsers.<br><br></span><span style="color: #000000;">To view instructions on how to upgrade your internet browser, visit&nbsp;<span style="color: #0000ff;"><a href="http://browsehappy.com/" target="_blank"><span style="color: #0000ff;">http://browsehappy.com/</span></a>.</span></span></p>
<p style="text-align: justify;"><a href="#top">Back to Top</a></p>
<div style="text-align: justify;" align="center"><hr align="center" size="1" width="100%"></div>
<p style="text-align: justify;"><strong><span style="color: #000000;"><a name="Q45"></a>Q45: Change Password</span></strong></p>
<p style="text-align: justify;"><span style="color: #000000;"><span style="color: #000000;">Once <a href="http://shoresummerrentals.com/sponsors/login.php">logged in</a>&nbsp;to your dashboard, click on "Account".&nbsp; This is where you&nbsp;can change your password as well as the main Account Holder's information.&nbsp; Make sure to save all changes.</span></span></p>
<p style="text-align: justify;"><a href="#top">Back to Top</a></p>
<div style="text-align: justify;" align="center"><hr align="center" size="1" width="100%"></div>
<p style="text-align: justify;"><strong><span style="color: #000000;"><a name="Q46"></a>Q46: Property Address Visible on Ad</span></strong></p>
<p style="text-align: justify;"><span style="color: #000000;">Although we highly recommend&nbsp;displaying the address of your rental property, please <a href="http://www.shoresummerrentals.com/contactus.php">contact us</a> if you do not wish to have it displayed on your ad on the <span style="color: #000000;">live website.&nbsp; It&nbsp;</span>is more likely for a renter to be interested (and less suspicious of a scam)&nbsp;if they know the location.&nbsp; Most renters will&nbsp;skip properties that do not display addresses.&nbsp;&nbsp;We do require the address for our records.&nbsp; Listings that do not display the address are not eligble for the <a href="http://www.shoresummerrentals.com/content/guarantee-and-verify.html">Rental Guarantee.</a><br></span></p>
<p style="text-align: justify;"><a href="#top">Back to Top</a></p>
<div style="text-align: justify;" align="center"><hr align="center" size="1" width="100%"></div>
<p style="text-align: justify;"><strong><span style="color: #000000;"><a name="Q47"></a>Q47: How Can I Protect my Privacy</span></strong></p>
<p style="text-align: justify;"><span style="color: #000000;"><br></span></p>
<p style="text-align: justify;"><a href="#top">Back to Top</a></p>
<div style="text-align: justify;" align="center"><hr align="center" size="1" width="100%"></div>
<p style="text-align: justify;"><strong><span style="color: #000000;"><a name="Q48"></a>Q48: Do I Need to Know HTML or How to Design my Summer Vacation Rental Page?</span></strong></p>
<p style="text-align: justify;"><span style="color: #000000;">Nope. When you join ShoreSummerRentals.com, you will complete a simple online form that walks you though the process of creating your rental listing.&nbsp; You will be able to enter long detailed descriptions, amenities, photos, rates and availability for your rental home.&nbsp; If you need help, we are here for you!&nbsp; <a href="http://www.shoresummerrentals.com/content/listing-instructions.html">Creating and/or Editing&nbsp;Instructions.</a></span></p>
<p style="text-align: justify;"><a href="#top">Back to Top</a></p>
<div style="text-align: justify;" align="center"><hr align="center" size="1" width="100%"></div>
<p style="text-align: justify;"><strong><span style="color: #000000;"><a name="Q49"></a>Q49: Do I Need to Create 2&nbsp;Listings&nbsp;for my home to appear on ShoreSummerRentals.com and VacationRentals2U.com?<br></span></strong><span style="color: #000000;">Nope.&nbsp; </span><span style="color: #000000;">Once you have added your vacation rental property to the ShoreSummerRentals.com website, your information will duplicate on the <a href="http://www.VacationRentals2U.com">VacationRentals2U.com</a>&nbsp;website. &nbsp;No need to enter your property information twice, even though you will get twice the exposure by being on two websites. </span></p>
<p style="text-align: justify;"><a href="#top">Back to Top</a></p>
<div style="text-align: justify;" align="center"><hr align="center" size="1" width="100%"></div>
<p style="text-align: justify;"><strong><span style="color: #000000;"><a name="Q50"></a>Q50: Who Updates my Listings?</span></strong></p>
<p style="text-align: justify;"><span style="color: #000000;">You do! We provide you with a secure Web-based administration tool which allows you to update the details of your vacation rental property such as availability, rates, description, title, amenities, photographs, rental policies and terms. We make sure that your vacation rental listing page is immediately updated on both the VacationRentals2U.com and ShoreSummerRentals.com sites.</span></p>
<p style="text-align: justify;"><a href="#top">Back to Top</a></p>
<div style="text-align: justify;" align="center"><hr align="center" size="1" width="100%"></div>
<p style="text-align: justify;"><strong><span style="color: #000000;"><a name="Q51"></a>Q51: May I provide links to other web pages external to my ad?</span></strong></p>
<p style="text-align: justify;"><span style="color: #000000;">In general, the answer is yes; however there are rules concerning which links are acceptable and which links are not.&nbsp; We have designated areas for up to 3 external links that you will see when entering your property details.&nbsp; External links are not permitted to be displayed in the description field or any other area except where designated.&nbsp; We do ask for the link to point to another web page that contains additional information about that property. If you have multiple listings with us, you may link your Cape May vacation rental, for example, to your Wildwood vacation rental page, and vice versa. We reserve the right to remove links to websites that directly compete with VacationRentals2U.com and ShoreSummerRentals.com.</span></p>
<p style="text-align: justify;"><a href="#top">Back to Top</a></p>
<div style="text-align: justify;" align="center"><hr align="center" size="1" width="100%"></div>
<p style="text-align: justify;"><strong><span style="color: #000000;">Google Maps</span></strong></p>
<p style="text-align: justify;"><strong><span style="color: #000000;"><a name="Q52"></a>Q52: Google Maps</span></strong></p>
<p style="text-align: justify;"><span style="color: #000000;">ShoreSummerRentals.com now offers the Google map feature&nbsp;for visitors&nbsp;to see the exact location of your property. &nbsp;By entering your property address, the map feature will automatically activate showing the location of your rental home.&nbsp; <span style="color: #000000;">Please note that in order for the Google map feature to work, you need&nbsp;have your street address ONLY in the&nbsp;address field.&nbsp; For example, you cannot have&nbsp;"123 Summer Street (near 4th Street)" in the address field.&nbsp; It does seem to work if you have "123 Summer Street, 2nd Floor" though.&nbsp; If you still notice that the map is showing incorrectly, go to the property location field when logged in and tab out of the field.&nbsp; This fine tunes the map.<br></span></span></p>
<p style="text-align: justify;"><span style="color: #000000;"><a href="#top">Back to Top</a></span></p>
<div style="text-align: justify;" align="center"><hr align="center" size="1" width="100%"></div>
<p style="text-align: justify;"><strong><span style="color: #000000;"><a name="Q53"></a>Q53: Map is Not Showing My Location Accurately</span></strong></p>
<p style="text-align: justify;"><span style="color: #000000;">If you are not happy with the way Google Maps is displaying the location of your property, please follow the directions above in FAQ - Q52, check out <a href="https://support.google.com/maps/?hl=en#topic=3092425">Google Maps Support</a> or <a href="http://www.shoresummerrentals.com/contactus.php">contact us</a>.</span></p>
<p style="text-align: justify;"><a href="#top">Back to Top</a></p>
<div style="text-align: justify;" align="center"><hr align="center" size="1" width="100%"></div>
<p style="text-align: justify;"><span style="color: #000000;"><strong><a name="Q54"></a>Q54: Can I Disable the Map?</strong></span></p>
<p style="text-align: justify;"><span style="color: #000000;">No, you can not disable the map.<br></span></p>
<p style="text-align: justify;"><a href="#top">Back to Top</a></p>
<div style="text-align: justify;" align="center"><hr align="center" size="1" width="100%"></div>
<p style="text-align: justify;"><span style="color: #000000;"><strong>Photos</strong></span></p>
<p style="text-align: justify;"><span style="color: #000000;"><strong><a name="Q55"></a>Q55: How do I add and change photographs to my vacation rental listing?</strong></span></p>
<p style="text-align: justify;"><span style="color: #000000;">You are permitted to upload up to 32 appealing photos of your home. Once you upload the photo, click on “Caption” to give the photo a name.&nbsp; Click the SUBMIT button at the very bottom of this Listing Description page to save the photos.&nbsp; Simply Clicking the SUBMIT button when uploading, without clicking it once again on the Listing Description page, will NOT save them.&nbsp; Images must be under 10MB in size.&nbsp; If they are larger, please resize before uploading.&nbsp; * Please note that we are aware that the photos are not displaying in the order uploaded and are working on a solution.</span><br><br><span style="color: #000000;"><strong>There are several ways to get your photos posted on ShoreSummerRentals.com.</strong> </span><br><br><span style="color: #000000;">1. Upload them yourself. </span><br><br><span style="color: #000000;">2. E-mail them. Simply attach your photos and e-mail them to us at photos@shoresummerrentals.com. Be sure to indicate your name and vacation rental number so we know where to post your photos. </span><br><br><span style="color: #000000;">3. Send Us a Link. If your photos are located on another website or your vacation rental website simply e-mail us at photos@shoresummerrentals.com the website URL for each photo. If the site allows us to copy the photos, we will. </span><br><br><span style="color: #000000;">4. Snail-Mail a Disk. Let us do all the work for you. Mail your disk of photos to the address below. We will upload and add them to your vacation rental listing for you. </span><br><br><span style="color: #000000;">Vacations Rentals 2U.com / ShoreSummerRentals.com P.O. Box 55 Somers Point, NJ 08244</span></p>
<p style="text-align: justify;"><a href="#top">Back to Top</a></p>
<div style="text-align: justify;" align="center"><hr align="center" size="1" width="100%"></div>
<p style="text-align: justify;"><span style="color: #000000;"><strong><a name="Q56"></a>Q56: Additional Photo Tips<br></strong></span></p>
<p style="text-align: justify;"><span style="color: #000000;"><span style="color: #000000;">Pictures say a thousand words. I cannot stress enough how important photos are since most renters will not see your property until the day that they pull up to at the start of their vacation. Make sure your photos are very bright and you post all the rooms of the home as well as the surrounding areas. People love outdoor shots such as ocean views, the beach, the boardwalk, nightlife, etc. More pictures will put you ahead of all the competition and secure you significantly more rental income than owners who only post a couple dark interior photos.</span></span></p>
<p style="text-align: justify;"><a href="#top">Back to Top</a></p>
<div style="text-align: justify;" align="center"><hr align="center" size="1" width="100%"></div>
<p style="text-align: justify;"><span style="color: #000000;"><strong><a name="Q57"></a>Q57: <br></strong></span></p>
<p style="text-align: justify;"><span style="color: #000000;"><br></span></p>
<p style="text-align: justify;"><a href="#top">Back to Top</a></p>
<div style="text-align: justify;" align="center"><hr align="center" size="1" width="100%"></div>
<p style="text-align: justify;"><strong><span style="color: #000000;"><a name="Q58"></a>Q58: Can I Copy Photos from Another Website?</span></strong></p>
<p style="text-align: justify;"><span style="color: #000000;">Many other sites copyright their photos once you add them to your listing and thus they cannot be legally uploaded to our site without permission. You must obtain permission from the other site before uploading your photos to your listing.</span></p>
<p style="text-align: justify;"><a href="#top">Back to Top</a></p>
<div style="text-align: justify;" align="center"><hr align="center" size="1" width="100%"></div>
<p style="text-align: justify;"><strong><span style="color: #000000;"><a name="Q59"></a>Q59: Do You Charge to Update Photos or Make Changes to My Property Listings?</span></strong></p>
<p style="text-align: justify;"><span style="color: #000000;">No, we do not charge for updates to your listings. As a paid member, you are entitled to log into your account and make changes to your ad as you see fit. We provide you with the necessary online tools to easily update your vacation rental property listings.</span></p>
<p style="text-align: justify;"><a href="#top">Back to Top</a></p>
<div style="text-align: justify;" align="center"><hr align="center" size="1" width="100%"></div>
<p style="text-align: justify;"><span style="color: #000000;"><strong><a name="Q60"></a>Q60: My Text and Photos Were Copied</strong></span></p>
<p style="text-align: justify;">Since we do not copyright your listing text or photos, legally there isn't anything we can do but remove the content from our site. We recommend you ask the owner to remove any copied material in a non-threatening manner. If they refuse, we will co-operate as required by any legal proceeding. If you believe that your ad has been plagiarized, please <a href="http://www.shoresummerrentals.com/contactus.php">contact us.</a></p>
<p style="text-align: justify;"><a href="#top">Back to Top</a></p>
<div style="text-align: justify;" align="center"><hr align="center" size="1" width="100%"></div>
<p style="text-align: justify;"><strong><span style="color: #000000;"><a name="Q61"></a>Q61: </span></strong></p>
<p style="text-align: justify;">&nbsp;</p>
<p style="text-align: justify;"><a href="#top">Back to Top</a></p>
<div style="text-align: justify;" align="center"><hr align="center" size="1" width="100%"></div>
<p style="text-align: justify;"><strong><span style="color: #000000;">Calendar</span></strong></p>
<p style="text-align: justify;"><strong><span style="color: #000000;"><a name="Q62"></a>Q62: Do You Offer a Rental Property Availability Calendar?</span></strong></p>
<p style="text-align: justify;"><span style="color: #000000;">Yes, there is an availability calendar in which you simply select the dates your vacation rental property is unavailable. Renters can now search by property availability so please keep the calendar up to date. It's easy to use and is provided at no extra charge.</span></p>
<p style="text-align: justify;"><a href="#top">Back to Top</a></p>
<div style="text-align: justify;" align="center"><hr align="center" size="1" width="100%"></div>
<p style="text-align: justify;"><strong><span style="color: #000000;"><a name="Q63"></a>Q63: Where Do I Update My Calendar?</span></strong></p>
<p style="text-align: justify;"><span style="color: #000000;"><a href="http://shoresummerrentals.com/sponsors/login.php">Login to your account.</a><span style="color: #000000;"> Click on the Calendar link in your dashboard.</span>&nbsp; <span style="color: #000000;">Enter the arrival and departure dates of the reserved rental.&nbsp; </span><span style="color: #000000;">Enter the name of the renter or any character will suffice - this will not appear on the public site. </span><span style="color: #000000;">Click Submit.</span></span></p>
<p style="text-align: justify;"><a href="#top">Back to Top</a></p>
<div style="text-align: justify;" align="center"><hr align="center" size="1" width="100%"></div>
<p style="text-align: justify;"><strong><span style="color: #000000;"><a name="Q64"></a>Q64: Do I Have to Update My Calendar?</span></strong></p>
<p style="text-align: justify;"><span style="color: #000000;">While keeping a calendar up to date is not required, our research has shown that most vacationers skip over properties that show all availability open. It gives the renter a sense of urgency when they see others are interested in your property and that they should act quickly. If you still do not want to use the calendar feature, you can leave it blank.</span></p>
<p style="text-align: justify;"><a href="#top">Back to Top</a></p>
<div style="text-align: justify;" align="center"><hr align="center" size="1" width="100%"></div>
<p style="text-align: justify;"><strong><span style="color: #000000;"><a name="Q65"></a>Q65: Can I Link Another Calendar to My Ad?</span></strong></p>
<p style="text-align: justify;"><span style="color: #000000;">No. Our properties can be searched by availability according to OUR calendar.</span></p>
<p style="text-align: justify;"><a href="#top">Back to Top</a></p>
<div style="text-align: justify;" align="center"><hr align="center" size="1" width="100%"></div>
<p style="text-align: justify;"><strong><span style="color: #000000;">Rental Property Rates</span></strong></p>
<p style="text-align: justify;"><strong><span style="color: #000000;"><a name="Q66"></a>Q66: Where Do I Post My Rates?</span></strong></p>
<p style="text-align: justify;"><span style="color: #000000;"><a href="http://shoresummerrentals.com/sponsors/login.php">Login to your account. </a><span style="color: #000000;">Click on the Rate link in your dashboard. </span><span style="color: #000000;">Fill in the information regarding check-in, check-out, payment terms and any additional notes. </span><span style="color: #000000;">Click the Save button at the bottom of the page before entering rates to save what you entered so far. </span><span style="color: #000000;">Enter your rates. </span><span style="color: #000000;">Click the Save button. </span></span><span style="color: #000000;"><span style="color: #000000;">To edit rates already entered, place your curser in the field you want to update, make the change, hit Enter.</span></span><strong><span style="color: #000000;"><br><br></span></strong><a href="#top">Back to Top</a></p>
<div style="text-align: justify;" align="center"><hr align="center" size="1" width="100%"></div>
<p style="text-align: justify;"><strong><span style="color: #000000;"><a name="Q67"></a>Q67: Do I Have to Post Rates?</span></strong></p>
<p style="text-align: justify;"><span style="color: #000000;">New listings must have rates posted in order to qualify for the <a href="http://www.shoresummerrentals.com/content/guarantee-and-verify.html">Rental Guarantee.</a>&nbsp; While posting rates is not required once the Rental Guarantee period has passed, our research has shown that most vacationers skip over properties that do not have rates posted. <strong>Most properties are searched by rates so if your rates are not posted in the actual rate table, your ad will not show up when someone searches by rates.</strong>&nbsp; Knowing all of this and you still do not want to use the rate feature, you can leave it blank.</span></p>
<p style="text-align: justify;"><a href="#top">Back to Top</a></p>
<div style="text-align: justify;" align="center"><hr align="center" size="1" width="100%"></div>
<p style="text-align: justify;"><strong><span style="color: #000000;">Rental Coordination and Forms</span></strong></p>
<p style="text-align: justify;"><span style="color: #000000;"><a name="Q68"></a><strong>Q68: Who is responsible for writing the rental lease agreement?</strong></span></p>
<p style="text-align: justify;"><span style="color: #000000;">All vacation rental property leases must be coordinated by the property owner.&nbsp; We supply sample leases and welcome letters, a rental procedure checklist, and an inventory checklist form to registered members.&nbsp; <a href="http://www.shoresummerrentals.com/contactus.php">Contact us</a> to request any of those items.&nbsp; <br></span></p>
<p style="text-align: justify;"><a href="#top">Back to Top</a></p>
<div style="text-align: justify;" align="center"><hr align="center" size="1" width="100%"></div>
<p style="text-align: justify;"><strong><span style="color: #000000;"><a name="Q69"></a>Q69: Who determines the price I charge for my vacation rental property?</span></strong></p>
<p style="text-align: justify;"><span style="color: #000000;">You determine the price to charge for your vacation rental property. You may update your pricing structure at any time by logging into your account and clicking on Edit Rates.</span></p>
<p style="text-align: justify;"><a href="#top">Back to Top</a></p>
<div style="text-align: justify;" align="center"><hr align="center" size="1" width="100%"></div>
<p style="text-align: justify;"><span style="color: #000000;"><strong><a name="Q70"></a>Q70: How Do I Get the Renter the Key?</strong></span></p>
<p style="text-align: justify;"><span style="color: #000000;">You can either mail the keys to the renter a week before their scheduled vacation with a letter explaining what is expected during their stay and upon their departure along with directions to the property. You can include a padded self addressed envelope for them to drop in the mail after they leave. Another option would be to install a combination door lock so no key is needed at all, just a code. This way all you have to do is e-mail them a letter a week before their arrival with instructions, direvtions and codes. That is what I do and it works out great. I purchased a Schlage model at Lowe's.</span></p>
<p style="text-align: justify;"><a href="#top">Back to Top</a></p>
<div style="text-align: justify;" align="center"><hr align="center" size="1" width="100%"></div>
<p style="text-align: justify;"><strong><span style="color: #000000;"><a name="Q71"></a>Q71: <br></span></strong></p>
<p style="text-align: justify;"><span style="color: #000000;"><br></span></p>
<p style="text-align: justify;"><a href="#top">Back to Top</a></p>
<div style="text-align: justify;" align="center"><hr align="center" size="1" width="100%"></div>
<p style="text-align: justify;"><strong><span style="color: #000000;">Cancellation Policy</span></strong></p>
<p style="text-align: justify;"><strong><span style="color: #000000;"><a name="Q72"></a>Q72: May I cancel my vacation rental listing at any time?</span></strong></p>
<p style="text-align: justify;"><span style="color: #000000;">Yes, but no refunds will be given for just changing your mind. We do offer a <a href="http://www.shoresummerrentals.com/content/guarantee-and-verify.html">Rental Guarantee</a> which would allow a full refund within 90 days of the listing activation if the listing meets the criteria of our Rental Guarantee program.&nbsp; This is only for new owners who have not yet renewed a current listing on our site. This is only for the very first property an owner lists with us and they are not happy with the results they receive. Your ad will be removed and guarantees are only granted to one property once. If you sell your property, you are not eligible for a refund or credit for any unused time.</span></p>
<p style="text-align: justify;"><a href="#top">Back to Top</a></p>
<div style="text-align: justify;" align="center"><hr align="center" size="1" width="100%"></div>
<p style="text-align: justify;"><strong><span style="color: #000000;"><a name="Q73"></a>Q73: Can I delete my advertisement if I am no longer interested in advertising?</span></strong></p>
<p style="text-align: justify;"><span style="color: #000000;">Yes.&nbsp; Please <a href="http://www.shoresummerrentals.com/contactus.php">contact us</a> and we will delete your listing.&nbsp; Please note that once we delete it, we can no longer pull the information back if you decide to advertise later down the road.&nbsp; The listing will have to be recreated from scratch.&nbsp; If you only want to pause it until the following year, we can deactivate it so it does not show active on the websites without actually deleting the property from your account. This way when you are ready to advertise again, all you have to do is login to your account and click on Renew Listing.&nbsp; Payment will be required to reactivate the listing. No credit is given for unused time.</span></p>
<p style="text-align: justify;"><a href="#top">Back to Top</a></p>
<div style="text-align: justify;" align="center"><hr align="center" size="1" width="100%"></div>
<p style="text-align: justify;"><strong><span style="color: #000000;">Miscellaneous Questions</span></strong></p>
<p style="text-align: justify;"><strong><span style="color: #000000;"><a name="Q74"></a>Q74: Can I find a listing using the listing number?</span></strong></p>
<p style="text-align: justify;"><span style="color: #000000;">To find a listing, enter the Rental ID number in the search box on the homepage of VacationRentals2U.com and ShoreSummerRentals.com and click on Search or Go. <br></span></p>
<p style="text-align: justify;"><a href="#top">Back to Top</a></p>
<div style="text-align: justify;" align="center"><hr align="center" size="1" width="100%"></div>
<p style="text-align: justify;"><strong><span style="color: #000000;"><a name="Q75"></a>Q75: Is There a Direct Link to My Listing?</span></strong></p>
<p style="text-align: justify;"><span style="color: #000000;">There are several ways to access and even share your listing.&nbsp;</span></p>
<p style="text-align: justify;"><span style="color: #000000;">1. There is a direct link to each property you list.</span><br><span style="color: #000000;">The URL for your listing will be:</span><br><span style="color: #000000;">http://www.shoresummerrentals.com/listing/______.html (enter rental id in blank area before ".html")</span></p>
<p style="text-align: justify;"><span style="color: #000000;">2. Click on Email a Friend from your listing.</span></p>
<p style="text-align: justify;"><span style="color: #000000;">3. Click on Share of Facebook from your listing.</span></p>
<p style="text-align: justify;"><span style="color: #000000;">4. Click on Share on Twitter from your listing.</span><br><br><a href="#top">Back to Top</a></p>
<div style="text-align: justify;" align="center"><hr align="center" size="1" width="100%"></div>
<p style="text-align: justify;"><strong><span style="color: #000000;"><a name="Q76"></a>Q76: My City is Not Listed on Your Website.</span></strong></p>
<p style="text-align: justify;"><span style="color: #000000;">You can list your vacation rental property in any city or state in the United States even if you do not see any other properties in that town yet. The city will dynamically create by you typing in the city name in the city box when creating your ad.</span></p>
<p style="text-align: justify;"><a href="#top">Back to Top</a></p>
<div style="text-align: justify;" align="center"><hr align="center" size="1" width="100%"></div>
<p style="text-align: justify;"><strong><span style="color: #000000;"><a name="Q77"></a>Q77: Featured Properties.</span></strong></p>
<p style="text-align: justify;"><span style="color: #000000;">This is an Add-On to our Main Memberships.&nbsp; Click here for <a href="http://www.shoresummerrentals.com/advertise.php">Pricing and Details</a>.&nbsp; <br></span></p>
<p style="text-align: justify;"><a href="#top">Back to Top</a></p>
<div style="text-align: justify;" align="center"><hr align="center" size="1" width="100%"></div>
<p style="text-align: justify;"><strong><span style="color: #000000;"><a name="Q78"></a>Q78: Cash Back Referral Program</span></strong></p>
<p style="text-align: justify;"><span style="color: #000000;">Word of mouth is key to our success and very important to us. We now offer a very generous cash back <a href="http://www.shoresummerrentals.com/content/cash-back-referral.html">Referral Program</a>.&nbsp; <br></span></p>
<p style="text-align: justify;"><a href="#top">Back to Top</a></p>
<div style="text-align: justify;" align="center"><hr align="center" size="1" width="100%"></div>
<p style="text-align: justify;"><strong><span style="color: #000000;"><a name="Q79"></a>Q79: Can I Restore an Expired Listing?</span></strong></p>
<p style="text-align: justify;"><span style="color: #000000;">Yes.&nbsp; As long as you did not ask us to delete it entirely from our database.&nbsp; All listings are saved in our database and can be reactivated by making a payment online. Just login to your account and click on Renew Property. The system will walk you through the payment process and upon payment acceptance, your ad will automatically reactivate showing up on the site instantly. Please make sure you update current descriptions, photos, rates and availability.</span></p>
<p style="text-align: justify;"><a href="#top">Back to Top</a></p>
<div style="text-align: justify;" align="center"><hr align="center" size="1" width="100%"></div>
<p style="text-align: justify;"><span style="color: #000000;"><strong><a name="Q80"></a>Q80: Can I Restore a Deleted Listing?</strong></span></p>
<p style="text-align: justify;"><span style="color: #000000;">No, that is why it is very important that you understand that once deleted, everything is gone - photos, descriptions, rates - EVERYTHING.&nbsp; That is why we always highly recommend just asking us to pause it unless you have sold.&nbsp; Noone knows what the future may hold.&nbsp; <br></span></p>
<p style="text-align: justify;"><a href="#top">Back to Top</a></p>
<div style="text-align: justify;" align="center"><hr align="center" size="1" width="100%"></div>
<p style="text-align: justify;"><strong><span style="color: #000000;"><a name="Q81"></a>Q81: Renters: Bounced Email</span></strong></p>
<p style="text-align: justify;"><span style="color: #000000;">If a message is returned to you undeliverable or an owner is not responding to inquiries, please <a href="http://www.shoresummerrentals.com/contactus.php">contact us</a>.</span></p>
<p style="text-align: justify;"><a href="#top">Back to Top</a></p>
<div style="text-align: justify;" align="center"><hr align="center" size="1" width="100%"></div>
<p style="text-align: justify;"><strong><span style="color: #000000;"><a name="Q82"></a>Q82: Owners: Bounced Email</span></strong></p>
<p style="text-align: justify;"><span style="color: #000000;">If a message is returned to you undelivered, we cannot provide you with the correct address to contact the person who sent the inquiry. Form-based inquiries ask the sender to enter an email address. There is no way to verify the validity of this address before sending the inquiry. It is up to the sender to ensure the email address provided exists and is typed in correctly. Sometimes it is possible to guess the address by checking for common misspellings (ex. Yahoo.con instead of Yahoo.com).</span><br><br><span style="color: #000000;">We cannot locate the correct address for undeliverable email address provided by prospective renters.</span><br><br><span style="color: #000000;">Send yourself a test email just entering "test" in the message box and an email address other than the one you registered with in the "From" box and click submit. Make sure the address you put in the From box is not the same address you used to register with and is a real address. If you do not receive the test email, check your inquiry box on VacationRentals2U.com and ShoreSummerRentals.com to see if it is there. If it is, make sure you have not blocked email from VacationRentals2U.com or ShoreSummerRentals.com via your spam controls (AOL users can enter keyword "SPAM CONTROLS" to check this). </span><br><br><span style="color: #000000;">If you have checked all the above and still did not receive your test email, please <a href="http://www.shoresummerrentals.com/contactus.php">contact us.</a></span></p>
<p style="text-align: justify;"><a href="#top">Back to Top</a></p>
<div style="text-align: justify;" align="center"><hr align="center" size="1" width="100%"></div>
<p style="text-align: justify;"><strong><span style="color: #000000;"><a name="Q83"></a>Q83: Someone copied my listing text</span></strong></p>
<p style="text-align: justify;"><span style="color: #000000;">If you believe that your work has been plagiarized, please <a href="http://www.shoresummerrentals.com/contactus.php">contact us.</a></span></p>
<p style="text-align: justify;"><a href="#top">Back to Top</a></p>
<div style="text-align: justify;" align="center"><hr align="center" size="1" width="100%"></div>
<p style="text-align: justify;"><strong><span style="color: #000000;">Spam, Scams, Complaints and Abuse</span></strong></p>
<p style="text-align: justify;"><strong><span style="color: #000000;"><a name="Q84"></a>Q84: Spam Emails</span></strong></p>
<p style="text-align: justify;"><span style="color: #000000;">Unsolicited commercial e-mail (SPAM) is prohibited from being sent through our system. That is why it is very important not to add your email in your descriptions where it is visible to be spidered and used to bombard you with SPAM.&nbsp; For further information about internet fraud and fake check scams, see <a href="http://www.fraud.org">fraud.org</a>.</span></p>
<p style="text-align: justify;"><a href="#top">Back to Top</a></p>
<div style="text-align: justify;" align="center"><hr align="center" size="1" width="100%"></div>
<p style="text-align: justify;"><span style="color: #000000;"><strong><a name="Q85"></a>Q85: Suspicious Email</strong></span></p>
<p style="text-align: justify;"><span style="color: #000000;">Usually emails with poor grammar, spelling, odd use of capital letters and/or asking you to wire money or participate in an unusual bank transaction is a scam. The sender is usually attempting to obtain money, personal information or your email address. We recommend that you let us know you received this through our service and delete the message without responding. For further information about internet fraud and fake check scams, see <span style="color: #000000;"><a href="http://www.fraud.org">fraud.org</a>.</span></span></p>
<p style="text-align: justify;"><span style="color: #3366ff;"><a href="#top"><span style="color: #3366ff;">Back to Top</span></a></span></p>
<div style="text-align: justify;" align="center"><hr align="center" size="1" width="100%"></div>
<p style="text-align: justify;"><strong><span style="color: #000000;"><a name="Q86"></a>Q86: Renter Overpayment Scams</span></strong></p>
<p style="text-align: justify;"><span style="color: #000000;">Scammers will send you an inquiry, which may seem legitimate, but at some point suggest they send you more than your rental fees and ask you wire back the difference. The check they send you will be a fake, but usually takes the bank several weeks to catch it. By then, you have wired the difference back and will be unable to recover what you have sent. In some instances, the bad check will even clear your bank initially. We suggest you insist on exact payment, however, if you do accept an overpayment, wait at least 30 days before returning any funds. If you receive a suspicious inquiry mentioning that a third party (a business, assistant, or someone who owes them money) is going to send an overpayment, we recommend that you delete the inquiry without responding. See fraud.org for more information.</span></p>
<p style="text-align: justify;"><a href="#top">Back to Top</a></p>
<div style="text-align: justify;" align="center"><hr align="center" size="1" width="100%"></div>
<p style="text-align: justify;"><span style="color: #000000;"><strong><a name="Q87"></a>Q87: File a Complaint about a Property Owner.</strong></span></p>
<p style="text-align: justify;"><span style="color: #000000;">VacationRentals2U.com and ShoreSummerRentals.com reserves the right to remove or not remove a listing based on the legitimacy and severity of the complaint. If you have a complaint about a property on VacationRentals2U.com and ShoreSummerRentals.com, please <span style="color: #000000;"><a href="http://www.shoresummerrentals.com/contactus.php">contact us.</a></span><br></span></p>
<p style="text-align: justify;"><a href="#top">Back to Top</a></p>
<div style="text-align: justify;" align="center"><hr align="center" size="1" width="100%"></div>
<p style="text-align: justify;"><strong><span style="color: #000000;"><a name="Q88"></a>Q88: Protection against Abuse/Fraud.</span></strong></p>
<p style="text-align: justify;"><span style="color: #000000;">While abuse and fraud is not common in the vacation rentals industry, we recommend you take certain steps to protect yourself and increase the chance that your transaction goes smoothly. </span><br><br><span style="color: #000000;">Some vacation rental sites offer free trials or free listings, but these increase the chance of abuse and fraud because the owner is not verified by way of a financial transaction, listings are often copied from other sites and have errors and are out of date. It?s safer to use VacationRentals2U.com and ShoreSummerRentals.com as we devote significant resources to verify the legitimacy of our owners and their listings. We can also assure you that we investigate suspicious listings and remove listings for serious or multiple complaints. </span><br><br><span style="color: #000000;">We recommend that you request references, ask for a rental contract, and research the owner and property before your stay. You should also never wire money to owners via Western Union, Money gram or other instant wire transfer services. If you do not feel comfortable working with a certain owner, move on to a different rental.</span></p>
<p style="text-align: justify;"><a href="#top">Back to Top</a></p>
<div style="text-align: justify;" align="center"><hr align="center" size="1" width="100%"></div>
<p style="text-align: justify;"><strong><span style="color: #000000;"><a name="Q89"></a>Q89: Report Abuse/Fraud</span></strong></p>
<p style="text-align: justify;"><span style="color: #000000;">If you are the victim of Fraud/Abuse from a listing found on ShoreSummerRentals.com, please&nbsp;<a href="http://www.shoresummerrentals.com/contactus.php">contact us </a></span>with&nbsp;all the details.</p>
<p style="text-align: justify;"><a href="#top">Back to Top</a></p></div>			
			</div>
			
					
		</div>

	</div>





		
    </script>
	<script src="{{ asset('/front/js/Chart.js') }}"></script>		
@endsection