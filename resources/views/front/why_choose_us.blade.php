@extends('front')

@section('content')

<style>

.why__ChooseUs{
	width:1200px;
	padding:0;
}
.content-custom h1, .content-custom h2, .content-custom h3{
    line-height: 1;
    text-indent: 0;
	margin:0;
}
.content-custom{
	margin-top:0;
}
.content-custom p{
	font-size:18px;
	line-height: 1.5;
    font-weight: 300;
	margin-bottom:10px;
}
.content-custom a, .content-custom a:visited {
    text-decoration: underline;
}
.content-custom h1, .content-custom h2, .content-custom h3 {
    font-weight: 400;
}
table{
	clear:none;
}

</style>
	
	<div class="why__ChooseUs container">

        
		<div class="content content-full margin-top">

			<div class="content-main">
		
				<div class="content-custom"><table class="templateContainer" style="width: 600px;" border="0" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td align="center" valign="top">
<table id="templateHeader" style="width: 600px;" border="0" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td class="headerContainer" valign="top">
<table class="mcnImageBlock" style="width: 100%;" border="0" cellspacing="0" cellpadding="0">
<tbody class="mcnImageBlockOuter">
<tr>
<td class="mcnImageBlockInner" valign="top">
<table class="mcnImageContentContainer" style="width: 100%;" border="0" cellspacing="0" cellpadding="0" align="left">
<tbody>
<tr>
<td class="mcnImageContent" valign="top"><img class="mcnImage" src="https://gallery.mailchimp.com/3fd02cb749e6ae50b2a8ebc8a/images/6a7b3d7f-6209-4ddd-95f7-a720eb5ecf57.jpg" alt="" width="564" align="middle"></td>
</tr>
</tbody>
</table>
<div class="clearfix"></div>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td align="center" valign="top">
<table id="templateBody" style="width: 600px;" border="0" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td class="bodyContainer" valign="top">
<table class="mcnTextBlock" style="width: 100%;" border="0" cellspacing="0" cellpadding="0">
<tbody class="mcnTextBlockOuter">
<tr>
<td class="mcnTextBlockInner" valign="top">
<table class="mcnTextContentContainer" style="width: 100%;" border="0" cellspacing="0" cellpadding="0" align="left">
<tbody>
<tr>
<td class="mcnTextContent" valign="top">
<p><span style="font-family: arial, helvetica, sans-serif; font-size: medium;"><span style="color: #000000;">Hello, my name is Maria Kirk and I own and operate&nbsp;<span style="text-decoration: underline;"><a href="http://www.shoresummerrentals.com" target="_blank"><span style="color: #000000; text-decoration: underline;"><span style="color: #0000ff; text-decoration: underline;">S</span><span style="color: #0000ff; text-decoration: underline;">horeSummerRentals.com</span></span></a></span>. &nbsp;I would like to take this opportunity to introduce you to who we are and what we are all about here at&nbsp;<span style="color: #0000ff;"><span style="text-decoration: underline;"><a href="http://www.shoresummerrentals.com" target="_blank"><span style="color: #0000ff; text-decoration: underline;">ShoreSummerRentals.com</span></a></span>.</span></span></span></p>
<p><span style="font-family: arial, helvetica, sans-serif; font-size: medium;"><span style="color: #000000;"><a href="https://youtu.be/LOolniDGnYk"><img src="http://shoresummerrentals.com/custom/domain_1/image_files/Marketing/MARIA_MISCELLANEOUS/sitemgr_Me_Image_with_arrow.png" alt="" width="225" height="189"></a><br></span><span style="color: #000000;"><br></span><span style="color: #000000;"><strong>FIRST AND FOREMOST<br><br></strong></span><span style="color: #000000;"><strong>No booking fees at all.&nbsp;&nbsp;You also have full control of your email leads.</strong></span><span style="color: #000000;"><a href="https://www.shoresummerrentals.com/advertise.php" target="_blank"><br></a></span><span style="color: #000000;"><br>We are a&nbsp;<em><strong>local owned</strong></em>&nbsp; business offering a large variety of <em><strong>Jersey Shore Rentals.</strong></em> We are not a faceless public corporation looking only to generate profits and our bottom line. &nbsp;Our goal is to secure the most rental&nbsp;income possible for all our owners for a fraction of what our competitors charge, providing you a huge <strong><em>"Return on Your Investment"</em></strong>. &nbsp;It is possible to book most (if not all) of your rentals using&nbsp;<em><strong>Shore Summer Rentals</strong>&nbsp;</em><em>and w</em>e are up to 40% cheaper than comparable packages with our large competitors.&nbsp;<span style="text-decoration: underline; color: #0000ff;"><a href="http://www.shoresummerrentals.com/advertise.php" target="_blank"><span style="color: #0000ff; text-decoration: underline;">Our Memberships</span></a></span>&nbsp;start at just $199.&nbsp;<br><br></span></span><span style="font-family: arial, helvetica, sans-serif; font-size: medium;"><span style="color: #000000;"><span style="color: #0000ff;"><a href="http://www.shoresummerrentals.com" target="_blank"><span style="color: #0000ff;">ShoreSummerRentals.com</span></a>&nbsp;</span>never, ever charge booking fees and never&nbsp;plan on doing that. &nbsp;We also do not control your email leads, as they are all yours, you paid for them. We are a true "For Rental by Owner" website and plan to stay that way.&nbsp;&nbsp;As most of you know, we launched a brand new, state of the art website last year and it has been phenomenal so far.&nbsp;We have made so many upgrades with new&nbsp;features combined with&nbsp;an aggressive marketing plan that brings quality renters&nbsp;to our site&nbsp;and the results have been&nbsp;amazing so far. &nbsp;<br><br></span></span><span style="font-family: arial, helvetica, sans-serif; font-size: medium;"><span style="color: #000000;"><span><span><span><strong><span>New owners use <span style="text-decoration: underline;">Promo Code</span>:&nbsp;</span></strong></span></span></span><span style="color: #ff6600;"><strong>25new2016</strong></span><span><span><span><strong><span>&nbsp;to save $25 on your membership.&nbsp;<br></span></strong></span></span></span><span><span style="color: #0000ff;"><a href="https://www.shoresummerrentals.com/advertise.php" target="_blank"><span style="color: #0000ff;">Join us now.</span></a></span>&nbsp;</span><br></span></span></p>
<p><span style="font-family: arial, helvetica, sans-serif; font-size: medium;"><span style="text-decoration: underline;"><span style="text-decoration: underline;"><span style="text-decoration: underline;"><span style="text-decoration: underline;"><a href="https://www.shoresummerrentals.com/advertise.php" target="_blank"><br></a></span></span></span></span></span></p>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
<table class="mcnCaptionBlock" style="width: 100%;" border="0" cellspacing="0" cellpadding="0">
<tbody class="mcnCaptionBlockOuter">
<tr>
<td class="mcnCaptionBlockInner" valign="top">
<table class="mcnCaptionLeftContentOuter" style="width: 100%;" border="0" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td class="mcnCaptionLeftContentInner" valign="top">
<table class="mcnCaptionLeftImageContentContainer" border="0" cellspacing="0" cellpadding="0" align="right">
<tbody>
<tr>
<td class="mcnCaptionLeftImageContent" valign="top">
<p><span style="color: #000000; font-family: arial, helvetica, sans-serif; font-size: medium;"><span style="color: #000000;"><a title="" href="https://vimeo.com/145863061" target="_blank"><img src="https://gallery.mailchimp.com/3fd02cb749e6ae50b2a8ebc8a/images/5148db89-11e9-4467-8db5-161c811c10cd.png" alt="" width="300"></a></span></span></p>
</td>
</tr>
</tbody>
</table>
<table class="mcnCaptionLeftTextContentContainer" style="width: 264px;" border="0" cellspacing="0" cellpadding="0" align="left">
<tbody>
<tr>
<td class="mcnTextContent" style="text-align: left;" valign="top"><span style="font-size: medium; font-family: arial, helvetica, sans-serif;"><span style="color: #000000;"><strong>WHO WE ARE</strong></span><span style="color: #000000;"><strong><br></strong></span><span style="color: #000000;"><span style="text-decoration: underline; color: #0000ff;"><a href="http://www.shoresummerrentals.com" target="_blank"><span style="color: #0000ff; text-decoration: underline;">ShoreSummerRentals</span></a></span>&nbsp;consists of my husband Chris, our assistant Raya and myself. &nbsp;We were&nbsp;founded back in 2003 (way before the big rental corporations existed) with the idea that it should be easy to connect NJ Shore vacation homeowners directly with quality tenants.&nbsp;So much has happened since we started 13 years ago, most importantly meeting such&nbsp;great people along the way. We have built thousands of lasting relationships. &nbsp;</span></span><span style="color: #000000; font-family: arial, helvetica, sans-serif; font-size: medium;"><span style="text-decoration: underline; color: #3366ff;"><span style="text-decoration: underline;"><span style="text-decoration: underline;"><span style="color: #3366ff; text-decoration: underline;"><a href="https://www.shoresummerrentals.com/advertise.php" target="_blank"><br><br></a></span></span></span></span></span>
<table border="0" cellspacing="0" cellpadding="0" align="left">
<tbody>
<tr>
<td class="mcnShareContentItemContainer" valign="top">
<table class="mcnShareContentItem" width="" border="0" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td align="left" valign="middle">
<table width="" border="0" cellspacing="0" cellpadding="0" align="left">
<tbody>
<tr>
<td class="mcnShareIconContent" align="center" valign="middle" width="24">
<p><span style="font-family: arial, helvetica, sans-serif; font-size: medium;"><strong><a href="http://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Feepurl.com%2FbKuDpL" target="_blank"><img src="http://cdn-images.mailchimp.com/icons/social-block-v2/outline-color-facebook-48.png" alt="" width="24" height="24"></a></strong></span></p>
</td>
<td class="mcnShareTextContent" align="left" valign="middle">
<p><span style="color: #3366ff; font-family: arial, helvetica, sans-serif; font-size: medium;"><strong><a href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Feepurl.com%2FbKu5Pr"><span style="color: #3366ff;">Share</span></a></strong></span></p>
</td>
</tr>
</tbody>
</table>
<table width="" border="0" cellspacing="0" cellpadding="0" align="left">
<tbody>
<tr>
<td class="mcnShareIconContent" align="center" valign="middle" width="24">
<p><span style="font-family: arial, helvetica, sans-serif; font-size: medium;"><strong>&nbsp;</strong></span></p>
</td>
<td class="mcnShareTextContent" align="left" valign="middle">
<p><span style="font-family: arial, helvetica, sans-serif; font-size: medium;"><strong>&nbsp;</strong></span></p>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
<table border="0" cellspacing="0" cellpadding="0" align="left">
<tbody>
<tr>
<td class="mcnShareContentItemContainer" valign="top">
<table class="mcnShareContentItem" width="" border="0" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td align="left" valign="middle">
<table width="" border="0" cellspacing="0" cellpadding="0" align="left">
<tbody>
<tr>
<td class="mcnShareIconContent" align="center" valign="middle" width="24">
<p><span style="font-family: arial, helvetica, sans-serif; font-size: medium;"><strong><a href="http://twitter.com/intent/tweet?text=SSR+Intro+for+Link:%20http%3A%2F%2Feepurl.com%2FbKuDpL" target="_blank"><img src="http://cdn-images.mailchimp.com/icons/social-block-v2/outline-color-twitter-48.png" alt="" width="24" height="24"></a></strong></span></p>
</td>
<td class="mcnShareTextContent" align="left" valign="middle">
<p><span style="font-family: arial, helvetica, sans-serif; font-size: medium;"><strong><span style="color: #3366ff;"><span style="color: #3366ff;"><a href="https://twitter.com/intent/tweet?text=Why+Rent+or+Advertise+with+ShoreSummerRentals.com%3F:%20http%3A%2F%2Feepurl.com%2FbKu5Pr"><span style="color: #3366ff;">Tweet</span></a>&nbsp; &nbsp; &nbsp;</span></span></strong></span></p>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
<p><span style="font-family: arial, helvetica, sans-serif; font-size: medium;">&nbsp;<br></span></p>
</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
<table class="mcnCaptionBlock" style="width: 100%;" border="0" cellspacing="0" cellpadding="0">
<tbody class="mcnCaptionBlockOuter">
<tr>
<td class="mcnCaptionBlockInner" valign="top">
<table class="mcnCaptionRightContentOuter" style="width: 100%;" border="0" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td class="mcnCaptionRightContentInner" valign="top">
<table class="mcnCaptionRightImageContentContainer" border="0" cellspacing="0" cellpadding="0" align="left">
<tbody>
<tr>
<td class="mcnCaptionRightImageContent" valign="top">
<p><span style="color: #000000; font-family: arial, helvetica, sans-serif; font-size: medium;"><a title="" href="https://vimeo.com/145863061" target="_blank"><span style="color: #000000;"><br><img src="https://gallery.mailchimp.com/3fd02cb749e6ae50b2a8ebc8a/images/1c758666-c84b-4481-a6fc-f0cc09ebce17.jpg" alt="" width="325"></span></a></span></p>
</td>
</tr>
</tbody>
</table>
<table class="mcnCaptionRightTextContentContainer" style="width: 264px;" border="0" cellspacing="0" cellpadding="0" align="right">
<tbody>
<tr>
<td class="mcnTextContent" valign="top"><span style="font-size: medium; font-family: arial, helvetica, sans-serif;"><span style="color: #000000;"><strong><br>OUR MISSION</strong></span><span style="color: #000000;"><br>We exist to promote the Jersey Shore and help our clients gain maximum return on their rental home investment&nbsp;<strong>for a fraction of what our competitors charge. &nbsp;</strong><span class="s1">We work with our clients personally building lasting relationships. &nbsp;</span>&nbsp;We love the New Jersey shore... It is all we focus on, getting NJ shore vacation homes fully booked. &nbsp;<span style="text-decoration: underline; color: #3366ff;"><span style="text-decoration: underline;"><br></span></span></span></span></td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
<table class="mcnTextBlock" style="width: 100%;" border="0" cellspacing="0" cellpadding="0">
<tbody class="mcnTextBlockOuter">
<tr>
<td class="mcnTextBlockInner" valign="top">
<table class="mcnTextContentContainer" style="width: 100%;" border="0" cellspacing="0" cellpadding="0" align="left">
<tbody>
<tr>
<td class="mcnTextContent" style="text-align: left;" valign="top"><span style="font-family: arial, helvetica, sans-serif; font-size: medium;"><strong style="font-family: helvetica; font-size: small;"><span style="color: #000000;"><br><span style="font-size: medium;">ARE YOU ON YOUR OWN? &nbsp;</span></span><br></strong><span style="color: #000000;"><span style="text-decoration: underline;"><strong>Not at all!</strong></span><strong style="font-family: helvetica; font-size: small;">&nbsp;</strong>&nbsp;We are here to help&nbsp;you with everything you need from pictures, writing your ad, performing a rate analysis and more.&nbsp; &nbsp;You have direct access to us, the&nbsp;company owners. To us, our clients are family and family takes care of the family. &nbsp;<br><br><span><span><span><strong><span>New owners use <span style="text-decoration: underline;">Promo Code</span>:&nbsp;</span></strong></span></span></span><span style="color: #ff6600;"><strong>25new2016</strong></span><span><span><span><strong><span>&nbsp;to save $25 on your membership.&nbsp;<br></span></strong></span></span></span><span style="color: #0000ff;"><a href="https://www.shoresummerrentals.com/advertise.php" target="_blank"><span style="color: #0000ff;">Join us now.</span></a>&nbsp;&nbsp;</span></span><span style="text-decoration: underline; color: #3366ff;"><a href="https://www.shoresummerrentals.com/advertise.php" target="_blank"><span style="text-decoration: underline; color: #3366ff;"><br><br></span></a></span><span style="color: #000000;"><img src="https://gallery.mailchimp.com/3fd02cb749e6ae50b2a8ebc8a/images/9a167a28-a0e9-404c-8d43-d29664a4ac1c.jpeg" alt="" width="73" height="50" align="none"><br></span><span style="color: #000000;"><strong>WHAT MAKES US DIFFERENT THAN THE BIG GUYS &nbsp;<br></strong></span><span style="color: #000000;"><img src="https://gallery.mailchimp.com/3fd02cb749e6ae50b2a8ebc8a/images/6d15f7c4-f957-49e1-8c6b-faaf36c3487b.png" alt="" width="15" height="15" align="none">&nbsp;<span class="s1">We work with our clients personally and build a lasting relationship.</span><br><img src="https://gallery.mailchimp.com/3fd02cb749e6ae50b2a8ebc8a/images/6d15f7c4-f957-49e1-8c6b-faaf36c3487b.png" alt="" width="15" height="15" align="none">&nbsp;We are a local resource for our clients with a&nbsp;sole focus on securing them rentals.<br></span><span style="color: #000000;"><img src="https://gallery.mailchimp.com/3fd02cb749e6ae50b2a8ebc8a/images/6d15f7c4-f957-49e1-8c6b-faaf36c3487b.png" alt="" width="15" height="15" align="none">&nbsp;We know firsthand what it takes to get booked at the Jersey&nbsp;Shore.<br></span><span style="color: #000000;"><img src="https://gallery.mailchimp.com/3fd02cb749e6ae50b2a8ebc8a/images/6d15f7c4-f957-49e1-8c6b-faaf36c3487b.png" alt="" width="15" height="15" align="none">&nbsp;We are vacation property owners just like you.&nbsp;<br></span><span style="color: #000000;"><img src="https://gallery.mailchimp.com/3fd02cb749e6ae50b2a8ebc8a/images/6d15f7c4-f957-49e1-8c6b-faaf36c3487b.png" alt="" width="15" height="15" align="none">&nbsp;We know your challenges.<br></span><span style="color: #000000;"><img src="https://gallery.mailchimp.com/3fd02cb749e6ae50b2a8ebc8a/images/6d15f7c4-f957-49e1-8c6b-faaf36c3487b.png" alt="" width="15" height="15" align="none">&nbsp;We spent our summers at the NJ Shore growing up and cherish&nbsp;those memories.<br></span><span style="color: #000000;"><img src="https://gallery.mailchimp.com/3fd02cb749e6ae50b2a8ebc8a/images/6d15f7c4-f957-49e1-8c6b-faaf36c3487b.png" alt="" width="15" height="15" align="none">&nbsp;Our clients have direct access to us, the&nbsp;company owners.<br>&nbsp;</span></span></td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
<table class="mcnTextBlock" style="width: 100%;" border="0" cellspacing="0" cellpadding="0">
<tbody class="mcnTextBlockOuter">
<tr>
<td class="mcnTextBlockInner" valign="top">
<table class="mcnTextContentContainer" style="width: 100%;" border="0" cellspacing="0" cellpadding="0" align="left">
<tbody>
<tr>
<td class="mcnTextContent"><span style="font-family: arial, helvetica, sans-serif; font-size: medium;"><span style="color: #000000;"><strong>PROPERTY OWNER TESTIMONIALS</strong><br><span style="color: #ff0000;">[Don't take our word for it, view a sampling of what our current owners are saying]</span><strong><br></strong><img src="https://gallery.mailchimp.com/3fd02cb749e6ae50b2a8ebc8a/images/86e7cc84-8b92-4e52-bb80-c043ad1c7b54.png" alt="" width="10" height="10" align="none">&nbsp; "I always receive an answer from Maria or Raya right away." <br><img src="https://gallery.mailchimp.com/3fd02cb749e6ae50b2a8ebc8a/images/86e7cc84-8b92-4e52-bb80-c043ad1c7b54.png" alt="" width="10" height="10" align="none">&nbsp;&nbsp;"My questions are always answered, addressed and solved very quickly."&nbsp;<br><img src="https://gallery.mailchimp.com/3fd02cb749e6ae50b2a8ebc8a/images/86e7cc84-8b92-4e52-bb80-c043ad1c7b54.png" alt="" width="10" height="10" align="none">&nbsp;&nbsp;"Maria is always very responsive to anything we need."<br><img src="https://gallery.mailchimp.com/3fd02cb749e6ae50b2a8ebc8a/images/86e7cc84-8b92-4e52-bb80-c043ad1c7b54.png" alt="" width="10" height="10" align="none">&nbsp;&nbsp;"Great customer service.&nbsp; Always willing to help."<br><img src="https://gallery.mailchimp.com/3fd02cb749e6ae50b2a8ebc8a/images/86e7cc84-8b92-4e52-bb80-c043ad1c7b54.png" alt="" width="10" height="10" align="none">&nbsp;&nbsp;"Your personalized service, excellent!"&nbsp;<br><img src="https://gallery.mailchimp.com/3fd02cb749e6ae50b2a8ebc8a/images/86e7cc84-8b92-4e52-bb80-c043ad1c7b54.png" alt="" width="10" height="10" align="none">&nbsp;&nbsp;"The site has grown but I still get the same service as I did when you started."<br><br></span><span style="color: #0000ff;"><span style="color: #3366ff;"><span style="text-decoration: underline;"><a href="https://www.shoresummerrentals.com/advertise.php" target="_blank"><span style="color: #3366ff; text-decoration: underline;">Main Memberships</span></a></span>&nbsp;and&nbsp;<span style="text-decoration: underline;"><a href="http://shoresummerrentals.com/content/add-on-services.html" target="_blank"><span style="color: #3366ff; text-decoration: underline;">Add-On Services</span></a>.</span></span><br><span style="text-decoration: underline; color: #3366ff;"><a href="https://vimeo.com/145863061" target="_blank"><span style="color: #3366ff; text-decoration: underline;">Meet Our Team.</span></a></span><br><span style="text-decoration: underline; color: #3366ff;"><a href="https://vimeo.com/145005475" target="_blank"><span style="color: #3366ff; text-decoration: underline;">Behind the Scenes with Maria and Raya</span></a>.</span><br><span style="text-decoration: underline;"><a href="https://vimeo.com/144163118" target="_blank"><span style="color: #0000ff; text-decoration: underline;"><span style="color: #3366ff; text-decoration: underline;">Maria Explains Different Membership Levels</span><br><br></span></a></span></span><span style="color: #000000;">Please do not hesitate to contact me on my cell phone if there is anything I can do to help you get started or answer any questions you may have regarding <em><strong>Shore Summer Rentals</strong></em>.&nbsp;Thank you! &nbsp;<br><br><span style="color: #3366ff;"><span style="color: #3366ff;"><strong><span style="color: #000000;">New owners use <span style="text-decoration: underline;">Promo Code</span>: </span></strong></span></span></span><span style="color: #ff6600;"><strong>25new2016</strong></span><span style="color: #000000;"><span style="color: #3366ff;"><span style="color: #3366ff;"><strong><span style="color: #000000;"> to save $25 on your membership.&nbsp;<br></span></strong></span></span></span><a href="https://www.shoresummerrentals.com/advertise.php" target="_blank"><span style="color: #0000ff;">Join us now</span>.</a>&nbsp;&nbsp;</span></td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
<table class="mcnShareBlock" style="width: 100%;" border="0" cellspacing="0" cellpadding="0">
<tbody class="mcnShareBlockOuter">
<tr>
<td class="mcnShareBlockInner" valign="top">
<p><span style="color: #000000; font-family: arial, helvetica, sans-serif; font-size: medium;"><span style="color: #000000;"><a href="https://www.shoresummerrentals.com/advertise.php" target="_blank"><br></a></span></span></p>
<table border="0" cellspacing="0" cellpadding="0" align="left">
<tbody>
<tr>
<td class="mcnShareContentItemContainer" valign="top">
<table class="mcnShareContentItem" width="" border="0" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td align="left" valign="middle">
<table width="" border="0" cellspacing="0" cellpadding="0" align="left">
<tbody>
<tr>
<td class="mcnShareIconContent" align="center" valign="middle" width="24">
<p><span style="font-size: medium; color: #3366ff; font-family: arial, helvetica, sans-serif;"><strong><a href="http://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Feepurl.com%2FbKuDpL" target="_blank"><span style="color: #3366ff;"><img src="http://cdn-images.mailchimp.com/icons/social-block-v2/outline-color-facebook-48.png" alt="" width="24" height="24"></span></a></strong></span></p>
</td>
<td class="mcnShareTextContent" align="left" valign="middle">
<p><span style="font-size: medium; color: #3366ff; font-family: arial, helvetica, sans-serif;"><strong><a href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Feepurl.com%2FbKu5Pr"><span style="color: #3366ff;">Share</span></a></strong></span></p>
</td>
</tr>
</tbody>
</table>
<table width="" border="0" cellspacing="0" cellpadding="0" align="left">
<tbody>
<tr>
<td class="mcnShareIconContent" align="center" valign="middle" width="24">
<p><span style="font-family: arial, helvetica, sans-serif; font-size: medium;"><strong>&nbsp;</strong></span></p>
</td>
<td class="mcnShareTextContent" align="left" valign="middle">
<p><span style="font-family: arial, helvetica, sans-serif; font-size: medium;"><strong>&nbsp;</strong></span></p>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
<table border="0" cellspacing="0" cellpadding="0" align="left">
<tbody>
<tr>
<td class="mcnShareContentItemContainer" valign="top">
<table class="mcnShareContentItem" width="" border="0" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td align="left" valign="middle">
<table width="" border="0" cellspacing="0" cellpadding="0" align="left">
<tbody>
<tr>
<td class="mcnShareIconContent" align="center" valign="middle" width="24">
<p><span style="font-family: arial, helvetica, sans-serif; font-size: medium; color: #3366ff;"><strong><a href="http://twitter.com/intent/tweet?text=SSR+Intro+for+Link:%20http%3A%2F%2Feepurl.com%2FbKuDpL" target="_blank"><span style="color: #3366ff;"><img src="http://cdn-images.mailchimp.com/icons/social-block-v2/outline-color-twitter-48.png" alt="" width="24" height="24"></span></a></strong></span></p>
</td>
<td class="mcnShareTextContent" align="left" valign="middle">
<p><span style="font-family: arial, helvetica, sans-serif; font-size: medium; color: #3366ff;"><strong><a href="https://twitter.com/intent/tweet?text=Why+Rent+or+Advertise+with+ShoreSummerRentals.com%3F:%20http%3A%2F%2Feepurl.com%2FbKu5Pr"><span style="color: #3366ff;">Tweet</span></a>&nbsp; &nbsp; &nbsp;</strong></span></p>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
<p><span style="font-size: medium; font-family: arial, helvetica, sans-serif;">&nbsp;&nbsp;</span></p>
</td>
</tr>
</tbody>
</table>
<table class="mcnCaptionBlock" style="width: 100%;" border="0" cellspacing="0" cellpadding="0">
<tbody class="mcnCaptionBlockOuter">
<tr>
<td class="mcnCaptionBlockInner" valign="top">
<table class="mcnCaptionLeftContentOuter" style="width: 100%;" border="0" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td class="mcnCaptionLeftContentInner" style="text-align: left;"><span style="font-family: arial, helvetica, sans-serif; font-size: medium;"><span style="color: #000000;"><br></span></span><span style="font-size: medium;"><span style="font-family: arial, helvetica, sans-serif;"><span style="color: #000000;">Sincerely,</span></span><span style="font-family: arial, helvetica, sans-serif;"><span style="color: #000000;"><br></span><span style="color: #000000;"><em><strong>Maria Kirk, Owner and Founder</strong></em></span><span style="color: #000000;"><a style="font-family: helvetica; font-size: small;" href="http://www.shoresummerrentals.com/"><span style="color: #000000;"><br></span></a><span style="color: #3366ff;"><a href="http://www.ShoreSummerRentals.com"><span style="color: #3366ff;">ShoreSummerRentals.com<br></span></a></span></span><span style="color: #000000;"><a href="http://www.shoresummerrentals.com/"><img src="https://gallery.mailchimp.com/3fd02cb749e6ae50b2a8ebc8a/images/b8fa79ca-94a1-4e77-a1b5-1994a8d29128.png" alt="" width="125"></a><br></span><span style="color: #000000;">P.O. Box 55</span><span style="color: #000000;"><br>Somers Point, NJ 08244<br>Office: 609-677-1580<br>Cell: 856-986-2331<br>Fax: 609-677-1590</span></span></span></td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td align="center" valign="top">&nbsp;</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<div><span style="font-family: arial, helvetica, sans-serif; font-size: medium; color: #000000;"><span style="text-decoration: underline;"><span><br></span></span></span></div></div>			
			</div>
			
					
		</div>

	</div>
						

					
@endsection