@extends('front')
<?php
use App\Functions\Functions;
?>

@section('content')

	<link rel="stylesheet" href="{{ asset('front/css/bootstrap.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('front/css/members.min.css') }}" />

     <!--link rel="stylesheet" href="{{ asset('front/css/bootstrap-responsive.min.css') }}" /-->
     

    
    
	<div class="container members">

        <div class="rate-page-container container-fluid ">

	<div>

								
		
  <nav class="minor-nav">
        <ul>
            <li>
                <a href="<?php echo url('sponsors/listing/'.$listing_id.''); ?>">Listing Information</a>
            </li>

                            <li >
               <a href="<?php echo url('sponsors/backlinks/'.$listing_id.''); ?>">Backlink</a>
                </li>
            
            
            <li>
                <a class="active"  href="<?php echo url('sponsors/rate/'.$listing_id.''); ?>">Rate</a>
            </li>
            
            <li>
                <a href="<?php echo url('sponsors/specials/'.$listing_id.''); ?>">Specials</a>
            </li>
        </ul>
    </nav>

		
        <div class="baseForm">

            <form name="rates" action="<?php echo url('sponsors/rateStore'); ?>" method="post" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="rate_id" value="" />
                <input type="hidden" name="listing_id" value="<?php echo $listing_id; ?>">
                <input type="hidden" name="letter" value="" />
                <input type="hidden" name="screen" value="" />
    				
 @if(isset($message)==1)                            
    <p class="successMessage">The rate has been added.</p>
    @endif  
	<table border="0" cellpadding="0" cellspacing="0" class="standard-table">
		
		<tr>
			<th>
				<span>Checkin:</span>
			</th>
			<td>
				<input type="text" name="checkin" value="<?php echo isset($spRateInfo[0]->checkin)?$spRateInfo[0]->checkin:''; ?>" placeholder="00:00 PM" maxlength="10" />
			</td>
		</tr>
		<tr>
			<th>
				<span>Check Out:</span>
			</th>
			<td>
				<input type="text" name="checkout" value="<?php echo isset($spRateInfo[0]->checkout)?$spRateInfo[0]->checkout:''; ?>" placeholder="00:00 AM" maxlength="10" />
			</td>
		</tr>
		<tr>
			<th>
				<span>Payment Types Accepted:</span>
			</th>
			<td>
				<input type="text" name="payment_types" value="<?php echo isset($spRateInfo[0]->payment_types)?$spRateInfo[0]->payment_types:''; ?>" maxlength="250"/>
			</td>
		</tr>
		<tr>
			<th>
				<span>Booking Deposit and Payment Terms:</span>
			</th>
			<td>
				<input type="text" name="payment_terms" value="<?php echo isset($spRateInfo[0]->payment_terms)?$spRateInfo[0]->payment_terms:''; ?>" maxlength="750" />
			</td>
		</tr>
		<tr>
			<th>
				<span>Cancellation Policy:</span>
			</th>
			<td>
				<input type="text" name="cancellation_policy" value="<?php echo isset($spRateInfo[0]->cancellation_policy)?$spRateInfo[0]->cancellation_policy:''; ?>" maxlength="250" />
			</td>
		</tr>
		<tr>
			<th>
				<span>Security Deposit:</span>
			</th>
			<td>
				<input type="text" name="security_deposit" value="<?php echo isset($spRateInfo[0]->security_deposit)?$spRateInfo[0]->security_deposit:''; ?>" maxlength="250" />
			</td>
		</tr>
		<tr>
			<th>
				<span>Notes:</span>
			</th>
			<td>
				<textarea name="notes" rows="5"/><?php echo isset($spRateInfo[0]->notes)?$spRateInfo[0]->notes:''; ?></textarea>
			</td>
		</tr>
	
	</table>

	<table border="0" cellpadding="0" cellspacing="0" class="standard-table scheduleTheTable">
		<tr>
			<th style="text-align: center;width:16.6%;">
				<span>Start Date</span>
			</th>
			<th style="text-align: center;width:16.6%;">
				<span>End Date</span>
			</th>
			<th style="text-align: center;width:16.6%;">
				<span>Daily</span>
			</th>
			<th style="text-align: center;width:16.6%;">
				<span>Weekly</span>
			</th>
			<th style="text-align: center;width:16.6%;">
				<span>Monthly</span>
			</th>
			<th style="text-align: center;width:16.6%;">
				<span>Season</span>
			</th>
		</tr>

		<div style="padding: 10px;">
			<p style="color: red;text-align: left; font-size:12px;"><b>Note:</b> To edit rates already entered, place your curser in the field you want to update, make the change, hit Enter.</p>
		</div>

		<tr>
			<td>
				<input type="text" class="calendar hasDatepicker" id="from_date" name="from_date" value="" placeholder="mm/dd/yyyy" />
			</td>
			<td>
				<input type="text" class="calendar hasDatepicker" id="to_date" name="to_date" value="" placeholder="mm/dd/yyyy" />
			</td>
			<td>
				<input type="text" id="day" name="day" value="" maxlength="5" style="text-align:right;" placeholder="0.00"/>
			</td>
			<td>
				<input type="text" id="week" name="week" value="" maxlength="5" style="text-align:right;" placeholder="0.00"/>
			</td>
			<td>
				<input type="text" id="month" name="month" value="" maxlength="5" style="text-align:right;" placeholder="0.00"/>
			</td>
			<td>
				<input type="text" id="season" name="season" value="" maxlength="5" style="text-align:right;" placeholder="0.00"/>
			</td>
		</tr>
	</table>
	
	<script type="text/javascript">
		$(document).ready(function() {
	        	        $("#from_date").datepicker({
	            minDate: 0,
	            onSelect: function(dateText, inst) {
	                var actualDate = new Date(dateText);
	                var newDate = new Date(actualDate.getFullYear(), actualDate.getMonth(), actualDate.getDate()+1);
	                $("#to_date").datepicker("option", "minDate", newDate);
	            },
	            onClose: function(dateText, inst) {
	                setTimeout(function() {
	                    $("#to_date").focus();
	                }, 200);
	            }
	        });

	        $("#to_date").datepicker();
	    });
	</script>
                <button type="submit" value="Submit" class="input-button-form">Save</button>
				
				
            </form>

            	
        </div>
<script type="text/javascript">
    

    $(document).ready(function() {
        // $('.date_calendar').datepicker();


        //hide the current price and shows a div with an <input type text>
        $(".value_div").click(function(){
            $(this).hide();
            var new_price_div_id = $(this).attr("id");
            new_price_div_id+= "_new";
            new_price_div = document.getElementById(new_price_div_id);
            $(new_price_div).show();
        });

        //when the users presses enter from one of the inputs, executes update_rate(rate_id,rate_field,rate_value)
        $(".new_value").keypress(function(event){
            if (event.which == 13) {
                $(this).hide();

                var aux         = $(this).attr("id").split("_");
                var rate_id     = aux[0];
                var rate_field  = aux[1]; 
                    if (rate_field == "from" || rate_field == "to") {
                        rate_field+="_"+aux[2];
                    }
                var new_value   = $("#"+rate_id+"_"+rate_field+"_input").val();
                var loading_div = $(this).attr("id");
                loading_div = loading_div.replace("_new","_loading");
                $("#"+loading_div).show();
                update_rate(rate_id,rate_field,new_value);
            }
        });

    });

    //Delete rate
    function dialog_delete(delete_element) {
        delete_id = delete_element.id;
        delete_id = delete_id.split("_");
        delete_id = delete_id[0];

        $( "#dialog_confirm" ).dialog({
          resizable: false,
          height:140,
          modal: true,
          buttons: {
            "Delete": function() {
              delete_rate(delete_id);
              $( this ).dialog( "close" );
              
            },
            Cancel: function() {
              $( this ).dialog( "close" );
            }
          }
        });
    }


    function update_rate(rate_id,rate_field,rate_value) {  

        $.post('<?php echo url('sponsors/ajax_listing_rate'); ?>', {
            id: rate_id,
            field: rate_field,
             _token: '<?php echo csrf_token();  ?>',
            value: rate_value,
            action: "update"
        }, function(data) { 
            console.log(data);
            var obj = JSON.parse(data);
            if (obj.error_msg) {
                $("#"+rate_id+"_"+rate_field+"_loading").hide();
                $("#"+rate_id+"_"+rate_field).show();
                update_error(obj.error_msg);
            } else if (obj) {
                var obj = JSON.parse(data);
                update_divs(rate_id,rate_field,obj.money_value);
            }
        });

    }


    function update_error(msg){
        $("#return_message").html("<p class=\"errorMessage\">"+msg+"</p>");
        $('#return_message').css('visibility','visible').hide().fadeIn(); 
        setTimeout(function() {
              $('#return_message').css('visibility','hidden')
        }, 3000);
    }


    function update_divs(id,field,value) {
        $("#return_message").html("<p class=\"successMessage\">The Rate has been Updated.</p>");
        $('#return_message').css('visibility','visible').hide().fadeIn();
        setTimeout(function() {
              $('#return_message').css('visibility','hidden')
        }, 3000);

        var value_div_id = id+"_"+field;
        var loading_div_id = id+"_"+field+"_loading";
        value_div = $("#"+value_div_id);
        $("#"+loading_div_id).hide();

        if (field == "from_date" || field == "to_date") {
            var html_content = $("#"+id+"_"+field+"_input").val();
            value_div.html(html_content);
        } else {
            var currency_symbol = '$';
            value_div.html(currency_symbol+" "+value);
        }

        value_div.show();
        
    }

</script>
        <br>
    @if(count($spRate)>0)
     <div id="dialog_confirm" title="Delete Rate ?" style="display:none;">
      <p>
        <span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>
        This item will be permanently deleted and cannot be recovered. Are you sure?
      </p>
    </div>

    <div id="return_message" style="visibility:hidden; margin:10px;">
        <p class=\"successMessage\">Updated</p>
    </div>
        <table class="table-itemlist startscheduleTab">
        <tbody class="startscheduleTabBody"><tr>
            <th>
                <span>Start Date</span>
            </th>
            <th>
                <span>End Date</span>
            </th>
            <th>
                <span>Daily</span>
            </th>
            <th>
                <span>Weekly</span>
            </th>
            <th>
                <span>Monthly</span>
            </th>                        
            <th>
                <span>Season</span>
            </th>                                    
            <th>
                <span>Options</span>
            </th>
        </tr>
        @foreach($spRate as $key=>$val)
             <tr>

                <td>
                    <div id="{{ $val->id }}_from_date_loading" style="display:none" class="loading_div">
                        <img src="<?php echo url('front/images/img_loading.gif'); ?>">
                    </div>                    
                    <div class="new_value" style="display:none" id="{{ $val->id }}_from_date_new">
                 <input type="text" class="date_calendar" style="width:80px" value="{{ Functions::format_date($val->from_date) }}" id="{{ $val->id }}_from_date_input">
                    </div>
                    <div class="value_div" style="cursor:pointer;color:#2980b9;" id="{{ $val->id }}_from_date">{{ Functions::format_date($val->from_date) }}</div>
                </td>

                <td>
                         <div id="{{ $val->id }}_to_date_loading" style="display:none" class="loading_div">
                        <img src="<?php echo url('front/images/img_loading.gif'); ?>">
                    </div>                 
                    <div class="new_value" style="display:none" id="{{ $val->id }}_to_date_new">
                        <input type="text" class="date_calendar" style="width:80px" value="{{ Functions::format_date($val->to_date) }}" id="{{ $val->id }}_to_date_input">
                    </div>
                    <div class="value_div" style="cursor:pointer;color:#2980b9;" id="{{ $val->id }}_to_date">
                       {{ Functions::format_date($val->to_date) }}</div>
                </td>



                                    <td>
                        <div id="{{ $val->id }}_day_loading" style="display:none" class="loading_div">
                        <img src="https://www.shoresummerrentals.com/images/img_loading.gif">
                    </div>  
                        <div class="new_value" id="{{ $val->id }}_day_new" style="display:none">
                            <input type="text" maxlength="8" size="8" value="{{ $val->day }}" id="{{ $val->id }}_day_input">
                        </div>
                        <div style="cursor:pointer; width:100%; color:#2980b9;" class="value_div" id="{{ $val->id }}_day">&#36; {{ Functions::format_money($val->day) }}</div>
                    </td>                    
                                    <td>
                        <div id="{{ $val->id }}_week_loading" style="display:none" class="loading_div">
                        <img src="https://www.shoresummerrentals.com/images/img_loading.gif">
                    </div>  
                        <div class="new_value" id="{{ $val->id }}_week_new" style="display:none">
                            <input type="text" maxlength="8" size="8" value="{{ $val->week }}" id="{{ $val->id }}_week_input">
                        </div>
                        <div style="cursor:pointer; width:100%; color:#2980b9;" class="value_div" id="{{ $val->id }}_week">&#36; {{ Functions::format_money($val->week) }}</div>
                    </td>                    
                                    <td>
                       <div id="{{ $val->id }}_month_loading" style="display:none" class="loading_div">
                        <img src="https://www.shoresummerrentals.com/images/img_loading.gif">
                    </div> 
                        <div class="new_value" id="{{ $val->id }}_month_new" style="display:none">
                      <input type="text" maxlength="8" size="8" value="{{ $val->month }}" id="{{ $val->id }}_month_input">
                        </div>
                        <div style="cursor:pointer; width:100%; color:#2980b9;" class="value_div" id="{{ $val->id }}_month">&#36; {{ Functions::format_money($val->month) }}</div>
                    </td>                    
                                    <td>
                       <div id="{{ $val->id }}_season_loading" style="display:none" class="loading_div">
                        <img src="https://www.shoresummerrentals.com/images/img_loading.gif">
                    </div> 
                        <div class="new_value" id="{{ $val->id }}_season_new" style="display:none">
                            <input type="text" maxlength="8" size="8" value="{{ $val->season }}" id="{{ $val->id }}_season_input">
                        </div>
                        <div style="cursor:pointer; width:100%; color:#2980b9;" class="value_div" id="{{ $val->id }}_season">&#36; {{ Functions::format_money($val->season) }}</div>
                    </td>                    
                
                                                
                <td nowrap="" class="main-options">
            <a href="<?php echo url('sponsors/delete_rate/'.$val->id.'/'.$val->listing_id.'');?>">Delete</a></td>

            </tr>

            @endforeach

            </tbody>
            </table>
             @endif
        
	</div>
	</div>
	</div><!-- Close container-fluid div -->

		 <script type="text/javascript">
        
        if ($("#myChart").length) {
            //This will get the first returned node in the jQuery collection.
            var ctx = $("#myChart").get(0).getContext("2d");
        }

        function initializeDashboard() {
            $(".dial").knob({
                readOnly:   true,
                fgColor:    "#2980b9",
                bgColor:    "#DEE1E3",
                fontWeight: 300,
                thickness:  .2,
                width:      70,
                height:     70
            });

            $(".status, .floating-tip, .alert-new, #item_renewal").tooltip({
                animation: true,
                placement: "top"
            });
             
            if ($("#myChart").length) {
                //Get context with jQuery - using jQuery's .get() method.
                ctx = $("#myChart").get(0).getContext("2d");
                loadChart();
            }
        }

        $(function() {
            $("#alert").fadeOut(5000);
            initializeDashboard();
        });
        
        function showReply(id) {
            $('#review_reply'+id).css('display', '');
            $('#link_reply'+id).css('display', 'none');
            $('#cancel_reply'+id).css('display', '');
        }
        
        function hideReply(id) {
            $('#review_reply'+id).css('display', 'none');
            $('#link_reply'+id).css('display', '');
            $('#cancel_reply'+id).css('display', 'none');
        }
        
        function showLead(id) {
            $('#lead_reply'+id).css('display', '');
            $('#link_lead'+id).css('display', 'none');
            $('#cancel_lead'+id).css('display', '');
        }
        
        function hideLead(id) {
            $('#lead_reply'+id).css('display', 'none');
            $('#link_lead'+id).css('display', '');
            $('#cancel_lead'+id).css('display', 'none');
        }
        
        function saveReply(id) {
            $("#submitReply"+id).css("cursor", "default");
            $("#submitReply"+id).prop("disabled", "disabled");
            $("#submitReply"+id).html('Wait, Loading...');
            
            $.post("https://www.shoresummerrentals.com/sponsors/ajax.php", $("#formReply"+id).serialize(), function(data) {
                if (data == "ok") {
                    $("#msgReviewE"+id).css("display", "none");
                    $("#msgReviewS"+id).css("display", "");
                    $("#msgReviewS"+id).fadeOut(5000);
                } else {
                    $("#msgReviewE"+id).css("display", "");
                    $("#msgReviewS"+id).css("display", "none");
                }
                $("#submitReply"+id).html('Submit');
                $("#submitReply"+id).prop("disabled", "");
                $("#submitReply"+id).css("cursor", "pointer");
            });
        }
        
        function saveLead(id) {
            $("#submitLead"+id).css("cursor", "default");
            $("#submitLead"+id).prop("disabled", "disabled");
            $("#submitLead"+id).html('Wait, Loading...');
            
            $.post("https://www.shoresummerrentals.com/sponsors/ajax.php", $("#formLead"+id).serialize(), function(data) {
                if (data == "ok") {
                    $("#msgLeadE"+id).css("display", "none");
                    $("#msgLeadS"+id).css("display", "");
                    $("#msgLeadS"+id).fadeOut(5000);
                    setTimeout("leadBox('hide', "+id+");", 4000);
                    $("#title_replied"+id).css("display", "none");
                    $("#new_replied"+id).css("display", "");
                } else {
                    $("#msgLeadE"+id).html(data);
                    $("#msgLeadE"+id).css("display", "");
                    $("#msgLeadS"+id).css("display", "none");
                }
                $("#submitLead"+id).html('Submit');
                $("#submitLead"+id).prop("disabled", "");
                $("#submitLead"+id).css("cursor", "pointer");
            });
        }
        
        function reviewBox(option, id) {
            $("#reviews-list").children(".item-review").children(".review-detail").stop(true,true).slideUp();
            $("#reviews-list").children(".item-review").children(".review-summary").stop(true,true).slideDown().removeClass("new");
            if (option == "show") {
                $("#review-summary-"+id).slideUp(); 
                $("#review-detail-"+id).slideDown();
                setItemAsViewed("review", id);
            } else {
                $("#review-summary-"+id).slideDown(); 
                $("#review-detail-"+id).slideUp();
            }            
        }
        
        function leadBox(option, id) {
            $("#leads-list").children(".item-review").children(".review-detail").stop(true,true).slideUp();
            $("#leads-list").children(".item-review").children(".review-summary").stop(true,true).slideDown().removeClass("new");
            if (option == "show") {
                $("#lead-summary-"+id).slideUp(); 
                $("#lead-detail-"+id).slideDown();
                setItemAsViewed("lead", id);
            } else {
                $("#lead-summary-"+id).slideDown(); 
                $("#lead-detail-"+id).slideUp();
            }            
        }
        
        function dealBox(option, id) {
            $("#deals-list").children(".item-review").children(".review-detail").stop(true,true).slideUp();
            $("#deals-list").children(".item-review").children(".review-summary").stop(true,true).slideDown();
            if (option == "show") {
                $("#deal-summary-"+id).slideUp(); 
                $("#deal-detail-"+id).slideDown();
            } else {
                $("#deal-summary-"+id).slideDown(); 
                $("#deal-detail-"+id).slideUp();
            }            
        }
        
        function changeDealStatus(option, id, promocode) {
            $.post("https://www.shoresummerrentals.com/sponsors/deal/deal.php",{action: option, promotion_id: promocode}, function() {
                if (option == "freeUpDeal") {
                    $("#label_used"+id).css("display", "");
                } else {
                    $("#label_used"+id).css("display", "none");
                }
            });
        }
        
        function setItemAsViewed(type, id) {
            $.post("https://www.shoresummerrentals.com/sponsors/ajax.php", {
                ajax_type: 'setItemAsViewed',
                type: type,
                id: id
            }, function () {});
        }
       
        function loadDashboard(item_type, item_id) {
            $.post("<?php echo url('loadDashboard');?>", {
                ajax_type: 'load_dashboard',
                item_type: item_type,
                item_id: item_id,
                _token:'{{ csrf_token() }}'
            }, function (ret) {
                $(".webitem").removeClass("active");
                $("#"+item_type+"_"+item_id).addClass("active");
                scrollPage('#float_layer');
                $("#dashboard").hide().html(ret).fadeIn(800);
                initializeDashboard();
            });
        }

        function selectLegend(option, id, chartdata) {
            var countVisible = 0;
            
            if (option == "viewALL") {
                
                if ( $(".legend-ALL").hasClass("isvisible")) {
//                    $(".legend-ALL").removeClass("isvisible");
//                    $("#optionLegend > li > i").removeClass("checked");
//                    $("#optionLegend > li").removeClass("isvisible");
//                    $("#controlLegend > li").remove();
                } else {
                    countVisible = 2;
                    $("#optionLegend > li > i").addClass("checked");                   
                    $(".legend-ALL").addClass("isvisible");
                    $("#optionLegend > li").not(".isvisible").clone().appendTo("#controlLegend");
                    $("#optionLegend > li").addClass("isvisible");
                }
            } else {
                id: id
                chartdata: chartdata
                $newlegend = $(".legend-"+id).clone();

                if ($(".legend-"+id).hasClass("isvisible")) {
                    
                    //Check if there's at least one other legend selected to prevent empty chart
                    $('#optionLegend li').each(function() {
                        if ($(this).hasClass("isvisible")) {
                            countVisible++;
                        }
                    });

                    if (countVisible > 1) {
                        $(".legend-"+id).children("i").removeClass("checked");
                        $(".legend-"+id).removeClass("isvisible");
                        $("#controlLegend").children(".legend-"+id).remove();
                        $(".legend-ALL").children("i").removeClass("checked");
                        $(".legend-ALL").removeClass("isvisible");
                    }
                } else {
                    countVisible = 2;
                    $(".legend-"+id).children("i").addClass("checked");
                    $(".legend-"+id).addClass("isvisible");
                    $newlegend.appendTo("#controlLegend");
                }
            }
            if (countVisible > 1) {
                controlChart();
            }
        }
                
        function loadChart() {
            var data = {
                labels : chartLabels,
                datasets : initialReport
            };
            var steps = 5;
            var max = maxInitialReport;
            if (max < steps) {
                steps = max;
            }
            var options = {
                bezierCurve : false,
                scaleOverride: true,
                scaleSteps: steps,
                scaleStepWidth: Math.ceil(max / steps),
                scaleStartValue: 0
            };
            ctx = $("#myChart").get(0).getContext("2d");
            new Chart(ctx).Line(data, options);        
        }
       
        function controlChart() {
            
            var datasets = new Array();
            var max = 0;
            var thisHighest = 0;
            $('#optionLegend li').each(function() {
                if ($(this).hasClass("isvisible")) {
                    var reportType = $(this).attr('report');
                    if (reportType) {
                        datasets.push(window[reportType]);
                        thisHighest = Math.max.apply(Math, window[reportType].data);
                        if (thisHighest > max) {
                            max = thisHighest;
                        }
                    }
                }
            });
            
            var steps = 5;
            if (max < steps) {
                steps = max;
            }
            var options = {
                bezierCurve : false,
                scaleOverride: true,
                scaleSteps: steps,
                scaleStepWidth: Math.ceil(max / steps),
                scaleStartValue: 0
            };

            var data = {
                labels : chartLabels,
                datasets : datasets
            };
            ctx = $("#myChart").get(0).getContext("2d");
            new Chart(ctx).Line(data, options);  

        }


function scrollPage(position_id){
    
	if(!position_id){
		$position_id = '#resultsMap';
	}else {
		$position_id = position_id;
	}
	//jQuery('html,body').animate({scrollTop: jQuery($position_id).offset().top},'slow');
    jQuery('html, body').animate({ scrollTop: jQuery('#dashboard').offset().top }, 1500);
}
    </script>
	<script src="{{ asset('/front/js/Chart.js') }}"></script>		
@endsection