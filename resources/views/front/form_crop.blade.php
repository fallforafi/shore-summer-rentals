<?php
use App\Functions\Functions;
    if ($sitemgrLang) {
        $loadSitemgrLangs = true;
    }

	header("Expires: Sat, 01 Jan 2000 00:00:00 GMT");
	header("Cache-Control: no-store, no-cache, must-revalidate");
	header("Cache-Control: post-check=0, pre-check=0", FALSE);
	header("Pragma: no-cache");
	header("Content-Type: text/html; charset=UTF-8", TRUE);

?>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	</head>
	<body>
		<script type="text/javascript">
			function setCrop(op, sulfix) {
				if (op == 0) {
                    if ($('#w'+sulfix).val() == 0 || $('#h'+sulfix).val() == 0) {
                        alert("System failure, please try again.");
                        document.getElementById("image"+sulfix).value = "";
                        document.getElementById("image_type"+sulfix).value = "";
                        parent.$.fancybox.close();
                      } else {
                        parent.$.fancybox.close();
                    }
				} else if (op == 1) {
                    document.getElementById("image"+sulfix).value = "";
                    document.getElementById("image_type"+sulfix).value = "";
                    parent.$.fancybox.close();
				}
			}
		</script>
		<div id="loadingBar" align="center">
			<img src="/images/content/img_loading_bar.gif"/>
		</div>

		<div align="center">
			<p class="errorMessage" id="noImageSpan{{ $multi }}" style="display:none; height: auto; overflow: hidden" >
				<span id="errorType" style="display:none">
					Invalid image type. Please insert a JPG, GIF or PNG image.
				</span>
				<span id="errorSize" style="display:none">
					<?
					$sizeMessage = "";
					$sizeMessage .= "The image file is too large.";
					$sizeMessage .= "Please try again with another image. ". config('params.UPLOAD_MAX_SIZE')." MB.";
					echo $sizeMessage;
					?>
				</span>
				<br />
				Please try again with another image.
			</p>
		</div>

		<div id="crop" style="display:none" align="center">
			<img id="imgUpload{{ $multi }}" style="display:none"/>
		</div>
		<?php /* if (Functions::string_strpos($_SERVER["HTTP_REFERER"], SITEMGR_ALIAS)){ ?>
		<div id="cropButtons" align="center">
			<input type="button" id="ButtonCropSubmit" style="display:none" value="Submit" onClick="setCrop(0, '{{ $multi }}');" class="input-button-form">
			<input type="button" value="Cancel" onClick="setCrop(1, '{{ $multi }}');" class="input-button-form">
		</div> 
		<?php }*/ ?>
		<div id="cropButtons" class="baseButtons">

			<p class="standardButton">
				<button id="ButtonCropSubmit" onClick="setCrop(0, '{{ $multi }}');" type="button">Submit</button>
			</p>
			<p class="standardButton">
				<button type="submit" onClick="setCrop(1, '{{ $multi }}');">Cancel</button>
			</p>
		
		</div>
		

	</body>
</html>