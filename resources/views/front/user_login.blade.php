@extends('front')

@section('content')

	<section class="login-page">
		<div class="container">	
			<div class="login__inr mt30 p20 clrhm inline-btns user-login-main">
				<h1>LOG IN TO THE OWNER AREA</h1>
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                             <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
				<div class="login__fom bdr1">
			<form name="formDirectory" method="post" action="{{ url('postLogin') }}" >
						 <input type="hidden" name="_token" value="{{ csrf_token() }}">
			@if(isset($advertise))			 
			<input type="hidden" name="advertise" value="yes">
            <input type="hidden" name="destiny" value="{{ $destiny }}">
            <input type="hidden" name="query" value="{{ $query }}">
            @endif
						<div class="form-group">
							<label>Account Holder E-mail</label>
							<input class="form-control" type="email" name="username" />
						</div>
						
						<div class="form-group">
							<label>Password</label>
							<input class="form-control" type="password" name="password" />
						</div>
						
						<div class="form-group">
							<input class="form-control" type="checkbox" name="check" />
							<label>Sign me in automatically</label>
						</div>
						
						<div class="form-group">
							<a href="#" class="forgot-pwd" >Forgot your password ?</a>
							<button class="form-control pul-rgt" type="submit" name="submit" />LOGIN</button>
						</div>
						
						<hr class="dvr"  />
						
						<div class="botlinks clrlist listview text-center">
							<ul>
								<li><a href="{{ url() }}">Back to Website</a></li>
								<li><a href="{{ url('advertise') }}">Do you want to advertise with us?</a></li>
							</ul>
						</div>
						
					</form>
				</div>
			</div>
	
	
	
		</div>
	</section>
@endsection