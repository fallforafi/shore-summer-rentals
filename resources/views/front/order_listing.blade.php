<?php 
use App\Functions\Functions;
use App\Functions\ListingCategory;


$defaultActionForm = $formloginaction."&amp;query=level=".(($level) ? $level : '');
    

  //$defaultActionForm .= "&amp;listingtemplate_id=".(isset($_POST["listingtemplate_id"]) ? $_POST["listingtemplate_id"] : $_GET["listingtemplate_id"]);


$feedDropDown = "<select name=\"feed\" id=\"feed\" multiple=\"multiple\" size=\"5\">";



   if (count(old('feed'))>0 ) {

        $return_categories_array = explode(",", old('feed'));

        if ($return_categories_array) {

            foreach ($return_categories_array as $each_category) {

                $categories[] = $cat = new ListingCategory();
                $cat->ListingCategory($each_category);


            }
        }
    }

    $catSelected = 0;
    if (count(old('feed'))>0) {
        $auxListing = new ListingCategory();
        $auxListing->ListingCategory();

        foreach ($categories as $category) {


            if ($category instanceof $auxListing) {
                $name = $category->title;
                $feedDropDown .= "<option value='".$category->id."'>$name</option>";
                $feedAjaxCategory[] = $category->id;
                $catSelected++;
            }
        }
    } else {

        $feedDropDown .= "<option></option>";

    }

    $feedDropDown .= "</select>";



?>
@extends('front')

@section('content')

    <link rel="stylesheet" href="{{ asset('front/css/structure.min.css') }}" />
	<link rel="stylesheet" href="{{ asset('front/css/bootstrap.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('front/css/members.min.css') }}" />
	<link rel="stylesheet" href="{{ asset('front/css/content_custom.min.css') }}" />

	<!--<link href="https://www.shoresummerrentals.com/custom/domain_1/theme/default/content_custom.min.css" rel="stylesheet" type="text/css" media="all">-->


    <!--link rel="stylesheet" href="{{ asset('front/css/bootstrap-responsive.min.css') }}" /-->
 
<div class="order-listing-page">
  

            <div>
               
                
    <div class="order-listing-container container">

        
	    <div class="content content-full">
	        
	        
    <script language="javascript" type="text/javascript">
		<!--

		function orderCalculate() {

			var xmlhttp;


			try {
				xmlhttp = new XMLHttpRequest();

			} catch (e) {
				try {
					xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
				} catch (e) {
					try {
						xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
					} catch (e) {
						xmlhttp = false;
					}
				}
			}


			if (document.getElementById("check_out_payment")) document.getElementById("check_out_payment").className = "isHidden";
			$("#check_out_payment_2").removeClass("isVisible").addClass("isHidden");
			if (document.getElementById("check_out_free")) document.getElementById("check_out_free").className = "isHidden";
            $("#check_out_free_2").removeClass("isVisible").addClass("isHidden");
			if (document.getElementById("loadingOrderCalculate")) document.getElementById("loadingOrderCalculate").style.display = "";
			if (document.getElementById("loadingOrderCalculate")) document.getElementById("loadingOrderCalculate").innerHTML = "Wait, Loading...";
			if (xmlhttp) {


            xmlhttp.onreadystatechange = function() {
					if (xmlhttp.readyState == 4) {
						if (xmlhttp.status == 200) {
							var price = xmlhttp.responseText;
							var arrPrice = price.split("|");
							var html = "";
							var tax_status = '';
                            var tax_info = "";
									if (arrPrice[0] > 0) {
									if (tax_status == "on") {
										html += "<strong>Subtotal Amount: </strong> <?php echo config('params.CURRENCY_SYMBOL'); ?>"+arrPrice[0].substring(0, arrPrice[0].length-2)+"."+arrPrice[0].substring(arrPrice[0].length-2, arrPrice[0].length);
										html += "<br /><strong>Tax Amount: </strong> <?php echo config('params.CURRENCY_SYMBOL'); ?>"+arrPrice[1].substring(0, arrPrice[1].length-2)+"."+arrPrice[1].substring(arrPrice[1].length-2, arrPrice[1].length);
										html += "<br /><strong>Total Price: </strong> <?php echo config('params.CURRENCY_SYMBOL'); ?>"+arrPrice[2].substring(0, arrPrice[2].length-2)+"."+arrPrice[2].substring(arrPrice[2].length-2, arrPrice[2].length);
										tax_info = "+7% Sales Tax (" + " <?php echo config('params.CURRENCY_SYMBOL'); ?>"+arrPrice[2].substring(0, arrPrice[2].length-2)+"."+arrPrice[2].substring(arrPrice[2].length-2, arrPrice[2].length) + ")";
									} else {
										html += "<strong>Total Price: </strong> <?php echo config('params.CURRENCY_SYMBOL'); ?>"+arrPrice[0].substring(0, arrPrice[0].length-2)+"."+arrPrice[0].substring(arrPrice[0].length-2, arrPrice[0].length);
									}
									$('#divTax').addClass('isVisible');
									$('#divTax').removeClass('isHidden');
                                    $("#free_item").attr("value", "0");
									if (document.getElementById("check_out_payment")) document.getElementById("check_out_payment").className = "isVisible";
                                    $("#check_out_payment_2").addClass("isVisible").removeClass("isHidden");
									if (document.getElementById("payment-method")) document.getElementById("payment-method").className = "isVisible";
					if (document.getElementById("checkoutpayment_total")) document.getElementById("checkoutpayment_total").innerHTML = html;
								} else {
									$('#divTax').addClass('isHidden');
									$('#divTax').removeClass('isVisible');
                                    $("#free_item").attr("value", "1");
									if (document.getElementById("check_out_free")) document.getElementById("check_out_free").className = "isVisible";
									$("#check_out_free_2").addClass("isVisible").removeClass("isHidden");
                                    if (document.getElementById("payment-method")) document.getElementById("payment-method").className = "isHidden";
									if (document.getElementById("checkoutfree_total")) document.getElementById("checkoutfree_total").innerHTML = "<strong>Total Price: </strong> <?php echo config('params.CURRENCY_SYMBOL'); ?>FREE";
								}
								if (tax_status == "on") document.getElementById("taxInfo").innerHTML = tax_info;
								if (document.getElementById("loadingOrderCalculate")) 
                                    document.getElementById("loadingOrderCalculate").style.display = "none";
							if (document.getElementById("loadingOrderCalculate")) document.getElementById("loadingOrderCalculate").innerHTML = "";
						}
					}
				}
				var get_level = "";
			if (document.order_item.level) get_level = "&level=" + document.order_item.level.value;
				var get_categories = "";
				if (document.order_item.feed) get_categories = "&categories=" + document.order_item.feed.length;
                
				var get_listingtemplate_id = "";
				if (document.order_item.select_listingtemplate_id) get_listingtemplate_id = "&listingtemplate_id=" + document.order_item.select_listingtemplate_id.value;
                    
				var get_discount_id = "";
				if (document.order_item.discount_id) get_discount_id = document.order_item.discount_id.value;
                
                var get_type = "";
				if (document.order_item.type) get_type = "&type=" + document.order_item.type.value;
                
                var get_expiration_setting = "";
				if (document.order_item.expiration_setting) get_expiration_setting = "&expiration_setting=" + document.order_item.expiration_setting.value;
                
				var get_unpaid_impressions = "";
				if (document.order_item.unpaid_impressions) get_unpaid_impressions = "&unpaid_impressions=" + document.order_item.unpaid_impressions.value;

              
                xmlhttp.open("GET", "<?php echo url('ordercalculateprice'); ?>?item=listing&item_id=<?php echo $unique_id; ?>"+get_level+get_categories+get_listingtemplate_id+"&discount_id="+get_discount_id+get_type+get_expiration_setting+get_unpaid_impressions, true);
				xmlhttp.send(null);
			}
		}
			function templateSwitch(template) {
				if (!template) template = 0;
				var title_template_0 = 'Title * <span>Required field</span>';
				document.order_item.listingtemplate_id.value = template;
				if (document.getElementById("title_label")) {
                    document.getElementById("title_label").innerHTML = eval("title_template_" + template);
                }
				orderCalculate();
				loadCategoryTree('template', 'listing_', 'ListingCategory', 0, template, '<?php echo url('order_listing'); ?>',1);
				updateFormAction();
			}
		        
                
        function updateFormAction() {
            var levelValue = "";
            var titleValue = "";
            var templateValue = "";
            var categValue = "";
            var discountValue = "";
            var packageValue = "";
            var packageID = "";
            var startDateValue = "";
            var endDateValue = "";
            var expirationValue = "";
            var advertiseItem = "listing";
            
            //Get level/type
            if (document.order_item.level) {
                levelValue = "level=" + document.order_item.level.value;
            } else if (document.order_item.type) {
                levelValue = "type=" + document.order_item.type.value;
            }
            
            //Get Title/Caption
            if (document.order_item.title) {
                titleValue = "&title=" + urlencode(document.order_item.title.value);
            } else if (document.order_item.caption) {
                titleValue = "&caption=" + urlencode(document.order_item.caption.value);
            }
            
            //Get expiration setting (banner)
            if (document.order_item.expiration_setting) {
                expirationValue = "&expiration_setting=" + document.order_item.expiration_setting.value;
            }
            
            //Get Template ID
            if (document.order_item.select_listingtemplate_id) {
                templateValue = "&listingtemplate_id=" + document.order_item.select_listingtemplate_id.value;
            }
            
            //Get Discount
            if (document.order_item.discount_id) {
                discountValue = "&discount_id=" + document.order_item.discount_id.value;
            }
            
            //Get Start Date (event)
            if (document.order_item.start_date) {
                startDateValue = "&start_date=" + document.order_item.start_date.value;
            }
            
            //Get End Date (event)
            if (document.order_item.end_date) {
                endDateValue = "&end_date=" + document.order_item.end_date.value;
            }
            
                                
           //Get Categories
            feed = document.order_item.feed;
            var return_categories = "";

            for (i = 0; i < feed.length; i++) {
                if (!isNaN(feed.options[i].value)) {
                    if (return_categories.length > 0) {
                        return_categories = return_categories + "," + feed.options[i].value;
                    } else {
                        return_categories = return_categories + feed.options[i].value;
                    }
                }
            }
            if (return_categories.length > 0) {
                categValue = "&return_categories=" + return_categories;
            }
            
                        
            //Get package
            if ($("#using_package").val() == "y") {
                packageID = $("#aux_package_id").val();
                packageValue = "&package_id="+packageID;
            } else if (advertiseItem == "article") {
                packageID = "skipPackageOffer";
                packageValue = "&package_id="+packageID;
            }
            
if (document.formDirectory != undefined)    document.formDirectory.action = "{{ $formloginaction }}&query=" + levelValue + templateValue + titleValue + categValue + discountValue;            


            }

        
		function JS_addCategory(id) {

            var seed = document.order_item.seed;
            var feed = document.order_item.feed;
            var flag = true;
             var text = $("#liContent"+id).html();
        

            for (i = 0; i < feed.length; i++) {
                if (feed.options[i].value == id) {
                    flag = false;
                }
                if (!feed.options[i].value) {
                    feed.remove(feed.options[i]);
                }
            }
            if (text && id && flag) {
                feed.options[feed.length] = new Option(text, id);
                $('#categoryAdd'+id).after("<span class=\"categorySuccessMessage\">Successfully added!</span>").css('display', 'none');
              $('#removeCategoriesButton').show(); 

                $('.categorySuccessMessage').fadeOut(5000);
                orderCalculate();
                updateFormAction();
            } else {
                if (!flag) {
                    $('#categoryAdd'+id).after("</a> <span class=\"categoryErrorMessage\">Already added</span> </li>");
                } else {
                    ('#categoryAdd'+id).after("</a> <span class=\"categoryErrorMessage\">Please, select a valid category</span> </li>");
                }
            }
            

        }

        
                
		function JS_submit() {
            disableButtons();
            feed = document.order_item.feed;
			return_categories = document.order_item.return_categories;
			if (return_categories.value.length > 0) {
                return_categories.value = "";
            }
			for (i = 0; i < feed.length; i++) {
				if (!isNaN(feed.options[i].value)) {
					if (return_categories.value.length > 0) {
                        return_categories.value = return_categories.value + "," + feed.options[i].value;
                    } else {
                        return_categories.value = return_categories.value + feed.options[i].value;
                    }
				}
			}
            		}

		//-->
	</script>

    <div style="display:none">
        
<form id="formDirectory" name="formDirectory" method="post" action="<?php echo $defaultActionForm; ?> ">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">

		
            <input type="hidden" name="advertise" value="yes">
            <input type="hidden" name="destiny" value="">
            <input type="hidden" name="query" value="">
            
            <input type="hidden" name="username" id="form_username" value="">
            <input type="hidden" name="password" id="form_password" value="">

        </form>
        
    </div>

    <form name="order_item" action="<?php echo url('order_listing'); ?>" method="post" class="standardForm" onsubmit="JS_submit();">


        <input type="hidden" name="advertise" value="yes">
        <input type="hidden" name="signup" value="true">
        
         <input type="hidden" name="level" id="level" value="{{ $level }}">
                
      <input type="hidden" name="listingtemplate_id" id="listingtemplate_id" value="">
        
        <div class="content-main" id="screen1" style="<?php echo (session('message_contact')|| session('message_account')?'display:none;':'display:block;'  ) ;  ?>">

            <div class="order-head">
                <h2>
                    Listing {{ $labelName }}                                       
                    - <i>{{ $labelPrice }}</i>{{ $labelPriceRenewal }}</h2>
             </div>

            <div class="order">

                <div id="errorMessage">&nbsp;</div>

                <div id="listing-info">

                    <div class="left textright">
                        <h3>Listing Information</h3>
                        <p>
                            What do you want to call your listing? This is where you give your listing title a catchy phrase to grab the viewer’s attention.                            Examples – "Enjoy Beachfront Luxury at its Finest" or "Affordable Beach Block Home with Pool"                        </p>
                    </div>

                    <div class="right">
                        
                        <div class="cont_70">
                            <label id="title_label" for="listing-title">Listing Title * <span>Required field</span></label>
                     <input type="text" name="title" id="listing-title" value="{{ old('title') }}" maxlength="100" onblur="easyFriendlyUrl(this.value, 'friendly_url', 'a-zA-Z0-9', '-'); updateFormAction(); $('#adv_title').html(this.val());">
                 <input type="hidden" name="friendly_url" id="friendly_url" value="" maxlength="150">
                                    </div>
                        
                                             

                                                
                        
                    </div>

                </div>

                                <div id="categories">

                    <div class="left textright">

                        <h3>Categories and sub-categories</h3>
                        <p>
                            Click on the “Add” icon to the right of the Vacation Rental Category.                            
                                                    </p>

                    </div>

                    <div class="right">

                        <p class="warningBOXtext">Only select sub-categories that directly apply to your type.<br>Your listing will automatically appear in the main category of each sub-category you select.</p>

                        <div class="cont_50">

                            <input type="hidden" name="return_categories" value="" />

                            <div class="treeView">

                                  <ul id="listing_categorytree_id_0" class="categoryTreeview">
                                    <li>&nbsp;</li>
                                </ul>
           

                            </div>

                        </div>

                        <div class="cont_50">
                            <label>Listing Categories * <span>Required field</span></label>
               <?php echo $feedDropDown; ?>
              <div class="text-center" id="removeCategoriesButton" style="display:none;">
                <a href="javascript:void(0);" onclick="JS_removeCategory(document.order_item.feed, true);">Remove Selected Category</a>
                            </div>
                        </div>

                    </div>

                </div>
                
                
                <div id="payment-method" class="isVisible">

                    <div class="left textright">
                        <h3>Payment Method</h3>
                        <p>How do you want to pay us?</p>
                    </div>

                    <div class="right">

                        <div class="clear clearfix option">
                         <div>
            <input type="radio" name="payment_method" value="authorize" id="radio1" 
            {{ ( old('payment_method') == "authorize" ? "checked=\"checked\"" : "") }} /><label for="radio1">By Credit Card</label>
            </div>

            <div>
            <input type="radio" name="payment_method" value="paypal" id="radio2"
             {{ ( old('payment_method') == "paypal" ? "checked=\"checked\"" : "") }}><label for="radio2">By PayPal</label>
            </div>

            <div>
            <input type="radio" name="payment_method" value="invoice" id="radio3" 
            {{ ( old('payment_method') == "invoice" ? "checked=\"checked\"" : "") }}/><label for="radio3">Print Invoice and Mail a Check</label>
            </div>                  
             </div>

                                                
  						<div class="clear clearfix cont_50">
         <label for="promocode">Promotional Code</label>
    <input type="text" id="promocode" name="discount_id" value="{{ old('discount_id') }}" maxlength="10" onblur="orderCalculate(); updateFormAction();">
                        </div>
                     
                  		                        
                    </div>

                </div>

                
                <div class="blockcontinue cont_100">

     <div id="loadingOrderCalculate" class="loadingOrderCalculate" style="display: none;"></div>

                     <input type="hidden" name="userLogged" id="userLogged" value="" />

                    <input type="hidden" name="free_item" id="free_item" value="1">

                    
                    <div id="check_out_payment" class="{{ $checkoutpayment_class }}">

                        <div class="cont_60 ">
                            <div id="checkoutpayment_total" class="orderTotalAmount"></div>
                        </div>

                        <div class="cont_40 ">
                         <script type="text/javascript">


 var activeNextStep = true;
function nextStep(item_type, feed, item_title, gotoPackage, finalStep) {
  

    if (!activeNextStep) {
        return;
    }
    activeNextStep = false;
        
    var paymentRadio = $('input[name=payment_method]');
    var checkedValue = paymentRadio.filter(':checked').val();
    
    if (finalStep) {
        $("#screenPackage").hide();
        $("#screen2").fadeIn();
        activeNextStep = true;
    } else {
     
       
        //validate first step

        var return_categories = "";
  
        if (feed != false) {
            for (i = 0; i < feed.length; i++) {
                if (!isNaN(feed.options[i].value)) {
                    if (return_categories.length > 0) {
                        return_categories = return_categories + "," + feed.options[i].value;
                    } else {
                        return_categories = return_categories + feed.options[i].value;
                    }
                }
            }
        }


        var payment_selected = 0;
        
        if (checkedValue || $("#free_item").val() == "1" || $("#userLogged").val() == "1") {
            payment_selected = 1;
        }

        $.post("<?php echo url('validateAdvertise'); ?>", {
            _token:                 '<?php echo csrf_token();  ?>',
            item_type:              item_type,
            signup:                 1,
            title:                  $("#"+item_title).val(),
            friendly_url:           $("#friendly_url").val(),
            return_categories:      return_categories,
            discount_id:            $("#promocode").val(),
            has_payment:            payment_selected
        }, function (response) {
            if (response.trim() == "ok") {
                $("#errorMessage").hide();
                $("#screen1").hide();
                $("#screen2").hide();
                if (gotoPackage) {
                    $("#screenPackage").fadeIn();
                } else {
                    $("#screen2").fadeIn();
                   $("#adv_title").html($("#listing-title").val());


                }
            } else {
                $("#errorMessage").html("<p class=\"errorMessage\">"+response+"</p>");
                $('html, body').animate({
                    scrollTop: $('#errorMessage').offset().top
                }, 500);
                $("#errorMessage").fadeIn();
            }
            activeNextStep = true;
            enableButtons();
        });
    
    }

}

function backStep(is_package, go_package) {
    if (is_package) {
        $("#screenPackage").hide();
    }
    $("#screen1").hide();
    $("#screen2").hide();
    
    if (go_package) {
         $("#screenPackage").fadeIn();
    } else {
        $("#screen1").fadeIn();
    }
}

function submitForm(formName) {
    if (typeof updateFormAction == "function") {
        updateFormAction();
    }
    if (formName == "formDirectory") {
        $("#form_username").attr("value", $("#dir_username").val());
        $("#form_password").attr("value", $("#dir_password").val());
        document.formDirectory.submit();
    } else if (formName == "formCurrentUser") {
        document.formCurrentUser.submit();
    }

}

function disableButtons() {
    var i;
    for (i = 1; i <= 2; i++) {
        document.getElementById('button'+i).innerHTML = "Wait";
    }
}

function enableButtons() {
    var i;
    for (i = 1; i <= 2; i++) {
        document.getElementById('button'+i).innerHTML = "Continue";
    }
}

function acceptPackage(pvalue) {
    $("#using_package").attr("value", pvalue);
    updateFormAction();
}




 </script>
                            <p class="checkoutButton bt-highlight">                               
 <button type="button" id="button1" onclick="return nextStep('listing', document.order_item.feed, 'listing-title', false,false);">Continue</button>
                <a href="javascript: void(0);" onclick="return backStep(true);">Or back to previous page</a>
                                <em></em>                               
                            </p>
                        </div>
                    </div>

                    
                    <div id="check_out_free" class="isHidden">

                        <div class="cont_60 ">
                            <div id="checkoutfree_total" class="orderTotalAmount"></div>
                        </div>

                        <div class="cont_40">
                            <p class="checkoutButton bt-highlight">                                
                              
                               
                           		<em></em>
                            </p>
                        </div>

                    </div>

                </div>

            </div>

        </div>
           
        
        <div class="content-main" id="screen2" style="<?php echo (session('message_contact')|| session('message_account')?'display:block;':'display:none;'  );  ?>">

            <div class="real-steps">
			    <ul class="standardStep steps-3">
			        <li class="steps-ui stepLast"><span>3</span>&nbsp;Confirmation</li>
			        <li class="steps-ui"><span>2</span>&nbsp;Check Out</li>
			        <li class="steps-ui stepActived"><span>1</span>&nbsp;Identification</li>
			    </ul>
			</div>

			<div class="row-fluid login-page">

			    <div class="col-sm-12 login-form-resp">
                    
			        <h1 class="text-center capitalized">
			            Register below to create a listing for <q id="adv_title"></q>
			            <small>Already registered? <a href="javascript:void(0);" onclick="$('#advertise_signup').css('display', 'none'); $('#advertise_login').fadeIn(500);">Login</a> to your account.</small>
			        </h1>
                    
			        <hr>

			        <div id="advertise_login" style="display:none">

			            <section class="login-box">


			                            
		<div class="form-login">
			<label for="username">Account Holder E-mail</label>
			<input class="col-sm-12" type="email" name="dir_username" id="dir_username" value="">
			<span class="clear"></span>
			<label for="password">Password</label>
			<input class="col-sm-12" type="password" name="dir_password" id="dir_password" value="">
		</div>
		
                    <div class="checkbox">
                <label>
                    <input type="checkbox" name="automatic_login" value="1" class="checkbox">
                    Sign me in automatically                </label>
            </div>
                    
        <div class="row-fluid action">

            <div class=" pull-left col-sm-6">					
     <p class="forgotpassword"><a href="https://www.shoresummerrentals.com/sponsors/forgot.php">Forgot your password?</a></p>
                            </div>

   <button class="btn btn-login pull-right col-sm-6" type="button" onclick="submitForm('formDirectory');">Log in</button>

        </div>			
            
	
			            </section>

			            <section class="login-underbox">
			                <p><a class="link-highlight" href="javascript:void(0);" onclick="$('#advertise_login').css('display', 'none'); $('#advertise_signup').fadeIn(500);">Not a member? Sign up now</a></p>
			            </section>

			        </div>
                    
	 <div id="advertise_signup" >
                        
			            <section class="login-box" >
 @if (session('message_contact') || session('message_account'))
 <p class="errorMessage" > 
    <?php if (Functions::string_strlen(trim(session('message_contact'))) > 0) { ?>
       <?php  echo session('message_contact'); ?>
            <?php } ?>
            <?php if ((Functions::string_strlen(trim(session('message_contact'))) > 0) && (Functions::string_strlen(trim(session('message_account'))) > 0)) { ?>
                <br />
            <?php } ?>
            <?php if (Functions::string_strlen(trim(session('message_account'))) > 0) { ?>
       <?php  echo session('message_account'); ?>
            <?php } ?>
 </p>
@endif
			                    
    <div class="form-signup">
        
        <div class="col-sm-6">
            <label for="first_name">Account Holder First Name</label>
            <input class="col-sm-12" type="text" name="first_name" id="first_name" value="{{ old('first_name') }}">
        </div>
        
        <div class="col-sm-6">
            <label for="last_name">Account Holder Last Name</label>
            <input class="col-sm-12" type="text" name="last_name" id="last_name" value="{{ old('last_name') }}">
        </div>

        <div class="col-sm-12"><label for="username">Account Holder E-mail</label>
        <input class="col-sm-12" type="email" name="username" id="username" value="{{ old('username') }}" maxlength="80" onblur="validateEmail(this.value); populateField(this.value,'email');"></div>
        <input type="hidden" name="email" id="email" value="">
        <p id="notValidAccount" class="alert alert-danger" style="display: none;">
            <i>This account is already in use.            <a href="javascript:void(0);" onclick="$('#advertise_signup').css('display', 'none'); $('#advertise_login').fadeIn(500);">Log in here</a>
            </i>
        </p>
        

         <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="col-sm-12">
			<label for="password">Account Holder Password</label>
			<input class="col-sm-12" type="password" name="password" id="password" maxlength="50">
        </div>
		<div class="col-sm-12">
			<label for="address">Account Holder Mailing Address1</label>
			<input class="col-sm-12" type="text" name="address" id="address" value="{{ old('address') }}" maxlength="50">    
		</div>
		<div class="col-sm-12">
			<label for="address2">Account Holder Mailing Address2</label>
			<input class="col-sm-12" type="text" name="address2" id="address2" value="{{ old('address2') }}" maxlength="50">
        </div>
        <div class="col-sm-6" style="margin-left: 0;">
            <label for="city">Account Holder Mailing City</label>
            <input class="col-sm-12" type="text" name="city" id="city" value="{{ old('city') }}"> 
        </div>

        <div class="col-sm-6">   
            <label for="state">Account Holder Mailing State</label>
            <input class="col-sm-12" type="text" name="state" id="state" value="{{ old('state') }}">
        </div>
        
        <span class="clear"></span>
		<div class="col-sm-12">
			<label for="zip">Account Holder Mailing Zip</label>
			<input class="col-sm-12" type="text" name="zip" id="zip" value="{{ old('zip') }}">
		</div>
        <div class="col-sm-6" style="margin-left: 0;">
            <label for="phone">Account Holder Phone</label>
            <input class="col-sm-12" type="text" name="phone" id="phone" value="{{ old('phone') }}">
        </div>

        <div class="col-sm-6">
            <label for="fax">Account Holder Fax</label>
            <input class="col-sm-12" type="text" name="fax" id="fax" value="{{ old('fax') }}">   
        </div>

        <span class="clear"></span>

    </div>

    
    <div class="row-fluid action">
        <p class="pull-left col-sm-6 text-small doubleline">
            By creating an account, I accept the <a rel="nofollow" href="https://www.shoresummerrentals.com/popup/popup.php?pop_type=terms" class="fancy_window_iframe">Terms of Use</a>.            <input type="hidden" name="agree_tou" value="1">
        </p>
        
                
                    
            <button class="btn btn-login pull-right col-sm-6" id="check_out_payment_2" type="submit" name="continue" value="">Submit</button>
        
                    
            <button class="btn btn-login pull-right col-sm-5" style="display:none;" id="check_out_free_2" type="submit" name="checkout" value="Continue">Submit</button>
            
            </div>
			            </section>

			            <section class="login-underbox">
			                <p><a class="already-login" href="javascript:void(0);" onclick="$('#advertise_signup').css('display', 'none'); $('#advertise_login').fadeIn(500);">Already a member - Log in here.</a></p>
			            </section>

			        </div>
                    
			    </div>

			</div>

        </div>

    </form>
	    </div>

	</div>

    <script language="javascript" type="text/javascript">
    function loadCategoryTree(action, prefix, category, category_id, template_id, path, domain_id) {

    var ajax_categories = 0;
    
    if (document.getElementById("feed")) {
        feed = document.getElementById("feed");
    } else if (document.getElementById("feed_listing") && prefix == "listing_") {
        feed = document.getElementById("feed_listing");
    } else if (document.getElementById("feed_event") && prefix == "event_") {
        feed = document.getElementById("feed_event");
    } else if (document.getElementById("feed_classified") && prefix == "classified_") {
        feed = document.getElementById("feed_classified");
    } else if (document.getElementById("feed_article") && prefix == "article_") {
        feed = document.getElementById("feed_article");
    }

    if (feed) {
        
        var categories_aux = new Array();
        
        if (feed.length > 0) {
            for(i =0 ; i < feed.length ; i++){
              categories_aux[i] = feed.options[i].value;
            }
            var categories = categories_aux.join(",");
            ajax_categories = categories;
        }
    }

    var xmlhttp;
    try {
        xmlhttp = new XMLHttpRequest();
    } catch (e) {
        try {
            xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
        } catch (e) {
            try {
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            } catch (e) {
                xmlhttp = false;
            }
        }
    }
    document.getElementById(prefix+"categorytree_id_"+category_id).style.display = "";
    document.getElementById(prefix+"categorytree_id_"+category_id).innerHTML = "<li class='loading'>Wait, Loading...</li>";
    if (xmlhttp) {
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == 4) {
                if (xmlhttp.status == 200) {
                    if (category_id > 0) document.getElementById(prefix+"opencategorytree_id_"+category_id).style.display = "none";
                    if (category_id > 0) document.getElementById(prefix+"opencategorytree_title_id_"+category_id).style.display = "none";
                    if (category_id > 0) document.getElementById(prefix+"closecategorytree_id_"+category_id).style.display = "";
                    if (category_id > 0) document.getElementById(prefix+"closecategorytree_title_id_"+category_id).style.display = "";
                    document.getElementById(prefix+"categorytree_id_"+category_id).innerHTML = xmlhttp.responseText;
                }
            }
        }
        
        xmlhttp.open("GET", "<?php echo url('loadcategorytree'); ?>?action=" + action + "&prefix=" + prefix + "&category=" + category + "&category_id=" + category_id + "&template_id=" + template_id + "&ajax_categories=" + ajax_categories + "&path=" + path, true);
        xmlhttp.send(null);
    }
}

function closeCategoryTree(prefix, category, category_id, path) {
    if (category_id > 0) document.getElementById(prefix+"closecategorytree_id_"+category_id).style.display = "none";
    if (category_id > 0) document.getElementById(prefix+"closecategorytree_title_id_"+category_id).style.display = "none";
    if (category_id > 0) document.getElementById(prefix+"opencategorytree_id_"+category_id).style.display = "";
    if (category_id > 0) document.getElementById(prefix+"opencategorytree_title_id_"+category_id).style.display = "";
    document.getElementById(prefix+"categorytree_id_"+category_id).innerHTML = "";
    document.getElementById(prefix+"categorytree_id_"+category_id).style.display = "none";
}
function JS_removeCategory(feed, order) {
    if (feed.selectedIndex >= 0) {
        $('#categoryAdd'+feed.options[feed.selectedIndex].value).after($('.categorySuccessMessage').empty());
        $('#categoryAdd'+feed.options[feed.selectedIndex].value ).fadeIn(500);
        feed.remove(feed.selectedIndex);
        if (order){
            orderCalculate();
        }
    }
    
    if(feed.length == 0){
        $('#removeCategoriesButton').hide();
    }

}


function urlencode( str ) {

    var histogram = {}, tmp_arr = [];
    var ret = (str+'').toString();

    var replacer = function(searchT, replace, str) {
        var tmp_arr = [];
        tmp_arr = str.split(searchT);
        return tmp_arr.join(replace);
    };

    // The histogram is identical to the one in urldecode.
    histogram["'"]   = '%27';
    histogram['(']   = '%28';
    histogram[')']   = '%29';
    histogram['*']   = '%2A';
    histogram['~']   = '%7E';
    histogram['!']   = '%21';
    histogram['%20'] = '+';
    histogram['\u20AC'] = '%80';
    histogram['\u0081'] = '%81';
    histogram['\u201A'] = '%82';
    histogram['\u0192'] = '%83';
    histogram['\u201E'] = '%84';
    histogram['\u2026'] = '%85';
    histogram['\u2020'] = '%86';
    histogram['\u2021'] = '%87';
    histogram['\u02C6'] = '%88';
    histogram['\u2030'] = '%89';
    histogram['\u0160'] = '%8A';
    histogram['\u2039'] = '%8B';
    histogram['\u0152'] = '%8C';
    histogram['\u008D'] = '%8D';
    histogram['\u017D'] = '%8E';
    histogram['\u008F'] = '%8F';
    histogram['\u0090'] = '%90';
    histogram['\u2018'] = '%91';
    histogram['\u2019'] = '%92';
    histogram['\u201C'] = '%93';
    histogram['\u201D'] = '%94';
    histogram['\u2022'] = '%95';
    histogram['\u2013'] = '%96';
    histogram['\u2014'] = '%97';
    histogram['\u02DC'] = '%98';
    histogram['\u2122'] = '%99';
    histogram['\u0161'] = '%9A';
    histogram['\u203A'] = '%9B';
    histogram['\u0153'] = '%9C';
    histogram['\u009D'] = '%9D';
    histogram['\u017E'] = '%9E';
    histogram['\u0178'] = '%9F';

    // Begin with encodeURIComponent, which most resembles PHP's encoding functions
    ret = encodeURIComponent(ret);

    for (searchT in histogram) {
        replace = histogram[searchT];
        ret = replacer(searchT, replace, ret) // Custom replace. No regexing
    }

    // Uppercase for full PHP compatibility
    return ret.replace(/(\%([a-z0-9]{2}))/g, function(full, m1, m2) {
        return "%"+m2.toUpperCase();
    });

    return ret;
}
</script>
    
    <script language="javascript" type="text/javascript">

    orderCalculate();
	loadCategoryTree('all', 'listing_', 'ListingCategory', 0, 0, '<?php echo url(); ?>',1);
		
		
        
        	</script>
    

                </div><!-- Close container-fluid div -->
        </div>
               
                
    





@endsection