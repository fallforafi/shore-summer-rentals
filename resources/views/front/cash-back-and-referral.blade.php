
@extends('front')

@section('content')

    <link rel="stylesheet" href="{{ asset('front/css/members.min.css') }}" />
	<link rel="stylesheet" href="{{ asset('front/css/content_custom.min.css') }}" />
	<link href="https://www.shoresummerrentals.com/custom/domain_1/theme/default/content_custom.min.css" rel="stylesheet" type="text/css" media="all">
    <!--link rel="stylesheet" href="{{ asset('front/css/bootstrap-responsive.min.css') }}" /-->
     
     
    
    
<style>

.content-custom h1, .content-custom h2, .content-custom h3{
    line-height: 1;
    text-indent: 0;
	margin:0;
}
.content-custom{
	margin-top:0;
}
.tellyourFriends{
	margin-top:-25px !important;
	margin-bottom:10px !important;
}
.content-custom p{
	font-size:18px;
	line-height: 1.5;
    font-weight: 300;
	margin-bottom:10px;
}
.content-custom a, .content-custom a:visited {
    text-decoration: underline;
}
</style>	  

               
                
    
    <div class="cashback-referral container">

        
		<div class="content content-full margin-top">

			<div class="content-main">
		
				<div class="content-custom"><h1><strong><br>CASH BACK REFERRAL PROGRAM</strong></h1>
<h1><span style="font-size: 10px;">&nbsp;</span></h1>
<h2 class="tellyourFriends"><span style="color: #ff6600;"><strong><span style="font-size: x-large;"><br>Tell Your Friends About ShoreSummerRentals.com</span></strong></span></h2>
<p><span style="color: #000000;"><br>"<span style="color: #000000;">Get Paid</span>" for sharing <a href="http://www.shoresummerrentals.com"><span style="color: #000000;">ShoreSummerRentals.com</span> </a>with your friends! <br><br></span><span style="color: #000000;"><em><strong>You can easily earn up to $75 for each referral!</strong></em>&nbsp;&nbsp; There is no limit, you can earn as much as you want.&nbsp; It’s easy, when your friends join us, tell them&nbsp;to<a href="http://shoresummerrentals.com/contactus.php"> contact us&nbsp;</a>with your name and email address immediately upon <a href="http://shoresummerrentals.com/advertise.php">registering</a> on our website.&nbsp; We will take care of the rest. &nbsp;&nbsp;Your friends will be able to advertise on both of our websites, <a href="http://www.shoresummerrentals.com/"><span style="color: #000000;">ShoreSummerRentals.com</span></a> &amp; <a href="http://www.vacationrentals2u.com"><span style="color: #000000;">VacationRentals2U.com</span></a>&nbsp;for one low price and you get paid in 90 days, immediately after the <a href="http://shoresummerrentals.com/content/guarantee-and-verify.html"><span style="color: #000000;">Rental Guarantee</span></a> policy has passed.&nbsp; Y</span><span style="color: #000000;">ou do not even have to be a previous or&nbsp;active customer.</span></p>
<p><strong><span style="color: #000000;">How Does The Referral Program Work?</span></strong></p>
<p><span style="color: #000000;">For the first new customer you refer to <a href="http://www.shoresummerrentals.com/"><span style="color: #000000;">ShoreSummerRentals.com</span></a>, we will pay you <em><strong>$50 cash</strong></em>.&nbsp; &nbsp;For each additional new customer you send to us within a 12 month period, we will pay you <em><strong>$75 cash for each new referral</strong></em>.&nbsp; There is no limit to the amount of money you can earn.</span></p>
<p><span style="color: #000000;">Each new referral will need to purchase an annual subscription to <a href="http://www.shoresummerrentals.com/"><span style="color: #000000;">ShoreSummerRentals.com</span></a>.&nbsp;</span></p>
<p><strong><span style="color: #000000;">IT'S SIMPLE AND A VERY EASY WAY TO EARN SOME EXTRA CASH QUICK!</span></strong></p>
<p><span style="color: #000000;">Who Can Participate? &nbsp;&nbsp;Anyone!</span></p>
<p><span style="color: #000000;">THAT’S IT! &nbsp;REFER YOUR FRIENDS NOW AND START EARNING EASY CASH!</span></p>
<p><strong><span style="color: #000000;">General Terms and Conditions</span></strong></p>
<p><span style="color: #000000;">Referrer cannot refer themselves or their own management company.</span></p>
<p><span style="color: #000000;">Referrals must be a brand new <a href="http://www.shoresummerrentals.com/"><span style="color: #000000;">ShoreSummerRentals.com</span></a> customer and must not have or previously had a property listing with us at any other time.</span></p>
<p><span style="color: #000000;">Referral &nbsp;bonus' cannot be applied retroactively for word-of-mouth referrals.&nbsp;&nbsp;Again, the referred must <a href="http://shoresummerrentals.com/contactus.php">contact us</a>&nbsp;immediately upon <a href="http://shoresummerrentals.com/advertise.php">registration</a> on <a href="http://www.shoresummerrentals.com/"><span style="color: #000000;">ShoreSummerRentals.com</span></a>.&nbsp; This program is valid beginning November 19, 2010 and can be terminated or modified at any time.</span><br><br><span style="color: #000000;">This offer is not transferrable.</span></p>
<p><span style="color: #000000;">Referrer's participation in this Program constitutes their acceptance of the program Terms and Conditions and any changes thereto. &nbsp;Referrers and referrals are responsible for remaining knowledgeable as to any changes <a href="http://www.shoresummerrentals.com/"><span style="color: #000000;">ShoreSummerRentals.com</span></a>&nbsp;may make to the program Terms and Conditions.</span></p>
<p><span style="color: #000000;"><a href="http://www.shoresummerrentals.com/"><span style="color: #000000;">ShoreSummerRentals.com</span></a> &amp; <a href="file:///C:UsersOwnerDocumentsSSRARCAvacationrentals2u.com"><span style="color: #000000;">VacationRentals2U.com</span></a> reserves the right to terminate this program at any time and, in its sole discretion, to honor any reward or refuse any referral bonus if <a href="http://www.shoresummerrentals.com/"><span style="color: #000000;">ShoreSummerRentals.com</span></a> determines that it was obtained under wrongful or fraudulent circumstances, that inaccurate or incomplete information was provided in creating a listing, or that any term of this Program or <a href="http://www.shoresummerrentals.com/"><span style="color: #000000;">ShoreSummerRentals.com</span></a>&nbsp;<a href="http://shoresummerrentals.com/content/termsof-use.html">Terms of Service </a>have been violated.</span></p>
<p><span style="color: #000000;"><br></span></p></div>			
			</div>
			
					
		</div>

	</div>





		
    </script>
	<script src="{{ asset('/front/js/Chart.js') }}"></script>		
@endsection