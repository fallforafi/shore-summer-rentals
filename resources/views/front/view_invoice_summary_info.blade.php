<?php 
use App\Models\Domain\Sql;
use App\Functions\Functions;
use App\Functions\ListingLevel;
use App\Functions\Listing;
use App\Functions\ListingChoice;
use App\Functions\EditorChoice;
use App\Functions\Invoice;
use App\Functions\InvoiceListing;
use App\Functions\CustomInvoice;

$id = $invoice->id;
$acctId=Functions::sess_getAccountIdFromSession();

//$_GET['id'] = $id

if($id){

		$sql = "SELECT * FROM Invoice WHERE id = '$id' AND status != 'N' AND hidden = 'n' ";
		if($acctId) $sql .= "AND account_id = '$acctId'";
		$r = Sql::fetch($sql);
		if(count($r) > 0) 
		$invoiceObj = new Invoice();
		$invoiceObj->Invoice($id);

		if($invoiceObj){

			//$invoiceStatusObj   = new InvoiceStatus();
			$listingLevelObj    = new ListingLevel();
			//$eventLevelObj      = new EventLevel();
			//$bannerLevelObj     = new BannerLevel();
			//$classifiedLevelObj = new ClassifiedLevel();
			//$articleLevelObj    = new ArticleLevel();

			$invoice_n["id"]           = $id;
			$invoice_n["username"]     = $invoiceObj->getString("username");
			$invoice_n["account_id"]   = $invoiceObj->getString("account_id");
			$invoice_n["ip"]           = $invoiceObj->getString("ip");
			$invoice_n["date"]         = Functions::format_date($invoiceObj->getString("date"),config('params.DEFAULT_DATE_FORMAT')." H:i:s","datetime");
			$invoice_n["status"]       = $invoiceObj->getString("status");
			$invoice_n["subtotal"]     = $invoiceObj->getString("subtotal_amount");
			$invoice_n["tax"]			 = Functions::payment_calculateTax($invoice_n["subtotal"],$invoiceObj->getString("tax_amount"),true,false); 
			$invoice_n["amount"]       = $invoiceObj->getString("amount");
			$invoice_n["currency"]     = $invoiceObj->getString("currency");
			$invoice_n["expire_date"]  = Functions::format_date($invoiceObj->getString("expire_date"),config('params.DEFAULT_DATE_FORMAT'),"date");
			$invoice_n["payment_date"] = Functions::format_date($invoiceObj->getString("payment_date"),config('params.DEFAULT_DATE_FORMAT')." H:i:s","datetime");
			

			$sql ="SELECT * FROM Invoice_Listing WHERE invoice_id = '$id'";

			$r = Sql::fetch($sql);
			$i=0;
			foreach($r as $row){

				$listingObj = new Listing();
				$listingObj->Listing($row->listing_id);


				$invoice_listing[$i]["invoice_id"]       = $invoiceObj->getString("id");
				$invoice_listing[$i]["listing_id"]       = $row->listing_id;
				$invoice_listing[$i]["listing_title"]    = $row->listing_title;
				$invoice_listing[$i]["listing_address"]  = $listingObj->getString('address');
				$invoice_listing[$i]["discount_id"]      = ($row->discount_id) ? $row->discount_id: 'N/A';
                $invoice_listing[$i]["level"]            = $row->level;
				$invoice_listing[$i]["level_label"]      = $row->level_label;
				$invoice_listing[$i]["renewal_date"]     = (Functions::format_date($row->renewal_date,config('params.DEFAULT_DATE_FORMAT'),"date")) ? Functions::format_date($row->renewal_date,config('params.DEFAULT_DATE_FORMAT'),"date") : 'N/A';
				$invoice_listing[$i]["categories"]       = $row->categories;
				$invoice_listing[$i]["extra_categories"] = $row->extra_categories;
				$invoice_listing[$i]["listingtemplate"]  = $row->listingtemplate_title;
				$invoice_listing[$i]["amount"]           = $row->amount;

				$i++;

			}

			$sql ="SELECT * FROM Invoice_ListingChoice WHERE invoice_id = '$id'";

			$r = Sql::fetch($sql);
			$i=0;
			foreach($r as $row){


				$listingChoiceObj = new ListingChoice();
				$listingChoiceObj->ListingChoice("", "",$row->listingchoice_id);

				$auxListingObj = new Listing();
				$auxListingObj->Listing($listingChoiceObj->getNumber('listing_id'));


				$invoice_listingchoice[$i]["invoice_id"]   		=  $invoiceObj->getString("id");
				$invoice_listingchoice[$i]["listing_id"]   		= $listingChoiceObj->getNumber("listing_id");
				$invoice_listingchoice[$i]["invoice_id"]   		= $invoiceObj->getString("id");
				$invoice_listingchoice[$i]["listingchoice_id"]  = $row->listingchoice_id;
				$invoice_listingchoice[$i]["listing_title"]  	= $row->listing_title;
				$invoice_listingchoice[$i]["listing_address"]   = $auxListingObj->getString('address');
				$invoice_listingchoice[$i]["badge_name"]  		= $row->badge_name;
				$invoice_listingchoice[$i]["renewal_date"]     = (Functions::format_date($row->renewal_date,config('params.DEFAULT_DATE_FORMAT'),"date")) ? Functions::format_date($row->renewal_date,config('params.DEFAULT_DATE_FORMAT'),"date") : 'N/A';
				$invoice_listingchoice[$i]["amount"]           = $row->amount;

				$i++;

			}


			$sql ="SELECT * FROM Invoice_CustomInvoice WHERE invoice_id = '$id'";

			$r = Sql::fetch($sql);
			$i=0;
			foreach($r as $row){
				
				$invoice_custominvoice[$i]["invoice_id"]        = $invoiceObj->getString("id");
				$invoice_custominvoice[$i]["custom_invoice_id"] = $row->custom_invoice_id;
				$invoice_custominvoice[$i]["title"]             = $row->title;
				$invoice_custominvoice[$i]["date"]              = $row->date;
				$invoice_custominvoice[$i]["items"]             = $row->items;
				$invoice_custominvoice[$i]["items_price"]       = $row->items_price;
				$invoice_custominvoice[$i]["subtotal"]          = $row->subtotal;
				$invoice_custominvoice[$i]["amount"]            = $row->amount;

				$i++;

			}
		}	
     
	}

  

if (isset($invoice_listing)) {?>

<table border="0" cellpadding="2" cellspacing="2" class="standard-innerTable">
		<tbody><tr>
			<th class="tableTitle" colspan="3"><p>Listings</p></th>
		</tr>
		@foreach($invoice_listing as $each_invoice_listing) 
			<tr>
				<th>
					<fieldset title='{{ $each_invoice_listing["listing_title"] }}'>
					#{{ $each_invoice_listing["listing_id"] }} - {{ $each_invoice_listing["listing_address"] }}											</fieldset>
				</th>
				<td class="infoLevel">{{ Functions::string_ucwords($each_invoice_listing["level_label"]) }}</td>
				<td class="infoAmount">
				{{ $each_invoice_listing["amount"]." (".$invoice_n["currency"].")" }}
				</td>
			</tr>
		
			@endforeach
			</tbody>
</table>
<br/>
<?php } 

if (isset($invoice_listingchoice)) {
?>
<table border="0" cellpadding="2" cellspacing="2" class="standard-innerTable">

<tbody><tr>
			<th class="tableTitle" colspan="2"><p>Listing Specials</p></th>
		</tr>
		@foreach($invoice_listingchoice as $each_invoice_listingchoice) 
			<tr>
				<th>
					<fieldset title='{{ $each_invoice_listingchoice["badge_name"] }}'>
					#{{ $each_invoice_listingchoice["listing_id"] }} - {{ $each_invoice_listingchoice["badge_name"] }}											</fieldset>
				</th>
			
				<td class="infoAmount">
				{{ $each_invoice_listingchoice["amount"]." (".$invoice_n["currency"].")" }}
				</td>
			</tr>
		
			@endforeach
			</tbody>
</table>
<br/>
<?php }
if (isset($invoice_custominvoice)) {

 ?>
<table border="0" cellpadding="2" cellspacing="2" class="standard-innerTable">

<tbody><tr>
			<th class="tableTitle" colspan="3"><p>Custom Invoice</p></th>
		</tr>
		@foreach ($invoice_custominvoice as $each_invoice_custominvoice)
			<tr>
				<th>
						<fieldset title="{{ $each_invoice_custominvoice['title'] }}">
						<?php
						$invoiceCustomInvoiceObj = new CustomInvoice();			
						$invoiceCustomInvoiceObj->CustomInvoice($each_invoice_custominvoice["custom_invoice_id"]);			
						if ($invoiceCustomInvoiceObj->getNumber("id") > 0) {
							//if (Functions::string_strpos($url_base, "/".SITEMGR_ALIAS."") !== false) {
								?>
								<!--<a href="$url_base?>/custominvoices/view.php?id=<$each_custominvoice["custom_invoice_id"]?>"></a>-->
									{{ Functions::system_showTruncatedText($each_invoice_custominvoice["title"], 60) }}
							
							<?php
							}
						else {
							?>
					{{ Functions::system_showTruncatedText($each_invoice_custominvoice["title"], 60) }}							
							<?php
						}
						?>
					</fieldset>
				</th>
			
				<td  class="infoLevel">{{ Functions::format_date($each_invoice_custominvoice["date"]) }}</td>
				<td  class="infoAmount">
					{{ $each_invoice_custominvoice["subtotal"]." (".$invoice_n["currency"].")" }}
				</td>
			</tr>
		
			@endforeach
			</tbody>
</table>
<br/>

<?php } ?>