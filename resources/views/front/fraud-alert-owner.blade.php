@extends('front')

@section('content')

<style>
.fraudAlertOwner h2{
	font-weight:600;
	font-size:42px;
	line-height: 0.5;
}
.fraudAlertOwner a, .fraudAlertOwner a:visited {
    text-decoration: underline;
}
.content-custom h2, .content-custom p {
    margin-bottom: 10px;
}
.fraudAlertOwner p {
    font-size: 18px !important;
    line-height: 1.5 !important;
    font-weight: 300 !important;
	margin:20px 0 0 !important;
}
.abtShoreSummertestimonial{
	margin-top:40px !important;
}

</style>

	
	<div class="fraudAlertOwner container">

        
		<div class="content content-full margin-top">

			<div class="content-main">
		
				<div class="content-custom"><h1><strong><br>OWNER FRAUD ALERT</strong></h1>
<h2><span style="font-size: x-large;"><strong><span style="color: #ff6600;"><br>Protecting Your Information</span></strong></span></h2>
<p><span style="color: #000000;"><strong><span style="text-decoration: underline;"><br>Thank you from an owner</span></strong></span><br><span style="color: #000000;">Maria, We would like to thank you for the warning of not accepting checks for more than rental rates and then being asked to send money order back to the potential tenants. We had received a check for $8,500.00. I took it to the bank and indicated my concern. After 5 days the bank has to assume that the check is good and they credit your account. They asked me to wait a few days before sending money order. &nbsp;Within a few days, the federal reserve responded indicating there was no such account. By your alert, we were not out $3,500.00.</span></p>
<p><span style="color: #000000;"><strong>See Scam Details Below</strong></span><br><span style="color: #000000;">We've recently been alerted to the fact that there is a scam being used to promote a "Counterfeit Cashier's Check Scam" to property owners, not on our site but others. For your own protection, please read this email in its entirety for explanations of variations of these scams and how to help protect yourself.</span><br><br><span style="color: #000000;">Please remember that all monetary transactions involve risk and if anything seems out of the ordinary, it might warrant extra scrutiny. The specific scams we have been alerted to go something like this:</span><br><br><span style="color: #000000;"><strong>Variation 1:</strong></span><br><span style="color: #000000;">1) Cashier's check is offered for deposit/rent.</span><br><span style="color: #000000;">2) Value of cashier's check exceeds the actual price - renter asks you to wire/send the balance back.</span><br><span style="color: #000000;">3) Banks will cash the fake cashier's check, release the funds to your account, AND THEN HOLD YOU RESPONSIBLE WHEN THE CHECK IS LATER DETERMINED TO BE FAKE.</span><br><br><span style="color: #000000;"><strong>Variation 2:</strong></span><br><span style="color: #000000;">1) Cashier's check is offered for deposit/rent.</span><br><span style="color: #000000;">2) Value of cashier's check is the correct amount, however the renter immediately cancels and asks you to wire/send the balance back (and keep a rebooking fee for the cancellation).</span><br><span style="color: #000000;">3) Banks will cash the fake cashier's check, release the funds to your account, AND THEN HOLD YOU RESPONSIBLE WHEN THE CHECK IS LATER DETERMINED TO BE FAKE.</span><br><br><span style="color: #000000;">Remember, counterfeit cashier's checks are usually cashed by your bank and the funds are released to you. It might take many days before the bank determines it is fake and holds you responsible for the amount. You should also never accept a cashier's check for an amount more than is owed to you. If you receive a cashier's check, please do not immediately issue any refund to a renter until you verify the check is valid. Please contact your bank for more information on confirming the validity of a cashier's check, especially if it is of international origin.</span><br><br><span style="color: #000000;">If you feel like you have been the victim of fraud on the Internet, please contact your local law enforcement. In the United States, you may also file a complaint of Internet fraud with the FBI and NW3C at <a href="http://www.ic3.gov/complaint/default.aspx"><span style="color: #000000;"><span style="color: #0000ff;">http://www.ic3.gov/default.aspx</span>.</span></a></span><br><br><span style="color: #000000;"><strong>WHO TO NOTIFY ABOUT FRAUD OR SCAM ATTEMPTS</strong>?</span></p>
<p><span style="color: #0000ff;"><a href="http://ftc.gov/multimedia/video/scam-watch/file-a-complaint.shtm"><span style="color: #0000ff;">FTC Video: How to report scams to the FTC</span></a></span></p>
<p><span style="color: #000000;">FTC toll free hotline: 877-FTC-HELP (877-382-4357)</span></p>
<p><span style="color: #0000ff;"><a href="http://www.ftc.gov/"><span style="color: #0000ff;">FTC online complaint form</span></a></span></p>
<p><span style="color: #000000;">Canadian PhoneBusters hotline: 888-495-8501</span></p>
<p><span style="color: #0000ff;"><a href="http://www.competitionbureau.gc.ca/"><span style="color: #0000ff;">Competition Bureau Canada</span></a>:</span><span style="color: #000000;"> 800-348-5358</span></p>
<p><span style="color: #0000ff;"><a href="http://www.ic3.gov/"><span style="color: #0000ff;">Internet Fraud Complaint Center</span></a></span></p></div>			
			</div>
			
					
		</div>

	</div>
						

					
@endsection