
@extends('front')

@section('content')

    <link rel="stylesheet" href="{{ asset('front/css/members.min.css') }}" />
	<link rel="stylesheet" href="{{ asset('front/css/content_custom.min.css') }}" />
	<link href="https://www.shoresummerrentals.com/custom/domain_1/theme/default/content_custom.min.css" rel="stylesheet" type="text/css" media="all">
    <!--link rel="stylesheet" href="{{ asset('front/css/bootstrap-responsive.min.css') }}" /-->
     
     
    
    
        

               
                
    
    <div class="guarantee-verify container">

        
		<div class="content content-full margin-top">

			<div class="content-main">
		
				<div class="content-custom"><div class="text">
<h1><strong><br>RENTAL GUARANTEE AND OWNERSHIP VERIFICATION</strong></h1>
<h1 style="margin-top:-25px;"><strong style=""><span style="color: #ff6600; font-size: x-large;"><br>Secure Rentals or Money Back</span></strong></h1>
<p style="text-align: justify; line-height:1.5; font-size:18px; font-weight:300;"><span style="color: #000000;"><span>If you are dissatisfied with your&nbsp;</span><strong>INITIAL&nbsp;</strong><a href="http://www.vacationrentals2u.com">Vacation Rentals 2U.com</a><span>&nbsp;or&nbsp;</span><a href="http://www.shoresummerrentals.com">Shore Summer Rentals.com</a><span>&nbsp;listing please e-</span><a href="mailto:maria@shoresummerrentals.com">mail us</a><span>&nbsp;and we will review your ad to go over it in detail to see if it has received proper exposure. If your ad has received over 10 e-mail contacts within a 3 month period OR secured at least ONE rental, a refund will</span><strong>&nbsp;NOT</strong><span>&nbsp;be issued. Your ad needs to be advertised at least 60 days to give our websites time to work for your rental property.</span></span></p>
<p style="text-align: justify; line-height:1.5; font-size:18px; font-weight:300;"><span style="color: #000000;">If it has been 60 days and you have not received 10 email contacts, <a href="mailto:maria@shoresummerrentals.com"><span style="color: #000000;">email me </span></a>and I will review your ad to make sure it was being showcased in the best possible manner. I will offer recommendations and help for the following 30 days. If, on the 90th day you still have less than 10 email contacts and have not secured at least ONE rental, we will cancel your listing and issue a prompt full refund.</span></p>
<p style="text-align: justify; line-height:1.5; font-size:18px; font-weight:300;"><span style="color: #000000;">This offer is valid for our <a href="http://www.shoresummerrentals.com/advertise.php">Gold or Diamond Main Memberships</a>&nbsp;only. &nbsp;Rental listings MUST have at least 15 appealing photos and listings that do not display the property address on the live website are not eligible for the Rental Guarantee as most renters skip those properties in fear that it may be a scam. &nbsp;All listings have the ability to upload up to 30 photos.<br><br></span><span style="color: #000000;"><strong>Refunds will be based on the following chart:<br></strong></span><span style="color: #000000;"><strong>0 - 59 Days</strong>: Trial Period - Not Eligible for Refund <br><strong><strong>60 - 90 Days</strong>: </strong>Recommendations and Help from ShoreSummerRentals.com <br><strong>90th Day - </strong>100% Refund and Cancellation of Listing (If you received less than 10 e-mail contacts). <br><strong>After 90 Days:</strong> Not Eligible for Refund<br><br></span><span style="color: #000000;"><strong>Please note, after the 91st day listed on the site (from the date the property was initially added to our site), NO refunds will be issued. </strong>Refunds are available for your first listing only. This offer is for new customers only, if you renew your listing there will be no more guarantee and no refund will be issued.<br></span></p>
<p class="prop-ownership"><strong><span style="color: #ff6600; font-size: x-large;">Property Ownership Verification<br></span></strong><span style="font-family: arial, helvetica, sans-serif; color: #000000; line-height:1.5; font-size:18px; font-weight:300;"><span style="text-align: justify;">Please note that we&nbsp;validate property ownership before any listing appears live on the websites.&nbsp; To expedite the process, please fax proof of ownership to 609-677-1590.&nbsp; Proof of ownership could be a utility bill, tax bill, settlement sheet from closing, etc.&nbsp; The property address needs to be on&nbsp;the item.&nbsp;&nbsp;<br></span><strong><span class="requiredE">PLEASE DO NOT ATTEMPT TO LIST A VACATION RENTAL&nbsp;IF YOU CANNOT PROVE PROPERTY OWNERSHIP!</span></strong></span></p>
</div></div>			
			</div>
			
					
		</div>

	</div>






		
    </script>
	<script src="{{ asset('/front/js/Chart.js') }}"></script>		
@endsection