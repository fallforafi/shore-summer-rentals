@extends('front')

@section('content')

	<section class="login-page">
		<div class="container">
			<h1>FREQUENTLY ASKED QUESTIONS</h1>
				<h3>Learn More About Listing Your Property</h3>
			
			
		<div class="accordion-area accordion-plus col-sm-12 p0">
				<h3>General</h3>
				<div class="panel-group " id="accordion">
					  
					  <div class="panel panel-default active">
						<div class="panel-heading">
						  <h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse1">
							  Q1: How can I be confident that the rental properties on Shore SummerRentals.com are legitimate?
							</a>
						  </h4>
						</div>
						<div id="collapse1" class="panel-collapse collapse in">
						  <div class="panel-body">
							We put our owners through a thorough verification process in order to prove ownership before allowing their ads to display live on our websites.  That being said, since the online service we provide for vacationers is comparable to that offered by a newspaper or magazine through which vacation homes are advertised, we still recommend that you take steps to protect yourself by requesting references and a list of past renters from the homeowners before agreeing to rent. If possible, we also suggest that you visit the property in advance.
						  </div>
						</div>
					  </div>
					  
					  <div class="panel panel-default">
						<div class="panel-heading">
						  <h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse2" class="collapsed">
							  Q2: Can you help create or modify my ad?
							</a>
						  </h4>
						</div>
						<div id="collapse2" class="panel-collapse collapse ">
						  <div class="panel-body">
							Of course,&nbsp;we are here to help!&nbsp; <a href="mailto:info@shoresummerrentals.com">E-mail</a> us for help in&nbsp;creating or modifying your ad.&nbsp; To edit your listing on your own, simply <a href="login.php">login</a> to your account and click on "Edit". Click here for&nbsp;<a href="listing-instructions.html">Creating and/or Editing&nbsp;Instructions</a>.
						  </div>
						</div>
					  </div>
					  
					  
					  <div class="panel panel-default">
						<div class="panel-heading">
						  <h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse3" class="collapsed">
							  Q3: I cannot see changes that I made to my ad.
							</a>
						  </h4>
						</div>
						<div id="collapse3" class="panel-collapse collapse ">
						  <div class="panel-body">
							All edits and changes are live and effective immediately. Since most browser pages are cached, you may need to click refresh your screen.  Your listing changes will appear for everyone else who is visiting the page for the first time. If you see the correct information in your Account Center when logged into your account, your listing has been changed.
						  </div>
						</div>
					  </div>
					  
					  
					  <div class="panel panel-default">
						<div class="panel-heading">
						  <h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse4" class="collapsed">
							  Q4: My new ad is not showing on the website.
							</a>
						  </h4>
						</div>
						<div id="collapse4" class="panel-collapse collapse ">
						  <div class="panel-body">
							All new rental listings need to be approved and activated by ShoreSummerRentals.com before it appears lives on our websites.  All edits and changes to an already approved rental listing are effective immediately.
						  </div>
						</div>
					  </div>
					  
					  
					  <div class="panel panel-default">
						<div class="panel-heading">
						  <h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse5" class="collapsed">
							  Q5: Which spot will my ad be displayed?
							</a>
						  </h4>
						</div>
						<div id="collapse5" class="panel-collapse collapse ">
						  <div class="panel-body">
							It all depends on the Main Membership that you purchase. We offer 4 Main Membership levels - <a href="advertise.php">Bronze, Silver, Gold and Diamond</a>.&nbsp; The higher the membership level you purchase, the higher your rental listing will appear in the search results.&nbsp; <a href="advertise.php">View Membership Details</a>.
						  </div>
						</div>
					  </div>
					  
					  <div class="panel panel-default">
						<div class="panel-heading">
						  <h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse6" class="collapsed">
							  Q6: What are the differences in your Main Membership levels?
							</a>
						  </h4>
						</div>
						<div id="collapse6" class="panel-collapse collapse ">
						  <div class="panel-body">
							<a href="advertise.php">View Membership Details</a>
						  </div>
						</div>
					  </div>
					  
					  <div class="panel panel-default">
						<div class="panel-heading">
						  <h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse7" class="collapsed">
							  Q7: I have more than one property in the same location, am I eligible for a discount?
							</a>
						  </h4>
						</div>
						<div id="collapse7" class="panel-collapse collapse ">
						  <div class="panel-body">
							Yes, <a href="mailto:info@shoresummerrentals.com">e-mail</a> us for pricing.
						  </div>
						</div>
					  </div>
					  
					  <div class="panel panel-default">
						<div class="panel-heading">
						  <h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse8" class="collapsed">
							  Q8: Do you offer discounts for more than one property in separate locations?
							</a>
						  </h4>
						</div>
						<div id="collapse8" class="panel-collapse collapse ">
						  <div class="panel-body">
							If you are listing multiple summer vacation rental properties at different locations, certain discounts may apply.&nbsp; Please <a href="mailto:info@shoresummerrentals.com">e-mail</a> us for more information about multiple property listing pricing.
						  </div>
						</div>
					  </div>
					  
					  
					  <div class="panel panel-default">
						<div class="panel-heading">
						  <h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse9" class="collapsed">
							  Q9: What is an Add-On and is it included in my Main Membership?
							</a>
						  </h4>
						</div>
						<div id="collapse9" class="panel-collapse collapse ">
						  <div class="panel-body">
							Add-On's&nbsp;are NOT included in ANY of our <a href="advertise.php">Main Memberships</a>. It is an optional Add-On to enhance your rental listing and increase your exposire and rental income.&nbsp; We offer <a href="advertise.php">3&nbsp;Add-On&nbsp;Packages </a>- Specials, Featured Property and&nbsp;Top Ranking.&nbsp; The Special's Add-On can be purchased online by logging into your account and clicking on Specials.&nbsp;&nbsp;You will need to <a href="mailto:info@shoresummerrentals.com">e-mail</a> us to purchase the Featured Property and/or one of the Top Ranking Add-On.&nbsp;
						  </div>
						</div>
					  </div>
					  
					  <div class="panel panel-default">
						<div class="panel-heading">
						  <h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse10" class="collapsed">
							  Q10: How do I Prove Property Owmership?
							</a>
						  </h4>
						</div>
						<div id="collapse10" class="panel-collapse collapse ">
						  <div class="panel-body">
							We will contact you to request&nbsp;documentation if we cannot verify ownership on our own.
						  </div>
						</div>
					  </div>
					  
					   <div class="panel panel-default">
						<div class="panel-heading">
						  <h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse11" class="collapsed">
							  Q11: I am Having Trouble Uploading Photos.
							</a>
						  </h4>
						</div>
						<div id="collapse11" class="panel-collapse collapse ">
						  <div class="panel-body">
							If you have followed the directions exactly and nothing is happening or the system kicks you out, your photos may be too large. Please e-mail us the photos for us to resize and them into your rental listing.
						  </div>
						</div>
					  </div>
					  
					   <div class="panel panel-default">
						<div class="panel-heading">
						  <h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse12" class="collapsed">
							  Q12: How Do I Edit My Calendar?
							</a>
						  </h4>
						</div>
						<div id="collapse12" class="panel-collapse collapse ">
						  <div class="panel-body">
							Login to your account and click on the Calendar tab. 
						  </div>
						</div>
					  </div>
					  
					

						<div class="panel panel-default">
						<div class="panel-heading">
						  <h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse13" class="collapsed">
							  Q13: Can I Switch Out Properties During the Year?
							</a>
						  </h4>
						</div>
						<div id="collapse13" class="panel-collapse collapse ">
						  <div class="panel-body">
							Each membership is property specific and cannot be swapped with other properties once they sell or book up. You will need to purchase a separate listing for each property.
						  </div>
						</div>
					  </div>
					  
					  	<div class="panel panel-default">
						<div class="panel-heading">
						  <h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse14" class="collapsed">
							  Q14: I Sold My Property, Can I Receive a Credit or Refund?
							</a>
						  </h4>
						</div>
						<div id="collapse14" class="panel-collapse collapse ">
						  <div class="panel-body">
							No. There are no refunds for properties that are sold during their current membership. All listings are sold to run the full term that is chosen by the member.&nbsp; If you sell your property and no longer wish for the listing to remain online, please <a href="mailto:info@shoresummerrentals.com">e-mail</a>&nbsp;us and we will&nbsp;remove&nbsp;the listing without refund.
						  </div>
						</div>
					  </div>
					  
					  <div class="panel panel-default">
						<div class="panel-heading">
						  <h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse15" class="collapsed">
							  Q15: Can You Renew My Listing With Last Years Credit Card Information?
							</a>
						  </h4>
						</div>
						<div id="collapse15" class="panel-collapse collapse ">
						  <div class="panel-body">
							No. For your security, we do not save credit card information on file. Your listing will NOT automatically renew. You will need to login to your account, click on Renew Listing and enter updated credit card information to renew. You can also mail a check to P.O. Box 55, Somers Point, NJ 08244 or call 877-SHORE-4U (746-7348) with your credit card number for us to manual process it for you.
						  </div>
						</div>
					  </div>
					  
					   <div class="panel panel-default">
						<div class="panel-heading">
						  <h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse16" class="collapsed">
							  Q16: Can I Reserve a Vacation Rental Property through Vacation Rentals 2U.com and ShoreSummerRentals.com?
							</a>
						  </h4>
						</div>
						<div id="collapse16" class="panel-collapse collapse ">
						  <div class="panel-body">
							No. We are strictly an advertising tool so all transactions must be coordinated through the renter and the property owner directly. <a href="advsearch">Click here</a> to search for the perfect vacation rental property.&nbsp; Once you find homes you like,&nbsp;click on the Contact Owner tab.
						  </div>
						</div>
					  </div>
					  
					  <h3>Membership Prices, Details and Payments</h3>
					  
					<div class="panel panel-default">
						<div class="panel-heading">
						  <h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse17" class="collapsed">
							  Q17: One Year Membership Term Only
							</a>
						  </h4>
						</div>
						<div id="collapse17" class="panel-collapse collapse ">
						  <div class="panel-body">
							We offer ONE year <a href="advertise.php">memberships</a> only.
						  </div>
						</div>
					  </div>
					 
					 
					 <div class="panel panel-default">
						<div class="panel-heading">
						  <h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse18" class="collapsed">
							  Q18: Are there setup fees involved to create my vacation rental property advertisement?
							</a>
						  </h4>
						</div>
						<div id="collapse18" class="panel-collapse collapse ">
						  <div class="panel-body">
							No. We are here to help! If you are not computer savvy, we will be happy to create your vacation rental property listing(s) at no additional cost.  E-mail us to request a Property Detail Form.  We will need you to fill it out in it's entirety and mail or fax it to us with payment along with proof of ownership.  Once this information is received and verified, we will have your rental listing up and running within 24 hours.  We will contact you at that point for you to review for accuracy.
						  </div>
						</div>
					  </div>					 
					 <div class="panel panel-default">
						<div class="panel-heading">
						  <h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse19" class="collapsed">
							  Q19: Are there management or referral fees involved with ShoreSummerRentals.com?
							</a>
						  </h4>
						</div>
						<div id="collapse19" class="panel-collapse collapse ">
						  <div class="panel-body">
							No. There are NO management or referral fees involved with Shore SummerRentals.com. Your only fee is the Main Membership cost and Add-On Package (optional) cost for a one-year listing of your vacation rental property. There are no hidden charges or unexpected terms.
						  </div>
						</div>
					  </div>					 
					 <div class="panel panel-default">
						<div class="panel-heading">
						  <h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse20" class="collapsed">
							  Q20: Do I have to have a credit card to sign up?
							</a>
						  </h4>
						</div>
						<div id="collapse20" class="panel-collapse collapse ">
						  <div class="panel-body">
							No. You can mail a check made payable to ShoreSummerRentals.com, P.O. Box 55, Somers Point, NJ 08244. Before mailing check, please register on the site so that I have your contact information or include your contact information with check. Once the check is received, I will create a blank ad for you to update at your convenience. If you would like us to prepare the rental listing&nbsp;for you, <a href="mailto:info@shoresummerrentals.com">e-mail</a>&nbsp;us to request a&nbsp;Property Detail Form.&nbsp; We will need you to fill it out in it's entirety and mail or fax it to us with payment along with proof of ownership.&nbsp; Once this information is received and verified, we will have your rental listing up and running within 24 hours.&nbsp;&nbsp;We will contact you at that point&nbsp;for you to review for accuracy.
						  </div>
						</div>
					  </div>					 
					 <div class="panel panel-default">
						<div class="panel-heading">
						  <h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse21" class="collapsed">
							  Q21: Main Membership Details and Pricing
							</a>
						  </h4>
						</div>
						<div id="collapse21" class="panel-collapse collapse ">
						  <div class="panel-body">
							<a href="advertise.php">Click here to view details and pricing</a>
						  </div>
						</div>
					  </div>					 
					 <div class="panel panel-default">
						<div class="panel-heading">
						  <h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse22" class="collapsed">
							  Q22: Add-On Package Details and Pricing
							</a>
						  </h4>
						</div>
						<div id="collapse22" class="panel-collapse collapse ">
						  <div class="panel-body">
							<a href="advertise.php">Click here to view details and pricing</a>
						  </div>
						</div>
					  </div>					 
					 <div class="panel panel-default">
						<div class="panel-heading">
						  <h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse23" class="collapsed">
							  Q23: Making Payments
							</a>
						  </h4>
						</div>
						<div id="collapse23" class="panel-collapse collapse ">
						  <div class="panel-body">
							By Check: please allow at least two weeks from the time it is mailed before it is processed. A notification by email is sent after each payment is processed. If you do not receive an email from us after 2 weeks, please <a href="contactus.php">contact us</a>. <br><br>
							By Credit Card: your credit card payment is usually processed immediately, but may take up to 24 hours. Upon receipt of payment, you will receive an email confirmation.&nbsp; If you did not receive an email confirmation, your payment was most likely not processed.&nbsp; To be sure,&nbsp;<a href="login.php">login</a>&nbsp;to your account to&nbsp;check the status of your listing. If original purchase, you will see a property in your account center that needs to be created with an expiration date a year from the current date. If it is a renewal, please check your listing expiration date to see if it extended another year from your last expiration date.
						  </div>
						</div>
					  </div>					 
					 <div class="panel panel-default">
						<div class="panel-heading">
						  <h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse24" class="collapsed">
							  Q24: Do You Have Auto Renewal Billing?
							</a>
						  </h4>
						</div>
						<div id="collapse24" class="panel-collapse collapse ">
						  <div class="panel-body">
							No. For your security, we do NOT save credit card information on file. A renewal needs to be initiated by you by logging into your account, clicking on Renew Listing and entering your up to date credit card information. You can also mail a check made payable to ShoreSummerRental.com and mailed to P.O. Box 55, Somers Point, NJ 08244.
						  </div>
						</div>
					  </div>					 
					 <div class="panel panel-default">
						<div class="panel-heading">
						  <h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse25" class="collapsed">
							  Q25: Rental Guarantee
							</a>
						  </h4>
						</div>
						<div id="collapse25" class="panel-collapse collapse ">
						  <div class="panel-body">
							View <a href="guarantee-and-verify.php">Rental Guarantee Details</a>.</p>
						  </div>
						</div>
					  </div>					 
					 <div class="panel panel-default">
						<div class="panel-heading">
						  <h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse26" class="collapsed">
							 Q26: My Property is Booked, I Decided Not to Rent My Property or I Sold my Property Before My Subscription Expired, Can I Get a Refund for the Time Not Used?
							</a>
						  </h4>
						</div>
						<div id="collapse26" class="panel-collapse collapse ">
						  <div class="panel-body">
							<p>No.&nbsp; Cancelling a subscription before the maximum duration does not result or entitle the property owner to a refund for any unused listing time.&nbsp; The sale of your property, securing a full time renter or property removal from rental market does not constitute a refund. All listings are sold to run the full term that is chosen by the member.</p>
							
							<p>If you sell your property and no longer wish for the listing to remain online, please <a href="mailto:info@shoresummerrentals.com">e-mail</a>&nbsp;us and we will&nbsp;remove&nbsp;the listing without refund.&nbsp; In the event you wish to remove your listing temporarily, we can inactivate the listing&nbsp;and reactivate&nbsp;it at a future date during your current subscription term.&nbsp; Your&nbsp;listing will not&nbsp;receive credit for the&nbsp;inactive time period.&nbsp; Natural disasters or sale of a home does not constitute a refund.</p>
							
						  </div>
						</div>
					  </div>					 
					 <div class="panel panel-default">
						<div class="panel-heading">
						  <h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse27" class="collapsed">
							  Q27: Payment Address
							</a>
						  </h4>
						</div>
						<div id="collapse27" class="panel-collapse collapse ">
						  <div class="panel-body">
							<p>
							Please make checks payable to:<br><br>
							ShoreSummerRentals.com<br>
							P.O. Box 55<br>
							Somers Point, NJ 08244
							</p>
						  </div>
						</div>
					  </div>	
						
					
					<h3>Security</h3>
					
						
					 <div class="panel panel-default">
						<div class="panel-heading">
						  <h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse28" class="collapsed">
							  Q28: Is the ShoreSummerRentals.com site secure?
							</a>
						  </h4>
						</div>
						<div id="collapse28" class="panel-collapse collapse ">
						  <div class="panel-body">
							Shore SummerRentals.com have employed sophisticated technology to ensure the most secure website for our advertisers and visitors. Our website was designed with your security in mind.
						  </div>
						</div>
					  </div>

					<h3>Exposure</h3>
					  
					 <div class="panel panel-default">
						<div class="panel-heading">
						  <h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse29" class="collapsed">
							  Q29: How much web traffic does ShoreSummerRentals.com get?
							</a>
						  </h4>
						</div>
						<div id="collapse29" class="panel-collapse collapse ">
						  <div class="panel-body">
							The Shore SummerRentals.com website has been a high traffic resource for the past 12 years receiving millions of hits per month.  Traffic, however, is a relative measure. What counts is the quality of traffic received on the website, not just the quantity. We pride ourselves in the volume of quality inquiries received on a regular basis, i.e., serious clients looking to rent your vacation rental property. We will strive daily to provide the same results with ShoreSummerRentals.com.
						  </div>
						</div>
					  </div>					 
					 <div class="panel panel-default">
						<div class="panel-heading">
						  <h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse30" class="collapsed">
							  Q30: How will I know if people are looking at my vacation rental listing?
							</a>
						  </h4>
						</div>
						<div id="collapse30" class="panel-collapse collapse ">
						  <div class="panel-body">
							Once your vacation rental property is in our index and published on the website, you can monitor the visits to your vacation rental property page.  Email inquiries from potential property renters are stored on your account for easy reference. We provide you with a real-time page counter that keeps track of all your hits. To view the page counter, just click on your vacation rental property details and look at the counter located at the bottom of the page.
						  </div>
						</div>
					  </div>					 
					 <div class="panel panel-default">
						<div class="panel-heading">
						  <h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse31" class="collapsed">
							  Q31: How will I receive vacation property rental inquiries?
							</a>
						  </h4>
						</div>
						<div id="collapse31" class="panel-collapse collapse ">
						  <div class="panel-body">
							Once a potential renter finds your vacation rental property, they will complete a simple inquiry form and the information will be sent to your chosen email account.   You will also receive a text message (optional) to alert you that someone emailed you about renting your home.  The quicker the owners respond, the more rentals they secure.  Additionally, your telephone number will be available for people who prefer to speak with a live person.
						  </div>
						</div>
					  </div>					 
					 <div class="panel panel-default">
						<div class="panel-heading">
						  <h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse32" class="collapsed">
							  Q32: How do you promote the ShoreSummerRentals.com website?
							</a>
						  </h4>
						</div>
						<div id="collapse32" class="panel-collapse collapse ">
						  <div class="panel-body">
							Results are key, and to that end we have gone to great lengths to make sure that your summer vacation rental property listings on the ShoreSummerRentals.com website receive maximum exposure. With extensive advertising, paid search engine placement, and a search engine optimization, we make sure that you get the most value for your listings. 
<br/><br/>
Paid Search Engine Placement 
We partner with top search engines and directories so that our website and your listings receive the most exposure possible. 
<br/><br/>
Search Engine Optimized Website
By making sure that our site is optimized for search engine indexing and is search engine friendly, we have been able to achieve phenomenal results with many of the top search engines and indexes on the Internet. We continually strive to make our site as easily accessible and well placed as possible.
						  </div>
						</div>
					  </div>					 
					 
					 <div class="panel panel-default">
						<div class="panel-heading">
						  <h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse33" class="collapsed">
							 Q33: What are the advantages of listing my vacation rental property on the ShoreSummerRentals.com website?
							</a>
						  </h4>
						</div>
						<div id="collapse33" class="panel-collapse collapse ">
						  <div class="panel-body">
							The most important advantage is value. We offer you exceptional value when you advertise your vacation rental on ShoreSummerRentals.com. We are also local and very accessible.  We have incorporated extensive techniques to make our site user-friendly for both potential renters and paid advertisers alike.
						  </div>
						</div>
					  </div>		
					  
					  <h3>Location</h3>
					  
					 <div class="panel panel-default">
						<div class="panel-heading">
						  <h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse34" class="collapsed">
							  Q34: What shore vacation towns are included for rental properties on the ShoreSummerRentals.com website?
							</a>
						  </h4>
						</div>
						<div id="collapse34" class="panel-collapse collapse ">
						  <div class="panel-body">
							ShoreSummerRentals.com now accommodates vacation rental properties in all the United States. We are currently looking for properties in all US towns.
						  </div>
						</div>
					  </div>					 
					 <div class="panel panel-default">
						<div class="panel-heading">
						  <h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse35" class="collapsed">
							  Q35: Besides Jersey Shore rental properties, what other vacation rental destinations are featured on our site?
							</a>
						  </h4>
						</div>
						<div id="collapse35" class="panel-collapse collapse ">
						  <div class="panel-body">
							ShoreSummerRentals.com also has a sister site, <a href="http://www.VacationRentals2U.com">VacationRentals2U.com&nbsp;</a>which now accommodates all vacation rental types such as the grand canyon, ski resorts, Disney, etc.&nbsp; We are currently looking for properties in all United Stated towns.
						  </div>
						</div>
					</div>
					  
					<div class="panel panel-default">
						<div class="panel-heading">
						  <h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse36" class="collapsed">
							  Q36: Does the ShoreSummerRentals.com website also promote winter vacation rental properties?
							</a>
						  </h4>
						</div>
						<div id="collapse36" class="panel-collapse collapse ">
						  <div class="panel-body">
							Yes. We can accommodate vacation rentals throughout the entire year - Spring, Summer, Fall and Winter getaways right at your fingertips. This also includes popular East Coast vacation destinations, such as Disney World in Florida all the way to the Grand Canyon. Please bookmark&nbsp;Shore SummerRentals.com or <a href="contactus.php">contact us</a> for additional information.
						  </div>
						</div>
					</div>
					
					
					<h3>Ad and Account Management</h3>
					    
					<div class="panel panel-default">
						<div class="panel-heading">
						  <h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse37" class="collapsed">
							  Q37: Listing Title Tips
							</a>
						  </h4>
						</div>
						<div id="collapse37" class="panel-collapse collapse ">
						  <div class="panel-body">
							The Listing Title is very important. It is the first things renters see and needs to grab their attention. We have a few tips and suggestions:
                            <br/><br/> 
                            Check that you use correct spelling
                            <br/><br/>
                            The use of symbols, numbers, or letters must adhere to the true meaning of the symbol.
                            <br/><br/>
                            Avoid excessive capitalization, repeated and unnecessary punctuation or symbols, and repetition Change your title often to keep your listing fresh.
						  </div>
						</div>
					</div>
					
					<div class="panel panel-default">
						<div class="panel-heading">
						  <h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse38" class="collapsed">
							  Q38: Listing Guidelines
							</a>
						  </h4>
						</div>
						<div id="collapse38" class="panel-collapse collapse ">
						  <div class="panel-body">
							<h4>We have certain guidelines that need to be adhered to when creating your ad.</h4> <br/><br/> 
							<h4>External URLs</h4>
							Shore SummerRentals.com has a dedicated area in our listings to place up to 3 external URL's that pertain to your property such as a personal website you created for your property with more photos, descriptions, etc.  Links to any of our competitor's site is strictly PROHIBITED and will be removed from your ad. You will be notified that this was done and warned that if it is placed on the ad again, your rental listing will be removed from our websites without refund. <br/><br/>
							<h4>Email Address in Body of Ad Prohibited</h4>
							Listings that contain email addresses are prime targets for email collecting spiders that look for email addresses for mailing lists that they sell to spammers. This is how SPAM is sent. If email addresses are present in our listings we will get spidered by these email collectors which will slow down the site, lower the listing views and inquires, and cause property owners to be spammed. <br/><br/>
							<h4>Each Listing is for ONE Property in ONE Location.</h4>
							Ads for multiple homes are prohibited.  ShoreSummerRentals.com listings are designed to advertise ONE PROPERTY in ONE LOCATION. You may NOT list more than one property per listing. If this is observed, you will be warned to remove the additional property advertisement, photos, etc. If you do not remove the information, ShoreSummerRentals.com has the right to deactivate your listing with no refund. <br/><br/>
							<h4>Closest Appropriate Region.</h4>
							Your listing must be placed in the closest appropriate region to your property. <br/><br/>
							<h4>Accurate Property Information</h4>
							Your listing must reflect accurate information about the property. <br/><br/>
							<h4>Plagiarizing Listings Prohibited</h4>
							Please do not copy information from another listing. <br/><br/>
							<h4>Review and Approval of Ads</h4>
							All listings must be reviewed by ShoreSummerRentals.com to ensure that they adhere to the guidelines specified in each section above. When building your listing, please double-check every field to ensure they don't include such items as, incorrect information, the characters < or >, email addresses, phone numbers or websites. These items will cause your listing to be rejected now or possibly deleted at a later time. Shore SummerRentals.com reserves the right to alter or permanently remove any listing that violates our guidelines or we feel could compromise the potential renter and/or our site's integrity without warning or notice. <br/><br/>
							
						  </div>
						</div>
					</div>
					
					<div class="panel panel-default">
						<div class="panel-heading">
						  <h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse39" class="collapsed">
							  Q39: Forgot Username or Password
							</a>
						  </h4>
						</div>
						<div id="collapse39" class="panel-collapse collapse ">
						  <div class="panel-body">
							If you forget your username or password, you may obtain it by going to the login page and clicking on the Forgot Your Password link.  Enter your email address and your password will then be mailed to your email address, assuming you have previously registered with us, and the email address you enter is the same one you registered with.
							
						  </div>
						</div>
					</div>
					
					<div class="panel panel-default">
						<div class="panel-heading">
						  <h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse40" class="collapsed">
							  Q40: Transfer a listing
							</a>
						  </h4>
						</div>
						<div id="collapse40" class="panel-collapse collapse ">
						  <div class="panel-body">
							Shore SummerRentals.com listings are transferable in the event the property was sold.   We can transfer the rental listing to the new owner's account with the permission of the original owner of the home and rental listing.  They have to be advertising the same property.  There is a $25.00 transfer fee.  <a href="contactus.php">Contact us</a> for details.  If you wish to cancel your service due to a sale or transfer, the cancellation policy above will apply. No pro-rated refunds will be offered to terminate your listing due to the sale of a property or if taken off of the rental market.
							
						  </div>
						</div>
					</div>
				
					<div class="panel panel-default">
						<div class="panel-heading">
						  <h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse41" class="collapsed">
							  Q41: Change Account Holder and Contact Owner Email Address
							</a>
						  </h4>
						</div>
						<div id="collapse41" class="panel-collapse collapse ">
						  <div class="panel-body">
							Please note that there are two locations to enter email addresses. 
                            <br/> 
							There is an account holder email address which is the email of the person in charge of the account.  This email is used to login to your account and where correspondence from management such as invoices, alerts, tips, etc.  will be sent.
							<h4>Update Account Holder Email:</h4>
							Once <a href="login.php">logged in</a> to your dashboard, click on "Account".  This is where you edit all the information for the account holder such as email, password, mailing address, phone numbers, etc.  Make sure to save all changes.
                            <br/>
							There is also an email address for the person who would like to receive the email inquiries from the renters.  The email addresses can be the same or different.
 							<h4>Update Contact Owner Email:</h4>
							Once <a href="login.php">logged in</a> to your dashboard, click on "Edit".  This is where you edit some of the property details and the email of the person you want to receive renter email inquiries.  Make sure to save all changes.
							
						  </div>
						</div>
					</div>
					
					<div class="panel panel-default">
						<div class="panel-heading">
						  <h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse42" class="collapsed">
							  Q42: Modify/Delete Listings
							</a>
						  </h4>
						</div>
						<div id="collapse42" class="panel-collapse collapse ">
						  <div class="panel-body">
							You have full access to your listings 24 hours a day. You can modify any portion of your listing at any time, including the pictures. Great flexibility and you control all of it.  View <a href="listing-instructions.php">editing instructions</a>.
							
							
						  </div>
						</div>
					</div>
					
					<div class="panel panel-default">
						<div class="panel-heading">
						  <h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse43" class="collapsed">
							  Q43: Having Trouble Registering on Website
							</a>
						  </h4>
						</div>
						<div id="collapse43" class="panel-collapse collapse ">
						  <div class="panel-body">
							If you attempt to register on the site and receive a message saying "the current user is already in the database".  This means you are already registered with us with that same email address.  If this is the case, please <a href="contactus.php">Contact us</a> to request your login information.
							
						  </div>
						</div>
					</div>
					
					<div class="panel panel-default">
						<div class="panel-heading">
						  <h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse44" class="collapsed">
							  Q44: What Web Browsers are Supported by Your Websites?
							</a>
						  </h4>
						</div>
						<div id="collapse44" class="panel-collapse collapse ">
						  <div class="panel-body">
							At ShoreSummerRentals, we strive to provide our customers with a modern, fast, safe and secure web browsing experience.
							<br/><br/>
							<h4>In order to provide you with a great user experience, we officially support the following browsers:</h4>
							* Internet Explorer 10 or higher <br/>
							* Firefox 32 or higher<br/>
							* Chrome 38 or higher<br/>
							* Safari 7 or higher<br/>
							<h4>On mobile devices, we officially support the following:</h4>
							* iOS Safari 7 or higher (iPhone and iPad)<br/>
                            * Android Chrome 38 or higher (Android Phones and Tablets)<br/><br/>
							While our sites may continue to function on other devices and in other browsers than those listed, occasionally you may see a warning if you are using a browser or device where ShoreSummerRentals has known issues.  While we do our best to support browsers that are most commonly in use, it is not possible to support all, and therefore we recommend upgrading to one of the above mentioned browsers.<br/><br/>
							To view instructions on how to upgrade your internet browser, visit <a href="http://browsehappy.com/">http://browsehappy.com/</a>
							
						  </div>
						</div>
					</div>
					
					<div class="panel panel-default">
						<div class="panel-heading">
						  <h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse45" class="collapsed">
							  Q45: Change Password
							</a>
						  </h4>
						</div>
						<div id="collapse45" class="panel-collapse collapse ">
						  <div class="panel-body">
						    Once <a href="login.php">logged in</a> to your dashboard, click on "Account".  This is where you can change your password as well as the main Account Holder's information.  Make sure to save all changes. 
							
						  </div>
						</div>
					</div>
					
					<div class="panel panel-default">
						<div class="panel-heading">
						  <h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse46" class="collapsed">
							  Q46: Property Address Visible on Ad
							</a>
						  </h4>
						</div>
						<div id="collapse46" class="panel-collapse collapse ">
						  <div class="panel-body">
						    Although we highly recommend displaying the address of your rental property, please <a href="contactus.php">Contact us</a> if you do not wish to have it displayed on your ad on the live website.  It is more likely for a renter to be interested (and less suspicious of a scam) if they know the location.  Most renters will skip properties that do not display addresses.  We do require the address for our records.  Listings that do not display the address are not eligble for the <a href="rental-guarantee.php">Rental Guarantee</a>.
							
						  </div>
						</div>
					</div>
					
					<div class="panel panel-default">
						<div class="panel-heading">
						  <h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse47" class="collapsed">
							  Q47: How Can I Protect my Privacy
							</a>
						  </h4>
						</div>
						<div id="collapse47" class="panel-collapse collapse ">
						  <div class="panel-body">
						   
							
						  </div>
						</div>
					</div>
					
					<div class="panel panel-default">
						<div class="panel-heading">
						  <h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse48" class="collapsed">
							  Q48: Do I Need to Know HTML or How to Design my Summer Vacation Rental Page?
							</a>
						  </h4>
						</div>
						<div id="collapse48" class="panel-collapse collapse ">
						  <div class="panel-body">
						    Nope. When you join ShoreSummerRentals.com, you will complete a simple online form that walks you though the process of creating your rental listing.  You will be able to enter long detailed descriptions, amenities, photos, rates and availability for your rental home.  If you need help, we are here for you! <a href="listing-instructions.php">Creating and/or Editing Instructions</a>.
							
						  </div>
						</div>
					</div>
					
					<div class="panel panel-default">
						<div class="panel-heading">
						  <h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse49" class="collapsed">
							  Q49: Do I Need to Create 2 Listings for my home to appear on ShoreSummerRentals.com and VacationRentals2U.com?
							</a>
						  </h4>
						</div>
						<div id="collapse49" class="panel-collapse collapse ">
						  <div class="panel-body">
						    Nope.  Once you have added your vacation rental property to the ShoreSummerRentals.com website, your information will duplicate on the <a href="http://www.VacationRentals2U.com">VacationRentals2U.com</a> website.  No need to enter your property information twice, even though you will get twice the exposure by being on two websites.
							
						  </div>
						</div>
					</div>
					
					<div class="panel panel-default">
						<div class="panel-heading">
						  <h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse50" class="collapsed">
							  Q50: Who Updates my Listings?
							</a>
						  </h4>
						</div>
						<div id="collapse50" class="panel-collapse collapse ">
						  <div class="panel-body">
						    You do! We provide you with a secure Web-based administration tool which allows you to update the details of your vacation rental property such as availability, rates, description, title, amenities, photographs, rental policies and terms. We make sure that your vacation rental listing page is immediately updated on both the VacationRentals2U.com and ShoreSummerRentals.com sites.
							
						  </div>
						</div>
					</div>
					
					<div class="panel panel-default">
						<div class="panel-heading">
						  <h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse51" class="collapsed">
							  Q51: May I provide links to other web pages external to my ad?
							</a>
						  </h4>
						</div>
						<div id="collapse51" class="panel-collapse collapse ">
						  <div class="panel-body">
						    In general, the answer is yes; however there are rules concerning which links are acceptable and which links are not.  We have designated areas for up to 3 external links that you will see when entering your property details.  External links are not permitted to be displayed in the description field or any other area except where designated.  We do ask for the link to point to another web page that contains additional information about that property. If you have multiple listings with us, you may link your Cape May vacation rental, for example, to your Wildwood vacation rental page, and vice versa. We reserve the right to remove links to websites that directly compete with VacationRentals2U.com and ShoreSummerRentals.com.
							
						  </div>
						</div>
					</div>
					
					<h3>Google Maps</h3>
					 
					<div class="panel panel-default">
						<div class="panel-heading">
						  <h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse52" class="collapsed">
							  Q52: Google Maps
							</a>
						  </h4>
						</div>
						<div id="collapse52" class="panel-collapse collapse ">
						  <div class="panel-body">
						    ShoreSummerRentals.com now offers the Google map feature for visitors to see the exact location of your property.  By entering your property address, the map feature will automatically activate showing the location of your rental home.  Please note that in order for the Google map feature to work, you need have your street address ONLY in the address field.  For example, you cannot have "123 Summer Street (near 4th Street)" in the address field.  It does seem to work if you have "123 Summer Street, 2nd Floor" though.  If you still notice that the map is showing incorrectly, go to the property location field when logged in and tab out of the field.  This fine tunes the map.
							
						  </div>
						</div>
					</div> 
					
					<div class="panel panel-default">
						<div class="panel-heading">
						  <h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse53" class="collapsed">
							  Q53: Map is Not Showing My Location Accurately
							</a>
						  </h4>
						</div>
						<div id="collapse53" class="panel-collapse collapse ">
						  <div class="panel-body">
						    If you are not happy with the way Google Maps is displaying the location of your property, please follow the directions above in FAQ - Q52, check out <a href="https://support.google.com/maps/?hl=en#topic=">Google Maps Support</a> or <a href="contactus.php">Contact us</a>.
							
						  </div>
						</div>
					</div> 
					
					<div class="panel panel-default">
						<div class="panel-heading">
						  <h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse54" class="collapsed">
							  Q54: Can I Disable the Map?
							</a>
						  </h4>
						</div>
						<div id="collapse54" class="panel-collapse collapse ">
						  <div class="panel-body">
						    No, you can not disable the map.
							
						  </div>
						</div>
					</div> 
					 
					<h3>Photos</h3> 
					
					
					<div class="panel panel-default">
						<div class="panel-heading">
						  <h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse55" class="collapsed">
							  Q55: How do I add and change photographs to my vacation rental listing?
							</a>
						  </h4>
						</div>
						<div id="collapse55" class="panel-collapse collapse ">
						  <div class="panel-body">
						    You are permitted to upload up to 32 appealing photos of your home. Once you upload the photo, click on “Caption” to give the photo a name.  Click the SUBMIT button at the very bottom of this Listing Description page to save the photos.  Simply Clicking the SUBMIT button when uploading, without clicking it once again on the Listing Description page, will NOT save them.  Images must be under 10MB in size.  If they are larger, please resize before uploading.  * Please note that we are aware that the photos are not displaying in the order uploaded and are working on a solution.
							<br/><br/>
							<h4>There are several ways to get your photos posted on ShoreSummerRentals.com.</h4> 
							<br/><br/>
							1. Upload them yourself. 
                               <br/><br/> 
							2. E-mail them. Simply attach your photos and e-mail them to us at photos@shoresummerrentals.com. Be sure to indicate your name and vacation rental number so we know where to post your photos. 
                               <br/><br/> 
							3. Send Us a Link. If your photos are located on another website or your vacation rental website simply e-mail us at photos@shoresummerrentals.com the website URL for each photo. If the site allows us to copy the photos, we will. 
                               <br/><br/>
							4. Snail-Mail a Disk. Let us do all the work for you. Mail your disk of photos to the address below. We will upload and add them to your vacation rental listing for you. 
                               <br/><br/>
							Vacations Rentals 2U.com / ShoreSummerRentals.com P.O. Box 55 Somers Point, NJ 08244
							
							
						  </div>
						</div>
					</div> 
					
					<div class="panel panel-default">
						<div class="panel-heading">
						  <h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse56" class="collapsed">
							  Q56: Additional Photo Tips
							</a>
						  </h4>
						</div>
						<div id="collapse56" class="panel-collapse collapse ">
						  <div class="panel-body">
						    Pictures say a thousand words. I cannot stress enough how important photos are since most renters will not see your property until the day that they pull up to at the start of their vacation. Make sure your photos are very bright and you post all the rooms of the home as well as the surrounding areas. People love outdoor shots such as ocean views, the beach, the boardwalk, nightlife, etc. More pictures will put you ahead of all the competition and secure you significantly more rental income than owners who only post a couple dark interior photos.
							
							
						  </div>
						</div>
					</div> 
					
					<div class="panel panel-default">
						<div class="panel-heading">
						  <h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse57" class="collapsed">
							  Q57: 
							</a>
						  </h4>
						</div>
						<div id="collapse57" class="panel-collapse collapse ">
						  <div class="panel-body">
						     
							
							
						  </div>
						</div>
					</div> 
					
					<div class="panel panel-default">
						<div class="panel-heading">
						  <h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse58" class="collapsed">
							  Q58: Can I Copy Photos from Another Website?
							</a>
						  </h4>
						</div>
						<div id="collapse58" class="panel-collapse collapse ">
						  <div class="panel-body">
						     Many other sites copyright their photos once you add them to your listing and thus they cannot be legally uploaded to our site without permission. You must obtain permission from the other site before uploading your photos to your listing.
							
							
						  </div>
						</div>
					</div> 
					
					<div class="panel panel-default">
						<div class="panel-heading">
						  <h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse59" class="collapsed">
							  Q59: Do You Charge to Update Photos or Make Changes to My Property Listings?
							</a>
						  </h4>
						</div>
						<div id="collapse59" class="panel-collapse collapse ">
						  <div class="panel-body">
						     No, we do not charge for updates to your listings. As a paid member, you are entitled to log into your account and make changes to your ad as you see fit. We provide you with the necessary online tools to easily update your vacation rental property listings.
							
							
						  </div>
						</div>
					</div> 
					
					<div class="panel panel-default">
						<div class="panel-heading">
						  <h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse60" class="collapsed">
							  Q60: My Text and Photos Were Copied
							</a>
						  </h4>
						</div>
						<div id="collapse60" class="panel-collapse collapse ">
						  <div class="panel-body">
						     Since we do not copyright your listing text or photos, legally there isn't anything we can do but remove the content from our site. We recommend you ask the owner to remove any copied material in a non-threatening manner. If they refuse, we will co-operate as required by any legal proceeding. If you believe that your ad has been plagiarized, please <a href="contactus.php">Contact us</a>.
							
							
						  </div>
						</div>
					</div> 
					
					<div class="panel panel-default">
						<div class="panel-heading">
						  <h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse61" class="collapsed">
							  Q61:
							</a>
						  </h4>
						</div>
						<div id="collapse61" class="panel-collapse collapse ">
						  <div class="panel-body">
						     
							
							
						  </div>
						</div>
					</div> 
					
					<h3>Calendar</h3> 
					
					<div class="panel panel-default">
						<div class="panel-heading">
						  <h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse62" class="collapsed">
							  Q62: Do You Offer a Rental Property Availability Calendar?
							</a>
						  </h4>
						</div>
						<div id="collapse62" class="panel-collapse collapse ">
						  <div class="panel-body">
						     Yes, there is an availability calendar in which you simply select the dates your vacation rental property is unavailable. Renters can now search by property availability so please keep the calendar up to date. It's easy to use and is provided at no extra charge.
							
							
						  </div>
						</div>
					</div> 
					
					<div class="panel panel-default">
						<div class="panel-heading">
						  <h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse63" class="collapsed">
							  Q63: Where Do I Update My Calendar?
							</a>
						  </h4>
						</div>
						<div id="collapse63" class="panel-collapse collapse ">
						  <div class="panel-body">
						     <a href="login.php">Login to your account</a>. Click on the Calendar link in your dashboard.  Enter the arrival and departure dates of the reserved rental.  Enter the name of the renter or any character will suffice - this will not appear on the public site. Click Submit.
							
							
						  </div>
						</div>
					</div>

					<div class="panel panel-default">
						<div class="panel-heading">
						  <h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse64" class="collapsed">
							  Q64: Do I Have to Update My Calendar?
							</a>
						  </h4>
						</div>
						<div id="collapse64" class="panel-collapse collapse ">
						  <div class="panel-body">
						     While keeping a calendar up to date is not required, our research has shown that most vacationers skip over properties that show all availability open. It gives the renter a sense of urgency when they see others are interested in your property and that they should act quickly. If you still do not want to use the calendar feature, you can leave it blank.
							
							
						  </div>
						</div>
					</div> 
					
					<div class="panel panel-default">
						<div class="panel-heading">
						  <h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse65" class="collapsed">
							  Q65: Can I Link Another Calendar to My Ad?
							</a>
						  </h4>
						</div>
						<div id="collapse65" class="panel-collapse collapse ">
						  <div class="panel-body">
						     No. Our properties can be searched by availability according to OUR calendar.
							
							
						  </div>
						</div>
					</div> 
					
					<h3>Rental Property Rates</h3>

                    <div class="panel panel-default">
						<div class="panel-heading">
						  <h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse66" class="collapsed">
							  Q66: Where Do I Post My Rates?
							</a>
						  </h4>
						</div>
						<div id="collapse66" class="panel-collapse collapse ">
						  <div class="panel-body">
						     <a href="login.php">Login to your account</a>. Click on the Rate link in your dashboard. Fill in the information regarding check-in, check-out, payment terms and any additional notes. Click the Save button at the bottom of the page before entering rates to save what you entered so far. Enter your rates. Click the Save button. To edit rates already entered, place your curser in the field you want to update, make the change, hit Enter.
							
							
						  </div>
						</div>
					</div>

                    <div class="panel panel-default">
						<div class="panel-heading">
						  <h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse67" class="collapsed">
							 Q67: Do I Have to Post Rates?
							</a>
						  </h4>
						</div>
						<div id="collapse67" class="panel-collapse collapse ">
						  <div class="panel-body">
						    New listings must have rates posted in order to qualify for the <a href="rental-guarantee.php">Rental Guarantee</a>.  While posting rates is not required once the Rental Guarantee period has passed, our research has shown that most vacationers skip over properties that do not have rates posted. <strong>Most properties are searched by rates so if your rates are not posted in the actual rate table, your ad will not show up when someone searches by rates.</strong>  Knowing all of this and you still do not want to use the rate feature, you can leave it blank.
							
							
						  </div>
						</div>
					</div>     					
					
					<h3>Rental Coordination and Forms</h3>
					
					<div class="panel panel-default">
						<div class="panel-heading">
						  <h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse68" class="collapsed">
							 Q68: Who is responsible for writing the rental lease agreement?
							</a>
						  </h4>
						</div>
						<div id="collapse68" class="panel-collapse collapse ">
						  <div class="panel-body">
						    All vacation rental property leases must be coordinated by the property owner.  We supply sample leases and welcome letters, a rental procedure checklist, and an inventory checklist form to registered members.  <a href="contactus.php">Contact us</a> to request any of those items.  
							
							
						  </div>
						</div>
					</div>

                    <div class="panel panel-default">
						<div class="panel-heading">
						  <h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse69" class="collapsed">
							 Q69: Who determines the price I charge for my vacation rental property?
							</a>
						  </h4>
						</div>
						<div id="collapse69" class="panel-collapse collapse ">
						  <div class="panel-body">
						    You determine the price to charge for your vacation rental property. You may update your pricing structure at any time by logging into your account and clicking on Edit Rates.  
							
							
						  </div>
						</div>
					</div>

                     <div class="panel panel-default">
						<div class="panel-heading">
						  <h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse70" class="collapsed">
							 Q70: How Do I Get the Renter the Key?
							</a>
						  </h4>
						</div>
						<div id="collapse70" class="panel-collapse collapse ">
						  <div class="panel-body">
						    You can either mail the keys to the renter a week before their scheduled vacation with a letter explaining what is expected during their stay and upon their departure along with directions to the property. You can include a padded self addressed envelope for them to drop in the mail after they leave. Another option would be to install a combination door lock so no key is needed at all, just a code. This way all you have to do is e-mail them a letter a week before their arrival with instructions, direvtions and codes. That is what I do and it works out great. I purchased a Schlage model at Lowe's.  
							
							
						  </div>
						</div>
					</div>  

 					<div class="panel panel-default">
						<div class="panel-heading">
						  <h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse71" class="collapsed">
							 Q71: 
							</a>
						  </h4>
						</div>
						<div id="collapse71" class="panel-collapse collapse ">
						  <div class="panel-body">
						     
							
							
						  </div>
						</div>
					</div> 
					
					<h3>Cancellation Policy</h3>
					
					<div class="panel panel-default">
						<div class="panel-heading">
						  <h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse72" class="collapsed">
							 Q72: May I cancel my vacation rental listing at any time?
							</a>
						  </h4>
						</div>
						<div id="collapse72" class="panel-collapse collapse ">
						  <div class="panel-body">
						    Yes, but no refunds will be given for just changing your mind. We do offer a <a href="rental-guarantee.php">Rental Guarantee</a> which would allow a full refund within 90 days of the listing activation if the listing meets the criteria of our Rental Guarantee program.  This is only for new owners who have not yet renewed a current listing on our site. This is only for the very first property an owner lists with us and they are not happy with the results they receive. Your ad will be removed and guarantees are only granted to one property once. If you sell your property, you are not eligible for a refund or credit for any unused time. 
							
							
						  </div>
						</div>
					</div> 
					
					<div class="panel panel-default">
						<div class="panel-heading">
						  <h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse73" class="collapsed">
							 Q73: Can I delete my advertisement if I am no longer interested in advertising?
							</a>
						  </h4>
						</div>
						<div id="collapse73" class="panel-collapse collapse ">
						  <div class="panel-body">
						    Yes.  Please <a href="contactus.php">Contact us</a> and we will delete your listing.  Please note that once we delete it, we can no longer pull the information back if you decide to advertise later down the road.  The listing will have to be recreated from scratch.  If you only want to pause it until the following year, we can deactivate it so it does not show active on the websites without actually deleting the property from your account. This way when you are ready to advertise again, all you have to do is login to your account and click on Renew Listing.  Payment will be required to reactivate the listing. No credit is given for unused time. 
							
							
						  </div>
						</div>
					</div> 
					
					
					<h3>Miscellaneous Questions</h3>
					
					 <div class="panel panel-default">
						<div class="panel-heading">
						  <h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse74" class="collapsed">
							 Q74: Can I find a listing using the listing number?
							</a>
						  </h4>
						</div>
						<div id="collapse74" class="panel-collapse collapse ">
						  <div class="panel-body">
						    To find a listing, enter the Rental ID number in the search box on the homepage of VacationRentals2U.com and ShoreSummerRentals.com and click on Search or Go. 
							
							
						  </div>
						</div>
					</div> 
					
					 <div class="panel panel-default">
						<div class="panel-heading">
						  <h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse75" class="collapsed">
							 Q75: Is There a Direct Link to My Listing?
							</a>
						  </h4>
						</div>
						<div id="collapse75" class="panel-collapse collapse ">
						  <div class="panel-body">
						    There are several ways to access and even share your listing. 
                            <br/><br/>   
							1. There is a direct link to each property you list.<br/><br/>
							The URL for your listing will be:
							http://www.shoresummerrentals.com/listing/______.html (enter rental id in blank area before ".html")<br/><br/>

							2. Click on Email a Friend from your listing.<br/><br/>

							3. Click on Share of Facebook from your listing.<br/><br/>

							4. Click on Share on Twitter from your listing. 
							
							
						  </div>
						</div>
					</div> 
					
					<div class="panel panel-default">
						<div class="panel-heading">
						  <h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse76" class="collapsed">
							 Q76: My City is Not Listed on Your Website.
							</a>
						  </h4>
						</div>
						<div id="collapse76" class="panel-collapse collapse ">
						  <div class="panel-body">
						    You can list your vacation rental property in any city or state in the United States even if you do not see any other properties in that town yet. The city will dynamically create by you typing in the city name in the city box when creating your ad.
							
							
						  </div>
						</div>
					</div> 
					
					<div class="panel panel-default">
						<div class="panel-heading">
						  <h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse77" class="collapsed">
							 Q77: Featured Properties.
							</a>
						  </h4>
						</div>
						<div id="collapse77" class="panel-collapse collapse ">
						  <div class="panel-body">
						    This is an Add-On to our Main Memberships.  Click here for <a href="advertise.php">Pricing and Details</a>.  
							
							
						  </div>
						</div>
					</div> 
					
					<div class="panel panel-default">
						<div class="panel-heading">
						  <h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse78" class="collapsed">
							 Q78: Cash Back Referral Program
							</a>
						  </h4>
						</div>
						<div id="collapse78" class="panel-collapse collapse ">
						  <div class="panel-body">
						    Word of mouth is key to our success and very important to us. We now offer a very generous cash back <a href="cash-back-referral.php">Referral Program</a>.    
							
							
						  </div>
						</div>
					</div> 
					
					<div class="panel panel-default">
						<div class="panel-heading">
						  <h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse79" class="collapsed">
							 Q79: Can I Restore an Expired Listing?
							</a>
						  </h4>
						</div>
						<div id="collapse79" class="panel-collapse collapse ">
						  <div class="panel-body">
						    Yes.  As long as you did not ask us to delete it entirely from our database.  All listings are saved in our database and can be reactivated by making a payment online. Just login to your account and click on Renew Property. The system will walk you through the payment process and upon payment acceptance, your ad will automatically reactivate showing up on the site instantly. Please make sure you update current descriptions, photos, rates and availability.    
							
							
						  </div>
						</div>
					</div>

                    <div class="panel panel-default">
						<div class="panel-heading">
						  <h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse80" class="collapsed">
							 Q80: Can I Restore a Deleted Listing?
							</a>
						  </h4>
						</div>
						<div id="collapse80" class="panel-collapse collapse ">
						  <div class="panel-body">
						    No, that is why it is very important that you understand that once deleted, everything is gone - photos, descriptions, rates - EVERYTHING.  That is why we always highly recommend just asking us to pause it unless you have sold.  Noone knows what the future may hold.      
							
							
						  </div>
						</div>
					</div>

                     <div class="panel panel-default">
						<div class="panel-heading">
						  <h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse81" class="collapsed">
							 Q81: Renters: Bounced Email
							</a>
						  </h4>
						</div>
						<div id="collapse81" class="panel-collapse collapse ">
						  <div class="panel-body">
						    If a message is returned to you undeliverable or an owner is not responding to inquiries, please <a href="contactus.php">Contact us</a>.     
							
							
						  </div>
						</div>
					</div> 

                    <div class="panel panel-default">
						<div class="panel-heading">
						  <h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse82" class="collapsed">
							 Q82: Owners: Bounced Email
							</a>
						  </h4>
						</div>
						<div id="collapse82" class="panel-collapse collapse ">
						  <div class="panel-body">
						    If a message is returned to you undelivered, we cannot provide you with the correct address to contact the person who sent the inquiry. Form-based inquiries ask the sender to enter an email address. There is no way to verify the validity of this address before sending the inquiry. It is up to the sender to ensure the email address provided exists and is typed in correctly. Sometimes it is possible to guess the address by checking for common misspellings (ex. Yahoo.con instead of Yahoo.com).
                            <br/><br/>
							We cannot locate the correct address for undeliverable email address provided by prospective renters.
                            <br/><br/>    
							Send yourself a test email just entering "test" in the message box and an email address other than the one you registered with in the "From" box and click submit. Make sure the address you put in the From box is not the same address you used to register with and is a real address. If you do not receive the test email, check your inquiry box on VacationRentals2U.com and ShoreSummerRentals.com to see if it is there. If it is, make sure you have not blocked email from VacationRentals2U.com or ShoreSummerRentals.com via your spam controls (AOL users can enter keyword "SPAM CONTROLS" to check this). 
                            <br/><br/>
							If you have checked all the above and still did not receive your test email, please <a href="contactus.php">Contact us</a>.    
							
							
						  </div>
						</div>
					</div> 

                    <div class="panel panel-default">
						<div class="panel-heading">
						  <h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse83" class="collapsed">
							 Q83: Someone copied my listing text
							</a>
						  </h4>
						</div>
						<div id="collapse83" class="panel-collapse collapse ">
						  <div class="panel-body">
						    If you believe that your work has been plagiarized, please <a href="contactus.php">Contact us</a>.    
							
							
						  </div>
						</div>
					</div>  					

                    <h3>Spam, Scams, Complaints and Abuse</h3>
					
					<div class="panel panel-default">
						<div class="panel-heading">
						  <h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse84" class="collapsed">
							 Q84: Spam Emails
							</a>
						  </h4>
						</div>
						<div id="collapse84" class="panel-collapse collapse ">
						  <div class="panel-body">
						    Unsolicited commercial e-mail (SPAM) is prohibited from being sent through our system. That is why it is very important not to add your email in your descriptions where it is visible to be spidered and used to bombard you with SPAM.  For further information about internet fraud and fake check scams, see <a href="http://www.fraud.org">fraud.org</a>.    
							
							
						  </div>
						</div>
					</div> 
					
					<div class="panel panel-default">
						<div class="panel-heading">
						  <h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse85" class="collapsed">
							 Q85: Suspicious Email
							</a>
						  </h4>
						</div>
						<div id="collapse85" class="panel-collapse collapse ">
						  <div class="panel-body">
						    Usually emails with poor grammar, spelling, odd use of capital letters and/or asking you to wire money or participate in an unusual bank transaction is a scam. The sender is usually attempting to obtain money, personal information or your email address. We recommend that you let us know you received this through our service and delete the message without responding. For further information about internet fraud and fake check scams, see <a href="http://www.fraud.org">fraud.org</a>.    
							
							
						  </div>
						</div>
					</div> 
					
					<div class="panel panel-default">
						<div class="panel-heading">
						  <h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse86" class="collapsed">
							 Q86: Renter Overpayment Scams
							</a>
						  </h4>
						</div>
						<div id="collapse86" class="panel-collapse collapse ">
						  <div class="panel-body">
						    Scammers will send you an inquiry, which may seem legitimate, but at some point suggest they send you more than your rental fees and ask you wire back the difference. The check they send you will be a fake, but usually takes the bank several weeks to catch it. By then, you have wired the difference back and will be unable to recover what you have sent. In some instances, the bad check will even clear your bank initially. We suggest you insist on exact payment, however, if you do accept an overpayment, wait at least 30 days before returning any funds. If you receive a suspicious inquiry mentioning that a third party (a business, assistant, or someone who owes them money) is going to send an overpayment, we recommend that you delete the inquiry without responding. See fraud.org for more information.    
							
							
						  </div>
						</div>
					</div> 
					
					<div class="panel panel-default">
						<div class="panel-heading">
						  <h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse87" class="collapsed">
							 Q87: File a Complaint about a Property Owner.
							</a>
						  </h4>
						</div>
						<div id="collapse87" class="panel-collapse collapse ">
						  <div class="panel-body">
						    VacationRentals2U.com and ShoreSummerRentals.com reserves the right to remove or not remove a listing based on the legitimacy and severity of the complaint. If you have a complaint about a property on VacationRentals2U.com and ShoreSummerRentals.com, please <a href="contactus.php">Contact us</a>.   
							
							
						  </div>
						</div>
					</div> 
					
					<div class="panel panel-default">
						<div class="panel-heading">
						  <h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse88" class="collapsed">
							 Q88: Protection against Abuse/Fraud.
							</a>
						  </h4>
						</div>
						<div id="collapse88" class="panel-collapse collapse ">
						  <div class="panel-body">
						   While abuse and fraud is not common in the vacation rentals industry, we recommend you take certain steps to protect yourself and increase the chance that your transaction goes smoothly. 
                           <br/><br/> 
							Some vacation rental sites offer free trials or free listings, but these increase the chance of abuse and fraud because the owner is not verified by way of a financial transaction, listings are often copied from other sites and have errors and are out of date. It?s safer to use VacationRentals2U.com and ShoreSummerRentals.com as we devote significant resources to verify the legitimacy of our owners and their listings. We can also assure you that we investigate suspicious listings and remove listings for serious or multiple complaints. 
                            <br/><br/>
							We recommend that you request references, ask for a rental contract, and research the owner and property before your stay. You should also never wire money to owners via Western Union, Money gram or other instant wire transfer services. If you do not feel comfortable working with a certain owner, move on to a different rental. 
							
							
						  </div>
						</div>
					</div>
					
					<div class="panel panel-default">
						<div class="panel-heading">
						  <h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse89" class="collapsed">
							 Q89: Report Abuse/Fraud
							</a>
						  </h4>
						</div>
						<div id="collapse89" class="panel-collapse collapse ">
						  <div class="panel-body">
						    If you are the victim of Fraud/Abuse from a listing found on ShoreSummerRentals.com, please <a href="contactus.php">Contact us</a> with all the details.
							
							
						  </div>
						</div>
					</div>
					
					
					
					 
				</div>
			</div>
		
		
		</div>
	</section>
  
@endsection