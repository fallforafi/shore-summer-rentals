<?php
use App\Functions\Functions;
use App\Functions\Contact;

			$block_bannerbyimpression = false;
			$block_custominvoice = false;

			if (isset($bill_info["badges"])) foreach ($bill_info["badges"] as $id => $info) {
				$badges_ids[] = $id;
				$badges_amounts[] = $info["total_fee"];
			}

			if (isset($bill_info["listings"])) foreach ($bill_info["listings"] as $id => $info) {
				$listing_ids[] = $id;
				$listing_amounts[] = $info["total_fee"];
			}

			if (isset($bill_info["custominvoices"])) foreach($bill_info["custominvoices"] as $id => $info) {
				$block_custominvoice = true;
				$custominvoice_ids[] = $id;
				$custominvoice_amounts[] = $info["amount"];
			}

			$stoppayment = false;


			if (!$stoppayment) {


				$acc_id=Functions::sess_getAccountIdFromSession();
				$contactObj = new Contact();
				$contactObj->Contact($acc_id);

				$amount = str_replace(",", ".", $bill_info["total_bill"]);
				if (isset($listing_ids)) $listing_ids = implode("::",$listing_ids);
				if (isset($listing_amounts)) $listing_amounts = implode("::",$listing_amounts);
				if (isset($badges_ids)) $badges_ids = implode("::",$badges_ids);
				if (isset($badges_amounts)) $badges_amounts = implode("::",$badges_amounts);
				//if ($event_ids) $event_ids = implode("::",$event_ids);
				//if ($event_amounts) $event_amounts = implode("::",$event_amounts);
				//if ($banner_ids) $banner_ids = implode("::",$banner_ids);
				//if ($banner_amounts) $banner_amounts = implode("::",$banner_amounts);
				//if ($classified_ids) $classified_ids = implode("::",$classified_ids);
				//if ($classified_amounts) $classified_amounts = implode("::",$classified_amounts);
				//if ($article_ids) $article_ids = implode("::",$article_ids);
				//if ($article_amounts) $article_amounts = implode("::",$article_amounts);
				if (isset($custominvoice_ids)) $custominvoice_ids = implode("::",$custominvoice_ids);
				if (isset($custominvoice_amounts)) $custominvoice_amounts = implode("::",$custominvoice_amounts);
				$authorize_account_id = Functions::sess_getAccountIdFromSession();
				$authorize_x_first_name = $contactObj->getString("first_name");
				$authorize_x_last_name = $contactObj->getString("last_name");
				$authorize_x_company = $contactObj->getString("company");
				$authorize_x_address = $contactObj->getString("address");
				$authorize_x_city = $contactObj->getString("city");
				$authorize_x_state = $contactObj->getString("state");
				$authorize_x_zip = $contactObj->getString("zip");
				$authorize_x_country = $contactObj->getString("country");
				$authorize_x_phone = $contactObj->getString("phone");
				$authorize_x_email = $contactObj->getString("email");


			}


?>
				
	<script language="javascript" type="text/javascript">
					<!--
					function submitOrder() {
						document.getElementById("authorizebutton").disabled = true;
						document.authorizeform.submit();
					}
					//-->
				</script>

				<form name="authorizeform" target="_self" action="<?php echo url('payment_method/authorize'); ?>" method="post">

					<div style="display: none;">
						
						<?php
							Functions::setting_get("payment_tax_status", $payment_tax_status);
							Functions::setting_get("payment_tax_value", $payment_tax_value);

							$subtotal_amount = $amount;
							if ($payment_tax_status == "on") {
								$tax_amount = Functions::payment_calculateTax($subtotal_amount, $payment_tax_value, true, false);
								$amount = Functions::payment_calculateTax($subtotal_amount, $payment_tax_value);
							} else {
								$tax_amount = 0;
								$payment_tax_value = 0;
							}
						?>
						<input type="hidden" name="_token" value="{{ csrf_token() }}">

						<input type="hidden" name="pay" value="1" />
						<input type="hidden" name="x_tax_amount" value="{{ $payment_tax_value }}" />
						<input type="hidden" name="x_subtotal_amount" value="{{ $subtotal_amount }}" />
						<input type="hidden" name="x_amount" value="{{ $amount }}" />
						<input type="hidden" name="x_invoice_num" value="{{ uniqid(0) }}" />
						<input type="hidden" name="x_cust_id" value="{{ $authorize_account_id }}" />
						<input type="hidden" name="x_listing_ids" value="{{ (isset($listing_ids)
						?$listing_ids:'') }}" />
					<input type="hidden" name="x_listing_amounts" value="{{ (isset($listing_amounts)
						?$listing_amounts:'') }}" />
						<input type="hidden" name="x_badges_ids" value="{{ (isset($badges_ids)
						?$badges_ids:'') }}" />
						<input type="hidden" name="x_badges_amounts" value="{{ (isset($badges_amounts)
						?$badges_amounts:'') }}" />
				<input type="hidden" name="x_custominvoice_ids" value="{{ (isset($custominvoice_ids)
						?$custominvoice_ids:'') }}" />
				<input type="hidden" name="x_custominvoice_amounts" value="{{ (isset($custominvoice_amounts)?$custominvoice_amounts:'') }}" />
						<input type="hidden" name="x_domain_id" value="1" />

					</div>

					<table align="center" width="95%" cellpadding="2" cellspacing="2" class="standard-table payment-authorize">
						<tbody><tr>
							<th colspan="2" class="standard-tabletitle">Billing Info</th>
						</tr>
						<tr>
							<th>* Card Number:</th>
							<td><input type="text" name="x_card_num" value="4111111111111111"></td>
						</tr>
						<tr>
							<th>* Card Expire date:</th>
							<td><input type="text" name="x_exp_date" value="12/2018"><span>mm/yyyy</span></td>
						</tr>
						<tr>
							<th>Card Code:</th>
							<td><input type="text" name="x_card_code" value="123"></td>
						</tr>
						<tr>
							<th colspan="2" class="standard-tabletitle">Customer Info
						</th></tr>
						<tr>
							<th>First Name:</th>
					<td><input type="text" name="x_first_name" value="{{ $authorize_x_first_name }}"></td>
						</tr>
						<tr>
							<th>Last Name:</th>
							<td><input type="text" name="x_last_name" value="{{ $authorize_x_last_name }}"></td>
						</tr>
						<tr>
							<th>Company:</th>
							<td><input type="text" name="x_company" value="{{ $authorize_x_company }}"></td>
						</tr>
						<tr>
							<th>Address:</th>
							<td><input type="text" name="x_address" value="{{ $authorize_x_address }}"></td>
						</tr>
						<tr>
							<th>City:</th>
							<td><input type="text" name="x_city" value="{{ $authorize_x_city }}"></td>
						</tr>
						<tr>
							<th>State:</th>
							<td><input type="text" name="x_state" value="{{ $authorize_x_state }}"></td>
						</tr>
						<tr>
							<th>Zip:</th>
							<td><input type="text" name="x_zip" value="{{ $authorize_x_zip }}"></td>
						</tr>
						<tr>
							<th>Country:</th>
							<td><input type="text" name="x_country" value="{{ $authorize_x_country }}"></td>
						</tr>
						<tr>
							<th>Phone:</th>
							<td><input type="tel" name="x_phone" value="{{ $authorize_x_phone }}"></td>
						</tr>
						<tr>
							<th>E-mail:</th>
							<td><input type="email" name="x_email" value="{{ $authorize_x_email }}"></td>
						</tr>
					</tbody></table>


	<?php 
		if ($payment_process == "signup") {?>
                        

                        <div class="blockcontinue cont_100">
                    
                    <div class="cont_70 empty"></div>
                    
                    <div class="cont_30 ">
                        
                        <p class="bt-highlight checkoutButton biggerbutton">
                       <button type="button" id="authorizebutton" onclick="submitOrder();">Place Order and Continue</button>

                        </p>
                        
                    </div>
                </div>
                    <?php    
                    } else { ?>
							<p class="standardButton paymentButton">
							<button type="button" id="authorizebutton" onclick="submitOrder();">Pay By CreditCard</button>
						</p>
					<?php } ?>
					
										
						<br>
						<div style="text-align: center; margin-top: 60px;">
            				<script type="text/javascript" src="https://seal.thawte.com/getthawteseal?host_name=www.shoresummerrentals.com&amp;size=S&amp;lang=en"></script>
            			</div>
</form>