@extends('popup')

@section('content')


	
<div class="modal-content modal-content-upload">

                        <h2>
                    <b>Add Image</b>
                    <span>
                        <a href="javascript:void(0);" onclick="parent.$.fancybox.close();">Close</a>
                    </span>
                </h2>

   <form id="uploadimage" name="uploadimage" action="<?php echo url('sponsors/popupstoreImage'); ?>" method="post" class="frmEmail" enctype="multipart/form-data">

                        <input type="hidden" name="pop_type" value="uploadimage">
                        
                       <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="item_type" value="listing">
                        <input type="hidden" name="main" value="">
                        <input type="hidden" name="level" value="<?php echo $level; ?>">
                        <input type="hidden" name="temp" value="">
                        <input type="hidden" name="gallery_item_id" id="gallery_item_id" value="<?php echo $item_id; ?>">
                        <input type="hidden" name="gallery_id" id="gallery_id" value="<?php echo $gallery_id; ?>">
                        <input type="hidden" name="image_id" id="image_id" value="">
                        <input type="hidden" name="thumb_id" id="thumb_id" value="">
                        <input type="hidden" name="item_id" id="item_id" value="<?php echo $item_id; ?>">
                        <input type="hidden" name="captions" id="captions" value="">
                        <input type="hidden" name="x1" value="0" id="x1">
                        <input type="hidden" name="y1" value="0" id="y1">
                        <input type="hidden" name="x2" value="" id="x2">
                        <input type="hidden" name="y2" value="" id="y2">
                        <input type="hidden" name="w" value="" id="w">
                        <input type="hidden" name="h" value="" id="h">
                        <input type="hidden" name="gallery_hash" value="<?php echo $gallery_hash; ?>">
                        <input type="hidden" name="domain_id" value="<?php echo $domain_id; ?>">
                        <input type="hidden" name="order" value="">
                        
	<script type="text/javascript">
		function sendFile () {
			setValue('yes');
			$('#uploadimage').submit();
			$('#loadingBar').css('display', '');
			$('#imgFile').css('display', 'none');
			$('#submitButton').css('display', 'none');
		}

		function setValue(op){
			if (document.getElementById("uploadThumbID"))
				document.getElementById("uploadThumbID").value=op;
		}

		function noUpload(){
			window.history.back();
		}
		
		
	</script>

		
	<table border="0" cellpadding="0" cellspacing="0" class="standardForm">
					<tbody><tr>
				<input type="hidden" id="uploadThumbID" name="uploadThumb" value="yes">
				<td class="TimageFile" width="140" height="30"><span>Image File</span>:</td>
				<td class="IimageFile">
					<input type="file" id="imgFile" name="image" size="30" onchange="sendFile();" class="inputExplode">
					<div id="loadingBar" align="center" style="display: none;">
						<img src="<?php echo url().'/images/img_loading_bar.gif'; ?>">
					</div>
				</td>
			</tr>
					</tbody></table>
	
	<table border="0" cellpadding="0" cellspacing="0" class="standardForm">
		<tbody><tr>
			<td height="24" colspan="2" class="formButton">
				<div id="overlay"></div>
				<p class="input-button-form">
					<button type="button" onclick="noUpload();" value="Upload" id="UploadImage">Cancel</button>
				</p>
			</td>
		</tr>
	</tbody></table>                    </form>


	 <p style="color:red;margin-left:150px;margin-top:5px;">1.Click on Browse</p>
                        <p style="color:red;margin-left:150px;margin-top:5px;">2.Locate the photo and double click to upload</p>
                        <p style="color:red;margin-left:150px;margin-top:5px;">3.Click on Submit</p>
                        <p style="color:red;margin-left:150px;margin-top:5px;">4.Add Caption</p>
                        <p style="color:red;margin-left:150px;margin-top:5px;">5.Click Submit button</p>
                        <p style="color:red;margin-left:150px;margin-top:5px;">6.Important: YOU MUST CLICK ON THE SUBMIT BUTTON AT THE BOTTOM OF THE LISTING INFORMATION PAGE TO SAVE ALL PHOTOS</p>
                        <p style="color:red;margin-left:150px;margin-top:5px;">* Please note that the images will display in the order uploaded.</p>

                        
                   <script>
                        $(document).ready(function() {
                            $('#uploadimage').submit(function() {
                                var index = $(parent.document).find('.ui-sortable li').length + 1;
                                $('#uploadimage').find('[name="order"]').val(index);
                            });
                        });
                    </script>
                                                <center>
                                <!--<img style="width: 70%;" src="https://www.shoresummerrentals.com/custom/domain_1/images/upload_images_instruct.jpg">  -->                          </center>
<!--                        <p style="color:red;margin-left:150px;margin-top:5px;">--><!--</p>-->
<!--                        <p style="color:red;margin-left:150px;margin-top:5px;">--><!--</p>-->
<!--                        <p style="color:red;margin-left:150px;margin-top:5px;">--><!--</p>-->
<!--                        <p style="color:red;margin-left:150px;margin-top:5px;">--><!--</p>-->
<!--                        <p style="color:red;margin-left:150px;margin-top:5px;">--><!--</p>-->
<!--                        <p style="color:red;margin-left:150px;margin-top:5px;">--><!--</p>-->
<!--                        <p style="color:red;margin-left:150px;margin-top:5px;">--><!--</p>-->
                                    
                </div>
	
	<style>
	.modal-content>h2 {
    margin-bottom: 5px;
}
</style>
	
    
 
 

<!--<div id="window-resizer-tooltip" style="display: none;"><a href="#" title="Edit settings"></a><span class="tooltipTitle">Window size: </span><span class="tooltipWidth" id="winWidth">1280</span> x <span class="tooltipHeight" id="winHeight">984</span><br><span class="tooltipTitle">Viewport size: </span><span class="tooltipWidth" id="vpWidth">1280</span> x <span class="tooltipHeight" id="vpHeight">548</span></div></body>-->
               @endsection