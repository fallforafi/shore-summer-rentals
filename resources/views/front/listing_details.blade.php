@extends('front')

@section('content')

	<link href="http://www.shoresummerrentals.com/custom/domain_1/theme/default/detail.min.css" rel="stylesheet" type="text/css" media="all" />
	<link rel="stylesheet" href="{{ asset('front/datepicker/jquery-ui.css') }}" />
	<script src="{{ asset('/front/datepicker/jquery-ui.js') }}"></script>

<style>

.ui-state-disabled, .ui-widget-content .ui-state-disabled, .ui-widget-header .ui-state-disabled{
	opacity:1;
}
.available span {
    background: #e8e8ea!important;
}
</style>
<?php
use App\Functions\Functions;

$currency= Config::get('params.currency');
?>

<script type="text/javascript">
	
    $('.hdr-top').each(function() {
  $(this).addClass('hdr-top-listing');
});

</script>


	<section class="inner-page listing-instructions-page listing-details-container">
		<div class="container">
			
			<div class="listing__lft listing__lft__one col-sm-9">
			<div class="affix" data-spy="affix" data-offset-top="300">
				<h3><?php echo $listingData[0]['title'].', '.$listingData[0]['address'].', '.$listingData[0]['bedroom'].' Bedrooms, '.$listingData[0]['bathroom'].' Bathrooms, '.$listingData[0]['sleeps'].' Sleeps '; ?></h3>

				<address itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress">
					<strong>Property Address:</strong>
						<span itemprop="streetAddress"><?php echo $listingData[0]['address'].' '.$listingData[0]['address2']; ?></span>
							<span><?php echo  $listingData[0]['location_4_title'].', '.$listingData[0]['location_3_title'].', '.$listingData[0]['zip_code']; ?></span>
							<meta itemprop="addressCountry" content="<?php echo $listingData[0]['location_1_abbreviation']; ?>">
							<meta itemprop="addressRegion" content="<?php echo $listingData[0]['location_3_abbreviation']; ?>">
                            <meta itemprop="addressLocality" content="<?php echo $listingData[0]['location_4_title']; ?>">
                            <meta itemprop="postalCode" content="<?php echo $listingData[0]['zip_code']; ?>">
							| <strong>Rental ID:</strong> #<?php echo $listingData[0]['rental_id']; ?>                        
                </address>
				
				<ul class="nav nav-pills nav-detail scrolink abt-property-list">
                        <li><a class="overviewBtnsLinks" href="#overview">Overview</a></li>
						<li><a class="overviewBtnsLinks" href="#specials">Specials</a></li>
                        <li><a class="overviewBtnsLinks" href="#location">Map</a></li>
                        <li><a class="overviewBtnsLinks" href="#availability">View Calendar</a></li>
                        <li><a class="overviewBtnsLinks" href="#rates">Rates</a></li>
                        <li><a class="overviewBtnsLinks" href="#photos">Photos</a></li>
              <li><a  rel="nofollow" href="<?php echo url('listing_emailform/'.$listingData[0]['rental_id']); ?>" class="fancy_window_tofriend btn btn-block">Email Owner</a>
                        <!--<a data-toggle="modal" data-target="#emailModal" class="bg-orange emailonwer-btn">-->

				        </li>
                </ul>
				</div>
				
				
				<div class="clearfix"></div>
				
				
				<div class="listing__gallery-area listing__properties-gallery thumb-indicat text-center">
				
				
						<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
						  

						  <!-- Wrapper for slides -->
						  <div class="carousel-inner" data-toggle="modal" data-target="#myModal">
							
							<?php if(!empty($listingData[0]['image'])){
                                foreach($listingData[0]['image'] as $key=>$val)
                                { ?>
                                    <div class="item <?php if($key=='0'){ echo "active";}  ?>">
                                        <img src="<?php echo config('params.imagesPath').$val->prefix.'photo_'.$val->id.".".strtolower($val->type); ?>" alt="...">
                                    </div>
                                <?php }
                                } ?>
						  </div>

						  <!-- Controls -->
						  <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
							<span class="glyphicon glyphicon-chevron-left fa fa-angle-left black"></span>
						  </a>
						  <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
							<span class="glyphicon glyphicon-chevron-right fa fa-angle-right black"></span>
						  </a>
						  <!-- Indicators -->
						    <span class="carousel-indicators indicators-arr prev">&#10094;</span>
							<span class="carousel-indicators indicators-arr next">&#10095;</span>
						  <ol class="carousel-indicators">
                          
                                <?php 
                               
                                if(!empty($listingData[0]['image'])){
                                foreach($listingData[0]['image'] as $key=>$val)
                                { ?>
                                    <li data-target="#carousel-example-generic" data-slide-to="<?php echo $key; ?>" class="active">
                                         <img src="<?php echo config('params.imagesPath').$val->prefix.'photo_'.$val->id.".".strtolower($val->type);?>" alt="...">
                                    </li>
									
                                <?php }
                                } ?>
							
							 
						  </ol>
						  
						</div>


				</div>
				<!-- Button trigger modal -->

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog preview__modalDialog" role="document">
    <div class="modal-content">
		<span class="cross-mark__gallery" data-dismiss="modal">&#10005</span>
      <div class="modal-body">
        <div class="listing__gallery-area listing__properties-gallery thumb-indicat text-center">
				
				
						<div id="preview-carousel-example-generic" class="preview__carousel carousel slide" data-ride="carousel">
						  

						  <!-- Wrapper for slides -->
						  <div class="carousel-inner">
							
							<?php if(!empty($listingData[0]['image'])){
                                foreach($listingData[0]['image'] as $key=>$val)
                                { ?>
                                    <div class="item preview__item <?php if($key=='0'){ echo "active";}  ?>">
                                        <img src="<?php echo config('params.imagesPath').$val->prefix.'photo_'.$val->id.".".strtolower($val->type); ?>" alt="...">
                                    </div>
                                <?php }
                                } ?>
						  </div>

						  <!-- Controls -->
						  <a class="left carousel-control" href="#preview-carousel-example-generic" data-slide="prev">
							<span class="carousel-indicators indicators-arr prev">&#10094;</span>
						  </a>
						  <a class="right carousel-control" href="#preview-carousel-example-generic" data-slide="next">
							<span class="carousel-indicators indicators-arr next">&#10095;</span>
						  </a>
						  
						  <ol class="carousel-indicators">
                          
                               
							
							 
						  </ol>
						  
						</div>


				</div>
      </div>
    </div>
  </div>
</div>
<script>
var pixels=0;
jQuery(function() {



var thumbmover = ".thumb-indicat .carousel-indicators li:nth-of-type(1)";

var thumbCount = $( ".thumb-indicat .carousel-indicators li" ).size();
 if (thumbCount<8){
  jQuery(".indicators-arr").addClass("hidden");
 }

  jQuery('.thumb-indicat .next').click(function () {
    pixels=pixels-80;      
 jQuery( thumbmover ).css('margin-left',pixels);
  //alert(pixels);
  if (pixels==-320){ pixels=0; }
 });
  
  jQuery('.thumb-indicat .prev').click(function () {
    pixels=pixels+80;      
 jQuery( thumbmover ).css('margin-left',pixels);
 //alert(pixels);
 
  if (pixels>=0){ pixels=0; }
  
 });
});
</script>	

<script>

$(document).on('click', '.overviewBtnsLinks', function(event){
    event.preventDefault();

    $('html, body').animate({
        scrollTop: $( $.attr(this, 'href') ).offset().top
    }, 500);
});

</script>	
<script>

$(document).ready(function(){
        $("h1, h2, p").removeClass("hdr-top");
});

</script>	
				
				
				
			<div id="overview" class="geninfo-area">
						<h3>General Information 
								<span class="general-icons pull-right small ">
									<a href="http://www.weather.com/weather/today/<?php echo $listingData[0]['zip_code']; ?>" target="_blank">
									
                                    <img src="http://www.shoresummerrentals.com/images/weather.png" alt=""> Weather </a>
                                </span>
								<?php if(isset($listingData[0]['feature_wheelchairaccess']) && $listingData[0]['feature_wheelchairaccess']=='y')
                                { ?>
                                    <span class="general-icons pull-right small mr10">
                                        <img src="{{ config('params.iconPath').'wheelchair.png' }}" alt=""/>
                                        Wheelchair Access
                                    </span>
                               <?php  } ?>
                              
                                
                                </h3>

								<address class="bg-silver p10" itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress">
									<strong>Property Address:</strong>
									<span itemprop="streetAddress"><?php echo $listingData[0]['address'].' '.$listingData[0]['address2']; ?></span>
									<span><?php echo  $listingData[0]['location_4_title'].', '.$listingData[0]['location_3_title'].', '.$listingData[0]['zip_code']; ?></span>
								</address>

						<div class="rooms-dtl col-sm-12 list-col-3 mb30">
						  <ul class="amenities">
							<li><strong>Rental ID: </strong> #<?php echo  $listingData[0]['rental_id']; ?></li>
							<li><strong>Bedrooms: </strong><?php echo  $listingData[0]['bedroom']; ?></li>
							<li><strong>Sleeps: </strong><?php echo  $listingData[0]['sleeps']; ?></li>
							<li><strong>Pool: </strong><?php echo  ($listingData[0]['feature_pool'] && $listingData[0]['feature_pool']=='n')?'No':'Yes'; ?> </li>
							<li><strong>Bathrooms: </strong><?php echo  $listingData[0]['bathroom']; ?></li>
							<li><strong>Type: </strong><?php echo   ucfirst(str_replace('_', ' ', $listingData[0]['property_type'])); ?></li>
							<li><strong>Pets: </strong><?php echo  ($listingData[0]['feature_petsallowed'] && $listingData[0]['feature_petsallowed']=='n')?'No':'Yes'; ?> </li>
							<li><strong>Linens: </strong><?php echo  ($listingData[0]['amenity_linensprovided'] && $listingData[0]['amenity_linensprovided']=='n')?'No':'Yes'; ?> </li>
							<li><strong>Outside Shower: </strong> <?php echo  ($listingData[0]['amenity_outsideshower'] && $listingData[0]['amenity_outsideshower']=='n')?'No':'Yes'; ?></li>
							<li><strong>Internet: </strong> <?php echo  ($listingData[0]['amenity_internet'] && $listingData[0]['amenity_internet']=='n')?'No':'Yes'; ?></li>
							<li><strong>Boat Slip: </strong> <?php echo  ($listingData[0]['feature_boatslip'] && $listingData[0]['feature_boatslip']=='n')?'No':'Yes'; ?></li>
							<li><strong>Elevator: </strong> <?php echo  ($listingData[0]['feature_elevator'] && $listingData[0]['feature_elevator']=='n')?'No':'Yes'; ?></li>
							<li><strong>Parking Spaces: </strong> <?php echo  ($listingData[0]['feature_coveredparking'] && $listingData[0]['feature_coveredparking']=='n')?'No':'Yes'; ?></li>
							<li><strong>Grill: </strong> <?php echo  ($listingData[0]['amenity_grill'] && $listingData[0]['amenity_grill']=='n')?'No':'Yes'; ?>No</li>
							<li><strong>Wheelchair Access: </strong> <?php echo  ($listingData[0]['feature_wheelchairaccess'] && $listingData[0]['feature_wheelchairaccess']=='n')?'No':'Yes'; ?></li>
							<li><strong>Distance to Beach: </strong><?php echo  $listingData[0]['distance_beach']; ?></li>
						  </ul>
						</div>
  
  

<?php if($listingData[0]['description'] && strlen(trim($listingData[0]['description']))>0 ) {?>
  <h4>Property Overview</h4>
  <p><?php echo  $listingData[0]['description']; ?></p>
<?php } ?>
  
  <h4>Amenities</h4>
  <div class="bg-silver geninfo-area-listingTwo col-sm-12 p10 list-col-3">
    <h6><strong>Amenities:</strong></h6>
    <ul class="amenities">
        <li><?php echo  ($listingData[0]['amenity_alarmclock'] && $listingData[0]['amenity_alarmclock']=='n')?'':'Alarm Clock';  ?> </li>
        <li><?php echo  ($listingData[0]['amenity_airhockey'] && $listingData[0]['amenity_airhockey']=='n')?'':'Air Hocky';  ?> </li>
        <li><?php echo  ($listingData[0]['amenity_answeringmachine'] && $listingData[0]['amenity_answeringmachine']=='n')?'':'Answering Machine';  ?> </li>
        <li><?php echo  ($listingData[0]['amenity_arcadegames'] && $listingData[0]['amenity_arcadegames']=='n')?'':'Arcade Games';  ?> </li>
        <li><?php echo  ($listingData[0]['amenity_blender'] && $listingData[0]['amenity_blender']=='n')?'':'Blender';  ?> </li>
        <li><?php echo  ($listingData[0]['amenity_billiards'] && $listingData[0]['amenity_billiards']=='n')?'':'Billiards';  ?> </li>
        <li><?php echo  ($listingData[0]['amenity_blurayplayer'] && $listingData[0]['amenity_blurayplayer']=='n')?'':'Bluray Player';  ?> </li>
        <li><?php echo  ($listingData[0]['amenity_books'] && $listingData[0]['amenity_books']=='n')?'':'Books';  ?> </li>
        <li><?php echo  ($listingData[0]['amenity_ceilingfan'] && $listingData[0]['amenity_ceilingfan']=='n')?'':'Ceiling Fan';  ?> </li>
        <li><?php echo  ($listingData[0]['amenity_coffeemaker'] && $listingData[0]['amenity_coffeemaker']=='n')?'':'Coffee Maker';  ?> </li>
        <li><?php echo  ($listingData[0]['amenity_cookingrange'] && $listingData[0]['amenity_cookingrange']=='n')?'':'Cooking Range';  ?> </li>
        <li><?php echo  ($listingData[0]['amenity_cookware'] && $listingData[0]['amenity_cookware']=='n')?'':'Cookware';  ?> </li>
        <li><?php echo  ($listingData[0]['amenity_cdplayer'] && $listingData[0]['amenity_cdplayer']=='n')?'':'CD Player';  ?> </li>
        <li><?php echo  ($listingData[0]['amenity_childshighchair'] && $listingData[0]['amenity_childshighchair']=='n')?'':'Childs highchair';  ?> </li>
        <li><?php echo  ($listingData[0]['amenity_computer'] && $listingData[0]['amenity_computer']=='n')?'':'Computer';  ?> </li>
        
        <li><?php echo  ($listingData[0]['amenity_deckfurniture'] && $listingData[0]['amenity_deckfurniture']=='n')?'':'Deck Furniture';  ?> </li>
        <li><?php echo  ($listingData[0]['amenity_dishwasher'] && $listingData[0]['amenity_dishwasher']=='n')?'':'Dish Washer';  ?> </li>
        <li><?php echo  ($listingData[0]['amenity_dvdplayer'] && $listingData[0]['amenity_dvdplayer']=='n')?'':'Dvd Player';  ?> </li>
        <li><?php echo  ($listingData[0]['amenity_dishes'] && $listingData[0]['amenity_dishes']=='n')?'':'Dishes';  ?> </li>
        <li><?php echo  ($listingData[0]['amenity_exercisefacilities'] && $listingData[0]['amenity_exercisefacilities']=='n')?'':'Exercise Facilities';  ?> </li>
        <li><?php echo  ($listingData[0]['amenity_foosball'] && $listingData[0]['amenity_foosball']=='n')?'':'Foosball';  ?> </li>
        <li><?php echo  ($listingData[0]['amenity_gametable'] && $listingData[0]['amenity_gametable']=='n')?'':'Gametable';  ?> </li>
        <li><?php echo  ($listingData[0]['amenity_games'] && $listingData[0]['amenity_games']=='n')?'':'Games';  ?> </li>
        <li><?php echo  ($listingData[0]['amenity_grill'] && $listingData[0]['amenity_grill']=='n')?'':'Grill';  ?> </li>
        <li><?php echo  ($listingData[0]['amenity_hairdryer'] && $listingData[0]['amenity_hairdryer']=='n')?'':'Hair dryer';  ?> </li>
        <li><?php echo  ($listingData[0]['amenity_icemaker'] && $listingData[0]['amenity_icemaker']=='n')?'':'Icemaker';  ?> </li>
        <li><?php echo  ($listingData[0]['amenity_internet'] && $listingData[0]['amenity_internet']=='n')?'':'Internet';  ?> </li>
        <li><?php echo  ($listingData[0]['amenity_ironandboard'] && $listingData[0]['amenity_ironandboard']=='n')?'':'Iron and board';  ?> </li>
        <li><?php echo  ($listingData[0]['amenity_kidsgames'] && $listingData[0]['amenity_kidsgames']=='n')?'':'Kids Games';  ?> </li>
        <li><?php echo  ($listingData[0]['amenity_linensprovided'] && $listingData[0]['amenity_linensprovided']=='n')?'':'Linens Provided';  ?> </li>
        <li><?php echo  ($listingData[0]['amenity_lobsterpot'] && $listingData[0]['amenity_lobsterpot']=='n')?'':'Lobster pot';  ?> </li>
        <li><?php echo  ($listingData[0]['amenity_minirefrigerator'] && $listingData[0]['amenity_minirefrigerator']=='n')?'':'Mini Refrigerator';  ?> </li>
        <li><?php echo  ($listingData[0]['amenity_mp3radiodock'] && $listingData[0]['amenity_mp3radiodock']=='n')?'':'Mp3 Radio Dock';  ?> </li>
        <li><?php echo  ($listingData[0]['amenity_pinball'] && $listingData[0]['amenity_pinball']=='n')?'':'Pinball';  ?> </li>
        <li><?php echo  ($listingData[0]['amenity_pingpong'] && $listingData[0]['amenity_pingpong']=='n')?'':'Pingpong';  ?> </li>
        <li><?php echo  ($listingData[0]['amenity_privatepool'] && $listingData[0]['amenity_privatepool']=='n')?'':'Private Pool';  ?> </li>
        <li><?php echo  ($listingData[0]['amenity_radio'] && $listingData[0]['amenity_radio']=='n')?'':'Radio';  ?> </li>
        <li><?php echo  ($listingData[0]['amenity_sofabed'] && $listingData[0]['amenity_sofabed']=='n')?'':'Sofabed';  ?> </li>
        <li><?php echo  ($listingData[0]['amenity_stereo'] && $listingData[0]['amenity_stereo']=='n')?'':'Stereo';  ?> </li>
        <li><?php echo  ($listingData[0]['amenity_toaster'] && $listingData[0]['amenity_toaster']=='n')?'':'Toaster';  ?> </li>
        
        
        <li><?php echo  ($listingData[0]['amenity_toasteroven'] && $listingData[0]['amenity_toasteroven']=='n')?'':'Toaster Oven';  ?> </li>
        <li><?php echo  ($listingData[0]['amenity_towelsprovided'] && $listingData[0]['amenity_towelsprovided']=='n')?'':'Towels Provided';  ?> </li>
        <li><?php echo  ($listingData[0]['amenity_toys'] && $listingData[0]['amenity_toys']=='n')?'':'Toys';  ?> </li>
        <li><?php echo  ($listingData[0]['amenity_vacuum'] && $listingData[0]['amenity_vacuum']=='n')?'':'Vacuum';  ?> </li>
        <li><?php echo  ($listingData[0]['amenity_videogameconsole'] && $listingData[0]['amenity_videogameconsole']=='n')?'':'Video Game Console';  ?> </li>
        <li><?php echo  ($listingData[0]['amenity_videogames'] && $listingData[0]['amenity_videogames']=='n')?'':'Video Games';  ?> </li>
        
    </ul>
  </div>
  
  <div class="clearfix"></div>
  
  <div class="bg-white geninfo-bedding col-sm-12 p10 list-col-3">
    <h6><strong>Bedding Sizes:</strong> 1 Queen Bed, 2 Full Beds, 1 Twin Bed, 1 Sofa Bed</h6>
  </div>
    <!--<h4>Features</h4> -->
    <div class="bg-silver geninfo-area-listingTwo col-sm-12 p10 list-col-3">
        <h6><strong>Features:</strong></h6>
        <ul class="amenities">
            <li><?php echo  ($listingData[0]['feature_airconditioning'] && $listingData[0]['feature_airconditioning']=='n')?'':'Air Conditioning';  ?> </li>
            <li><?php echo  ($listingData[0]['feature_barbecuegas'] && $listingData[0]['feature_barbecuegas']=='n')?'':'Barbecue gas';  ?> </li>
            <li><?php echo  ($listingData[0]['feature_balcony'] && $listingData[0]['feature_balcony']=='n')?'':'Balcony';  ?> </li>
            <li><?php echo  ($listingData[0]['feature_boatslip'] && $listingData[0]['feature_boatslip']=='n')?'':'Boatslip';  ?> </li>
            <li><?php echo  ($listingData[0]['feature_barbecuecharcoal'] && $listingData[0]['feature_barbecuecharcoal']=='n')?'':'Barbecue Charcoal';  ?> </li>
            <li><?php echo  ($listingData[0]['feature_clubhouse'] && $listingData[0]['feature_clubhouse']=='n')?'':'Clubhouse';  ?> </li>
            <li><?php echo  ($listingData[0]['feature_coveredparking'] && $listingData[0]['feature_coveredparking']=='n')?'':'Covered Parking';  ?> </li>
            <li><?php echo  ($listingData[0]['feature_diningroom'] && $listingData[0]['feature_diningroom']=='n')?'':'Dining Room';  ?> </li>
            <li><?php echo  ($listingData[0]['feature_elevator'] && $listingData[0]['feature_elevator']=='n')?'':'Elevator';  ?> </li>
            <li><?php echo  ($listingData[0]['feature_familyroom'] && $listingData[0]['feature_familyroom']=='n')?'':'Family Room';  ?> </li>
            <li><?php echo  ($listingData[0]['feature_gasfireplace'] && $listingData[0]['feature_gasfireplace']=='n')?'':'Gas Fireplaces';  ?> </li>
            <li><?php echo  ($listingData[0]['feature_gatedcommunity'] && $listingData[0]['feature_gatedcommunity']=='n')?'':'Gated community';  ?> </li>
            <li><?php echo  ($listingData[0]['feature_heated'] && $listingData[0]['feature_heated']=='n')?'':'Heated';  ?> </li>
            <li><?php echo  ($listingData[0]['feature_heatedpool'] && $listingData[0]['feature_heatedpool']=='n')?'':'Heated Pool';  ?> </li>
            <li><?php echo  ($listingData[0]['feature_hottubjacuzzi'] && $listingData[0]['feature_hottubjacuzzi']=='n')?'':' Hot Tub Jacuzzi';  ?> </li>
            
            
            
            <li><?php echo  ($listingData[0]['feature_kitchenette'] && $listingData[0]['feature_kitchenette']=='n')?'':'Kitchenette';  ?> </li>
            <li><?php echo  ($listingData[0]['feature_livingroom'] && $listingData[0]['feature_livingroom']=='n')?'':'Living Room';  ?> </li>
            <li><?php echo  ($listingData[0]['feature_loft'] && $listingData[0]['feature_loft']=='n')?'':'Loft';  ?> </li>
            <li><?php echo  ($listingData[0]['feature_onsitesecurity'] && $listingData[0]['feature_onsitesecurity']=='n')?'':'Onsite Security';  ?> </li>
            <li><?php echo  ($listingData[0]['feature_patio'] && $listingData[0]['feature_patio']=='n')?'':'Patio';  ?> </li>
            <li><?php echo  ($listingData[0]['feature_petsallowed'] && $listingData[0]['feature_petsallowed']=='n')?'':'Pets Allowed';  ?> </li>
            <li><?php echo  ($listingData[0]['feature_playroom'] && $listingData[0]['feature_playroom']=='n')?'':'Play Room';  ?> </li>
            <li><?php echo  ($listingData[0]['feature_pool'] && $listingData[0]['feature_pool']=='n')?'':'Pool';  ?> </li>
            <li><?php echo  ($listingData[0]['feature_porch'] && $listingData[0]['feature_porch']=='n')?'':'Porch';  ?> </li>
            <li><?php echo  ($listingData[0]['feature_rooftopdeck'] && $listingData[0]['feature_rooftopdeck']=='n')?'':'Rooftop Deck';  ?> </li>
            <li><?php echo  ($listingData[0]['feature_sauna'] && $listingData[0]['feature_sauna']=='n')?'':'Sauna';  ?> </li>
            <li><?php echo  ($listingData[0]['feature_smokingpermitted'] && $listingData[0]['feature_smokingpermitted']=='n')?'':'Smoking Permitted';  ?> </li>
            <li><?php echo  ($listingData[0]['feature_woodfireplace'] && $listingData[0]['feature_woodfireplace']=='n')?'':'Wood Fireplace';  ?> </li>
            <li><?php echo  ($listingData[0]['feature_wheelchairaccess'] && $listingData[0]['feature_wheelchairaccess']=='n')?'':'Wheelchair Access';  ?> </li>
            
            
        </ul>
    </div>
  <div class="clearfix"></div>
  <h4>Activities</h4>
    <div class="bg-white col-sm-12 p20 list-col-3">
    <h6><strong>Activities:</strong></h6>
        <ul class="amenities">
            <li><?php echo  ($listingData[0]['activity_antiquing'] && $listingData[0]['activity_antiquing']=='n')?'':'Antiquing';  ?> </li>
            <li><?php echo  ($listingData[0]['activity_basketballcourt'] && $listingData[0]['activity_basketballcourt']=='n')?'':'Basketball Court';  ?> </li>
            <li><?php echo  ($listingData[0]['activity_beachcombing'] && $listingData[0]['activity_beachcombing']=='n')?'':'Beachcombing';  ?> </li>
            <li><?php echo  ($listingData[0]['activity_bicycling'] && $listingData[0]['activity_bicycling']=='n')?'':'Bicycling';  ?> </li>
            <li><?php echo  ($listingData[0]['activity_bikerentals'] && $listingData[0]['activity_bikerentals']=='n')?'':'Bike Rentals';  ?> </li>
            <li><?php echo  ($listingData[0]['activity_birdwatching'] && $listingData[0]['activity_birdwatching']=='n')?'':'Birdwatching';  ?> </li>
            <li><?php echo  ($listingData[0]['activity_boatrentals'] && $listingData[0]['activity_boatrentals']=='n')?'':'Boat Rentals';  ?> </li>
            <li><?php echo  ($listingData[0]['activity_boating'] && $listingData[0]['activity_boating']=='n')?'':'Boating';  ?> </li>
            <li><?php echo  ($listingData[0]['activity_botanicalgarden'] && $listingData[0]['activity_botanicalgarden']=='n')?'':'Botanical Garden';  ?> </li>
            
            <li><?php echo  ($listingData[0]['activity_bikesprovided'] && $listingData[0]['activity_bikesprovided']=='n')?'':'Bikes Provided';  ?> </li>
            
            <li><?php echo  ($listingData[0]['activity_bayfishing'] && $listingData[0]['activity_bayfishing']=='n')?'':'Bay Fishing';  ?> </li>
            
            <li><?php echo  ($listingData[0]['activity_canoe'] && $listingData[0]['activity_canoe']=='n')?'':'Canoe';  ?> </li>
            <li><?php echo  ($listingData[0]['activity_churches'] && $listingData[0]['activity_churches']=='n')?'':'Churches';  ?> </li>
            <li><?php echo  ($listingData[0]['activity_cinemas'] && $listingData[0]['activity_cinemas']=='n')?'':'Cinemas';  ?> </li>
            
            <li><?php echo  ($listingData[0]['activity_deepseafishing'] && $listingData[0]['activity_deepseafishing']=='n')?'':'Deep Sea Fishing';  ?> </li>
            <li><?php echo  ($listingData[0]['activity_fishing'] && $listingData[0]['activity_fishing']=='n')?'':'Fishing';  ?> </li>
            <li><?php echo  ($listingData[0]['activity_fitnesscenter'] && $listingData[0]['activity_fitnesscenter']=='n')?'':'Fitness Center';  ?> </li>
            <li><?php echo  ($listingData[0]['activity_golf'] && $listingData[0]['activity_golf']=='n')?'':'Golf';  ?> </li>
            
            <li><?php echo  ($listingData[0]['activity_healthbeautyspa'] && $listingData[0]['activity_healthbeautyspa']=='n')?'':'Health Beauty Spa';  ?> </li>
            <li><?php echo  ($listingData[0]['activity_hiking'] && $listingData[0]['activity_hiking']=='n')?'':'Hiking';  ?> </li>
            <li><?php echo  ($listingData[0]['activity_horsebackriding'] && $listingData[0]['activity_horsebackriding']=='n')?'':'Horseback Riding';  ?> </li>
            <li><?php echo  ($listingData[0]['activity_horseshoes'] && $listingData[0]['activity_horseshoes']=='n')?'':'Horseshoes';  ?> </li>
            <li><?php echo  ($listingData[0]['activity_hotairballooning'] && $listingData[0]['activity_hotairballooning']=='n')?'':'Hot Air Ballooning';  ?> </li>
            <li><?php echo  ($listingData[0]['activity_iceskating'] && $listingData[0]['activity_iceskating']=='n')?'':'Ice Skating';  ?> </li>
            <li><?php echo  ($listingData[0]['activity_jetskiing'] && $listingData[0]['activity_jetskiing']=='n')?'':'Jet Skiing';  ?> </li>
            <li><?php echo  ($listingData[0]['activity_kayaking'] && $listingData[0]['activity_kayaking']=='n')?'':'Kayaking';  ?> </li>
            <li><?php echo  ($listingData[0]['activity_livetheater'] && $listingData[0]['activity_livetheater']=='n')?'':'Live Theater';  ?> </li>
            <li><?php echo  ($listingData[0]['activity_marina'] && $listingData[0]['activity_marina']=='n')?'':'Marina';  ?> </li>
            
            <li><?php echo  ($listingData[0]['activity_miniaturegolf'] && $listingData[0]['activity_miniaturegolf']=='n')?'':'Miniature Golf';  ?> </li>
            <li><?php echo  ($listingData[0]['activity_mountainbiking'] && $listingData[0]['activity_mountainbiking']=='n')?'':'Mountain Biking';  ?> </li>
            <li><?php echo  ($listingData[0]['activity_moviecinemas'] && $listingData[0]['activity_moviecinemas']=='n')?'':'Movie Cinemas';  ?> </li>
            <li><?php echo  ($listingData[0]['activity_museums'] && $listingData[0]['activity_museums']=='n')?'':'Museums';  ?> </li>
            <li><?php echo  ($listingData[0]['activity_paddleboating'] && $listingData[0]['activity_paddleboating']=='n')?'':'Paddle Boating';  ?> </li>
            <li><?php echo  ($listingData[0]['activity_paragliding'] && $listingData[0]['activity_paragliding']=='n')?'':'Paragliding';  ?> </li>
            <li><?php echo  ($listingData[0]['activity_parasailing'] && $listingData[0]['activity_parasailing']=='n')?'':'Parasailing';  ?> </li>
            <li><?php echo  ($listingData[0]['activity_playground'] && $listingData[0]['activity_playground']=='n')?'':'Playground';  ?> </li>
            <li><?php echo  ($listingData[0]['activity_recreationcenter'] && $listingData[0]['activity_recreationcenter']=='n')?'':'Recreation Center';  ?> </li>
            <li><?php echo  ($listingData[0]['activity_restaurants'] && $listingData[0]['activity_restaurants']=='n')?'':'Restaurants';  ?> </li>
            <li><?php echo  ($listingData[0]['activity_rollerblading'] && $listingData[0]['activity_rollerblading']=='n')?'':'Rollerblading';  ?> </li>
            <li><?php echo  ($listingData[0]['activity_sailing'] && $listingData[0]['activity_sailing']=='n')?'':'Sailing';  ?> </li>
            <li><?php echo  ($listingData[0]['activity_shelling'] && $listingData[0]['activity_shelling']=='n')?'':'Shelling';  ?> </li>
            <li><?php echo  ($listingData[0]['activity_shopping'] && $listingData[0]['activity_shopping']=='n')?'':'Shopping';  ?> </li>
            <li><?php echo  ($listingData[0]['activity_sightseeing'] && $listingData[0]['activity_sightseeing']=='n')?'':'Sightseeing';  ?> </li>
            <li><?php echo  ($listingData[0]['activity_skiing'] && $listingData[0]['activity_skiing']=='n')?'':'Skiing';  ?> </li>
            <li><?php echo  ($listingData[0]['activity_spa'] && $listingData[0]['activity_spa']=='n')?'':'Spa';  ?> </li>
            <li><?php echo  ($listingData[0]['activity_surffishing'] && $listingData[0]['activity_surffishing']=='n')?'':'Surf Fishing';  ?> </li>
            <li><?php echo  ($listingData[0]['activity_surfing'] && $listingData[0]['activity_surfing']=='n')?'':'Surfing';  ?> </li>
            <li><?php echo  ($listingData[0]['activity_swimming'] && $listingData[0]['activity_swimming']=='n')?'':'Swimming';  ?> </li>
            <li><?php echo  ($listingData[0]['activity_tennis'] && $listingData[0]['activity_tennis']=='n')?'':'Tennis';  ?> </li>
            <li><?php echo  ($listingData[0]['activity_themeparks'] && $listingData[0]['activity_themeparks']=='n')?'':'Theme Parks';  ?> </li>
            <li><?php echo  ($listingData[0]['activity_tennis'] && $listingData[0]['activity_tennis']=='n')?'':'Tennis';  ?> </li>
            
            <li><?php echo  ($listingData[0]['activity_waterparks'] && $listingData[0]['activity_waterparks']=='n')?'':'Waterparks';  ?> </li>
            <li><?php echo  ($listingData[0]['activity_walking'] && $listingData[0]['activity_walking']=='n')?'':'Walking';  ?> </li>
            <li><?php echo  ($listingData[0]['activity_waterskiing'] && $listingData[0]['activity_waterskiing']=='n')?'':'Water Skiing';  ?> </li>
            <li><?php echo  ($listingData[0]['activity_watertubing'] && $listingData[0]['activity_watertubing']=='n')?'':'Water Tubing';  ?> </li>
            <li><?php echo  ($listingData[0]['activity_wildlifeviewing'] && $listingData[0]['activity_wildlifeviewing']=='n')?'':'Wildlife Viewing';  ?> </li>
            <li><?php echo  ($listingData[0]['activity_zoo'] && $listingData[0]['activity_zoo']=='n')?'':'Zoo';  ?> </li>
            
            
        </ul>
    </div>
	<div class="clearfix"></div>
</div>
  
  <?php if(isset($listingData[0]['result_specials']) && !empty($listingData[0]['result_specials']) && $listingData[0]['result_specials']!=''){
    
    ?>
	<div>
    <div id="specials" class="section specials-section-area">
        <h4>Specials</h4>
        <div class="spec-area-bg">
            <p><strong>Location: <?php echo  $listingData[0]['location_4_title'].', '.$listingData[0]['location_3_title'].', '.$listingData[0]['zip_code']; ?></strong></p>
            
            <p><strong>Rental ID:</strong> <?php echo $listingData[0]['rental_id']; ?>  - <?php echo $listingData[0]['title'].', '.$listingData[0]['address'].', '.$listingData[0]['bedroom'].' Bedrooms, '.$listingData[0]['bathroom'].' Bathrooms, '.$listingData[0]['sleeps'].' Sleeps '; ?></p>
            <br>
            <?php foreach($listingData[0]['result_specials'] as $val) { ?>
            <p><strong>Ends <?php echo $val->sp_renewal_date; ?></strong></p><p><strong><?php echo $val->sp_name; ?></strong>: </p><?php echo $val->sp_description; ?><p>
           
            <?php } ?>
        </div>
    </div>
   <?php } ?>                     
                        
  
  <div class="pro-loc" id="location">
	<h4>Property Location</h4>
	<iframe src="<?php echo "https://www.google.com/maps?q=".$listingData[0]['latitude'].",".$listingData[0]['longitude'].'&output=embed'; ?>" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
  </div>
  
  
  <div class="check-availability" id="availability">
	<h3>Check Availability</h3>
	
	<div class="calender-plugin">
		<div id="availability" class="section">
                        <ul>
                            <li><strong>Colour Key:</strong></li>
                            <li class="available"><span>&nbsp;</span> Available</li>
                            <li class="booked"><span>&nbsp;</span> Booked</li>
                                                        <li class="changeoverday"><span>&nbsp;</span> Changeover Day</li>
                        </ul>

                        <div class="calendar-container availability-calendar">
                            <div class="calendar-prev"></div>
                        <div id="calendar"></div>
                            <div class="calendar-next"></div>
                        </div>
                        <div class="clearfix"></div>

                        <script>
                        var booked_dates={ 'dates': []};
                        <?php if(isset($listingData[0]['availablity']) && !empty($listingData[0]['availablity'])){ ?>
                            booked_dates = { 'dates': [ <?php foreach($listingData[0]['availablity'] as $val) {?>{ 'from_date': '<?php echo $val->start_date; ?>','from_period': '<?php echo $val->start_date_period; ?>','to_date': '<?php echo $val->end_date; ?>','to_period': '<?php echo $val->end_date_period; ?>' },<?php }?>]};;
                        <?php } ?>
                            $(document).ready(function () {
                                $(".calendar-prev").click(function () {
                                    $(".ui-datepicker-prev").trigger("click");
                                });

                                $(".calendar-next").click(function () {
                                    $(".ui-datepicker-next").trigger("click");
                                });
                            });

                            $('#calendar').datepicker({
                                numberOfMonths: [2,3],
                                stepMonths: 6,
                                minDate: 0,
                                maxDate: "+5y",
                                beforeShowDay: function(d) {
                                    // date format: mm-dd-yyyy
                                    var date = (d.getMonth()+1);
                                    if(d.getMonth()<9)
                                        date="0"+date;
                                    date+= "-";

                                    if(d.getDate()<10) date+="0";
                                        date+=d.getDate() + "-" + d.getFullYear();
                                        dateAux = date.replace(/-/g,"/");
                                    var date_type = '';

                                    for(i=0; i < booked_dates.dates.length; i++) {

                                        checkin = booked_dates.dates[i].from_date;
                                        checkout = booked_dates.dates[i].to_date;
                                        from_period = booked_dates.dates[i].from_period;
                                        to_period = booked_dates.dates[i].to_period;
                                        checkinAux = checkin.replace(/-/g,"/");
                                        checkoutAux = checkout.replace(/-/g,"/");

										//#8: Calendar Availability and Pricing
                                        if (checkin == date) {
                                            
                                            if(checkin == checkout && from_period == to_period) {
                                                if(from_period == 1) {

                                                    if((i+1 < booked_dates.dates.length ) && (booked_dates.dates[i+1].from_date == date)) {
                                                        date_type = 'booked';
                                                    } else {
                                                        date_type = 'bookedAM';
                                                    }
                                                } else {
                                                    if(booked_dates.dates[i-1].to_date == date) {
                                                        date_type = 'booked';
                                                    } else {
                                                        date_type = 'bookedPM';
                                                    }
                                                }
                                            } else {
                                                if(checkin == checkout && from_period != to_period) {
                                                    date_type = 'booked';
                                                } else if (checkin != checkout && from_period == to_period) {
                                                    if (from_period == 1) {
                                                        date_type = 'booked';
                                                    } else if (from_period == 2) {
                                                        date_type = 'bookedPM';
                                                    }
                                                } else if (checkin != checkout && from_period != to_period) {
                                                    if (from_period == 1) {
                                                        date_type = 'booked';
                                                    } else if (from_period == 2) {
                                                        date_type = 'bookedPM';
                                                    }
                                                }
                                            }
                                            break;
                                        } else if(checkout == date) {
                                            if(checkin != checkout) {
                                                if(booked_dates.dates[i].to_period == 2) {
                                                    date_type = 'booked';
                                                } else {
                                                    if((i+1 < booked_dates.dates.length ) && (booked_dates.dates[i+1].from_date == date)) {
                                                        date_type = 'booked';
                                                    } else {
                                                        date_type = 'bookedAM';
                                                    }
                                                }
                                            }
                                            break;
                                        } else if ( ( (new Date(dateAux) - new Date(checkinAux)) > 0 ) && ( (new Date(checkoutAux) - new Date(dateAux)) > 0 ) ) {
                                            date_type = 'booked';
                                            break;
                                        }
                                        
                                        /*if ( date == checkout ) {
                                            
                                            if ( (i+1 < booked_dates.dates.length ) && ((checkout == booked_dates.dates[i+1].from_date) || checkin == booked_dates.dates[i-1].to_date) ) {
                                                date_type = 'booked';
                                            } else {
                                                date_type = 'bookedAM';

                                            }
                                            break;
                                        } else if ( date == checkin ) {
                                            if(date == "12-16-2014") {
                                                console.log(checkin);
                                                console.log(checkout);
                                            }
                                            date_type = 'bookedPM';
                                            break;
                                        } else if ( ( (new Date(date) - new Date(checkin)) > 0 ) && ( (new Date(checkout) - new Date(date)) > 0 ) ) {
                                            date_type = 'booked';
                                            break;
                                        } else {
                                            date_type = 'available';
                                        }*/
                                    }
                                    return [false, date_type, ""];
                                }
                            });
                        </script>

                                                    <span class="availability-min-stay">Minimum Stay: 3 NIGHTS</span>                                          </div>
	</div>
	
  </div>
  
  <div class="rates-cont" id="rates">  
	<h3>Rates</h3>
    <?php  if(isset($listingData[0]['rate_list']) && !empty($listingData[0]['rate_list'])){
        ?>
        <table cellspacing="2" cellpadding="2" border="0" class="table table-striped table-condensed table-hover rates-table">
            <tbody>
                <tr>
                    <th width="100">Start Date</th>
                    <th width="100">End Date</th>
                    <th width="100">Daily</th>
                    <th width="100">Weekly</th>
                    <th width="100">Monthly</th>
                    <th width="100">Season</th>
                </tr>
                <?php foreach($listingData[0]['rate_list'] as $key=>$val) {
                    ?>
                    <tr>
                    <td><?php echo $val->from_date; ?></td>
                    <td><?php echo $val->to_date; ?></td>
                    <td><?php echo ($val->DAY>0)?$currency[Config::get('params.currency_default')]['symbol'].$val->DAY:''; ?></td>
                    <td><?php echo ($val->WEEK>0)?$currency[Config::get('params.currency_default')]['symbol'].$val->WEEK:''; ?></td>
                    <td><?php echo ($val->MONTH>0)?$currency[Config::get('params.currency_default')]['symbol'].$val->MONTH:''; ?></td>
                    <td><?php echo ($val->season>0)?$currency[Config::get('params.currency_default')]['symbol'].$val->season:''; ?></td>
                </tr>
               <?php }?>
                
           
            </tbody>
        </table>
 <?php   } ?>
       
	<p class="bg-silver payment-types p10"><strong>Payment Types Accepted: </strong><?php echo $listingData[0]['payment_types']; ?></p>

                            
	<p class="payment-types payment-notes"><strong>Notes: </strong><?php echo $listingData[0]['notes']; ?></p>
    
	
  </div>
  
  <?php if(isset($listingData[0]['image']) && !empty($listingData[0]['image'])){ ?>
  <div class="listing__photos property-photos-section" id="photos">
	<h4>Photos</h4>
    <?php foreach($listingData[0]['image'] as $val) {?>
	<div class="photo-box col-sm-6">
		<div class="photo__inr">
			<figure><?php echo $val->image_caption; ?></figure>
			<div class="photo__img">
				<img src="<?php echo config('params.imagesPath').$val->prefix.'photo_'.$val->id.".".strtolower($val->type); ?>" />
			</div>
		</div>
	</div>
	<?php } ?>
	
	
	
	
  </div>
  <?php } ?>
  
  
  <div class="owner-info-area col-sm-12 p0" id="owner">
  <?php if(isset($account_info) && !empty($account_info)){ ?>
	<h3>Owner Info</h3>
	<p class="owner-name">Owner Name: <?php echo $account_info[0]->first_name.' '.$account_info[0]->last_name; ?></p>
 	
	<div class="btns-group owner-info-btns mb20 mt20">
			<div class="col-sm-4"><a href="#" data-toggle="modal" data-target="#emailModal" class="btn-lg white bg-orange email-owner-btn">Email Owner</a></div>
			<div class="col-sm-4"><a class="btn-lg black bg-silver phone-btn"><i class="fa fa-phone"></i> <span data-hover="<?php echo $account_info[0]->phone; ?>" class="def">Owner Phone Number</span></a></div>
			<div class="col-sm-4"><a href="#" class="btn-lg white bg-blue seeall-btn">Click to See All of My Rentals</a></div>
			<div class="clearfix"></div>
	</div>
	<div class="clearfix"></div>
    <?php if($account_info[0]->url && $account_info[0]->url!=''){
    ?>
         <p>Links to more information:</p>
        <p><?php echo $account_info[0]->url; ?></p>
    <?php } ?>
   
	<?php } ?>
	<p>Rental ID: #<?php echo $listingData[0]['rental_id']; ?></p>
	<p>There have been <?php echo $listingData[0]['number_views']; ?> visitors to this page since the counter was last reset in 
    <?php 
    $updated = new DateTime($listingData[0]['updated']);
    echo $updated->format('Y');
    ?>
    .</p>
	<p>This listing was first published here in 
    <?php 
    $entered = new DateTime($listingData[0]['entered']);
    echo $entered->format('Y');
    ?>
    .</p>
	<p>Date last modified - <?php 
    $updated = new DateTime($listingData[0]['updated']);
    echo $updated->format('m-d-Y');
    ?></p>
	
  </div>
  
  
  
</div>




<div class="listing__rgt col-sm-3">
			
			
				<div class="detailBox">
                <?php if($listingData[0]['min_week']>0) {  ?>                                
					<h4>{{$currency[Config::get('params.currency_default')]['symbol']}} <?php echo $listingData[0]['min_week']; ?> - {{$currency[Config::get('params.currency_default')]['symbol']}}<?php echo $listingData[0]['max_week']; ?></h4>
					<span class="perweek">per week</span>
                    <hr class="dvr">
                <?php }elseif($listingData[0]['min_day']>0){
                    ?>
                   	<h4>{{$currency[Config::get('params.currency_default')]['symbol']}} <?php echo $listingData[0]['min_day']; ?> - {{$currency[Config::get('params.currency_default')]['symbol']}}<?php echo $listingData[0]['max_day']; ?></h4>
					<span class="perweek">per Day</span>
                    <hr class="dvr"> 
              <?php  }elseif($listingData[0]['min_season']>0){
                    ?>
                   	<h4>{{$currency[Config::get('params.currency_default')]['symbol']}} <?php echo $listingData[0]['min_season']; ?> - {{$currency[Config::get('params.currency_default')]['symbol']}}<?php echo $listingData[0]['max_season']; ?></h4>
					<span class="perweek">per season</span>
                    <hr class="dvr"> 
              <?php  } ?>
						
					<p><?php echo $listingData[0]['bedroom']; ?> Bedrooms / <?php echo $listingData[0]['bathroom']; ?> Baths / Sleeps <?php echo $listingData[0]['sleeps']; ?></p>
					
                    <?php if(isset($listingData[0]['checkin']) && $listingData[0]['checkin']!=''){
                        ?>
                        <hr class="dvr">
                        <p>Check in: <?php echo $listingData[0]['checkin']; ?><br>Check out: <?php echo $listingData[0]['checkout']; ?></p>
                  <?php  } ?>
					
				</div>

				<div class="scrolink">
					<a class="btn btn-lg btn-primary btn-block view-clndr-sidebar" href="#availability">View Calendar</a>
					<a href="#owner" class="btn  btn-lg btn-block white bg-orange owner-info-sidebar">Owner Info</a>
				</div>
				
				<div class="rate starRating-sidebar">
				
				<div class="rate-stars col-sm-12 p0">

		<a  href="<?php echo url('review/'.$listingData[0]['rental_id'].'/listing'); ?>" class="iframe fancy_window_review star-rating">
						<div class="stars-rating ">
							<div class="rate-0"></div>
						</div>
					</a>
				</div>
				
				<p><a rel="nofollow" href="<?php echo url('review/'.$listingData[0]['rental_id'].'/listing'); ?>" class="iframe fancy_window_review">Be the first to review this property!</a></p>
				</div>
				
				
				<a class="btn btn-lg bg-silver btn-block black owner-number-sidebar" ><i class="fa fa-phone"></i> <span class="def" data-hover="(610) 291-0899" class="def">Owner Phone Number</span></a>
				
				<a data-toggle="modal" data-target="#emailFriendModal" class="btn btn-lg bg-silver btn-block emailtofriend-btn black emailtofriend-btnSidebar">@ Email to Friend</a>
				
					
				<div class="checker">
					<p id="dates_available">Enter you rental dates</p>
					
					
                    <div class="form-group col-sm-6 pl0">
                      <input type="text" id="dpd1" value="" placeholder="Arrival Date" data-provide="datepicker" class="form-control" />  
                      <span class="add-on"><i class="fa fa-calendar"></i></span>
                    </div>

                    <div class="form-group col-sm-6 pl0 ">
                      <input type="text" id="dpd2" value="" placeholder="Departure Date" data-provide="datepicker" class="form-control" />
                      <span class="add-on"><i class="fa fa-calendar"></i></span>
                    </div>
                
				
				</div>
			
			</div>
		
			
					
			</div>
			<div class="clearfix"></div>
			
			
		</div>
	</section>
	
	
 
<!-- emailModal -->
<div class="modal fade emailModal" id="emailModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog w60">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4></h4>
      </div>
      <div class="modal-body">
        <iframe id="fancybox-frame1457578912627" name="fancybox-frame1457578912627" class="fancybox-iframe" frameborder="0" vspace="0" hspace="0" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen="" scrolling="auto" src="<?php echo url('listing_emailform/'.$listingData[0]['rental_id']); ?>" width="100%"></iframe>
      </div>
    </div>
  </div>
</div>


<!-- emailFriendModal 
<div class="modal fade emailModal" id="emailFriendModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog w60">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4></h4>
      </div>
      <div class="modal-body">
        <iframe id="fancybox-frame1457579659809" name="fancybox-frame1457579659809" class="fancybox-iframe" frameborder="0" vspace="0" hspace="0" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen="" scrolling="auto" src="https://www.shoresummerrentals.com/popup/popup.php?pop_type=listing_emailform&amp;id=<?php echo $listingData[0]['rental_id']; ?>&amp;receiver=friend" width="100%"></iframe>
      </div>
    </div>
  </div>
</div>
-->


	
	<!--<section class="advertisement-area mt30 advertisement-bottom text-center">
        <div class="container">
				<div class="googleAdsBottom">
					
					<script type="text/javascript">
                    google_ad_client	= "pub-0191483045807371";
                    google_ad_width		= 728;
                    google_ad_height	= 90;
                    google_ad_format	= "728x90_as";
                    google_ad_type		= "text_image";
                    google_ad_channel	= "";
                    google_color_border	= "336699";
                    google_color_bg		= "FFFFFF";
                    google_color_link	= "0000FF";
                    google_color_url	= "008000";
                    google_color_text	= "000000";
                </script>

					<script type="text/javascript" src="https://pagead2.googlesyndication.com/pagead/show_ads.js"></script><ins id="aswift_0_expand" style="display:none;border:none;height:0px;margin:0;padding:0;position:relative;visibility:visible;width:728px;background-color:transparent"><ins id="aswift_0_anchor" style="display:block;border:none;height:90px;margin:0;padding:0;position:relative;visibility:visible;width:728px;background-color:transparent"><iframe width="728" height="0" frameborder="0" marginwidth="0" marginheight="0" vspace="0" hspace="0" allowtransparency="true" scrolling="no" allowfullscreen="true" onload="var i=this.id,s=window.google_iframe_oncopy,H=s&amp;&amp;s.handlers,h=H&amp;&amp;H[i],w=this.contentWindow,d;try{d=w.document}catch(e){}if(h&amp;&amp;d&amp;&amp;(!d.body||!d.body.firstChild)){if(h.call){setTimeout(h,0)}else if(h.match){try{h=s.upd(h,i)}catch(e){}w.location.replace(h)}}" id="aswift_0" name="aswift_0" style="left:0;position:absolute;top:0;"></iframe></ins></ins>                    
				</div>
		</div>
	</section>-->
	
	
	<script>
	jQuery( ".def" ).click(function() {
var dataHover = $(this).attr("data-hover");
	jQuery(this).html(dataHover);
});
</script>
	<script>
                    var period = { 'rates': [{"id":"3287708","listing_id":"3603","from_date":"2016-05-21","to_date":"2016-05-28","day":"0.00","week":"1400.00","month":"0.00","season":"0.00","sameday_in_out":""},{"id":"3287709","listing_id":"3603","from_date":"2016-05-28","to_date":"2016-06-04","day":"0.00","week":"1700.00","month":"0.00","season":"0.00","sameday_in_out":""},{"id":"3287710","listing_id":"3603","from_date":"2016-06-04","to_date":"2016-07-02","day":"0.00","week":"1400.00","month":"0.00","season":"0.00","sameday_in_out":""},{"id":"3287711","listing_id":"3603","from_date":"2016-07-02","to_date":"2016-09-10","day":"0.00","week":"1700.00","month":"0.00","season":"0.00","sameday_in_out":""},{"id":"3287707","listing_id":"3603","from_date":"2016-09-10","to_date":"2016-10-01","day":"0.00","week":"1400.00","month":"0.00","season":"0.00","sameday_in_out":""}] };
                    function calculateRate(){
                        var checkin = $('#dpd1').val();
                        var checkout = $('#dpd2').val();
                        var text_available = 'Your dates are <strong class="text-success">Available!</strong>';
                        var text_notavailable = 'Your dates are <strong class="text-error">Not Available!</strong>';

                        if ( checkin && checkout ) {

                            // Check avilabilite on the calendar
                            available = true;
                            for(i=0; i < booked_dates.dates.length; i++) {
                                from_date = booked_dates.dates[i].from_date;
                                to_date = booked_dates.dates[i].to_date;

                                if ((( new Date(from_date) - new Date(checkin) <= 0 ) && ( new Date(checkin) - new Date(to_date) < 0 )) ||
                                    (( new Date(from_date) - new Date(checkout) < 0 ) && ( new Date(checkout) - new Date(to_date) <= 0 )) ||
                                    ( from_date == checkin ) ||  ( to_date == checkout )) {
                                    available = false;
                                    break;
                                }
                            }

                            if (available) {

                                // Calculate number of nights
                                // var nights_millisec = Math.abs(new Date(checkin) - new Date(checkout));
                                // var one_day = 1000*60*60*24;
                                // nights = Math.round(nights_millisec/one_day);

                                // // Calculate prices
                                // // rate_total = 0;
                                // price = 0;
                                // for(i=0; i < period.rates.length; i++) {
                                //     from_date = period.rates[i].from_date;
                                //     to_date = period.rates[i].to_date;

                                //     if ((( new Date(checkin) - new Date(from_date) >= 0 ) && ( new Date(to_date) - new Date(checkin) >= 0 )) ||
                                //         (( new Date(checkout) - new Date(from_date) >= 0 ) && ( new Date(to_date) - new Date(checkout) >= 0 ))) {
                                //         if ( nights < 7 ) price = period.rates[i].day;
                                //         if ( nights >= 7 && nights < 30 ) price = period.rates[i].week;
                                //         if ( nights >= 30 ) price = period.rates[i].month;
                                //     }
                                // }

                                $('#dates_available').html(text_available);
                                // $('#nights').text('Selected day(s): '+nights);
                                // $('#rate_per_night').text("$" + price);
                                // $('#rate_total').text('$' + rate_total);
                            } else {
                                $('#dates_available').html(text_notavailable);
                            }
                        } else {
                            // $('#nights').html('');
                            $('#dates_available').html('Enter your rental dates');
                        }
                    }
                    $(document).ready(function(){
                        $("#dpd1").datepicker({
                            minDate: 0,
                            onSelect: function(dateText, inst) {
                                var actualDate = new Date(dateText);
                                var newDate = new Date(actualDate.getFullYear(), actualDate.getMonth(), actualDate.getDate()+1);
                                $('#dpd2').datepicker('option', 'minDate', newDate );
                            },
                            onClose: function() {
                                $('#dpd2').focus();
                            }
                        });

                        $("#dpd2").datepicker({
                            onClose: function() {
                                calculateRate();
                            }
                        });

                        $("#dpd2").bind("focus", function( event ){
                            if ( $('#dpd1').val() == '' ) {
                                $('#dpd1').focus();
                            }
                        });
                    });
                </script>
					
@endsection