@extends('front')

@section('content')

    <link rel="stylesheet" href="{{ asset('front/css/members.min.css') }}" />
	<link rel="stylesheet" href="{{ asset('front/css/content_custom.min.css') }}" />
	<link href="https://www.shoresummerrentals.com/custom/domain_1/theme/default/content_custom.min.css" rel="stylesheet" type="text/css" media="all">
    <!--link rel="stylesheet" href="{{ asset('front/css/bootstrap-responsive.min.css') }}" /-->
     
     
    
    
<style>

.content-custom h1, .content-custom h2, .content-custom h3{
    line-height: 1;
    text-indent: 0;
	margin:0;
}
.content-custom{
	margin-top:0;
}
.content-custom p{
	font-size:18px;
	line-height: 1.5;
    font-weight: 300;
}
.content-custom a, .content-custom a:visited {
    text-decoration: underline;
}
.spam-warning-container{
	width:1200px;
	padding:0;
}
.content-custom h1, .content-custom h2, .content-custom h3 {
    font-weight: 400;
}
h1{
	margin-top:30px;
}
h1.avoid-msgs{
	color: #ff6600;
	font-weight: 600;
	font-size: x-large;
}
em, strong {
    font-weight: 600;
}

</style>	  

               
                
    
   <div class="free-rental-sign-form container">

        
		<div class="content content-full margin-top">

			<div class="content-main">
		
				<div class="content-custom"><h1><strong><br>FREE RENTAL SIGN FORM</strong></h1>
<h1><strong><br></strong></h1>
<h2><strong><span style="color: #ff6600; font-size: x-large;">Help Advertising Your Rental</span></strong></h2>
<p style="margin-top:10px;"><span style="color: #000000;">Your membership includes a FREE professional "FOR RENT" sign on your property. &nbsp;&nbsp;In certain towns, you are only permitted to hang one larger sign (18" x 24") or three smaller signs (6" x 24"). We provide wire ties with the sign to hang on railing or decking. The sign will have your phone number and rental id number on it. This has been very successful in securing additional rentals.&nbsp; By having the Rental ID on the sign, the renter will be able to pull up your property listing on our website to view interior photos, rates and all the details of your property.&nbsp; <a href="http://www.shoresummerrentals.com/contactus.php">Contact us</a> to request a sign. <span style="color: #000000;"><strong><span style="color: #000000;">Please include your name, property address, unit number, city, state, small or large sign and any other information you think we would need with your <a href="http://www.shoresummerrentals.com/contactus.php">request</a>.<br><br><a href="http://www.shoresummerrentals.com/contactus.php"><span style="color: #000000;"><img style="display: block; margin-left: auto; margin-right: auto;" src="http://shoresummerrentals.com/custom/domain_1/image_files/5_photo_224.jpg" alt="" width="500" height="381"></span></a></span></strong></span></span></p>
<p><a href="http://www.shoresummerrentals.com/contactus.php"><span style="color: #000000;"><span style="color: #000000;"><strong><span style="color: #000000;"><span style="color: #000000;"><span style="color: #000000;"><img style="display: block; margin-left: auto; margin-right: auto;" src="http://shoresummerrentals.com/custom/domain_1/image_files/5_photo_140.jpg" alt="" width="500" height="121"></span></span></span></strong></span></span></a></p>
<p><span style="color: #000000;">Please do not request a sign unless we have access to hang it on your property or you will be down in the next week or so to hang it yourself. The signs are costly and have been dropped off to several property owners in the past with owners never hanging them. &nbsp;If you have a lock box with a key to your property or someone in the area who has a key, I would be happy to pick up the key so that I can hang the sign for you. &nbsp;If you would like a sign for your property, please <a href="http://www.shoresummerrentals.com/contactus.php">contact us</a>.&nbsp; The sign will be tied to the decking or a railing. &nbsp;&nbsp;If you require a sturdy stand for the lawn, there will be a cost of $20. You can pay by credit card or mail a check.</span></p>
<p><span style="color: #000000;"><br></span></p></div>			
			</div>
			
					
		</div>

	</div>




		
    </script>
	<script src="{{ asset('/front/js/Chart.js') }}"></script>		
@endsection