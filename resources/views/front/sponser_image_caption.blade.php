@extends('popup')

@section('content')


  <link rel="stylesheet" type='text/css' href="{{ asset('front/css/structure.min.css') }}" />

<div class="modal-content modal-content-upload">
@if(session('message'))
<?php echo session('message');  ?>
@endif        
                <h2>
                    <b>Captions</b>
                    <span>
                        <a href="javascript:void(0);" onclick="parent.$.fancybox.close();">Close</a>
                    </span>
                </h2>



 <form id="uploadimage" name="uploadimage" action="<?php echo url('sponsors/change_imagecaption'); ?>" method="post" class="frmEmail" enctype="multipart/form-data">

                        <input type="hidden" name="pop_type" value="uploadimage">
                        
                       <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="item_type" value="listing">
                        <input type="hidden" name="main" value="">
                        <input type="hidden" name="level" value="<?php echo $level; ?>">
                        <input type="hidden" name="temp" value="">
                        <input type="hidden" name="gallery_item_id" id="gallery_item_id" value="<?php echo $item_id; ?>">
                        <input type="hidden" name="gallery_id" id="gallery_id" value="<?php echo $gallery_id; ?>">
                        <input type="hidden" name="image_id" id="image_id" value="<?php echo $image_id; ?>">
                        <input type="hidden" name="thumb_id" id="thumb_id" value="<?php echo $thumb_id; ?>">
                        <input type="hidden" name="item_id" id="item_id" value="<?php echo $item_id; ?>">
                        <input type="hidden" name="captions" id="captions" value="">
                        <input type="hidden" name="x1" value="0" id="x1">
                        <input type="hidden" name="y1" value="0" id="y1">
                        <input type="hidden" name="x2" value="" id="x2">
                        <input type="hidden" name="y2" value="" id="y2">
                        <input type="hidden" name="w" value="" id="w">
                        <input type="hidden" name="h" value="" id="h">
                        <input type="hidden" name="gallery_hash" value="">
                        <input type="hidden" name="order" value="">
                        
	<script type="text/javascript">

		function sendFile () {
			setValue('yes');
			$('#uploadimage').submit();
			$('#loadingBar').css('display', '');
			$('#imgFile').css('display', 'none');
			$('#submitButton').css('display', 'none');
		}

		function setValue(op){
			if (document.getElementById("uploadThumbID"))
				document.getElementById("uploadThumbID").value=op;
		}

		function noUpload(){
			parent.$.fancybox.close();
		}
		
		
	</script>

		
	<table border="0" cellpadding="0" cellspacing="0" class="standardForm">
						        
		<tbody><tr>
			<th class="wrapcaption">Image Caption:</th>
			<td class="wrapcaption">
			<input id="image_caption" type="text" name="image_caption" value="{{ $image_caption }}" class="inputExplode" maxlength="250"></td>
		</tr>
        
			</tbody></table>
	
	<table border="0" cellpadding="0" cellspacing="0" class="standardForm">
		<tbody><tr>
			<td height="24" colspan="2" class="formButton">
				<div id="overlay"></div>
				<p class="input-button-form">
				<button id="submitButton UploadImage" type="submit" onclick="setValue('no');$('td.formButton #overlay').show();" value="Upload">Submit										</button>
				</p>
			</td>
		</tr>
	</tbody></table>                    </form>
                                    
                </div>
                 @endsection