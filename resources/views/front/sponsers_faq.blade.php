
@extends('front')

@section('content')

	<link rel="stylesheet" href="{{ asset('front/css/bootstrap.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('front/css/members.min.css') }}" />
	<link href="https://www.shoresummerrentals.com/custom/domain_1/theme/default/content_custom.min.css" rel="stylesheet" type="text/css" media="all">
    <!--link rel="stylesheet" href="{{ asset('front/css/bootstrap-responsive.min.css') }}" /-->
     
     
    
    
	<div class="faq-page-container container members">

        <div class="container-fluid">

    <div class="faqMembers">

        
        
	<script type="text/javascript">
		function showAnswer(answer) {
			$(document).ready(function() {
				if ($('#'+answer).css('display') == 'none') {
					$('#'+answer).slideDown(400);					
                } else {
					$('#'+answer).slideUp(400);
                }
			});
		}
	</script>

	<div class="content-faq">
	
        <div class="faq-search flex-box-group">
            <div class="row-fluid">
                <form name="faq" action="/sponsors/faq.php" method="get">
                    <h4 class="col-sm-4">How can we help you?</h4>
                    <input type="search" class="col-sm-4" name="keyword" id="keyword" placeholder="Enter a question, keyword, topic..." value="" />
                    <button type="submit" class="btn btn-success col-sm-2 pull-right">Search</button>
                </form>
            </div>
        </div>
	
        
    <div class="sitemgr-pagination top-pagination">

        
        <div class="pagination-results">
            Found <strong>10</strong> records        </div>

    </div>

    <div class="faqAnswers"><div><h3 class="standardSubTitle"><a href="javascript:void(0);" onclick="showAnswer('answer0');">How does the "Sign me in automatically" work?</a></h3><p id="answer0" style="display:none">The Sign me in automatically is optional, it saves your username and password on your computer and every time you access the page you will be automatically logged in.</p></div><div><h3 class="standardSubTitle"><a href="javascript:void(0);" onclick="showAnswer('answer1');">What happens if I forget my password?</a></h3><p id="answer1" style="display:none">If you forget your password, please click on the 'Forgot your Password?' link of Owner login page. The password recovery email will be sent to the Account Holder's email address provided. The email will contain a link which will redirect the user to the 'Manage Account' section, where the password can be updated.</p></div><div><h3 class="standardSubTitle"><a href="javascript:void(0);" onclick="showAnswer('answer2');">How can I change my password?</a></h3><p id="answer2" style="display:none">After you are logged in, click on the 'Account link, you will see the Current Password field, type your current password in this field and your new password on the fields Password and Retype Password, then hit the submit button.</p></div><div><h3 class="standardSubTitle"><a href="javascript:void(0);" onclick="showAnswer('answer3');">Can I change my username?</a></h3><p id="answer3" style="display:none">Your username is the Account Holder's email address.  You can change it by logging into your account, click on the Account tab and type in your new e-mail.  Don't forget to Save.</p></div><div><h3 class="standardSubTitle"><a href="javascript:void(0);" onclick="showAnswer('answer4');">Can I change my item level?</a></h3><p id="answer4" style="display:none">Yes, you can.  After your listing is expired, you can change your membership type.  If you do not see that option, please contact us.</p></div><div><h3 class="standardSubTitle"><a href="javascript:void(0);" onclick="showAnswer('answer5');">How do I become a Featured Property?</a></h3><p id="answer5" style="display:none">Click on the List Your Property link from the Home Page to view the details and cost of this Add-On.  If you are interested, click on the Contact Us link from the Home Page to request this Add-On.  Also, the Diamond Level Main Membership includes the Featured Property Add-On for the entire year.</p></div><div><h3 class="standardSubTitle"><a href="javascript:void(0);" onclick="showAnswer('answer6');">How Can I Buy a Top Spot?</a></h3><p id="answer6" style="display:none">Click on the List Your Property link from the Home Page to view the details and cost of this Add-On.  If you are interested, click on the Contact Us link from the Home Page to request this Add-On.</p></div><div><h3 class="standardSubTitle"><a href="javascript:void(0);" onclick="showAnswer('answer7');">Why am I receiving an 'Account Locked' message?</a></h3><p id="answer7" style="display:none">If you attempt to access your account and type in an incorrect password 5 times the account will lock for 1 hour. This is for security reasons.</p></div><div><h3 class="standardSubTitle"><a href="javascript:void(0);" onclick="showAnswer('answer8');">Can you help create or modify my ad?</a></h3><p id="answer8" style="display:none">Of course, we are here to help but it really is pretty simple to do yourself and noone knows your home as well as you do.  You can find instructions on editing your listing on the Home Page under the Owner navigation called Listing Instructions.  If you need help, contact us.</p></div><div><h3 class="standardSubTitle"><a href="javascript:void(0);" onclick="showAnswer('answer9');">Which pages are included in the Specials Add-On?</a></h3><p id="answer9" style="display:none">The pages that you will have the option to advertise on are Last Minute Specials, Partial Week Rentals, Full Summer Rentals, Monthly Winter Rentals, Under Age of 25 Rentals and Properties for Sale.  Each page costs $25 but is valid for an entire year.</p></div></div>	
	</div>
    
    
</div>


            </div><!-- Close container-fluid div -->
            
        </div><!-- Close container div -->



		 <script type="text/javascript"> 
        
        if ($("#myChart").length) {
            //This will get the first returned node in the jQuery collection.
            var ctx = $("#myChart").get(0).getContext("2d");
        }

        function initializeDashboard() {
            $(".dial").knob({
                readOnly:   true,
                fgColor:    "#2980b9",
                bgColor:    "#DEE1E3",
                fontWeight: 300,
                thickness:  .2,
                width:      70,
                height:     70
            });

            $(".status, .floating-tip, .alert-new, #item_renewal").tooltip({
                animation: true,
                placement: "top"
            });
             
            if ($("#myChart").length) {
                //Get context with jQuery - using jQuery's .get() method.
                ctx = $("#myChart").get(0).getContext("2d");
                loadChart();
            }
        }

        $(function() {
            $("#alert").fadeOut(5000);
            initializeDashboard();
        });
        
        function showReply(id) {
            $('#review_reply'+id).css('display', '');
            $('#link_reply'+id).css('display', 'none');
            $('#cancel_reply'+id).css('display', '');
        }
        
        function hideReply(id) {
            $('#review_reply'+id).css('display', 'none');
            $('#link_reply'+id).css('display', '');
            $('#cancel_reply'+id).css('display', 'none');
        }
        
        function showLead(id) {
            $('#lead_reply'+id).css('display', '');
            $('#link_lead'+id).css('display', 'none');
            $('#cancel_lead'+id).css('display', '');
        }
        
        function hideLead(id) {
            $('#lead_reply'+id).css('display', 'none');
            $('#link_lead'+id).css('display', '');
            $('#cancel_lead'+id).css('display', 'none');
        }
        
        function saveReply(id) {
            $("#submitReply"+id).css("cursor", "default");
            $("#submitReply"+id).prop("disabled", "disabled");
            $("#submitReply"+id).html('Wait, Loading...');
            
            $.post("https://www.shoresummerrentals.com/sponsors/ajax.php", $("#formReply"+id).serialize(), function(data) {
                if (data == "ok") {
                    $("#msgReviewE"+id).css("display", "none");
                    $("#msgReviewS"+id).css("display", "");
                    $("#msgReviewS"+id).fadeOut(5000);
                } else {
                    $("#msgReviewE"+id).css("display", "");
                    $("#msgReviewS"+id).css("display", "none");
                }
                $("#submitReply"+id).html('Submit');
                $("#submitReply"+id).prop("disabled", "");
                $("#submitReply"+id).css("cursor", "pointer");
            });
        }
        
        function saveLead(id) {
            $("#submitLead"+id).css("cursor", "default");
            $("#submitLead"+id).prop("disabled", "disabled");
            $("#submitLead"+id).html('Wait, Loading...');
            
            $.post("https://www.shoresummerrentals.com/sponsors/ajax.php", $("#formLead"+id).serialize(), function(data) {
                if (data == "ok") {
                    $("#msgLeadE"+id).css("display", "none");
                    $("#msgLeadS"+id).css("display", "");
                    $("#msgLeadS"+id).fadeOut(5000);
                    setTimeout("leadBox('hide', "+id+");", 4000);
                    $("#title_replied"+id).css("display", "none");
                    $("#new_replied"+id).css("display", "");
                } else {
                    $("#msgLeadE"+id).html(data);
                    $("#msgLeadE"+id).css("display", "");
                    $("#msgLeadS"+id).css("display", "none");
                }
                $("#submitLead"+id).html('Submit');
                $("#submitLead"+id).prop("disabled", "");
                $("#submitLead"+id).css("cursor", "pointer");
            });
        }
        
        function reviewBox(option, id) {
            $("#reviews-list").children(".item-review").children(".review-detail").stop(true,true).slideUp();
            $("#reviews-list").children(".item-review").children(".review-summary").stop(true,true).slideDown().removeClass("new");
            if (option == "show") {
                $("#review-summary-"+id).slideUp(); 
                $("#review-detail-"+id).slideDown();
                setItemAsViewed("review", id);
            } else {
                $("#review-summary-"+id).slideDown(); 
                $("#review-detail-"+id).slideUp();
            }            
        }
        
        function leadBox(option, id) {
            $("#leads-list").children(".item-review").children(".review-detail").stop(true,true).slideUp();
            $("#leads-list").children(".item-review").children(".review-summary").stop(true,true).slideDown().removeClass("new");
            if (option == "show") {
                $("#lead-summary-"+id).slideUp(); 
                $("#lead-detail-"+id).slideDown();
                setItemAsViewed("lead", id);
            } else {
                $("#lead-summary-"+id).slideDown(); 
                $("#lead-detail-"+id).slideUp();
            }            
        }
        
        function dealBox(option, id) {
            $("#deals-list").children(".item-review").children(".review-detail").stop(true,true).slideUp();
            $("#deals-list").children(".item-review").children(".review-summary").stop(true,true).slideDown();
            if (option == "show") {
                $("#deal-summary-"+id).slideUp(); 
                $("#deal-detail-"+id).slideDown();
            } else {
                $("#deal-summary-"+id).slideDown(); 
                $("#deal-detail-"+id).slideUp();
            }            
        }
        
        function changeDealStatus(option, id, promocode) {
            $.post("https://www.shoresummerrentals.com/sponsors/deal/deal.php",{action: option, promotion_id: promocode}, function() {
                if (option == "freeUpDeal") {
                    $("#label_used"+id).css("display", "");
                } else {
                    $("#label_used"+id).css("display", "none");
                }
            });
        }
        
        function setItemAsViewed(type, id) {
            $.post("https://www.shoresummerrentals.com/sponsors/ajax.php", {
                ajax_type: 'setItemAsViewed',
                type: type,
                id: id
            }, function () {});
        }
       
        function loadDashboard(item_type, item_id) {
            $.post("<?php echo url('loadDashboard');?>", {
                ajax_type: 'load_dashboard',
                item_type: item_type,
                item_id: item_id,
                _token:'{{ csrf_token() }}'
            }, function (ret) {
                $(".webitem").removeClass("active");
                $("#"+item_type+"_"+item_id).addClass("active");
                scrollPage('#float_layer');
                $("#dashboard").hide().html(ret).fadeIn(800);
                initializeDashboard();
            });
        }

        function selectLegend(option, id, chartdata) {
            var countVisible = 0;
            
            if (option == "viewALL") {
                
                if ( $(".legend-ALL").hasClass("isvisible")) {
//                    $(".legend-ALL").removeClass("isvisible");
//                    $("#optionLegend > li > i").removeClass("checked");
//                    $("#optionLegend > li").removeClass("isvisible");
//                    $("#controlLegend > li").remove();
                } else {
                    countVisible = 2;
                    $("#optionLegend > li > i").addClass("checked");                   
                    $(".legend-ALL").addClass("isvisible");
                    $("#optionLegend > li").not(".isvisible").clone().appendTo("#controlLegend");
                    $("#optionLegend > li").addClass("isvisible");
                }
            } else {
                id: id
                chartdata: chartdata
                $newlegend = $(".legend-"+id).clone();

                if ($(".legend-"+id).hasClass("isvisible")) {
                    
                    //Check if there's at least one other legend selected to prevent empty chart
                    $('#optionLegend li').each(function() {
                        if ($(this).hasClass("isvisible")) {
                            countVisible++;
                        }
                    });

                    if (countVisible > 1) {
                        $(".legend-"+id).children("i").removeClass("checked");
                        $(".legend-"+id).removeClass("isvisible");
                        $("#controlLegend").children(".legend-"+id).remove();
                        $(".legend-ALL").children("i").removeClass("checked");
                        $(".legend-ALL").removeClass("isvisible");
                    }
                } else {
                    countVisible = 2;
                    $(".legend-"+id).children("i").addClass("checked");
                    $(".legend-"+id).addClass("isvisible");
                    $newlegend.appendTo("#controlLegend");
                }
            }
            if (countVisible > 1) {
                controlChart();
            }
        }
                
        function loadChart() {
            var data = {
                labels : chartLabels,
                datasets : initialReport
            };
            var steps = 5;
            var max = maxInitialReport;
            if (max < steps) {
                steps = max;
            }
            var options = {
                bezierCurve : false,
                scaleOverride: true,
                scaleSteps: steps,
                scaleStepWidth: Math.ceil(max / steps),
                scaleStartValue: 0
            };
            ctx = $("#myChart").get(0).getContext("2d");
            new Chart(ctx).Line(data, options);        
        }
       
        function controlChart() {
            
            var datasets = new Array();
            var max = 0;
            var thisHighest = 0;
            $('#optionLegend li').each(function() {
                if ($(this).hasClass("isvisible")) {
                    var reportType = $(this).attr('report');
                    if (reportType) {
                        datasets.push(window[reportType]);
                        thisHighest = Math.max.apply(Math, window[reportType].data);
                        if (thisHighest > max) {
                            max = thisHighest;
                        }
                    }
                }
            });
            
            var steps = 5;
            if (max < steps) {
                steps = max;
            }
            var options = {
                bezierCurve : false,
                scaleOverride: true,
                scaleSteps: steps,
                scaleStepWidth: Math.ceil(max / steps),
                scaleStartValue: 0
            };

            var data = {
                labels : chartLabels,
                datasets : datasets
            };
            ctx = $("#myChart").get(0).getContext("2d");
            new Chart(ctx).Line(data, options);  

        }


function scrollPage(position_id){
    
	if(!position_id){
		$position_id = '#resultsMap';
	}else {
		$position_id = position_id;
	}
	//jQuery('html,body').animate({scrollTop: jQuery($position_id).offset().top},'slow');
    jQuery('html, body').animate({ scrollTop: jQuery('#dashboard').offset().top }, 1500);
}
    </script>
	<script src="{{ asset('/front/js/Chart.js') }}"></script>		
@endsection