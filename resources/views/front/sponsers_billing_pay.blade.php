<?php
use App\Functions\Functions;
if (config('params.PAYMENT_FEATURE') != "on") { exit; }
    if ((config('params.CREDITCARDPAYMENT_FEATURE') != "on") && (config('params.INVOICEPAYMENT_FEATURE') != "on")) { exit; }


 $max_item_sum = 20;
$stop_payment = false;
?>
@extends('front')

@section('content')


	<link rel="stylesheet" href="{{ asset('front/css/bootstrap.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('front/css/members.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('front/css/structure.min.css') }}" />

    
	<link href="https://www.shoresummerrentals.com/custom/domain_1/theme/default/content_custom.min.css" rel="stylesheet" type="text/css" media="all">
    <!--link rel="stylesheet" href="{{ asset('front/css/bootstrap-responsive.min.css') }}" /-->
     
     
    
    
	<div class="payPage-container container members">

        <div class="container-fluid">

        <div class="row-fluid">
            
            <nav class="minor-nav">
                
                <ul>
                    <li>
                        <a class="active"  href="<?php echo url('sponsors/billing'); ?>">Check Out</a>
                    </li>
                    <li>
                        <a href="<?php echo url('sponsors/transactions'); ?>">Transaction History</a>
                    </li>
                </ul>
                
            </nav>
            
        </div>

    
    <div>

                        
        
            
	<h2 class="standardSubTitle">Billing Information</h2>

	<ul class="list-view">
		<li class="list-back">
			<a href="javascript:history.back(-1);">Back</a>
@if (isset($payment_message))
<p class="errorMessage">
{{ config('params.LANG_MSG_PROBLEMS_WERE_FOUND') }}:<br />
<?php echo $payment_message; ?>
</p>
@elseif ((!isset($bill_info["badges"])) && (!isset($bill_info["listings"]))  && (!isset($bill_info["events"]))  && (!isset($bill_info["banners"]))  && (!isset($bill_info["classifieds"])) && (!isset($bill_info["articles"])) && (!isset($bill_info["custominvoices"]))) 
<p class="informationMessage">
{{ config('params.LANG_MSG_NO_ITEMS_SELECTED_REQUIRING_PAYMENT') }}<br />
</p>
@else

			<h5 class="text-right"><a href="<?php echo url('advertise'); ?>" target="_blank" class="">View Membership Details</a></h5>   
			 <h5 class="text-right"><a href="<?php echo url('contact-us'); ?>" target="_blank" class="">Contact us to downgrade and/or for multiple property discounts</a></h5>
		</li>
	</ul>

@if (isset($bill_info["listings"])):    
<?php
if(count($bill_info["listings"]) > $max_item_sum){
 ?>
 <p class="errorMessage">You can only pay for {{ $max_item_sum }} each time Listing. <br /> Make the process again with less items.</p>
 <?php

        $arr_size = count($bill_info["listings"]);
        for($i=0; $i < $arr_size; $i++){
            $dump = array_pop($bill_info["listings"]);
        }

        $stop_payment = true;

    }
?>

<table class="standard-tableTOPBLUE" border="0" cellpadding="2" cellspacing="2">
            <tbody><tr>
                <th>Listing Name</th>
                <th width="100">Level</th>
                  <th width="120">Promotional Code</th>
                 <th width="100">Promo credit</th>
                <th width="70">Renewal</th>
                <th width="60">Cost</th>
            </tr>
   <?php foreach($bill_info["listings"] as $id => $info) { ?>

                   <tr>
            <td># {{ $id }} - {{ $info["address"] }}</td>                    
             <td>{{ Functions::string_ucwords($info["level"]) }}</td>

                <?php if (config('params.PAYMENT_FEATURE') == "on") { ?>
                            <?php if ((config('params.CREDITCARDPAYMENT_FEATURE') == "on") || (  config('params.INVOICEPAYMENT_FEATURE')== "on")) { ?>
                            
        <td style="text-align:center">{{ ($info["discount_id"]) ? $info["discount_id"] : 'N/A' }}</td>    
                            <?php } ?>
                        <?php } ?>  

                  <td >
                            <?php if($info["discount_type"] === 'monetary value'):?>
                                - {{ config('params.CURRENCY_SYMBOL').$info["discount_amount"] }}
                            <?php elseif($info["discount_type"] === 'percentage'):?>
                                <?php
                                $price = floatval($info["total_fee"])/((100-floatval($info["discount_amount"]))/100)-$info['total_fee'];
                                ?>
                                -{{config('params.CURRENCY_SYMBOL').number_format($price, 2) }}

                            <?php endif;?>
                        </td>
                    <td>{{ Functions::format_date($info["renewal_date"])}}</td>

                    <td>{{ config('params.CURRENCY_SYMBOL').$info["total_fee"] }}
                    </td>
                </tr>
      <?php } ?>          
                          
                    </tbody></table>
    @endif

@if (isset($bill_info["badges"])):
<?php
if(count($bill_info["badges"]) > $max_item_sum){
 ?>
 <p class="errorMessage">You can only pay for {{ $max_item_sum }} each time Badges. <br /> Make the process again with less items.</p>
 <?php

        $arr_size = count($bill_info["badges"]);
        for($i=0; $i < $arr_size; $i++){
            $dump = array_pop($bill_info["badges"]);
        }

        $stop_payment = true;

    }
?>
	
	
		<table class="standard-tableTOPBLUE" border="0" cellpadding="2" cellspacing="2">
			<tbody><tr>
				<th>Listing Specials</th>
                <th width="120">Promotional Code</th>
                <th width="100">Promo credit</th>
				<th width="70">Renewal</th>
				<th width="60">Cost</th>
			</tr>
   <?php foreach($bill_info["badges"] as $id => $info) { ?>

				<tr>
                <td># {{ $info["listing_id"] }} - {{ $info["name"] }}</td>                    
                    <td style="text-align:center">{{ ($info["discount_id"]) ? $info["discount_id"] : 'N/A' }}</td>
                   <td>
                            <?php if($info["discount_type"] === 'monetary value'):?>
                                - {{ config('params.CURRENCY_SYMBOL').$info["discount_amount"] }}
                            <?php elseif($info["discount_type"] === 'percentage'):?>
                                <?php
                                $price = floatval($info["total_fee"])/((100-floatval($info["discount_amount"]))/100)-$info['total_fee'];
                                ?>
                                -{{config('params.CURRENCY_SYMBOL').number_format($price, 2) }}

                            <?php endif;?>
                        </td>
                    <td>{{ Functions::format_date($info["renewal_date"])}}</td>
                    <td>{{ config('params.CURRENCY_SYMBOL').$info["total_fee"] }}</td>
				</tr>
       <?php } ?>   
               
					</tbody>
             </table>

@endif



    @if (isset($bill_info["custominvoices"])):

    <?php
if(count($bill_info["custominvoices"]) > $max_item_sum){
 ?>
 <p class="errorMessage">You can only pay for {{ $max_item_sum }} each time Custom Invoices. <br /> Make the process again with less items.</p>
 <?php

        $arr_size = count($bill_info["custominvoices"]);
        for($i=0; $i < $arr_size; $i++){
            $dump = array_pop($bill_info["custominvoices"]);
        }
        

        $stop_payment = true;

    }
?>


        <table class="standard-tableTOPBLUE" border="1" cellpadding="2" cellspacing="2">
            <tr>
                <th>Title</th>
                <th width="120">Items</th>
                <th width="70">Date</th>
                <th width="60">Amount</th>
            </tr>
   <?php foreach($bill_info["custominvoices"] as $id => $info) { ?>
        <tr>
                    <td>{{ Functions::system_showTruncatedText($info["title"], 35) }}</td>
                    <td><a href="<?php echo url('custominvoice_items/'.$info["id"]); ?>" class="link-table iframe fancy_window_custom" style="text-decoration: underline;">View</a></td>
                      <td>{{ Functions::format_date($info["date"])}}</td>
                    <td>{{ config('params.CURRENCY_SYMBOL').$info["subtotal"] }}</td>
                    
                </tr>

    
      <?php } ?>           
    @endif

    @if($bill_info["total_bill"]):

    

	<table class="standard-tableTOPBLUE levelTopdetail" border="1" cellpadding="2" cellspacing="2">
    @if ($payment_tax_status=='on' || $bill_info["tax_amount"] > 0) {
        <tr>
                        <th width="340" style="text-align:right">Subtotal Amount &nbsp;</th>
                        <td>
                            {{ config('params.CURRENCY_SYMBOL').$bill_info["total_bill"] }}
                        </td>
                    </tr>
                    <tr>
                        <th width="340" style="text-align:right">{{ $payment_tax_label."(".$bill_info["tax_amount"]."%)" }} &nbsp;</th>
                        <td>
                            {{ config('params.CURRENCY_SYMBOL'). Functions::payment_calculateTax($bill_info["total_bill"], $bill_info["tax_amount"], true, false) }}
                        </td>
                    </tr>
       @endif  
								<tbody><tr>
					<th width="340" style="text-align:right">Total Price &nbsp;</th>
			<td style="text-align: right; right: 10px; position:relative">{{ config('params.CURRENCY_SYMBOL').Functions::format_money($bill_info["amount"]) }}</td>
				</tr>
			</tbody>

            </table>
     @endif     
			
		<ul class="list-view">
			<li class="list-back"><a href="<?php echo url('sponsors/billing'); ?>">Back</a></li>
		</ul>

		
				<script language="javascript" type="text/javascript">
					<!--
					function submitOrder() {
						document.getElementById("authorizebutton").disabled = true;
						document.authorizeform.submit();
					}
					//-->
				</script>

                    <?php
        // && (config('params.INVOICEPAYMENT_FEATURE') == "on")  billing/invoice/'.$bill_info["invoice_number"]
                  
        if (($payment_method == "invoice")) {
            ?>
            <div class="text-center">
                <a href="<?php echo url('sponsors/billing/invoice/'.$bill_info["invoice_number"]); ?>" class="iframe fancy_window_invoice btn btn-info">Pay By Invoice</a>
            </div>
            <?php
        } else {
            $payment_process = "billing";

            ?>
            @include('front/form_billing_'.$payment_method)


        
       <?php } ?>				

	@endif 
        
    </div>


            </div><!-- Close container-fluid div -->
            
        </div>



		 <script type="text/javascript"> 
        
        if ($("#myChart").length) {
            //This will get the first returned node in the jQuery collection.
            var ctx = $("#myChart").get(0).getContext("2d");
        }

        function initializeDashboard() {
            $(".dial").knob({
                readOnly:   true,
                fgColor:    "#2980b9",
                bgColor:    "#DEE1E3",
                fontWeight: 300,
                thickness:  .2,
                width:      70,
                height:     70
            });

            $(".status, .floating-tip, .alert-new, #item_renewal").tooltip({
                animation: true,
                placement: "top"
            });
             
            if ($("#myChart").length) {
                //Get context with jQuery - using jQuery's .get() method.
                ctx = $("#myChart").get(0).getContext("2d");
                loadChart();
            }
        }

        $(function() {
            $("#alert").fadeOut(5000);
            initializeDashboard();
        });
        
        function showReply(id) {
            $('#review_reply'+id).css('display', '');
            $('#link_reply'+id).css('display', 'none');
            $('#cancel_reply'+id).css('display', '');
        }
        
        function hideReply(id) {
            $('#review_reply'+id).css('display', 'none');
            $('#link_reply'+id).css('display', '');
            $('#cancel_reply'+id).css('display', 'none');
        }
        
        function showLead(id) {
            $('#lead_reply'+id).css('display', '');
            $('#link_lead'+id).css('display', 'none');
            $('#cancel_lead'+id).css('display', '');
        }
        
        function hideLead(id) {
            $('#lead_reply'+id).css('display', 'none');
            $('#link_lead'+id).css('display', '');
            $('#cancel_lead'+id).css('display', 'none');
        }
        
        function saveReply(id) {
            $("#submitReply"+id).css("cursor", "default");
            $("#submitReply"+id).prop("disabled", "disabled");
            $("#submitReply"+id).html('Wait, Loading...');
            
            $.post("https://www.shoresummerrentals.com/sponsors/ajax.php", $("#formReply"+id).serialize(), function(data) {
                if (data == "ok") {
                    $("#msgReviewE"+id).css("display", "none");
                    $("#msgReviewS"+id).css("display", "");
                    $("#msgReviewS"+id).fadeOut(5000);
                } else {
                    $("#msgReviewE"+id).css("display", "");
                    $("#msgReviewS"+id).css("display", "none");
                }
                $("#submitReply"+id).html('Submit');
                $("#submitReply"+id).prop("disabled", "");
                $("#submitReply"+id).css("cursor", "pointer");
            });
        }
        
        function saveLead(id) {
            $("#submitLead"+id).css("cursor", "default");
            $("#submitLead"+id).prop("disabled", "disabled");
            $("#submitLead"+id).html('Wait, Loading...');
            
            $.post("https://www.shoresummerrentals.com/sponsors/ajax.php", $("#formLead"+id).serialize(), function(data) {
                if (data == "ok") {
                    $("#msgLeadE"+id).css("display", "none");
                    $("#msgLeadS"+id).css("display", "");
                    $("#msgLeadS"+id).fadeOut(5000);
                    setTimeout("leadBox('hide', "+id+");", 4000);
                    $("#title_replied"+id).css("display", "none");
                    $("#new_replied"+id).css("display", "");
                } else {
                    $("#msgLeadE"+id).html(data);
                    $("#msgLeadE"+id).css("display", "");
                    $("#msgLeadS"+id).css("display", "none");
                }
                $("#submitLead"+id).html('Submit');
                $("#submitLead"+id).prop("disabled", "");
                $("#submitLead"+id).css("cursor", "pointer");
            });
        }
        
        function reviewBox(option, id) {
            $("#reviews-list").children(".item-review").children(".review-detail").stop(true,true).slideUp();
            $("#reviews-list").children(".item-review").children(".review-summary").stop(true,true).slideDown().removeClass("new");
            if (option == "show") {
                $("#review-summary-"+id).slideUp(); 
                $("#review-detail-"+id).slideDown();
                setItemAsViewed("review", id);
            } else {
                $("#review-summary-"+id).slideDown(); 
                $("#review-detail-"+id).slideUp();
            }            
        }
        
        function leadBox(option, id) {
            $("#leads-list").children(".item-review").children(".review-detail").stop(true,true).slideUp();
            $("#leads-list").children(".item-review").children(".review-summary").stop(true,true).slideDown().removeClass("new");
            if (option == "show") {
                $("#lead-summary-"+id).slideUp(); 
                $("#lead-detail-"+id).slideDown();
                setItemAsViewed("lead", id);
            } else {
                $("#lead-summary-"+id).slideDown(); 
                $("#lead-detail-"+id).slideUp();
            }            
        }
        
        function dealBox(option, id) {
            $("#deals-list").children(".item-review").children(".review-detail").stop(true,true).slideUp();
            $("#deals-list").children(".item-review").children(".review-summary").stop(true,true).slideDown();
            if (option == "show") {
                $("#deal-summary-"+id).slideUp(); 
                $("#deal-detail-"+id).slideDown();
            } else {
                $("#deal-summary-"+id).slideDown(); 
                $("#deal-detail-"+id).slideUp();
            }            
        }
        
        function changeDealStatus(option, id, promocode) {
            $.post("https://www.shoresummerrentals.com/sponsors/deal/deal.php",{action: option, promotion_id: promocode}, function() {
                if (option == "freeUpDeal") {
                    $("#label_used"+id).css("display", "");
                } else {
                    $("#label_used"+id).css("display", "none");
                }
            });
        }
        
        function setItemAsViewed(type, id) {
            $.post("https://www.shoresummerrentals.com/sponsors/ajax.php", {
                ajax_type: 'setItemAsViewed',
                type: type,
                id: id
            }, function () {});
        }
       
        function loadDashboard(item_type, item_id) {
            $.post("<?php echo url('loadDashboard');?>", {
                ajax_type: 'load_dashboard',
                item_type: item_type,
                item_id: item_id,
                _token:'{{ csrf_token() }}'
            }, function (ret) {
                $(".webitem").removeClass("active");
                $("#"+item_type+"_"+item_id).addClass("active");
                scrollPage('#float_layer');
                $("#dashboard").hide().html(ret).fadeIn(800);
                initializeDashboard();
            });
        }

        function selectLegend(option, id, chartdata) {
            var countVisible = 0;
            
            if (option == "viewALL") {
                
                if ( $(".legend-ALL").hasClass("isvisible")) {
//                    $(".legend-ALL").removeClass("isvisible");
//                    $("#optionLegend > li > i").removeClass("checked");
//                    $("#optionLegend > li").removeClass("isvisible");
//                    $("#controlLegend > li").remove();
                } else {
                    countVisible = 2;
                    $("#optionLegend > li > i").addClass("checked");                   
                    $(".legend-ALL").addClass("isvisible");
                    $("#optionLegend > li").not(".isvisible").clone().appendTo("#controlLegend");
                    $("#optionLegend > li").addClass("isvisible");
                }
            } else {
                id: id
                chartdata: chartdata
                $newlegend = $(".legend-"+id).clone();

                if ($(".legend-"+id).hasClass("isvisible")) {
                    
                    //Check if there's at least one other legend selected to prevent empty chart
                    $('#optionLegend li').each(function() {
                        if ($(this).hasClass("isvisible")) {
                            countVisible++;
                        }
                    });

                    if (countVisible > 1) {
                        $(".legend-"+id).children("i").removeClass("checked");
                        $(".legend-"+id).removeClass("isvisible");
                        $("#controlLegend").children(".legend-"+id).remove();
                        $(".legend-ALL").children("i").removeClass("checked");
                        $(".legend-ALL").removeClass("isvisible");
                    }
                } else {
                    countVisible = 2;
                    $(".legend-"+id).children("i").addClass("checked");
                    $(".legend-"+id).addClass("isvisible");
                    $newlegend.appendTo("#controlLegend");
                }
            }
            if (countVisible > 1) {
                controlChart();
            }
        }
                
        function loadChart() {
            var data = {
                labels : chartLabels,
                datasets : initialReport
            };
            var steps = 5;
            var max = maxInitialReport;
            if (max < steps) {
                steps = max;
            }
            var options = {
                bezierCurve : false,
                scaleOverride: true,
                scaleSteps: steps,
                scaleStepWidth: Math.ceil(max / steps),
                scaleStartValue: 0
            };
            ctx = $("#myChart").get(0).getContext("2d");
            new Chart(ctx).Line(data, options);        
        }
       
        function controlChart() {
            
            var datasets = new Array();
            var max = 0;
            var thisHighest = 0;
            $('#optionLegend li').each(function() {
                if ($(this).hasClass("isvisible")) {
                    var reportType = $(this).attr('report');
                    if (reportType) {
                        datasets.push(window[reportType]);
                        thisHighest = Math.max.apply(Math, window[reportType].data);
                        if (thisHighest > max) {
                            max = thisHighest;
                        }
                    }
                }
            });
            
            var steps = 5;
            if (max < steps) {
                steps = max;
            }
            var options = {
                bezierCurve : false,
                scaleOverride: true,
                scaleSteps: steps,
                scaleStepWidth: Math.ceil(max / steps),
                scaleStartValue: 0
            };

            var data = {
                labels : chartLabels,
                datasets : datasets
            };
            ctx = $("#myChart").get(0).getContext("2d");
            new Chart(ctx).Line(data, options);  

        }


function scrollPage(position_id){
    
	if(!position_id){
		$position_id = '#resultsMap';
	}else {
		$position_id = position_id;
	}
	//jQuery('html,body').animate({scrollTop: jQuery($position_id).offset().top},'slow');
    jQuery('html, body').animate({ scrollTop: jQuery('#dashboard').offset().top }, 1500);
}
    </script>
	<script src="{{ asset('/front/js/Chart.js') }}"></script>		
@endsection