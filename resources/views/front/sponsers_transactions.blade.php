<?php
use App\Functions\Functions;
use App\Functions\ItemStatus;
?>
@extends('front')

@section('content')

<style type="text/css">
.standard-innerTable {
    width: 98% !important;
    margin: 1%;
}



.plus {
    width: 40px;
}
  </style>   

	<link rel="stylesheet" href="{{ asset('front/css/bootstrap.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('front/css/members.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('front/css/structure.min.css') }}" />
	<link href="{{ asset('front/css/content_custom.min.css') }}" rel="stylesheet" type="text/css" media="all">
    <!--link rel="stylesheet" href="{{ asset('front/css/bootstrap-responsive.min.css') }}" /-->
     
    
	 <div class="transactions-page-container container members">

        <div class="container-fluid">

        <div class="row-fluid">
            
              <nav class="minor-nav">
                
                <ul>
                    <li>
                        <a href="<?php echo url('sponsors/billing'); ?>">Check Out</a>
                    </li>
                    <li>
                        <a class="active" href="<?php echo url('sponsors/transactions'); ?>">Transaction History</a>
                    </li>
                </ul>
                
            </nav>
            
        </div>

    
    <div class="members">

            <script type="text/javascript">
        function JS_openDetail(type, id) {
            document.getElementById(type+'info_'+id).style.display = '';
            document.getElementById(type+'img_'+id).innerHTML = '<img style="cursor: pointer; cursor: hand;" src="<?php echo url('images/img_close.gif'); ?>" onclick="JS_closeDetail(\''+type+'\', '+id+');" />'
        }

        function JS_closeDetail(type, id) {
            document.getElementById(type+'info_'+id).style.display = 'none';
            document.getElementById(type+'img_'+id).innerHTML = '<img style="cursor: pointer; cursor: hand;" src="<?php echo url('images/img_open.gif'); ?>" onclick="JS_openDetail(\''+type+'\', '+id+');" />'
        }
    </script>

    <table class="table-itemlist">

        <tbody><tr>
            <th style="width:20px">&nbsp;</th>
            <th>Id</th>
            <th>System</th>
            <th>Status</th>
            <th>Date</th>
            <th>Subtotal</th>
            <th>Tax</th>
            <th>Amount</th>
            <th style="width:72px">Options</th>
        </tr>
    <?php if ($transactions) 
     foreach($transactions as $transaction) 
     {  
      $id = $transaction->id;
            $str_time = Functions::format_getTimeString($transaction->transaction_datetime);
         
          if (config("params.LANG_LABEL_".$transaction->transaction_status)) {
                $labelStatus = Functions::string_strtoupper(config("params.LANG_LABEL_".$transaction->transaction_status));
            } else {
                $labelStatus = $transaction->transaction_status;
            }
     ?>  

       <tr>
                <td class="plus">
                    <div id="transactionimg_{{ $id }}">
                        <img style="cursor: pointer; cursor: hand;" src="<?php echo url('images/img_open.gif'); ?>" onclick="JS_openDetail('transaction', '{{ $id }}');" />
                    </div>
                </td>

                <td>
                    <span title="{{ $id }}" style="cursor:default">{{ $id }}</span>
                </td>
                
                <td>
                    <?php
                    if (($transaction->system_type != "simplepay") && ($transaction->system_type != "paypal") && ($transaction->system_type != "manual") && ($transaction->system_type != "pagseguro")) {
                        $type_field = Functions::system_showText(config('params.LANG_CREDITCARD'));
                    } else {
                        $type_field = ucfirst($transaction->system_type);
                    }
                    ?>
                    <span title="{{ $type_field }}" style="cursor:default">{{ $type_field }}</span>
                </td>

                <td>
                    <span title="{{ $labelStatus }}" style="cursor:default">{{ $labelStatus }}</span>
                </td>

                <td>
                    <span title="{{ Functions::format_date($transaction->transaction_datetime, config('params.DEFAULT_DATE_FORMAT'), 'datetime').' - '.$str_time }}" style="cursor:default">{{ Functions::format_date($transaction->transaction_datetime, config('params.DEFAULT_DATE_FORMAT'), 'datetime').' - '.$str_time }}</span>
                </td>

                <td>
                    <?php
                    if ($transaction->transaction_subtotal > 0) $subtotal_field = $transaction->transaction_subtotal." (".$transaction->transaction_currency.")";
                    else $subtotal_field = "0.00 (".$transaction->transaction_currency.")";
                    ?>
                    <span title="{{ $subtotal_field }}" style="cursor:default">{{ $subtotal_field }}</span>
                </td>

                <td>
                    <?php
                    if ($transaction->transaction_tax > 0) $tax_field = Functions::payment_calculateTax($subtotal_field,$transaction->transaction_tax , true, false)." (".$transaction->transaction_currency.")";
                    else $tax_field = "0.00 (".$transaction->transaction_currency.")";
                    ?>
                    <span title="{{ $tax_field }}" style="cursor:default">{{ $tax_field }}</span>
                </td>

                <td>
                    <?php
                    if ($transaction->transaction_amount > 0) $amount_field = $transaction->transaction_amount." (".$transaction->transaction_currency.")";
                    else $amount_field = "0.00 (".$transaction->transaction_currency.")";
                    ?>
                    <span title="{{ $amount_field }}" style="cursor:default">{{ $amount_field }}</span>
                </td>

               <td nowrap="" class="main-options">
                <a class="table-itemlist-view" href="{{ url('sponsors/transactions/view_transaction/'.$id) }}">View</a>
            </td>
            </tr>

            <tr id="transactioninfo_{{ $id }}" style="display:none;">
                <td colspan="10">
                @include('front/view_transaction_summary_info')
                  <?php //include (INCLUDES_DIR."/views/view_transaction_summary_info.php")?>
                </td>
            </tr>

            <tr></tr>
        
     

     <?php  } ?>   


     <?php if ($invoices) 
     foreach($invoices as $invoice) {   
            //$invoiceStatusObj = new InvoiceStatus();
            $id = $invoice->id;
            $str_time    = Functions::format_getTimeString($invoice->date);
            $account_id  = $invoice->account_id;
            $username    = $invoice->username;
            $id          = $invoice->id;
            $ip          = $invoice->ip;
            $date        = Functions::format_date($invoice->date,config('params.DEFAULT_DATE_FORMAT'), "datetime")." - ".$str_time;
            $status      = Functions::check_status($invoice->status);
            $amount      = $invoice->amount;
            $subtotal    = $invoice->subtotal_amount;
            $tax         = $invoice->tax_amount;
            $expire_date =Functions::format_date($invoice->date,config('params.DEFAULT_DATE_FORMAT'), "date");
            $valTax      = Functions::payment_calculateTax($subtotal,$tax,true,false);  

        ?>
        <tr>
            <td class="plus">
              <div id="invoiceimg_{{ $id }}" class="table-itemlist-img">
                    <img style="cursor: pointer; cursor: hand;" src="<?php echo url('images/img_open.gif'); ?>" onclick="JS_openDetail('invoice', '{{ $id }}');">
                </div>
            </td>

            <td>
                <span title="{{ $id }}" style="cursor:default">{{ $id }}</span>
            </td>
            
            <td>Invoice</td>

            <td>
                <a title="{{ $invoice->status }}" class="link-table"><?php echo $status; ?></a>
            </td>

            <td>
                <span title="{{ $date }}" style="cursor:default">{{ $date }}</span>
            </td>

            <td>
                <span title="{{ $subtotal }} ({{ $invoice->currency }})" style="cursor:default">{{ $subtotal }} ({{ $invoice->currency }})</span>
            </td>

            <td>
                <span title="{{ $valTax }} ({{ $invoice->currency }})" style="cursor:default">{{ $valTax }} ({{ $invoice->currency }})</span>
            </td>

            <td>
                <span title="{{ $amount }} ({{ $invoice->currency }})" style="cursor:default">{{ $amount }} ({{ $invoice->currency }})</span>
            </td>

            <td nowrap="" class="main-options">
                <a class="table-itemlist-view" href="{{ url('sponsors/transactions/view_invoice/'.$id) }}">View</a>
                <b>|</b>
                <a class="table-itemlist-print iframe fancy_window_invoice" href="{{  url('sponsors/billing/invoice/'.$id) }}" class="">
                    Print                </a>
            </td>
        </tr>

          <tr id="invoiceinfo_{{ $id }}" style="display:none; background-color:white;"> 

             <td colspan="9">

             @include('front/view_invoice_summary_info')     

             </td>

          </tr>

          <tr></tr>

        <?php } ?>

        
     


        <tr></tr>

        
     

        
    </tbody></table>

    </div>


            </div><!-- Close container-fluid div -->
             
        </div>



		 <script type="text/javascript"> 
        


function scrollPage(position_id){
    
	if(!position_id){
		$position_id = '#resultsMap';
	}else {
		$position_id = position_id;
	}
	//jQuery('html,body').animate({scrollTop: jQuery($position_id).offset().top},'slow');
    jQuery('html, body').animate({ scrollTop: jQuery('#dashboard').offset().top }, 1500);
}
    </script>
	<script src="{{ asset('/front/js/Chart.js') }}"></script>		
@endsection