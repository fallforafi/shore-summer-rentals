@extends('login_signup')

@section('content')


<?php
$required='required';
$required="";
?>
<section id="form"><!--form-->
		<div class="container">
			<div class="row">
				<div class="login-fom col-sm-5 col-sm-offset-1">
					<div class="login-form"><!--login form-->
						<h2>New User Signup!</h2>
						
						@if (count($errors->signup) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->signup->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
						@endif
						
						@if (Session::has('success'))
						<div class="alert alert-success">
							<h4><i class="icon fa fa-check"></i> &nbsp  {!! Session('success') !!}</h4>
						</div>
						@endif
						  {!! Form::open(array( 'class' => 'form','url' => 'signUpPost', 'name' => 'register')) !!}
     	                  {!! Form::text('firstName', Request::input('firstName') , array('placeholder'=>"First Name",'class' => 'form-control',$required) ) !!}
                          
                            {!! Form::text('lastName', null , array('placeholder'=>"Last Name",'class' => 'form-control',$required) ) !!}
						    {!! Form::text('email', null , array('placeholder'=>"Email",'class' => 'form-control',$required) ) !!}
							{!! Form::password('password', array('placeholder'=>"Password",'class' => 'form-control',$required) ) !!}
                            {!! Form::password('password_confirmation', array(
                                
                                'data-match-error'=>"Whoops, these don't match",
                                'placeholder'=>"Confirm Password",
                                "data-match"=>"#password",
                                'class' => 'form-control',$required))
                            !!}
        								
                            <select name="country" id="country" <?php echo $required;?> class="form-control">
    							<option >Country</option>
    							@foreach ($country as $ctry)
    								<option value="{{ $ctry->id }}">{{ $ctry->name }}</option>
    							@endforeach
            				</select>
                            
                            {!! Form::text('phone', null , array('placeholder'=>"Phone",'class' => 'form-control',$required) ) !!}
                            
							{!! Form::text('state', null , array('placeholder'=>"State",'class' => 'form-control',$required) ) !!}
                            {!! Form::text('city', null , array('placeholder'=>"City",'class' => 'form-control',$required) ) !!}
                            {!! Form::text('phone', null , array('placeholder'=>"Phone",'class' => 'form-control',$required) ) !!}
							{!! Form::text('zip', null , array('placeholder'=>"Zip",'class' => 'form-control',$required) ) !!}
                            {!! Form::text('address', null , array('placeholder'=>"Address",'class' => 'form-control',$required) ) !!}
                            
						    {!! Form::text('company', null , array('placeholder'=>"Company",'class' => 'form-control',$required) ) !!}
                            {!! Form::text('website', 'http://' , array('placeholder'=>"Website",'class' => 'form-control',$required) ) !!}
                        </div>
							<button type="submit" class="btn btn-default">Signup</button>
						</form>
					</div><!--/login form-->
				<div class="or-area col-sm-1">
					<h2 class="or">OR</h2>
				</div>
				<div class="login-fom col-sm-5 ">
					<div class="login-form"><!--sign up form-->
						<h2>Login to your account</h2>
						@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
						@endif
						<form method="POST" action="{{ url('postLogin') }}">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<input type="email" name="email" placeholder="Email Address" required />
							<input type="password" placeholder="Password" name="password" required  />
							<span>
								<input type="checkbox" class="checkbox" name="remember"> Remember Me
							</span>
							<button type="submit" class="btn btn-default">Login</button>
						</form>
					</div><!--/sign up form-->
				</div>
                	</div> 
			</div>
		</div>
</section><!--/form-->
					
@endsection