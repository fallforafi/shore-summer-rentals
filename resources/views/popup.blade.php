<!DOCTYPE html>
<html lang="en">
<head>

<meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<!--iPhone from zooming form issue-->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
    <!--<meta name="viewport" content="width=device-width, initial-scale=1">-->
    <title><?php echo isset($headerTitle)?$headerTitle:url();  ?></title>
    
    <meta name="author" content="<?php echo isset($headerAuthor)?$headerAuthor:''; ?>" />
    <meta name="description" content="<?php echo isset($headerDescription)?$headerDescription:'ShoreSummerRentals.com is the premier resource for finding & advertising vacation property rentals. Specializing in Ocean City NJ rentals, LBI rentals, Sea Isle City rentals, Cape May rentals, Wildwood rentals and Jersey Shore Rentals. View now! '; ?>" />
    <meta name="keywords" content="<?php echo isset($headerKeywords)?$headerKeywords:'ocean city nj rentals, lbi rentals, sea isle city rentals, cape may rentals, wildwood rentals, jersey shore rentals'; ?> " />
    
    <?php 
    if(isset($header_searching_tag) && !empty($header_searching_tag))
    {
        foreach($header_searching_tag as $val)
        {
            echo $val->value;
        }
    }
    ?>
    <link href="{{ asset('front/css/bootstrap.min.css') }}" rel="stylesheet">
	<link rel="icon" type="image/png" href="{{ asset('front/images/favicon.png') }}" /> 
    <link rel="stylesheet" href="{{ asset('front/style.css') }}" />
	<link rel="stylesheet" href="{{ asset('front/css/animate.css') }}" />
	<link rel="stylesheet" href="{{ asset('front/css/slidenav.css') }}" />
	<link rel="stylesheet" href="{{ asset('front/css/font-awesome.min.css') }}" />
	<link rel="stylesheet" href="{{ asset('front/css/foundation-icons.css') }}" />
	<link rel="stylesheet" href="{{ asset('front/css/swiper.min.css') }}" />
<link rel="stylesheet" href="{{ asset('front/css/popup.css') }}" />
    <link rel="stylesheet" href="{{ asset('front/css/popup.min.css') }}" />


<link rel="stylesheet" href="{{ asset('front/scripts/fancybox/v2/jquery.fancybox.css') }}" />  
 <link rel="stylesheet" href="{{ asset('front/scripts/jcrop/css/jquery.Jcrop.css') }}" type="text/css"/>

<style type="text/css">
    .fancybox-inner {
    height: 600px !important;
}
.modal-content>h2 {
    margin-bottom: 5px;
}
</style>

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
        <script type="text/javascript" src="https://www.shoresummerrentals.com/scripts/front/jquery-1.8.3.min.js"></script>
     <script type="text/javascript" src="{{ asset('front/scripts/jcrop/js/jquery.Jcrop.js') }}"></script>
     <script  type="text/javascript"  src="{{ asset('front/scripts/fancybox/v2/jquery.fancybox.pack.js') }}"></script>
  


    <script src="{{ asset('front/js/css_browser_selector.js') }}" type="text/javascript"></script>
	
	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>-->
    <!-- Include all compiled plugins (below), or include individual files as needed -->
	<script src="{{ asset('front/scripts/jquery/jquery_ui/js/jquery-ui-1.7.2.custom.min.js') }}" language="javascript" type="text/javascript"></script>
    <link rel="stylesheet" href="{{ asset('front/scripts/jquery/jquery_ui/css/jquery-ui-1.7.2.custom.css') }}" type="text/css">
    <script src="{{ asset('front/js/bootstrap-datepicker.js') }}"></script>
	<script>
		$('.input-daterange input').each(function() {
			$(this).datepicker("clearDates");
		});
	</script>   

    <script type="text/javascript">
                $(document).ready(function() {
                    <?php
                    if ( config('params.DEFAULT_DATE_FORMAT') == "m/d/Y" ) $date_format = "mm/dd/yy";
                    elseif ( config('params.DEFAULT_DATE_FORMAT') == "d/m/Y" ) $date_format = "dd/mm/yy";
                    ?>
                    $("#arrive").datepicker({
                        minDate: 0,
                        onSelect: function(dateText, inst) {
                            var actualDate = new Date(dateText);
                            var newDate = new Date(actualDate.getFullYear(), actualDate.getMonth(), actualDate.getDate()+1);
                            $("#depart").datepicker("option", "minDate", newDate);
                        },
                        onClose: function(dateText, inst) {
                            setTimeout(function() {
                                $("#depart").focus();
                            }, 200);
                        }
                    });

                    $("#depart").datepicker();
                });
            </script> 
</head>
<body >


    <section id="page-content">
            @yield('content')

    
	</section>
    
    
<script src="{{ asset('/front/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('/front/js/viewportchecker.js') }}"></script>


<!--script src="{{ asset('/front/js/swiper.jquery.min.js') }}"></script>

<script>
	var swiper = new Swiper('.swiper-container', {
		pagination: '.swiper-pagination',
		slidesPerView: '7',
		centeredSlides: false,
		paginationClickable: true,
		nextButton: '.swiper-button-next',
		prevButton: '.swiper-button-prev',
		spaceBetween: 15,
		autoplay: 2500,
		autoplayDisableOnInteraction: false,
		breakpoints: {
        1024: {
            slidesPerView: 4,
            spaceBetween: 40
        },
        768: {
            slidesPerView: 3,
            spaceBetween: 30
        },
        640: {
            slidesPerView: 2,
            spaceBetween: 20
        },
        320: {
            slidesPerView: 1,
            spaceBetween: 10
        }
    }
	});
</script-->
</body>
</html>