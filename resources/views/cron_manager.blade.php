#!/usr/bin/php -q
<?php
use App\Functions\Functions;
use App\Models\Domain\Sql;
	# ----------------------------------------------------------------------------------------------------
	# * FILE: cron_manager.php
	# ----------------------------------------------------------------------------------------------------
	////////////////////////////////////////////////////////////////////////////////////////////////////
	ini_set("html_errors", FALSE);
	////////////////////////////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////////////////////////
	define("BLOCK",1000);

	$path = "";
	$full_name = "";
	$file_name = "";
	$full_name = $_SERVER["SCRIPT_FILENAME"];
	if (strlen($full_name) > 0) {
		$osslash = ((strtoupper(substr(PHP_OS, 0, 3)) == 'WIN') ? '\\' : '/');
		$file_pos = strpos($full_name, $osslash."cron".$osslash);
		if ($file_pos !== false) {
			$file_name = substr($full_name, $file_pos);
		}
		$path = substr($full_name, 0, (strlen($file_name)*(-1)));
	}
	if (strlen($path) == 0) $path = url();
	define("EDIRECTORY_ROOT", $path);

	define("BIN_PATH", EDIRECTORY_ROOT."/bin");
	//////////////////////////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////////////////////////
	$_inCron = true;

	////////////////////////////////////////////////////////////////////////////////////////////////////
	function getmicrotime() {
		list($usec, $sec) = explode(" ", microtime());
		return ((float)$usec + (float)$sec);
	}
	$time_start = getmicrotime();

	/**
	 * Files to Cron manager
	 */
	//$files[] = "daily_maintenance.php";
	//$files[] = "email_traffic.php";
	//$files[] = "renewal_reminder.php";
	//$files[] = "statisticreport.php";
	//$files[] = "randomizer.php";
	//$files[] = "location_update.php";
	//$files[] = "report_rollup.php";
	//$files[] = "sitemap.php";
	$files[] = "export_listings.php";

	
	/*
	 * Save information about cron running
	 	
	$files[] = "export_events.php";
	$files[] = "export_mailapp.php";
	$files[] = "rollback_import.php";
	$files[] = "rollback_import_events.php";
	$files[] = "count_locations.php";
	 */

	/*
	$host = _DIRECTORYDB_HOST;
	$db   = _DIRECTORYDB_NAME;
	$user = _DIRECTORYDB_USER;
	$pass = _DIRECTORYDB_PASS;
	$link = mysql_connect($host, $user, $pass);
	mysql_query("SET NAMES 'utf8'", $link);
	mysql_query('SET character_set_connection=utf8', $link);
	mysql_query('SET character_set_client=utf8', $link);
	mysql_query('SET character_set_results=utf8', $link);
	mysql_select_db($db);*/
	
	$sql = "SELECT value FROM Setting WHERE name = 'running_cron_manager'";
	$result	= Sql::fetchMain($sql);
	
	if (count($result)>0) {
        
        if ($result[0]->value == "n") {
            
            $sql_update = "UPDATE Setting SET value = 'n' WHERE name = 'running_cron_manager'";
            Sql::updateSqlMain($sql_update);


            for ($i=0;$i<count($files);$i++) {

            	include($files[$i]);


               /* if (is_file(EDIRECTORY_ROOT."/".$files[$i])) {

                    system("php -f ".EDIRECTORY_ROOT."/".$files[$i]);
                }*/

            }

            $sql_update = "UPDATE Setting SET value = 'n' WHERE name = 'running_cron_manager'";
            Sql::updateSqlMain($sql_update);
        }
		
	} else {
        $sql = "INSERT INTO Setting (name, value) VALUES ('running_cron_manager', 'n');";
            Sql::insertSqlMain($sql_update);
    }
	////////////////////////////////////////////////////////////////////////////////////////////////////
	$time_end = getmicrotime();
	$time = $time_end - $time_start;
	print "Cron Manager - ".date("Y-m-d H:i:s")." - ".round($time, 2)." seconds.\n";
?>