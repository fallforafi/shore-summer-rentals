 <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

      <!-- Sidebar user panel (optional) -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="{{ asset('adminlte/dist/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
        
          <p></p>
          <!-- Status -->
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>

      <!-- search form (Optional) -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->

      <!-- Sidebar Menu -->
      <ul class="sidebar-menu">
        <li class="header">Menu Bar</li>
		
		
        <!-- Optionally, you can add icons to the links -->
		<!-- Admin Links Start-->
		<?php  /* if(Auth::user()->role->role == 'admin') { ?>
        <li class="active"><a href="{{ url('/clients') }}"><i class="fa fa-user"></i> <span>Users</span></a></li>
		<?php }  */ ?>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Categories</span> <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu" style="display: none;">
            <li><a href="{{ url('admin/categories') }}"><i class="fa fa-circle-o"></i> List Categories</a></li>
            <li><a href="{{ url('admin/categories/create') }}"><i class="fa fa-circle-o"></i> Add Category</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Products</span> <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu" style="display: none;">
            <li><a href="{{ url('admin/products') }}"><i class="fa fa-circle-o"></i> List Products</a></li>
            <li><a href="{{ url('admin/products/create') }}"><i class="fa fa-circle-o"></i> Add Products</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Products Attributes</span> <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu" style="display: none;">
            <li><a href="{{ url('admin/attributes') }}"><i class="fa fa-circle-o"></i> List Attributes</a></li>
            <li><a href="{{ url('admin/attributes/create') }}"><i class="fa fa-circle-o"></i> Add Attribute</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Content Blocks</span> <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu" style="display: none;">
            <li><a href="{{ url('admin/content') }}"><i class="fa fa-circle-o"></i> List Content Blocks</a></li>
            <li><a href="{{ url('admin/content/create') }}"><i class="fa fa-circle-o"></i> Add New Content Blocks</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Orders</span> <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu" style="display: none;">
            <li><a href="{{ url('admin/orders') }}"><i class="fa fa-circle-o"></i> All Orders</a></li>
          </ul>
        </li>
		
		<li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Guest Book</span> <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu" style="display: none;">
            <li><a href="{{ url('admin/guestbook') }}"><i class="fa fa-circle-o"></i> All Messages</a></li>
          </ul>
        </li>
		
		<li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Contact Us</span> <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu" style="display: none;">
            <li><a href="{{ url('admin/contactus') }}"><i class="fa fa-circle-o"></i> All contacts</a></li>
          </ul>
        </li>
        
        
        
		<!-- Admin Links End-->
		
		<!-- Client Links Start-->
		<?php /* if(Auth::user()->role->role != 'admin') { ?>
        <li><a href="{{ url('/profile') }}"><i class="fa fa-book"></i> <span>Profile</span></a></li>
			
			<!-- Check if cafe already added Not : Cafe will be added once only -->
			<?php if(count(Auth::user()->cafe) == 0) { ?>
			<li><a href="{{ url('/create_cafe') }}"><i class="fa fa-beer"></i> <span>Add Your Cafe</span></a></li>
			<?php }else{  ?>
			<li><a href="{{ url('/my_cafe') }}"><i class="fa fa-beer"></i> <span>Your Cafe</span></a></li>
			<?php }  ?>
			
		<?php }  */ ?>
		<!-- Client Links End-->
      </ul>
      <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
  </aside>