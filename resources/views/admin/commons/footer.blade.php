<!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
      Cafe-Web
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2016 <a href="#">Cafe-Web</a>.</strong> All rights reserved.
  </footer>