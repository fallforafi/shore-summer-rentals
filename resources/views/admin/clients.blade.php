@extends('admin/admin_template')

@section('content')
 
 <hr>
	<div class="box box-primary">
       <div class="box-header with-border">
			<div class="box-body">
				<div class="col-md-12">
					<h1>Clients</h1>
					<br>
						<table class="table table-striped table-bordered table-hover">
							 <thead>
							 <tr class="bg-info">
								 <th>#</th>
								 <th>Name</th>
								 <th>Email</th>
								 <th>Registration Date</th>
								 
							 </tr>
							 </thead>
							 <tbody>
							 <?php $i=1; ?>
							 @foreach ($data as $row)
								 <tr>
									 <td><?php echo $i; ?></td>
									 <td><?php  echo $row->name; ?></td>
									 <td><?php  echo $row->email; ?></td>
									 <td><?php  echo date("d M Y",strtotime($row->created_at)); ?></td>
									 
								 </tr>
							<?php $i++; ?>
							 @endforeach

							 </tbody>

						</table>
				</div>
			</div>
		</div>
	</div>
@endsection

<script>
	  
	   function delete_category(value){
		  var r = confirm("Sure to Delete this Category ?");
			if (r == true) {
				window.location ='delete_category/'+value;
			} 
			else 
			{   
			}
	  }
	  
</script>