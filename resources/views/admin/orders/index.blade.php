@extends('admin/admin_template')

@section('content')

<?php $currency = Config::get('params.currency'); ?>
<!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <div class="col-md-12">
  <!-- PRODUCT LIST -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">( Total : {{ count($orders) }} ) </h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <ul class="products-list product-list-in-box">
				
				<table class="table">
            	<thead>
                	<tr>
                        <td>Order Id</td>
                    	<td>Name</td>
                        <td>Email</td>
                        <td>Added</td>
                        <td>Status</td>
                        <td>Total</td>
                        <td></td>                                    
                    </tr>
                </thead>
                <tbody>
                @foreach($orders as $order)
                	<tr>
                    <td>{{$order->id}}</td>
                    	<td>{{ $order->billingFirstName.' '.$order->billingLastName }}</td>
                        <td>{{ $order->email}}</td>
                        <td>{{ date('d M Y',strtotime($order->created_at))}}</td>
                        <td>{{ ucfirst($order->orderStatus)}}</td>
                        <td>{{ $currency[Config::get('params.currency_default')]['symbol']}}{{ $order->grandTotal}}</td>
                        <td><a href="order/{{$order->id}}">Detail</a></td>                                    
                    </tr>
                @endforeach 
                </tbody>
            </table>
            
              </ul>
            </div>
            <!-- /.box-body -->
            <div class="box-footer text-center">
              <a href="javascript::;" class="uppercase"></a>
            </div>
            <!-- /.box-footer -->
          </div>
	</div>
    <!-- /.col -->
</div>
<!-- /.row -->	

@endsection