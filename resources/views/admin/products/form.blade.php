<?php
//d($errors,1);
$required="required";
$required="";
?>
@include('admin/commons/errors')
<div class="form-group">
    {!! Form::label('name') !!}
    {!! Form::text('name', null , array('class' => 'form-control',$required) ) !!}
</div>
<div class="form-group">
    {!! Form::label('category') !!}
    {!! Form::select('category_id', $categories,null,['class' => 'form-control',$required]) !!}
</div>

<div class="form-group">
    {!! Form::label('teaser') !!}
    {!! Form::textarea('teaser', null, ['size' => '105x3','class' => 'form-control',$required]) !!} 
</div>

<div class="form-group">
    {!! Form::label('description') !!}
    {!! Form::textarea('description', null, ['size' => '105x3','class' => 'form-control',$required]) !!} 
</div>

<div class="form-group">
    {!! Form::label('url') !!}
    {!! Form::text('url', null , array('class' => 'form-control',$required) ) !!}
</div>

<div class="form-group">
    {!! Form::label('price') !!}
    {!! Form::text('price', null , array('class' => 'form-control',$required) ) !!}
</div>

<div class="form-group">
    {!! Form::checkbox('sale',1,false,['id'=>'sale']); !!}
    Sale This Product. 
</div>

<div class="form-group">
    {!! Form::label('Sale Price') !!}
    {!! Form::text('salePrice', null , array('class' => 'form-control',$required) ) !!}
</div>

<div class="form-group">
    {!! Form::label('keywords') !!}
    {!! Form::text('keywords', null , array('class' => 'form-control',$required) ) !!}
</div>

<div class="form-group">
    {!! Form::label('attibutes') !!}
    {!!Form::select('attibutes',$attributes,$productAttributes,array('class' => 'form-control',$required,'multiple'=>'multiple','name'=>'attributes[]'))!!}
</div>
<div class="form-group">
    {!! Form::label('image') !!}
    {!! Form::file('image', null,array($required,'class'=>'form-control')) !!}
</div>
<div class="form-group">
<button type="submit" class="btn btn-primary btn-block btn-flat">Save</button>
</div>