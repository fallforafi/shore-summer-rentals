<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<!--iPhone from zooming form issue-->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
    <!--<meta name="viewport" content="width=device-width, initial-scale=1">-->
   <?php 
      if(isset($listing_title)) {
   ?>

    <title><?php echo $listing_title;  ?></title>
    
    <meta name="author" content="<?php echo isset($header_author[0]->value)?$header_author[0]->value:''; ?>" />
    <meta name="description" content="<?php echo isset($meta_data[0]->description)?$meta_data[0]->description:'ShoreSummerRentals.com is the premier resource for finding & advertising vacation property rentals. Specializing in Ocean City NJ rentals, LBI rentals, Sea Isle City rentals, Cape May rentals, Wildwood rentals and Jersey Shore Rentals. View now! '; ?>" />
    <meta name="keywords" content="<?php echo isset($meta_data[0]->keywords)?$meta_data[0]->keywords:'ocean city nj rentals, lbi rentals, sea isle city rentals, cape may rentals, wildwood rentals, jersey shore rentals'; ?> " />


    <?php  } ?>
    
    <?php if(Session::get('SESS_ACCOUNT_ID') && Session::get('SESS_LOGIN')) {
        ?>
         <title><?php echo isset($headerTitle)?$headerTitle:url();  ?></title>
    
    <meta name="author" content="<?php echo isset($headerAuthor)?$headerAuthor:''; ?>" />
    <meta name="description" content="<?php echo isset($headerDescription)?$headerDescription:'ShoreSummerRentals.com is the premier resource for finding & advertising vacation property rentals. Specializing in Ocean City NJ rentals, LBI rentals, Sea Isle City rentals, Cape May rentals, Wildwood rentals and Jersey Shore Rentals. View now! '; ?>" />
    <meta name="keywords" content="<?php echo isset($headerKeywords)?$headerKeywords:'ocean city nj rentals, lbi rentals, sea isle city rentals, cape may rentals, wildwood rentals, jersey shore rentals'; ?> " />
    
   <?php }else
    { ?>
       <title><?php echo isset($meta_data[0]->title)?$meta_data[0]->title:'Birthday Shirts';  ?></title>
    
    <meta name="author" content="<?php echo isset($header_author[0]->value)?$header_author[0]->value:''; ?>" />
    <meta name="description" content="<?php echo isset($meta_data[0]->description)?$meta_data[0]->description:'ShoreSummerRentals.com is the premier resource for finding & advertising vacation property rentals. Specializing in Ocean City NJ rentals, LBI rentals, Sea Isle City rentals, Cape May rentals, Wildwood rentals and Jersey Shore Rentals. View now! '; ?>" />
    <meta name="keywords" content="<?php echo isset($meta_data[0]->keywords)?$meta_data[0]->keywords:'ocean city nj rentals, lbi rentals, sea isle city rentals, cape may rentals, wildwood rentals, jersey shore rentals'; ?> " />
     
   <?php  }
   ?>


    
   
    <?php 
    if(isset($header_searching_tag) && !empty($header_searching_tag))
    {
        foreach($header_searching_tag as $val)
        {
            echo $val->value;
        }
    }
    ?>
    <link href="{{ asset('front/css/bootstrap.min.css') }}" rel="stylesheet">
	<link rel="icon" type="image/png" href="{{ asset('front/images/favicon.png') }}" /> 
    <link rel="stylesheet" href="{{ asset('front/style.css') }}" />
	<link rel="stylesheet" href="{{ asset('front/css/animate.css') }}" />
	<link rel="stylesheet" href="{{ asset('front/css/slidenav.css') }}" />
	<link rel="stylesheet" href="{{ asset('front/css/font-awesome.min.css') }}" />
	<link rel="stylesheet" href="{{ asset('front/css/foundation-icons.css') }}" />
	<link rel="stylesheet" href="{{ asset('front/css/swiper.min.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('front/scripts/fancybox/v2/jquery.fancybox.css') }}" /> 
<link rel="stylesheet" type="text/css" href="{{ asset('front/scripts/fancybox/v2/jquery.fancybox.min.css') }}" />  

 <link rel="stylesheet" href="{{ asset('front/scripts/jcrop/css/jquery.Jcrop.css') }}" type="text/css"/>

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <script src="{{ asset('front/js/css_browser_selector.js') }}" type="text/javascript"></script>
    <script type="text/javascript" src="{{ asset('front/scripts/jquery-1.8.3.min.js') }}"></script>
     <script type="text/javascript" src="{{ asset('front/scripts/jcrop/js/jquery.Jcrop.js') }}"></script>
     <script  type="text/javascript"  src="{{ asset('front/scripts/fancybox/v2/jquery.fancybox.pack.js') }}"></script>

   

	<script src="{{ asset('/front/js/kodeized.js') }}"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
	<script src="{{ asset('front/js/bootstrap-datepicker.js') }}"></script>
	<script>
		$('.input-daterange input').each(function() {
			$(this).datepicker("clearDates");
		});
	</script>
<style type="text/css">
.billing-page .fancybox-inner {
    height: 600px !important;
}
.view_invoice-page .fancybox-inner {
    height: 600px !important;
}
.transactions-page .fancybox-inner {
    height: 600px !important;
}


.modal-content>h2 {
    margin-bottom: 5px;
}
</style>

 <script type="text/javascript">
            
            $(function() {  

       <?php if(Session::get('SESS_ACCOUNT_ID')) { ?>
        
                //Update Billing Notification
                $.post("<?php echo url('getunpaidItems'); ?>", {
                   _token: '<?php echo csrf_token();  ?>',
                    ajax_type: 'getunpaidItems'
                }, function (ret) {
                    if (ret > 0) {
                        $("#billing_notify").html(ret);
                        $("#billing_notify").fadeIn();
                    }
                });
         <?php } ?>       
                
                $("a.fancy_window").fancybox({
                    'hideOnContentClick'    : false,
                    'overlayShow'           : true,
                    'overlayOpacity'        : 0.75,
                    'frameWidth'            : 560,
                    'frameHeight'           : 550,
                    
                                                                    
                    'padding'               : 0,
                    'margin'                : 0,
                    'showCloseButton'       : false,

                                        
                    'titleShow'             : false
                });
                     
                //Modules preview (except for banners)
                $("a.fancy_window_preview").fancybox({
                    'type'                  : 'iframe',
                    'width'                 : 1110,
                    'height'                : 440,
                    'closeBtn'              : false,
                    'padding'               : 0,
                    'margin'                : 0
                });
                     
                //Banner preview
                 $("a.fancy_window_preview_banner").fancybox({
                                        'closeBtn'              : false,
                                        'type'                  : 'iframe',
                    'width'                 : 800,
                    'maxHeight'             : 250,
                    'padding'               : 0,
                    'margin'                : 0
                });
                
                //Transaction/Invoice Detail > View custom invoice items / package items
                $("a.fancy_window_custom").fancybox({
                                        'closeBtn'              : false,
                    'padding'               : 0,
                    'maxHeight'             : 500,
                                        'type'                  : 'iframe',
                    'width'                 : 620,
                    'height'                : 370
                });
                
                //Print invoice (claim / sign up / billing)
                $("a.fancy_window_invoice").fancybox({
                    'type'                  : 'iframe',
                    'width'                 : 680,
                    'height'                : 580
                });
                
                //Agree terms (claim / signup)
                $("a.fancy_window_terms").fancybox({
                                        'closeBtn'              : false,
                    'padding'               : 0,
                                        'type'                  : 'iframe',
                    'width'                 : 650,
                    'maxHeight'             : 500
                });

                  $("a.fancy_window_review").fancybox({
                                    type                : 'iframe',
                                    maxWidth            : 600,
                                    maxHeight           : 600,
                                    padding             : 0,
                                    margin              : 0,
                                    closeBtn            : false
                                }); 
                  $("a.fancy_window_tofriend").fancybox({
                                    type                : 'iframe',
                                    maxWidth            : 780,
                                    height              : 625,
                                    padding             : 0,
                                    margin              : 0,
                                    closeBtn            : false,
                                    autoSize            : false
                                });
                
            });
            
        </script>  

        
</head>
<body >
<div id="float_layer" class="floatLayer"></div>
    @include('front/common/header')
    <section id="page-content">
            @yield('content')

    @include('front/common/footer')
	</section>
    
<script src="{{ asset('/front/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('/front/js/viewportchecker.js') }}"></script>

<!--script src="{{ asset('/front/js/swiper.jquery.min.js') }}"></script>

<script>
	var swiper = new Swiper('.swiper-container', {
		pagination: '.swiper-pagination',
		slidesPerView: '7',
		centeredSlides: false,
		paginationClickable: true,
		nextButton: '.swiper-button-next',
		prevButton: '.swiper-button-prev',
		spaceBetween: 15,
		autoplay: 2500,
		autoplayDisableOnInteraction: false,
		breakpoints: {
        1024: {
            slidesPerView: 4,
            spaceBetween: 40
        },
        768: {
            slidesPerView: 3,
            spaceBetween: 30
        },
        640: {
            slidesPerView: 2,
            spaceBetween: 20
        },
        320: {
            slidesPerView: 1,
            spaceBetween: 10
        }
    }
	});
</script-->
 <script type="text/javascript">
                    $(document).ready(function() {
                        $('.selectpicker .select').selectpicker();loadToolTip('general');loadToolTip('detail');$('input').placeholder();              
                              
                 //The following API calls are needed to work with the fancybox 1.3.4 (members area)
                                

                                $("a.fancy_window_twilio").fancybox({
                                    type                : 'iframe',
                                    maxWidth            : 330,
                                    maxHeight           : 335,
                                    padding             : 0,
                                    margin              : 0,
                                    closeBtn            : false
                                });

                                $("a.fancy_window_review").fancybox({
                                    type                : 'iframe',
                                    maxWidth            : 600,
                                    maxHeight           : 600,
                                    padding             : 0,
                                    margin              : 0,
                                    closeBtn            : false
                                });                  });
                </script>
</body>
</html>