<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

use Illuminate\Http\Request;



Route::get('/', 'HomeController@index');
Route::get('home', 'HomeController@index');
Route::get('why-choose-us'.'.html'	, 'HomeController@WhyChooseUs');
Route::get('fraud-alert-renters'.'.html', 'HomeController@RentersFraudAlert');
Route::get('fraud-alert-owner'.'.html', 'HomeController@RentersOwnerAlert');
Route::get('faqs'.'.html', 'HomeController@contentFaqs');
Route::get('our-team'.'.html', 'HomeController@OurTeam');
Route::get('photography-services'.'.html', 'HomeController@PhotographyServices');
Route::get('privacy-policy'.'.html', 'HomeController@PrivacyPolicy');
Route::get('spam-warning'.'.html', 'HomeController@SpamWarning');
Route::get('testimonials-owners'.'.html', 'HomeController@TestimonialsOwners');
Route::get('advertise', 'HomeController@Advertise');
Route::get('order_listing/{level}', 'HomeController@orderListing');
Route::get('for-rent-sign-request'.'.html', 'HomeController@rentalSignForm');
Route::get('guarantee-and-verify'.'.html', 'HomeController@guaranteeVerify');
Route::get('cash-back-referral'.'.html', 'HomeController@cashbackReferral');
Route::get('listing-instructions'.'.html', 'HomeController@listingInstruction');

Route::get('ordercalculateprice', 'HomeController@ordercalculateprice');
Route::get('loadcategorytree', 'HomeController@loadcategorytree');
Route::post('validateAdvertise', 'HomeController@validateAdvertise');
Route::post('order_listing', 'HomeController@order_signup');
Route::get('signup/invoice', 'HomeController@signupInvoice');
Route::get('signup/payment/{payment_method}', 'HomeController@signupPayment');
Route::get('termsof-use'.'.html', 'HomeController@TermsofUse');
Route::get('legal-disclaimer'.'.html', 'HomeController@LegalDisclaimer');

// Auth 
Route::controllers(['auth' => 'Auth\AuthController','password' => 'Auth\PasswordController',]);



// Front End 
//Route::get('/', 'FrontController@index');
//Route::get('index', 'FrontController@index');

// hassan
Route::get('contact-us', 'ContactusController@index');
Route::post('contact-send', 'ContactusController@store');
// regiter login system
Route::get('login', 'UserController@index');
Route::post('login', 'UserController@login');
Route::post('postLogin', 'UserController@post_login');
Route::get('logout', 'UserController@post_logout');




Route::get('advsearch', 'SearchController@index');
Route::get('randomizer', 'SearchController@randomizer');
Route::post('adv-location', 'SearchController@advance_location');

Route::get('compare_listing', 'SearchController@compareListing');



Route::post('submit-review', 'SearchController@StoreReview');



Route::post('quick-info', 'SearchController@quickInfo');

Route::get('search', 'SearchController@SearchResults');


Route::get('advancedsearch-checkform', 'SearchController@AdvancedsearchCheckform');

Route::post('loadmap', 'SearchController@loadmap');
Route::get('results_ajax', 'SearchController@results_ajax');



Route::get('advance-search-results', 'SearchController@AdvanceSearchResults');

Route::get('review/{id}/{type}', 'SearchController@listingReview');


Route::get('listing/{id}'.'.html', 'SearchController@listingDetail');
Route::get('specials/{id}', 'SearchController@getSpecials');
Route::get('new-jersey.html', 'SearchController@getNjlistings');
Route::get('sponsors', 'UserController@sponser_dashboard');
Route::get('sponsors/listing/rate/{id}', 'SponserController@sponser_listing_rate');
Route::post('loadDashboard', 'SponserController@sponser_load_dashboard');

Route::get('sponsors/listing/{id}', 'SponserController@sponser_listing');

Route::get('sponsors/listing/{id}/{process}', 'SponserController@sponser_listing');

Route::post('sponsors/listing/listingStore', 'SponserController@sponser_storeListing');
Route::post('sponsors/returngallery', 'SponserController@sponser_returngallery');

Route::get('sponsors/popupUploadImage', 'SponserController@sponser_popupUploadImage');

Route::post('sponsors/popupstoreImage', 'SponserController@sponser_popupstoreImage');

Route::get('sponsors/makemainimage', 'SponserController@sponser_makemainimage');

Route::get('sponsors/changemainimage', 'SponserController@sponser_changemainimage');

Route::get('sponsors/delete_image', 'SponserController@sponser_popup_image');
Route::post('sponsors/delete_image', 'SponserController@sponser_delete_image');


Route::get('sponsors/imagecaption', 'SponserController@sponsers_image_caption');

Route::post('sponsors/change_imagecaption', 'SponserController@sponser_change_imagecaption');


Route::post('sponsors/popupCropImage', 'SponserController@sponser_popupCropImage');

Route::post('get-search-pos', 'SponserController@getSearchPosSelect');


Route::get('sponsors/locations', 'SponserController@sponser_getcity');

Route::get('sponsors/listinglevel', 'SponserController@listing_listinglevel');


Route::get('sponsors/addlisting', 'SponserController@sponser_listing');


Route::get('sponsors/preview/{id}', 'SponserController@sponser_preview');
Route::get('sponsors/backlinks/{id}', 'SponserController@sponser_listing_backlinks');
Route::post('check_website', 'SponserController@check_website');
Route::post('store/backlinks', 'SponserController@store_listing_backlinks');
Route::get('sponsors/rate/{id}', 'SponserController@sponser_listing_rate');
Route::get('sponsors/rate/{id}/{message}', 'SponserController@sponser_listing_rate');


Route::get('sponsors/badges/{id}', 'SponserController@sponser_listing_specials');
Route::get('sponsors/specials/{id}', 'SponserController@sponser_listing_specials');
Route::post('sponsors/rateStore', 'SponserController@sponsor_storeRate');
Route::post('sponsors/ajax_listing_rate', 'SponserController@ajax_listing_rate');
Route::get('sponsors/delete_rate/{id}/{listing_id}', 'SponserController@sponsor_DelPage');
Route::post('sponsors/delete_rates', 'SponserController@sponsor_DelRate');

Route::get('sponsors/availability/{id}', 'SponserController@sponser_listing_availability');

Route::post('sponsors/storeAvailability', 'SponserController@sponsor_storeAvailability');

Route::get('sponsors/availability/{id}/{availability_id}', 'SponserController@sponser_listing_availability');

Route::post('sponsors/years', 'SponserController@sponsor_getYear');


Route::post('sponsors/specialStore', 'SponserController@sponsor_storeSpecial');

Route::get('sponsors/account', 'SponserController@sponsor_account');
Route::get('sponsors/help', 'SponserController@sponsor_help');
Route::post('help/store', 'SponserController@sponsor_help_store');
Route::get('sponsors/faq', 'SponserController@sponsor_faq');
Route::get('profile', 'SponserController@listing_profile');
Route::get('profile/edit', 'SponserController@listing_profile_edit');
Route::post('profile/update', 'SponserController@listing_profile_update');

//BillingController
Route::get('sponsors/billing', 'BillingController@sponser_listing_billing');
Route::post('sponsors/billing/check_promo', 'BillingController@sponsor_check_promo');
Route::post('sponsors/billing/pay', 'BillingController@sponsor_billing_pay');
Route::get('sponsors/billing/invoice/{id}', 'BillingController@sponsor_billing_invoice');
Route::get('custominvoice_items/{id}', 'BillingController@sponsor_custominvoice_items');

Route::get('sponsors/transactions', 'BillingController@sponser_listing_transactions');
Route::get('sponsors/transactions/view_invoice/{id}', 'BillingController@sponsor_transactions_viewinvoice');
Route::get('sponsors/transactions/view_transaction/{id}', 'BillingController@sponsor_transactions_viewtransaction');
Route::post('payment_method/authorize', 'BillingController@payment_method_authorize');


Route::post('sponsors/review_reply', 'SponserController@sponsor_review_reply');
Route::post('sponsors/lead_reply', 'SponserController@sponsor_lead_reply');

Route::get('form_crop', 'SponserController@cropProfileImage');
Route::post('profile/ajax', 'SponserController@listing_profile_ajax');
Route::post('getunpaidItems', 'SponserController@getunpaidItems');
Route::get('check_friendlyurl', 'SponserController@check_friendlyurl');
Route::get('listing_emailform/{id}', 'SearchController@listing_emailform');
Route::post('listing_emailformstore', 'SearchController@listing_emailformstore');
Route::post('sponsors/updateAccount', 'SponserController@sponsor_update_account');
Route::get('captcha', 'SearchController@captcha');
Route::get('cron-manager', 'HomeController@cron_manager');
Route::post('cron-manager', 'HomeController@cron_manager');



$admin= "Admin";
Route::group(
    array('prefix' => 'admin'), 
    function() {
        $admin= "Admin\\";
        
		Route::get('/', $admin.'HomeController@WhyChooseUs');
        Route::get('/signin', $admin.'HomeController@get_signin');
        
    }
);

Route::get('{all}', 'SearchController@SearchResults')->where('all', '(.*)');


