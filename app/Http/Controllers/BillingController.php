<?php 
namespace App\Http\Controllers;
use DB,PDO;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Validator, Input, Redirect, Session, Cookie,Mail;
use App\Models\Domain\Sql;
use App\Functions\Functions;
use App\Functions\Listing;
use App\Functions\ListingLevel;
use App\Functions\DiscountCode;
use App\Functions\ListingCategory;
use App\Functions\Listing_Category;
use App\Functions\Account;
use App\Functions\Contact;
use App\Functions\Invoice;
use App\Functions\ItemStatus;
use App\Functions\InvoiceListing;
use App\Functions\Setting;
use App\Functions\InvoiceListingChoice;
use App\Functions\EditorChoice;
use App\Functions\ListingChoice;
use App\Functions\CustomInvoice;
use App\Functions\InvoiceCustomInvoice;
use App\Functions\PaymentLog;
use App\Functions\PaymentListingLog;
use App\Functions\PaymentListingChoiceLog;
use App\Functions\PaymentCustomInvoiceLog;

class BillingController extends Controller {

	    protected $layout = 'layouts.search';

	  function sponser_listing_billing(Request $request)
 	{
        $userAccountId = Session::get('SESS_ACCOUNT_ID');
        $profileSp = Functions::SponsersProfile();
      	$ownerName= '';
        if(!empty($profileSp))
        {
           $ownerName = $profileSp[0]->first_name.' '.$profileSp[0]->last_name.', ';
       }
       $meta_data = Sql::sponsersMeta($userAccountId);
       $headerTitle = $ownerName.'Welcome to the Sponsor Section -'.$meta_data[0]->value;
      	$headerAuthor = $meta_data[1]->value;
       $headerDescription = $meta_data[2]->value;
       $headerKeywords = $meta_data[3]->value;
       $footer_text = Sql::footerText();
       $spListingData = Sql::SponserListing($userAccountId);
        $max_item_sum=50;
		$sess_id=Functions::sess_getAccountIdFromSession();

		if (!isset($second_step)) {

			

			$sql = "SELECT count(id) as total_count FROM Listing WHERE account_id = ".$sess_id;
			$result = Sql::fetch($sql);
			if ($result !== false) {
				if ($row = $result) {
					$total_listing_sum = $row[0]->total_count;
				}
			}
			if ($total_listing_sum <= 50) {
				$listings = Sql::getBillingListing($sess_id,'title');


			} else {
				$overbadges_msg = config('params.LANG_MSG_OVERITEM_MORETHAN')." ".$max_item_sum." ".config('params.LANG_LISTING_DESIGNATION_PLURAL').". ".config('LANG_MSG_OVERITEM_CONTACTADMIN').".";
			}


			
			
		} 
		

		if ($listings) {

			$levelObj = new ListingLevel();
			$levelObj->ListingLevel(true);
			
			foreach ($listings as $each_listing) {
			
				if (isset($second_step)) {
					$auxListingObj = new Listing($each_listing->id);
					$each_listing->renewal_date = $auxListingObj->getNextRenewalDate();
					unset($auxListingObj);
				}

				$category_amount = 0;
				$sql = "SELECT category_id FROM Listing_Category WHERE listing_id = {$each_listing->id}";
				$result = Sql::fetch($sql);

				if(count($result)){
					foreach($result as $row){
						$category_amount++;
					}

				}
				// setting some of the bill information
				$bill_info["listings"]["{$each_listing->id}"]["id"]           = htmlspecialchars($each_listing->id);
				$bill_info["listings"]["{$each_listing->id}"]["title"]           = htmlspecialchars($each_listing->title);
				$bill_info["listings"]["{$each_listing->id}"]["address"]         = htmlspecialchars($each_listing->address);
				$bill_info["listings"]["{$each_listing->id}"]["level"]           = $each_listing->name;
				$bill_info["listings"]["{$each_listing->id}"]["level_number"]    = $each_listing->level;
				$bill_info["listings"]["{$each_listing->id}"]["logo"]            = ($each_listing->image_id) ? 1 : 0;
				$bill_info["listings"]["{$each_listing->id}"]["url"]             = ($each_listing->url) ? 1 : 0;
				$bill_info["listings"]["{$each_listing->id}"]["category_amount"] = ($category_amount) ? $category_amount : 0;
				$bill_info["listings"]["{$each_listing->id}"]["renewal_date"]    = $each_listing->renewal_date;
				$bill_info["listings"]["{$each_listing->id}"]["discount_id"]     = $each_listing->discount_id;
				$bill_info["listings"]["{$each_listing->id}"]["status"]          = $each_listing->status;


				$discountAmount = $discountType = null;
				if ($each_listing->discount_id) {
					$sql = "SELECT `amount`, `type` FROM Discount_Code WHERE id = '" .$each_listing->discount_id . "'";
					$result = Sql::fetch($sql);
					foreach($result as $row){
						$discountAmount = $row->amount;
						$discountType = $row->type;
					}
				}
				$bill_info["listings"]["{$each_listing->id}"]["discount_amount"] = $discountAmount;
				$bill_info["listings"]["{$each_listing->id}"]["discount_type"] = $discountType;


				// Bill pricing
				$thisListing = new Listing();
				$bill_info["listings"]["{$each_listing->id}"]["total_fee"] = $thisListing->getListingPrice($each_listing->level);


				unset($thisListing);
				if ($bill_info["listings"]["{$each_listing->id}"]["total_fee"] <= 0) {
					unset($bill_info["listings"]["{$each_listing->id}"]);
					continue;
				}
				// Need To Check Out
				$thisListing = new Listing();
				$thisListing->Listing($each_listing->id); 
				if ($thisListing->needToCheckOut()) 
					$bill_info["listings"]["{$each_listing->id}"]["needtocheckout"] = "y";
				else $bill_info["listings"]["{$each_listing->id}"]["needtocheckout"] = "n";
				//d($bill_info["listings"]["{$each_listing->id}"]["needtocheckout"],1);die;

				//unset($thisListing);	

				/*if (($category_amount > 0) && (($category_amount - $levelObj->getFreeCategory($each_listing->level)) > 0)) {
					$bill_info["listings"]["{$each_listing->id}"]["extra_category_amount"] = $category_amount - $levelObj->getFreeCategory($each_listing["level"]);
				} else {
					$bill_info["listings"]["{$each_listing->id}"]["extra_category_amount"] = 0;
				}*/


				// Setting total bill
				/*if($bill_info["listings"]["{$each_listing->id}"]["total_fee"])
					$bill_info["total_bill"] += $bill_info["listings"]["{$each_listing->id}"]["total_fee"];*/

				// Money format for listing fees
				if($bill_info["listings"]["{$each_listing->id}"]["total_fee"])
				 if($bill_info["listings"]["{$each_listing->id}"]["discount_type"] === 'monetary value'):
                   $bill_info["listings"]["{$each_listing->id}"]["total_fee"]=Functions::format_money($bill_info["listings"]["{$each_listing->id}"]["total_fee"]-$bill_info["listings"]["{$each_listing->id}"]["discount_amount"]);
                     elseif($bill_info["listings"]["{$each_listing->id}"]["discount_type"] === 'percentage'):
                    $bill_info["listings"]["{$each_listing->id}"]["total_fee"]=Functions::format_money(floatval($bill_info["listings"]["{$each_listing->id}"]["total_fee"])/((100-floatval($bill_info["listings"]["{$each_listing->id}"]["discount_amount"]))/100)-$bill_info["listings"]["{$each_listing->id}"]["total_fee"]);
                    else: 
					$bill_info["listings"]["{$each_listing->id}"]["total_fee"] = Functions::format_money($bill_info["listings"]["{$each_listing->id}"]["total_fee"]);	
                   endif;


				


			}
		if (empty($bill_info["listings"])) unset($bill_info["listings"]);


		if (!isset($second_step)) {

			$sql = "SELECT count(id) as total_count FROM Listing_Choice WHERE account_id = ".$sess_id;
			$result = Sql::fetch($sql);
			if ($result !== false) {
				if ($row = $result) {
					$total_badges_sum = $row[0]->total_count;
				}
			}
		
			if ($total_badges_sum <= 50) {
				
				$badges = Sql::getBadgesListing($sess_id,'editor_choice_id');
			
				
			} else {
				$overbadges_msg = config('params.LANG_MSG_OVERITEM_MORETHAN')." ".$max_item_sum." ".config('params.LANG_LISTING_DESIGNATION_PLURAL').". ".config('LANG_MSG_OVERITEM_CONTACTADMIN').".";
			}
		}
	

		if ($badges) {

			foreach ($badges as $each_badge) {

				if (isset($second_step)) {
					$auxListingChoiceObj = new ListingChoice($each_badge["editor_choice_id"], $each_badge["listing_id"]);
					$each_badge["renewal_date"] = $auxListingChoiceObj->getNextRenewalDate();
					unset($auxListingChoiceObj);
				}
				
				$editorObj = new EditorChoice();
				$editor=$editorObj->EditorChoicePrice($each_badge->editor_choice_id);

				$listingObj = new Listing( $each_badge->listing_id );
				// setting some of the bill information
				$bill_info["badges"]["{$each_badge->id}"]["name"]   = (isset($editor[0])?htmlspecialchars($editor[0]->name):'');
				$bill_info["badges"]["{$each_badge->id}"]["listing_title"]   = htmlspecialchars($listingObj->title);
				$bill_info["badges"]["{$each_badge->id}"]["address"]   	   = htmlspecialchars($listingObj->address);
				$bill_info["badges"]["{$each_badge->id}"]["logo"]            = (isset($editor[0]->image_id)) ? 1 : 0;
				$bill_info["badges"]["{$each_badge->id}"]["listing_id"]      = $each_badge->listing_id;
				$bill_info["badges"]["{$each_badge->id}"]["editor_id"]	   = $each_badge->editor_choice_id;
				$bill_info["badges"]["{$each_badge->id}"]["discount_id"]     = $each_badge->discount_id;
				$bill_info["badges"]["{$each_badge->id}"]["renewal_date"]    = $each_badge->renewal_date;
				$bill_info["badges"]["{$each_badge->id}"]["status"]          = $each_badge->status;


				$discountAmount = $discountType = null;
				if ($each_badge->discount_id) {
					$sql = "SELECT `amount`, `type` FROM Discount_Code WHERE id = '" .$each_badge->discount_id . "'";
					$result = Sql::fetch($sql);
					foreach($result as $row){
						$discountAmount = $row->amount;
						$discountType = $row->type;
					}
				}
				$bill_info["badges"]["{$each_badge->id}"]["discount_amount"] = $discountAmount;
				$bill_info["badges"]["{$each_badge->id}"]["discount_type"] = $discountType;



				unset($listingObj);

				// Bill pricing
				$ListingChoice = new ListingChoice();

				$bill_info["badges"]["{$each_badge->id}"]["total_fee"] =$ListingChoice->getBadgesPrice($each_badge->editor_choice_id);

				unset($thisListingChoice);

				if ($bill_info["badges"]["{$each_badge->id}"]["total_fee"] <= 0) {
					unset($bill_info["badges"]["{$each_badge->id}"]);
					continue;
				}

				// Need To Check Out
				$thisListingChoice = new ListingChoice();
				$thisListingChoice->ListingChoice($each_badge->editor_choice_id,$each_badge->listing_id);

				if ($thisListingChoice->needToCheckOut()) $bill_info["badges"]["{$each_badge->id}"]["needtocheckout"] = "y";
				else $bill_info["badges"]["{$each_badge->id}"]["needtocheckout"] = "n";
				unset($thisListingChoice);


				// Money format for listing fees
				if($bill_info["badges"]["{$each_badge->id}"]["total_fee"])
				 if($bill_info["badges"]["{$each_badge->id}"]["discount_type"] === 'monetary value'):
                   $bill_info["badges"]["{$each_badge->id}"]["total_fee"]=Functions::format_money($bill_info["badges"]["{$each_badge->id}"]["total_fee"]-$bill_info["badges"]["{$each_badge->id}"]["discount_amount"]);
                     elseif($bill_info["badges"]["{$each_badge->id}"]["discount_type"] === 'percentage'):
                    $bill_info["badges"]["{$each_badge->id}"]["total_fee"]=Functions::format_money(floatval($bill_info["badges"]["{$each_badge->id}"]["total_fee"])/((100-floatval($bill_info["badges"]["{$each_badge->id}"]["discount_amount"]))/100)-$bill_info["badges"]["{$each_badge->id}"]["total_fee"]);
                    else: 
					$bill_info["badges"]["{$each_badge->id}"]["total_fee"] = Functions::format_money($bill_info["badges"]["{$each_badge->id}"]["total_fee"]);	
                   endif;

			}

			// There is no listing been payed.
			if (empty($bill_info["badges"])) unset($bill_info["badges"]);

		}


		if (config('params.CUSTOM_INVOICE_FEATURE') == "on") {

			Functions::setting_get("payment_tax_status", $payment_tax_status);
			$customInvoices = Sql::getCustomInvoice($sess_id);
		if ($customInvoices) {

		foreach($customInvoices as $each_custom_invoice)
		{
			$customInvoiceObj = new CustomInvoice();
			$customInvoiceObj->CustomInvoice($each_custom_invoice->id);

			// setting some of the bill information
			$bill_info["custominvoices"]["{$each_custom_invoice->id}"]["id"]     = $each_custom_invoice->id;

			$bill_info["custominvoices"]["{$each_custom_invoice->id}"]["subtotal"] = $customInvoiceObj->getNumber("subtotal");

			if ($payment_tax_status == "on") $bill_info["custominvoices"]["{$each_custom_invoice->id}"]["tax"] = $payment_tax_value;
			else $bill_info["custominvoices"]["{$each_custom_invoice->id}"]["tax"] = 0;

			if ($payment_tax_status == "on") $bill_info["custominvoices"]["{$each_custom_invoice[0]->id}"]["amount"] = Functions::payment_calculateTax($customInvoiceObj->getPrice(), $payment_tax_value, true, true);
				else $bill_info["custominvoices"]["{$each_custom_invoice->id}"]["amount"] = $customInvoiceObj->getPrice();
					$bill_info["custominvoices"]["{$each_custom_invoice->id}"]["title"]  = htmlspecialchars($each_custom_invoice->title);
					$bill_info["custominvoices"]["{$each_custom_invoice->id}"]["date"]   = $each_custom_invoice->date;

					// Money format for custom invoice ammount
					if($bill_info["custominvoices"]["{$each_custom_invoice->id}"]["amount"])
				$bill_info["custominvoices"]["{$each_custom_invoice->id}"]["amount"] = Functions::format_money($bill_info["custominvoices"]["{$each_custom_invoice->id}"]["amount"]);

					// There is not custom invoice being payed.
					if(empty($bill_info["custominvoices"])) unset($bill_info["custominvoices"]);

					if ($bill_info["custominvoices"]["{$each_custom_invoice->id}"]["amount"] <= 0) {
						unset($bill_info["custominvoices"]["{$each_custom_invoice->id}"]);
						continue;
					}


		}


		}
	}




return view('front.sponsers_billing',compact('spListingData','bill_info','headerTitle','headerAuthor','headerDescription','headerKeywords','footer_text'));
			
			// There is no listing been payed.
	
		


		}

        
 } 


 function sponsor_check_promo(Request $request)
{

	if($request->_token==csrf_token())
	    {
	    $result = array();

	    $code=$request->code;
	    $target=$request->target;
	    $requestId=$request->requestId;
	    

    if (!empty($code)) {
        $result['requestId'] =$requestId;
        list($type, $targetId) = explode("_",$target);
        if(!empty($target)){
            $sql = "SELECT `amount`, `type` FROM Discount_Code WHERE id = '" . $code. "'";
            $row = Sql::fetch($sql);
            $amount = $row[0]->amount;
            if ($row[0]->type=== 'percentage') {
                switch($type){
                    case "listing":
                        $sql = "SELECT `ListingLevel`.`price` FROM `Listing` LEFT JOIN `ListingLevel` ON `ListingLevel`.`value`=`Listing`.`level` AND 
                        	`ListingLevel`.`theme`='default' WHERE `id`='".intval($targetId)."'";
                        $target = Sql::fetch($sql);
                        $price = $target[0]->price;
                        break;
                    case "ListingChoice":
                        $sql = "SELECT `Editor_Choice`.`price` FROM `Listing_Choice` LEFT JOIN `Editor_Choice` ON `Editor_Choice`.`id`=`Listing_Choice`.`editor_choice_id` WHERE `Listing_Choice`.`id`='".intval($targetId)."'";
                         $target = Sql::fetch($sql);
                        $price = $target[0]->price;
                        break;
                }
                $amount = ($price * $row[0]->amount)/100;
            }
            $result['amount'] = "-".config('params.CURRENCY_SYMBOL').number_format($amount, 2);
        }else{
            $result['message'] = "Promo code is not valid.";
        }
    }

    header("Content-type: application/json");
    echo json_encode($result);
    }
}



 function sponsor_billing_pay(Request $request)
 {
 	if($request->_token==csrf_token())
	{	
		$payment_method=$request->payment_method;
		$second_step=$request->second_step;
		$listing_id=$request->listing_id;
		$badge_id=$request->badge_id;
		$discountlisting_id=$request->discountlisting_id;
		$discountbadge_id=$request->discountbadge_id; 
		$custom_invoice_id=$request->custom_invoice_id; 

        $userAccountId = Session::get('SESS_ACCOUNT_ID');
        $profileSp = Functions::SponsersProfile();
      	$ownerName= '';
        if(!empty($profileSp))
        {
           $ownerName = $profileSp[0]->first_name.' '.$profileSp[0]->last_name.', ';
       }
       $meta_data = Sql::sponsersMeta($userAccountId);
       $headerTitle = $ownerName.'Welcome to the Sponsor Section -'.$meta_data[0]->value;
       $headerAuthor = $meta_data[1]->value;
       $headerDescription = $meta_data[2]->value;
       $headerKeywords = $meta_data[3]->value;
       $footer_text = Sql::footerText();

       //Second Step
       if ($second_step && !$payment_method) {
		$payment_message = config('params.LANG_MSG_NO_PAYMENT_METHOD_SELECTED');
		}

		$max_item_sum=50;
		$sess_id=Functions::sess_getAccountIdFromSession();

		if (isset($second_step)) {

		if($listing_id)	{
			for ($i=0; $i < count($listing_id); $i++) {

					$list_id=$listing_id[$i];
					$listingObj = new Listing();
					$listingObj->Listing($listing_id[$i]);

					if (!Functions::is_valid_discount_code($discountlisting_id[$i], "listing", $listing_id[$i], $payment_message, $error_num)) {

					$payment_message = Functions::string_substr($payment_message, 0 ,-1);
		$payment_message .= "On Listings \"<b>".$listingObj->getString("title")."</b>\"<br />";

					}	
					elseif (is_array($discountlisting_id)){
						$listingObj->setString("discount_id", $discountlisting_id[$i]);
						$listingObj->Save();

						unset($listingObj);
					}

					$listings[]= Sql::getBillingListingId($listing_id[$i],$sess_id);


			}
		}	
	} 
		
		$bill_info["total_bill"]=0;
		if (isset($listings)) {

			$levelObj = new ListingLevel();
			$levelObj->ListingLevel(true);
			
			foreach ($listings as $each_listing) 
			{
				if (isset($second_step)) {
					$auxListingObj = new Listing();
					$auxListingObj->Listing($each_listing[0]->id);
					$each_listing[0]->renewal_date = $auxListingObj->getNextRenewalDate();
					unset($auxListingObj);
				}

				$category_amount = 0;
				$sql = "SELECT category_id FROM Listing_Category WHERE listing_id = {$each_listing[0]->id}";
				$result = Sql::fetch($sql);

				if(count($result)){
					foreach($result as $row){
						$category_amount++;
					}

				}
				// setting some of the bill information
				$bill_info["listings"]["{$each_listing[0]->id}"]["id"]           = htmlspecialchars($each_listing[0]->id);
				$bill_info["listings"]["{$each_listing[0]->id}"]["title"]           = htmlspecialchars($each_listing[0]->title);
				$bill_info["listings"]["{$each_listing[0]->id}"]["address"]         = htmlspecialchars($each_listing[0]->address);
				$bill_info["listings"]["{$each_listing[0]->id}"]["level"]           = $each_listing[0]->name;
				$bill_info["listings"]["{$each_listing[0]->id}"]["level_number"]    = $each_listing[0]->level;
				$bill_info["listings"]["{$each_listing[0]->id}"]["logo"]            = ($each_listing[0]->image_id) ? 1 : 0;
				$bill_info["listings"]["{$each_listing[0]->id}"]["url"]             = ($each_listing[0]->url) ? 1 : 0;
				$bill_info["listings"]["{$each_listing[0]->id}"]["category_amount"] = ($category_amount) ? $category_amount : 0;
				$bill_info["listings"]["{$each_listing[0]->id}"]["renewal_date"]    = $each_listing[0]->renewal_date;
				$bill_info["listings"]["{$each_listing[0]->id}"]["discount_id"]     = $each_listing[0]->discount_id;
				$bill_info["listings"]["{$each_listing[0]->id}"]["status"]          = $each_listing[0]->status;


				$discountAmount = $discountType = null;
				if ($each_listing[0]->discount_id) {
					$sql = "SELECT `amount`, `type` FROM Discount_Code WHERE id = '" .$each_listing[0]->discount_id . "'";
					$result = Sql::fetch($sql);
					foreach($result as $row){
						$discountAmount = $row->amount;
						$discountType = $row->type;
					}
				}
				$bill_info["listings"]["{$each_listing[0]->id}"]["discount_amount"] = $discountAmount;
				$bill_info["listings"]["{$each_listing[0]->id}"]["discount_type"] = $discountType;


				// Bill pricing
				$thisListing = new Listing();
				$bill_info["listings"]["{$each_listing[0]->id}"]["total_fee"] = $thisListing->getListingPrice($each_listing[0]->level);


				unset($thisListing);
				if ($bill_info["listings"]["{$each_listing[0]->id}"]["total_fee"] <= 0) {
					unset($bill_info["listings"]["{$each_listing[0]->id}"]);
					continue;
				}
				// Need To Check Out
				$thisListing = new Listing();
				$thisListing->Listing($each_listing[0]->id); 
				if ($thisListing->needToCheckOut()) 
					$bill_info["listings"]["{$each_listing[0]->id}"]["needtocheckout"] = "y";
				else $bill_info["listings"]["{$each_listing[0]->id}"]["needtocheckout"] = "n";

				unset($thisListing);

				if (($category_amount > 0) && (($category_amount - $levelObj->getFreeCategory($each_listing[0]->level)) > 0)) {
					$bill_info["listings"]["{$each_listing[0]->id}"]["extra_category_amount"] = $category_amount - $levelObj->getFreeCategory($each_listing[0]["level"]);
				} else {
					$bill_info["listings"]["{$each_listing[0]->id}"]["extra_category_amount"] = 0;
				}

				// Money format for listing fees
				if($bill_info["listings"]["{$each_listing[0]->id}"]["total_fee"])
				 if($bill_info["listings"]["{$each_listing[0]->id}"]["discount_type"] === 'monetary value'):
                   $bill_info["listings"]["{$each_listing[0]->id}"]["total_fee"]=Functions::format_money($bill_info["listings"]["{$each_listing[0]->id}"]["total_fee"]-$bill_info["listings"]["{$each_listing[0]->id}"]["discount_amount"]);
                     elseif($bill_info["listings"]["{$each_listing[0]->id}"]["discount_type"] === 'percentage'):
                    $bill_info["listings"]["{$each_listing[0]->id}"]["total_fee"]=Functions::format_money(floatval($bill_info["listings"]["{$each_listing[0]->id}"]["total_fee"])/((100-floatval($bill_info["listings"]["{$each_listing[0]->id}"]["discount_amount"]))/100)-$bill_info["listings"]["{$each_listing[0]->id}"]["total_fee"]);
                    else:
					$bill_info["listings"]["{$each_listing[0]->id}"]["total_fee"] = Functions::format_money($bill_info["listings"]["{$each_listing[0]->id}"]["total_fee"]);	
                   endif;
				


					// Setting total bill
				
				if($bill_info["listings"]["{$each_listing[0]->id}"]["total_fee"])
				{
					$bill_info["total_bill"] += $bill_info["listings"]["{$each_listing[0]->id}"]["total_fee"];
				}

				


			}
		}
		if (empty($bill_info["listings"])) unset($bill_info["listings"]);

		
		if (isset($second_step)) {

		if($badge_id)	{
			for ($i=0; $i < count($badge_id); $i++) {
					$listingChoiceObj = new ListingChoice();
					$listingChoiceObj->ListingChoice("", "", $badge_id[$i]);

					if (is_array($discountbadge_id)){
                        $listingChoiceObj->setString("discount_id", $discountbadge_id[$i]);
                        $listingChoiceObj->Save();
                       	unset($listingChoiceObj);
                    }
				
					$badges[]= Sql::getBadgesListingId($badge_id[$i],$sess_id,'editor_choice_id');

			}
		}	
	} 

		if (isset($badges)) 
		{

			foreach ($badges as $each_badge) {

				if (isset($second_step)) {
					$auxListingChoiceObj = new ListingChoice();
					$auxListingChoiceObj->ListingChoice($each_badge[0]->editor_choice_id, $each_badge[0]->listing_id);
					$each_badge[0]->renewal_date = $auxListingChoiceObj->getNextRenewalDate();
					unset($auxListingChoiceObj);
				}
				
				$editorObj = new EditorChoice();
				$editor=$editorObj->EditorChoicePrice($each_badge[0]->editor_choice_id);

				$listingObj = new Listing( $each_badge[0]->listing_id );
				// setting some of the bill information
				$bill_info["badges"]["{$each_badge[0]->id}"]["name"]   = (isset($editor[0])?htmlspecialchars($editor[0]->name):'');
				$bill_info["badges"]["{$each_badge[0]->id}"]["listing_title"]   = htmlspecialchars($listingObj->title);
				$bill_info["badges"]["{$each_badge[0]->id}"]["address"]   	   = htmlspecialchars($listingObj->address);
				$bill_info["badges"]["{$each_badge[0]->id}"]["logo"]            = (isset($editor[0]->image_id)) ? 1 : 0;
				$bill_info["badges"]["{$each_badge[0]->id}"]["listing_id"]      = $each_badge[0]->listing_id;
				$bill_info["badges"]["{$each_badge[0]->id}"]["editor_id"]	   = $each_badge[0]->editor_choice_id;
				$bill_info["badges"]["{$each_badge[0]->id}"]["discount_id"]     = $each_badge[0]->discount_id;
				$bill_info["badges"]["{$each_badge[0]->id}"]["renewal_date"]    = $each_badge[0]->renewal_date;
				$bill_info["badges"]["{$each_badge[0]->id}"]["status"]          = $each_badge[0]->status;


				$discountAmount = $discountType = null;
				if ($each_badge[0]->discount_id) {
					$sql = "SELECT `amount`, `type` FROM Discount_Code WHERE id = '" .$each_badge[0]->discount_id . "'";
					$result = Sql::fetch($sql);
					foreach($result as $row){
						$discountAmount = $row->amount;
						$discountType = $row->type;
					}
				}
				//d($discountAmount,1);die;
				$bill_info["badges"]["{$each_badge[0]->id}"]["discount_amount"] = $discountAmount;
				$bill_info["badges"]["{$each_badge[0]->id}"]["discount_type"] = $discountType;



				unset($listingObj);

				// Bill pricing
				$ListingChoice = new ListingChoice();

				$bill_info["badges"]["{$each_badge[0]->id}"]["total_fee"] =$ListingChoice->getBadgesPrice($each_badge[0]->editor_choice_id);

				unset($thisListingChoice);

				if ($bill_info["badges"]["{$each_badge[0]->id}"]["total_fee"] <= 0) {
					unset($bill_info["badges"]["{$each_badge[0]->id}"]);
					continue;
				}

				// Need To Check Out
				$thisListingChoice = new ListingChoice();
				$thisListingChoice->ListingChoice($each_badge[0]->editor_choice_id,$each_badge[0]->listing_id);


				if ($thisListingChoice->needToCheckOut()) $bill_info["badges"]["{$each_badge[0]->id}"]["needtocheckout"] = "y";
				else $bill_info["badges"]["{$each_badge[0]->id}"]["needtocheckout"] = "n";
				unset($thisListingChoice);

			// Money format for listing fees
				if($bill_info["badges"]["{$each_badge[0]->id}"]["total_fee"])
				 if($bill_info["badges"]["{$each_badge[0]->id}"]["discount_type"] === 'monetary value'):
                   $bill_info["badges"]["{$each_badge[0]->id}"]["total_fee"]=Functions::format_money($bill_info["badges"]["{$each_badge[0]->id}"]["total_fee"]-$bill_info["badges"]["{$each_badge[0]->id}"]["discount_amount"]);
                     elseif($bill_info["badges"]["{$each_badge[0]->id}"]["discount_type"] === 'percentage'):
                    $bill_info["badges"]["{$each_badge[0]->id}"]["total_fee"]=Functions::format_money(floatval($bill_info["badges"]["{$each_badge[0]->id}"]["total_fee"])/((100-floatval($bill_info["badges"]["{$each_badge[0]->id}"]["discount_amount"]))/100)-$bill_info["badges"]["{$each_badge[0]->id}"]["total_fee"]);
                    else: 
					$bill_info["badges"]["{$each_badge[0]->id}"]["total_fee"] = Functions::format_money($bill_info["badges"]["{$each_badge[0]->id}"]["total_fee"]);	
                   endif;

				// Setting total bill
				if($bill_info["badges"]["{$each_badge[0]->id}"]["total_fee"])
					$bill_info["total_bill"] += $bill_info["badges"]["{$each_badge[0]->id}"]["total_fee"];

				

			}

			if (empty($bill_info["badges"])) unset($bill_info["badges"]);

		}
		

		if (config('params.CUSTOM_INVOICE_FEATURE') == "on") {


			Functions::setting_get("payment_tax_status", $payment_tax_status);
			Functions::setting_get("payment_tax_value", $payment_tax_value);

	
			if(isset($custom_invoice_id)){
					for($i=0; $i < count($custom_invoice_id); $i++){
					

					$customInvoices[] = Sql::getCustomInvoiceId($custom_invoice_id[$i],$sess_id);

					}
				}

		if (isset($customInvoices)) {

		foreach($customInvoices as $each_custom_invoice)
		{
			$customInvoiceObj = new CustomInvoice();
			$customInvoiceObj->CustomInvoice($each_custom_invoice[0]->id);

			// setting some of the bill information
			$bill_info["custominvoices"]["{$each_custom_invoice[0]->id}"]["id"]     = $each_custom_invoice[0]->id;

			$bill_info["custominvoices"]["{$each_custom_invoice[0]->id}"]["subtotal"] = $customInvoiceObj->getNumber("subtotal");

			if ($payment_tax_status == "on") $bill_info["custominvoices"]["{$each_custom_invoice[0]->id}"]["tax"] = $payment_tax_value;
			else $bill_info["custominvoices"]["{$each_custom_invoice[0]->id}"]["tax"] = 0;

			if ($payment_tax_status == "on") $bill_info["custominvoices"]["{$each_custom_invoice[0]->id}"]["amount"] = Functions::payment_calculateTax($customInvoiceObj->getPrice(), $payment_tax_value, true, true);
				else $bill_info["custominvoices"]["{$each_custom_invoice[0]->id}"]["amount"] = $customInvoiceObj->getPrice();
					$bill_info["custominvoices"]["{$each_custom_invoice[0]->id}"]["title"]  = htmlspecialchars($each_custom_invoice[0]->title);
					$bill_info["custominvoices"]["{$each_custom_invoice[0]->id}"]["date"]   = $each_custom_invoice[0]->date;


				// Setting total bill
					if($bill_info["custominvoices"]["{$each_custom_invoice[0]->id}"]["amount"]) {
						$bill_info["total_bill"] += $bill_info["custominvoices"]["{$each_custom_invoice[0]->id}"]["subtotal"];
					}	

					// Money format for custom invoice ammount
					if($bill_info["custominvoices"]["{$each_custom_invoice[0]->id}"]["amount"])
				$bill_info["custominvoices"]["{$each_custom_invoice[0]->id}"]["amount"] = Functions::format_money($bill_info["custominvoices"]["{$each_custom_invoice[0]->id}"]["amount"]);

					// There is not custom invoice being payed.
					if(empty($bill_info["custominvoices"])) unset($bill_info["custominvoices"]);

					if ($bill_info["custominvoices"]["{$each_custom_invoice[0]->id}"]["amount"] <= 0) {
						unset($bill_info["custominvoices"]["{$each_custom_invoice[0]->id}"]);
						continue;
					}


		}


		}
	}


			// Money format for total bill
		$bill_info["total_bill"] = Functions::format_money($bill_info["total_bill"]);


		if ($payment_tax_status == "on") $bill_info["tax_amount"] = $payment_tax_value;
		else $bill_info["tax_amount"] = 0;
		if ($payment_tax_status == "on") $bill_info["amount"] = Functions::payment_calculateTax(str_replace(",","",$bill_info["total_bill"]), $payment_tax_value, false);
		else $bill_info["amount"] = str_replace(",","",$bill_info["total_bill"]);




		 
	


	/*if ($payment_tax_status == "on") $bill_info["tax_amount"] = $payment_tax_value;
	else $bill_info["tax_amount"] = 0;
	if (isset($payment_tax_status) == "on") $bill_info["amount"] = Functions::payment_calculateTax(str_replace(",","",$bill_info["total_bill"]), $payment_tax_value, false);
	else $bill_info["amount"] = str_replace(",","",$bill_info["total_bill"]);*/	
}

	// INVOICE 
		if ($second_step) 
		{	
			if (($payment_method == "invoice") && ($bill_info["total_bill"] > 0)) 
			{

		if (isset($bill_info["badges"]) || isset($bill_info["listings"]) || isset($bill_info["events"]) || isset($bill_info["banners"]) || isset($bill_info["classifieds"]) || isset($bill_info["articles"]) || isset($bill_info["custominvoices"])) {

			//$invoiceStatusObj = new InvoiceStatus();
			$accountObj = new Account();
			$account=$accountObj->Account($sess_id);
			$arr_invoice["account_id"]		= $sess_id;
			$arr_invoice["username"]		= $accountObj->getString("username");
			$arr_invoice["ip"]				= $_SERVER["REMOTE_ADDR"];
			$arr_invoice["date"]			= date("Y")."-".date("m")."-".date("d")." ".date("H").":".date("i").":".date("s");

			$arr_invoice["status"]			= 'P'; //$invoiceStatusObj->getDefault()


			if (isset($payment_tax_status) == "on")
						$arr_invoice["tax_amount"]  = $payment_tax_value;
					else
						$arr_invoice["tax_amount"]  = 0;

					// SUBTOTAL AMOUNT
					$arr_invoice["subtotal_amount"] = str_replace(",","",$bill_info["total_bill"]);

					// TOTAL AMOUNT
					if (isset($payment_tax_status)  == "on")
						$arr_invoice["amount"]		= Functions::payment_calculateTax(str_replace(",","",$bill_info["total_bill"]), $payment_tax_value, false);
					else
						$arr_invoice["amount"]		= str_replace(",","",$bill_info["total_bill"]);
					
					$arr_invoice["currency"]		= config('params.INVOICEPAYMENT_CURRENCY');
					$arr_invoice["expire_date"]		= date("Y-m-d",mktime(0,0,0,date("m")+1,date("d"),date("Y")));

					$invoiceObj = new Invoice();
				 	$invoices=$invoiceObj->Invoice($arr_invoice);
					$invoiceObj->Save();

					$bill_info["invoice_number"] = $invoiceObj->id;


						if ($bill_info["listings"]) 
						foreach ($bill_info["listings"] as $id => $info)
						{

						$listingObj = Sql::BillingInformation($id);	
						//$listingObj = new Listing();
						//$listingObj->Listing($id);
                        $levelObj = new ListingLevel();
                        $levelObj->ListingLevel(true);

						$category_amount = 0;
						$sql = "SELECT category_id FROM Listing_Category WHERE listing_id = ".$listingObj[0]->id."";
						$result = Sql::fetch($sql);
						if(count($result)){
						foreach($result as $row){
						$category_amount++;
							}

						}

						$arr_invoice_listing["invoice_id"]    = $invoiceObj->id;
						$arr_invoice_listing["listing_id"]    = $id;
						$arr_invoice_listing["listing_title"] = $listingObj[0]->title;
						$arr_invoice_listing["discount_id"]   = $listingObj[0]->discount_id;
                        $arr_invoice_listing["level"]         = $listingObj[0]->level;
						$arr_invoice_listing["level_label"]   = $listingObj[0]->name;
						$arr_invoice_listing["renewal_date"]  = $listingObj[0]->renewal_date;
						$arr_invoice_listing["categories"]    = ($category_amount) ? $category_amount : 0;
						$arr_invoice_listing["amount"]        = str_replace(",","",$info["total_fee"]);

						$arr_invoice_listing["extra_categories"] = 0;
						if (($category_amount > 0) && (($category_amount - $levelObj->getFreeCategory($listingObj[0]->level)) > 0)) {
							$arr_invoice_listing["extra_categories"] = $category_amount - $levelObj->getFreeCategory($listingObj[0]->level);
						} else {
							$arr_invoice_listing["extra_categories"] = 0;
						}

						$arr_invoice_listing["listingtemplate_title"] = "";
						if (config('params.LISTINGTEMPLATE_FEATURE') == "on" && config('CUSTOM_LISTINGTEMPLATE_FEATURE') == "on") {

							if ($listingObj->getString("listingtemplate_id")) {
								$listingTemplateObj = new ListingTemplate($listingObj->getString("listingtemplate_id"));
								$arr_invoice_listing["listingtemplate_title"] = $listingTemplateObj->getString("title", false);
							}
						}

						$invoiceListingObj = new InvoiceListing();
						$invoiceListingObj->InvoiceListing($arr_invoice_listing);
						$invoiceListingObj->Save();

						unset($listingObj);
						unset($invoiceListingObj);

					}

					if (isset($bill_info["badges"])) 
						foreach ($bill_info["badges"] as $id => $info) {

						$listingChoiceObj = Sql::BadgesInformation($id);		
						//$listingChoiceObj = new ListingChoice();
						//$listingChoiceObj->ListingChoice("", "", $id);
						//$editorObj = new EditorChoice();
						//$editorObj->EditorChoice( $listingChoiceObj->editor_choice_id );
                        $listingObj = new Listing();
                         $listingObj->Listing( $listingChoiceObj[0]->listing_id );

						$arr_invoice_listing_choice["invoice_id"]   	= $invoiceObj->getString("id");
						$arr_invoice_listing_choice["listingchoice_id"] = $id;
						$arr_invoice_listing_choice["badge_name"]  		= $listingChoiceObj[0]->name;
						$arr_invoice_listing_choice["listing_title"]  	= $listingObj->title;
                        $arr_invoice_listing_choice["renewal_date"] 	= $listingChoiceObj[0]->renewal_date;
						$arr_invoice_listing_choice["amount"]       	= str_replace(",","",$info["total_fee"]);

						$invoicelistingChoiceObj = new InvoiceListingChoice();
						$invoicelistingChoiceObj->InvoiceListingChoice($arr_invoice_listing_choice);
						$invoicelistingChoiceObj->Save();

						unset($listingChoiceObj);
						unset($invoicelistingChoiceObj);

					}

				if (isset($bill_info["custominvoices"])) foreach ($bill_info["custominvoices"] as $id => $info) {

						$customInvoiceObj = new CustomInvoice();
						$customInvoiceObj->CustomInvoice($id);

						$arr_invoice_custominvoice["invoice_id"]        = $invoiceObj->getString("id");
						$arr_invoice_custominvoice["custom_invoice_id"] = $id;
						$arr_invoice_custominvoice["title"]             = $customInvoiceObj->getString("title");
						$arr_invoice_custominvoice["date"]              = $customInvoiceObj->getString("date");
						$arr_invoice_custominvoice["items"]             = $customInvoiceObj->getTextItems();
						$arr_invoice_custominvoice["items_price"]       = $customInvoiceObj->getTextPrices();
						$arr_invoice_custominvoice["subtotal"]          = str_replace(",","",$info["subtotal"]);
						$arr_invoice_custominvoice["tax"]				= str_replace(",","",$info["tax"]);
						$arr_invoice_custominvoice["amount"]            = str_replace(",","",$info["amount"]);

						$invoiceCustomInvoiceObj = new InvoiceCustomInvoice();
						$invoiceCustomInvoiceObj->InvoiceCustomInvoice($arr_invoice_custominvoice);
						$invoiceCustomInvoiceObj->Save();

						unset($customInvoiceObj);
						unset($invoiceCustomInvoiceObj);

					}


		}
		}
	

	

	$spListingData = Sql::SponserListing($userAccountId);
        return view('front.sponsers_billing_pay',compact('spListingData','payment_method','bill_info','headerTitle','headerAuthor','payment_message','headerDescription','headerKeywords','footer_text','payment_message','payment_tax_status'));

       
      
    }

 }


 function sponsor_billing_invoice(Request $request)
 {
 	$error = false;
 	$id=$request->id;
 	$sess_id=Functions::sess_getAccountIdFromSession();

 	if ($id) {
		$invoiceObj = new Invoice();
		$invoiceObj->Invoice($id);
		if ((!$invoiceObj->id) || ($invoiceObj->id <= 0)) $error = true;
		if ($sess_id != $invoiceObj->account_id) $error = true;
		

	} else {
		$error = true;
	}
	if (!$error) {

		// Invoice info
		if ($invoiceObj->getString("status") == "N") 
		{
			$invoiceObj->setString("status","P");
			$invoiceObj->Save();
		}

		// Account info
		$contactObj = new Contact();
		$contactObj->Contact($sess_id);

		// Listing info
		$sql = "SELECT * FROM Invoice_Listing WHERE invoice_id = '{$id}'";
		$rs = Sql::fetch($sql);

		$i=0;
		
		foreach($rs as $row)
		{
			$listingObj = new Listing();
			$listingObj->Listing($row->listing_id);
			$arr_invoice_listing[$i] = $row;
			$arr_invoice_listing[$i]->renewal_date = Functions::format_date($row->renewal_date);

	if (config('params.LISTINGTEMPLATE_FEATURE') == "on" && config('params.CUSTOM_LISTINGTEMPLATE_FEATURE') == "on") {
				if ($listingObj->getNumber("listingtemplate_id")) {
					$listingTemplateObj = new ListingTemplate($listingObj->getNumber("listingtemplate_id"));
					$arr_invoice_listing[$i]["listingtemplate"] = $listingTemplateObj->getString("title");
				}
			}
		
			$arr_invoice_listing[$i]->listing_address = $listingObj->getString("address");
			$arr_invoice_listing[$i++]->listing_title = $row->listing_title;
				
			unset($listingObj);
		}

	// Listing Choice info

		$sql = "SELECT * FROM Invoice_ListingChoice WHERE invoice_id = '{$id}'";
		$rs = Sql::fetch($sql);

		$i=0;
		foreach($rs as $row)
		{
			$listingChoiceObj = new ListingChoice();
			$listingChoiceObj->ListingChoice("", "", $row->listingchoice_id);
			$listingObjAux = new Listing();
			$listingObjAux->Listing($listingChoiceObj->getNumber("listing_id"));

			$arr_invoice_listingchoice[$i] = $row;
			$arr_invoice_listingchoice[$i]->rental_id = $listingObjAux->getNumber("id");
			$arr_invoice_listingchoice[$i]->listing_address = $listingObjAux->getString("address");
			$arr_invoice_listingchoice[$i]->renewal_date = ($listingChoiceObj->renewal_date != "
				-00-00" ? Functions::format_date($listingChoiceObj->renewal_date) : Functions::format_date($listingChoiceObj->getNextRenewalDate()));

			$arr_invoice_listingchoice[$i]->listing_title = $row->listing_title;
			$arr_invoice_listingchoice[$i++]->badge_name = $row->badge_name;
			unset($listingChoiceObj);
		}

		$sql = "SELECT * FROM Invoice_CustomInvoice WHERE invoice_id = '{$id}'";
		$rs = Sql::fetch($sql);

		$i=0;
		foreach($rs as $row) {
			$arr_invoice_custominvoice[$i] = $row;
			$i++;
		}






	}


 	 return view('front.sponsers_billing_invoice',compact('contactObj','invoiceObj','arr_invoice_listing','arr_invoice_listingchoice','arr_invoice_custominvoice'));
 	
 }
 function sponsor_custominvoice_items(Request $request)
	 {

	 	$id=$request->id;
	 	$customInvoiceItems = false;

	//if (!$view || $view != "payment_log") {
		if ($id) {
			$customInvoice = new CustomInvoice();
			$customInvoice->CustomInvoice($id);
			if ($customInvoice->getNumber("account_id") != Functions::sess_getAccountIdFromSession()) {
				exit;
			}
			$customInvoiceItems = $customInvoice->getItems();

				$params['type'] = 'Home Page';
	            $meta_data = Sql::HomePageSummerSale($params);
	            
	            $header_author = Sql::HomePageHeaderAuthor();
	            
	            //$header_searching_tag = Sql::HomePageHeaderSearchingTag();
	            
	      return view('front.sponsers_custominvoice_items',compact('meta_data','header_author','header_searching_tag','customInvoiceItems','customInvoice'));
		} else {
			exit;
		}

	} 

	function payment_method_authorize(Request $request)
	{
		if($request->_token==csrf_token())
	    {
	    	$pay=$request->pay;
	    	$payment_message='';
	    	$try_again_message='';
	    if ($pay)
	    {

	    $validationok = "yes";
	    		
	    $x_cc_exp_date = explode("/", $request->x_exp_date);
		$x_cc_month_exp_date = $x_cc_exp_date[0];
		$x_cc_year_exp_date = $x_cc_exp_date[1];

		// installments calculation
		$installments = 0;

		$installments += 1; // increase one installment because the first is charged immediately.
		if ($installments > 36) $installments = 36;
		$renewal_increase = $installments;		

			if ($pay && $validationok == "yes") 
			{

				// Configuration
				$x_login       = '669wsXGS5Ss';
				$x_tran_key    = '86DGhm47K2268zVF';
				$x_customer_ip = $_SERVER["REMOTE_ADDR"];
				if ( $request->x_listing_ids ) {
					$x_description = "Rental Listing(s): " . str_replace('::', ', ', $request->x_listing_ids);
				} elseif ( $request->x_badges_ids) {
					$x_description = "Specials Renewal";
				} else {
					$x_description = "Item Renewal";
				}
				$x_invoice_num = $request->x_invoice_num;
				$x_cust_id     = $request->x_cust_id;
				$x_card_num    = $request->x_card_num;
				$x_exp_date    = $request->x_exp_date;
				$x_card_code   = $request->x_card_code;
				$x_first_name  = $request->x_first_name;
				$x_last_name   = $request->x_last_name;
				$x_company     = $request->x_company;
				$x_address     = $request->x_address;
				$x_city        = $request->x_city;
				$x_state       = $request->x_state;
				$x_zip         = $request->x_zip;
				$x_country     = $request->x_country;
				$x_phone       = $request->x_phone;
				$x_email       = $request->x_email;
				$x_amount      = $request->x_amount;
				$x_domain_id   = $request->x_domain_id;
				$domain_id = $x_domain_id;
				//$x_package_id  = $request->x_package_id;


				if (config('params.AUTHORIZERECURRING_FEATURE') == "on") {
					$x_start_date        = date("Y-m-d");
					$x_expiration_date   = date("Y-m", mktime(0, 0, 0, $x_cc_month_exp_date, 1, $x_cc_year_exp_date));
					$x_total_occurrences = $installments;
					$x_length            = AUTHORIZE_RECURRINGLENGTH;
					$x_unit              = AUTHORIZE_RECURRINGUNIT;
				} else {
					$x_email_customer   = "TRUE";
					$x_delim_data       = "TRUE";
					$x_delim_char       = "||";
					$x_encap_char       = "";
					$x_type             = "AUTH_CAPTURE";
					$x_test_request     = "FALSE";
					$x_relay_response   = "FALSE";
					$x_duplicate_window = "120";
					$x_method           = "CC";
				}

				$x_listing_ids           = $request->x_listing_ids;
				$x_listing_amounts       = $request->x_listing_amounts;
				$x_badges_ids            = $request->x_badges_ids;
				$x_badges_amounts        = $request->x_badges_amounts;
				/*$x_event_ids             = $_POST["x_event_ids"];
				$x_event_amounts         = $_POST["x_event_amounts"];
				$x_banner_ids            = $_POST["x_banner_ids"];
				$x_banner_amounts        = $_POST["x_banner_amounts"];
				$x_classified_ids        = $_POST["x_classified_ids"];
				$x_classified_amounts    = $_POST["x_classified_amounts"];
				$x_article_ids           = $_POST["x_article_ids"];
				$x_article_amounts       = $_POST["x_article_amounts"];*/
				$x_custominvoice_ids     = $request->x_custominvoice_ids;
				$x_custominvoice_amounts = $request->x_custominvoice_amounts;


				$content="x_version=3.1"
					."&x_login=".urlencode(trim($x_login))
					."&x_email=".urlencode(trim($x_email))
					."&x_first_name=".urlencode(trim($x_first_name))
					."&x_last_name=".urlencode(trim($x_last_name))
					."&x_company=".urlencode(trim($x_company))
					."&x_address=".urlencode(trim($x_address))
					."&x_city=".urlencode(trim($x_city))
					."&x_state=".urlencode(trim($x_state))
					."&x_zip=".urlencode(trim($x_zip))
					."&x_country=".urlencode(trim($x_country))
					."&x_phone=".urlencode(trim($x_phone))
					."&x_email_customer=".urlencode(trim($x_email_customer))
					."&x_amount=".urlencode(trim($x_amount))
					."&x_test_request=".urlencode(trim($x_test_request))
					."&x_duplicate_window=".urlencode(trim($x_duplicate_window))
					."&x_customer_ip=".urlencode(trim($x_customer_ip))
					."&x_invoice_num=".urlencode(trim($x_invoice_num))
					."&x_description=".urlencode(trim($x_description))
					."&x_cust_id=".urlencode(trim($x_cust_id))
					."&x_card_num=".urlencode(trim($x_card_num))
					."&x_card_code=".urlencode(trim($x_card_code))
					."&x_exp_date=".urlencode(trim($x_exp_date))
					."&x_first_name=".urlencode(trim($x_first_name))
					."&x_last_name=".urlencode(trim($x_last_name))
					."&x_delim_data=".urlencode(trim($x_delim_data))
					."&x_delim_char=".urlencode(trim($x_delim_char))
					."&x_encap_char=".urlencode(trim($x_encap_char))
					."&x_type=".urlencode(trim($x_type))
					."&x_method=".urlencode(trim($x_method))
					."&x_relay_response=".urlencode(trim($x_relay_response))
					."&x_cust_ip=".urlencode(trim($x_customer_ip))
					."&x_listing_ids=".urlencode($x_listing_ids)
					."&x_listing_amounts=".urlencode($x_listing_amounts)
					."&x_badges_ids=".urlencode($x_badges_ids)
					."&x_badges_amounts=".urlencode($x_badges_amounts)
					."&x_custominvoice_ids=".urlencode($x_custominvoice_ids)
					."&x_custominvoice_amounts=".urlencode($x_custominvoice_amounts)
					."&x_tran_key=".urlencode(trim($x_tran_key));

					$ch = curl_init();

				curl_setopt($ch, CURLOPT_URL, "https://test.authorize.net/gateway/transact.dll");
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

				if (config('params.AUTHORIZERECURRING_FEATURE') == "on") {
					curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type: text/xml"));
					curl_setopt($ch, CURLOPT_HEADER, 1);
				} else {
					$agent = "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.0)";
					$ref = "http://".$_SERVER["HTTP_HOST"].$_SERVER["PHP_SELF"];
					curl_setopt($ch, CURLOPT_NOPROGRESS, 1);
					curl_setopt($ch, CURLOPT_VERBOSE, 1);
					curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
					curl_setopt($ch, CURLOPT_TIMEOUT, 120);
					curl_setopt($ch, CURLOPT_USERAGENT, $agent);
					curl_setopt($ch, CURLOPT_REFERER, $ref);
				}
				curl_setopt($ch, CURLOPT_POSTFIELDS, $content);
				curl_setopt($ch, CURLOPT_POST, 1);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

				$curl_response = curl_exec($ch);
	            
	            $curl_response = str_replace('"', "", $curl_response);

				curl_close($ch);

				if ($curl_response) {	

					$details = explode($x_delim_char, $curl_response);

						$transaction_authorize["response_code"]         = $details[0];
						$transaction_authorize["response_subcode"]      = $details[1];
						$transaction_authorize["response_reason_code"]  = $details[2];
						$transaction_authorize["response_reason_text"]  = $details[3];
						$transaction_authorize["approval_code"]         = $details[4];
						$transaction_authorize["avs_result_code"]       = $details[5];
						$transaction_authorize["transaction_id"]        = $details[6];
						$transaction_authorize["invoice_number"]        = $details[7];
						$transaction_authorize["description"]           = $details[8];
						$transaction_authorize["amount"]                = $details[9];
						$transaction_authorize["method"]                = $details[10];
						$transaction_authorize["transaction_type"]      = $details[11];
						$transaction["account_id"]                      = $details[12];
						$transaction_authorize["cardholder_first_name"] = $details[13];
						$transaction_authorize["cardholder_last_name"]  = $details[14];
						$transaction_authorize["company"]               = $details[15];
						$transaction_authorize["billing_address"]       = $details[16];
						$transaction_authorize["city"]                  = $details[17];
						$transaction_authorize["state"]                 = $details[18];
						$transaction_authorize["zip"]                   = $details[19];
						$transaction_authorize["country"]               = $details[20];
						$transaction_authorize["phone"]                 = $details[21];
						$transaction_authorize["fax"]                   = $details[22];
						$transaction_authorize["email"]                 = $details[23];
						$transaction_authorize["ship_to_first_name"]    = $details[24];
						$transaction_authorize["ship_to_last_name"]     = $details[25];
						$transaction_authorize["ship_to_company"]       = $details[26];
						$transaction_authorize["ship_to_address"]       = $details[27];
						$transaction_authorize["ship_to_city"]          = $details[28];
						$transaction_authorize["ship_to_state"]         = $details[29];
						$transaction_authorize["ship_to_zip"]           = $details[30];
						$transaction_authorize["ship_to_country"]       = $details[31];
						$transaction_authorize["tax_amount"]            = $details[32];
						$transaction_authorize["duty_amount"]           = $details[33];
						$transaction_authorize["freight_amount"]        = $details[34];
						$transaction_authorize["tax_example_flag"]      = $details[35];
						$transaction_authorize["po_number"]             = $details[36];
						$transaction_authorize["md5_hash"]              = $details[37];
						$transaction_authorize["cvv2"]                  = $details[38];
						$transaction_authorize["cavv"]                  = $details[39];
						$transaction["ip"]                              = $details[68];
						$transaction["listing_ids"]                     = $details[69];
						$transaction["listing_amounts"]                 = $details[70];
						$transaction["badges_ids"]                      = $details[71];
						$transaction["badges_amounts"]                  = $details[72];
						/*$transaction["event_ids"]                       = $details[73];
						$transaction["event_amounts"]                   = $details[74];
						$transaction["banner_ids"]                      = $details[75];
						$transaction["banner_amounts"]                  = $details[76];
						$transaction["classified_ids"]                  = $details[77];
						$transaction["classified_amounts"]              = $details[78];
						$transaction["article_ids"]                     = $details[79];
						$transaction["article_amounts"]                 = $details[80];*/
						$transaction["custominvoice_ids"]               = $details[73];
						$transaction["custominvoice_amounts"]           = $details[74];
						$transaction["transaction_id"]                  = $transaction_authorize["transaction_id"];
						$transaction["recurring"]                       = "n";

						
						if ($transaction_authorize["response_code"] == "1") {
							$transaction["transaction_status"] = 'Approved';
							$transaction_status = "ok";
						} elseif ($transaction_authorize["response_code"] == "2") {
							$transaction["transaction_status"] = 'Declined';
						} elseif ($transaction_authorize["response_code"] == "3") {
							$transaction["transaction_status"] = 'Error';
						} else {
							$transaction["transaction_status"] = 'Failed';
						}



					$transaction_authorize["trans_time"]            = date("Y-m-d H:i:s");
					$accountObj = new Account();
					$accountObj->Account($transaction["account_id"]);

					$transaction["username"]                        = $accountObj->getString("username");
					$transaction["transaction_datetime"]            = $transaction_authorize["trans_time"];
					$transaction["transaction_subtotal"]            = $request->x_subtotal_amount;
					$transaction["transaction_tax"]					= $request->x_tax_amount;
					$transaction["transaction_amount"]              = $transaction_authorize["amount"];
					$transaction["transaction_currency"]            = config('params.PAYMENT_CURRENCY');
					$transaction["system_type"]                     = "authorize";
					$transaction["notes"]                           = "";
				}

				if(isset($transaction_status)) if ($transaction_status == "ok") {

					$payment_success = "y";

					$payment_message .= "<p class=\"successMessage\">Status: ".$transaction["transaction_status"]."<br />\n";

					$payment_message .= "Transaction Code : ".$transaction["transaction_id"]."<br />\n";
					$payment_message .= $transaction_authorize["response_reason_text"]."<br />\n";
					
					$payment_message .= "<br />\n".config('params.LANG_MSG_TRANSACTIONS_MAY_NOT_OCCUR_IMMEDIATELY')."<br />\n".config('params.LANG_MSG_AFTER_PAYMENT_IS_PROCESSED_INFO_ABOUT')."<br />\n".config('params.LANG_MSG_MAY_BE_FOUND_IN_YOUR')." <a href=\"".url('sponsors/transactions')."\">transaction history</a>\n";
					$payment_message .= "</p>";

					$transaction["return_fields"] = Functions::system_array2nvp($transaction_authorize, " || ");

					$paymentLogObj =new PaymentLog();
					$paymentLogObj->PaymentLog($transaction);
					$paymentLogObj->Save(1);

					$listing_ids           = explode("::",$transaction["listing_ids"]);
					$listing_amounts       = explode("::",$transaction["listing_amounts"]);
					$badges_ids            = explode("::",$transaction["badges_ids"]);
					$badges_amounts        = explode("::",$transaction["badges_amounts"]);
					/*$event_ids             = explode("::",$transaction["event_ids"]);
					$event_amounts         = explode("::",$transaction["event_amounts"]);
					$banner_ids            = explode("::",$transaction["banner_ids"]);
					$banner_amounts        = explode("::",$transaction["banner_amounts"]);
					$classified_ids        = explode("::",$transaction["classified_ids"]);
					$classified_amounts    = explode("::",$transaction["classified_amounts"]);
					$article_ids           = explode("::",$transaction["article_ids"]);
					$article_amounts       = explode("::",$transaction["article_amounts"]);*/
					$custominvoice_ids     = explode("::",$transaction["custominvoice_ids"]);
					$custominvoice_amounts = explode("::",$transaction["custominvoice_amounts"]);	



					if (!empty($listing_ids[0])) {

							$listingStatus = new ItemStatus();

							$amountAux = 0;
							$levelObj = new ListingLevel();
							$levelObj->ListingLevel();

							foreach($listing_ids as $each_listing_id){

								$listingObj = new Listing();
								$listingObj->Listing($each_listing_id);
				$listingObj->setString("renewal_date", $listingObj->getNextRenewalDate($renewal_increase));			
					
								
								Functions::setting_get("listing_approve_paid", $listing_approve_paid);
									
								if ($listing_approve_paid){
									$listingObj->setString("status", $listingStatus->getDefaultStatus());
								}else{ 
									$listingObj->setString("status", "A");
								}
															
								$listingObj->Save();

								$category_amount = 0;
								$sql = "SELECT category_id FROM Listing_Category WHERE listing_id = ".$listingObj->getString("id")."";
								$result = Sql::fetch($sql);
								if(count($result)){
						foreach($result as $row){
							$category_amount++;
						}

					}

							$transaction_listing_log["payment_log_id"] = $paymentLogObj->getNumber("id");
							$transaction_listing_log["listing_id"]     = $each_listing_id;
							$transaction_listing_log["listing_title"]  = $listingObj->getString("title", false);
	                        $transaction_listing_log["level"]          = $listingObj->getString("level");
							$transaction_listing_log["level_label"]    = $levelObj->showLevel($listingObj->getString("level"));
							$transaction_listing_log["renewal_date"]   = $listingObj->getString("renewal_date");
							$transaction_listing_log["discount_id"]    = $listingObj->getString("discount_id");
								$transaction_listing_log["categories"]     = ($category_amount) ? $category_amount : 0;
								$transaction_listing_log["amount"]         = str_replace(",","",$listing_amounts[$amountAux]);
								$amountAux++;

								$transaction_listing_log["extra_categories"] = 0;


								if (($category_amount > 0) && (($category_amount - $levelObj->getFreeCategory($listingObj->getString("level"))) > 0)) {
							$transaction_listing_log["extra_categories"] = $category_amount - $levelObj->getFreeCategory($listingObj->getString("level"));
								} else {
									$transaction_listing_log["extra_categories"] = 0;
								}

								$transaction_listing_log["listingtemplate_title"] = "";
								/*if (LISTINGTEMPLATE_FEATURE == "on" && CUSTOM_LISTINGTEMPLATE_FEATURE == "on") {
									if ($listingObj->getString("listingtemplate_id")) {
										$listingTemplateObj = new ListingTemplate($listingObj->getString("listingtemplate_id"));
										$transaction_listing_log["listingtemplate_title"] = $listingTemplateObj->getString("title", false);
									}
								}*/

								$paymentListingLogObj = new PaymentListingLog();
								$paymentListingLogObj->PaymentListingLog($transaction_listing_log, $domain_id);
								$paymentListingLogObj->Save();

							}

							unset($listingObj);

						}	

					if (!empty($badges_ids[0])) {

							$listingChoiceStatus = new ItemStatus();

							$amountAux = 0;

							foreach($badges_ids as $each_badges_id){

								$listingChoiceObj = new ListingChoice();
								$listingChoiceObj->ListingChoice("", "", $each_badges_id);

								$listingChoiceObj->setString("renewal_date", $listingChoiceObj->getNextRenewalDate($renewal_increase));	

								$listingObjAux = new Listing();
								$listingObjAux->Listing($listingChoiceObj->getNumber("listing_id"));
								$editorObj = new EditorChoice();
								$editorObj->EditorChoice($listingChoiceObj->getNumber("editor_choice_id") );

								//$transaction_listingchoice_log = $row;
								
								
								Functions::setting_get("listing_approve_paid", $listing_approve_paid);
									
								if ($listing_approve_paid){
									$listingChoiceObj->setString("status", $listingChoiceStatus->getDefaultStatus());
								}else{ 
									$listingChoiceObj->setString("status", "A");
								}
															
								$listingChoiceObj->Save();

								$transaction_listingchoice_log["payment_log_id"]  = $paymentLogObj->getNumber("id");
								$transaction_listingchoice_log["listingchoice_id"]= $each_badges_id;
								$transaction_listingchoice_log["listing_id"]      = $listingObjAux->getNumber("id");
								$transaction_listingchoice_log["listing_title"]   = $listingObjAux->getString("title");
								$transaction_listingchoice_log["renewal_date"]    = $listingChoiceObj->getString("renewal_date");
								//$transaction_listingchoice_log["discount_id"]   = $listingObj->getString("discount_id");
								$transaction_listingchoice_log["badge_name"]      = $editorObj->getString("name");
								$transaction_listingchoice_log["amount"]          = str_replace(",","",$badges_amounts[$amountAux]);
								$amountAux++;

								$paymentListingChoiceLogObj = new PaymentListingChoiceLog();
								$paymentListingChoiceLogObj->PaymentListingChoiceLog($transaction_listingchoice_log, $domain_id);
								$paymentListingChoiceLogObj->Save();

							}

							unset($editorObj);
							unset($listingObjAux);
							unset($listingChoiceObj);

						}
					if (!empty($custominvoice_ids[0])) {

							$amountAux = 0;
							foreach($custominvoice_ids as $each_custominvoice_id){

								$customInvoiceObj = new CustomInvoice();
								$customInvoiceObj->CustomInvoice($each_custominvoice_id);
								$x_tax_amount=$request->x_tax_amount;
								if ($x_tax_amount > 0) {
									$cInvoiceAmount = Functions::payment_calculateTax($customInvoiceObj->getNumber("amount"), $x_tax_amount);
								} else {
									$cInvoiceAmount = $customInvoiceObj->getNumber("amount");
								}
								$customInvoiceObj->setNumber("tax", $x_tax_amount);
								$customInvoiceObj->setNumber("subtotal", $customInvoiceObj->getNumber("amount"));
								$customInvoiceObj->setNumber("amount", $cInvoiceAmount);
								$customInvoiceObj->setString("paid", "y");
								$customInvoiceObj->Save();

								$transaction_custominvoice_log["payment_log_id"]    = $paymentLogObj->getNumber("id");
								$transaction_custominvoice_log["custom_invoice_id"] = $each_custominvoice_id;
								$transaction_custominvoice_log["title"]             = $customInvoiceObj->getString("title");
								$transaction_custominvoice_log["date"]              = $customInvoiceObj->getString("date");
								$transaction_custominvoice_log["items"]             = $customInvoiceObj->getTextItems();
								$transaction_custominvoice_log["items_price"]       = $customInvoiceObj->getTextPrices();
								$transaction_custominvoice_log["amount"]            = $customInvoiceObj->getNumber("subtotal");
								$amountAux++;

								$paymentCustomInvoiceLogObj = new PaymentCustomInvoiceLog();
								$paymentCustomInvoiceLogObj->PaymentCustomInvoiceLog($transaction_custominvoice_log, $domain_id);
								$paymentCustomInvoiceLogObj->Save();

							}

							unset($customInvoiceObj);

						}	

				}
				else {

					$payment_message .= "<p class=\"errorMessage\">Status: ".$transaction["transaction_status"]."<br />\n";
					if (config('params.AUTHORIZERECURRING_FEATURE') == "on") {
						$payment_message .= $transaction_authorize["response_code"]." - ".$transaction_authorize["response_text"]."<br />\n";
					} else {
						$payment_message .= $transaction_authorize["response_reason_code"]." - ".$transaction_authorize["response_reason_text"]."<br />\n";
					}

					if ($process == "signup") $try_again_message = "\n<a href=\"".url('signup/payment/authorize')."\">".config('params.LANG_MSG_CLICK_HERE_TO_TRY_AGAIN')."</a>\n";
					/*elseif ($process == "claim") $try_again_message = "\n<a style=\"cursor:pointer\" href=\"".DEFAULT_URL."/".MEMBERS_ALIAS."/".$process."/payment.php?payment_method=authorize&claimlistingid=".$claimlistingid."\">".system_showText(LANG_MSG_CLICK_HERE_TO_TRY_AGAIN)."</a>\n";*/
					else $try_again_message = "\n<a href=\"".url('sponsors/billing')."\">".config('params.LANG_MSG_CLICK_HERE_TO_TRY_AGAIN')."</a>\n";

				}

				$payment_message .= $try_again_message."</p>\n";


			}

	    }
	    else 
	    {
		$payment_message .= "<p class=\"errorMessage\">\n";
		$payment_message .= "Status:Error<br />\n";
		$payment_message .= config('params.LANG_MSG_PAYMENT_INVALID_PARAMS')."<br />\n";
		$payment_message .= "<a href=\"".url('sponsors/billing')."\">".config('params.LANG_MSG_CLICK_HERE_TO_TRY_AGAIN')."</a></p>\n";
		}


		DB::connection('domain')->setFetchMode(PDO::FETCH_CLASS);

	    $userAccountId = Session::get('SESS_ACCOUNT_ID');
        $profileSp = Functions::SponsersProfile();
      	$ownerName= '';
        if(!empty($profileSp))
        {
           $ownerName = $profileSp[0]->first_name.' '.$profileSp[0]->last_name.', ';
       }
       $meta_data = Sql::sponsersMeta($userAccountId);
       $headerTitle = $ownerName.'Welcome to the Sponsor Section -'.$meta_data[0]->value;
       $headerAuthor = $meta_data[1]->value;
       $headerDescription = $meta_data[2]->value;
       $headerKeywords = $meta_data[3]->value;
       $footer_text = Sql::footerText();
       
       
       $spListingData = Sql::SponserListing($userAccountId);
        return view('front.processpayment',compact('spListingData','payment_message','headerTitle','headerAuthor','headerDescription','headerKeywords','footer_text'));	
	    

	    }
	    else
	    {
	    	return false;
	    }


	    
	}   

	function sponser_listing_transactions(Request $request)
 {
        $userAccountId = Session::get('SESS_ACCOUNT_ID');
        $id=($request->id)?$request->id:0;
        $accId=Functions::sess_getAccountIdFromSession();
        $profileSp = Functions::SponsersProfile();
     	$ownerName= '';
        if(!empty($profileSp))
        {
           $ownerName = $profileSp[0]->first_name.' '.$profileSp[0]->last_name.', ';
       }
       $meta_data = Sql::sponsersMeta($userAccountId);
       $headerTitle = $ownerName.'Welcome to the Sponsor Section -'.$meta_data[0]->value;
       $headerAuthor = $meta_data[1]->value;
       $headerDescription = $meta_data[2]->value;
       $headerKeywords = $meta_data[3]->value;
       $footer_text = Sql::footerText();
       
       
       if (config('params.INVOICEPAYMENT_FEATURE') == "on") {
        /*$invoiceStatusObj = new InvoiceStatus();
        unset($sql_where);
        unset($where);
        $sql_where[] = " hidden = 'n' ";
        if ($acctId) {
            $sql_where[] = " account_id = $acctId ";
        }
        if ($invoiceStatusObj->getDefault()) {
            $sql_where[] = " status != '".$invoiceStatusObj->getDefault()."' ";
        }
        if ($sql_where) {
            $where .= " ".implode(" AND ", $sql_where)." ";
        }*/
           
        ///$pageObj  = new pageBrowsing("Invoice", $screen, false, "date DESC", "", "", $where);
        //$invoices = $pageObj->retrievePage("array");
       
    }
     $invoices=Sql::getInvoice('n','N',$accId,'date');
     $transactions=Sql::getTransaction('n',$accId);

    //Retrieving payment logs
 
    	$spListingData = Sql::SponserListing($userAccountId);
        return view('front.sponsers_transactions',compact('spListingData','invoices','transactions','invoice_listing','invoice_listingchoice','headerTitle','headerAuthor','headerDescription','headerKeywords','footer_text'));

 }

  function sponsor_transactions_viewinvoice(Request $request)
 {
        $userAccountId = Session::get('SESS_ACCOUNT_ID');
        $id = $request->id;
		$acctId=Functions::sess_getAccountIdFromSession();
        $profileSp = Functions::SponsersProfile();
      	$ownerName= '';
        if(!empty($profileSp))
        {
           $ownerName = $profileSp[0]->first_name.' '.$profileSp[0]->last_name.', ';
       }
       $meta_data = Sql::sponsersMeta($userAccountId);
       $headerTitle = $ownerName.'Welcome to the Sponsor Section -'.$meta_data[0]->value;
       $headerAuthor = $meta_data[1]->value;
       $headerDescription = $meta_data[2]->value;
       $headerKeywords = $meta_data[3]->value;
       $footer_text = Sql::footerText();

       if($id){

		$sql = "SELECT * FROM Invoice WHERE id = '$id' AND status != 'N' AND hidden = 'n' ";
		if($acctId) $sql .= "AND account_id = '$acctId'";
		$r = Sql::fetch($sql);
		if(count($r) > 0) 
		$invoiceObj = new Invoice();
		$invoiceObj->Invoice($id);

		if($invoiceObj){

			//$invoiceStatusObj   = new InvoiceStatus();
			$listingLevelObj    = new ListingLevel();
			//$eventLevelObj      = new EventLevel();
			//$bannerLevelObj     = new BannerLevel();
			//$classifiedLevelObj = new ClassifiedLevel();
			//$articleLevelObj    = new ArticleLevel();

			$invoice["id"]           = $id;
			$invoice["username"]     = $invoiceObj->getString("username");
			$invoice["account_id"]   = $invoiceObj->getString("account_id");
			$invoice["ip"]           = $invoiceObj->getString("ip");
			$invoice["date"]         = Functions::format_date($invoiceObj->getString("date"),config('params.DEFAULT_DATE_FORMAT')." H:i:s","datetime");
			$invoice["status"]       = Functions::check_status($invoiceObj->getString("status"));
			$invoice["subtotal"]     = $invoiceObj->getString("subtotal_amount");
			$invoice["tax"]			 = Functions::payment_calculateTax($invoice["subtotal"],$invoiceObj->getString("tax_amount"),true,false); 
			$invoice["amount"]       = $invoiceObj->getString("amount");
			$invoice["currency"]     = $invoiceObj->getString("currency");
			$invoice["expire_date"]  = Functions::format_date($invoiceObj->getString("expire_date"),config('params.DEFAULT_DATE_FORMAT'),"date");
			$invoice["payment_date"] = Functions::format_date($invoiceObj->getString("payment_date"),config('params.DEFAULT_DATE_FORMAT')." H:i:s","datetime");
			

			$sql ="SELECT * FROM Invoice_Listing WHERE invoice_id = '$id'";

			$r = Sql::fetch($sql);
			$i=0;
			foreach($r as $row){

				$listingObj = new Listing();
				$listingObj->Listing($row->listing_id);


				$invoice_listing[$i]["invoice_id"]       = $invoiceObj->getString("id");
				$invoice_listing[$i]["listing_id"]       = $row->listing_id;
				$invoice_listing[$i]["listing_title"]    = $row->listing_title;
				$invoice_listing[$i]["listing_address"]  = $listingObj->getString('address');
				$invoice_listing[$i]["discount_id"]      = ($row->discount_id) ? $row->discount_id: 'N/A';
                $invoice_listing[$i]["level"]            = $row->level;
				$invoice_listing[$i]["level_label"]      = $row->level_label;
				$invoice_listing[$i]["renewal_date"]     = (Functions::format_date($row->renewal_date,config('params.DEFAULT_DATE_FORMAT'),"date")) ? Functions::format_date($row->renewal_date,config('params.DEFAULT_DATE_FORMAT'),"date") : 'N/A';
				$invoice_listing[$i]["categories"]       = $row->categories;
				$invoice_listing[$i]["extra_categories"] = $row->extra_categories;
				$invoice_listing[$i]["listingtemplate"]  = $row->listingtemplate_title;
				$invoice_listing[$i]["amount"]           = $row->amount;

				$i++;

			}

			$sql ="SELECT * FROM Invoice_ListingChoice WHERE invoice_id = '$id'";

			$r = Sql::fetch($sql);
			$i=0;
			foreach($r as $row){


				$listingChoiceObj = new ListingChoice();
				$listingChoiceObj->ListingChoice("", "",$row->listingchoice_id);

				$auxListingObj = new Listing();
				$auxListingObj->Listing($listingChoiceObj->getNumber('listing_id'));


				$invoice_listingchoice[$i]["invoice_id"]   		=  $invoiceObj->getString("id");
				$invoice_listingchoice[$i]["listing_id"]   		= $listingChoiceObj->getNumber("listing_id");
				$invoice_listingchoice[$i]["invoice_id"]   		= $invoiceObj->getString("id");
				$invoice_listingchoice[$i]["listingchoice_id"]  = $row->listingchoice_id;
				$invoice_listingchoice[$i]["listing_title"]  	= $row->listing_title;
				$invoice_listingchoice[$i]["listing_address"]   = $auxListingObj->getString('address');
				$invoice_listingchoice[$i]["badge_name"]  		= $row->badge_name;
				$invoice_listingchoice[$i]["renewal_date"]     = (Functions::format_date($row->renewal_date,config('params.DEFAULT_DATE_FORMAT'),"date")) ? Functions::format_date($row->renewal_date,config('params.DEFAULT_DATE_FORMAT'),"date") : 'N/A';
				$invoice_listingchoice[$i]["amount"]           = $row->amount;

				$i++;

			}



			$sql ="SELECT * FROM Invoice_CustomInvoice WHERE invoice_id = '$id'";

			$rs = Sql::fetch($sql);
			$i=0;
			foreach($rs as $row)
			{
				
				$invoice_custominvoice[$i]["invoice_id"]        = $invoiceObj->getString("id");
				$invoice_custominvoice[$i]["custom_invoice_id"] = $row->custom_invoice_id;
				$invoice_custominvoice[$i]["title"]             = $row->title;
				$invoice_custominvoice[$i]["date"]              = $row->date;
				$invoice_custominvoice[$i]["items"]             = $row->items;
				$invoice_custominvoice[$i]["items_price"]       = $row->items_price;
				$invoice_custominvoice[$i]["subtotal"]          = $row->subtotal;
				$invoice_custominvoice[$i]["amount"]            = $row->amount;

				$i++;

			}	
		}	
     
	}
       
       
       $spListingData = Sql::SponserListing($userAccountId);
        return view('front.sponsers_transactions_viewinvoice',compact('spListingData','invoice','invoice_listing','invoice_listingchoice','headerTitle','headerAuthor','headerDescription','headerKeywords','footer_text'));

 }

  function sponsor_transactions_viewtransaction(Request $request)
 {
        $userAccountId = Session::get('SESS_ACCOUNT_ID');
        $id = $request->id;
		$acctId=Functions::sess_getAccountIdFromSession();
        $profileSp = Functions::SponsersProfile();
      	$ownerName= '';
        if(!empty($profileSp))
        {
           $ownerName = $profileSp[0]->first_name.' '.$profileSp[0]->last_name.', ';
       }
       $meta_data = Sql::sponsersMeta($userAccountId);
       $headerTitle = $ownerName.'Welcome to the Sponsor Section -'.$meta_data[0]->value;
       $headerAuthor = $meta_data[1]->value;
       $headerDescription = $meta_data[2]->value;
       $headerKeywords = $meta_data[3]->value;
       $footer_text = Sql::footerText();

       if($id){

		$sql = "SELECT * FROM Payment_Log WHERE id = '$id' AND hidden = 'n' ";
		if($acctId) $sql .= "AND account_id = '$acctId'";
		$transactionObj = Sql::fetch($sql);
		if(count($transactionObj) > 0) 
		

		if($transactionObj){

			//$invoiceStatusObj   = new InvoiceStatus();
			$listingLevelObj    = new ListingLevel();
			//$eventLevelObj      = new EventLevel();
			//$bannerLevelObj     = new BannerLevel();
			//$classifiedLevelObj = new ClassifiedLevel();
			//$articleLevelObj    = new ArticleLevel();

			$transaction["id"]                   = $transactionObj[0]->id;
			$transaction["payment_log_id"]       = $transactionObj[0]->id;
			$transaction["account_id"]           = $transactionObj[0]->account_id;
			$transaction["username"]             = $transactionObj[0]->username;
			$transaction["ip"]                   = $transactionObj[0]->ip;
			$transaction["transaction_id"]       = $transactionObj[0]->transaction_id;
			$transaction["transaction_status"]   = $transactionObj[0]->transaction_status;
			$transaction["transaction_datetime"] = Functions::format_date($transactionObj[0]->transaction_datetime, config('params.DEFAULT_DATE_FORMAT')." H:i:s", "datetime");
			$transaction["transaction_subtotal"] = $transactionObj[0]->transaction_subtotal;
			$transaction["transaction_tax"]      = Functions::payment_calculateTax($transactionObj[0]->transaction_subtotal,$transactionObj[0]->transaction_tax,true,false);
			$transaction["transaction_amount"]   = $transactionObj[0]->transaction_amount;
			$transaction["transaction_currency"] = $transactionObj[0]->transaction_currency;
			$transaction["system_type"]          = $transactionObj[0]->system_type;
			$transaction["recurring"]            = $transactionObj[0]->recurring;
			$transaction["notes"]                = $transactionObj[0]->notes;
			

			$sql ="SELECT * FROM Payment_Listing_Log WHERE payment_log_id = '{$transaction["payment_log_id"]}'";

			$r = Sql::fetch($sql);
			$i=0;
			foreach($r as $row){

				$listingObj = new Listing();
				$listingObj->Listing($row->listing_id);


				$transaction_listing_log[$i]["listing_id"]       = $row->listing_id;
				$transaction_listing_log[$i]["listing_title"]    = $row->listing_title;
				$transaction_listing_log[$i]["listing_address"]  = $listingObj->getString('address');
				$transaction_listing_log[$i]["discount_id"]      = ($row->discount_id) ? $row->discount_id: 'N/A';
                $transaction_listing_log[$i]["level"]            = $row->level;
				$transaction_listing_log[$i]["level_label"]      = $row->level_label;
				$transaction_listing_log[$i]["renewal_date"]     = (Functions::format_date($row->renewal_date,config('params.DEFAULT_DATE_FORMAT'),"date")) ? Functions::format_date($row->renewal_date,config('params.DEFAULT_DATE_FORMAT'),"date") : 'N/A';
				$transaction_listing_log[$i]["categories"]       = $row->categories;
				$transaction_listing_log[$i]["extra_categories"] = $row->extra_categories;
				$transaction_listing_log[$i]["listingtemplate"]  = $row->listingtemplate_title;
				$transaction_listing_log[$i]["amount"]           = $row->amount;

				$i++;

			}

			$sql ="SELECT * FROM Payment_ListingChoice_Log WHERE payment_log_id = '{$transaction["payment_log_id"]}'";


			$r = Sql::fetch($sql);
			$i=0;
			foreach($r as $row){


				$listingChoiceObj = new ListingChoice();
				$listingChoiceObj->ListingChoice("", "",$row->listingchoice_id);

				$auxListingObj = new Listing();
				$auxListingObj->Listing($listingChoiceObj->getNumber('listing_id'));


			
				$transaction_listingchoice_log[$i]["listing_id"]   		= $listingChoiceObj->getNumber("listing_id");
				$transaction_listingchoice_log[$i]["invoice_id"]   		= $transactionObj[0]->id;
				$transaction_listingchoice_log[$i]["listingchoice_id"]  = $row->listingchoice_id;
				$transaction_listingchoice_log[$i]["listing_title"]  	= $row->listing_title;
				$transaction_listingchoice_log[$i]["listing_address"]   = $auxListingObj->getString('address');
				$transaction_listingchoice_log[$i]["badge_name"]  		= $row->badge_name;
				$transaction_listingchoice_log[$i]["renewal_date"]     = (Functions::format_date($row->renewal_date,config('params.DEFAULT_DATE_FORMAT'),"date")) ? Functions::format_date($row->renewal_date,config('params.DEFAULT_DATE_FORMAT'),"date") : 'N/A';
				$transaction_listingchoice_log[$i]["amount"]           = $row->amount;

				$i++;
		}	
     
	}
       }
       
       $spListingData = Sql::SponserListing($userAccountId);
        return view('front.sponsers_transactions_viewtransaction',compact('spListingData','transaction','transaction_listing_log','transaction_listingchoice_log','headerTitle','headerAuthor','headerDescription','headerKeywords','footer_text'));

 }

}