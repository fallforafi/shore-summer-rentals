<?php 
namespace App\Http\Controllers;
use DB;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Validator, Input, Redirect;
use App\Models\Domain\Sql;
use App\Functions\Functions;

class ContactusController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/
    protected $layout = 'layouts.search';
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//$this->middleware('auth');
	}
    

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
	   $params=array();
       $footer_text = Sql::footerText();
       $params['type'] = 'Home Page';
       $meta_data = Sql::HomePageSummerSale($params);
       $header_author = Sql::HomePageHeaderAuthor();
       $header_searching_tag = Sql::HomePageHeaderSearchingTag();  
	   return view('front.contact_us',compact('meta_data','header_author','header_searching_tag','footer_text')); 
    }
    
	public function store(Request $request)
	{
		
        if($request->_token==csrf_token())
        {
            	$validation = array('name' => 'required|max:30',
    			'email' => 'required|email|max:30',
    			'phone' => 'required',
                'title' => 'required|min:6|max:200',
                'messageBody' => 'required'
                );
    		
    		$validator = Validator::make($request->all(),$validation);
    
    		if ($validator->fails()) {
    			return redirect()->back()->withErrors($validator->errors())->withInput();
    		}

            $to      = $request->email;
            $subject = $request->title;
            $message = $request->messageBody;
            $headers='';
        
            Functions::sendEmail($to,$subject,$message,$headers);
    
            return redirect('contact-us')->withInput();
        }
        else
        {
            return redirect('contact-us');
        }  
	}
}