<?php 
namespace App\Http\Controllers;
use DB;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Validator, Input, Redirect, Session, Cookie,Mail;
use App\Models\Domain\Sql;
use App\Models\Main\MainSql;
use App\Functions\Functions;
use App\Functions\FormValidator;
use App\Functions\DiscountCode;
use App\Functions\ListingLevel;
use App\Functions\Listing;
use App\Functions\Rate;
use App\Functions\RateInformation;
use App\Functions\ItemStatus;
use App\Functions\ListingChoice;
use App\Functions\Account;
use App\Functions\Location4;
use App\Functions\EditorChoice;
use App\Functions\Contact;
use App\Functions\Setting;
use App\Functions\Review;
use App\Functions\Lead;
use App\Functions\ListingCategory;
use App\Functions\Listing_Category;
use App\Functions\Profile;
use App\Functions\Image;
use App\Functions\Account_Domain;

class SponserController extends Controller  {

    protected $layout = 'layouts.search';
    
    /**
		 * @var integer
		 * @access Privateo
		 */
		var $id;
		/**
		 * @var integer
		 * @access Private
		 */
		var $account_id;
		/**
		 * @var integer
		 * @access Private
		 */
		var $image_id;
		/**
		 * @var integer
		 * @access Private
		 */
		var $thumb_id;
		/**
		 * @var integer
		 * @access Private
		 */
		var $promotion_id;
		/**
		 * @var integer
		 * @access Private
		 */
		var $location_1;
		/**
		 * @var integer
		 * @access Private
		 */
		var $location_2;
		/**
		 * @var integer
		 * @access Private
		 */
		var $location_3;
		/**
		 * @var integer
		 * @access Private
		 */
		var $location_4;
		/**
		 * @var integer
		 * @access Private
		 */
		var $location_5;
		/**
		 * @var date
		 * @access Private
		 */
		var $renewal_date;
		/**
		 * @var integer
		 * @access Private
		 */
		var $discount_id;
		/**
		 * @var integer
		 * @access Private
		 */
		var $reminder;
		/**
		 * @var date
		 * @access Private
		 */
		var $updated;
		/**
		 * @var date
		 * @access Private
		 */
		var $entered;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $title;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $seo_title;
		/**
		 * @var char
		 * @access Private
		 */
		var $claim_disable;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $friendly_url;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $email;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $url;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $display_url;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $address;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $address2;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $zip_code;
        /**
		 * @var varchar
		 * @access Private
		 */
		var $zip5;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $phone;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $fax;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $description;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $seo_description;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $long_description;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $video_snippet;
        /**
		 * @var varchar
		 * @access Private
		 */
		var $video_description;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $keywords;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $seo_keywords;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $attachment_file;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $attachment_caption;
        /**
		 * @var varchar
		 * @access Private
		 */
		var $features;
        /**
		 * @var integer
		 * @access Private
		 */
		var $price;
        /**
		 * @var varchar
		 * @access Private
		 */
		var $facebook_page;
		/**
		 * @var char
		 * @access Private
		 */
		var $status;
        /**
		 * @var char
		 * @access Private
		 */
		var $suspended_sitemgr;
		/**
		 * @var integer
		 * @access Private
		 */
		var $level;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $locations;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $hours_work;
		/**
		 * @var integer
		 * @access Private
		 */
		var $listingtemplate_id;
        /**
		 * @var varchar
		 * @access Private
		 */
		var $custom_text0;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_text1;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_text2;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_text3;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_text4;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_text5;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_text6;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_text7;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_text8;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_text9;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_short_desc0;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_short_desc1;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_short_desc2;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_short_desc3;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_short_desc4;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_short_desc5;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_short_desc6;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_short_desc7;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_short_desc8;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_short_desc9;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_long_desc0;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_long_desc1;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_long_desc2;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_long_desc3;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_long_desc4;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_long_desc5;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_long_desc6;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_long_desc7;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_long_desc8;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_long_desc9;
        /**
		 * @var char
		 * @access Private
		 */
		var $custom_checkbox0;
		/**
		 * @var char
		 * @access Private
		 */
		var $custom_checkbox1;
		/**
		 * @var char
		 * @access Private
		 */
		var $custom_checkbox2;
		/**
		 * @var char
		 * @access Private
		 */
		var $custom_checkbox3;
		/**
		 * @var char
		 * @access Private
		 */
		var $custom_checkbox4;
		/**
		 * @var char
		 * @access Private
		 */
		var $custom_checkbox5;
		/**
		 * @var char
		 * @access Private
		 */
		var $custom_checkbox6;
		/**
		 * @var char
		 * @access Private
		 */
		var $custom_checkbox7;
		/**
		 * @var char
		 * @access Private
		 */
		var $custom_checkbox8;
		/**
		 * @var char
		 * @access Private
		 */
		var $custom_checkbox9;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_dropdown0;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_dropdown1;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_dropdown2;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_dropdown3;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_dropdown4;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_dropdown5;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_dropdown6;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_dropdown7;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_dropdown8;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_dropdown9;
		/**
		 * @var integer
		 * @access Private
		 */
		var $number_views;
		/**
		 * @var integer
		 * @access Private
		 */
		var $avg_review;
        /*
         * @var real
         * @access Private
         */
        var $latitude;
        /*
         * @var real
         * @access Private
         */
        var $longitude;
		/**
		 * @var integer
		 * @access Private
		 */
		var $map_zoom;

		/**
		 * @var mixed
		 * @access Private
		 */
		var $locationManager;

		/**
		 * @var array
		 * @access Private
		 */
		var $data_in_array;
		/**
		 * @var integer
		 * @access Private
		 */
		var $domain_id;
		/**
		 * @var integer
		 * @access Private
		 */
		var $package_id;
		/**
		 * @var integer
		 * @access Private
		 */
		var $package_price;
		/**
		 * @var char
		 * @access Private
		 */
		var $backlink;
        /**
		 * @var string
		 * @access Private
		 */
		var $backlink_url;
		/**
		 * @var integer
		 * @access Private
		 */
		var $clicktocall_number;
		/**
		 * @var integer
		 * @access Private
		 */
		var $clicktocall_extension;
		/**
		 * @var date
		 * @access Private
		 */
		var $clicktocall_date;
		var $activation_date;
		/**
		 * @var integer
		 * @access Private
		 */
		var $bedroom;
		var $bedsize;
		var $sleeps;
		var $bathroom;
		var $property_type;
		var $view;
		var $distance_beach;
		var $development_name;

		var $airport_distance;
		var $airport_abbreviation;

		var $contact_fname;
		var $contact_lname;

		var $external_link1;
		var $external_link_text1;
		var $external_link2;
		var $external_link_text2;
		var $external_link3;
		var $external_link_text3;
		/**
		 * @var char
		 * @access Private
		 */
		var $amenity_airhockey;
		var $amenity_alarmclock;
		var $amenity_answeringmachine;
		var $amenity_arcadegames;
		var $amenity_billiards;
		var $amenity_blender;
		var $amenity_blurayplayer;
		var $amenity_books;
		var $amenity_casetteplayer;
		var $amenity_cdplayer;
		var $amenity_ceilingfan;
		var $amenity_childshighchair;
		var $amenity_coffeemaker;
		var $amenity_communalpool;
		var $amenity_computer;
		var $amenity_cookware;
		var $amenity_cookingrange;
		var $amenity_deckfurniture;
		var $amenity_dishwasher;
		var $amenity_dishes;
		var $amenity_dvdplayer;
		var $amenity_exercisefacilities;
		var $amenity_foosball;
		var $amenity_gametable;
		var $amenity_games;
		var $amenity_grill;
		var $amenity_hairdryer;
		var $amenity_icemaker;
		var $amenity_internet;
		var $amenity_ironandboard;
		var $amenity_kidsgames;
		var $amenity_linensprovided;
		var $amenity_lobsterpot;
		var $amenity_microwave;
		var $amenity_minirefrigerator;
		var $amenity_mp3radiodock;
		var $amenity_outsideshower;
		var $amenity_oven;
		var $amenity_pinball;
		var $amenity_pingpong;
		var $amenity_privatepool;
		var $amenity_radio;
		var $amenity_refrigeratorfreezer;
		var $amenity_sofabed;
		var $amenity_stereo;
		var $amenity_telephone;
		var $amenity_television;
		var $amenity_toaster;
		var $amenity_toasteroven;
		var $amenity_towelsprovided;
		var $amenity_toys;
		var $amenity_utensils;
		var $amenity_vacuum;
		var $amenity_vcr;
		var $amenity_videogameconsole;
		var $amenity_videogames;
		var $amenity_washerdryer;
		var $amenity_wifi;
		var $feature_airconditioning;
		var $feature_balcony;
		var $feature_barbecuecharcoal;
		var $feature_barbecuegas;
		var $feature_boatslip;
		var $feature_cablesatellitetv;
		var $feature_clubhouse;
		var $feature_coveredparking;
		var $feature_deck;
		var $feature_diningroom;
		var $feature_elevator;
		var $feature_familyroom;
		var $feature_fullkitchen;
		var $feature_garage;
		var $feature_gasfireplace;
		var $feature_gatedcommunity;
		var $feature_wheelchairaccess;
		var $feature_heated;
		var $feature_heatedpool;
		var $feature_hottubjacuzzi;
		var $feature_internetaccess;
		var $feature_kitchenette;
		var $feature_livingroom;
		var $feature_loft;
		var $feature_onsitesecurity;
		var $feature_patio;
		var $feature_petsallowed;
		var $feature_playroom;
		var $feature_pool;
		var $feature_porch;
		var $feature_rooftopdeck;
		var $feature_sauna;
		var $feature_smokingpermitted;
		var $feature_woodfireplace;
		var $activity_antiquing;
		var $activity_basketballcourt;
		var $activity_beachcombing;
		var $activity_bicycling;
		var $activity_bikerentals;
		var $activity_birdwatching;
		var $activity_boatrentals;
		var $activity_boating;
		var $activity_botanicalgarden;
		var $activity_canoe;
		var $activity_churches;
		var $activity_cinemas;
		var $activity_bikesprovided;
		var $activity_deepseafishing;
		var $activity_fishing;
		var $activity_fitnesscenter;
		var $activity_golf;
		var $activity_healthbeautyspa;
		var $activity_hiking;
		var $activity_horsebackriding;
		var $activity_horseshoes;
		var $activity_hotairballooning;
		var $activity_iceskating;
		var $activity_jetskiing;
		var $activity_kayaking;
		var $activity_livetheater;
		var $activity_marina;
		var $activity_miniaturegolf;
		var $activity_mountainbiking;
		var $activity_museums;
		var $activity_paddleboating;
		var $activity_paragliding;
		var $activity_parasailing;
		var $activity_playground;
		var $activity_recreationcenter;
		var $activity_restaurants;
		var $activity_rollerblading;
		var $activity_sailing;
		var $activity_shelling;
		var $activity_shopping;
		var $activity_sightseeing;
		var $activity_skiing;
		var $activity_bayfishing;
		var $activity_spa;
		var $activity_surffishing;
		var $activity_surfing;
		var $activity_swimming;
		var $activity_tennis;
		var $activity_themeparks;
		var $activity_walking;
		var $activity_waterparks;
		var $activity_waterskiing;
		var $activity_watertubing;
		var $activity_wildlifeviewing;
		var $activity_zoo;
		var $isppilisting;
		var $deposit_amount;
		var $tax_per_inquiry;
		var $unique_email;
		/**
		 * @var string
		 * @access Private
		 */
		var $is_featured;
		/**
		 * @var date
		 * @access Private
		 */
		var $featured_date;
		/**
		 * @var integer
		 * @access Private
		 */
		var $search_pos;
		/**
		 * @var date
		 * @access Private
		 */
		var $search_pos_date;
        
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//$this->middleware('auth');
        // Error msg border (bool true/false)
			$this->drawBorder = TRUE;

			// Initial position of string for error msg
			$this->initialPixelCol = 3;
			$this->initialPixelRow = 2;

			// Draw image border (bool true/false)
			$this->drawImgBorder = FALSE;

			// Screen output (bool true/false)
			$this->screenOutput = FALSE;

			// Supported files and their respective output functions
			$this->setSupported("IMAGETYPE_GIF", "imageCreateFromGif", "imagegif");
			$this->setSupported("IMAGETYPE_JPEG", "imageCreateFromJpeg", "imagejpeg");
			$this->setSupported("IMAGETYPE_PNG", "imageCreateFromPng", "imagepng");

			// Keep original size
			$this->originalSize = FALSE;

			// Image properties
			$this->img = array();

			// Full destination path with file name to store the file.
			$this->destination_path = "/tmp/image_teste.gif";
            
	}
   
  
 function sponser_load_dashboard(Request $request)
 {
        $item_id =  $request->item_id;
        $item_type= $request->item_type;

        $userAccountId = Session::get('SESS_ACCOUNT_ID');
        
        $spListingDataId = Sql::SponserListingById($item_id,$userAccountId);


      return view('front.sponsers_listing_dashboard',compact('spListingDataId','item_id','item_type'));

 }


 function sponser_listing_backlinks(Request $request)
 {
       	$userAccountId = Session::get('SESS_ACCOUNT_ID');
       	$listing_id =  $request->id;
        $profileSp = Functions::SponsersProfile();
      	$ownerName= '';
        if(!empty($profileSp))
        {
           $ownerName = $profileSp[0]->first_name.' '.$profileSp[0]->last_name.', ';
       	}
       $meta_data = Sql::sponsersMeta($userAccountId);
       $headerTitle = $ownerName.'Welcome to the Sponsor Section -'.$meta_data[0]->value;
      $headerAuthor = $meta_data[1]->value;
       $headerDescription = $meta_data[2]->value;
       $headerKeywords = $meta_data[3]->value;
       $footer_text = Sql::footerText();
       
       $spListingData = Sql::SponserListing($userAccountId);

        if ($listing_id){
        $listingObj = new Listing();
        $listingObj->Listing($listing_id);
        if ($listingObj->getString("backlink") == "y"){
            $backlinkCheck = true;
        } else {
            $backlinkCheck = false;
        }
        $backlink_url = $listingObj->getString("backlink_url");
        
        $backlinks = "<a href=\"";
        $backlinks .= url()."/listing/backlinks/".$listingObj->getString("friendly_url").".html";
        $backlinks .= "\">".$listingObj->getString("title")."</a>";
    }
        return view('front.sponsers_backlink',compact('spListingData','listing_id','headerTitle','headerAuthor','headerDescription','headerKeywords','footer_text','backlinks'));

 }

  function check_website(Request $request)
 {
 	  if($request->_token==csrf_token())
     {

    header("Content-Type: text/html; charset= UTF-8", TRUE);
    header("Accept-Encoding: gzip, deflate");
    header("Expires: Sat, 01 Jan 2000 00:00:00 GMT");
    header("Cache-Control: no-store, no-cache, must-revalidate");
    header("Cache-Control: post-check=0, pre-check", FALSE);
    header("Pragma: no-cache");
    
     	$url=$request->url;
     	$id=$request->id;
     	  if ($url && $id){


        
        $listingObj = new Listing();
       $listingObj->Listing($id);

        
        $searchFor = url()."/listing/backlinks/".$listingObj->getString("friendly_url").".html";

        $agent = "Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)";
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_USERAGENT, $agent);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION,1);
        curl_setopt($ch, CURLOPT_TIMEOUT,60);
        $output = curl_exec($ch);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        $val_httpCode = false;
        $val_content = false;

        if($httpcode >= 200 && $httpcode < 400){
            $val_httpCode = true;
        }

        if (($output) && (Functions::string_strpos($output, $searchFor) !== false)){
            $val_content = true;
        }

        if ($val_httpCode && $val_content){
            echo "OK";
        } else {
            echo "ERROR";
        }
    } else {
        echo "ERROR";
    }
        
     }
 }

 function store_listing_backlinks(Request $request)
 {
 	 if($request->_token==csrf_token())
     {
     	$backlink_url=$request->backlink_url;
     	$id=$request->id;
     	if (Functions::validate_form("backlink",$request->all() , $message_backlink)) {
            $listingObj = new Listing();
            $listingObj->Listing($id);
            
            $backlink_url = trim($backlink_url);
            
            // fixing url field if needed.
			if ($backlink_url != "") {
				if (Functions::string_strpos($backlink_url, "://") !== false) {
					$aux_url = explode("://", $backlink_url);
					$aux_url = $aux_url[1];
					$backlink_url = $aux_url;
				}
				$backlink_url = "http://".$backlink_url;
			}
            
            if (isset($sitemgr)){
                $listingObj->setString("backlink", ($backlink ? "y" : "n"));
                $listingObj->setString("backlink_url", $backlink_url);
            } else {
                $listingObj->setString("backlink", "y");
                $listingObj->setString("backlink_url", $backlink_url);
            }
            $listingObj->save();

            if (isset($sitemgr)){

            	 return redirect('sponsors/backlinks/'.$id)->with('message','1');

                //header("Location: $url_redirect/index.php?message=1");
            } else {
         	 return redirect('sponsors/backlinks/'.$id)->with('message','10');

                //header("Location: $url_redirect/index.php?message=10");
            }
            exit;
        }
     }
       
 }
 
 function sponser_listing(Request $request)
 {
    
        $userAccountId = Session::get('SESS_ACCOUNT_ID');
        if(!$userAccountId)
        {
            return redirect(url());
        }
        $listing_id =  $request->id;
        $level =  isset($_GET['level'])?$_GET['level']:0;
        $return_categories=isset($_GET['return_categories'])?$_GET['return_categories']:'';
        $title=isset($_GET['title'])?$_GET['title']:'';
        $discount_id=isset($_GET['discount_id'])?$_GET['discount_id']:'';
        
        $listingtemplate_id =  isset($_GET['listingtemplate_id'])?$_GET['listingtemplate_id']:0;
        $gallery_hash = "listing".($listing_id ? "_$listing_id" : "")."_".uniqid(rand(), true);
        
        
      
        $profileSp = Functions::SponsersProfile();
        $ownerName= '';
        if(!empty($profileSp))
        {
            $ownerName = $profileSp[0]->first_name.' '.$profileSp[0]->last_name.', ';
        }
        $meta_data = Sql::sponsersMeta($userAccountId);
        $headerTitle = $ownerName.'Welcome to the Sponsor Section -'.$meta_data[0]->value;
        $headerAuthor = $meta_data[1]->value;
        $headerDescription = $meta_data[2]->value;
        $headerKeywords = $meta_data[3]->value;
        $footer_text = Sql::footerText();
        
        $locations_3=DB::table('Location_3')->where('location_1','1')->orderby('name','asc')->get();
        
        $developmentData = Sql::DevelopmentData();
        $accountId = $userAccountId;
  

      	$categories = "";

       if($listing_id>0)
        {
      $listing = new Listing();
      $listing->Listing($listing_id);
      
          //Categories
      if (!$categories) if ($listing) $categories = $listing->getCategories(false, false, $listing_id, true, true);
 	 }
      if ($categories!='') {

		for ($i=0; $i<count($categories); $i++) {
			$arr_category[$i]["name"] = $categories[$i]->title;
			$arr_category[$i]["value"] = $categories[$i]->id;
			$arr_return_categories[] = $categories[$i]->id;
		}
		if ($arr_return_categories) $return_categories = implode(",", $arr_return_categories);
		array_multisort($arr_category);
		$feedDropDown = "<select name='feed' id='feed' multiple size='5' style=\"width:500px\">";
		if ($arr_category) foreach ($arr_category as $each_category) {
			$feedDropDown .= "<option value='".$each_category["value"]."'>".$each_category["name"]."</option>";
			$feedAjaxCategory[] = $each_category["value"];
		}
		$feedDropDown .= "</select>";
	}

	else
	{

		if ($return_categories!='') {
			$return_categories_array = explode(",", $return_categories);
			if ($return_categories_array) {
				foreach ($return_categories_array as $each_category) {
					$categories[] = $cat = new ListingCategory();
					$cat->ListingCategory($each_category);
				}
			}
		}

		$feedDropDown = "<select name='feed' id='feed' multiple size='5' style=\"width:500px\">";
		if ($categories!=null) {
			foreach ($categories as $category) {
				$name = $category->title;
				$feedDropDown .= "<option value='".$category->id."'>$name</option>";
				$feedAjaxCategory[] = $category->id;
			}
		}
	


		$feedDropDown .= "</select>";
	}
		

        if($listing_id>0)
        {
            $spListingDataId = Sql::SponserListingById($listing_id,$userAccountId);
            $spListingGallery = Sql::SponserListingGallery($listing_id,$userAccountId);
            return view('front.sponsers_listing',compact('spListingDataId','spListingGallery','developmentData','headerTitle','headerAuthor','headerDescription','headerKeywords','footer_text','accountId','gallery_hash','feedDropDown','title','discount_id','locations_3'));

        }
        else
        {
            return view('front.sponsers_listing',compact('spListingGallery','developmentData','headerTitle','headerAuthor','headerDescription','headerKeywords','footer_text','accountId','level','gallery_hash','feedDropDown','title','discount_id','locations_3'));

        }
        
        
 }
 
 function sponser_storeListing(Request $request)
 {
     if($request->_token==csrf_token())
     {
        
        	//d($request->_token,1);
        	//d(csrf_token(),1);
        
            $id = $request->id;
            $gallery_hash=$request->gallery_hash;

            unset($arr_keywords);
            unset($each_keyword);
            unset($aux_kw);
            unset($new_arr_keywords);
            unset($aux_keywords);
            $arr_keywords = isset($keywords)?explode("\n", $keywords):array();
            foreach ($arr_keywords as $each_keyword) {
                $aux_kw = trim($each_keyword);
                if (strlen($aux_kw) > 0) {
                    $new_arr_keywords[] = $aux_kw;
                }
            }
            if (isset($new_arr_keywords)) $aux_keywords = implode(" || ", $new_arr_keywords);
            $request->keywords = isset($aux_keywords)?$aux_keywords:'';
            $request->array_keywords = isset($new_arr_keywords)?$new_arr_keywords:'';
    		##################################################
    
            $request->title = trim($request->title);
    		$request->email = trim($request->email);
    		$request->url = trim(isset($request->url)?$request->url:'');
    		$request->title = preg_replace('/\s\s+/', ' ', $request->title);
    		$request->friendly_url = str_replace(".htm", "", $request->friendly_url);
    		$request->friendly_url = str_replace(".html", "", $request->friendly_url);
    		$request->friendly_url = trim($request->friendly_url);
    		
            
    		$sqlFriendlyURL = "";
            if(isset($request->friendly_url) && $request->friendly_url>0)
    		{
                $sqlFriendlyURL .= " SELECT friendly_url FROM Listing WHERE friendly_url = ".$request->friendly_url." ";
                if ($id) $sqlFriendlyURL .= " AND id != $id ";
                $sqlFriendlyURL .= " LIMIT 1 ";
                $sqlFriendlyURLResult = Sql::getFriendlyUrl($sqlFriendlyURL);
    		}
    		$resultFriendlyURL = isset($sqlFriendlyURLResult[0]->friendly_url)?$sqlFriendlyURLResult[0]->friendly_url:0;
    		if ($resultFriendlyURL> 0) { 
    			if ($id) $request->friendly_url = $request->friendly_url.config('params.FRIENDLYURL_SEPARATOR').$id;
    			else $request->friendly_url = $request->friendly_url.config('params.FRIENDLYURL_SEPARATOR').uniqid();
    		} 
            if (!$id && !$request->friendly_url) {    
                $request->friendly_url = uniqid();  
            }
            
            //$_POST["video_snippet"] = str_replace("\"", "'", $request->video_snippet);
    		
            // strip \r chars provided by Windows, in order to keep character count standard
            if (isset($request->description)) {
                $request->description = str_replace("\r", "", $request->description);
            }
    
    		if(isset($request->hours_work)){
    			$request->hours_work = str_replace("\r", "", $request->hours_work);
    		}
    		if(isset($request->locations)){
    			$request->locations = str_replace("\r", "", $request->locations);
    		}
    
    		//if ($_FILES['attachment_file']['name']) {
//    			$array_allowed_types = array('pdf','doc','txt','jpg','gif','png');
//    			$arr_attachment = explode(".",$_FILES['attachment_file']['name']);
//    			$attachment_extension = $arr_attachment[count($arr_attachment)-1];
//    			if (in_array(string_strtolower($attachment_extension),$array_allowed_types)) {
//    				$allow_attachment_file = "true";
//    			} else {
//    				$allow_attachment_file = "false";
//    			}
//    		}
            
    		if (isset($isppilisting) && $isppilisting == 'y') 
    			if ($deposit_amount != "" && $tax_per_inquiry != "") $emptyPPI_fields = false;
    			else $emptyPPI_fields = true;
                $errorReturned = $this->validate_form("listing", $request->all(), $message_listing);
              
            $validate_discount=Functions::is_valid_discount_code($request->discount_id, "listing",$request->id, $message_error, $discount_error_num);  


    		if (empty($errorReturned) && Functions::is_valid_discount_code($request->discount_id, "listing",$request->id, $message_error, $discount_error_num)) {

     		
    			// adding new locations if posted
    			if ($request->new_location2_field != "" || $request->new_location3_field != "" || $request->new_location4_field != ""
                 || $request->new_location5_field != "") {
    
    				$locationsToSave = array();
    
    				$_locations = explode(",", "1,3,4");
    				$_defaultLocations = explode (",", '');
    				$_nonDefaultLocations = array_diff_assoc($_locations, $_defaultLocations);
    
    				foreach ($_defaultLocations as $defLoc)
    					$locationsToSave[$defLoc] = $request->location_.$defLoc;
    
    				$stop_insert_location = false;
                    
    				foreach ($_nonDefaultLocations as $nonDefLoc) {
    					if (trim($request->location_.$nonDefLoc)!="")
    						$locationsToSave[$nonDefLoc] = $request->location_.$nonDefLoc;
    					else {
    						if (!$stop_insert_location) {
    							if (!$request->new_location.$nonDefLoc.'_field') {
    								$stop_insert_location = true;
    							} else {
    								$objNewLocationLabel = "Location".$nonDefLoc;
    								$objNewLocation = new $objNewLocationLabel;
    
    								foreach ($locationsToSave as $level => $value)
    									$objNewLocation->setString("location_".$level, $value);
    
    								$objNewLocation->setString("name", $_POST['new_location'.$nonDefLoc.'_field']);
    								$objNewLocation->setString("friendly_url", $_POST['new_location'.$nonDefLoc.'_friendly']);
    								$objNewLocation->setString("default", "n");
    								$objNewLocation->setString("featured", "n");
    								
    								$newLocationFlag = $objNewLocation->retrievedIfRepeated($_locations);
    								if ($newLocationFlag) $objNewLocation->setNumber("id", $newLocationFlag);
    								else $objNewLocation->Save();
    								$_POST["location_".$nonDefLoc] = $objNewLocation->getNumber("id");
    								$locationsToSave[$nonDefLoc]=$_POST["location_".$nonDefLoc];
    							}
    						}
    					}
    				}
    			}	
    
    			//updating listing level to default level if current level is not active
                if (!isset($levelObj)) {
                    $levelObj = new ListingLevel();
                    $levelObj->ListingLevel(true);
                    
                    $listing = new Listing($id);

                    if ($levelObj->getActive($request->level) == 'n') {
                        $request->level = $levelObj->getDefaultLevel();    
                    }
                }
                //d($request->level,1);
                // fixing url field if needed.
    			if (isset($request->url) && trim($request->url) != "") {
    				if (string_strpos($request->url, "://") !== false) {
    					$aux_url = explode("://", $request->url);
    					$aux_url = $aux_url[1];
    					$request->url = $aux_url;
    				}
    				$request->url = $request->url_protocol. $request->url;
    			}
                //d($_POST,1);
                // setting seo_description and seo_keyword when member signup for an item
                if ( isset($members) && isset($process) &&  $process == 'signup' ) {
                    (!isset($_POST["seo_description"])) ? ($_POST["seo_description"] = str_replace("\n", " ", $_POST["description"])) : '';
                    (!isset($_POST["seo_keywords"])) ? ($_POST["seo_keywords"] = str_replace(" ||", ",", $_POST["keywords"])) : '';  
                }
              
                // removing linebreaks from seo_description
                
                if ( !$id ) {
                    if(isset($_POST["seo_description"]))
                        ($_POST["seo_description"] = str_replace("\n", " ", $_POST["seo_description"]));
                }
   
    			// Clean Attachment
    			if (isset($remove_attachment) || isset($_FILES['attachment_file']['tmp_name']) && (file_exists($_FILES['attachment_file']['tmp_name']))) {
    				if ($id) { 
    					$listing = new Listing($id);
    					if ($id_attachment_file = $listing->getString("attachment_file")) {
    						if (file_exists(EXTRAFILE_DIR."/".$id_attachment_file)) {
    							@unlink(EXTRAFILE_DIR."/".$id_attachment_file);
    						}
    						$listing->setString("attachment_file", "");
    						$_POST["attachment_file"] = "";
    						$listing->save();
    					}
    					unset($listing);
    				}
    			}

    			$status = new ItemStatus();
                 
    			$listing = new Listing($id);
                if($id && $id!='0')
                    $listing->Listing($id);
                
    			$new_listing = false;
    			$update_listing = false;
                
    			if (!$listing->getString("id") || $listing->getString("id") == 0){
    				$new_listing = true;
    				$aux_package_id = $request->package_id;
    				$request->package_id = 0;
    				
    				 
    				//d($request->$image_id,1);die;
        if($gallery_hash!=0)
        {
        	        $listing->system_addItemGallery($gallery_hash, $request->title, $galleryIDC, $image_id,$thumb_id);

        }            

                   
    				
    				//d($galleryIDC,1);die;

    				$message = 0;                
    				$emailNotification = true;
    
    				$newest = "1";
    
    				$listingLevelObj = new ListingLevel(true);
    				//if ($listingLevelObj->getHasPromotion($request->level) == "y") $extramessage_promotion = "1";
    				
    				$listing->arrayMakeFromRow($_POST);
    				$listing->setString("status", $status->getDefaultStatus());													
    				$listing->setDate("renewal_date", "00/00/0000"); 
    				//if (strpos($url_base, "/".SITEMGR_ALIAS."") && $_POST["account_id"]) {
//    					system_renameGalleryImages($image_id, $thumb_id, $_POST["account_id"], $galleryIDC);
//    				}
    
    				$request->package_id = $aux_package_id;
    
    			} else {
    			 
    				$update_listing = true;
    
    				$galleryIDC = isset($gallery_id)?$gallery_id:'';
    				

    				//d($gallery_id,1);die;
    				//$listing->system_addItemGallery($gallery_hash, $request->title, $gallery_id, $image_id, $thumb_id);




    				$message = 1;
    				$emailNotification = false; 
    				if ($listing->getString("status") != $status->getDefaultStatus()){ $emailNotification = true; }  
    
    				//security issue
    				unset($_POST["status"]);
    				unset($_POST["renewal_date"]);
    
    				if (!$listing->hasRenewalDate()) {
    					$_POST["renewal_date"] = "0000-00-00";
    				}
    //commented by hassan
    				//if (strpos($url_base, "/".SITEMGR_ALIAS."")) {
//    
//    					$_POST["status"] = $listing->getString("status");
//    
//    					if ($listing->getNumber("account_id") != $_POST["account_id"] || $_POST["account_id"]) {
//                            if ($listing->getNumber("account_id") != $_POST["account_id"]){
//                                if ($listing->getNumber("promotion_id") > 0){
//                                    // remove relationship if sitemgr change account
//                                    $listing->removePromotionID(); 
//                                }
//    						}
//                            
//    						$image_idT = $listing->getNumber("image_id");
//    						$thumb_idT = $listing->getNumber("thumb_id");
//    
//    						system_renameGalleryImages($image_idT, $thumb_idT, $_POST["account_id"], $galleryIDC);
//    					}
//    
//    				} else {
    					$last_status = $listing->getString("status");
    				//}
    				
                    //if (string_strpos($url_base, "/".SITEMGR_ALIAS."")) {
//                        if(!$_POST["isppilisting"]) $_POST["isppilisting"] = "n";
//                        if(!$_POST["unique_email"]) $_POST["unique_email"] = "n";                    
//                    }else{
                        if(!isset($_POST["isppilisting"])) $_POST["isppilisting"] = isset($listing->isppilisting)?$listing->isppilisting:'';
                        if(!isset($_POST["unique_email"])) $_POST["unique_email"] = isset($listing->unique_email)?$listing->unique_email:'';                    
                  //  }
                    
    				//custom - ppi
    				if (isset($isppilisting) && $isppilisting == "y") {
    				
    					$_POST["renewal_date"] = "0000-00-00";
    					//if the sitemgr changed the deposit_amount or added it (ie, made the listing a ppi listing), its status is set to Pending
    					if ($listing->deposit_amount != $deposit_amount || $listing->isppilisting == 'n') {
    						$_POST["status"] = 'P';
    						$sql = "UPDATE Listing SET deposit_amount = $deposit_amount, tax_per_inquiry = $tax_per_inquiry, unique_email = '$unique_email', ppi_warning = 'n' WHERE id = $listing->id";
    					}
    					else 
    						$sql = "UPDATE Listing SET deposit_amount = $deposit_amount, tax_per_inquiry = $tax_per_inquiry, unique_email = '$unique_email' WHERE id = $listing->id";
    					Sql::updateListingDeposite($sql);
    
    					$account = new Account($account_id);					
    					$ppi = $account->isPPIacc();
    
    					if (!empty($ppi)) {
    						$listings_updated = array();
    						$array_listings_ppi = array();
    						if (!empty($ppi["ppi_listings"]))
    							$array_listings_ppi = json_decode($ppi["ppi_listings"]);
    						foreach ($array_listings_ppi as $value) {
    							if ($value != $id)
    						 		$listings_updated[] = $value;
    						}
    						$listings_updated = json_encode($listings_updated);
    						$sql = "UPDATE PPI SET ppi_listings = '$listings_updated' WHERE account_id = ".$ppi["account_id"];
    						MainSql::ppi_listingsUpdate($sql);						
    					}
    				}
    				else {					
    					$account = new Account($account_id);					
    					$ppi = $account->isPPIacc();
    
    					if (!empty($ppi)) {
    						$array_listings_ppi = array();
    						if (!empty($ppi["ppi_listings"]))
    							$array_listings_ppi = json_decode($ppi["ppi_listings"]);
    						if (in_array($id, $array_listings_ppi) == FALSE) {							
    							$array_listings_ppi[] = $id;
    							$array_listings_ppi = json_encode($array_listings_ppi);
    							$sql = "UPDATE PPI SET ppi_listings = '$array_listings_ppi' WHERE account_id = ".$ppi["account_id"];
    							MainSql::ppi_listingsUpdate($sql);
    						}
    					}
    				}
    				//
    
    				$listing->arrayMakeFromRow($_POST);
    			}
    
    			if (isset($image_id)) {
    				$listing->setNumber("image_id",$image_id);
    				$listing->setNumber("thumb_id",$thumb_id);
    			}
    
    			// if ($id) {
    			// 	if(!$members) {
    			// 		$lcObj = new ListingChoice();
    			// 		$lcObj->setNumber("listing_id", $id);
    			// 		$lcObj->Delete();
    			// 	} else {
    			// 		$choices = db_getFromDB("editor_choice", "available", 1, "all", "id", "object", SELECTED_DOMAIN_ID);
    			// 		if($choices) {
    			// 			foreach($choices as $choice) {
    			// 				$lcObj = new ListingChoice($choice->getNumber("id"), $id);
    			// 				$lcObj->DeleteAvailable();
    			// 			}
    			// 		}
    			// 	}
    			// }
    			
    			/*AM
    			$levelObjTmp = new ListingLevel(true);
    			$levelsTmp = $levelObjTmp->getLevelValues();
    			if ($levelsTmp && !in_array($listing->getNumber("level"), $levelsTmp)) {
    				$listing->setNumber("level", $levelObjTmp->getDefaultLevel());
    			}
    			unset($levelObjTmp);
    			unset($levelsTmp);*/
    			
    			
    			
    			//custom - ppi
    			if (!$update_listing) {
    				if (isset($isppilisting) && $isppilisting == "y") {		
    					$_POST["renewal_date"] = "0000-00-00";
    					$sql = "UPDATE Listing SET deposit_amount = $deposit_amount, tax_per_inquiry = $tax_per_inquiry, unique_email = '$unique_email' WHERE id = $listing->id";
    					Sql::updateListingDeposite($sql);
    				}
    				else {
                        if(!isset($account_id))
                        {
                          $account_id =  Session::get('SESS_ACCOUNT_ID');  
                        }
                        else
                        {
                            $account_id=0;
                        }
    					$account = new Account();
                        $account->Account($account_id);
    					$ppi = $account->isPPIacc();
    					if (!empty($ppi)) {
    						$array_listings_ppi = array();
    						if (!empty($ppi["ppi_listings"]))
    							$array_listings_ppi = json_decode($ppi["ppi_listings"]);
    						if (in_array("$listing->id", $array_listings_ppi)) {
    							$array_listings_ppi[] = $listing->id;
    							$array_listings_ppi = json_encode($array_listings_ppi);												
    							$sql = "UPDATE PPI SET ppi_listings = '$array_listings_ppi' WHERE account_id = ".$ppi["account_id"];
    								MainSql::ppi_listingsUpdate($sql);							
    						}
    					}
    				}
    				//
    			}			
    
    			//setting gallery
                if(!isset($galleryIDC))
                    $galleryIDC = $request->gallery_id;
    			//$listing->setGalleries($galleryIDC);
    
    			// ATTACHMENT FILE UPLOAD
    			if (isset($_FILES['attachment_file']['tmp_name']) && (file_exists($_FILES['attachment_file']['tmp_name']))) {
    				$file_name = "attach_".$listing->getNumber("id").".".$attachment_extension;
    				$listing->setString("attachment_file",$file_name);
    				$file_path = EXTRAFILE_DIR."/".$file_name;
    				if (filesize($_FILES['attachment_file']['tmp_name'])) {
    					copy($_FILES['attachment_file']['tmp_name'],$file_path);
    				} else {
    					$listing->setString("attachment_file","");
    					$msg_post_listing = 2;
    				}
    				//$listing->Save();
    			}
    
    			// if ($_POST['choice']) {
    			// 	foreach ($_POST['choice'] as $value) {
    			// 		$listingChoiceObj = new ListingChoice($value, $id);
    			// 		$listingChoiceObj->setNumber("editor_choice_id", $value);
    			// 		$listingChoiceObj->setNumber("listing_id", $listing->getNumber("id"));
    			// 		$listingChoiceObj->Save();
    			// 		unset($listingChoiceObj);
    			// 	}
    			// }
    
    			if (config('params.ZIPCODE_PROXIMITY') == "on") {
    				$listing->zipproximity_updateDB("Listing", $listing->getNumber("id"));
    				$listing->zipproximity_updateDB("Listing_Summary", $listing->getNumber("id"));
    			}
                if(!isset($maptuning_done))
                $maptuning_done='';
                $listing->system_updateMaptuningDate("Listing", $listing->getNumber("id"), $maptuning_done);
            
    			
    			// E-mail notify
    			if ($listing->account_id > 0) {

    				if($message == 0) {

    					$contactObj = new Contact();
    					$contactObj->Contact($listing->account_id);
    					if($emailNotificationObj = Functions::system_checkEmail(config('params.SYSTEM_NEW_LISTING'))) {
    						//setting_get("sitemgr_send_email", $sitemgr_send_email);
    						//setting_get("sitemgr_email", $sitemgr_email);
    						//$sitemgr_emails = explode(",", $sitemgr_email);
    						$login_info=$contactObj->first_name.' '.$contactObj->last_name;
    						$subject = $emailNotificationObj->getString("subject");
    						$body = $emailNotificationObj->getString("body");
    						$body = str_replace("ACCOUNT_NAME",$login_info,$body);
    						$body =  Functions::system_replaceEmailVariables($body,$listing->getNumber('id'),'listing');
    						$body	 = str_replace($_SERVER["HTTP_HOST"], url(), $body);
    						$subject = str_replace("ACCOUNT_NAME",$login_info,$subject);

    						$subject =  Functions::system_replaceEmailVariables($subject,$listing->getNumber('id'),'listing');
    						$body = html_entity_decode($body);
    						$subject = html_entity_decode($subject);
    						$error = false;

    						 $data= [
            'sitemgr_msg' => $body,
            'subject' => $subject,
            'email_send' =>$contactObj->getString("email")
        ];

    Mail::send('email.owner',['data' => $data], function($message) use ($data) {
   
        $message->from('amoosjohn@gmail.com');
     $message->to($data['email_send'])->cc('amoosjohn@gmail.com')->subject($data['subject']);
    });


//system_mail($contactObj->getString("email"), $subject, $body, EDIRECTORY_TITLE." <$sitemgr_email>", $emailNotificationObj->getString("content_type"), "", $emailNotificationObj->getString("bcc"), $error);
    					}
    				}
    			}
    			/*
    			if ($emailNotification) {
    				if (!Functions::string_strpos($url_base, "/".SITEMGR_ALIAS."")) {
                        
                        $domain_url = ((SSL_ENABLED == "on" && FORCE_SITEMGR_SSL == "on") ? SECURE_URL : NON_SECURE_URL);
    					$domain_url = str_replace($_SERVER["HTTP_HOST"], $domain->getstring("url"), $domain_url);
                        
    					setting_get("sitemgr_listing_email",$sitemgr_listing_email);
    					$sitemgr_listing_emails = explode(",",$sitemgr_listing_email);
                        
    					$account = new Account($acctId);
                        setting_get("new_listing_email", $new_listing_email);
    					setting_get("update_listing_email", $update_listing_email);
    					$sentUp = 0;
    					$sentNew = 0;
                        
                        $emailSubject = system_showText(LANG_NOTIFY_LISTING);
    					$sitemgr_msg = system_showText(LANG_LABEL_SITE_MANAGER).",<br /><br />";
    					
    					if ($_POST["id"]) {
    						$sitemgr_msg .= ucfirst(LANG_LISTING_FEATURE_NAME)." \"".$listing->getString("title")."\" ".system_showText(LANG_NOTIFY_ITEMS_1)." \"".system_showAccountUserName($account->getString("username"))."\" ".system_showText(LANG_NOTIFY_ITEMS_3)."<br /><br />";
    						$link_sitemgrmsg = "<a href=\"".$domain_url."/".SITEMGR_ALIAS."/".LISTING_FEATURE_FOLDER."/settings.php?id=".$listing->getNumber("id")."\" target=\"_blank\">".$domain_url."/".SITEMGR_ALIAS."/".LISTING_FEATURE_FOLDER."/settings.php?id=".$listing->getNumber("id")."</a><br /><br />";
    						$sentUp = 1;
    
    					} else {
    						$sitemgr_msg .= ucfirst(LANG_LISTING_FEATURE_NAME)." \"".$listing->getString("title")."\" ".system_showText(LANG_NOTIFY_ITEMS_2)." \"".system_showAccountUserName($account->getString("username"))."\" ".system_showText(LANG_NOTIFY_ITEMS_3)."<br /><br />";
    						$link_sitemgrmsg = "<a href=\"".$domain_url."/".SITEMGR_ALIAS."/".LISTING_FEATURE_FOLDER."/view.php?id=".$listing->getNumber("id")."\" target=\"_blank\">".$domain_url."/".SITEMGR_ALIAS."/".LISTING_FEATURE_FOLDER."/view.php?id=".$listing->getNumber("id")."</a><br /><br />";
    						$sentNew = 1;
    					}
    					$sitemgr_msg .= $link_sitemgrmsg.EDIRECTORY_TITLE;
                        
    					if (($update_listing_email && $sentUp == 1) || ($new_listing_email && $sentNew == 1)) {
                            system_notifySitemgr($sitemgr_listing_emails, $emailSubject, $sitemgr_msg);
    					}
    				}
    			} 
                */
    			/******************************************************/
    
    			// setting categories
        		$return_categories=$request->feed;
                if(count($return_categories)>0)
                {
                	$return_categories_array = explode(",", $return_categories);
    				$listing->setCategories($return_categories_array); // MUST BE ALWAYS AFTER $LISTINGOBJECT->SAVE();
                }
    			
    
    			/*
    			 * Check if is bought package
    			 */
    			if($request->using_package == "y"){
    				/*
    				 * Check if exists package
    				 */
    				$packageObj = new Package();
    				$array_package_offers = $packageObj->getPackagesByDomainID(SELECTED_DOMAIN_ID, "listing", $listing->level);
    				$hasPackage = false;
    				if ((is_array($array_package_offers)) and (count($array_package_offers)>0) and $array_package_offers[0]) {
    
    					unset($array_info_package);
    					$array_info_package["item_type"]		= "listing";
    					$array_info_package["item_id"]			= $listing->getNumber("id");
    					$array_info_package["item_name"]		= $listing->getString("title");
    					$array_info_package["item_friendly_ur"]	= $listing->getString("friendly_url");
    					$array_info_package["package_id"][0]	= $package_id;
    					package_buying_package($array_info_package, false, true);
    
    				}
    			}
                if(!isset($account_id))
                {
                  $account_id =  Session::get('SESS_ACCOUNT_ID');  
                }
                else
                {
                    $account_id=0;
                }
    			if ($new_listing){
                    $account = new Account($account_id);
    				$listing->setting_get("listing_approve_free", $listing_approve_free);
    				$isppilisting = ($account->isPPIListing() == true ? true : false);
    				if (!$listing_approve_free && !$listing->needToCheckOut($isppilisting)){
    					$listing->setString("status", "A");
    					//$listing->save();
    				} 				
    			}
    /*
    			if (strpos($url_base, "/".MEMBERS_ALIAS."") && $update_listing) {
                    $account = new Account($account_id);
    				setting_get("listing_approve_updated", $listing_approve_updated);
    				$isppilisting = ($account->isPPIListing($listing->id) == true ? true : false);
    				if ($last_status == "A" && !$listing->needToCheckOut($isppilisting) && !$listing_approve_updated && $process != "signup"){
    					$listing->setString("status", "A");
    					$listing->save();
    				} else if ($process == "signup"){
    					$listing->setString("status", $last_status);
    					$listing->save();
    				}
    			}			
    			
    			if (!empty($ppi) && !isset($isppilisting)) {
    				$listing->setString("status", $ppi["status"]);
    				//setting renewal_date as 0000-00-00 so when the daily_maintenance is running, this listing isn't set as Expired
    				$listing->renewal_date = "00/00/0000";
    				$listing->save();
    			}			
    			
    
    			if ($process == "claim") {
    
    				$claimObject = new Claim($claim_id);
    
    				$claimObject->setString("step", "c");
    
    				$claimObject->setString("listing_title", $listing->getString("title", false));
    				$claimObject->setString("new_location_1", $listing->getNumber("location_1"));
    				$claimObject->setString("new_location_2", $listing->getNumber("location_2"));
    				$claimObject->setString("new_location_3", $listing->getNumber("location_3"));
    				$claimObject->setString("new_location_4", $listing->getNumber("location_4"));
    				$claimObject->setString("new_location_5", $listing->getNumber("location_5"));
    				$claimObject->setString("new_title", $listing->getString("title", false));
    				$claimObject->setString("new_friendly_url", $listing->getString("friendly_url", false));
    				$claimObject->setString("new_email", $listing->getString("email", false));
    				$claimObject->setString("new_url", $listing->getString("url", false));
    				$claimObject->setString("new_phone", $listing->getString("phone", false));
    				$claimObject->setString("new_fax", $listing->getString("fax", false));
    				$claimObject->setString("new_address", $listing->getString("address", false));
    				$claimObject->setString("new_address2", $listing->getString("address2", false));
    				$claimObject->setString("new_zip_code", $listing->getString("zip_code", false));
    				$claimObject->setString("new_level", $listing->getNumber("level"));
    				$claimObject->setString("new_listingtemplate_id", $listing->getNumber("listingtemplate_id"));
    
    				$claimObject->save();
    
    				if ($listing->needToCheckOut()) {
    					header("Location: $url_redirect/billing.php?claimlistingid=".$id);
    				} else {
    					$claimObject->setString("step", "e");
    					$claimObject->save();
    					header("Location: ".DEFAULT_URL."/".MEMBERS_ALIAS."/claim/claimfinish.php?claimlistingid=".$claimlistingid);
    				}
    
    			} else { 
    
    				header("Location: $url_redirect/".(($search_page) ? "search.php" : "index.php")."?module=listing&process=".$process."&newest=".$newest."&message=".$message."&extramessage_promotion=".$extramessage_promotion."&screen=$screen&letter=$letter".(($url_search_params) ? "&$url_search_params" : ""));
    
    			}
    */

   		

   			//$gallery=new Gallery();
    		//$temp=$gallery->Save();
    	


 
    	//Add or Update
				if(!$new_listing)
    			{
$gallery_id=$request->gallery_id;

  
    	if(isset($gallery_hash) && $gallery_id==0)
  {

    		$sql = "INSERT INTO Gallery (title,
        												account_id,
        												entered,
        												updated)
        										VALUES ('$request->title',
        												$request->account_id,
        												NOW(),
        												NOW())";
        			Sql::insertGallery($sql);

			

   $sel="Select max(id) as id from Gallery";
   $tempId  = Sql::getId($sel);    
    if($tempId[0]->id)
      {
        $gallery_new_id= $tempId[0]->id;
      }


   $gallery = Sql::getGallery_Temp($gallery_hash);
   $totalImages = count($gallery);


		

		if (count($gallery) > 0) {
		
			for ($i=0; $i<$totalImages; $i++) {
				$image_id=$gallery[$i]->image_id;
				$image_caption=$gallery[$i]->image_caption;
				$thumb_id=$gallery[$i]->thumb_id;
				$thumb_caption=$gallery[$i]->thumb_caption;
				$image_default=$gallery[$i]->image_default;
				
				$sql = "INSERT INTO Gallery_Image (gallery_id,
        												image_id,
        												image_caption,
        												thumb_id,
        												thumb_caption,
        												image_default)
        										VALUES ($gallery_new_id,
        												$image_id,
        												'$image_caption',
        												$thumb_id,
        												'$thumb_caption',
        												'$image_default')";
        			Sql::insertGallery($sql);


				
			}
			$del = "DELETE FROM Gallery_Temp WHERE sess_id ='$gallery_hash'";
        	Sql::deleteImage($del);
			//$listing->setGalleries();

  $additem = "INSERT INTO Gallery_Item (item_type,item_id,gallery_id) VALUES ('listing',
  $id,$gallery_new_id)";
	Sql::insertGalleryitem($additem);




		}


		

   }



    		$listing->setString("friendly_url", $listing->getNumber("id"));
    				$listing->Save();
    				//d('Update 1',1);die;
    		}

    			if (!$update_listing) {
    				
    				$listing->setString("friendly_url", $listing->getNumber("id"));
    				$listing->Save();
  if(isset($gallery_hash) && $id==0)
  {
    		$sql = "INSERT INTO Gallery (title,
        												account_id,
        												entered,
        												updated)
        										VALUES ('$request->title',
        												$request->account_id,
        												NOW(),
        												NOW())";
        			Sql::insertGallery($sql);

        			

   $sel="Select max(id) as id from Gallery";
   $tempId  = Sql::getId($sel);    
    if($tempId[0]->id)
      {
        $gallery_new_id= $tempId[0]->id;
      }

   
 
   $gallery = Sql::getGallery_Temp($gallery_hash);
   $totalImages = count($gallery);
		

		if (count($gallery) > 0) {
		
			for ($i=0; $i<$totalImages; $i++) {
				$image_id=$gallery[$i]->image_id;
				$image_caption=$gallery[$i]->image_caption;
				$thumb_id=$gallery[$i]->thumb_id;
				$thumb_caption=$gallery[$i]->thumb_caption;
				$image_default=$gallery[$i]->image_default;
				
				$sql = "INSERT INTO Gallery_Image (gallery_id,
        												image_id,
        												image_caption,
        												thumb_id,
        												thumb_caption,
        												image_default)
        										VALUES ($gallery_new_id,
        												$image_id,
        												'$image_caption',
        												$thumb_id,
        												'$thumb_caption',
        												'$image_default')";
        			Sql::insertGallery($sql);


				
			}
			$del = "DELETE FROM Gallery_Temp WHERE sess_id ='$gallery_hash'";
        	Sql::deleteImage($del);


			//$listing->setGalleries();

			 $listsel="Select max(id) as id from Listing";
  			 $listId  = Sql::getId($listsel);    
    		if($listId[0]->id)
     		 {
      			  $listing_new_id= $listId[0]->id;
      		 }

  $additem = "INSERT INTO Gallery_Item (item_type,item_id,gallery_id) VALUES ('listing',
  $listing_new_id,$gallery_new_id)";

        		//d($sql1,1);die;										
        			Sql::insertGalleryitem($additem);

		}


		

   }

//d('Save',1);
}

    if(isset($request->id) && $request->id!='0')
    {
    return redirect('sponsors/listing/'.$request->id);
       exit;
    }	
    else
    {
    	   return redirect('sponsors')->with('message','Listing successfully updated!');
    			exit;
    }
     
    
    		} else {


                $errors = $this->validate_form("listing", $_POST, $message_listing);
              	
       
              return Redirect::back()->withInput()->withErrors($errors); 
    		  
    		}
    
    		
     }
    
 }
 

 function sponser_returngallery(Request $request)
 {
    if($request->_token==csrf_token())
     {
        $userAccountId = Session::get('SESS_ACCOUNT_ID');
        
        $id = $request->id?$request->id:0;
        $gallery_id =$request->gallery_id;
        $new_image = $request->new_image;
        $sess = $request->sess;
        $module = $request->module;
        $main = $request->main;
        $level = $request->level;
        $gallery_hash = $request->gallery_hash;

        //d($gallery_hash,1);
        
        switch ($module){
            case "blog" :       $thumb_width = IMAGE_BLOG_THUMB_WIDTH;
    							$thumb_height = IMAGE_BLOG_THUMB_HEIGHT;
    							break;
    		case "article" :	$thumb_width = IMAGE_ARTICLE_THUMB_WIDTH;
    							$thumb_height = IMAGE_ARTICLE_THUMB_HEIGHT;
    							break;
    		case "classified" : $thumb_width = IMAGE_CLASSIFIED_THUMB_WIDTH;
    							$thumb_height = IMAGE_CLASSIFIED_THUMB_HEIGHT;
    							break;
    		case "event" :		$thumb_width = IMAGE_EVENT_THUMB_WIDTH;
    							$thumb_height = IMAGE_EVENT_THUMB_HEIGHT;
    							break;
    		case "listing" :	$thumb_width = config('params.IMAGE_LISTING_THUMB_WIDTH');
    							$thumb_height = config('params.IMAGE_LISTING_THUMB_HEIGHT');
    							break;
    	}
        
        $hasmain_image = true;
    
        $imagesWritten = 0;
      
        //$return_code='';

          if ($module != "blog"){
            $return_code = "<ul>";
        }
        
        if ($id && $gallery_id!=0) {

        
		$galleries = Sql::getGalleryId($id,$module);
        if(!empty($galleries) &&  $galleries[0]->gallery_id>0)
            $gallery_id = $galleries[0]->gallery_id;

         //d($galleries[0]->gallery_id,1);die;
         if (isset($gallery_id) && $gallery_id!=0  && $module != "blog") {

       
		//$return_code .=	"<input type=\"hidden\" name=\"gallery_id\" id=\"gallery_id\" value=".$gallery_id." />";
		$gallery = Sql::getGallery($gallery_id);

		
		if ((count($gallery) > 0) && ($gallery[0]->thumb_id)) {
			$col = 1;
			//d($col,1);die;
		} else {
			$col = 0;
		}
		
        
        $totalImages = count($gallery);

		if (count($gallery) > 0) {
			for ($i=0; $i<$totalImages; $i++) {
				if ($col == 0) {
					$return_code .= "";
				}
				$col++;

				//$imageObj = new Image($gallery->image[$i]["thumb_id"]);
                $imageData = Sql::getImageById($gallery[$i]->thumb_id);
				$altImage = $gallery[$i]->thumb_caption;
				$thumb_caption = Functions::truncate($altImage, 40);
                if ($module != "blog"){
                    $imagesWritten++;
                    if ($imagesWritten % 2 && $imagesWritten != 1) {
                        $return_code .= "<br clear=\"all\" />";
                    }
 				if ($gallery[$i]->image_default == 'y') {
                   $return_code .= "<li data-image-id='".$gallery[$i]->image_id."' class=\"mainImageGallery\">";
                    } else {
                        $return_code .= "<li data-image-id='".$gallery[$i]->image_id."'>";
                    }
                }
                $imageName = $imageData[0]->prefix.'photo_'.$imageData[0]->id.'.'.strtolower($imageData[0]->type);
                //url('custom/domain_1/image_files/'.$imageName)
                $tempCode='<div></div><img width="'.$imageData[0]->width.'" height="'.$imageData[0]->height.'" src="'.config('params.imagesPath').'/'.$imageName.'" alt="" title="" border="0" style="display:inline">';
				$return_code .= "<div style='height: 241px;'><div class=\"galleryImgThumb\">".$tempCode."</div>";
				$return_code .= "<h5>".$thumb_caption."</h5>";
                if ($sess == config('params.SITEMGR_ALIAS')){
                    $return_code .= "<a href=\"".url()."/$sess/uploadimage.php?item_type=$module&level=$level&item_id=".$id."&gallery_id=".$gallery_id."&image_id=".$gallery[$i]->image_id."&thumb_id=".$gallery[$i]->thumb_id."&captions=y\" class=\"ImageEditCaptions iframe fancy_window_imgCaptions\">Captions</a>";
                } else {
                    //echo url('sponsors/change_imagecaption?pop_type=uploadimage&item_type='.$module.'&level='.$level.'&item_id='.$id.'&gallery_id='.$gallery_id.'&image_id='.$gallery[$i]->image_id.'&thumb_id='.$gallery[$i]->thumb_id.'&captions=y');die;
                    $return_code .= "<a  href=\"".url('sponsors/imagecaption?item_type='.$module.'&level='.$level.'&item_id='.$id.'&gallery_id='.$gallery_id.'&image_id='.$gallery[$i]->image_id.'&thumb_id='.$gallery[$i]->thumb_id.'&captions=y')."\" class=\"ImageEditCaptions iframe fancy_window_imgCaptions\">Captions</a>";
                }
               
            $return_code .= "<a  href=\"".url('sponsors/delete_image?item_id='.$id.'&item_type='.$module.'&gallery_image_id='.$gallery[$i]->image_id.'&gallery_id='.$gallery_id)."\" class=\"ImageDelete iframe fancy_window_imgDelete \" >Delete</a>";
				
                if ($main != "false"){
                    if($gallery[$i]->image_default=="n")
                    {
                        $return_code .= "<a href=\"javascript:void(0);\" title=\""."Click here to set as gallery image"."\" class=\"mainImageLinkBK\" onclick=\"makeMain(".$gallery[$i]->image_id.",".$gallery[$i]->thumb_id.",".$id.",'n','$module')\">"."Make Main"."</a>";
                    }
                    else
                    {
                        $return_code .= "<a href=\"javascript:void(0);\" class=\"mainImageLink mainImageLinkBK\" title=\""."Click here to set as gallery image"."\" onclick=\"changeMain(".$gallery[$i]->image_id.",".$gallery[$i]->thumb_id.",".$id.",'y',".$gallery_id.",'$module')\">"."Main"."</a>";
                    }
				}
                
                if ($module != "blog"){
                    $return_code .= "</div></li>";
                }
                
       

			}

			 return $return_code;
           
		}

	}

}
else{
      

	if($new_image=='y')
	{

		$col = 0;
		$sess_id = $gallery_hash;
		

		$gallery = Sql::getGallery_Temp($sess_id);
		$totalImages = count($gallery);
		

		if (count($gallery) > 0) {
		
			for ($i=0; $i<$totalImages; $i++) {
				if ($col == 0) {
					$return_code .= "";
				}


				$col++;

				//$imageObj = new Image($gallery->image[$i]["thumb_id"]);

                $imageData = Sql::getImageById($gallery[$i]->thumb_id);
				$altImage = $gallery[$i]->thumb_caption;
				$thumb_caption = Functions::truncate($altImage, 40);
				$image_id_temp=$gallery[$i]->image_id;
				$thumb_id_temp=$gallery[$i]->thumb_id;
             	if ($module != "blog"){
                    $imagesWritten++;
                    if ($imagesWritten % 2 && $imagesWritten != 1) {
                        $return_code .= "<br clear=\"all\" />";
                    }
                    if ($gallery[$i]->image_default == 'y') {
                   $return_code .= "<li data-image-id='".$image_id_temp."' class=\"mainImageGallery\">";
                    } else {
                        $return_code .= "<li data-image-id='".$image_id_temp."'>";
                    }
                }

               

               $imageName = $imageData[0]->prefix.'photo_'.$imageData[0]->id.'.'.strtolower($imageData[0]->type);
             
               
                $tempCode='<div></div><img width="'.$imageData[0]->width.'" height="'.$imageData[0]->height.'" src="'.url('custom/domain_1/image_files/'.$imageName).'" alt="" title="" border="0" style="display:inline">';

				$return_code .= "<div style='height: 241px;'><div class=\"galleryImgThumb\">".$tempCode."</div>";
				$return_code .= "<h5>".$thumb_caption."</h5>";
                if ($sess == config('params.SITEMGR_ALIAS')){
                    $return_code .= "<a href=\"".url()."/$sess/uploadimage.php?item_type=$module&level=$level&item_id=".$id."&gallery_id=".$gallery_id."&image_id=".$gallery[$i]->image_id."&thumb_id=".$gallery[$i]->thumb_id."&captions=y\" class=\"ImageEditCaptions iframe fancy_window_imgCaptions\">Captions</a>";
                } else {
                    //echo url('sponsors/change_imagecaption?pop_type=uploadimage&item_type='.$module.'&level='.$level.'&item_id='.$id.'&gallery_id='.$gallery_id.'&image_id='.$gallery[$i]->image_id.'&thumb_id='.$gallery[$i]->thumb_id.'&captions=y');die;
                    $return_code .= "<a onclick=\"  var answer = prompt('Please add caption ', '');if(!answer) return false;   var _href = $(this).attr('href'); $(this).attr('href', _href+'&image_caption='+answer);  window.location=$(this).attr('href'); \" href=\"".url('sponsors/change_imagecaption?pop_type=uploadimage&item_type='.$module.'&level='.$level.'&item_id='.$id.'&gallery_id='.$gallery_id.'&image_id='.$gallery[$i]->image_id.'&thumb_id='.$gallery[$i]->thumb_id.'&captions=y')."\" class=\"ImageEditCaptions iframe fancy_window_imgCaptions\">Captions</a>";
                }
               
                $return_code .= "<a onclick=\"if (!confirm('Are you sure?')) return false;\" href=\"".url('sponsors/delete_image?item_id='.$id.'&item_type='.$module.'&gallery_image_id='.$gallery[$i]->image_id.'&gallery_id='.$gallery_id.'&_token='.csrf_token())."\" class=\"ImageDelete iframe fancy_window_imgDelete \" >Delete</a>";

				
                if ($main != "false"){
                    if($gallery[$i]->image_default=="n")
                    {
                        $return_code .= "<a href=\"javascript:void(0);\" title=\""."Click here to set as gallery image"."\" class=\"mainImageLinkBK\" onclick=\"makeMain(".$gallery[$i]->image_id.",".$gallery[$i]->thumb_id.",".$id.",'n','$module')\">"."Main"."</a>";
                    }
                    else
                    {
                    	 $return_code .=	"<input type=\"hidden\" name=\"image_id\" id=\"image_id\" value=".$image_id_temp." />";

                    	  $return_code .=	"<input type=\"hidden\" name=\"thumb_id\" id=\"thumb_id\" value=".$thumb_id_temp." />";


                    	 
                        $return_code .= "<a href=\"javascript:void(0);\" class=\"mainImageLink mainImageLinkBK\" title=\""."Click here to set as gallery image"."\" onclick=\"changeMain(".$gallery[$i]->image_id.",".$gallery[$i]->thumb_id.",".$id.",'y',".$gallery_id.",'$module')\">"."Main"."</a>";
                    }


                     if ($module != "blog"){
                    $return_code .= "</div></li>";
                    
                }
			}
                 

                 
             
			}

			 
			return $return_code;    
	
		  
			
			
		}

           
		

	}



       }


  

	
     }
 }
 
    
    function sponser_popupUploadImage(Request $request)
    {
        
        //$gallery_hash = $request->gallery_hash;
        $item_type = $request->item_type;
        $item_id = $request->item_id;
        $gallery_id = $request->gallery_id;
        $photos = $request->photos;
        $level = $request->level;
        $domain_id = $request->domain_id;
        $gallery_hash = $request->gallery_hash;
        
        return view('front.sponsers_image_popup',compact('gallery_hash','item_type','item_id','gallery_id','photos','level','domain_id'));

    }
    
    function sponser_popupstoreImage(Request $request)
    {
       
        if($request->_token==csrf_token())
     {

      //crop and submit form

        $userAccountId = Session::get('SESS_ACCOUNT_ID');
		$dir  = "custom/domain_1/image_files/";
        $randNumber = round(microtime(true));
        
       $temp = explode(".", $_FILES["image"]["name"]);
       $newfilename = "resize_".$randNumber . '.' . end($temp);
       $item_type = $request->item_type;
       $level = $request->level;

       
    if (!Session::get('random_key'))
    {
        Session::put('random_key', uniqid(($userAccountId).strtotime(date('Y-m-d H:i:s'))));
        
    }

	$upload_dir = "custom/domain_1/image_files";			// The directory for the images to be saved in
	$upload_path = $upload_dir."/";																	// The path to where the image will be saved
	$large_image_prefix = "resize_";																// The prefix name to large image
	$large_image_name = $large_image_prefix.Session::get('random_key');								// New name of the large image (append the timestamp to the filename)
	$large_image_location = $upload_path.$large_image_name;											// Path to the image folder

    if ($item_type == "blog"){
        $item_type = "post";
    }
    
	if ($item_type == "listing"){
		$thumbWidthItem = config('params.IMAGE_LISTING_THUMB_WIDTH');
		$thumbHeightItem = config('params.IMAGE_LISTING_THUMB_HEIGHT');
		
	}elseif($item_type == "classified"){

		$thumbWidthItem = IMAGE_CLASSIFIED_THUMB_WIDTH;
		$thumbHeightItem = IMAGE_CLASSIFIED_THUMB_HEIGHT;	

	}elseif($item_type == "article"){
		$thumbWidthItem = IMAGE_ARTICLE_THUMB_WIDTH;
		$thumbHeightItem = IMAGE_ARTICLE_THUMB_HEIGHT;	
	}elseif($item_type == "event"){
		$thumbWidthItem = IMAGE_EVENT_THUMB_WIDTH;
		$thumbHeightItem = IMAGE_EVENT_THUMB_HEIGHT;
	}elseif($item_type == "post"){
		$thumbWidthItem = IMAGE_BLOG_THUMB_WIDTH;
		$thumbHeightItem = IMAGE_BLOG_THUMB_HEIGHT;
	}

   
       
        if ($_POST["uploadThumb"] == "yes") { //upload image
		// Only one of these image types should be allowed for upload
		$allowed_image_types = array('image/pjpeg'=>"jpg",'image/jpeg'=>"jpg",'image/jpg'=>"jpg",'image/gif'=>"gif",'image/x-png'=>"png",'image/png'=>"png");
		$allowed_image_ext = array_unique($allowed_image_types); // do not change this
		$image_ext = "";

		foreach ($allowed_image_ext as $mime_type => $ext) {
			$image_ext .= strtoupper($ext)." / ";
		}
		$image_ext = substr($image_ext,0,strlen($image_ext)-2);

		
        Session::forget('random_key');
        Session::put('random_key', uniqid(($userAccountId).strtotime(date('Y-m-d H:i:s'))));
		
		//Image Locations
		$large_image_name = $large_image_prefix.Session::get('random_key');
		$large_image_location = $upload_path.$large_image_name;
		$dir =  "custom/domain_1/image_files";
		$files = glob("$dir/resize_".Session::get('random_key').".*");
		
		//removing image files
		if ($files)
			foreach ($files as $file) unlink($file);

		//Get the file information
		$userfile_tmp = $_FILES['image']['tmp_name'];
		$userfile_size = $_FILES['image']['size'];
		$userfile_type = $_FILES['image']['type'];
		$filename = basename($_FILES['image']['name']);
		$file_ext = strtolower(substr($filename, strrpos($filename, '.') + 1));
		
		$maxImageSize = "0000000";
		$maxImageSize = substr($maxImageSize, strlen((config('params.UPLOAD_MAX_SIZE') * 10) + 1));
		$maxImageSize = ((config('params.UPLOAD_MAX_SIZE') * 10) + 1)."00000";

		list($imagewidth, $imageheight, $imageType) = getimagesize($userfile_tmp);
		$auxfile_ext = image_type_to_mime_type($imageType);
		$auxextArray = explode("/",$auxfile_ext);

		$file_ext = $auxextArray[1];
		if ($file_ext == "jpeg"){
            $file_ext = "jpg";
        }
		
		//Only process if the file is a JPG, GIF or PNG and below the allowed limit
		if((!empty($_FILES["image"])) && ($_FILES['image']['error'] == 0)) {

			foreach ($allowed_image_types as $mime_type => $ext) {
				//loop through the specified image types and if they match the extension then break out
				//everything is ok so go and check file size
				if($file_ext == $ext && $userfile_type == $mime_type){
					$error = "";
					break;
				}else{
					$error = "<p class=\"errorMessage\">Only  <strong>".$image_ext."</strong> images accepted for upload!</p>";
				}
			}
			//check if the file size is above the allowed limit
			if ($userfile_size > $maxImageSize) {
				$error .= "<p class=\"errorMessage\">Images must be under  ".config('params.UPLOAD_MAX_SIZE')."MB</p>";
			}

		}else{
			$error = "<p class=\"errorMessage\"> Select an image for upload!</p>";
		}
		//Everything is ok, so we can upload the image.
		if (strlen($error) == 0){

			if (isset($_FILES['image']['name'])){
				//this file could now has an unknown file extension (we hope it's one of the ones set above!)
				$large_image_location = $large_image_location.".".$file_ext;

				//put the file ext in the session so we know what file to look for once its uploaded
                Session::put('user_file_ext',".".$file_ext);
                //$request->file('image')->move($userfile_tmp,$large_image_location);
				move_uploaded_file($userfile_tmp, $large_image_location);
				chmod($large_image_location, 0777);
                list($width, $height, $type, $attr) = getimagesize($large_image_location);
                
				$width = $width;
				$height = $height;
			}
			//Refresh the page to show the new uploaded image
            $gallery_hash = $request->gallery_hash;
            $captions = $request->captions;
            $temp =$request->temp;
            $gallery_item_id = $request->gallery_item_id;
            $gallery_id = $request->gallery_id;
            $image_id = $request->image_id;
            $thumb_id = $request->thumb_id;
            $item_id = $request->item_id;
            $item_type = $request->item_type;
            $main = $request->main;
            $pop_type = $request->pop_type;
            $domain_id = $request->domain_id;
            //d('Error',1);


            return view('front.sponsers_image_crop',compact('gallery_hash','captions','temp','gallery_item_id',
            'gallery_id','image_id','thumb_id','item_id','item_type','main','pop_type','level','domain_id','large_image_location',
            'thumbWidthItem','thumbHeightItem','width','height'));
            
		}
	}
    
	
       }
     return $return_upload_message;
   
    }
    
    
    function sponser_popupCropImage(Request $request)
    {
       
        if($request->_token==csrf_token())
        {
            $return_upload_message="";
            $userAccountId = Session::get('SESS_ACCOUNT_ID');
            //Refresh the page to show the new uploaded image
            $gallery_hash = $request->gallery_hash;
            $captions = $request->captions;
            $temp =$request->temp;
            $gallery_item_id = $request->gallery_item_id;
            $gallery_id = $request->gallery_id;
            $image_id = $request->image_id;
            $thumb_id = $request->thumb_id;
            $item_id = $request->item_id;
            $item_type = $request->item_type;
            $main = $request->main;
            $pop_type = $request->pop_type;
            $domain_id = $request->domain_id;
            $w = $request->w;
            $h = $request->h;
            $x1 = $request->x1;
            $y1 = $request->y1;
            
            
            $thumbWidthItem = $request->thumbWidthItem; 
            $thumbHeightItem  = $request->thumbHeightItem;
            
            $imageWidthItem = config('params.IMAGE_SLIDER_WIDTH');
            $imageHeightItem = config('params.IMAGE_SLIDER_HEIGHT');
    
            $dir =  "custom/domain_1/image_files";
            $files = glob("$dir/resize_".Session::get('random_key').".*");

		if ($files[0]){
			$info = getimagesize($files[0]);
			$type = $info['mime'];

			switch ($type) {
				case 'image/gif':	$img_type = 'gif';
									$img_r = imagecreatefromgif($files[0]);
									break;

				case 'image/jpeg':	$img_type = 'jpeg';
                                    $img_r = imagecreatefromjpeg($files[0]);                                    
									break;

				case 'image/x-png':	$img_type = 'png';
									$img_r = imagecreatefrompng($files[0]);
									break;

				case 'image/png':	$img_type = 'png';
									$img_r = imagecreatefrompng($files[0]);
									break;
			}

            if (!$w || !$h) {
                $upload_image = "failed";
            } else {
            
                $dst_r = ImageCreateTrueColor( $w, $h );

                if ($img_r) {

                    $lowQuality = false;
                    if($img_type == "png" || $img_type == "gif"){
                        imagealphablending($dst_r, false);
                        imagesavealpha($dst_r,true);
                        $transparent = imagecolorallocatealpha( $dst_r, 255, 255, 255, 127 );
                        imagefill( $dst_r, 0, 0, $transparent );
                        imagecolortransparent( $dst_r, $transparent);
                        $transindex = imagecolortransparent($img_r);
                        if($transindex >= 0) {
                            $lowQuality = true; //only use imagecopyresized (low quality) if the image is a transparent gif
                        }
                    }

                    if ($img_type == "gif" && $lowQuality){ //use imagecopyresized for gif to keep the transparency. The functions imagecopyresized and imagecopyresampled works in the same way with the exception that the resized image generated through imagecopyresampled is smoothed so that it is still visible.
                        //low quality
                        imagecopyresized( $dst_r,
                                        $img_r,
                                        0,
                                        0,
                                        $x1,
                                        $y1,
                                        $w,
                                        $h,
                                        $w,
                                        $h
                                    );
                    } else {
                        //better quality
                        imagecopyresampled( $dst_r,
                                        $img_r,
                                        0,
                                        0,
                                        $x1,
                                        $y1,
                                        $w,
                                        $h,
                                        $w,
                                        $h
                                    );
                    }


                }

                /*
                * Saving JPG as PNG 
                */
                if((config('params.FORCE_SAVE_JPG_AS_PNG') == "on") && ($img_type == "jpeg")) {                
                    $crop_image = $dir."/crop_image.png";
                }else{                
                    $crop_image = $dir."/crop_image.$img_type";
                }			

                if ($img_type == 'gif') {
                    imagegif($dst_r, $crop_image);
                } elseif ($img_type == 'jpeg') {
                    if(config('params.FORCE_SAVE_JPG_AS_PNG') == "on"){
                        imagepng($dst_r, $crop_image);
                    }else{
                        imagejpeg($dst_r, $crop_image);
                    }
                } elseif ($img_type == 'png') {
                    imagepng($dst_r, $crop_image);
                }

                
                    $auxPrefix = $userAccountId."_";
                

                //removing image files
                foreach($files as $file) unlink($file);
                if ((file_exists($_FILES['image']['tmp_name']) || file_exists($crop_image))) {
                    $imageArray = $this->image_uploadForItem((($crop_image) ? $crop_image : $_FILES['image']['tmp_name']), $auxPrefix,
                     $imageWidthItem, $imageHeightItem, $thumbWidthItem, $thumbHeightItem);
                    if ($imageArray["success"]) {
                        $upload_image = "success";
                        $remove_image = false;
                    } else { 
                        $upload_image = "failed";
                    }
                }
            }
		}

		if($upload_image == "failed"){ // error or no image uploaded
			$return_upload_message .= "<p class=\"errorMessage\">Error uploading image. Please try again.".($error ? '<br />'.$error : '')."</p>";
		} else { // image uploaded or captions changed
			
			$sess_id = $request->gallery_hash;

			if ($imageArray["image_id"]){ // image uploaded

				$sqlCount = "SELECT COUNT(image_id) AS total FROM Gallery_Temp WHERE sess_id = '".$sess_id."'";
				$totlaTemp = Sql::getTotalofImages($sqlCount);
				
				
				if ($request->item_id){
					
					$str_item_type = ucfirst($request->item_type);
					
					if ($str_item_type == "Listing" || $str_item_type == "Event" || $str_item_type == "Classified" || $str_item_type == "Article"){
						$itemGal = Sql::getGalleryId($request->item_id,'listing');
                        if(count($itemGal)>0)
						{
						  $gal_id = $itemGal[0]->gallery_id;

						
							$gal = Sql::getGallery($gal_id);
                            $total_images = count($gal);
						}
                        else
                        {
                            $total_images = 0;
                        }
							
						
					} else {
						$total_images = $totlaTemp[0]->total;
					}

				} else {
					$total_images = $totlaTemp[0]->total;
				}
				$image_caption = isset($image_caption)?$image_caption:'';
                $thumb_caption = isset($thumb_caption)?$thumb_caption:'';

                   $sql = "INSERT INTO Gallery_Temp (
						image_id,
						image_caption,
						thumb_id,
						thumb_caption,
						image_default,
						sess_id
					)";
				if ((($total_images <= 0 && $totlaTemp[0]->total <= 0)))
					$setasdefault = "y";

				$sql .= "VALUES (
							".$imageArray["image_id"].",
							'".addslashes($image_caption)."',
							".addslashes($imageArray["thumb_id"]).",
							'".addslashes($thumb_caption)."',
							'".(isset($setasdefault) ? "y":"n")."',
							'".$sess_id."'
						)";
                        
                     Sql::insertTempGallery($sql);  

                if($gallery_id!=0)
                {


                	$gallery = Sql::getGallery_Temp($sess_id);
  					$image_id=$gallery[0]->image_id;
					$image_caption=$gallery[0]->image_caption;
					$thumb_id=$gallery[0]->thumb_id;
					$thumb_caption=$gallery[0]->thumb_caption;
					$image_default=$gallery[0]->image_default;
             
                	$sql = "INSERT INTO Gallery_Image (gallery_id,
        												image_id,
        												image_caption,
        												thumb_id,
        												thumb_caption,
        												image_default)
        										VALUES ($gallery_id,
        												$image_id,
        												'$image_caption',
        												$thumb_id,
        												'$thumb_caption',
        												'$image_default')";
        			Sql::insertGallery($sql);


        			$del = "DELETE FROM Gallery_Temp WHERE sess_id ='$sess_id'";
        			Sql::deleteImage($del);



                }
            
				 
                     //$sql ="INSERT INTO `Gallery_Item`(`item_type`, `item_id`, `gallery_id`) VALUES ('".$request->item_type."','".$request->item_id."','".$gallery_id."')" ;
//			         Sql::insertTempGallery($sql);
				
				$uploadImageUpdate = "n";
				
			} else { //captions changed

				if ($request->temp){
					$postImageCaption = addslashes($image_caption);
					$postThumbCaption = addslashes($image_caption);

						

                    $sql = "UPDATE Gallery_Temp SET
                    image_caption = \"$postImageCaption\",
                    thumb_caption = \"$postThumbCaption\"
                    WHERE image_id=".$request->image_id;
					Sql::updateTempGallery($sql);
					
				} else {
					if ($request->gallery_id){
						$postImageCaption = addslashes($image_caption);
						$postThumbCaption = addslashes($thumb_caption);

                        $sql = "UPDATE Gallery_Image SET
                        image_caption = \"$postImageCaption\",
                        thumb_caption = \"$postThumbCaption\"
                        WHERE image_id=".$request->image_id;
						Sql::updateTempGallery($sql);
						
					} else {
						
						$postImageCaption = addslashes($image_caption);
						$postThumbCaption = addslashes($thumb_caption);

						if ($item_type == "post"){
							
                            $sql = "UPDATE Post SET image_caption = \"$postImageCaption\", 
                            thumb_caption = \"$postThumbCaption\"
                            WHERE image_id=".$request->image_id;							
							
						} else {
						
                            $sql = "UPDATE Gallery_Image SET
                            image_caption = \"$postImageCaption\",
                            thumb_caption = \"$postThumbCaption\"
                            WHERE image_id=".$request->image_id;
						
						}
						
						Sql::updateTempGallery($sql);
							
					}
				}
				$uploadImageUpdate = "y";
			}
			
			$return_upload_message .= "<p class=\"successMessage\">".($imageArray["image_id"] > 0 ? ('Image successfully uploaded!') : ('Image successfully updated!'))."</p>";

			
		}
		
            Session::forget('random_key');
   //return redirect('sponsors/addlisting?id='.$request->item_id.'&listingtemplate_id=0&level='.$request->level);

        }
        else
        {
            return false;
        }
    }
    
    function sponser_makemainimage(Request $request)
    {
        if($request->_token==csrf_token())
        {
            
        
            # ----------------------------------------------------------------------------------------------------
        	# CODE
        	# ----------------------------------------------------------------------------------------------------
        	$domain_id = $request->domain_id;
        	$image_id = $request->image_id;
        	$thumb_id = $request->thumb_id;
        	$item_id = $request->item_id;
        	$temp = $request->temp;
        	$item_type = ucfirst($request->item_type);
        	$sess_id = $request->gallery_hash;
        
        	if ($temp == 'y'){
        
        		//$dbMain = db_getDBObject(DEFAULT_DB, true);
        		//$dbObj = db_getDBObjectByDomainID($domain_id, $dbMain);
        		if ($item_id == 0){
        			$sql = "Update Gallery_Temp SET image_default = 'n' WHERE sess_id = '".$sess_id."'";
        			$dbObj->query($sql);
        			$sql = "Update Gallery_Temp SET image_default = 'y' WHERE image_id = $image_id";
        			$dbObj->query($sql);
        		}
        
        		if ($item_id != 0){
        
        			$sql = "SELECT image_id, thumb_id FROM $item_type WHERE id = $item_id";
        			$row = mysql_fetch_array($dbObj->query($sql));
        			$old_image = $row["image_id"];
        			$old_thumb = $row["thumb_id"];
        
        			$sql = "SELECT	id,
        							gallery_id,
        							image_caption,
        							thumb_caption
        					FROM Gallery_Image
        					WHERE image_id = $old_image AND thumb_id = $old_thumb";
        			$row = mysql_fetch_array($dbObj->query($sql));
        
        			$old_id = $row["id"];
        			$old_gallery_id = $row["gallery_id"];
        			$old_image_caption = addslashes($row["image_caption"]);
        			$old_thumb_caption = addslashes($row["thumb_caption"]);
        
        			$sql = "SELECT	image_caption,
        							thumb_caption
        					FROM Gallery_Temp
        					WHERE image_id = $image_id AND thumb_id = $thumb_id";
        			$row = mysql_fetch_array($dbObj->query($sql));
        
        			$image_caption = addslashes($row["image_caption"]);
        			$thumb_caption = addslashes($row["thumb_caption"]);
        
        			$sql = "UPDATE $item_type SET image_id = $image_id, thumb_id = $thumb_id WHERE id = $item_id";
        			$dbObj->query($sql);
        
        			if ($old_image && $old_thumb){
        				$sql = "UPDATE Gallery_Temp SET 
        											image_id = $old_image,
        											image_caption = \"$old_image_caption\",
        											thumb_id = $old_thumb,
        											thumb_caption = \"$old_thumb_caption\",
        											image_default = \"n\"
        						WHERE image_id = $image_id";
        				$dbObj->query($sql);
        
        				$sql = "UPDATE Gallery_Image SET 
        											image_id = $image_id,
        											image_caption = \"$image_caption\",
        											thumb_id = $thumb_id,
        											thumb_caption = \"$thumb_caption\"
        						WHERE id = $old_id";
        				$dbObj->query($sql);
        
        			}else {
        				$sql = "DELETE FROM Gallery_Temp WHERE image_id = $image_id";
        				$dbObj->query($sql);
        				$sql = "SELECT gallery_id FROM Gallery_Item WHERE item_id = $item_id AND item_type = '".string_strtolower($item_type)."'";
        				$row = mysql_fetch_array($dbObj->query($sql));
        				$gallery_id = $row["gallery_id"];
                        $sql = "Update Gallery_Temp SET image_default = 'n' WHERE sess_id = '".$sess_id."'";
                        $dbObj->query($sql);
        				$sql = "INSERT INTO Gallery_Image (gallery_id,
        													image_id,
        													image_caption,
        													thumb_id,
        													thumb_caption,
        													image_default)
        											VALUES ($gallery_id,
        													$image_id,
        													'$image_caption',
        													$thumb_id,
        													'$thumb_caption',
        													'y')";
        				$dbObj->query($sql);
        			}
        		}
        	}else{
        
        		
        		$sql = "SELECT image_id, thumb_id FROM $item_type WHERE id = $item_id";
                $old_imageData = Sql::getOldImage($sql);
        		
        		$old_image = $old_imageData[0]->image_id;
        		$old_thumb = $old_imageData[0]->thumb_id;
        
        		$sql = "UPDATE $item_type SET image_id = $image_id, thumb_id = $thumb_id WHERE id = $item_id";
 		        Sql::updateImageType($sql);
        
        		$sql = "SELECT	id,
        						gallery_id,
        						image_caption,
        						thumb_caption
        				FROM Gallery_Image
        				WHERE image_id = $image_id AND thumb_id = $thumb_id";
        		$imageData = Sql::getImageGallery($sql);
        
        		$id = $imageData[0]->id;
        		$gallery_id = $imageData[0]->gallery_id;
        		$image_caption = addslashes($imageData[0]->image_caption);
        		$thumb_caption = addslashes($imageData[0]->thumb_caption);
        
        		if ($old_image && $old_thumb){
        
        
        			 $sql = "SELECT	id,
        							image_caption,
        							thumb_caption
        					FROM Gallery_Image
        					WHERE image_id = $old_image AND thumb_id = $old_thumb";
        			$old_imageData = Sql::getImageGallery($sql);
        
        			$old_id = $old_imageData[0]->id;
        			$old_image_caption = addslashes($old_imageData[0]->image_caption);
        			$old_thumb_caption = addslashes($old_imageData[0]->thumb_caption);
        
        			//update the gallery images ids as the older images ids
        			$sql = "UPDATE Gallery_Image SET	image_id = $old_image,
        												image_caption = '$old_image_caption',
        												thumb_id = $old_thumb,
        												thumb_caption = '$old_thumb_caption'
        					WHERE id = $id";
        			Sql::updateImageType($sql);
        
        			//update the new main image captions with the gallery captions
        			$sql = "UPDATE Gallery_Image SET	image_id = $image_id,
        												image_caption = '$image_caption',
        												thumb_id = $thumb_id,
        												thumb_caption = '$thumb_caption'
        					WHERE id = $old_id";
        			Sql::updateImageType($sql);
        		}else {
        			$sql = "INSERT INTO Gallery_Image (gallery_id,
        												image_id,
        												image_caption,
        												thumb_id,
        												thumb_caption,
        												image_default)
        										VALUES ($gallery_id,
        												$image_id,
        												'$image_caption',
        												$thumb_id,
        												'$thumb_caption',
        												'y')";
        			Sql::inserImageNew($sql);
        
        			$sql = "DELETE FROM Gallery_Image WHERE id = $id";
        			Sql::deleteImage($sql);
        		}
        	}
         }
         else
         {
            return false;
         }
    }


 function sponser_changemainimage(Request $request)
    {
        if($request->_token==csrf_token())
        {
            
        
            # ----------------------------------------------------------------------------------------------------
        	# CODE
        	# ----------------------------------------------------------------------------------------------------
        	$domain_id = $request->domain_id;
        	$image_id = $request->image_id;
        	$thumb_id = $request->thumb_id;
        	$item_id = $request->item_id;
        	$temp = $request->temp;
        	$item_type = ucfirst($request->item_type);
        	$sess_id = $request->gallery_hash;
        
        	
        	$gallery_id = $request->gallery_id;
        	
        	$level = $request->level;
        	$sess_id = $request->gallery_hash;
        
        	
        	$sql = "SELECT COUNT(*) as totalImages FROM Gallery_Temp WHERE image_default = 'n' AND sess_id = '".$sess_id."'";
        	$r = Sql::tempGalleryCount($sql);
        
        	//if ($item_type=='Listing') {
//        		$listingLevelGallery = new ListingLevel();
//        		$maxImages = $listingLevelGallery->getImages($level);
//        	}elseif ($item_type=='Article') {
//        		$articleLevelGallery = new ArticleLevel();
//        		$maxImages = $articleLevelGallery->getImages($level);
//        	}elseif ($item_type=='Classified') {
//        		$classifiedLevelGallery = new ClassifiedLevel();
//        		$maxImages = $classifiedLevelGallery->getImages($level);
//        	}elseif ($item_type=='Event') {
//        		$eventLevelGallery = new EventLevel();
//        		$maxImages = $eventLevelGallery->getImages($level);
//        	}
            $maxImages=32;
        	$cont_temp = $r[0]->totalImages;
         
        	if ($gallery_id){
        		$gallery = Sql::getGallery($gallery_id);
                
        		$cont_gal = count($gallery);
        	}
        
        	if (($cont_temp+$cont_gal)>=$maxImages){
        		echo "error";
        	}else{
        		if ($temp=='y'){
        			$sql = "Update Gallery_Temp SET image_default = 'n' WHERE image_id = $image_id";
        			Sql::tempGalleryupdate($sql);
        		}else{
        			$sql = "SELECT	image_caption,
        							thumb_caption
        					FROM Gallery_Image WHERE image_id = $image_id";
        			$r = Sql::getImageGallery($sql);
        			$aux = $r;
        
        			$sql = "DELETE FROM Gallery_Image WHERE image_id = $image_id";
        			Sql::deleteImage($sql);
        			unset($gallery);
        			$gallery = Sql::getGallery($gallery_id);
        
        			
        			$image_caption = $aux[0]->image_caption;
        			
        			$thumb_caption = $aux[0]->thumb_caption;
        			$image_default = 'n';
                    
        			$sql = "INSERT INTO Gallery_Image (gallery_id,
        												image_id,
        												image_caption,
        												thumb_id,
        												thumb_caption,
        												image_default)
        										VALUES ($gallery_id,
        												$image_id,
        												'$image_caption',
        												$thumb_id,
        												'$thumb_caption',
        												'$image_default')"; 
        			Sql::inserImageNew($sql);
        
        			$sql = "UPDATE $item_type SET image_id = '', thumb_id = '' WHERE id = $item_id";
        			Sql::updateImageType($sql);
        		}
        	}
         }
         else
         {
            return false;
         }
    }
    

    function sponser_popup_image(Request $request)
    {	
    	 $item_type = $request->item_type;
        $item_id = $request->item_id;
        $gallery_id = $request->gallery_id;
        //$gallery_hash = $request->gallery_hash;
        $gallery_image_id = $request->gallery_image_id;
       	


        
        return view('front.sponser_image_delete',compact('item_type','item_id','gallery_id','gallery_image_id'));
    }
    
    function sponser_delete_image(Request $request)
    {
        if($request->_token==csrf_token())
        {
        
            if ($request->temp=='y'){
    			
    			$sql = "SELECT thumb_id FROM Gallery_Temp WHERE image_id=".$_POST["gallery_image_id"];
    			$row = mysql_fetch_array($dbObj->query($sql));
    
    			$sql = "DELETE FROM Gallery_Temp WHERE image_id=".$_POST["gallery_image_id"];
    			$dbObj->query($sql);
    
    			$image = new Image();
    			$image->Image($_POST["gallery_image_id"]);
    			$image->Delete();
    			$image = new Image();
    			$image->Image($row["thumb_id"]);
    			$image->Delete();
    		} else{

    			if ($request->gallery_image_id){
    				
    				$sql = "DELETE FROM Gallery_Image WHERE image_id = ".$request->gallery_image_id;
        			
        			Sql::deleteImage($sql);

    			} else {
    
    				$item_type=ucfirst($request->item_type);
    
    				$item = $request->item_id;
                    $itemData =    Sql::getImageFromListing($item);
    				$image_idDel = $itemData[0]->image_id;
    				$thumb_idDel = $itemData[0]->thumb_id;
    
    				$sql = "DELETE FROM Image WHERE id = $image_idDel";
        			Sql::deleteImage($sql);
                    
                    $sql = "DELETE FROM Image WHERE id = $thumb_idDel";
        			Sql::deleteImage($sql);
                    
    				
    				$sql = "UPDATE $item_type SET image_id='',thumb_id='' WHERE id=".$item;
    				Sql::updateImageType($sql);
    			}
    		}
    
    		$return_upload_message= "<p class=\"successMessage\">"."Image successfully deleted!"."</p>";
    		$deleteImageSucess = "success";
             return redirect('sponsors/delete_image')->with('message',$return_upload_message)->with('deleteImageSucess',$deleteImageSucess)->with('item_id',$request->item_id);
        }
        else
        {
            return false;
        }
    }

    function sponsers_image_caption(Request $request)
    {
        
  	header("Content-Type: text/html; charset=utf-8", TRUE);
	header("Accept-Encoding: gzip, deflate");
    header("Expires: Sat, 01 Jan 2000 00:00:00 GMT");
    header("Cache-Control: no-store, no-cache, must-revalidate");
    header("Cache-Control: post-check=0, pre-check", FALSE);
    header("Pragma: no-cache");
        $item_type = $request->item_type;
        $item_id = $request->item_id;
        $gallery_id = $request->gallery_id;
        $level = $request->level;
        //$gallery_hash = $request->gallery_hash;
        $captions = $request->captions;
        $image_id = $request->image_id;
        $thumb_id = $request->thumb_id;

        $row=DB::connection('domain')->table('Gallery_Image')->where('image_id',$image_id)->get();
        if(count($row)>0)
        {
        	$image_caption=$row[0]->image_caption;
        }
        else
        {
        	$image_caption='';

        }

        
        return view('front.sponser_image_caption',compact('item_type','item_id','gallery_id','thumb_id','level','image_id','captions','image_caption'));

    }
    
    function sponser_change_imagecaption(Request $request)
    {
        if($request->_token==csrf_token())
        {
            if ($request->gallery_id){
						$ImageCaption = addslashes($request->image_caption);
						
						$sql = "UPDATE Gallery_Image SET
							image_caption = \"$ImageCaption\"
							
						WHERE image_id=".$request->image_id;
							Sql::updateImageType($sql);
						$return_upload_message= "<p class=\"successMessage\">"."Image successfully updated!	"."</p>";	

                      return redirect('sponsors/imagecaption')->with('message',$return_upload_message);
						
					} 

		}
		else
        {
            return false;
        }			
            
        
    }
    
    
    function image_uploadForItem($tmp_name, $prefix, $fullwidth, $fullheight, $thumbwidth="", $thumbheight="", $force_main = false) {

		$types             = array("1" => "GIF", "2" => "JPG", "3" => "PNG");
		$info              = getimagesize($tmp_name);
		$row_image["type"] = $types[$info[2]];
        $temp = 'custom/temp';
		if ( ($types[$info[2]] == "JPG") || ($types[$info[2]] == "GIF") || ($types[$info[2]] == "PNG") ) {

			copy($tmp_name, $temp."/thumb_".substr(strrchr($tmp_name, "/"), 1));

			$idinserted = $this->image_upload($tmp_name, $fullwidth, $fullheight, $prefix, $force_main);
			//$imageObj->save();

			if ($thumbwidth && $thumbheight) {
			$idinsertedthumb = 	$this->image_upload($temp."/thumb_".substr(strrchr($tmp_name, "/") ,1), $thumbwidth, $thumbheight, $prefix, $force_main);
			$array["thumb_id"] = $idinsertedthumb;	
			}

			$array["success"] = true;
			$array["image_id"] = $idinserted;
            
			

		} else {
			unlink($tmp_name);
			$array["success"] = false;
			$array["image_id"] = 0;
			$array["thumb_id"] = 0;
		}

		return $array;

	}
    
    
    
    function image_upload($tmp_name, $maxWidth, $maxHeight, $prefix, $force_main = false, $resize = true) {

		if ($force_main) {
			$_image_dir = 'custom/profile';
		} else {
			$_image_dir = "custom/domain_1/image_files";
		}
        $temp = url('custom/temp');
		$types               = array("1" => "GIF", "2" => "JPG", "3" => "PNG");
		$image_temp          = $tmp_name;
		$info                = @getimagesize($image_temp);
		$row_image["type"]   = $types[$info[2]];
		$row_image["width"]  = $info[0];
		$row_image["height"] = $info[1];
		$row_image["prefix"] = $prefix;

		if ( ($types[$info[2]] == "JPG") || ($types[$info[2]] == "GIF") || ($types[$info[2]] == "PNG") ) {

			$unique_id = md5(uniqid(rand(), true));

			if ($row_image["type"] == "GIF"){
                $new_name = $unique_id.".gif";
            }
			if ($row_image["type"] == "JPG"){
                $new_name = $unique_id.".jpg";                
            }
			if ($row_image["type"] == "PNG"){
                $new_name = $unique_id.".png";
            }

			@rename($image_temp, $temp."/$new_name");

			if ($resize){
				$this->image_getNewDimension($maxWidth, $maxHeight, $row_image["width"], $row_image["height"], $newWidth, $newHeight);
			} else {
				$newWidth = $row_image["width"];
				$newHeight = $row_image["height"];
			}
		
            $this->set("thumbWidth", $newWidth);
			$this->set("thumbHeight", $newHeight);
			$this->set("destination_path", $image_temp);
			$this->makeThumb($temp."/$new_name");
            
			//if ($row_image["type"] == "GIF") $extension = "gif";
			//if ($row_image["type"] == "JPG") $extension = "jpg";
			//if ($row_image["type"] == "PNG") $extension = "png";

			$extension = strtolower($types[$info[2]]);
            
            if((config('params.FORCE_SAVE_JPG_AS_PNG') == "on") && ($extension == "jpg")){
                $extension = "png";
                $types[$info[2]] = $extension;
            }
            
			$info = getimagesize($image_temp);

			$typeTemp = $row_image["type"] = $types[$info[2]];
			$widthTemp = $row_image["width"] = $info[0];
			$heightTemp = $row_image["height"] = $info[1];
			$prefixTemp = $row_image["prefix"] = $prefix;
            
			$sql = "INSERT INTO Image"
					. " (type, width, height,prefix)"
					. " VALUES"
					. " ('$typeTemp', '$widthTemp', '$heightTemp', '$prefixTemp')";
		
		  if($force_main)
		  {
				$lastinsertedId =  Sql::inserCroppedMainImage($sql);                

		  }
		  else{
	 $lastinsertedId =  Sql::inserCroppedImage($sql);                

		  }		
                                
            
			copy($image_temp, $_image_dir."/".$prefixTemp."photo_".$lastinsertedId.".$extension");
			@unlink($image_temp);

			if ($new_name){
				@unlink($temp."/".$new_name);
			}

			return $lastinsertedId;

		} else {
			@unlink($image_temp);
			return;
		}

	}
    
    
    function image_getNewDimension($maxW, $maxH, $oldW, $oldH, &$newW, &$newH) {
		if (($oldW <= $maxW) && ($oldH <= $maxH)) { // without resize
			$newW = $oldW;
			$newH = $oldH;
		} else { // with resize
			if (($maxW / $oldW) <= ($maxH / $oldH)) { // resize from width
				$newW = $oldW * ($maxW / $oldW);
				$newH = $oldH * ($maxW / $oldW);
			} elseif (($maxW / $oldW) > ($maxH / $oldH)) { // resize from height
				$newW = $oldW * ($maxH / $oldH);
				$newH = $oldH * ($maxH / $oldH);
			}
		}
	}
    
    
    
    
    function makeThumb($file) {

			if (file_exists($file)) {

				// Defining image size
				if ($this->originalSize) { // Original size
					$newWidth = $this->img["width"];
					$newHeight = $this->img["height"];
				} else { // Default size
					$newWidth = $this->thumbWidth;
					$newHeight = $this->thumbHeight;
				}

				// Check if image is supported
				if (!$this->isSupported($file)) {
					$this->writeLine("Image type not supported!");
				} else { // if (!$this->isSupported($file)) {

					$create_func   = $this->retrieveCreateFunction($file);
                    if((strpos(strtolower($this->img["type"]), "jpg") !== false) && (FORCE_SAVE_JPG_AS_PNG == "on")){
                        $src = ImageCreateTrueColor($this->img["width"], $this->img["height"]); 
                    }else{
                        $src = $create_func($file);
                    }
					
					$img           = ImageCreateTrueColor($newWidth, $newHeight);
                    $lowQuality = false;
					
					//fix for transparent png and gif
					if (strpos(strtolower($this->img["type"]), "png") !== false || strpos(strtolower($this->img["type"]), "gif") !== false){
						imagealphablending($img, false);
						imagesavealpha($img,true);
						$transparent = imagecolorallocatealpha( $img, 255, 255, 255, 127 );
						imagefill( $img, 0, 0, $transparent );
						imagecolortransparent( $img, $transparent);
                        $transindex = imagecolortransparent($src);
                        if($transindex >= 0) {
                            $lowQuality = true; //only use imagecopyresized (low quality) if the image is a transparent gif
                        }
					}
					
					if (strpos(strtolower($this->img["type"]), "gif") !== false && $lowQuality){
						//low quality, but keeps tranparent for .gif
						imagecopyresized ($img, $src, 0, 0, 0, 0, $newWidth, $newHeight, $this->img["width"], $this->img["height"]);
					} else {
						//better quality, but no transparency for .gif
						@imagecopyresampled ($img, $src, 0, 0, 0, 0, $newWidth, $newHeight, $this->img["width"], $this->img["height"]);
					}
					

					if ($this->drawImgBorder) {
						ImageRectangle($img, 0, 0, $newWidth -2, $newHeight -2, $borderColor);
					}

					$generate_func = $this->retrieveGenerateFunction($file);
                    if($generate_func == "imagejpeg"){                        
                        if(FORCE_SAVE_JPG_AS_PNG == "on"){                            
                            imagepng($img,$this->destination_path);
                        }else{                            
                            $generate_func($img,$this->destination_path,82); // To increase quality of image
                            //$generate_func($img,$this->destination_path);
                        }                   
                        
                    }else{
                        $generate_func($img,$this->destination_path);                        
                    }


					if ($this->screenOutput) {
                        
						$generate_func = $this->retrieveGenerateFunction($file);
						header("Content-type: image/jpeg");
						$generate_func($img);
					}

				} // } else { // if (!$this->isSupported($file)) {

			} else $this->writeLine("Image not found!");

		} 
        
        
        
        function isSupported($file) {
			$this->loadFileInfo($file);
			$ext   =& $this->supportedExt;
			$count =& $ext["count"];
			for ($i = 0; $i < $count; $i++) {
				if ($ext["type"][$i] == $this->img["type"]) {
					$supported = TRUE;
					break;
				} else{
                    $supported = FALSE;
                }
			}
			return $supported;
		}
        
        
        function loadFileInfo($file) {
			if (file_exists($file)) {
				list ($this->img["width"], $this->img["height"]) = GetImageSize ($file);
				$this->img["str_len"]  = strlen(basename($file));
				$this->img["name"]     = substr(basename($file),0,$len-4);
				$this->img["ext"]      = substr(basename($file),$len-3,strlen(basename($file)));
				if (strtoupper($this->img["ext"]) == "JPG" || strtoupper($this->img["ext"]) == "JPEG"){                    
                    $this->img["type"] = "IMAGETYPE_JPEG";                    
                }elseif (strtoupper($this->img["ext"]) == "GIF"){
                    $this->img["type"] = "IMAGETYPE_GIF";
                }elseif (strtoupper($this->img["ext"]) == "PNG"){
                    $this->img["type"] = "IMAGETYPE_PNG";
                }
			}
		}
        
        function retrieveCreateFunction($file) {
			$ext   =& $this->supportedExt;
			$count =& $ext["count"];
			for ($i = 0; $i < $count; $i++){
                if ($ext["type"][$i] == $this->img["type"]){
                    break;
                }
            }
			return $ext["create_function"][$i];
		}
        
        /**
		* Set values for class variables.
		*
		* @name:   set
		* @access: public
		* @param:  string $key
		* @param:  string $value
		* @return: void
		**/
		function set($key, $value) {
			$this->$key = $value;
		}
        
        /**
		* This function meant to set all supported image types and your respective create functions.
		* So if you are trying to resize a jpg file you should use this functions like this below:
		* -> $this->setSupported(IMAGETYPE_JPEG, imageCreateFromJpeg);
		* Note that the default settings are done by the constructor of this class.
		*
		* @name:   setSupported
		* @access: private
		* @param:  string $value
		* @param:  string $function
		* @return: void
		**/
		function setSupported($value, $create_function,$generate_function) {
			$this->supportedExt["type"][]              = $value;
			$this->supportedExt["create_function"][]   = $create_function;
			$this->supportedExt["generate_function"][] = $generate_function;
			$this->supportedExt["count"]               = count($this->supportedExt["type"]);
		}
        
        /**
		* This function meant to create a image with a error message when the flag outputScreen is turned on.
		*
		* @name:   writeLine
		* @access: private
		* @param:  string $string
		* @param:  string $width
		* @return: void
		**/
		function writeLine($string, $width = FALSE) {
			if ($width === FALSE) {
				$strLen = strlen($string);
				$width = ($strLen * 10) - ($strLen * 2.8);
			}
			$img        = ImageCreateTrueColor($width+1, 16);
			$textColor   = ImageColorAllocate ($img, 255, 255, 255);
			$borderColor = ImageColorAllocate ($img, 0, 0, 0);
			if ($this->drawBorder) ImageRectangle($img, 0, 0, $width, 15, $borderColor);
			ImageString ($img, 3, $this->initialPixelCol, $this->initialPixelRow,  $string, $borderColor);
			if ($this->screenOutput) {
				$generate_func   = $this->retrieveGenerateFunction($file);
				header("Content-type: image/jpeg");
				$generate_func($img);
			}
		}
        
        
        /**
		 * <code>
		 *		$this->makeFromRow($row);
		 * <code>
		 * @copyright Copyright 2005 Arca Solutions, Inc.
		 * @author Arca Solutions, Inc.
		 * @version 8.0.00
		 * @name makeFromRow
		 * @access Public
		 * @param array $row
		 */
		function makeFromRow($row='') {

			$status = new ItemStatus();
			$level = new ListingLevel();

			$this->id					= ($row["id"])					? $row["id"]					: ($this->id				? $this->id					: 0);
			$this->account_id			= ($row["account_id"])			? $row["account_id"]			: 0;
			$this->image_id				= ($row["image_id"])			? $row["image_id"]				: ($this->image_id			? $this->image_id			: 0);
			$this->thumb_id				= ($row["thumb_id"])			? $row["thumb_id"]				: ($this->thumb_id			? $this->thumb_id			: 0);
			$this->promotion_id			= ($row["promotion_id"])		? $row["promotion_id"]			: ($this->promotion_id		? $this->promotion_id		: 0);
			$this->location_1			= ($row["location_1"])			? $row["location_1"]			: 0;
			$this->location_2			= ($row["location_2"])			? $row["location_2"]			: 0;
			$this->location_3			= ($row["location_3"])			? $row["location_3"]			: 0;
			$this->location_4			= ($row["location_4"])			? $row["location_4"]			: 0;
			$this->location_5			= ($row["location_5"])			? $row["location_5"]			: 0;
			$this->renewal_date			= ($row["renewal_date"])		? $row["renewal_date"]			: ($this->renewal_date		? $this->renewal_date		: 0);
			$this->discount_id			= ($row["discount_id"])			? $row["discount_id"]			: "";
			$this->reminder				= ($row["reminder"])			? $row["reminder"]				: ($this->reminder			? $this->reminder			: 0);
			$this->entered				= ($row["entered"])				? $row["entered"]				: ($this->entered			? $this->entered			: "");
			$this->updated				= ($row["updated"])				? $row["updated"]				: ($this->updated			? $this->updated			: "");
			$this->title				= ($row["title"])				? $row["title"]					: ($this->title				? $this->title				: "");
			$this->seo_title			= ($row["seo_title"])			? $row["seo_title"]				: ($this->seo_title			? $this->seo_title			: "");
			$this->claim_disable		= ($row["claim_disable"])		? $row["claim_disable"]			: "n";
			$this->friendly_url			= ($row["friendly_url"])		? $row["friendly_url"]			: "";
			$this->email				= ($row["email"])				? $row["email"]					: "";
			$this->url					= ($row["url"])					? $row["url"]					: "";
			$this->display_url			= ($row["display_url"])			? $row["display_url"]			: "";
			$this->address				= ($row["address"])				? $row["address"]				: "";
			$this->address2				= ($row["address2"])			? $row["address2"]				: "";
			$this->zip_code				= ($row["zip_code"])			? $row["zip_code"]				: "";
			$this->zip5                 = ($row["zip5"])                ? $row["zip5"]                  : "";
			$this->phone				= ($row["phone"])				? $row["phone"]					: "";
			$this->fax					= ($row["fax"])					? $row["fax"]					: "";
			$this->description			= ($row["description"])         ? $row["description"]			: "";
			$this->seo_description		= ($row["seo_description"])     ? $row["seo_description"]		: ($this->seo_description	? $this->seo_description	: "");
			$this->long_description     = ($row["long_description"])	? $row["long_description"]		: "";
			$this->video_snippet		= ($row["video_snippet"])		? $row["video_snippet"]			: "";
			$this->video_description	= ($row["video_description"])	? $row["video_description"]		: "";
			$this->keywords             = ($row["keywords"])			? $row["keywords"]				: "";
			$this->seo_keywords         = ($row["seo_keywords"])		? $row["seo_keywords"]			: ($this->seo_keywords		? $this->seo_keywords		: "");
			$this->attachment_file		= ($row["attachment_file"])		? $row["attachment_file"]		: ($this->attachment_file	? $this->attachment_file	: "");
			$this->attachment_caption	= ($row["attachment_caption"])	? $row["attachment_caption"]	: "";
			$this->features             = ($row["features"])            ? $row["features"]              : "";
			$this->price                = ($row["price"])               ? $row["price"]                 : ($this->price		? $this->price		: "");
			$this->facebook_page        = ($row["facebook_page"])       ? $row["facebook_page"]         : "";
			$this->status				= ($row["status"])				? $row["status"]				: $status->getDefaultStatus();
			$this->suspended_sitemgr	= ($row["suspended_sitemgr"])   ? $row["suspended_sitemgr"]		: ($this->suspended_sitemgr		? $this->suspended_sitemgr		: "n");
			$this->level				= ($row["level"])				? $row["level"]					: ($this->level				? $this->level				: $level->getDefaultLevel());
			$this->hours_work			= ($row["hours_work"])			? $row["hours_work"]			: "";
			$this->locations			= ($row["locations"])			? $row["locations"]				: "";
			$this->latitude             = ($row["latitude"])			? $row["latitude"]				: ($this->latitude		? $this->latitude		: "");
			$this->longitude			= ($row["longitude"])			? $row["longitude"]				: ($this->longitude		? $this->longitude		: "");
			$this->map_zoom             = ($row["map_zoom"])            ? $row["map_zoom"]              : 0;
			$this->listingtemplate_id	= ($row["listingtemplate_id"])	? $row["listingtemplate_id"]	: 0;

            $this->custom_text0			= ($row["custom_text0"])		? $row["custom_text0"]			: "";
			$this->custom_text1			= ($row["custom_text1"])		? $row["custom_text1"]			: "";
			$this->custom_text2			= ($row["custom_text2"])		? $row["custom_text2"]			: "";
			$this->custom_text3			= ($row["custom_text3"])		? $row["custom_text3"]			: "";
			$this->custom_text4			= ($row["custom_text4"])		? $row["custom_text4"]			: "";
			$this->custom_text5			= ($row["custom_text5"])		? $row["custom_text5"]			: "";
			$this->custom_text6			= ($row["custom_text6"])		? $row["custom_text6"]			: "";
			$this->custom_text7			= ($row["custom_text7"])		? $row["custom_text7"]			: "";
			$this->custom_text8			= ($row["custom_text8"])		? $row["custom_text8"]			: "";
			$this->custom_text9			= ($row["custom_text9"])		? $row["custom_text9"]			: "";
			$this->custom_short_desc0	= ($row["custom_short_desc0"])	? $row["custom_short_desc0"]	: "";
			$this->custom_short_desc1	= ($row["custom_short_desc1"])	? $row["custom_short_desc1"]	: "";
			$this->custom_short_desc2	= ($row["custom_short_desc2"])	? $row["custom_short_desc2"]	: "";
			$this->custom_short_desc3	= ($row["custom_short_desc3"])	? $row["custom_short_desc3"]	: "";
			$this->custom_short_desc4	= ($row["custom_short_desc4"])	? $row["custom_short_desc4"]	: "";
			$this->custom_short_desc5	= ($row["custom_short_desc5"])	? $row["custom_short_desc5"]	: "";
			$this->custom_short_desc6	= ($row["custom_short_desc6"])	? $row["custom_short_desc6"]	: "";
			$this->custom_short_desc7	= ($row["custom_short_desc7"])	? $row["custom_short_desc7"]	: "";
			$this->custom_short_desc8	= ($row["custom_short_desc8"])	? $row["custom_short_desc8"]	: "";
			$this->custom_short_desc9	= ($row["custom_short_desc9"])	? $row["custom_short_desc9"]	: "";
			$this->custom_long_desc0	= ($row["custom_long_desc0"])	? $row["custom_long_desc0"]		: "";
			$this->custom_long_desc1	= ($row["custom_long_desc1"])	? $row["custom_long_desc1"]		: "";
			$this->custom_long_desc2	= ($row["custom_long_desc2"])	? $row["custom_long_desc2"]		: "";
			$this->custom_long_desc3	= ($row["custom_long_desc3"])	? $row["custom_long_desc3"]		: "";
			$this->custom_long_desc4	= ($row["custom_long_desc4"])	? $row["custom_long_desc4"]		: "";
			$this->custom_long_desc5	= ($row["custom_long_desc5"])	? $row["custom_long_desc5"]		: "";
			$this->custom_long_desc6	= ($row["custom_long_desc6"])	? $row["custom_long_desc6"]		: "";
			$this->custom_long_desc7	= ($row["custom_long_desc7"])	? $row["custom_long_desc7"]		: "";
			$this->custom_long_desc8	= ($row["custom_long_desc8"])	? $row["custom_long_desc8"]		: "";
			$this->custom_long_desc9	= ($row["custom_long_desc9"])	? $row["custom_long_desc9"]		: "";
            $this->custom_checkbox0		= ($row["custom_checkbox0"])	? $row["custom_checkbox0"]		: "n";
			$this->custom_checkbox1		= ($row["custom_checkbox1"])	? $row["custom_checkbox1"]		: "n";
			$this->custom_checkbox2		= ($row["custom_checkbox2"])	? $row["custom_checkbox2"]		: "n";
			$this->custom_checkbox3		= ($row["custom_checkbox3"])	? $row["custom_checkbox3"]		: "n";
			$this->custom_checkbox4		= ($row["custom_checkbox4"])	? $row["custom_checkbox4"]		: "n";
			$this->custom_checkbox5		= ($row["custom_checkbox5"])	? $row["custom_checkbox5"]		: "n";
			$this->custom_checkbox6		= ($row["custom_checkbox6"])	? $row["custom_checkbox6"]		: "n";
			$this->custom_checkbox7		= ($row["custom_checkbox7"])	? $row["custom_checkbox7"]		: "n";
			$this->custom_checkbox8		= ($row["custom_checkbox8"])	? $row["custom_checkbox8"]		: "n";
			$this->custom_checkbox9		= ($row["custom_checkbox9"])	? $row["custom_checkbox9"]		: "n";
			$this->custom_dropdown0		= ($row["custom_dropdown0"])	? $row["custom_dropdown0"]		: "";
			$this->custom_dropdown1		= ($row["custom_dropdown1"])	? $row["custom_dropdown1"]		: "";
			$this->custom_dropdown2		= ($row["custom_dropdown2"])	? $row["custom_dropdown2"]		: "";
			$this->custom_dropdown3		= ($row["custom_dropdown3"])	? $row["custom_dropdown3"]		: "";
			$this->custom_dropdown4		= ($row["custom_dropdown4"])	? $row["custom_dropdown4"]		: "";
			$this->custom_dropdown5		= ($row["custom_dropdown5"])	? $row["custom_dropdown5"]		: "";
			$this->custom_dropdown6		= ($row["custom_dropdown6"])	? $row["custom_dropdown6"]		: "";
			$this->custom_dropdown7		= ($row["custom_dropdown7"])	? $row["custom_dropdown7"]		: "";
			$this->custom_dropdown8		= ($row["custom_dropdown8"])	? $row["custom_dropdown8"]		: "";
			$this->custom_dropdown9		= ($row["custom_dropdown9"])	? $row["custom_dropdown9"]		: "";

			$this->number_views			= ($row["number_views"])		? $row["number_views"]			: ($this->number_views		? $this->number_views	: 0);
			$this->avg_review			= ($row["avg_review"])			? $row["avg_review"]			: ($this->avg_review		? $this->avg_review		: 0);
			$this->package_id			= ($row["package_id"])			? $row["package_id"]			: ($this->package_id			? $this->package_id				: 0);
			$this->package_price		= ($row["package_price"])		? $row["package_price"]			: ($this->package_price			? $this->package_price			: 0);
			$this->backlink				= ($row["backlink"])			? $row["backlink"]				: ($this->backlink				? $this->backlink				: "n");
            $this->backlink_url				= ($row["backlink_url"])			? $row["backlink_url"]				: ($this->backlink_url				? $this->backlink_url				: "");
            $this->clicktocall_number		= ($row["clicktocall_number"])		? $row["clicktocall_number"]	: ($this->clicktocall_number	? $this->clicktocall_number			: "");
            $this->clicktocall_extension	= ($row["clicktocall_extension"])	? $row["clicktocall_extension"]	: ($this->clicktocall_extension	? $this->clicktocall_extension		: 0);
            $this->clicktocall_date			= ($row["clicktocall_date"])		? $row["clicktocall_date"]		: ($this->clicktocall_date		? $this->clicktocall_date			: "");
            $this->activation_date			= ($row["activation_date"])			? $row["activation_date"]		: ($this->activation_date		? $this->activation_date			: "");
            $this->bedroom			= ($row["bedroom"])			? $row["bedroom"]		: "";
            $this->bedsize			= ($row["bedsize"])			? $row["bedsize"]		: "";
            $this->sleeps			= ($row["sleeps"])			? $row["sleeps"]		: "";
            $this->bathroom			= ($row["bathroom"])		? $row["bathroom"]		: "";
            $this->property_type			= ($row["property_type"])		? $row["property_type"]		: "";
            $this->view						= ($row["view"])				? $row["view"]				: ($this->view				? $this->view				: "");
            $this->distance_beach			= ($row["distance_beach"])		? $row["distance_beach"]	: ($this->distance_beach	? $this->distance_beach		: "");
            $this->development_name			= ($row["development_name"])		? $row["development_name"]		: "";
            
            $this->airport_distance			= ($row["airport_distance"])		? $row["airport_distance"]		: "";
            $this->airport_abbreviation			= ($row["airport_abbreviation"])		? $row["airport_abbreviation"]		: "";

            $this->contact_fname			= ($row["contact_fname"])		? $row["contact_fname"]		: "";
            $this->contact_lname			= ($row["contact_lname"])		? $row["contact_lname"]		: "";

            $this->external_link1			= ($row["external_link1"])		? $row["external_link1"]		: "";
            $this->external_link_text1			= ($row["external_link_text1"])		? $row["external_link_text1"]		: "";
            $this->external_link2			= ($row["external_link2"])		? $row["external_link2"]		: "";
            $this->external_link_text2			= ($row["external_link_text2"])		? $row["external_link_text2"]		: "";
            $this->external_link3			= ($row["external_link3"])		? $row["external_link3"]		: "";
            $this->external_link_text3			= ($row["external_link_text3"])		? $row["external_link_text3"]		: "";

            $this->amenity_airhockey = ($row["amenity_airhockey"])	? $row["amenity_airhockey"]	: "n";
			$this->amenity_alarmclock = ($row["amenity_alarmclock"])	? $row["amenity_alarmclock"]	: "n";
			$this->amenity_answeringmachine = ($row["amenity_answeringmachine"])	? $row["amenity_answeringmachine"]	: "n";
			$this->amenity_arcadegames = ($row["amenity_arcadegames"])	? $row["amenity_arcadegames"]	: "n";
			$this->amenity_billiards = ($row["amenity_billiards"])	? $row["amenity_billiards"]	: "n";
			$this->amenity_blender = ($row["amenity_blender"])	? $row["amenity_blender"]	: "n";
			$this->amenity_blurayplayer = ($row["amenity_blurayplayer"])	? $row["amenity_blurayplayer"]	: "n";
			$this->amenity_books = ($row["amenity_books"])	? $row["amenity_books"]	: "n";
			$this->amenity_casetteplayer = ($row["amenity_casetteplayer"])	? $row["amenity_casetteplayer"]	: "n";
			$this->amenity_cdplayer = ($row["amenity_cdplayer"])	? $row["amenity_cdplayer"]	: "n";
			$this->amenity_ceilingfan = ($row["amenity_ceilingfan"])	? $row["amenity_ceilingfan"]	: "n";
			$this->amenity_childshighchair = ($row["amenity_childshighchair"])	? $row["amenity_childshighchair"]	: "n";
			$this->amenity_coffeemaker = ($row["amenity_coffeemaker"])	? $row["amenity_coffeemaker"]	: "n";
			$this->amenity_communalpool = ($row["amenity_communalpool"])	? $row["amenity_communalpool"]	: "n";
			$this->amenity_computer = ($row["amenity_computer"])	? $row["amenity_computer"]	: "n";
			$this->amenity_cookware = ($row["amenity_cookware"])	? $row["amenity_cookware"]	: "n";
			$this->amenity_cookingrange = ($row["amenity_cookingrange"])	? $row["amenity_cookingrange"]	: "n";
			$this->amenity_deckfurniture = ($row["amenity_deckfurniture"])	? $row["amenity_deckfurniture"]	: "n";
			$this->amenity_dishwasher = ($row["amenity_dishwasher"])	? $row["amenity_dishwasher"]	: "n";
			$this->amenity_dishes = ($row["amenity_dishes"])	? $row["amenity_dishes"]	: "n";
			$this->amenity_dvdplayer = ($row["amenity_dvdplayer"])	? $row["amenity_dvdplayer"]	: "n";
			$this->amenity_exercisefacilities = ($row["amenity_exercisefacilities"])	? $row["amenity_exercisefacilities"]	: "n";
			$this->amenity_foosball = ($row["amenity_foosball"])	? $row["amenity_foosball"]	: "n";
			$this->amenity_gametable = ($row["amenity_gametable"])	? $row["amenity_gametable"]	: "n";
			$this->amenity_games = ($row["amenity_games"])	? $row["amenity_games"]	: "n";
			$this->amenity_grill = ($row["amenity_grill"])	? $row["amenity_grill"]	: "n";
			$this->amenity_hairdryer = ($row["amenity_hairdryer"])	? $row["amenity_hairdryer"]	: "n";
			$this->amenity_icemaker = ($row["amenity_icemaker"])	? $row["amenity_icemaker"]	: "n";
			$this->amenity_internet = ($row["amenity_internet"])	? $row["amenity_internet"]	: "n";
			$this->amenity_ironandboard = ($row["amenity_ironandboard"])	? $row["amenity_ironandboard"]	: "n";
			$this->amenity_kidsgames = ($row["amenity_kidsgames"])	? $row["amenity_kidsgames"]	: "n";
			$this->amenity_linensprovided = ($row["amenity_linensprovided"])	? $row["amenity_linensprovided"]	: "n";
			$this->amenity_lobsterpot = ($row["amenity_lobsterpot"])	? $row["amenity_lobsterpot"]	: "n";
			$this->amenity_microwave = ($row["amenity_microwave"])	? $row["amenity_microwave"]	: "n";
			$this->amenity_minirefrigerator = ($row["amenity_minirefrigerator"])	? $row["amenity_minirefrigerator"]	: "n";
			$this->amenity_mp3radiodock = ($row["amenity_mp3radiodock"])	? $row["amenity_mp3radiodock"]	: "n";
			$this->amenity_outsideshower = ($row["amenity_outsideshower"])	? $row["amenity_outsideshower"]	: "n";
			$this->amenity_oven = ($row["amenity_oven"])	? $row["amenity_oven"]	: "n";
			$this->amenity_pinball = ($row["amenity_pinball"])	? $row["amenity_pinball"]	: "n";
			$this->amenity_pingpong = ($row["amenity_pingpong"])	? $row["amenity_pingpong"]	: "n";
			$this->amenity_privatepool = ($row["amenity_privatepool"])	? $row["amenity_privatepool"]	: "n";
			$this->amenity_radio = ($row["amenity_radio"])	? $row["amenity_radio"]	: "n";
			$this->amenity_refrigeratorfreezer = ($row["amenity_refrigeratorfreezer"])	? $row["amenity_refrigeratorfreezer"]	: "n";
			$this->amenity_sofabed = ($row["amenity_sofabed"])	? $row["amenity_sofabed"]	: "n";
			$this->amenity_stereo = ($row["amenity_stereo"])	? $row["amenity_stereo"]	: "n";
			$this->amenity_telephone = ($row["amenity_telephone"])	? $row["amenity_telephone"]	: "n";
			$this->amenity_television = ($row["amenity_television"])	? $row["amenity_television"]	: "n";
			$this->amenity_toaster = ($row["amenity_toaster"])	? $row["amenity_toaster"]	: "n";
			$this->amenity_toasteroven = ($row["amenity_toasteroven"])	? $row["amenity_toasteroven"]	: "n";
			$this->amenity_towelsprovided = ($row["amenity_towelsprovided"])	? $row["amenity_towelsprovided"]	: "n";
			$this->amenity_toys = ($row["amenity_toys"])	? $row["amenity_toys"]	: "n";
			$this->amenity_utensils = ($row["amenity_utensils"])	? $row["amenity_utensils"]	: "n";
			$this->amenity_vacuum = ($row["amenity_vacuum"])	? $row["amenity_vacuum"]	: "n";
			$this->amenity_vcr = ($row["amenity_vcr"])	? $row["amenity_vcr"]	: "n";
			$this->amenity_videogameconsole = ($row["amenity_videogameconsole"])	? $row["amenity_videogameconsole"]	: "n";
			$this->amenity_videogames = ($row["amenity_videogames"])	? $row["amenity_videogames"]	: "n";
			$this->amenity_washerdryer = ($row["amenity_washerdryer"])	? $row["amenity_washerdryer"]	: "n";
			$this->amenity_wifi = ($row["amenity_wifi"])	? $row["amenity_wifi"]	: "n";

			$this->feature_airconditioning = ($row["feature_airconditioning"])	? $row["feature_airconditioning"]	: "n";
			$this->feature_balcony = ($row["feature_balcony"])	? $row["feature_balcony"]	: "n";
			$this->feature_barbecuecharcoal = ($row["feature_barbecuecharcoal"])	? $row["feature_barbecuecharcoal"]	: "n";
			$this->feature_barbecuegas = ($row["feature_barbecuegas"])	? $row["feature_barbecuegas"]	: "n";
			$this->feature_boatslip = ($row["feature_boatslip"])	? $row["feature_boatslip"]	: "n";
			$this->feature_cablesatellitetv = ($row["feature_cablesatellitetv"])	? $row["feature_cablesatellitetv"]	: "n";
			$this->feature_clubhouse = ($row["feature_clubhouse"])	? $row["feature_clubhouse"]	: "n";
			$this->feature_coveredparking = ($row["feature_coveredparking"])	? $row["feature_coveredparking"]	: "n";
			$this->feature_deck = ($row["feature_deck"])	? $row["feature_deck"]	: "n";
			$this->feature_diningroom = ($row["feature_diningroom"])	? $row["feature_diningroom"]	: "n";
			$this->feature_elevator = ($row["feature_elevator"])	? $row["feature_elevator"]	: "n";
			$this->feature_familyroom = ($row["feature_familyroom"])	? $row["feature_familyroom"]	: "n";
			$this->feature_fullkitchen = ($row["feature_fullkitchen"])	? $row["feature_fullkitchen"]	: "n";
			$this->feature_garage = ($row["feature_garage"])	? $row["feature_garage"]	: "n";
			$this->feature_gasfireplace = ($row["feature_gasfireplace"])	? $row["feature_gasfireplace"]	: "n";
			$this->feature_gatedcommunity = ($row["feature_gatedcommunity"])	? $row["feature_gatedcommunity"]	: "n";
			$this->feature_wheelchairaccess = ($row["feature_wheelchairaccess"])	? $row["feature_wheelchairaccess"]	: "n";
			$this->feature_heated = ($row["feature_heated"])	? $row["feature_heated"]	: "n";
			$this->feature_heatedpool = ($row["feature_heatedpool"])	? $row["feature_heatedpool"]	: "n";
			$this->feature_hottubjacuzzi = ($row["feature_hottubjacuzzi"])	? $row["feature_hottubjacuzzi"]	: "n";
			$this->feature_internetaccess = ($row["feature_internetaccess"])	? $row["feature_internetaccess"]	: "n";
			$this->feature_kitchenette = ($row["feature_kitchenette"])	? $row["feature_kitchenette"]	: "n";
			$this->feature_livingroom = ($row["feature_livingroom"])	? $row["feature_livingroom"]	: "n";
			$this->feature_loft = ($row["feature_loft"])	? $row["feature_loft"]	: "n";
			$this->feature_onsitesecurity = ($row["feature_onsitesecurity"])	? $row["feature_onsitesecurity"]	: "n";
			$this->feature_patio = ($row["feature_patio"])	? $row["feature_patio"]	: "n";
			$this->feature_petsallowed = ($row["feature_petsallowed"])		? $row["feature_petsallowed"]		: "n";
			$this->feature_playroom = ($row["feature_playroom"])	? $row["feature_playroom"]	: "n";
			$this->feature_pool = ($row["feature_pool"])	? $row["feature_pool"]	: "n";
			$this->feature_porch = ($row["feature_porch"])	? $row["feature_porch"]	: "n";
			$this->feature_rooftopdeck = ($row["feature_rooftopdeck"])	? $row["feature_rooftopdeck"]	: "n";
			$this->feature_sauna = ($row["feature_sauna"])	? $row["feature_sauna"]	: "n";
			$this->feature_smokingpermitted = ($row["feature_smokingpermitted"])	? $row["feature_smokingpermitted"]	: "n";
			$this->feature_woodfireplace = ($row["feature_woodfireplace"])	? $row["feature_woodfireplace"]	: "n";
			
			$this->activity_antiquing = ($row["activity_antiquing"])			? $row["activity_antiquing"]			: "n";
			$this->activity_basketballcourt = ($row["activity_basketballcourt"])			? $row["activity_basketballcourt"]			: "n";
			$this->activity_beachcombing = ($row["activity_beachcombing"])			? $row["activity_beachcombing"]			: "n";
			$this->activity_bicycling = ($row["activity_bicycling"])			? $row["activity_bicycling"]			: "n";
			$this->activity_bikerentals = ($row["activity_bikerentals"])			? $row["activity_bikerentals"]			: "n";
			$this->activity_birdwatching = ($row["activity_birdwatching"])			? $row["activity_birdwatching"]			: "n";
			$this->activity_boatrentals = ($row["activity_boatrentals"])			? $row["activity_boatrentals"]			: "n";
			$this->activity_boating = ($row["activity_boating"])			? $row["activity_boating"]			: "n";
			$this->activity_botanicalgarden = ($row["activity_botanicalgarden"])			? $row["activity_botanicalgarden"]			: "n";
			$this->activity_canoe = ($row["activity_canoe"])			? $row["activity_canoe"]			: "n";
			$this->activity_churches = ($row["activity_churches"])			? $row["activity_churches"]			: "n";
			$this->activity_cinemas = ($row["activity_cinemas"])			? $row["activity_cinemas"]			: "n";
			$this->activity_bikesprovided = ($row["activity_bikesprovided"])			? $row["activity_bikesprovided"]			: "n";
			$this->activity_deepseafishing = ($row["activity_deepseafishing"])			? $row["activity_deepseafishing"]			: "n";
			$this->activity_fishing = ($row["activity_fishing"])			? $row["activity_fishing"]			: "n";
			$this->activity_fitnesscenter = ($row["activity_fitnesscenter"])			? $row["activity_fitnesscenter"]			: "n";
			$this->activity_golf = ($row["activity_golf"])			? $row["activity_golf"]			: "n";
			$this->activity_healthbeautyspa = ($row["activity_healthbeautyspa"])			? $row["activity_healthbeautyspa"]			: "n";
			$this->activity_hiking = ($row["activity_hiking"])			? $row["activity_hiking"]			: "n";
			$this->activity_horsebackriding = ($row["activity_horsebackriding"])			? $row["activity_horsebackriding"]			: "n";
			$this->activity_horseshoes = ($row["activity_horseshoes"])			? $row["activity_horseshoes"]			: "n";
			$this->activity_hotairballooning = ($row["activity_hotairballooning"])			? $row["activity_hotairballooning"]			: "n";
			$this->activity_iceskating = ($row["activity_iceskating"])			? $row["activity_iceskating"]			: "n";
			$this->activity_jetskiing = ($row["activity_jetskiing"])			? $row["activity_jetskiing"]			: "n";
			$this->activity_kayaking = ($row["activity_kayaking"])			? $row["activity_kayaking"]			: "n";
			$this->activity_livetheater = ($row["activity_livetheater"])			? $row["activity_livetheater"]			: "n";
			$this->activity_marina = ($row["activity_marina"])			? $row["activity_marina"]			: "n";
			$this->activity_miniaturegolf = ($row["activity_miniaturegolf"])			? $row["activity_miniaturegolf"]			: "n";
			$this->activity_mountainbiking = ($row["activity_mountainbiking"])			? $row["activity_mountainbiking"]			: "n";
			$this->activity_museums = ($row["activity_museums"])			? $row["activity_museums"]			: "n";
			$this->activity_paddleboating = ($row["activity_paddleboating"])			? $row["activity_paddleboating"]			: "n";
			$this->activity_paragliding = ($row["activity_paragliding"])			? $row["activity_paragliding"]			: "n";
			$this->activity_parasailing = ($row["activity_parasailing"])			? $row["activity_parasailing"]			: "n";
			$this->activity_playground = ($row["activity_playground"])			? $row["activity_playground"]			: "n";
			$this->activity_recreationcenter = ($row["activity_recreationcenter"])			? $row["activity_recreationcenter"]			: "n";
			$this->activity_restaurants = ($row["activity_restaurants"])			? $row["activity_restaurants"]			: "n";
			$this->activity_rollerblading = ($row["activity_rollerblading"])			? $row["activity_rollerblading"]			: "n";
			$this->activity_sailing = ($row["activity_sailing"])			? $row["activity_sailing"]			: "n";
			$this->activity_shelling = ($row["activity_shelling"])			? $row["activity_shelling"]			: "n";
			$this->activity_shopping = ($row["activity_shopping"])			? $row["activity_shopping"]			: "n";
			$this->activity_sightseeing = ($row["activity_sightseeing"])			? $row["activity_sightseeing"]			: "n";
			$this->activity_skiing = ($row["activity_skiing"])			? $row["activity_skiing"]			: "n";
			$this->activity_bayfishing = ($row["activity_bayfishing"])			? $row["activity_bayfishing"]			: "n";
			$this->activity_spa = ($row["activity_spa"])			? $row["activity_spa"]			: "n";
			$this->activity_surffishing = ($row["activity_surffishing"])			? $row["activity_surffishing"]			: "n";
			$this->activity_surfing = ($row["activity_surfing"])			? $row["activity_surfing"]			: "n";
			$this->activity_swimming = ($row["activity_swimming"])			? $row["activity_swimming"]			: "n";
			$this->activity_tennis = ($row["activity_tennis"])			? $row["activity_tennis"]			: "n";
			$this->activity_themeparks = ($row["activity_themeparks"])			? $row["activity_themeparks"]			: "n";
			$this->activity_walking = ($row["activity_walking"])			? $row["activity_walking"]			: "n";
			$this->activity_waterparks = ($row["activity_waterparks"])			? $row["activity_waterparks"]			: "n";
			$this->activity_waterskiing = ($row["activity_waterskiing"])			? $row["activity_waterskiing"]			: "n";
			$this->activity_watertubing = ($row["activity_watertubing"])			? $row["activity_watertubing"]			: "n";
			$this->activity_wildlifeviewing = ($row["activity_wildlifeviewing"])			? $row["activity_wildlifeviewing"]			: "n";
			$this->activity_zoo = ($row["activity_zoo"])			? $row["activity_zoo"]			: "n";
            
            $this->isppilisting        = ($row["isppilisting"])     ? $row["isppilisting"]        : ($this->isppilisting    ? $this->isppilisting    : "");
            $this->deposit_amount        = ($row["deposit_amount"])     ? $row["deposit_amount"]        : ($this->deposit_amount    ? $this->deposit_amount    : 0);
            $this->tax_per_inquiry        = ($row["tax_per_inquiry"])     ? $row["tax_per_inquiry"]        : ($this->tax_per_inquiry    ? $this->tax_per_inquiry    : 0);
            $this->unique_email        = ($row["unique_email"])     ? $row["unique_email"]        : ($this->unique_email    ? $this->unique_email    : "n");
			$this->required_stay = ($row["required_stay"]			? $row["required_stay"]			: '');
			$this->is_featured = ($row["is_featured"]			? $row["is_featured"]			: 0);
			$this->featured_date = ($row["featured_date"]			? $row["featured_date"]			: '');
			$this->search_pos = ($row["search_pos"]			? $row["search_pos"]			: 6);
			$this->search_pos_date = ($row["search_pos_date"]			? $row["search_pos_date"]			: "");

			$this->data_in_array 	= $row;

		}
        
        
        /**
		 * <code>
		 *		//Using this in forms or other pages.
		 *		$listingObj->Save();
		 * <br /><br />
		 *		//Using this in Listing() class.
		 *		$this->Save();
		 * </code>
		 * @copyright Copyright 2005 Arca Solutions, Inc.
		 * @author Arca Solutions, Inc.
		 * @version 8.0.00
		 * @name Save
		 * @access Public
		 */
		function Save() {

			


			$aux_old_account = str_replace("'", "", $this->old_account_id);
			$aux_account = str_replace("'", "", $this->account_id);

			$this->friendly_url = string_strtolower($this->friendly_url);

			if ($this->id) {

				$sql = "SELECT status FROM Listing WHERE id = $this->id";
				$result = $dbObj->query($sql);
				if ($row = mysql_fetch_assoc($result)) $last_status = $row["status"];
				$this_status = $this->status;
				$this_id = $this->id;

				$sql = "UPDATE Listing SET"
					. " account_id         = $this->account_id,"
					. " image_id           = $this->image_id,"
					. " thumb_id           = $this->thumb_id,"
					. " promotion_id       = $this->promotion_id,"
					. " location_1         = $this->location_1,"
					. " location_2         = $this->location_2,"
					. " location_3         = $this->location_3,"
					. " location_4         = $this->location_4,"
					. " location_5         = $this->location_5,"
					. " renewal_date       = $this->renewal_date,"
					. " discount_id        = $this->discount_id,"
					. " reminder           = $this->reminder,"
					. " updated            = NOW(),"
					. " title              = $this->title,"
					. " seo_title          = $this->seo_title,"
					. " claim_disable      = $this->claim_disable,"
					. " friendly_url       = $this->friendly_url,"
					. " email              = $this->email,"
					. " url                = $this->url,"
					. " display_url        = $this->display_url,"
					. " address            = $this->address,"
					. " address2           = $this->address2,"
					. " zip_code           = $this->zip_code,"
					. " phone              = $this->phone,"
					. " fax                = $this->fax,"
					. " description        = $this->description,"
					. " seo_description    = $this->seo_description,"
					. " long_description   = $this->long_description,"
					. " video_snippet      = $this->video_snippet,"
					. " video_description  = $this->video_description,"
					. " keywords           = $this->keywords,"
					. " seo_keywords       = $this->seo_keywords,"
					. " attachment_file    = $this->attachment_file,"
					. " attachment_caption = $this->attachment_caption,"
					. " features           = $this->features,"
					. " price              = $this->price,"
					. " facebook_page      = $this->facebook_page,"
					. " status             = $this->status,"
					. " suspended_sitemgr  = $this->suspended_sitemgr,"
					. " level              = $this->level,"
					. " hours_work         = $this->hours_work,"
					. " locations          = $this->locations,"
					. " listingtemplate_id = $this->listingtemplate_id,"
                    . " custom_text0       = $this->custom_text0,"
					. " custom_text1       = $this->custom_text1,"
					. " custom_text2       = $this->custom_text2,"
					. " custom_text3       = $this->custom_text3,"
					. " custom_text4       = $this->custom_text4,"
					. " custom_text5       = $this->custom_text5,"
					. " custom_text6       = $this->custom_text6,"
					. " custom_text7       = $this->custom_text7,"
					. " custom_text8       = $this->custom_text8,"
					. " custom_text9       = $this->custom_text9,"
					. " custom_short_desc0 = $this->custom_short_desc0,"
					. " custom_short_desc1 = $this->custom_short_desc1,"
					. " custom_short_desc2 = $this->custom_short_desc2,"
					. " custom_short_desc3 = $this->custom_short_desc3,"
					. " custom_short_desc4 = $this->custom_short_desc4,"
					. " custom_short_desc5 = $this->custom_short_desc5,"
					. " custom_short_desc6 = $this->custom_short_desc6,"
					. " custom_short_desc7 = $this->custom_short_desc7,"
					. " custom_short_desc8 = $this->custom_short_desc8,"
					. " custom_short_desc9 = $this->custom_short_desc9,"
					. " custom_long_desc0  = $this->custom_long_desc0,"
					. " custom_long_desc1  = $this->custom_long_desc1,"
					. " custom_long_desc2  = $this->custom_long_desc2,"
					. " custom_long_desc3  = $this->custom_long_desc3,"
					. " custom_long_desc4  = $this->custom_long_desc4,"
					. " custom_long_desc5  = $this->custom_long_desc5,"
					. " custom_long_desc6  = $this->custom_long_desc6,"
					. " custom_long_desc7  = $this->custom_long_desc7,"
					. " custom_long_desc8  = $this->custom_long_desc8,"
					. " custom_long_desc9  = $this->custom_long_desc9,"
                    . " custom_checkbox0   = $this->custom_checkbox0,"
					. " custom_checkbox1   = $this->custom_checkbox1,"
					. " custom_checkbox2   = $this->custom_checkbox2,"
					. " custom_checkbox3   = $this->custom_checkbox3,"
					. " custom_checkbox4   = $this->custom_checkbox4,"
					. " custom_checkbox5   = $this->custom_checkbox5,"
					. " custom_checkbox6   = $this->custom_checkbox6,"
					. " custom_checkbox7   = $this->custom_checkbox7,"
					. " custom_checkbox8   = $this->custom_checkbox8,"
					. " custom_checkbox9   = $this->custom_checkbox9,"
					. " custom_dropdown0   = $this->custom_dropdown0,"
					. " custom_dropdown1   = $this->custom_dropdown1,"
					. " custom_dropdown2   = $this->custom_dropdown2,"
					. " custom_dropdown3   = $this->custom_dropdown3,"
					. " custom_dropdown4   = $this->custom_dropdown4,"
					. " custom_dropdown5   = $this->custom_dropdown5,"
					. " custom_dropdown6   = $this->custom_dropdown6,"
					. " custom_dropdown7   = $this->custom_dropdown7,"
					. " custom_dropdown8   = $this->custom_dropdown8,"
					. " custom_dropdown9   = $this->custom_dropdown9,"
					. " number_views	   = $this->number_views,"
					. " avg_review		   = $this->avg_review,"
					. " latitude           = $this->latitude,"
					. " longitude          = $this->longitude,"
					. " map_zoom           = $this->map_zoom,"
					. " package_id		   = $this->package_id,"
					. " package_price	   = $this->package_price,"
					. " backlink		   = $this->backlink,"
					. " backlink_url            = $this->backlink_url,"
					. " clicktocall_number		= $this->clicktocall_number,"
					. " clicktocall_extension	= $this->clicktocall_extension,"
					. " clicktocall_date		= $this->clicktocall_date,"
					. " activation_date			= $this->activation_date,"
					. " bedroom			= $this->bedroom,"
					. " bedsize			= $this->bedsize,"
					. " sleeps			= $this->sleeps,"
					. " bathroom		= $this->bathroom,"
					. " property_type		= $this->property_type,"
					. " `view`		= $this->view,"
					. " distance_beach		= $this->distance_beach,"
					. " development_name		= $this->development_name,"
					. " airport_distance		= $this->airport_distance,"
					. " airport_abbreviation		= $this->airport_abbreviation,"
					. " contact_fname		= $this->contact_fname,"
					. " contact_lname		= $this->contact_lname,"
					. " external_link1		= $this->external_link1,"
					. " external_link_text1		= $this->external_link_text1,"
					. " external_link2		= $this->external_link2,"
					. " external_link_text2		= $this->external_link_text2,"
					. " external_link3		= $this->external_link3,"
					. " external_link_text3		= $this->external_link_text3,"
					. " amenity_airhockey = $this->amenity_airhockey,"
					. " amenity_alarmclock = $this->amenity_alarmclock,"
					. " amenity_answeringmachine = $this->amenity_answeringmachine,"
					. " amenity_arcadegames = $this->amenity_arcadegames,"
					. " amenity_billiards = $this->amenity_billiards,"
					. " amenity_blender = $this->amenity_blender,"
					. " amenity_blurayplayer = $this->amenity_blurayplayer,"
					. " amenity_books = $this->amenity_books,"
					. " amenity_casetteplayer = $this->amenity_casetteplayer,"
					. " amenity_cdplayer = $this->amenity_cdplayer,"
					. " amenity_ceilingfan = $this->amenity_ceilingfan,"
					. " amenity_childshighchair = $this->amenity_childshighchair,"
					. " amenity_coffeemaker = $this->amenity_coffeemaker,"
					. " amenity_communalpool = $this->amenity_communalpool,"
					. " amenity_computer = $this->amenity_computer,"
					. " amenity_cookware = $this->amenity_cookware,"
					. " amenity_cookingrange = $this->amenity_cookingrange,"
					. " amenity_deckfurniture = $this->amenity_deckfurniture,"
					. " amenity_dishwasher  = $this->amenity_dishwasher,"
					. " amenity_dishes = $this->amenity_dishes,"
					. " amenity_dvdplayer = $this->amenity_dvdplayer,"
					. " amenity_exercisefacilities = $this->amenity_exercisefacilities,"
					. " amenity_foosball = $this->amenity_foosball,"
					. " amenity_gametable = $this->amenity_gametable,"
					. " amenity_games = $this->amenity_games,"
					. " amenity_grill = $this->amenity_grill,"
					. " amenity_hairdryer = $this->amenity_hairdryer,"
					. " amenity_icemaker = $this->amenity_icemaker,"
					. " amenity_internet = $this->amenity_internet,"
					. " amenity_ironandboard = $this->amenity_ironandboard,"
					. " amenity_kidsgames = $this->amenity_kidsgames,"
					. " amenity_linensprovided = $this->amenity_linensprovided,"
					. " amenity_lobsterpot = $this->amenity_lobsterpot,"
					. " amenity_microwave = $this->amenity_microwave,"
					. " amenity_minirefrigerator = $this->amenity_minirefrigerator,"
					. " amenity_mp3radiodock = $this->amenity_mp3radiodock,"
					. " amenity_outsideshower = $this->amenity_outsideshower,"
					. " amenity_oven = $this->amenity_oven,"
					. " amenity_pinball = $this->amenity_pinball,"
					. " amenity_pingpong = $this->amenity_pingpong,"
					. " amenity_privatepool = $this->amenity_privatepool,"
					. " amenity_radio = $this->amenity_radio,"
					. " amenity_refrigeratorfreezer = $this->amenity_refrigeratorfreezer,"
					. " amenity_sofabed = $this->amenity_sofabed,"
					. " amenity_stereo = $this->amenity_stereo,"
					. " amenity_telephone = $this->amenity_telephone,"
					. " amenity_television = $this->amenity_television,"
					. " amenity_toaster = $this->amenity_toaster,"
					. " amenity_toasteroven = $this->amenity_toasteroven,"
					. " amenity_towelsprovided = $this->amenity_towelsprovided,"
					. " amenity_toys = $this->amenity_toys,"
					. " amenity_utensils = $this->amenity_utensils,"
					. " amenity_vacuum = $this->amenity_vacuum,"
					. " amenity_vcr = $this->amenity_vcr,"
					. " amenity_videogameconsole = $this->amenity_videogameconsole,"
					. " amenity_videogames = $this->amenity_videogames,"
					. " amenity_washerdryer = $this->amenity_washerdryer,"
					. " amenity_wifi = $this->amenity_wifi,"
					. " feature_airconditioning = $this->feature_airconditioning,"
					. " feature_balcony = $this->feature_balcony,"
					. " feature_barbecuecharcoal = $this->feature_barbecuecharcoal,"
					. " feature_barbecuegas = $this->feature_barbecuegas,"
					. " feature_boatslip = $this->feature_boatslip,"
					. " feature_cablesatellitetv = $this->feature_cablesatellitetv,"
					. " feature_clubhouse = $this->feature_clubhouse,"
					. " feature_coveredparking = $this->feature_coveredparking,"
					. " feature_deck = $this->feature_deck,"
					. " feature_diningroom = $this->feature_diningroom,"
					. " feature_elevator = $this->feature_elevator,"
					. " feature_familyroom = $this->feature_familyroom,"
					. " feature_fullkitchen = $this->feature_fullkitchen,"
					. " feature_garage = $this->feature_garage,"
					. " feature_gasfireplace = $this->feature_gasfireplace,"
					. " feature_gatedcommunity = $this->feature_gatedcommunity,"
					. " feature_wheelchairaccess = $this->feature_wheelchairaccess,"
					. " feature_heated = $this->feature_heated,"
					. " feature_heatedpool = $this->feature_heatedpool,"
					. " feature_hottubjacuzzi = $this->feature_hottubjacuzzi,"
					. " feature_internetaccess = $this->feature_internetaccess,"
					. " feature_kitchenette = $this->feature_kitchenette,"
					. " feature_livingroom = $this->feature_livingroom,"
					. " feature_loft = $this->feature_loft,"
					. " feature_onsitesecurity = $this->feature_onsitesecurity,"
					. " feature_patio = $this->feature_patio,"
					. " feature_petsallowed = $this->feature_petsallowed,"
					. " feature_playroom = $this->feature_playroom,"
					. " feature_pool = $this->feature_pool,"
					. " feature_porch = $this->feature_porch,"
					. " feature_rooftopdeck = $this->feature_rooftopdeck,"
					. " feature_sauna = $this->feature_sauna,"
					. " feature_smokingpermitted = $this->feature_smokingpermitted,"
					. " feature_woodfireplace = $this->feature_woodfireplace,"
					. " activity_antiquing = $this->activity_antiquing,"
					. " activity_basketballcourt = $this->activity_basketballcourt,"
					. " activity_beachcombing = $this->activity_beachcombing,"
					. " activity_bicycling = $this->activity_bicycling,"
					. " activity_bikerentals = $this->activity_bikerentals,"
					. " activity_birdwatching = $this->activity_birdwatching,"
					. " activity_boatrentals = $this->activity_boatrentals,"
					. " activity_boating = $this->activity_boating,"
					. " activity_botanicalgarden = $this->activity_botanicalgarden,"
					. " activity_canoe = $this->activity_canoe,"
					. " activity_churches = $this->activity_churches,"
					. " activity_cinemas = $this->activity_cinemas,"
					. " activity_bikesprovided = $this->activity_bikesprovided,"
					. " activity_deepseafishing = $this->activity_deepseafishing,"
					. " activity_fishing = $this->activity_fishing,"
					. " activity_fitnesscenter = $this->activity_fitnesscenter,"
					. " activity_golf = $this->activity_golf,"
					. " activity_healthbeautyspa = $this->activity_healthbeautyspa,"
					. " activity_hiking = $this->activity_hiking,"
					. " activity_horsebackriding = $this->activity_horsebackriding,"
					. " activity_horseshoes = $this->activity_horseshoes,"
					. " activity_hotairballooning = $this->activity_hotairballooning,"
					. " activity_iceskating = $this->activity_iceskating,"
					. " activity_jetskiing = $this->activity_jetskiing,"
					. " activity_kayaking = $this->activity_kayaking,"
					. " activity_livetheater = $this->activity_livetheater,"
					. " activity_marina = $this->activity_marina,"
					. " activity_miniaturegolf = $this->activity_miniaturegolf,"
					. " activity_mountainbiking = $this->activity_mountainbiking,"
					. " activity_museums = $this->activity_museums,"
					. " activity_paddleboating = $this->activity_paddleboating,"
					. " activity_paragliding = $this->activity_paragliding,"
					. " activity_parasailing = $this->activity_parasailing,"
					. " activity_playground = $this->activity_playground,"
					. " activity_recreationcenter = $this->activity_recreationcenter,"
					. " activity_restaurants = $this->activity_restaurants,"
					. " activity_rollerblading = $this->activity_rollerblading,"
					. " activity_sailing = $this->activity_sailing,"
					. " activity_shelling = $this->activity_shelling,"
					. " activity_shopping = $this->activity_shopping,"
					. " activity_sightseeing = $this->activity_sightseeing,"
					. " activity_skiing = $this->activity_skiing,"
					. " activity_bayfishing = $this->activity_bayfishing,"
					. " activity_spa = $this->activity_spa,"
					. " activity_surffishing = $this->activity_surffishing,"
					. " activity_surfing = $this->activity_surfing,"
					. " activity_swimming = $this->activity_swimming,"
					. " activity_tennis = $this->activity_tennis,"
					. " activity_themeparks = $this->activity_themeparks,"
					. " activity_walking = $this->activity_walking,"
					. " activity_waterparks = $this->activity_waterparks,"
					. " activity_waterskiing = $this->activity_waterskiing,"
					. " activity_watertubing = $this->activity_watertubing,"
					. " activity_wildlifeviewing = $this->activity_wildlifeviewing,"
					. " activity_zoo = $this->activity_zoo,"
					. " isppilisting = $this->isppilisting,"
					. " deposit_amount = $this->deposit_amount,"
					. " tax_per_inquiry = $this->tax_per_inquiry,"
					. " unique_email = $this->unique_email,"
					. " required_stay = $this->required_stay,"
					. " is_featured = $this->is_featured,"
					. " featured_date = $this->featured_date,"
					. " search_pos = $this->search_pos,"
					. " search_pos_date = $this->search_pos_date"

					. " WHERE id           = $this->id";

				$dbObj->query($sql);

				$last_status = str_replace("\"", "", $last_status);
				$last_status = str_replace("'", "", $last_status);
				$this_status = str_replace("\"", "", $this_status);
				$this_status = str_replace("'", "", $this_status);
				$this_id = str_replace("\"", "", $this_id);
				$this_id = str_replace("'", "", $this_id);
				system_countActiveListingByCategory($this_id);

				if ($last_status != "P" && $this_status == "P"){
					activity_newToApproved($aux_log_domain_id, $this->id, "listing", $this->title);
				} else if ($last_status == "P" && $this_status != "P") {
					activity_deleteRecord($aux_log_domain_id, $this->id, "listing");
				} else if ($last_status == $this_status){
					activity_updateRecord($aux_log_domain_id, $this->id, $this->title, "item", "listing");
				}

				/*
				 * Populate Listings to front
				 */
				unset($listingSummaryObj);
				$listingSummaryObj = new ListingSummary();

				$listingSummaryObj->PopulateTable($this->id, "update");
				$this->updateCategoryStatusByID();

				if ($aux_old_account != $aux_account && $aux_account != 0) {
					$accDomain = new Account_Domain($aux_account, SELECTED_DOMAIN_ID);
					$accDomain->Save();
					$accDomain->saveOnDomain($aux_account, $this);
				}
                
			} else {
                $aux_seoDescription = $this->description;
                $aux_seoDescription = str_replace(array("\r\n", "\n"), " ", $aux_seoDescription);
                $aux_seoDescription = str_replace("\\\"", "", $aux_seoDescription);
                
				$sql = "INSERT INTO Listing"
					. " (account_id,"
					. " image_id,"
					. " thumb_id,"
					. " promotion_id,"
					. " location_1,"
					. " location_2,"
					. " location_3,"
					. " location_4,"
					. " location_5,"
					. " renewal_date,"
					. " discount_id,"
					. " reminder,"
					. " fulltextsearch_keyword,"
					. " fulltextsearch_where,"
					. " updated,"
					. " entered,"
					. " title,"
					. " seo_title,"
					. " claim_disable,"
					. " friendly_url,"
					. " email,"
					. " url,"
					. " display_url,"
					. " address,"
					. " address2,"
					. " zip_code,"
					. " phone,"
					. " fax,"
					. " description,"
					. " seo_description,"
					. " long_description,"
					. " video_snippet,"
					. " video_description,"
					. " keywords,"
					. " seo_keywords,"
					. " attachment_file,"
					. " attachment_caption,"
					. " features,"
					. " price,"
					. " facebook_page,"
					. " status,"
					. " level,"
					. " hours_work,"
					. " locations,"
					. " listingtemplate_id,"
                    . " custom_text0,"
					. " custom_text1,"
					. " custom_text2,"
					. " custom_text3,"
					. " custom_text4,"
					. " custom_text5,"
					. " custom_text6,"
					. " custom_text7,"
					. " custom_text8,"
					. " custom_text9,"
					. " custom_short_desc0,"
					. " custom_short_desc1,"
					. " custom_short_desc2,"
					. " custom_short_desc3,"
					. " custom_short_desc4,"
					. " custom_short_desc5,"
					. " custom_short_desc6,"
					. " custom_short_desc7,"
					. " custom_short_desc8,"
					. " custom_short_desc9,"
					. " custom_long_desc0,"
					. " custom_long_desc1,"
					. " custom_long_desc2,"
					. " custom_long_desc3,"
					. " custom_long_desc4,"
					. " custom_long_desc5,"
					. " custom_long_desc6,"
					. " custom_long_desc7,"
					. " custom_long_desc8,"
					. " custom_long_desc9,"
                    . " custom_checkbox0,"
					. " custom_checkbox1,"
					. " custom_checkbox2,"
					. " custom_checkbox3,"
					. " custom_checkbox4,"
					. " custom_checkbox5,"
					. " custom_checkbox6,"
					. " custom_checkbox7,"
					. " custom_checkbox8,"
					. " custom_checkbox9,"
					. " custom_dropdown0,"
					. " custom_dropdown1,"
					. " custom_dropdown2,"
					. " custom_dropdown3,"
					. " custom_dropdown4,"
					. " custom_dropdown5,"
					. " custom_dropdown6,"
					. " custom_dropdown7,"
					. " custom_dropdown8,"
					. " custom_dropdown9,"
					. " number_views,"
					. " avg_review,"
					. " latitude,"
					. " longitude,"
					. " map_zoom,"
					. " package_id,"
					. " package_price,"
					. " backlink,"
					. " backlink_url,"
					. " clicktocall_number,"
					. " clicktocall_extension,"
					. " clicktocall_date,"
					. " activation_date,"
					. " bedroom,"
					. " bedsize,"
					. " sleeps,"
					. " bathroom,"
					. " property_type,"
					. " `view`,"
					. " distance_beach,"
					. " development_name,"
					. " airport_distance,"
					. " airport_abbreviation,"
					. " contact_fname,"
					. " contact_lname,"
					. " external_link1,"
					. " external_link_text1,"
					. " external_link2,"
					. " external_link_text2,"
					. " external_link3,"
					. " external_link_text3,"
					. " amenity_airhockey,"
					. " amenity_alarmclock,"
					. " amenity_answeringmachine,"
					. " amenity_arcadegames,"
					. " amenity_billiards,"
					. " amenity_blender,"
					. " amenity_blurayplayer,"
					. " amenity_books,"
					. " amenity_casetteplayer,"
					. " amenity_cdplayer,"
					. " amenity_ceilingfan,"
					. " amenity_childshighchair,"
					. " amenity_coffeemaker,"
					. " amenity_communalpool,"
					. " amenity_computer,"
					. " amenity_cookware,"
					. " amenity_cookingrange,"
					. " amenity_deckfurniture,"
					. " amenity_dishwasher ,"
					. " amenity_dishes,"
					. " amenity_dvdplayer,"
					. " amenity_exercisefacilities,"
					. " amenity_foosball,"
					. " amenity_gametable,"
					. " amenity_games,"
					. " amenity_grill,"
					. " amenity_hairdryer,"
					. " amenity_icemaker,"
					. " amenity_internet,"
					. " amenity_ironandboard,"
					. " amenity_kidsgames,"
					. " amenity_linensprovided,"
					. " amenity_lobsterpot,"
					. " amenity_microwave,"
					. " amenity_minirefrigerator,"
					. " amenity_mp3radiodock,"
					. " amenity_outsideshower,"
					. " amenity_oven,"
					. " amenity_pinball,"
					. " amenity_pingpong,"
					. " amenity_privatepool,"
					. " amenity_radio,"
					. " amenity_refrigeratorfreezer,"
					. " amenity_sofabed,"
					. " amenity_stereo,"
					. " amenity_telephone,"
					. " amenity_television,"
					. " amenity_toaster,"
					. " amenity_toasteroven,"
					. " amenity_towelsprovided,"
					. " amenity_toys,"
					. " amenity_utensils,"
					. " amenity_vacuum,"
					. " amenity_vcr,"
					. " amenity_videogameconsole,"
					. " amenity_videogames,"
					. " amenity_washerdryer,"
					. " amenity_wifi,"
					. " feature_airconditioning,"
					. " feature_balcony,"
					. " feature_barbecuecharcoal,"
					. " feature_barbecuegas,"
					. " feature_boatslip,"
					. " feature_cablesatellitetv,"
					. " feature_clubhouse,"
					. " feature_coveredparking,"
					. " feature_deck,"
					. " feature_diningroom,"
					. " feature_elevator,"
					. " feature_familyroom,"
					. " feature_fullkitchen,"
					. " feature_garage,"
					. " feature_gasfireplace,"
					. " feature_gatedcommunity,"
					. " feature_wheelchairaccess,"
					. " feature_heated,"
					. " feature_heatedpool,"
					. " feature_hottubjacuzzi,"
					. " feature_internetaccess,"
					. " feature_kitchenette,"
					. " feature_livingroom,"
					. " feature_loft,"
					. " feature_onsitesecurity,"
					. " feature_patio,"
					. " feature_petsallowed,"
					. " feature_playroom,"
					. " feature_pool,"
					. " feature_porch,"
					. " feature_rooftopdeck,"
					. " feature_sauna,"
					. " feature_smokingpermitted,"
					. " feature_woodfireplace,"
					. " activity_antiquing,"
					. " activity_basketballcourt,"
					. " activity_beachcombing,"
					. " activity_bicycling,"
					. " activity_bikerentals,"
					. " activity_birdwatching,"
					. " activity_boatrentals,"
					. " activity_boating,"
					. " activity_botanicalgarden,"
					. " activity_canoe,"
					. " activity_churches,"
					. " activity_cinemas,"
					. " activity_bikesprovided,"
					. " activity_deepseafishing,"
					. " activity_fishing,"
					. " activity_fitnesscenter,"
					. " activity_golf,"
					. " activity_healthbeautyspa,"
					. " activity_hiking,"
					. " activity_horsebackriding,"
					. " activity_horseshoes,"
					. " activity_hotairballooning,"
					. " activity_iceskating,"
					. " activity_jetskiing,"
					. " activity_kayaking,"
					. " activity_livetheater,"
					. " activity_marina,"
					. " activity_miniaturegolf,"
					. " activity_mountainbiking,"
					. " activity_museums,"
					. " activity_paddleboating,"
					. " activity_paragliding,"
					. " activity_parasailing,"
					. " activity_playground,"
					. " activity_recreationcenter,"
					. " activity_restaurants,"
					. " activity_rollerblading,"
					. " activity_sailing,"
					. " activity_shelling,"
					. " activity_shopping,"
					. " activity_sightseeing,"
					. " activity_skiing,"
					. " activity_bayfishing,"
					. " activity_spa,"
					. " activity_surffishing,"
					. " activity_surfing,"
					. " activity_swimming,"
					. " activity_tennis,"
					. " activity_themeparks,"
					. " activity_walking,"
					. " activity_waterparks,"
					. " activity_waterskiing,"
					. " activity_watertubing,"
					. " activity_wildlifeviewing,"
					. " activity_zoo,"
					. " isppilisting,"
					. " deposit_amount,"
					. " tax_per_inquiry,"
					. " unique_email,"
					. " is_featured,"
					. " featured_date,"
					. " search_pos,"
					. " search_pos_date,"
					. " required_stay)"
					. " VALUES"
					. " ($this->account_id,"
					. " $this->image_id,"
					. " $this->thumb_id,"
					. " $this->promotion_id,"
					. " $this->location_1,"
					. " $this->location_2,"
					. " $this->location_3,"
					. " $this->location_4,"
					. " $this->location_5,"
					. " $this->renewal_date,"
					. " $this->discount_id,"
					. " $this->reminder,"
					. " '',"
					. " '',"
					. " NOW(),"
					. " NOW(),"
					. " $this->title,"
					. " $this->title,"
					. " $this->claim_disable,"
					. " $this->friendly_url,"
					. " $this->email,"
					. " $this->url,"
					. " $this->display_url,"
					. " $this->address,"
					. " $this->address2,"
					. " $this->zip_code,"
					. " $this->phone,"
					. " $this->fax,"
					. " $this->description,"
					. " $aux_seoDescription,"
					. " $this->long_description,"
					. " $this->video_snippet,"
					. " $this->video_description,"
					. " $this->keywords,"
					. " ".str_replace(" || ", ", ", $this->keywords).","
					. " $this->attachment_file,"
					. " $this->attachment_caption,"
					. " $this->features,"
					. " $this->price,"
					. " $this->facebook_page,"
					. " $this->status,"
					. " $this->level,"
					. " $this->hours_work,"
					. " $this->locations,"
					. " $this->listingtemplate_id,"
                    . " $this->custom_text0,"
					. " $this->custom_text1,"
					. " $this->custom_text2,"
					. " $this->custom_text3,"
					. " $this->custom_text4,"
					. " $this->custom_text5,"
					. " $this->custom_text6,"
					. " $this->custom_text7,"
					. " $this->custom_text8,"
					. " $this->custom_text9,"
					. " $this->custom_short_desc0,"
					. " $this->custom_short_desc1,"
					. " $this->custom_short_desc2,"
					. " $this->custom_short_desc3,"
					. " $this->custom_short_desc4,"
					. " $this->custom_short_desc5,"
					. " $this->custom_short_desc6,"
					. " $this->custom_short_desc7,"
					. " $this->custom_short_desc8,"
					. " $this->custom_short_desc9,"
					. " $this->custom_long_desc0,"
					. " $this->custom_long_desc1,"
					. " $this->custom_long_desc2,"
					. " $this->custom_long_desc3,"
					. " $this->custom_long_desc4,"
					. " $this->custom_long_desc5,"
					. " $this->custom_long_desc6,"
					. " $this->custom_long_desc7,"
					. " $this->custom_long_desc8,"
					. " $this->custom_long_desc9,"
                    . " $this->custom_checkbox0,"
					. " $this->custom_checkbox1,"
					. " $this->custom_checkbox2,"
					. " $this->custom_checkbox3,"
					. " $this->custom_checkbox4,"
					. " $this->custom_checkbox5,"
					. " $this->custom_checkbox6,"
					. " $this->custom_checkbox7,"
					. " $this->custom_checkbox8,"
					. " $this->custom_checkbox9,"
					. " $this->custom_dropdown0,"
					. " $this->custom_dropdown1,"
					. " $this->custom_dropdown2,"
					. " $this->custom_dropdown3,"
					. " $this->custom_dropdown4,"
					. " $this->custom_dropdown5,"
					. " $this->custom_dropdown6,"
					. " $this->custom_dropdown7,"
					. " $this->custom_dropdown8,"
					. " $this->custom_dropdown9,"
					. " $this->number_views,"
					. " $this->avg_review,"
					. " $this->latitude,"
					. " $this->longitude,"
					. " $this->map_zoom,"
					. " $this->package_id,"
					. " $this->package_price,"
					. " $this->backlink,"
					. " $this->backlink_url,"
					. " $this->clicktocall_number,"
					. " $this->clicktocall_extension,"
					. " $this->clicktocall_date,"
					. " $this->activation_date,"
					. " $this->bedroom,"
					. " $this->bedsize,"
					. " $this->sleeps,"
					. " $this->bathroom,"
					. " $this->property_type,"
					. " $this->view,"
					. " $this->distance_beach,"
					. " $this->development_name,"
					. " $this->airport_distance,"
					. " $this->airport_abbreviation,"
					. " $this->contact_fname,"
					. " $this->contact_lname,"
					. " $this->external_link1,"
					. " $this->external_link_text1,"
					. " $this->external_link2,"
					. " $this->external_link_text2,"
					. " $this->external_link3,"
					. " $this->external_link_text3,"
					. " $this->amenity_airhockey,"
					. " $this->amenity_alarmclock,"
					. " $this->amenity_answeringmachine,"
					. " $this->amenity_arcadegames,"
					. " $this->amenity_billiards,"
					. " $this->amenity_blender,"
					. " $this->amenity_blurayplayer,"
					. " $this->amenity_books,"
					. " $this->amenity_casetteplayer,"
					. " $this->amenity_cdplayer,"
					. " $this->amenity_ceilingfan,"
					. " $this->amenity_childshighchair,"
					. " $this->amenity_coffeemaker,"
					. " $this->amenity_communalpool,"
					. " $this->amenity_computer,"
					. " $this->amenity_cookware,"
					. " $this->amenity_cookingrange,"
					. " $this->amenity_deckfurniture,"
					. " $this->amenity_dishwasher,"
					. " $this->amenity_dishes,"
					. " $this->amenity_dvdplayer,"
					. " $this->amenity_exercisefacilities,"
					. " $this->amenity_foosball,"
					. " $this->amenity_gametable,"
					. " $this->amenity_games,"
					. " $this->amenity_grill,"
					. " $this->amenity_hairdryer,"
					. " $this->amenity_icemaker,"
					. " $this->amenity_internet,"
					. " $this->amenity_ironandboard,"
					. " $this->amenity_kidsgames,"
					. " $this->amenity_linensprovided,"
					. " $this->amenity_lobsterpot,"
					. " $this->amenity_microwave,"
					. " $this->amenity_minirefrigerator,"
					. " $this->amenity_mp3radiodock,"
					. " $this->amenity_outsideshower,"
					. " $this->amenity_oven,"
					. " $this->amenity_pinball,"
					. " $this->amenity_pingpong,"
					. " $this->amenity_privatepool,"
					. " $this->amenity_radio,"
					. " $this->amenity_refrigeratorfreezer,"
					. " $this->amenity_sofabed,"
					. " $this->amenity_stereo,"
					. " $this->amenity_telephone,"
					. " $this->amenity_television,"
					. " $this->amenity_toaster,"
					. " $this->amenity_toasteroven,"
					. " $this->amenity_towelsprovided,"
					. " $this->amenity_toys,"
					. " $this->amenity_utensils,"
					. " $this->amenity_vacuum,"
					. " $this->amenity_vcr,"
					. " $this->amenity_videogameconsole,"
					. " $this->amenity_videogames,"
					. " $this->amenity_washerdryer,"
					. " $this->amenity_wifi,"
					. " $this->feature_airconditioning,"
					. " $this->feature_balcony,"
					. " $this->feature_barbecuecharcoal,"
					. " $this->feature_barbecuegas,"
					. " $this->feature_boatslip,"
					. " $this->feature_cablesatellitetv,"
					. " $this->feature_clubhouse,"
					. " $this->feature_coveredparking,"
					. " $this->feature_deck,"
					. " $this->feature_diningroom,"
					. " $this->feature_elevator,"
					. " $this->feature_familyroom,"
					. " $this->feature_fullkitchen,"
					. " $this->feature_garage,"
					. " $this->feature_gasfireplace,"
					. " $this->feature_gatedcommunity,"
					. " $this->feature_wheelchairaccess,"
					. " $this->feature_heated,"
					. " $this->feature_heatedpool,"
					. " $this->feature_hottubjacuzzi,"
					. " $this->feature_internetaccess,"
					. " $this->feature_kitchenette,"
					. " $this->feature_livingroom,"
					. " $this->feature_loft,"
					. " $this->feature_onsitesecurity,"
					. " $this->feature_patio,"
					. " $this->feature_petsallowed,"
					. " $this->feature_playroom,"
					. " $this->feature_pool,"
					. " $this->feature_porch,"
					. " $this->feature_rooftopdeck,"
					. " $this->feature_sauna,"
					. " $this->feature_smokingpermitted,"
					. " $this->feature_woodfireplace,"
					. " $this->activity_antiquing,"
					. " $this->activity_basketballcourt,"
					. " $this->activity_beachcombing,"
					. " $this->activity_bicycling,"
					. " $this->activity_bikerentals,"
					. " $this->activity_birdwatching,"
					. " $this->activity_boatrentals,"
					. " $this->activity_boating,"
					. " $this->activity_botanicalgarden,"
					. " $this->activity_canoe,"
					. " $this->activity_churches,"
					. " $this->activity_cinemas,"
					. " $this->activity_bikesprovided,"
					. " $this->activity_deepseafishing,"
					. " $this->activity_fishing,"
					. " $this->activity_fitnesscenter,"
					. " $this->activity_golf,"
					. " $this->activity_healthbeautyspa,"
					. " $this->activity_hiking,"
					. " $this->activity_horsebackriding,"
					. " $this->activity_horseshoes,"
					. " $this->activity_hotairballooning,"
					. " $this->activity_iceskating,"
					. " $this->activity_jetskiing,"
					. " $this->activity_kayaking,"
					. " $this->activity_livetheater,"
					. " $this->activity_marina,"
					. " $this->activity_miniaturegolf,"
					. " $this->activity_mountainbiking,"
					. " $this->activity_museums,"
					. " $this->activity_paddleboating,"
					. " $this->activity_paragliding,"
					. " $this->activity_parasailing,"
					. " $this->activity_playground,"
					. " $this->activity_recreationcenter,"
					. " $this->activity_restaurants,"
					. " $this->activity_rollerblading,"
					. " $this->activity_sailing,"
					. " $this->activity_shelling,"
					. " $this->activity_shopping,"
					. " $this->activity_sightseeing,"
					. " $this->activity_skiing,"
					. " $this->activity_bayfishing,"
					. " $this->activity_spa,"
					. " $this->activity_surffishing,"
					. " $this->activity_surfing,"
					. " $this->activity_swimming,"
					. " $this->activity_tennis,"
					. " $this->activity_themeparks,"
					. " $this->activity_walking,"
					. " $this->activity_waterparks,"
					. " $this->activity_waterskiing,"
					. " $this->activity_watertubing,"
					. " $this->activity_wildlifeviewing,"
					. " $this->activity_zoo,"
					. " $this->isppilisting,"
					. " $this->deposit_amount,"
					. " $this->tax_per_inquiry,"
					. " $this->unique_email,"
					. " $this->is_featured,"
					. " $this->featured_date,"
					. " $this->search_pos,"
					. " $this->search_pos_date,"
					. " $this->required_stay)";
				$dbObj->query($sql);
				$this->id = mysql_insert_id($dbObj->link_id);

				if (sess_getAccountIdFromSession() || string_strpos($_SERVER["PHP_SELF"],"order_") !== false){
					activity_newActivity($aux_log_domain_id, $this->account_id, 0, "newitem", "listing", $this->title);
				}

				if ($this->status == "'P'"){
					activity_newToApproved($aux_log_domain_id, $this->id, "listing", $this->title);
				}

				domain_updateDashboard("number_listings","inc", 0, $aux_log_domain_id);

				/*
				 * Populate Listings to front
				 */
				unset($listingSummaryObj);
				$listingSummaryObj = new ListingSummary();
				/*
				 * Used to package
				 */
				$this->prepareToUse(); //prevent some fields to be saved with empty quotes
				if(is_numeric($this->domain_id)){
					$listingSummaryObj->setNumber("domain_id",$this->domain_id);
				}else{
					$listingSummaryObj->domain_id = SELECTED_DOMAIN_ID;
				}


				$listingSummaryObj->PopulateTable($this->id, "insert");

				//Reload the Listing object variables

				$sql = "SELECT * FROM Listing WHERE id = $this->id";
				$row = mysql_fetch_array($dbObj->query($sql));
				$this->makeFromRow($row);
				$this->prepareToSave();

				$this_status = $this->status;
				$this_id = $this->id;
				$this_status = str_replace("\"", "", $this_status);
				$this_status = str_replace("'", "", $this_status);
				$this_id = str_replace("\"", "", $this_id);
				$this_id = str_replace("'", "", $this_id);
				system_countActiveListingByCategory($this_id);

				/*
				 * Save to featured temp
				 */
				$this->SaveToFeaturedTemp();

				if ($aux_account != 0) {
					domain_SaveAccountInfoDomain($aux_account, $this);
				}

			}

            $this->prepareToUse();

            /**
             * Save listing_id on Promotion table
             */
            if($this->promotion_id != "0"){
                unset($promotionObj);
                $promotionObj = new Promotion($this->promotion_id);
                $promotionObj->setListingId($this);
            }
                        
			$this->setFullTextSearch();
		}
        
        
        function validate_form($form, $array=array(), &$error)
        {
            $errors=array();
            $elems[] = array('name'=>'account_id', 'label'=>config('params.LANG_LABEL_ACCOUNT'), 'type'=>'text', 'required'=>true, 'cont'=>'digit' );

			$ctr_url = (isset($array["display_url"])) ? true : false;

			
			//if (isset($array["listingtemplate_id"]) && config('params.CUSTOM_LISTINGTEMPLATE_FEATURE') == "on") {
//                
//                if (USING_THEME_TEMPLATE && THEME_TEMPLATE_ID && THEME_TEMPLATE_ID == $_POST["listingtemplate_id"]){
//                    $isThemeTemplate = true;
//                } else {
//                    $isThemeTemplate = false;
//                }
//                
//				$listingTemplateObj = new ListingTemplate($_POST["listingtemplate_id"]);
//				$templateFields = $listingTemplateObj->getListingTemplateFields("", $isThemeTemplate);
//
//				if ($templateFields) {
//					foreach ($templateFields as $each_field) {
//						if ($each_field["required"]=="y") {
//							$elems[] = array('name'=>$each_field["field"], 'label'=>(defined($each_field["label"]) ? constant($each_field["label"]) :$each_field["label"]), 'type'=>'text', 'required'=>true, 'len_max'=>'65535');
//						}
//					}
//				}
//			}
			

			$elems[] = array('name'=>'title',             'label'=>config('params.LANG_LABEL_NAME_OR_TITLE'),       'type'=>'text',   'required'=>true,     'len_max'=>'100'                   );
			$elems[] = array('name'=>'friendly_url',      'label'=>config('params.LANG_LABEL_PAGE_NAME'),           'type'=>'text',   'required'=>false,    'len_max'=>'255'                   );
			$elems[] = array('name'=>'description',       'label'=>config('params.LANG_LABEL_SUMMARY_DESCRIPTION'), 'type'=>'text',   'required'=>false,    'len_max'=>'255'                   );
			$elems[] = array('name'=>'renewal_date',      'label'=>config('params.LANG_LABEL_RENEWAL_DATE'),        'type'=>'text',   'required'=>false,    'len_max'=>'10',    'cont'=>'date' );
			//$elems[] = array('name'=>'url',               'label'=>config('params.LANG_LABEL_WEB_ADDRESS'),         'type'=>'text',   'required'=>$crt_url, 'len_max'=>'255'                   );
			$elems[] = array('name'=>'fax',               'label'=>config('params.LANG_LABEL_FAX'),                 'type'=>'text',   'required'=>false,    'len_max'=>'255'                   );
			$elems[] = array('name'=>'long_description',  'label'=>config('params.LANG_LABEL_LONG_DESCRIPTION'),    'type'=>'text',   'required'=>false,    'len_max'=>'65535'                 );
			$elems[] = array('name'=>'status',            'label'=>config('params.LANG_LABEL_STATUS'),              'type'=>'text',   'required'=>false,    'len_max'=>'1'                     );
			$elems[] = array('name'=>'level',             'label'=>config('params.LANG_LABEL_LEVEL'),               'type'=>'select', 'required'=>false,    'len_max'=>'2',    'cont'=>'digit' );
			$elems[] = array('name'=>'return_categories', 'label'=>config('params.LANG_LABEL_CATEGORY_PLURAL'),     'type'=>'text',   'required'=>true,                                        );
			
			if ( isset($orderListing) ) {
				$elems[] = array('name'=>'email',             'label'=>config('params.LANG_LABEL_EMAIL'),              	'type'=>'text',   'required'=>false,    	'len_max'=>'255'                     );
				$elems[] = array('name'=>'phone',             'label'=>config('params.LANG_LABEL_PHONE'),               'type'=>'text',   'required'=>false,    	'len_max'=>'255'                   );
				$elems[] = array('name'=>'address',           'label'=>config('params.LANG_LABEL_ADDRESS1'),      		'type'=>'text',   'required'=>false,    	'len_max'=>'255'                   );
				$elems[] = array('name'=>'zip_code',          'label'=>config('params.ZIPCODE_LABEL'),  'type'=>'text',   'required'=>false,    	'len_max'=>'255'                     );
				$elems[] = array('name'=>'location_1',        'label'=>config('params.LANG_LABEL_COUNTRY'),             'type'=>'select',   'required'=>false,    	'len_max'=>'255'                     );
				$elems[] = array('name'=>'location_3',        'label'=>config('params.LANG_LABEL_STATE'),              	'type'=>'select',   'required'=>false,    	'len_max'=>'255'                     );
				$elems[] = array('name'=>'location_4',        'label'=>config('params.LANG_LABEL_CITY'),              	'type'=>'select',   'required'=>false,    	'len_max'=>'255'                     );	
			} else {
				$elems[] = array('name'=>'email',             'label'=>config('params.LANG_LABEL_EMAIL'),              	'type'=>'text',   'required'=>true,    	'len_max'=>'255'                     );
				$elems[] = array('name'=>'phone',             'label'=>config('params.LANG_LABEL_PHONE'),               'type'=>'text',   'required'=>true,    	'len_max'=>'255'                   );
				$elems[] = array('name'=>'address',           'label'=>config('params.LANG_LABEL_ADDRESS1'),      		'type'=>'text',   'required'=>true,    	'len_max'=>'255'                   );
				$elems[] = array('name'=>'zip_code',          'label'=>config('params.ZIPCODE_LABEL'),  'type'=>'text',   'required'=>true,    	'len_max'=>'255'                     );
				#57: Label Changes on listing form
				$elems[] = array('name'=>'contact_fname',	  'label'=>config('params.LANG_LABEL_FIRST_NAME'),          'type'=>'text',   'required'=>true,    	'len_max'=>'255'                     );
				$elems[] = array('name'=>'contact_lname',	  'label'=>config('params.LANG_LABEL_LAST_NAME'),           'type'=>'text',   'required'=>true,    	'len_max'=>'255'                     );
				$elems[] = array('name'=>'location_1',        'label'=>config('params.LANG_LABEL_COUNTRY'),             'type'=>'select', 'required'=>true,    	'len_max'=>'255'                     );
				if(isset($array['new_location3_field']) && $array['new_location3_field'] == "" && isset($array['new_location3_friendly']) && $array['new_location3_friendly'] == "") {
					$elems[] = array('name'=>'location_3',        'label'=>config('params.LANG_LABEL_STATE'),    'type'=>'select',   'required'=>true,    	'len_max'=>'255'                     );
				}
				if(isset($array['new_location4_field']) && $array['new_location4_field'] == "" && isset($array['new_location4_friendly']) && $array['new_location4_friendly'] == "") {
					$elems[] = array('name'=>'location_4',        'label'=>config('params.LANG_LABEL_CITY'),     'type'=>'select',   'required'=>true,    	'len_max'=>'255'                     );
				}
			}
            $f = new FormValidator;
			$f->FormValidator($elems);
			$err = $f->validate($_POST);

			if ($err) {
				$errors[] = "<b>".config('params.LANG_MSG_FIELDS_CONTAIN_ERRORS')."</b>";
				$valid = $f->getValidElems();
				foreach ($valid as $field_title => $field_array) {
					foreach ($field_array as $field) if (!$field['validation']) $errors[] = "&#149;&nbsp;".$field['label'];
				}
			}

			$return_categories_array = isset($return_categories)?explode(",", $return_categories):array();
			$return_categories_array = array_unique($return_categories_array);

            if(count($return_categories_array) > config('params.LISTING_MAX_CATEGORY_ALLOWED')) $errors[] = "&#149;&nbsp;".config('params.LANG_MSG_MAX_OF_CATEGORIES_1')." ".config('params.LISTING_MAX_CATEGORY_ALLOWED')." ".config('params.LANG_MSG_MAX_OF_CATEGORIES_2');

			if (isset($friendly_url)) {
				$sql = "SELECT friendly_url FROM Listing WHERE friendly_url = ".db_formatString($friendly_url)."";
				if($id) $sql .= " AND id != $id ";
				$sql .= " LIMIT 1";
				$sqlFriendlyURLResult = Sql::getFriendlyUrl($sqlFriendlyURL);
                $rs = isset($sqlFriendlyURLResult[0]->friendly_url)?$sqlFriendlyURLResult[0]->friendly_url:0;
				if($rs> 0) $errors[] = "&#149;&nbsp;".config('params.LANG_MSG_FRIENDLY_URL_IN_USE');
				if(!preg_match(config('params.FRIENDLYURL_REGULAREXPRESSION'), $friendly_url)) $errors[] = "&#149;&nbsp;".config('params.LANG_MSG_PAGE_NAME_INVALID_CHARS');
			}
            
            if (isset($latitude)){
                if (!is_numeric($latitude) || $latitude < -90 || $latitude > 90){
                    $errors[] = "&#149;&nbsp;".config('params.LANG_LABEL_MAP_INVALID_LAT');
                }
            }
            
            if (isset($longitude)){
               if (!is_numeric($latitude) || $latitude < -90 || $latitude > 90){
                    $errors[] = "&#149;&nbsp;".config('params.LANG_LABEL_MAP_INVALID_LON');
                } 
            }

            if (isset($array_keywords)) {
                if (count($array_keywords) > config('params.MAX_KEYWORDS')) {
                    $errors[] = "&#149;&nbsp;".config('params.LANG_MSG_MAX_OF_KEYWORDS_ALLOWED_1')." ".config('params.MAX_KEYWORDS')." ".config('params.LANG_MSG_MAX_OF_KEYWORDS_ALLOWED_2');
                }
                $kwlarge = false;
                foreach ($array_keywords as $kw) {
                    if (strlen($kw) > 50) {
                        $kwlarge = true;
                    }
                }
                if (isset($kwlarge) && $kwlarge) {
                    $errors[] = "&#149;&nbsp;".config('params.LANG_MSG_PLEASE_INCLUDE_KEYWORDS');
                }
            }
                return $errors;
          
        }
        
 
    
    
    function getSearchPosSelect(Request $request)
    {
        $has_pos1 = Sql::getSelectLocation4("listing", "SELECT id FROM Listing WHERE search_pos = 1 AND location_4 = ".$request->location_4);
        $has_pos2 = Sql::getSelectLocation4("listing", "SELECT id FROM Listing WHERE search_pos = 2 AND location_4 = ".$request->location_4);
        $has_pos3 = Sql::getSelectLocation4("listing", "SELECT id FROM Listing WHERE search_pos = 3 AND location_4 = ".$request->location_4);
        $has_pos4 = Sql::getSelectLocation4("listing", "SELECT id FROM Listing WHERE search_pos = 4 AND location_4 = ".$request->location_4);
        $has_pos5 = Sql::getSelectLocation4("listing", "SELECT id FROM Listing WHERE search_pos = 5 AND location_4 = ".$request->location_4);
           
            $return = "<th>";
            $return .= config('params.LANG_LABEL_SEARCH_POS').":";
            $return .= "</th>";
            $return .= "<td colspan=\"2\">";
            $return .= "<select style=\"width: 50px\" name=\"search_pos\">";
            if(count($has_pos1) > 1 && count($has_pos2) > 1 && count($has_pos3) > 1 && count($has_pos4) > 1 && count($has_pos5) > 1) {
                $return .= "<option value=\"6\">No pos</option>";
            }
            $return .= "<option value=\"6\" " . ($search_pos == 6 ? "selected" : "").">0</option>";
            if(count($has_pos1) == 0) {
                $return .= "<option value=\"1\" ".($search_pos == 1 ? "selected" : "").">1</option>";
            }
            if(count($has_pos2) == 0) {
                $return .= "<option value=\"2\" ".($search_pos == 2 ? "selected" : "").">2</option>";
            }
            if(count($has_pos3) == 0) {
                $return .= "<option value=\"3\" ".($search_pos == 3 ? "selected" : "").">3</option>";
            }
            if(count($has_pos4) == 0) {
                $return .= "<option value=\"4\" ".($search_pos == 4 ? "selected" : "").">4</option>";
            }
            if(count($has_pos5) == 0) {
                $return .= "<option value=\"5\" ".($search_pos == 5 ? "selected" : "").">5</option>";
            }
            $return .= "</select>";
            $return .= "</td>";


        
        print_r($return);
    }
    
    
    function sponser_getcity(Request $request)
    {
       $id =  $request->id;
    	$level = $request->level;
    	$childLevel = $request->childLevel;
    	$type = $request->type;
        $loc4Id = $request->loc4Id;
       
    	if ($type == 'byId') {
    		if ($childLevel) {
    			$objLocationLabel = "Location".$childLevel;
    			${"Location"+$childLevel}= new Location4;
    			${"Location"+$childLevel}->SetString("location_".$level, $id);
    			$retrieved_locations = ${"Location"+$childLevel}->retrieveLocationByLocation($level);
    			if (!empty($retrieved_locations)) {
    				$return = "<option id=\"l_location_".$childLevel."\" value=\"\"></option>";
    				foreach ($retrieved_locations as $each_location) {
    					$location_id = $each_location->id;
    					$location_name = $each_location->name;
                        if(isset($loc4Id) && $loc4Id>0 && $location_id==$loc4Id)
                        {
                            $selected = 'SELECTED';
                        }
                        else
                        {
                            $selected = '';
                        }
    					$return .= "<option ".$selected." id=\"option_L".$childLevel."_ID".$location_id."\" value=\"".$location_id."\">".trim($location_name)."</option>";
    				}
                    return $return;
    			} else $return = "empty";
    		}
    		else $return = "empty";
    	} 
    }
    
    
    function listing_listinglevel(Request $request)
    {
        
        $userAccountId = Session::get('SESS_ACCOUNT_ID');
        if(!$userAccountId)
        {
            return redirect(url());
        }
        
      
        $profileSp = Functions::SponsersProfile();
        $ownerName= '';
        if(!empty($profileSp))
        {
            $ownerName = $profileSp[0]->first_name.' '.$profileSp[0]->last_name.', ';
        }
        $meta_data = Sql::sponsersMeta($userAccountId);
        $headerTitle = $ownerName.'Welcome to the Sponsor Section -'.$meta_data[0]->value;
        $headerAuthor = $meta_data[1]->value;
        $headerDescription = $meta_data[2]->value;
        $headerKeywords = $meta_data[3]->value;
        $footer_text = Sql::footerText();
        $accountId = $userAccountId;
        $spListinglevel = Sql::getListingLevel();

        
        return view('front.sponsers_listing_level',compact('profileSp','spListinglevel','headerTitle','headerAuthor','headerDescription','headerKeywords','footer_text','accountId'));

    }

    function sponser_listing_rate(Request $request)
    {
        //echo $request->id;
       	$userAccountId = Session::get('SESS_ACCOUNT_ID');
       	$listing_id =  $request->id;
       	$message =  $request->message;
        $profileSp = Functions::SponsersProfile();
      	$ownerName= '';
        if(!empty($profileSp))
        {
           $ownerName = $profileSp[0]->first_name.' '.$profileSp[0]->last_name.', ';
       }
       $meta_data = Sql::sponsersMeta($userAccountId);
       $headerTitle = $ownerName.'Welcome to the Sponsor Section -'.$meta_data[0]->value;
      $headerAuthor = $meta_data[1]->value;
       $headerDescription = $meta_data[2]->value;
       $headerKeywords = $meta_data[3]->value;
       $footer_text = Sql::footerText();

       $spRate=Sql::SponserListingRate($request->id);
       $spRateInfo=Sql::SponserRateInformation($request->id);
       
       $spListingData = Sql::SponserListing($userAccountId);
        return view('front.sponsers_rate',compact('spListingData','spRate','spRateInfo','message','listing_id','headerTitle','headerAuthor','headerDescription','headerKeywords','footer_text'));
 	}
 


    function sponsor_storeRate(Request $request)
    {
	    if($request->_token==csrf_token())
	    {
	    	$listing_id=$request->listing_id;

	    	$rateInfo = new RateInformation();
	    	$rateInfo->listing_id=$request->listing_id;
	    	$rateInfo->checkin=$request->checkin;
	    	$rateInfo->checkout=$request->checkout;
	    	$rateInfo->payment_types=$request->payment_types;
	    	$rateInfo->payment_terms=$request->payment_terms;
	    	$rateInfo->cancellation_policy=$request->cancellation_policy;
	    	$rateInfo->security_deposit=$request->security_deposit;
	    	$rateInfo->notes=$request->notes;


            $rateInfo->Save();


	    	if($request->from_date && $request->to_date)	
	    	{
	    		$rate =  new Rate();
	    		$from_date=$rate->setDate('from_date',$request->from_date);
	    		$to_date=$rate->setDate('to_date',$request->to_date);
	    		
		    	$rate->listing_id=$request->listing_id;
		    	$rate->from_date=$from_date;
		    	$rate->to_date=$to_date;
		    	$rate->day=($request->day)?$request->day:0.00;
		    	$rate->week=($request->week)?$request->week:0.00;
		    	$rate->month=($request->month)?$request->month:0.00;
		    	$rate->season=($request->season)?$request->season:0.00;
		    	//$rate->id=$request->rate_id;

		    	//d($rate->month,1);die;
		    	$rate->Save();

	    	}
	    	$message=1;
	    	return redirect('sponsors/rate/'.$listing_id.'/'.$message);

	    }

    }

    
    function sponsor_DelPage($id,$listing_id)
    {
    	 $userAccountId = Session::get('SESS_ACCOUNT_ID');
        $profileSp = Functions::SponsersProfile();
      $ownerName= '';
        if(!empty($profileSp))
        {
           $ownerName = $profileSp[0]->first_name.' '.$profileSp[0]->last_name.', ';
       }
       $meta_data = Sql::sponsersMeta($userAccountId);
       $headerTitle = $ownerName.'Welcome to the Sponsor Section -'.$meta_data[0]->value;
      $headerAuthor = $meta_data[1]->value;
       $headerDescription = $meta_data[2]->value;
       $headerKeywords = $meta_data[3]->value;
       $footer_text = Sql::footerText();
       
       $spListingData = Sql::SponserListing($userAccountId);
        return view('front.delete_rate',compact('spListingData','id','listing_id','headerTitle','headerAuthor','headerDescription','headerKeywords','footer_text'));
    	
    }

    function sponsor_DelRate(Request $request)
    {
    	if($request->_token==csrf_token())
    	{


    	$rate = new Rate();
    	
		$rate->Delete($request->rate_id);


		return redirect('sponsors/rate/'.$request->listing_id);
		}
    }

    function ajax_listing_rate(Request $request)
 	{
 		//d('Here',1);die;
    if($request->_token==csrf_token())
     { 
     	$userAccountId = Session::get('SESS_ACCOUNT_ID');
     	$value=$request->value;
     	$action=$request->action;
     	$field=$request->field;
     	$id=$request->id;
     	$valid_value='';

        if (is_numeric($value) && $value >= 0) {
            $valid_value = true;
        } else {
            $valid_date = Functions::validate_date($value);

            if (!$valid_date) {
                $valid_value = false;
                $error = "Please, insert a valid value.";                    
            }

        }

        //Update Rate
        if ($action == "update" && $valid_value) {
            $sql = "UPDATE Rate SET ".$field." = ".$value." WHERE id = ".$id;
            $res = Sql::insertSql($sql);
            if ($res) {
                $return["value"] = $value;
                $return["money_value"] = Functions::format_money($value,true);
            }


         //error handling
        } else if ($action == "update" && $valid_date) {
         
            $rate = new Rate();
            
            $date=$rate->setDate($field,$value);

           	$sql = "UPDATE Rate SET ".$field." = '".$date."' WHERE id = ".$id;

            Sql::insertSql($sql);

            
            
            $return["value"] = $value;
            $return[$field] = $value;

        } else if ($error) {
            $return["error_msg"] = $error;
        }

        $return = json_encode($return);
        print_r($return);
     }


    }

 function sponser_listing_specials(Request $request)
 {
        $userAccountId = Session::get('SESS_ACCOUNT_ID');
        $profileSp = Functions::SponsersProfile();
      	$ownerName= '';
      	$listing_id =  $request->id;
        if(!empty($profileSp))
        {
           $ownerName = $profileSp[0]->first_name.' '.$profileSp[0]->last_name.', ';
       }
       $meta_data = Sql::sponsersMeta($userAccountId);
       $headerTitle = $ownerName.'Welcome to the Sponsor Section -'.$meta_data[0]->value;
       $headerAuthor = $meta_data[1]->value;
       $headerDescription = $meta_data[2]->value;
       $headerKeywords = $meta_data[3]->value;
       $footer_text = Sql::footerText();
       $editorChoices = Sql::EditorChoice(1);
       
       
       $spListingData = Sql::SponserListing($userAccountId);
        return view('front.sponsers_specials',compact('spListingData','editorChoices','listing_id','headerTitle','headerAuthor','headerDescription','headerKeywords','footer_text'));

 }

 function sponsor_storeSpecial(Request $request)
 {
 	if($request->_token==csrf_token())
	    {
	    	$listing_id=$request->id;
	    	$arr_cho=count($request->choice);
	    	

	    	$arr_des=count($request->description);
	    	

	    	if($arr_des>0)
	    	{
	    	foreach ($request->description as $lc_id => $description) {
	    	
	    	$listchoice = new ListingChoice();
	    	$listchoice->ListingChoice("", "", $lc_id);
	    	$listchoice->setNumber("id", $lc_id);
	
	    	$listchoice->setString("description", $description);
           //d($description,1);die;
            $listchoice->Save();
        	}
	    	}
	    	if($arr_cho>0)
	    	{
	    	foreach ($request->choice as $value) {
	    	
	    	$listchoice = new ListingChoice($value, $listing_id);
	    	$listchoice->ListingChoice($value, $listing_id);

	    	$listing =Sql::SponserListingId($listing_id);
	    	
	    	$listchoice->setNumber("editor_choice_id", $value);
			$listchoice->setNumber("listing_id", $listing_id);
			$listchoice->setNumber("account_id", $listing[0]->account_id);

            $listchoice->Save();

	    	
     
	    	}
	    	
        	
	    	}
	    	       $message=1;



	    	return redirect('sponsors/badges/'.$listing_id)->with('message',$message);

	    }
 }
    

	function sponser_preview(Request $request)
	 {
	 		$resourc_id = $request->id;
	        
	        if(is_numeric($resourc_id) && $resourc_id>0)
	        {
	            $footer_text = Sql::footerText();
	        
	            $params['type'] = 'Home Page';
	            $meta_data = Sql::HomePageSummerSale($params);
	            
	            $header_author = Sql::HomePageHeaderAuthor();
	            
	            $header_searching_tag = Sql::HomePageHeaderSearchingTag();
	            $featured_images = Sql::getHomePageFeaturedRental();
	            
	            $listingData = Sql::getListingDetail($resourc_id);
	            $account_id = $listingData[0]['account_id'];
	            if(isset($account_id) && !empty($account_id))
	            {
	                $account_info = MainSql::AccountInfo($account_id);
	            }
	            else
	            {
	                $account_info=array();
	            }
	            
	            return view('front.sponsers_preview',compact('listingData','account_info','footer_text','meta_data','header_author','header_searching_tag'));

	        }
	        else
	        {
	            hearder('Location:'.url());
	            return true;
	        }
	 }


	   function sponser_listing_availability(Request $request)
 		{

 			
 		$userAccountId = Session::get('SESS_ACCOUNT_ID');
        $profileSp = Functions::SponsersProfile();
      	$ownerName= '';
      	$listing_id=$request->id;
        $availability_id=$request->availability_id?$request->availability_id:'';


        if(!empty($profileSp))
        {
           $ownerName = $profileSp[0]->first_name.' '.$profileSp[0]->last_name.', ';
       }
       $meta_data = Sql::sponsersMeta($userAccountId);
       $headerTitle = $ownerName.'Welcome to the Sponsor Section -'.$meta_data[0]->value;
       $headerAuthor = $meta_data[1]->value;
       $headerDescription = $meta_data[2]->value;
       $headerKeywords = $meta_data[3]->value;
       $footer_text = Sql::footerText();
       $spListingData = Sql::SponserListing($userAccountId);

       $table_availability=Sql::system_GetListingAvailabilityByListingId($listing_id);
              	

       if($availability_id>0)
        {

        	$spAvailabeId=Sql::system_GetListingAvailabilityById($availability_id);
        	 
        	 	return view('front.sponsers_availability',compact('headerTitle','spAvailabeId','availability_id','table_availability','spListingData','listing_id','headerAuthor','headerDescription','headerKeywords','footer_text'));
        
        }
        else
        {

            return view('front.sponsers_availability',compact('headerTitle','table_availability','spListingData','listing_id','headerAuthor','headerDescription','headerKeywords','footer_text'));

        }


        

             

	 }

	 function sponsor_storeAvailability(Request $request)
 	 {
 	 	if($request->_token==csrf_token())
	    {
	    	$start_date=(($request->start_date!="")?Functions::import_formatDate($request->start_date):'');
	    	$end_date=(($request->end_date!="")?Functions::import_formatDate($request->end_date):'');
	    	$start_date_period=$request->start_date_period;
	    	$end_date_period=$request->end_date_period;
	    	$customer_name=$request->customer_name;
	    	$listing_id=$request->id;
	    	$availability_id=$request->availability_id;
	    	$delete_availability=$request->delete_availability?$request->delete_availability:'';

        	 if($delete_availability==1)
        	 {
        	 	$userAccountId = Session::get('SESS_ACCOUNT_ID');
		        $profileSp = Functions::SponsersProfile();
		      	$ownerName= '';
		      	$listing_id=$request->id;
		        $availability_id=$request->availability_id?$request->availability_id:'';


		       if(!empty($profileSp))
		       {
		           $ownerName = $profileSp[0]->first_name.' '.$profileSp[0]->last_name.', ';
		       }
		       $meta_data = Sql::sponsersMeta($userAccountId);
		       $headerTitle = $ownerName.'Welcome to the Sponsor Section -'.$meta_data[0]->value;
		       $headerAuthor = $meta_data[1]->value;
		       $headerDescription = $meta_data[2]->value;
		       $headerKeywords = $meta_data[3]->value;
		       $footer_text = Sql::footerText();
		       $spListingData = Sql::SponserListing($userAccountId);

				return view('front.sponsers_availability',compact('headerTitle','start_date','start_date_period','end_date','end_date_period','delete_availability','availability_id','spListingData','listing_id','headerAuthor','headerDescription','headerKeywords','footer_text'));
        	 }

        	 elseif($delete_availability==2)
        	 {
        	 	$del_availability=Sql::system_DeleteAvailabilityById($availability_id);
        	 	 $message_listing_availability_correct = 'Listing successfully updated!';
             
           return redirect('sponsors/availability/'.$listing_id)->with('message',$message_listing_availability_correct);

        	 }
        	 else
        	 {

	    	if(!Sql::system_CheckListingAvailabilityBooking($listing_id,$start_date,$start_date_period,$end_date,$end_date_period,$availability_id))
	    	{

	    	if($start_date!="" && $end_date!="")	
	    		{
	    			$save_availability=Sql::system_SaveListingAvailabilityBooking($listing_id,$start_date,$start_date_period,$end_date,$end_date_period,$customer_name,$availability_id);

             $message_listing_availability_correct = 'Listing successfully updated!';
                 return redirect('sponsors/availability/'.$listing_id)->with('message',$message_listing_availability_correct);
	    		}
	    		else
	    		{
	    			$message_listing_availability = 'Kindly Select Check in and Check out date';
	    			return redirect('sponsors/availability/'.$listing_id)->with('message_listing_availability',$message_listing_availability);;
	    		}

              
	    	}	
	    	else
	    	{
	    		
	    		$message_listing_availability = 'Date already booked!';
                unset($message_listing_availability_correct);
                return redirect('sponsors/availability/'.$listing_id)->with('message_listing_availability',$message_listing_availability);
	    		
	    	}
	    	

          
        	 }
	    	
	    }

 	 }


 	 function sponsor_getYear(Request $request)
 	 {
 	 	if($request->_token==csrf_token())
	    {
	    	$listing_id=$request->id;
	    	$spyear=$request->year?$request->year:'';



	    	return redirect('sponsors/availability/'.$listing_id)->with('year',$spyear);
	    }

 	 }
   
    

	function listing_profile(Request $request)
	 {
			$userAccountId = Session::get('SESS_ACCOUNT_ID');
			$profileSp = Functions::SponsersProfile();
			$acc_id=Functions::sess_getAccountIdFromSession();
			$image_id=$profileSp[0]->image_id;

			$ownerName= '';
			if(!empty($profileSp))
			{
			   $ownerName = $profileSp[0]->first_name.' '.$profileSp[0]->last_name.', ';
		   }
		   $meta_data = Sql::sponsersMeta($userAccountId);
		   $headerTitle = $ownerName.'Welcome to the Sponsor Section -'.$meta_data[0]->value;
		   $headerAuthor = $meta_data[1]->value;
		   $headerDescription = $meta_data[2]->value;
		   $headerKeywords = $meta_data[3]->value;
		   $footer_text = Sql::footerText();
		  	$account=new Account();
		  	$account->Account($acc_id);
		  	if ($image_id) {
            
            $imageObj = new Image();
            $imageObj->Image($image_id, true);
            if ($imageObj->imageExists()) {
            	 			$file = $imageObj->getPath(true);

        	$imgTag = "<img src=\"".$file."\" width=\"".config('params.PROFILE_MEMBERS_IMAGE_WIDTH')."\" height=\"".config('params.PROFILE_MEMBERS_IMAGE_HEIGHT')."\"  style=\"display:inline;\"/>";
            } else {
                $imgTag = "<img src=\"".url()."/images/profile_noimage.gif\" width=\"".config('params.PROFILE_MEMBERS_IMAGE_WIDTH')."\" height=\"".config('params.PROFILE_MEMBERS_IMAGE_HEIGHT')."\" alt=\"No Image\" />";
            }
        } else {
            $imgTag = "<img src=\"".url()."/images/profile_noimage.gif\" width=\"".config('params.PROFILE_MEMBERS_IMAGE_WIDTH')."\" height=\"".config('params.PROFILE_MEMBERS_IMAGE_HEIGHT')."\" alt=\"No Image\" />";
        }

		   
		   
		   $spListingData = Sql::SponserListing($userAccountId);
	return view('front.sponsers_profile',compact('spListingData','profileSp','account','headerTitle','headerAuthor','headerDescription','headerKeywords','footer_text','imgTag'));

	 }
	 
	function listing_profile_edit(Request $request)
	 {
			$userAccountId = Session::get('SESS_ACCOUNT_ID');
			$profileSp = Functions::SponsersProfile();
			$ownerName= '';
			$image_id=$profileSp[0]->image_id;
			if(!empty($profileSp))
			{
			   $ownerName = $profileSp[0]->first_name.' '.$profileSp[0]->last_name.', ';
		   }
		   $meta_data = Sql::sponsersMeta($userAccountId);
		   $headerTitle = $ownerName.'Welcome to the Sponsor Section -'.$meta_data[0]->value;
		   $headerAuthor = $meta_data[1]->value;
		   $headerDescription = $meta_data[2]->value;
		   $headerKeywords = $meta_data[3]->value;
		   $footer_text = Sql::footerText();
			Session::put('random_key', uniqid(($userAccountId).strtotime(date('Y-m-d H:i:s'))));
			$domain_url=url('profile');

			if ($image_id) {
            $imageObj = new Image();
            $imageObj->Image($image_id, true);
            if ($image_id) {
            	$file = $imageObj->getPath(true);
        	$imgTag = "<img src=\"".$file."\" width=\"".config('params.PROFILE_MEMBERS_IMAGE_WIDTH')."\" height=\"".config('params.PROFILE_MEMBERS_IMAGE_HEIGHT')."\"  style=\"display:inline;\"/>";

            } else {
                $imgTag = "<img src=\"".url()."/images/profile_noimage.gif\" width=\"".config('params.PROFILE_MEMBERS_IMAGE_WIDTH')."\" height=\"".config('params.PROFILE_MEMBERS_IMAGE_HEIGHT')."\" alt=\"No Image\" />";
            }


        }

         else {
            $imgTag = "<img src=\"".url()."/images/profile_noimage.gif\" width=\"".config('params.PROFILE_MEMBERS_IMAGE_WIDTH')."\" height=\"".config('params.PROFILE_MEMBERS_IMAGE_HEIGHT')."\" alt=\"No Image\" />";
        }
		   
		   $spListingData = Sql::SponserListing($userAccountId);
			return view('front.profile_edit',compact('spListingData','profileSp','domain_url','userAccountId','headerTitle','headerAuthor','headerDescription','headerKeywords','footer_text','imgTag','image_id'));

	 }
	 
	 
	 function sponsor_help(Request $request)
 {
        $userAccountId = Session::get('SESS_ACCOUNT_ID');
        $acctId = Functions::sess_getAccountIdFromSession();
        $profileSp = Functions::SponsersProfile();
      	$ownerName= '';
        if(!empty($profileSp))
        {
           $ownerName = $profileSp[0]->first_name.' '.$profileSp[0]->last_name.', ';
       }
       $meta_data = Sql::sponsersMeta($userAccountId);
       $headerTitle = $ownerName.'Welcome to the Sponsor Section -'.$meta_data[0]->value;
       $headerAuthor = $meta_data[1]->value;
       $headerDescription = $meta_data[2]->value;
       $headerKeywords = $meta_data[3]->value;
       $footer_text = Sql::footerText();
       
       $contactObj = new Contact();
      	$contactObj->Contact($acctId);
		$name  = $contactObj->getString("first_name")." ".$contactObj->getString("last_name");
		$email   = $contactObj->getString("email");

       
       $spListingData = Sql::SponserListing($userAccountId);
        return view('front.sponsers_help',compact('spListingData','headerTitle','headerAuthor','headerDescription','headerKeywords','footer_text','name','email'));

 }

 function sponsor_help_store(Request $request)
 {
        if($request->_token==csrf_token())
	    {
	    	$validate_help = Functions::validate_form("help",$request->all() , $message_help);

	    	if ($validate_help) {

			$text = $request->text;
			$name=$request->name;
			$email=$request->email;
			$text = str_replace("\r\n", "\n", $text);
			$text = str_replace("\n", "\r\n", $text);
            
            //setting_get("sitemgr_support_email", $sitemgr_support_email);
			//$sitemgr_support_emails = explode(",", $sitemgr_support_email);
            

			//$emailSubject = "[".EDIRECTORY_TITLE."] ".system_showText(LANG_NOTIFY_MEMBERSHELP);
            
           $sitemgr_msg = config('params.LANG_LABEL_SITE_MANAGER').",<br /><br />".config('params.LANG_NOTIFY_MEMBERSHELP_1')."<br ><br />".config('params.LANG_NOTIFY_MEMBERSHELP_2').": ".$name."<br /><br />"."".config('params.LANG_NOTIFY_MEMBERSHELP_3').": ".$email."<br/ ><br />".config('params.LANG_NOTIFY_MEMBERSHELP_4').": <br /><br />".nl2br($text)."";
            
            //system_notifySitemgr($sitemgr_support_emails, $emailSubject, $sitemgr_msg);
    $emailSubject = "Help Request";

           Mail::send('email.help',
    array(
            'sitemgr_msg' => $sitemgr_msg,
            'emailSubject' => $emailSubject,
          	 'email' => $email,
        ), function($message)
    {

     //$data = Input::all();
	 $from=$request->email;
     $emailSubject = "Help Request";

        $message->from($from);
        $message->to('amoos@golpik.com')->cc($from)->bcc($from)->subject($emailSubject);
    });

            //Functions::sendEmail('amoos@golpik.com',$emailSubject,$sitemgr_msg,'');
		
            $success = true;
            $message_help ='Your e-mail has been sent. Thank you.';

		}



	    	 return redirect('sponsors/help')->with('message', $message_help);

	    }		
 }
 
 function sponsor_faq(Request $request)
 {
        $userAccountId = Session::get('SESS_ACCOUNT_ID');
        $profileSp = Functions::SponsersProfile();
      	$ownerName= '';
        if(!empty($profileSp))
        {
           $ownerName = $profileSp[0]->first_name.' '.$profileSp[0]->last_name.', ';
       }
       $meta_data = Sql::sponsersMeta($userAccountId);
       $headerTitle = $ownerName.'Welcome to the Sponsor Section -'.$meta_data[0]->value;
       $headerAuthor = $meta_data[1]->value;
       $headerDescription = $meta_data[2]->value;
       $headerKeywords = $meta_data[3]->value;
       $footer_text = Sql::footerText();
       
       
       $spListingData = Sql::SponserListing($userAccountId);
        return view('front.sponsers_faq',compact('spListingData','headerTitle','headerAuthor','headerDescription','headerKeywords','footer_text'));

 }
 


 function sponsor_account(Request $request)
 {
        $userAccountId = Session::get('SESS_ACCOUNT_ID');
        $profileSp = Functions::SponsersProfile();
        $account_id=Functions::sess_getAccountIdFromSession();
      	$ownerName= '';
        if(!empty($profileSp))
        {
           $ownerName = $profileSp[0]->first_name.' '.$profileSp[0]->last_name.', ';
       }
       $meta_data = Sql::sponsersMeta($userAccountId);
       $headerTitle = $ownerName.'Welcome to the Sponsor Section -'.$meta_data[0]->value;
       $headerAuthor = $meta_data[1]->value;
       $headerDescription = $meta_data[2]->value;
       $headerKeywords = $meta_data[3]->value;
       $footer_text = Sql::footerText();

       $account=new Account();
       $account->Account($account_id);

       $contact=new Contact();
       $contact->Contact($account_id);

       $spListingData = Sql::SponserListing($userAccountId);
        return view('front.sponsers_account',compact('spListingData','account_id','account','contact','headerTitle','headerAuthor','headerDescription','headerKeywords','footer_text'));

 }

 function sponsor_update_account(Request $request)
 {
 	if($request->_token==csrf_token())
	{	
		//="on";	
		  $account_id=Functions::sess_getAccountIdFromSession();
	 	  $account=new Account();
       	  $account->Account($account_id);
       	  $message= "";
       	  $message_style = "errorMessage";



       	  $request->notify_traffic_listing= ($request->notify_traffic_listing ? 'y' : 'n');
          $request->notify_sms= ($request->notify_sms ? 'y' : 'n');


       	  if ((Functions::string_strlen($request->password)) || (Functions::string_strlen($request->retype_password))) {
				$validate_membercurrentpassword = Functions::validate_memberCurrentPassword($request->current_password, $account_id, $message_member);

			} else {
				$validate_membercurrentpassword = true;
			}

	$validate_account = Functions::validate_MEMBERS_account('n',$request->username,$request->password,$request->retype_password, $message_account, $account_id);

	if ($validate_membercurrentpassword > 0 && $validate_account > 0) {//  && $validate_contact

		 $lastNewsletter = $account->getString("newsletter");
			if ($account->getString("foreignaccount") == "y") {
					$account->setString("foreignaccount_done", "y");
					$account->save();
		}



		  $notifyUser = false;
		  if ($request->password) {
                    $notifyUser = true;
					$account->setString("password", $request->password);
					$account->updatePassword();
		  }

		 if ($request->username) {
                    if ($account->getString("username") != $request->username) {
                        $notifyUser = true;
                    }
             $account->setString("username", $request->username);
      	 }  

      	$account->setString("notify_traffic_listing",$request->notify_traffic_listing);
		$account->setString("notify_sms",$request->notify_sms);
		$account->setString("publish_contact", $request->publish_contact); 

		if ($request->newsletter) {
                    $actualNewsletter = "y";
                } else {
                    $actualNewsletter = "n";
                }
                
         $account->setString("newsletter", $actualNewsletter);
                
		 $account->Save();

		$contact = new Contact();
       	$contact->Contact($account_id);
       	$contact->setString("first_name",$request->first_name);
		$contact->setString("last_name",$request->last_name);
		$contact->setString("company", $request->company);
		$contact->setString("address", $request->address);
		$contact->setString("address2", $request->address2);
		$contact->setString("city", $request->city);
		$contact->setString("state", $request->state);
		$contact->setString("zip", $request->zip);
		$contact->setString("phone", $request->phone);
		$contact->setString("fax", $request->fax);
		//$contact->setString("fax", $request->fax);
		$contact->Save();

		 $message = "Account successfully updated!";
		 $message_style= "successMessage";

		 
		}
		
		else {

		if ((Functions::string_strlen(trim($validate_membercurrentpassword)) > 0) || (Functions::string_strlen(trim($validate_account)) > 0) ) {
		if (Functions::string_strlen(trim(isset($message_account))) > 0) { 
               $message.=($message_account)?$message_account:''."<br />";
               $message_style = "errorMessage";
            if (Functions::string_strlen(trim(isset($message_member))) > 0) { 
                     $message.="<br />";
              } 
          }
          if (Functions::string_strlen(trim(isset($message_member))) > 0) { 
                     $message.=($message_member)?$message_member:'';
                    $message_style = "errorMessage";


           } 

          
          }
          else
          {
          	$message = "";
			$message_style = "";
          }

				
		}

         
   
		 return redirect('sponsors/account')->with('message',$message)->with('message_style', $message_style);

	
		
	}
 }
 function sponser_search_username(Request $request)
 {


 	header("Content-Type: text/html; charset= UTF-8", TRUE);
    header("Accept-Encoding: gzip, deflate");
    header("Expires: Sat, 01 Jan 2000 00:00:00 GMT");
    header("Cache-Control: no-store, no-cache, must-revalidate");
    header("Cache-Control: post-check=0, pre-check", FALSE);
    header("Pragma: no-cache");
    
    $currentAcc = $request->current_acc;
	
    $skipValidation = false;
	if ($request->username == 'erro') {
		$error = "&#149;&nbsp; Special characters are not allowed for e-mail.";
	} else {
        /*if ($request->username == LANG_LABEL_USERNAME) { //IE 7/8/9 fix with placeholder plugin
            $checkIE = is_ie(false, $ieVersion);
            if ($checkIE && $ieVersion < 10) {
                $_GET['username'] = "";
                $skipValidation = true;
            }
        }*/
        if (!$skipValidation) {
            $error = Functions::validate_username($request->username);
        }
	}
	if ($error) {
		$error = str_replace("&#149;&nbsp;", "", $error);
		echo "<span class=\"UsernameRegistered\">".$error."</span>";
	} else {
	
		$input = Functions::string_strtolower(trim($request->username));
		$option = Functions::string_strtolower(trim($request->option));
		$whereStr = $input;
		$checkUsername = true;
		if ($input){
			if ($option == 'members') {

				$sql = "SELECT * FROM Account WHERE username = '$request->username'";
				$account_exists = Sql::fetchMain($sql);

				if(count($account_exists)>0)
				{
					$accExistsID = $account_exists[0]->id;
					if ($accExistsID) {
						$checkUsername = false;
					}
				}
                

			}

			if ($checkUsername) {
				echo "<span class=\"UsernameNotRegistered\">This e-mail is available.</span>";
			} else {
                if (!$currentAcc || $accExistsID != $currentAcc) {
                    echo "<span class=\"UsernameRegistered\">$request->username already registered!</span>";
                } else {
                    echo "&nbsp;";
                }
			}
		}
	}

 }

	function sponsor_review_reply(Request $request)
	{
		if($request->_token==csrf_token())
	{	
	 header("Content-Type: text/html; charset= UTF-8", TRUE);
   	 header("Accept-Encoding: gzip, deflate");
     header("Expires: Sat, 01 Jan 2000 00:00:00 GMT");
   	 header("Cache-Control: no-store, no-cache, must-revalidate");
     header("Cache-Control: post-check=0, pre-check", FALSE);
     header("Pragma: no-cache");



     if (Functions::string_strlen(trim($request->reply)) > 0) {

     	  //setting_get("review_approve", $review_approve);
     		$review_approve ="on";
            $responseapproved = 0;
            if (!$review_approve == "on") $responseapproved = 1;

            $reviewObj = new Review();
            $reviewObj->Review($request->idReview);
            $reviewObj->setString("response", trim($request->reply));
            $reviewObj->setString("responseapproved", $responseapproved);
            $reviewObj->Save();
            //$reply=rim($request->reply)




      echo "ok";
        } else {
            echo "error";
        }

}
	}

	function sponsor_lead_reply(Request $request)
	{
		if($request->_token==csrf_token())
		{	
		 header("Content-Type: text/html; charset= UTF-8", TRUE);
	   	 header("Accept-Encoding: gzip, deflate");
	     header("Expires: Sat, 01 Jan 2000 00:00:00 GMT");
	   	 header("Cache-Control: no-store, no-cache, must-revalidate");
	     header("Cache-Control: post-check=0, pre-check", FALSE);
	     header("Pragma: no-cache");

	   	$ajax_type=$request->ajax_type;
		$to=$request->to;
		$message=$request->message;
		$action=$request->action;

	    if ($ajax_type == "lead_reply") {
        
        //extract($_POST);
        $isAjax = true;

        //$response = $url_base."/leads/index.php?item_type=$item_type&item_id=$item_id&screen=$screen&letter=$letter";
        $errorMessage = "";

        if ($action == "reply") {
            
            if (!Functions::validate_email($to)) {
                $errorMessage .= "&#149;&nbsp;".config('params.LANG_MSG_ENTER_VALID_EMAIL_ADDRESS')."<br />";
            }
            
            if (!$message) {
                $errorMessage .= "&#149;&nbsp;".config('params.LANG_LEAD_TYPEMESSAGE');
            }
            
            if (!$errorMessage) {
                $leadObj = new Lead();
                 $leadObj->Lead($request->idLead);
                $leadObj->Reply($message, $to);
                $response = "&message=1";
                if (!$isAjax) {
                    header("Location: ".$response);
                    exit;
                } else {
                    echo "ok";
                }
            } elseif($isAjax) {
                echo $errorMessage;
            }
            
        }


        
		}

	}
}

	function getunpaidItems(Request $request)
	{
		if($request->_token==csrf_token())
		{	
		 header("Content-Type: text/html; charset= UTF-8", TRUE);
	   	 header("Accept-Encoding: gzip, deflate");
	     header("Expires: Sat, 01 Jan 2000 00:00:00 GMT");
	   	 header("Cache-Control: no-store, no-cache, must-revalidate");
	     header("Cache-Control: post-check=0, pre-check", FALSE);
	     header("Pragma: no-cache");

	   	$ajax_type=$request->ajax_type;
		$sess_id=Functions::sess_getAccountIdFromSession();

	    if ($ajax_type == "getunpaidItems") {
        
        	if (!isset($second_step)) {
			$sql = "SELECT count(id) as total_count FROM Listing WHERE account_id = ".$sess_id;
			$result = Sql::fetch($sql);
			if ($result !== false) {
				if ($row = $result) {
					$total_listing_sum = $row[0]->total_count;
				}
			}
			if ($total_listing_sum <= 50) {
				$listings = Sql::getBillingListing($sess_id,'title');


			}
						
		} 
		

		if ($listings) {

			$levelObj = new ListingLevel();
			$levelObj->ListingLevel(true);
			
			foreach ($listings as $each_listing) {
				// Need To Check Out
				$thisListing = new Listing();
				$thisListing->Listing($each_listing->id); 
				if ($thisListing->needToCheckOut()) 
					$bill_info["listings"]["{$each_listing->id}"]["needtocheckout"] = "y";
				else $bill_info["listings"]["{$each_listing->id}"]["needtocheckout"] = "n";
			

			}


		}
		if (empty($bill_info["listings"])) unset($bill_info["listings"]);


		if (!isset($second_step)) {

			$sql = "SELECT count(id) as total_count FROM Listing_Choice WHERE account_id = ".$sess_id;
			$result = Sql::fetch($sql);
			if ($result !== false) {
				if ($row = $result) {
					$total_badges_sum = $row[0]->total_count;
				}
			}
		
			if ($total_badges_sum <= 50) {
				
				$badges = Sql::getBadgesListing($sess_id,'editor_choice_id');
			
				
			}
		}
	

		if (count($badges)>0) {

			foreach ($badges as $each_badge) {

				// Need To Check Out
				$thisListingChoice = new ListingChoice();
				$thisListingChoice->ListingChoice($each_badge->editor_choice_id,$each_badge->listing_id);


				if ($thisListingChoice->needToCheckOut()) $bill_info["badges"]["{$each_badge->id}"]["needtocheckout"] = "y";
				else $bill_info["badges"]["{$each_badge->id}"]["needtocheckout"] = "n";
				unset($thisListingChoice);

			}

			// There is no listing been payed.

		}
	if (empty($bill_info["badges"])) unset($bill_info["badges"]);

    
        $toPayItems[] = "listings";
       if (!empty($bill_info["badges"]))
       {
       	 $toPayItems[] = "badges";
       }
       
        /*$toPayItems[] = "events";
        $toPayItems[] = "banners";
        $toPayItems[] = "classifieds";
        $toPayItems[] = "articles";*/
         if (!empty($bill_info["custominvoices"]))
       {
      $toPayItems[] = "custominvoices";
       }
        //$toPayItems[] = "custominvoices";

        $countUnpaid = 0;

        foreach ($toPayItems as $toPayItem) {

            if ($bill_info[$toPayItem]) {

                if ($toPayItem == "custominvoices") {
                    $countUnpaid++;
                } else {
                    foreach($bill_info[$toPayItem] as $id => $info){
                        if ($info["needtocheckout"] == "y") {
                            $countUnpaid++;
                        }
                    }
                }
            }

        }

        echo $countUnpaid;
        
    } 

	}
	else
	{
		return false;
	}
}

	function listing_profile_update(Request $request)
	 {
	 	if($request->_token==csrf_token())
	    {
	    $account_id =$request->account_id;
		// Image Crop
		if ($request->image_type != "") {
		

			// TYPES
                //1 = GIF, 2 = JPG, 3 = PNG, 4 = SWF, 5 = PSD, 6 = BMP, 7 = TIFF(intel byte order), 8 = TIFF(motorola byte order), 9 = JPC, 10 = JP2, 11 = JPX, 12 = JB2, 13 = SWC, 14 = IFF, 15 = WBMP, 16 = XBM
                $user_id = Session::get('random_key');
                $dir = 'custom/profile';
                $files = glob("$dir/_0_".$user_id."_*.*");
               	//d($files[0]);die;
                if ($files[0]) {

                    switch ($request->image_type) {
                        case 1:
                            $img_type = 'gif';
                            $img_r = imagecreatefromgif($files[0]);
                        break;
                        case 2:
                            $img_type = 'jpeg';
                            $img_r = imagecreatefromjpeg($files[0]);
                        break;
                        case 3:
                            $img_type = 'png';
                            $img_r = imagecreatefrompng($files[0]);
                        break;
                    }

                    $dst_r = ImageCreateTrueColor($request->w, $request->h);

                    if ($img_r) {
                        $lowQuality = false;
                        if($img_type == "png" || $img_type == "gif"){
                        	imagealphablending($dst_r, false);
                            imagesavealpha($dst_r,true);
                            $transparent = imagecolorallocatealpha( $dst_r, 255, 255, 255, 127 );
                            imagefill( $dst_r, 0, 0, $transparent ); 
                            imagecolortransparent( $dst_r, $transparent);
                            $transindex = imagecolortransparent($img_r);
                            if($transindex >= 0) {
                                $lowQuality = true; //only use imagecopyresized (low quality) if the image is a transparent gif
                            }
                        }
                        
                        if ($img_type == "gif" && $lowQuality){ //use imagecopyresized for gif to keep the transparency. The functions imagecopyresized and imagecopyresampled works in the same way with the exception that the resized image generated through imagecopyresampled is smoothed so that it is still visible.
                            //low quality
                            imagecopyresized($dst_r, $img_r, 0, 0, $request->x, $request->y, $request->w, $request->h, $request->w, $request->h);
            
                        } else {
                            //better quality
                            imagecopyresampled($dst_r, $img_r, 0, 0, $request->x, $request->y, $request->w, $request->h, $request->w, $request->h);
                        }
                        
                    }

                    $crop_image = $dir."/crop_image.$img_type";
                    if ($img_type == 'gif') {
                        imagegif($dst_r, $crop_image);
                    } elseif ($img_type == 'jpeg') {
                        imagejpeg($dst_r, $crop_image);
                    } elseif ($img_type == 'png') {
                        imagepng($dst_r, $crop_image);
                    }

                    //removing image files
                    foreach ($files as $file) unlink($file);

			if ((file_exists($_FILES['image']['tmp_name']) || file_exists($crop_image)) && (!$request->crop_submit)) {
				$imageArray = $this->image_uploadForItem((($crop_image) ? $crop_image : $_FILES['image']['tmp_name']),$account_id."_", 200, 200, "", "", true);
						if ($imageArray["success"]) {
							$request->facebook_image = "";
							$request->facebook_image_height = 0;
							$request->facebook_image_width = 0;
							$upload_image = "success";
							$remove_image = false;
							$request->image_id = $imageArray["image_id"];
                            //remove old image
                            $profileObj = new Profile();
        					$profileObj->Profile($account_id);
				            $oldImage = $profileObj->getNumber("image_id");
				            
                            if ($oldImage) {
                                $imageAux = new Image();
                                $imageAux->Image($oldImage, true);
                                if ($imageAux) $imageAux->Delete();
                            }
                            
						} else $upload_image = "failed";
					}

                  
                }
			
		}    	

	if (isset($_POST['profile_save'])) {
		
		$accObj = new Account();
		$accObj->Account($account_id);
		$profileObj = new Profile();
		$profileObj->Profile($account_id);

	if (($accObj->getString("foreignaccount") == "y") && ($accObj->getString("foreignaccount_done") == "n")) {
		$request->nickname = $request->first_name." ".$request->last_name;

		if ($request->facebook_image) {
					//$request->image_id = "";
		}
                    
         $request->profile_complete = "y";

		$profileObj->makeFromRow($request->all());
		$profileObj->Save();

		return redirect('profile/edit')->with('message','Profile Updated Successfully');

	}

	else {
				$message_profile='';
					if ($request->facebook_image) {
						//$request->image_id = "";
					}

					if (!trim($request->nickname)) {
						$message_profile .= "&#149;&nbsp;Your Name is required.<br />";
						$error = 1;
					}
                    
                    /*if (!$friendly_url) {
                        $message_profile = "&#149;&nbsp;".system_showText(LANG_LABEL_YOURURL_REQUIRED)."<br />";
                        $error = 1;
                    } else {
                        if (!preg_match(FRIENDLYURL_REGULAREXPRESSION, $friendly_url)) {
                            $message_profile = "&#149;&nbsp;".system_showText(LANG_MSG_FRIENDLY_URL_INVALID_CHARS)."<br />";
                            $error = 1;
                        }
                    }

					if ($profileObj->fUrl_Exists($_POST["friendly_url"])) {
						$message_profile .= "&#149;&nbsp;".system_showText(LANG_MSG_PAGE_URL_IN_USE);
						$error = 1;
					}*/

					if (!isset($error)) {

						$profileObj->Profile($request->all());
						$profileObj->setString('image_id',$request->image_id);


						$profileObj->Save();
						$accDomain = new Account_Domain();
						$accDomain->Account_Domain($profileObj->getNumber("account_id"));
						$accDomain->Save();
						//$accDomain->saveOnDomain($profileObj->getNumber("account_id"), false, false, $profileObj);

				return redirect('profile/edit')->with('message','Profile Updated Successfully');


						/*$profileObj = new Profile($account_id);
						$profileObj->extract();*/
					}
				}


		//$accDomain = new Account_Domain($profileObj->getNumber("account_id"), SELECTED_DOMAIN_ID);
		//$accDomain->Save();
		//$accDomain->saveOnDomain($profileObj->getNumber("account_id"), false, false, $profileObj);

	}	

	function arrayValue($v, $w) {
        if ( $v != '' && $w == '' ) return $v;
        elseif ( $w != '' ) return $w;
    }

    $aux_aspectRat = 0;
    if (Functions::string_strpos($_SERVER["PHP_SELF"], "/profile") !== false || (Functions::string_strpos($_SERVER["PHP_SELF"], "/account") !== false)) {
          $_image_relative_path = config('params.IMAGE_RELATIVE_PATH');
        $_image_dir = 'custom/domain_1/image_files';
        $_image_url = 'custom/domain_1/image_files';
    } else {
    	  $_image_relative_path = config('params.PROFILE_IMAGE_RELATIVE_PATH');
        $_image_dir = 'custom/profile';
        $_image_url = url('custom/profile');
        $aux_aspectRat = 1;
  

    }

    $user_id = Session::get('random_key');
    $dir = $_image_dir;
    $return='';

    $crop_sub=$_POST["crop_submit"];

    if ( is_array($crop_sub) ) $crop = array_shift( $crop_sub);
    else $crop =   $crop_sub;
    if ( $crop == 'true' ) $crop = true;

    if ( !is_bool($crop) ) {

        $counter = $crop;
        $files = array_keys($_FILES);
        $file = $files[$crop-1];
    } else {
        $counter = '';
        $arrayKeysFiles = array_keys($_FILES);
        $file = array_shift($arrayKeysFiles);
    }

    if ($crop && $_FILES[$file]) {

        $str_type = $_FILES[$file]["type"];
        if ( is_array($str_type) ) $str_type = array_reduce($str_type, 'arrayValue');
        $array_type = explode("/", $str_type);
        $imagetype = array_pop($array_type);
        if ($imagetype == 'pjpeg') $imagetype = 'jpeg';
        if ($imagetype == 'x-png') $imagetype = 'png';

        $files = glob("$dir/_".($crop - 1)."_".$user_id."_*.*");

         $return.="<script type=\"text/javascript\">" ;
        if ($_FILES[$file]["error"] == 1) {
             $return.= "parent.noImage('size','" . $counter . "');" ;
        }else if ($imagetype == 'jpeg' || $imagetype == 'gif' || $imagetype == 'png') {

            $imagename = uniqid("_".($crop - 1)."_".$user_id."_") . "." . $imagetype;

            $file_tmpname = $_FILES[$file]['tmp_name'];
            if ( is_array($file_tmpname) ) $file_tmpname = array_reduce($file_tmpname, 'arrayValue');
            $file_name = $_FILES[$file]['name'];
            if ( is_array($file_name) ) $file_name = array_reduce($file_name, 'arrayValue');

            // move (actually just rename) the temporary file to the real name.
            // write permissions are required by the script to place the image here.

            $maxImageSize = "0000000";
            $maxImageSize = Functions::string_substr($maxImageSize, Functions::string_strlen((config('params.UPLOAD_MAX_SIZE') * 10) + 1));
            $maxImageSize = ((config('params.UPLOAD_MAX_SIZE') * 10) + 1)."00000";

            if (filesize($file_tmpname) < $maxImageSize) {
                move_uploaded_file ( $file_tmpname, $dir."/".$imagename );
                list($width, $height, $type, $attr) = getimagesize($_image_dir."/".$imagename);
                if ($type == "1" || $type == "2" || $type == "3"){
                    if(file_exists($dir."/".$imagename) && $file_name != '') {
                        $return.="parent.SetImageFile(\"" . $_image_url."/".$imagename . "\", " . $width . ", " . $height . ", " . $type . ", '" . $counter . "' , $aux_aspectRat);" ;
                    }
                } else {
                    $return.= "parent.noImage('type','" . $counter . "');" ;
                }
            } else {
                $return.= "parent.noImage('size','" . $counter . "');" ;
            }
        } else {
            $return.="parent.noImage('type','" . $counter . "');" ;
        }

        $return.="</script>";

        foreach ($files as $file) unlink($file);
    }



    		$userAccountId = Session::get('SESS_ACCOUNT_ID');
			$profileSp = Functions::SponsersProfile();
			$ownerName= '';
			$image_id=$profileSp[0]->image_id;

			if(!empty($profileSp))
			{
			   $ownerName = $profileSp[0]->first_name.' '.$profileSp[0]->last_name.', ';
		   }
		   $meta_data = Sql::sponsersMeta($userAccountId);
		   $headerTitle = $ownerName.'Welcome to the Sponsor Section -'.$meta_data[0]->value;
		   $headerAuthor = $meta_data[1]->value;
		   $headerDescription = $meta_data[2]->value;
		   $headerKeywords = $meta_data[3]->value;
		   $footer_text = Sql::footerText();
			//Session::put('random_key', uniqid(($userAccountId).strtotime(date('Y-m-d H:i:s'))));
			$domain_url=url('profile');
			if ($image_id) {
            
            $imageObj = new Image();
            $imageObj->Image($image_id, true);
            if ($imageObj->imageExists()) {
 			$file = $imageObj->getPath(true);
        	$imgTag = "<img src=\"".$file."\" width=\"".config('params.PROFILE_MEMBERS_IMAGE_WIDTH')."\" height=\"".config('params.PROFILE_MEMBERS_IMAGE_HEIGHT')."\"  style=\"display:inline;\"/>";
            } else {
                $imgTag = "<img src=\"".url()."/images/profile_noimage.gif\" width=\"".config('params.PROFILE_MEMBERS_IMAGE_WIDTH')."\" height=\"".config('params.PROFILE_MEMBERS_IMAGE_HEIGHT')."\" alt=\"No Image\" />";
            }
        } else {
            $imgTag = "<img src=\"".url()."/images/profile_noimage.gif\" width=\"".config('params.PROFILE_MEMBERS_IMAGE_WIDTH')."\" height=\"".config('params.PROFILE_MEMBERS_IMAGE_HEIGHT')."\" alt=\"No Image\" />";
        }
		  
		   
		   $spListingData = Sql::SponserListing($userAccountId);
			return view('front.profile_edit',compact('spListingData','domain_url','profileSp','userAccountId','headerTitle','headerAuthor','headerDescription','headerKeywords','footer_text','return','image_id','imgTag'));





}
else
{
	return false;
}	

			
	 }




 function cropProfileImage(Request $request)
 {
 	$multi=$request->multi;
 	$sitemgrLang=$request->sitemgrLang;



 	return view('front.form_crop',compact('multi','sitemgrLang'));


 }


	function listing_profile_ajax(Request $request)
	 {
	 	if($request->_token==csrf_token())
	    {
	    header("Content-Type: text/html; charset= UTF-8", TRUE);
	   	 header("Accept-Encoding: gzip, deflate");
	     header("Expires: Sat, 01 Jan 2000 00:00:00 GMT");
	   	 header("Cache-Control: no-store, no-cache, must-revalidate");
	     header("Cache-Control: post-check=0, pre-check", FALSE);
	     header("Pragma: no-cache");

	    	$action=$request->action;
	    	$account_id=$request->account_id;
	    	$ajax=$request->ajax;

	if ($ajax) 
	{
				
        if ($action == "changeStatus") 
        {
        	if ($request->has_profile == "checked" || $request->has_profile == "true") {
                        $has_profile = true;
                    } else {
                        $has_profile = false;
                    }
             $accObj = new Account();
             $accObj->Account($account_id);
             $accObj->changeProfileStatus($has_profile);

             //$accDomain = new Account_Domain($accObj->getNumber("id"), SELECTED_DOMAIN_ID);
             //$accDomain->Save();
             //$accDomain->saveOnDomain($accObj->getNumber("id"), $accObj);        

      	}
      	elseif ($action == "removePhoto") 
        {

        	$profileObj = new Profile();
        	$profileObj->Profile($account_id);
            $idm = $profileObj->getNumber("image_id");
            $image = new Image();
            $image->Image($idm, true);
			if ($image) $image->Delete();
                    
                    $profileObj->setNumber("image_id", 0);
                    $profileObj->setString("facebook_image", "");
                    $profileObj->setNumber("facebook_image_height", 0);
                    $profileObj->setNumber("facebook_image_width", 0);
                    
                    $profileObj->save();

        }

    } 

	    }
	 }	

	function check_friendlyurl(Request $request)
	 {
	 		header("Content-Type: text/html; charset=UTF-8", TRUE);
		    header("Accept-Encoding: gzip, deflate");
		    header("Expires: Sat, 01 Jan 2000 00:00:00 GMT");
		    header("Cache-Control: no-store, no-cache, must-revalidate");
		    header("Cache-Control: post-check=0, pre-check", FALSE);
		    header("Pragma: no-cache");
		    $type=$request->type;
		    $friendly_url=$request->friendly_url;
		    $current_acc=$request->current_acc;

		     if ($type == "profile") {

       
        $sql = "SELECT account_id FROM Profile WHERE friendly_url = '".$friendly_url."' AND account_id != ".$current_acc;
        $result = Sql::fetchMain($sql);
        if (count($result) > 0) {
            echo "not ok";
        } else {
            echo "ok";
        }

    }



	 }




	
	    
    
    

}
