<?php 
namespace App\Http\Controllers;
use DB;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Validator, Input, Redirect,DateTime,Session,Mail,Config;
use App\Functions\Location4;
use App\Functions\Location3;
use App\Functions\Listing;
use App\Functions\Functions;
use App\Functions\Lead;
use App\Functions\Review;
use App\Functions\Contact;
use App\Functions\Account;
use App\Functions\Profile;
use App\Functions\ContactReport;
use App\Models\Domain\Sql;
use App\Models\Main\MainSql;

class SearchController extends Controller {

    protected $layout = 'layouts.search';
    
    
    protected $friendly_url;
   

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
	  
	  //d(Session::all());
		//$this->middleware('auth');
	}
    

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
       $params = array();
       $states = Sql::AdvanceSeachStates();
       $statesData = MainSql::AdvanceSeachStatesData(implode(',',$states));
       $developmentNames = Sql::DevelopmentData();
       $footer_text = Sql::footerText();
       
       $params['type'] = 'Home Page';
       $meta_data = Sql::HomePageSummerSale($params);
       
       $header_author = Sql::HomePageHeaderAuthor();
       
       $header_searching_tag = Sql::HomePageHeaderSearchingTag();
       
       return view('front.advance_search',compact('statesData','developmentNames','footer_text','meta_data','header_author','header_searching_tag'));
	}
    
    public function advance_location(Request $request)
    {
        
        if($request->_token==csrf_token())
        {
            $state  = $request->state;
            if($state && $state!='' && $state!=0) 
            {
                $listing_ids = Sql::ListingLocation();
                if(!empty($listing_ids))
                {
                    foreach($listing_ids as $val)
                    {
                        $temp_ids[] = $val->location_4;
                    }
                }
                $listing_idsClean = implode(',',$temp_ids);
                if(!empty($listing_idsClean))
                {
                    $cities = MainSql::AdvanceLocation($state,$listing_idsClean);
                    return json_encode($cities);
                }
                else
                {
                    echo json_encode('-1');
                    return false;
                }
                
                
            }
            else
            {
                echo json_encode('-1');
                return false;
            }
        }
        else
        {
            echo json_encode('0');
            return false;
        }
    }
    
    public function SearchResults(Request $request)
    {  
        
        $params=array();
        $requestParams='';
        $requestParams = $request->all; 
        $otherLinks = str_replace("+", "-",explode('/', $requestParams));
        //d($otherLinks);die;
        ///////
        $locationsArray = Session::get('locations');
        $loc3 = isset($locationsArray['location_3'])?$locationsArray['location_3']:0;
        $loc4 = isset($locationsArray['location_4'])?$locationsArray['location_4']:0;
        $rate_array=array();
        $checkin_aux=array();
        $checkout_aux=array();
        $features_array=array();
        $amenities_array=array();
        //$params['location_3']=$loc3;
//        $params['location_1']=0;
//        $params['location_4']=$loc4;
        $params['location_3']=0;
        $params['location_1']=0;
        $params['location_4']=0;

        $params['feature_smokingpermitted']=0;
        $params['feature_petsallowed']=0;
        $params['feature_wheelchairaccess']=0;
        $params['feature_pool']=0;
        $params['feature_elevator']=0;
        $params['feature_boatslip']=0;
        $params['amenity_internet']=0;
        
        $params['rental_id']=0;
        $params['view']=0;
        $params['address']=0;
        $params['sleeps_number']  = 0;
        $params['sleeps_condition'] = 0;
        $params['bedroom_number']  = 0;
        $params['bedroom_condition'] = 0;
        $params['bathroom_number']  = 0;
        $params['bathroom_condition'] = 0;
        $params['distance_beach']=0;
        $params['property_type']=0;
        $params['development_name']=0;
        $params['keyword']  = 0;
        $params['checkin']=0;
        $params['checkout']=0;
        $params['period']=0;
        $params['rate_min_number']=0;
        $params['rate_max_number']=0;
        
        $params['orderByLocation_3']=0;
        $params['orderByLocation_4']=0;
        
        $params['rating']=0;
        $page_offset = 0;
        
        foreach($otherLinks as $val)
        {
            
             if (strpos($val, 'screen-') !== false) {
            $screen_array = explode("-", $val);
            $page_offset = $screen_array[1];
            }
            if (strpos($val, 'specials-') !== false) {
                $special_array = explode("-", $val);
                $special = $special_array[1];
                if($special!='')
                {
                    $spData = Sql::getSpId($special);
                    if($spData[0]->id!='' && $spData[0]->id>0)
                    {
                        $sp_id = $spData[0]->id;
                        
        
                        if(is_numeric($sp_id) && $sp_id>0)
                        {
                           
                            $per_page = 100;
                            $offset = $page_offset?$page_offset:1;
                            
                            $footer_text = Sql::footerText();
                        
                            $states = Sql::AdvanceSeachStates();
                            if(!empty($states))
                            {
                                $statesData = MainSql::AdvanceSeachStatesData(implode(',',$states));
                            }
                            else
                            {
                                $statesData=array();
                            }
                            
                            $meta_sp = Sql::getSpecialTitle($sp_id);
                
                            $name_special = $meta_sp[0]->name;
                            $params['type'] = 'Home Page';
                            $meta_data = Sql::HomePageSummerSale($params);
                            if(isset($meta_sp) && !empty($meta_sp))
                            {
                               $meta_data[0]->title=$meta_sp[0]->title;
                               $meta_data[0]->description=$meta_sp[0]->meta_description;
                               $meta_data[0]->keywords=$meta_sp[0]->meta_keys;
                               
                               
                            }
                            
                            $header_author = Sql::HomePageHeaderAuthor();
                            
                            $header_searching_tag = Sql::HomePageHeaderSearchingTag();
                            $featured_images = Sql::getHomePageFeaturedRental();
                            
                            
                            $result = Sql::getSpecials($sp_id, $offset, $per_page);
                           
                            $results = $result['result'];
                            
                			$result_count = $result['total'][0]->total;
                			$result_pages = $result_count/$per_page;
                			if($offset + $per_page < $result_count) {
                				$next_offset = $offset + $per_page;
                			} else {
                				$next_offset = 0;
                			}
                			$current_offset = $offset;
                			$per_page = $per_page;
                            
                            return view('front.special_listing',compact('results','result_pages','page_offset','result_count','next_offset','current_offset','per_page','statesData','featured_images','sp_id','name_special','footer_text','meta_data','header_author','header_searching_tag'));
                            
                        }
                        else
                        {
                            continue;
                        }
                    }
                    else
                    {
                        continue;
                    }
                    
                }
                else
                {
                    continue;
                }
            }
            
            if (strpos($val, 'rate') !== false) {
            $rate_array = explode("-", $val);
            }
            if (strpos($val, 'united-states') !== false) {
                if(isset($otherLinks[0]) && !empty($otherLinks[0]))
                {
                  $params['location_1'] =  $this->Location1friendlyUrl($otherLinks[0]);
                }
                
                if(isset($otherLinks[1]) && !empty($otherLinks[1]))
                {
                  $params['location_3'] =  $this->Location3friendlyUrl($otherLinks[1]);
                }
            if(isset($otherLinks[2]) && !empty($otherLinks[2]))
                {
                  $params['location_4'] =  $this->Location4friendlyUrl($otherLinks[2]);
                }
            
            }
            
             if (strpos($val, 'rating') !== false) {
            $rating_array = explode("-", $val);
            
             $params['rating']=$rating_array[1];
            }
            
            if (strpos($val, 'checkin') !== false) {
            $checkin_aux = explode("-", $val);
            
            $checkin_aux = $checkin_aux[1]."/".$checkin_aux[2]."/".$checkin_aux[3];
            }
            
            if (strpos($val, 'checkout') !== false) {
            $checkout_aux = explode("-", $val);
            $checkout_aux = $checkout_aux[1]."/".$checkout_aux[2]."/".$checkout_aux[3];
            }
            
            if (strpos($val, 'feature') !== false) {
                    $features_array = explode("-", $val);
                     if($val && $features_array){
                    foreach ($features_array as $value) {
                        switch ($value) {
                            case 'noSmokingpermitted':
                                $params['feature_smokingpermitted']='n';
                                break;
                            case 'smokingpermitted':
                                $params['feature_smokingpermitted']='y';
                                break;
                            case 'noPetsallowed':
                            $params['feature_petsallowed']='n';
                                break;
                            case 'petsallowed':
                                $params['feature_petsallowed']='y';
                                break;
                            case 'noWheelchairaccess':
                            $params['feature_wheelchairaccess']='n';
                                break;
                            case 'wheelchairaccess':
                                $params['feature_wheelchairaccess']='y';
                                break;
                            case 'noPool':                 
                            $params['feature_pool']='n';
                                break;
                            case 'pool':
                                $params['feature_pool']='y';
                                 break;
                            case 'noElevator':
                            $params['feature_elevator']='n';
                                break;
                            case 'elevator':
                            $params['feature_elevator']='y';
                                break;
                            case 'noBoatslip':
                            $params['feature_boatslip']='n';
                                break;
                            case 'boatslip':
                                $params['feature_boatslip']='y';
                                break;
                        }
                    }
                } 
                unset($value);
            }
            if (strpos($val, 'amenity') !== false) {
                //amenities
                $amenities_array = explode("-", $val);
                
                if($val && $amenities_array){
                    foreach ($amenities_array as $value) {
                        switch ($value) {
                            case 'noInternet':
                            $params['amenity_internet']='n';
                                    break;
                            case 'internet':
                                $params['amenity_internet']='y';
                                break;
                        }
                    }
                } 
                unset($value);
            }
           
            
             
            if (strpos($val, 'filter_location_3') !== false) 
            {
                 if($val)
               {
                    $params['location_3']=$val;
               }
               else
               {
                    $params['location_3']=0;
               }
            }
            
            if (strpos($val, 'filter_location_4') !== false)
            {
               if($val)
               {
                    $params['location_4']=$val;
               }
               else
               {
                    $params['location_4']=0;
               }
       
            }
            if (strpos($val, 'orderby-') !== false)
            {
                
               if($val)
               {
                    $params['orderByLocation_4']=$val;
               }
               else
               {
                    $params['orderByLocation_4']=0;
               }
       
            }
            
            if (strpos($val, 'id') !== false)
            {
                if($val)
               {
                    $rental_id = str_replace("id-","",$val);
                    $params['rental_id']=$rental_id;
               }
               else
               {
                    $params['rental_id']=0;
               }
            }
            
            if (strpos($val, 'view') !== false)
            {
                if($val)
               {
                     $viewTemp = explode("-",$val);
                     
                    $params['view']=$viewTemp[1];
               }
               else
               {
                    $params['view']=0;
               }
            }
            
            if (strpos($val, 'address') !== false)
            {
                if($val)
               {
                    $address = str_replace("address-","",$val);
                    $params['address']=trim(str_replace("-"," ",$address));
               }
               else
               {
                    $params['address']=0;
               }
            }
            //d($val);
            if (strpos($val, 'sleeps') !== false)
            {
                if($val)
                {
                    $sleeps = $val;
                    $arr_sleeps = preg_split('/(?<=[0-9])(?=[a-z]+)/i',$sleeps);                                                               
                    
                    $params['sleeps_number']  = str_replace('-', '', filter_var($arr_sleeps[0], FILTER_SANITIZE_NUMBER_INT));
                    $params['sleeps_condition'] = $arr_sleeps[1];
                    
                    //setCookie("sleeps", $val, time()+3600, '/', $_SERVER['HTTP_HOST']);
                    
                } 
                else {
                    $params['sleeps_number']  = 0;
                    $params['sleeps_condition'] = 0;
                } 
            }
            
            
            if (strpos($val, 'bedroom') !== false)
            {
                if($val)
               {
                    $bedroom = $val;
                    $arr_bedroom = preg_split('/(?<=[0-9])(?=[a-z]+)/i',$bedroom);                                                               
                    
                    $params['bedroom_number']  = str_replace('-', '', filter_var($arr_bedroom[0], FILTER_SANITIZE_NUMBER_INT));
                    $params['bedroom_condition'] = $arr_bedroom[1];
               }
               else
               {                                                           
                
                $params['bedroom_number']  = 0;
                $params['bedroom_condition'] = 0;
               }
            } 
            
            
            if (strpos($val, 'bathroom') !== false)
            {
                if($val)
               {
                   $bathroom = $val;
                    $arr_bathroom = preg_split('/(?<=[0-9])(?=[a-z]+)/i',$bathroom);                                                               
                    
                    $params['bathroom_number']  = str_replace('-', '', filter_var($arr_bathroom[0], FILTER_SANITIZE_NUMBER_INT));
                    $params['bathroom_condition'] = $arr_bathroom[1];
               }
               else
               {
                    $params['bathroom_number']  = 0;
                    $params['bathroom_condition'] = 0;
               }
            }
            
            
            if (strpos($val, 'distance_beach') !== false)
            {
               
                 if($val)
                   {
                    $distance_beach = explode("-", $val);
                    
                        $params['distance_beach']=$distance_beach[1];
                   }
                   else
                   {
                        $params['distance_beach']=0;
                   }
                   
            }
            
            if (strpos($val, 'property_type') !== false)
            {
                if($val)
                {
                    $property_type = explode("-", $val);
                    $params['property_type']=trim(str_replace("-"," ",$property_type[1]));
                }
                else
                {
                    $params['property_type']=0;
                }
            }
            
            if (strpos($val, 'development_name') !== false)
            {
                if($val)
               {
                    $development_name = str_replace("development_name-","",$val);
                    $params['development_name']=trim(str_replace("-"," ",$development_name));
               }
               else
               {
                    $params['development_name']=0;
               }
            }
            
            if (strpos($val, 'amenity') !== false)
            {
                if($val) setCookie("amenity", $val, time()+3600, '/', $_SERVER['HTTP_HOST']);
                else {
                    unset($_Session['amenity']);
                    setCookie("amenity", '', time() - 3600, "/", $_SERVER['HTTP_HOST']);
                } 
            }
            
            if (strpos($val, 'keyword') !== false)
            {
                
                if($val)
                {
                    $keyword = explode("keyword-", $val);
                    $params['keyword']  = trim($keyword[1]);
                }
                else {
                    $params['keyword']  = 0;
                } 

            }
            
            
            if($checkin_aux)
            {
                $date_checkin = new DateTime($checkin_aux);
                $params['checkin']  = $date_checkin->format('Y-m-d');//  31.07.2012
            } 
            else {
                $params['checkin']=0;
            } 
            if($checkout_aux){
                $date_checkout = new DateTime($checkout_aux);
                $params['checkout']  = $date_checkout->format('Y-m-d');//  31.07.2012
            }
            else {
                $params['checkout']=0;
            } 
            
            if (strpos($val, 'period') !== false)
            {
                if($val)
                {
                    $params['period']=substr($val, 7);
                }
                else {
                    $params['period']=0;
                } 
            }
            
              
            
        }
        
        
       
        if(!empty($rate_array) && $rate_array[0])
        {
            if($rate_array[1])
           {
                $rate_min = $rate_array[1];
                if(is_numeric($rate_min))
                {
                    $params['rate_min_number'] = $rate_min;
                }
                else
                {
                    $params['rate_min_number']=0;
                }
               
           }
           else
           {                                                           
            
            $params['rate_min_number']=0;
           }
        } 
         
        
        if(!empty($rate_array) && $rate_array[1])
        {
            if($rate_array[2])
           {
                $rate_max = $rate_array[2];
                if(is_numeric($rate_max))
                {
                    $params['rate_max_number'] = $rate_max;
                }
                else
                {
                    $params['rate_max_number']=0;
                }
               
           }
           else
           {                                                           
            
            $params['rate_max_number']=0;
           }
        }
        
        
        //d($_Session['location_1']);die;
            
       
        //////////
		if (!$page_offset)
		{
			$page_offset = 0;
		}
		else
		{
			$page_offset = ($page_offset==1)?0:$page_offset;
		}
		
        $per_page = 100;
        $offset = $page_offset?$page_offset:0;
       
        $footer_text = Sql::footerText();
        
        $params['type'] = 'Home Page';
        $meta_data = Sql::HomePageSummerSale($params);
        $meta_data[0]->title = 'Listing Results';
        $header_author = Sql::HomePageHeaderAuthor();
        
        $header_searching_tag = Sql::HomePageHeaderSearchingTag();
        $featured_images = Sql::getHomePageFeaturedRental();
        
        $states = Sql::AdvanceSeachStates();
        $statesData = MainSql::AdvanceSeachStatesData(implode(',',$states));
        $cityData = array();
        $listingTitle = '';
        if(is_numeric($params['location_1']) && $params['location_1']>0)
        {
            
            $sql = "SELECT page_description,name as page_title, name as title,page_description as description,seo_keywords as keywords FROM Location_1 WHERE id = ".$params['location_1'];
            $meta_data=MainSql::getLocation($sql);
            
        }
        if(is_numeric($params['location_3']) && $params['location_3']>0  && is_numeric($params['location_4']) && !$params['location_4']>0)
        {
            $cityData = Sql::citiesFilters($params['location_3']);
            $sql = "SELECT page_description,page_title, seo_metatitle as title,page_description as description,seo_keywords as keywords FROM Location_3 WHERE id = ".$params['location_3'];
            $meta_data=MainSql::getLocation($sql);
            
        }
        if(is_numeric($params['location_4']) && $params['location_4']>0)
        {
            
            $sql = "SELECT page_description,page_title, seo_metatitle as title,page_description as description,seo_keywords as keywords FROM Location_4 WHERE id = ".$params['location_4'];
            $meta_data=MainSql::getLocation($sql);
            
        }
        
        
//        $date_checkin = new DateTime($request->checkin);
//        $params['checkin']  = $date_checkin->format('Y-m-d');  31.07.2012
//        $date_checkout = new DateTime($request->checkout);
//        $params['checkout']  = $date_checkout->format('Y-m-d');  31.07.2012
        if($page_offset>0)
        {
            $offset = ($page_offset-1) * $per_page; 
        }
        //$per_page = $offset+$per_page;
        //d($params);
        $result = Sql::SearchingResult($params, $offset, $per_page);
       
        $results = $result['result'];
        
		$result_count = $result['total'][0]->total;
		$result_pages = $result_count/$per_page;
		if($offset + $per_page < $result_count) {
			$next_offset = $offset + $per_page;
		} else {
			$next_offset = 0;
		}
		$current_offset = $offset;
		$per_page = $per_page;
        
        
        $rental_id=$params['keyword'];
        return view('front.results_search',compact('results','result_pages','page_offset','result_count','next_offset','current_offset','per_page','result_count','statesData','cityData','featured_images','listingTitle','footer_text','meta_data','header_author','header_searching_tag','rental_id','params'));
 
    }
    
    
    public function AdvanceSearchResults(Request $request)
    {
        $params=array();
        
		if (!$request->page_offset)
		{
			$page_offset = 1;
		}
		else
		{
			$page_offset = ($request->page_offset==1)?0:$request->page_offset;
		}
        $per_page = 100;
        $offset = $page_offset?$page_offset:1;
        
        $footer_text = Sql::footerText();
        
        $params['type'] = 'Home Page';
        $meta_data = Sql::HomePageSummerSale($params);
        $meta_data[0]->title = 'Listing Results';
        
        $header_author = Sql::HomePageHeaderAuthor();
        
        $header_searching_tag = Sql::HomePageHeaderSearchingTag();
        $featured_images = Sql::getHomePageFeaturedRental();
        
        
        $states = Sql::AdvanceSeachStates();
        $statesData = MainSql::AdvanceSeachStatesData(implode(',',$states));
        
        
        if($request->_token==csrf_token())
        {

           if($request->sleeps)
           {
                $sleeps = $request->sleeps;
                $arr_sleeps = preg_split('/(?<=[0-9])(?=[a-z]+)/i',$sleeps);                                                               
                
                $params['sleeps_number']  = $arr_sleeps[0];
                $params['sleeps_condition'] = $arr_sleeps[1];
           }
           else
           {                                                           
            
            $params['sleeps_number']  = 0;
            $params['sleeps_condition'] = 0;
           }
           
           if($request->bathroom)
           {
               $bathroom = $request->bathroom;
                $arr_bathroom = preg_split('/(?<=[0-9])(?=[a-z]+)/i',$bathroom);                                                               
                
                $params['bathroom_number']  = $arr_bathroom[0];
                $params['bathroom_condition'] = $arr_bathroom[1];
           }
           else
           {
                $params['bathroom_number']  = 0;
            $params['bathroom_condition'] = 0;
           }
            
            
          if($request->bedroom)
           {
                $bedroom = $request->bedroom;
                $arr_bedroom = preg_split('/(?<=[0-9])(?=[a-z]+)/i',$bedroom);                                                               
                
                $params['bedroom_number']  = $arr_bedroom[0];
                $params['bedroom_condition'] = $arr_bedroom[1];
           }
           else
           {                                                           
            
            $params['bedroom_number']  = 0;
            $params['bedroom_condition'] = 0;
           }
           
           if($request->rate_min)
           {
                $rate_min = $request->rate_min;
                if(is_numeric($rate_min))
                {
                    $params['rate_min_number'] = $rate_min;
                }
                else
                {
                    $params['rate_min_number']=0;
                }
               
           }
           else
           {                                                           
            
            $params['rate_min_number']=0;
           }
           
           if($request->rate_max)
           {
                $rate_max = $request->rate_max;
                if(is_numeric($rate_max))
                {
                    $params['rate_max_number'] = $rate_max;
                }
                else
                {
                    $params['rate_max_number']=0;
                }
               
           }
           else
           {                                                           
            
            $params['rate_max_number']=0;
           }
           
           
           if($request->period)
           {
                $params['period']=$request->period;
           }
           else
           {
                $params['period']=0;
           }
           
           if($request->location_3)
           {
                $params['location_3']=$request->location_3;
           }
           else
           {
                $params['location_3']=0;
           }
           
            if($request->location_4)
           {
                $params['location_4']=$request->location_4;
           }
           else
           {
                $params['location_4']=0;
           }
           
           if($request->development_name)
           {
                $params['development_name']=$request->development_name;
           }
           else
           {
                $params['development_name']=0;
           }
           
            if($request->rental_id)
           {
                $params['rental_id']=$request->rental_id;
           }
           else
           {
                $params['rental_id']=0;
           }
           
            if($request->view)
           {
                $params['view']=$request->view;
           }
           else
           {
                $params['view']=0;
           }
           
            if($request->property_type)
           {
                $params['property_type']=$request->property_type;
           }
           else
           {
                $params['property_type']=0;
           }
           
           if($request->address)
           {
                $params['address']=$request->address;
           }
           else
           {
                $params['address']=0;
           }
            
            
           
             if($request->feature_smokingpermitted)
           {
                $params['feature_smokingpermitted']=$request->feature_smokingpermitted;
           }
           else
           {
                $params['feature_smokingpermitted']=0;
           }
           
            if($request->feature_petsallowed)
           {
                $params['feature_petsallowed']=$request->feature_petsallowed;
           }
           else
           {
                $params['feature_petsallowed']=0;
           }
           
           if($request->feature_wheelchairaccess)
           {
                $params['feature_wheelchairaccess']=$request->feature_wheelchairaccess;
           }
           else
           {
                $params['feature_wheelchairaccess']=0;
           }
           
             if($request->distance_beach)
           {
                $params['distance_beach']=$request->distance_beach;
           }
           else
           {
                $params['distance_beach']=0;
           }
           
             if($request->feature_pool)
           {
                $params['feature_pool']=$request->feature_pool;
           }
           else
           {
                $params['feature_pool']=0;
           }
             if($request->feature_elevator)
           {
                $params['feature_elevator']=$request->feature_elevator;
           }
           else
           {
                $params['feature_elevator']=0;
           }
           
           if($request->feature_boatslip)
           {
                $params['feature_boatslip']=$request->feature_boatslip;
           }
           else
           {
                $params['feature_boatslip']=0;
           }
           
           
            if($request->amenity_internet)
           {
                $params['amenity_internet']=$request->amenity_internet;
           }
           else
           {
                $params['amenity_internet']=0;
           }
           
           
            if($request->orderByLocation_3)
           {
                $params['orderByLocation_3']=$request->orderByLocation_3;
           }
           else
           {
                $params['orderByLocation_3']=0;
           }
           
            if($request->rating)
           {
                $params['rating']=$request->rating;
           }
           else
           {
                $params['rating']=0;
           }
           
           
            
           
            $params['keyword']  = trim($request->keyword);
            $params['checkin']  = $request->checkin;
            $params['checkout']  = $request->checkout;
            if($page_offset>0)
            {
                $offset = ($page_offset-1) * $per_page; 
            }
            $result = Sql::SearchingResult($params, $offset, $per_page);
           
            $results = $result['result'];
            
			$result_count = $result['total'][0]->total;
			$result_pages = $result_count/$per_page;
			if($offset + $per_page < $result_count) {
				$next_offset = $offset + $per_page;
			} else {
				$next_offset = 0;
			}
			$current_offset = $offset;
			$per_page = $per_page;
            
            return view('front.results_search',compact('results','result_pages','page_offset','result_count','next_offset','current_offset','per_page','statesData','featured_images','footer_text','meta_data','header_author','header_searching_tag'));
 
        }
        else
        {
            header('Location: '.URL());
            return true;
        }
    }
    
    public function quickInfo(Request $request)
    {
        $params=array();
        
        if($request->_token==csrf_token())
        {
          $params['resource_id']  = $request->resource_id;
          $result = Sql::quickInfoListing($params);
          echo json_encode($result);
        }
        else
        {
            echo json_encode('0');
        }
    }
    
    public function compareListing()
    {
       
        $footer_text = Sql::footerText();
        
        $params['type'] = 'Home Page';
        $meta_data = Sql::HomePageSummerSale($params);
        
        $header_author = Sql::HomePageHeaderAuthor();
        
        $header_searching_tag = Sql::HomePageHeaderSearchingTag();
        $featured_images = Sql::getHomePageFeaturedRental();
        
        if(isset($_COOKIE['result_count']) &&  $_COOKIE['result_count']!='')
        {
            $params=array();
           
            $Session_array = $_COOKIE;
            $resource_ids = array();
            $row_number = array();

            //d($_COOKIE);
            for($i=0;$i<$_COOKIE['result_count'];$i++)
            {
                if(isset($_COOKIE['resource_id_'.$i]) && $_COOKIE['resource_id_'.$i]!='')
                {
                    $resource_ids[]  = $_COOKIE['resource_id_'.$i];
                    $row_number[] =$i;
                   
                }
                else
                {
                    continue;
                }
                
            }

            if(!empty($resource_ids) && count($resource_ids)>0) 
            {
               
                  $compareList = Sql::getCompareListing(implode(',',$resource_ids));
                  
 return view('front.compare_listing',compact('compareList','row_number','footer_text','meta_data','header_author','header_searching_tag'));

            }
            else
            {
               
                return Redirect::to('search');
            }
         }
        else
        {
            
            header('Location: '.url());
            return true;
        }
      

 
    }
    
    public function listingReview(Request $request)
    {
    header("Content-Type: text/html; charset=utf-8", TRUE);
    header("Accept-Encoding: gzip, deflate");
    header("Expires: Sat, 01 Jan 2000 00:00:00 GMT");
    header("Cache-Control: no-store, no-cache, must-revalidate");
    header("Cache-Control: post-check=0, pre-check", FALSE);
    header("Pragma: no-cache");
        $resourc_id = $request->id;
        $item_type = $request->type;
        if(is_numeric($resourc_id) && $resourc_id>0)
        {
            $footer_text = Sql::footerText();
        
            $params['type'] = 'Home Page';
            $meta_data = Sql::HomePageSummerSale($params);
            
            $header_author = Sql::HomePageHeaderAuthor();
            
            $header_searching_tag = Sql::HomePageHeaderSearchingTag();
            $featured_images = Sql::getHomePageFeaturedRental();
            
            $listingData = Sql::getListingTitle($resourc_id);

            $member_id=Functions::sess_getAccountIdFromSession();

            if (Functions::sess_getAccountIdFromSession()) 
            {
            $reviewerAcc = new Account();
            $reviewerAcc->Account($member_id);
            $reviewerInfo = new Contact();
            $reviewerInfo->Contact($member_id);

            $reviewerProfile = new Profile();
            $reviewerProfile->Profile($member_id);

             }
            $reviewer_name='';
            $reviewer_email='';
            $reviewer_location='';
    
    $imagedata = $this->captcha();

            return view('front.review',compact('listingData','item_type','footer_text','meta_data','header_author','header_searching_tag','member_id','reviewerAcc','reviewerInfo','reviewerProfile','reviewer_name','reviewer_email','reviewer_location','imagedata'));

        }
        else
        {
            hearder('Location:'.url());
            return true;
        }
        
    }
    
    public function listingDetail(Request $request)
    {
       
        $resourc_id = $request->id;
        
        if(is_numeric($resourc_id) && $resourc_id>0)
        {
            $footer_text = Sql::footerText();
        
            $params['type'] = 'Home Page';
            $meta_data = Sql::HomePageSummerSale($params);
            
            $header_author = Sql::HomePageHeaderAuthor();
            
            $header_searching_tag = Sql::HomePageHeaderSearchingTag();
            $featured_images = Sql::getHomePageFeaturedRental();
            
            $listingData = Sql::getListingDetail($resourc_id);
            $listing_title = $listingData[0]['title'];
            $account_id = $listingData[0]['account_id'];
            if(isset($account_id) && !empty($account_id))
            {
                $account_info = MainSql::AccountInfo($account_id);
            }
            else
            {
                $account_info=array();
            }
            
            return view('front.listing_details',compact('listingData','account_info','footer_text','meta_data','header_author','header_searching_tag','listing_title'));

        }
        else
        {
            hearder('Location:'.url());
            return true;
        }
        
    }
    
    public function getSpecials($special='')
    {
       
        $sp_id = $special;
        
        if(is_numeric($sp_id) && $sp_id>0)
        {
            $page_offset=1;
            //if (!$request->page_offset)
//    		{
//    			$page_offset = 1;
//    		}
//    		else
//    		{
//    			$page_offset = $request->page_offset;
//    		}
            $per_page = 100;
            $offset = $page_offset?$page_offset:1;
            
            $footer_text = Sql::footerText();
        
            $states = Sql::AdvanceSeachStates();
            if(!empty($states))
            {
                $statesData = MainSql::AdvanceSeachStatesData(implode(',',$states));
            }
            else
            {
                $statesData=array();
            }
            
            $meta_sp = Sql::getSpecialTitle($sp_id);

            $name_special = $meta_sp[0]->name;
            $params['type'] = 'Home Page';
            $meta_data = Sql::HomePageSummerSale($params);
            if(isset($meta_sp) && !empty($meta_sp))
            {
               $meta_data[0]->title=$meta_sp[0]->title;
               $meta_data[0]->description=$meta_sp[0]->meta_description;
               $meta_data[0]->keywords=$meta_sp[0]->meta_keys;
               
               
            }
            
            $header_author = Sql::HomePageHeaderAuthor();
            
            $header_searching_tag = Sql::HomePageHeaderSearchingTag();
            $featured_images = Sql::getHomePageFeaturedRental();
            
            
            $result = Sql::getSpecials($sp_id, $offset, $per_page);
           
            $results = $result['result'];
            
			$result_count = $result['total'][0]->total;
			$result_pages = $result_count/$per_page;
			if($offset + $per_page < $result_count) {
				$next_offset = $offset + $per_page;
			} else {
				$next_offset = 0;
			}
			$current_offset = $offset;
			$per_page = $per_page;
            
            view('front.special_listing',compact('results','result_pages','page_offset',
            'result_count','next_offset','current_offset','per_page','statesData',
            'featured_images','sp_id','name_special','footer_text','meta_data','header_author','header_searching_tag'));
            die;
        }
        else
        {
            hearder('Location:'.url());
            return true;
        }
        
    }
    
    public function randomizer()
    {
        Sql::UpdateRandNum();
    }

    
    public function AdvancedsearchCheckform()
    {
        $aux = explode(",",config('params.FRIENDLY_SEARCH_IDENTIFIERS'));
        
    	$identifiers = array();
    	foreach ($aux as $key => $value) {
    		$identifiers[$value] = '';
    	}
    
    	# ----------------------------------------------------------------------------------------------------
    	# breaking down the $_GET into $identifiers
    	# ----------------------------------------------------------------------------------------------------
    
    	//Module
    //	if ($_GET['module_key'])  {
    		$module = '';
    	//	unset($_GET['module_key']);
//    	}
//    
    	//Locations
    	if ( isset($_GET['location_3']) &&  $_GET['location_3'] != '' && !isset($_GET['location_1']) ) $_GET['location_1'] = 1;
    	$i = array(1,3,4,5);
        $locArray=array();
    	foreach ($i as $level) {
    		if ( isset($_GET['location_'.$level])  && $_GET['location_'.$level]) {
    			$obj = "Location".$level;

                 
                $locArray['location_'.$level] = $_GET['location_'.$level];
    			$locObj = $this->$obj($_GET['location_'.$level]);
    			$identifiers['locations'][$level] = $locObj;
    		} else {
    			break;
    		}
    	}
       
    	if( isset($_GET['sleeps_min']) || isset($_GET['sleeps_max'])) {
    	   Session::put('sleeps_min',$_GET['sleeps_min']);
           Session::put('sleeps_max',$_GET['sleeps_max']);               	   
    		$identifiers['sleeps'][] = ($_GET['sleeps_min'] ? $_GET['sleeps_min'] : 1).($_GET['sleeps_max'] ? "-".$_GET['sleeps_max'] : '');
    	}
        else
        {
            Session::forget('sleeps_min');
            Session::forget('sleeps_max');
        }
    	if( isset($_GET['sleeps'])) {
    	   Session::put('sleeps',$_GET['sleeps']);    	   
    		$identifiers['sleeps'][] = ($_GET['sleeps'] ? $_GET['sleeps'] : '');
    	}
        else
        {
            Session::forget('sleeps'); 
        }
    	if( isset($_GET['bedroom_min']) || isset($_GET['bedroom_max'])) {
    	   Session::put('bedroom_min',$_GET['bedroom_min']);
           Session::put('bedroom_max',$_GET['bedroom_max']);    	   
    		$identifiers['bedroom'][] = ($_GET['bedroom_min'] ? $_GET['bedroom_min'] : 1).($_GET['bedroom_max'] ? "-".$_GET['bedroom_max'] : '');
    	}
        else
        {
            Session::forget('bedroom_min');
            Session::forget('bedroom_max'); 
        }
    	if( isset($_GET['bedroom'])) {
    	   Session::put('bedroom',$_GET['bedroom']);    	   
    		$identifiers['bedroom'][] = ($_GET['bedroom'] ? $_GET['bedroom'] : '');
    	}
        else
        {
            Session::forget('bedroom');
        }
    	if( isset($_GET['bathroom_min']) || isset($_GET['bathroom_max'])) {
    	   Session::put('bathroom_min',$_GET['bathroom_min']);
           Session::put('bathroom_max',$_GET['bathroom_max']);    	   
    		$identifiers['bathroom'][] = ($_GET['bathroom_min'] ? $_GET['bathroom_min'] : 1).($_GET['bathroom_max'] ? "-".$_GET['bathroom_max'] : '');
    	}
        else
        {
            Session::forget('bathroom_min');
            Session::forget('bathroom_max');
        }
    	if(isset( $_GET['bathroom']) ) {
    	   Session::put('bathroom',$_GET['bathroom']);    	   
    		$identifiers['bathroom'][] = ($_GET['bathroom'] ? $_GET['bathroom'] : '');
    	}
        else
        {
            Session::forget('bathroom');
        }
    	if( isset($_GET['checkin']) || isset($_GET['checkout'])) {
    	   Session::put('checkin',str_replace('/', '-', $_GET['checkin']));
           Session::put('checkout',str_replace('/', '-', $_GET['checkout']));               	   
    		$identifiers['checkin'][] = str_replace('/', '-', $_GET['checkin']);
    		$identifiers['checkout'][] = str_replace('/', '-', $_GET['checkout']);
    	}
        else
        {
            Session::forget('checkin');
            Session::forget('checkout');
        }
    	if( (isset($_GET['rate_min']) && $_GET['rate_min']!='') && (isset($_GET['rate_max']) && $_GET['rate_max']!='') ) {
    	   Session::put('rate_min',$_GET['rate_min']);
           Session::put('rate_max',$_GET['rate_max']);
           Session::put('period',$_GET['period']);                          	   
   		   $identifiers['rate'][] = $_GET['rate_min'] . "-" . $_GET['rate_max'];
    	   $identifiers['period'][] = $_GET['period'];
    	}
        else
        {
            Session::forget('rate_min');
            Session::forget('rate_max');
            Session::forget('period');
        }
    
    	foreach ($_GET as $key => $value) {
    
    		//Amenities
    		if (strpos($key, "amenity_")!==false) {
    			if ( $value != '' ) {
    				if ( $value == 'no' ) $identifiers['amenity'][] = 'no' . $this->string_ucwords( str_replace("amenity_", "", $key ) );
    				else $identifiers['amenity'][] = str_replace("amenity_", "", $key);
    			}
    
    		//Features
    		} elseif (strpos($key, "feature_")!==false) {
    			if ( $value != '' ) {
    				if ( $value == 'no' ) $identifiers['feature'][] = 'no' . $this->string_ucwords( str_replace("feature_", "", $key ) );
    				else $identifiers['feature'][] = str_replace("feature_", "", $key);
    			}
    			
    		//Activities
    		} elseif(strpos($key, "activity_")!==false) {
    			if ( $value != '' ) {
    				if ( $value == 'no' ) $identifiers['activity'][] = 'no' . $this->string_ucwords( str_replace("activity_", "", $key ) );
    				else $identifiers['activity'][] = str_replace("activity_", "", $key);
    			}
    			
    		//Category
    		} elseif($key == 'category_id') {
    			$identifiers['category'][] = $value;
    
    		//Rental ID
    		} elseif($key == 'rental_id') {
    		  Session::put('id',$value);    		  
    			$identifiers['id'][] = $value;
    
    		//Development Name
    		} elseif($key == 'development_name') {
    		      		  
    			$identifiers['development_name'][] = urlencode($value);
    
    		//Distance to Beach
    		} elseif($key == 'distance_beach') {
    			$identifiers['distance_beach'][] = $value;
    
    		//Property Type
    		} elseif($key == 'property_type') {
    			$identifiers['property_type'][] = $value;
    
    		//dist
    		} elseif($key == 'dist') {
    			$identifiers['dist'][] = $value;
    
    		//zip
    		} elseif($key == 'zip') {
    			$identifiers['zip'][] = $value;
    
    		//view
    		} elseif($key == 'view') {
    			$identifiers['view'][] = urlencode($value);
    
    		//Keyword
    		} elseif($key == 'keyword') {
    			$value = trim($value);
    			$value = str_replace('  ', ' ', $value);
                Session::put('keyword',urlencode($value));
    			$identifiers['keyword'][] = urlencode($value);
    			
    		//address
    		} elseif($key == 'address') {
    			$identifiers['address'][] = urlencode($value);
    
    		//where
    		} elseif($key == 'where') {
    			$identifiers['where'][] = urlencode($value);
    
    		//match
    		} elseif($key == 'match') {
    			$identifiers['match'][] = $value;
    		}
    
    	}
    
    	# ------------------------------------------------------------------------------------
    	# Putting together the URL from $identifiers
    	# ------------------------------------------------------------------------------------
    	$empty_search = true;
        $redirect_url = '';
    	if (isset($identifiers['locations']) && !empty($identifiers['locations']))  {

            if(implode("/", $identifiers['locations'])=='united-states')
            {
                $locs = '';
            }
            else
            {
                
                $locs = '/'.implode("/", $identifiers['locations']);
            }
    		
            
    		$redirect_url.= "".$locs;
    		unset($identifiers['locations']);
    		$empty_search = false;
    	}
        
    	foreach ($identifiers as $key => $value) {
    		if (is_array($value) ) {
    			$value = array_filter($value);
   
    			if(!empty($value)) {
    				$redirect_url.="/".$key."-".implode("-", $value);
    				$empty_search = false;
    			}
    		}
    	}
    	if ($empty_search == false) {
    		$return_url = url().($module ? '/'.$module : '').$redirect_url;
    	} else {
    		$return_url = url()."/".($module ? $module."/" : '').config('params.FRIENDLY_SEARCH_ALIAS');
    	}
        $resultArray=array();
        Session::put('locations',$locArray);
        $resultArray['locArray'] = $locArray;
        $resultArray['return_url'] = $return_url;
        if(trim($return_url)==url())
        {
            $resultArray['return_url'] = $return_url.'/'.config('params.FRIENDLY_SEARCH_ALIAS');
            return $resultArray;
        }
        else
        {
            return $resultArray;
        }
    	
        
    	exit;
    }
    
    function Location1($var='') {
			if (is_numeric($var) && ($var)) {
			   $sql = "SELECT friendly_url FROM Location_1 WHERE id = $var";
			$row = 	MainSql::getLocation($sql);
				return $row[0]->friendly_url;
			} else {
                if (!is_array($var)) {
                    $var = array();
                }
				return  $var;
			}
		}
        
        function Location1friendlyUrl($var='') {
			if (($var)) {
			   $sql = "SELECT id FROM Location_1 WHERE friendly_url = '$var'";
			$row = 	MainSql::getLocation($sql);
				return $row[0]->id;
			} else {
                if (!is_array($var)) {
                    $var = array();
                }
				return  $var;
			}
		}
        
        
        
        
         function Location2($var='') {
			if (is_numeric($var) && ($var)) {
			   $sql = "SELECT friendly_url FROM Location_2 WHERE id = $var";
			$row = 	MainSql::getLocation($sql);
				return $row[0]->friendly_url;
			} else {
                if (!is_array($var)) {
                    $var = array();
                }
				return  $var;
			}
		}
        
         function Location3($var='') {
			if (is_numeric($var) && ($var)) {
			   $sql = "SELECT friendly_url FROM Location_3 WHERE id = $var";
			$row = 	MainSql::getLocation($sql);
				return $row[0]->friendly_url;
			} else {
                if (!is_array($var)) {
                    $var = array();
                }
				return  $var;
			}
		}
        
        function Location3friendlyUrl($var='') {
			if (($var)) {
			   $sql = "SELECT id FROM Location_3 WHERE friendly_url = '$var'";
			$row = 	MainSql::getLocation($sql);
            if($row)
				return $row[0]->id;
        else
        {
            return 0;
        }
			} else {
                return 0;
			}
		}
        
        
         function Location4($var='') {
			if (is_numeric($var) && ($var)) {
			   $sql = "SELECT friendly_url FROM Location_4 WHERE id = $var";
			$row = 	MainSql::getLocation($sql);
				return $row[0]->friendly_url;
			} else {
                if (!is_array($var)) {
                    $var = array();
                }
				return  $var;
			}
		}
        function Location4friendlyUrl($var='') {
			if (($var)) {
			   $sql = "SELECT id FROM Location_4 WHERE friendly_url = '$var'";
			$row = 	MainSql::getLocation($sql);
				  if($row)
				return $row[0]->id;
        else
        {
            return 0;
        }
			} else {
                if (!is_array($var)) {
                    $var = array();
                }
				return  $var;
			}
		}
        
        
         function Location5($var='') {
			if (is_numeric($var) && ($var)) {
			   $sql = "SELECT friendly_url FROM Location_5 WHERE id = $var";
			$row = 	MainSql::getLocation($sql);
				return $row[0]->friendly_url;
			} else {
                if (!is_array($var)) {
                    $var = array();
                }
				return  $var;
			}
		}
        
        function string_ucwords($str, $charset = 'UTF-8'
        ) {
		if (isset($str) || is_numeric($str)) {
			if (function_exists('mb_convert_case')) {
				$strUCWords = mb_convert_case($str, MB_CASE_TITLE, $charset);
			} else {
				$strUCWords = ucwords($str);
			}
		} else {
			$strUCWords = false;
		}
		return $strUCWords;
	}
    
    function getNjlistings()
    {
        $footer_text = Sql::footerText();
        
        $params['type'] = 'Home Page';
        $meta_data = Sql::HomePageSummerSale($params);
        $header_author = Sql::HomePageHeaderAuthor();
        
        $header_searching_tag = Sql::HomePageHeaderSearchingTag();
        $cityListingData = Sql::citiesListing(4, 'listing', 'title', 100);
        $cities=array();
        foreach($cityListingData as $key=>$city)
        {
            $urlarray = explode("/", $city->url);
            $cityt['full_url'] = $city->url;
            $cityt['total'] = $city->total;
            $cityt['title'] = $city->title;
            $cityt['url'] = $urlarray[1].".html";
            $cities[]=$cityt; 
        }
       
     
     
        
        
        return view('front.nj_listings',compact('cities','footer_text','meta_data','header_author','header_searching_tag'));
        die;
    }

    function loadmap(Request $request)
    {
        if($request->_token==csrf_token())
    {   
     header("Content-Type: text/html; charset= UTF-8", TRUE);
     header("Accept-Encoding: gzip, deflate");
     header("Expires: Sat, 01 Jan 2000 00:00:00 GMT");
     header("Cache-Control: no-store, no-cache, must-revalidate");
     header("Cache-Control: post-check=0, pre-check", FALSE);
     header("Pragma: no-cache");
     $module=$request->module;

   

     if ($request->action == "summary") {

          echo "<script type=\"text/javascript\">loadToolTip('summary_ajax');</script>";

           if ($module == 'listing') {

    
            //d(url('front/view_listing_summary'));die;
        $params=array();
        $requestParams='';
        $requestParams = $request->all; 
        $otherLinks = str_replace("+", "-",explode('/', $requestParams));
        //d($otherLinks);die;
        ///////
        $locationsArray = Session::get('locations');
        $loc3 = isset($locationsArray['location_3'])?$locationsArray['location_3']:0;
        $loc4 = isset($locationsArray['location_4'])?$locationsArray['location_4']:0;
        $rate_array=array();
        $checkin_aux=array();
        $checkout_aux=array();
        $features_array=array();
        $amenities_array=array();
        $currency= config('params.currency');

        $params['location_3']=0;
        $params['location_1']=0;
        $params['location_4']=0;

        $params['feature_smokingpermitted']=0;
        $params['feature_petsallowed']=0;
        $params['feature_wheelchairaccess']=0;
        $params['feature_pool']=0;
        $params['feature_elevator']=0;
        $params['feature_boatslip']=0;
        $params['amenity_internet']=0;
        
        $params['rental_id']=0;
        $params['view']=0;
        $params['address']=0;
        $params['sleeps_number']  = 0;
        $params['sleeps_condition'] = 0;
        $params['bedroom_number']  = 0;
        $params['bedroom_condition'] = 0;
        $params['bathroom_number']  = 0;
        $params['bathroom_condition'] = 0;
        $params['distance_beach']=0;
        $params['property_type']=0;
        $params['development_name']=0;
        $params['keyword']  = 0;
        $params['checkin']=0;
        $params['checkout']=0;
        $params['period']=0;
        $params['rate_min_number']=0;
        $params['rate_max_number']=0;
        
        $params['orderByLocation_3']=0;
        $params['orderByLocation_4']=0;
        
        $params['rating']=0;
        $page_offset = 0;
        $params['rental_id']  =0;
        $params['keyword']  =$request->id;
        $result = Sql::SearchingResult($params, 0,100);
        $results = $result['result'];
        //d($results);die;keyword-4705

         foreach($results as $key=>$val){ 

            $return='<div id="listing_summary_'.$val['rental_id'].'" class="result_summary summary-small summary ">
            <div class="summary__img col-sm-3">
          <a href="'.url('listing/'.$val['rental_id'].'.html').'" class="image"><img src="'.config('params.imagesPath').$val['prefix'].'photo_'.$val['image_id'].".".strtolower($val['type']).'" alt="" ></a>';
               if(isset($val['listing_choice']) && $val['listing_choice']>0 ){ 
           $return.='<div class="listing-tag-deal">
                 <div class="name-tag-deal">
                  <a href="'.url('listing/'.$val['rental_id']).'#specials"> </a>
                                    </div>
                                </div>';
                   }
               $return.='</div><div class="summary__dtl col-sm-6">
                 <h4><a href="'.url('listing/'.$val['rental_id'].'.html').'">'.$val['title'].', '.$val['address'].', '.$val['bedroom'].' Bedrooms, '.$val['bathroom'].' Bathrooms, '.$val['sleeps'].' Sleeps</a></h4>
                                   <div class="summary-address">
                                        <address>
                <span>Location: '.$val['address'].' '.$val['address2'].', '.$val['location_4_title'].', '.$val['location_3_title'].', '.$val['zip_code'].'</span>
                                        </address>
                                    </div>
             <p>'.$val['bedroom'].' BR, '.$val['bathroom'].' BA, Sleeps '.$val['sleeps'].'</p>
             <p>Rental ID# <a href="'.url('listing/'.$val['rental_id'].'.html').'">'.$val['rental_id'].'</a></p>
 <p class="quick-info"><a  style="cursor: pointer;" onclick="view_quickInfo('.(isset($key)?$key:0).','.$val['rental_id'].') " ><i class="fa fa-chevron-right"></i> Quick Property Info</a></p>
           <div class="review"><div class="rate">
                   <div class="rate-stars">
                  <div class="stars-rating color-6">
                      <div class="rate-'.$val['rating'].'"></div>
                 </div>
                                            </div>
                                            <p><a rel="nofollow" href="" data-toggle="modal" data-target="#reviewPropertyModal" class="iframe fancy_window_review">Be the first to review this property!</a></p>
                                            
                                          
                                                  </div>
                                                </div>
                                            
                                        </div>
                                    </div>
                                </div>
         <div class="summary__atc col-sm-3 text-right">';
         if(isset($periodForm) && $periodForm=='day'){
         if($val['min_day'] && $val['min_day']>0)
          {
          $return.='<p><strong>'.$currency[config('params.currency_default')]['symbol'].' '.$val['min_day'].' - '.$currency[config('params.currency_default')]['symbol'].' '.$val['max_day'].'</strong><br> per day</p>';
         }
         elseif($val['min_week'] && $val['min_week']>0){
           $return.='<p><strong>'.$currency[config('params.currency_default')]['symbol'].' '. $val['min_week'].' - '.$currency[config('params.currency_default')]['symbol'].' '.$val['min_week'].'</strong><br> per day</p>';  
          }
           }
           elseif(isset($periodForm) && $periodForm=='week')
             {
             if($val['min_week'] && $val['min_week']>0)
             {
             $return.='<p><strong>'.$currency[config('params.currency_default')]['symbol'].' '.$val['min_week'].' - '.$currency[config('params.currency_default')]['symbol'].' '.$val['max_week'].'</strong><br> per week</p> '; 
            }
           elseif($val['min_week'] && $val['min_week']>0){
          $return.='<p><strong>'.$currency[config('params.currency_default')]['symbol'].' '.$val['min_week'].' - '.$currency[config('params.currency_default')]['symbol'].' '.$val['min_week'].'</strong><br> per week</p>';   
             }
               }
           elseif(isset($periodForm) && $periodForm=='month')
            {
            if(isset($val['min_month']) && $val['min_month']>0)
            { 
            $return.='<p><strong>'.$currency[config('params.currency_default')]['symbol'].' '.$val['min_month'].' - '.$currency[config('params.currency_default')]['symbol'].' '.$val['max_month'].'</strong><br> per month</p> '; 
             }
             elseif($val['min_week'] && $val['min_week']>0){
            $return.='<p><strong>'.$currency[config('params.currency_default')]['symbol'].' '.$val['min_week'].' - '.$currency[config('params.currency_default')]['symbol'].' '.$val['min_week'].'</strong><br> per month</p> '; 
               }
            }
             elseif(isset($periodForm) && $periodForm=='season')
               {
               if($val['min_season'] && $val['min_season']>0)
                    {
                $return.='<p><strong>'.$currency[config('params.currency_default')]['symbol'].' '.$val['min_season'].' - '.$currency[config('params.currency_default')]['symbol'].' '.$val['max_season'].'</strong><br> per season</p>';     
                       }
               elseif($val['min_week'] && $val['min_week']>0){
                 }
            }
            else
             {
             if( $val['min_week'] && $val['min_week']>0)
               {
                 $return.='<p><strong>'.$currency[config('params.currency_default')]['symbol'].' '.$val['min_week'].' - '.$currency[config('params.currency_default')]['symbol'].' '.$val['max_week'].'</strong><br> per week</p>';
             }
         } 
                                                                  
         if($val['required_stay']){
                        
          $return.='<p>'.$val['required_stay'].' minimum stay</p>';
           }
             
         $return.='<p><a class="btn btn-primary" href="'.url('listing/'.$val['rental_id'].'.html').'">View Details</a></p>
     <div class="compare-featured fnc-fom">
       <div class="inputgroup"><input id="compare_'.(isset($key)?$key:0).'" value="'.$val['rental_id'].'" type="checkbox" name="comparison_chk" onclick="compare_listing('.(isset($result_count)?$result_count:1).','.(isset($key)?$key:0).','.$val['rental_id'].','.str_replace("'", '', str_replace('"', '', $val['title'])).');" class="form-control">
                                            <label> Compare </label>
                                        
                                    </div>
                                </div>
                            
                            </div>';

         }


         echo  $return;
         exit;

           }

     }
     

      if ($module == 'listing') {


       //$searchReturn = search_frontListingSearch($_GET, "listing_results");
  $sql = "SELECT id, title, latitude, longitude FROM listing WHERE latitude != '' AND longitude != '' AND status='A'";
    
    }

     $result = Sql::fetch($sql);
      foreach ($request->all() as $name => $value){
        if ($name != "screen" && $name != "letter" && $name != "url_full" && $name != "latitudeResults" && $name != "longitudeResults" && $name != "latitude" && $name != "longitude" && $name != "se_latitude" && $name != "se_longitude" && $name != "where" && $name != "zip" && $name != "dist" && $name != "location_1"&& $name != "location_2"&& $name != "location_3"&& $name != "location_4"&& $name != "location_5" && $name != "filter_location_1"&& $name != "filter_location_2"&& $name != "filter_location_3"&& $name != "filter_location_4"&& $name != "filter_location_5"){
            if ( $name == "keyword" || $name == "where" ) $array_search_params[] = $name."=".urlencode($value);            
            else $array_search_params = '';//$name."=".$value

        }
    }
    $url_search_params = $array_search_params;//$url_search_params [] implode("&", $array_search_params)


    $returnScript = "";
    if (count($result)) {
        
        /**
        * Get pointer to theme
        */
        $pointer_path = url('custom/domain_1/theme/default');
       
              
        $returnScript .= "var points = [";
        $totalPoints = 0;
        foreach ($result as $row) {
            $htmlCluster = "<a href=\"javascript: void(0);\" onclick=\"showSummary({$row->id});\">".addslashes($row->title)."</a>";
            $returnScript .= "['".$row->latitude."', '".$row->longitude."', ".$row->id.", '{$htmlCluster}'],";
            $totalPoints++;
        }

        $returnScript = substr($returnScript, 0, -1);
        
        $returnScript .= "];";
        
        $returnScript .= "  var totalPoints = ".$totalPoints.";
            
                            var clusterInfoWindow = new google.maps.InfoWindow();
            
                            var styles = [[{
                                    url: '".$pointer_path."/images/markers/m1.png',
                                    height: 27,
                                    width: 27,
                                    textColor: '#ffffff',
                                    textSize: 10
                                }, {
                                    url: '".$pointer_path."/images/markers/m2.png',
                                    height: 29,
                                    width: 29,
                                     textColor: '#ffffff',
                                    textSize: 10
                                }, {
                                    url: '".$pointer_path."/images/markers/m3.png',
                                    height: 34,
                                    width: 34,
                                    textColor: '#ffffff',
                                    textSize: 10
                                }, {
                                    url: '".$pointer_path."/images/markers/m4.png',
                                    height: 38,
                                    width: 38,
                                    textColor: '#ffffff',
                                    textSize: 10
                                }, {
                                    url: '".$pointer_path."/images/markers/m5.png',
                                    height: 46,
                                    width: 46,
                                    textColor: '#ffffff',
                                    textSize: 10
                                }]];
                                
                            var markerClusterer = null;
                            var map = null;";

                
        $returnScript .= "
            
            function showSummary(id) {
                var module = '".$module."';
                var current_id = $('#summarymap_current_id').val();
                var parameters = {
                    \"module\": module,
                    \"id\": id,
                    \"_token\":'".csrf_token()."',
                    \"action\": 'summary'
                };
                $(\"#summary_map\").css('display', '');
                $('html, body').animate({
                    scrollTop: $('#summary_map').offset().top
                }, 'slow');

                var inputText = '<input type=\'hidden\' id=\'summarymap_current_id\' value=\''+id+'\'>';

                if (current_id != id) {
                    $(\"#summary_map\").html('<p class=\"text-center\">Wait, Loading...</p>');
                    if ($('#summary_map_content_'+id).length && $('#summary_map_content_'+id).html()) {
                        $(\"#summary_map\").html($('#summary_map_content_'+id).html() + inputText);
                        $('#summary_map_content_'+id).html('');
                    } else {
                        $.post('".url('loadmap')."', parameters, function (ret) {
                            $(\"#summary_map\").html(ret + inputText);
                        });
                    }
                    $('.summary-price a, .button-call img, .button-send img, .share-social img').tooltip({
                        animation: true
                    });

                }
        
            }
        
            function reloadPins(){
                if (markerClusterer) {
                    markerClusterer.clearMarkers();
                }

                var markers = [];
                var bounds = new google.maps.LatLngBounds();

                for (i = 0; i < points.length; i++) {
                    var latLng = new google.maps.LatLng(points[i][0], points[i][1]);
                    var marker = new google.maps.Marker({
                        position: latLng,
                        draggable: true,
                        draggable: false,
                        icon: points[i][4]
                    });
                    
                    marker.set('id', points[i][2]);
                    marker.set('module', '$module');
                    marker.set('html', points[i][3]);
                    
                    google.maps.event.addListener(marker, 'click', function() {
                        var id = this.get('id');
                        showSummary(id);
                    });

                    markers.push(marker);
                    //bounds.extend(latLng);
                    //map.fitBounds(bounds);
 
                }

                markerClusterer = new MarkerClusterer(map, markers, {
                    maxZoom: null,
                    gridSize: null,
                    styles: styles[0]
                });
            }

            function refreshMap() { 
                if (markerClusterer) {
                    markerClusterer.clearMarkers();
                }

                var markers = [];
                var bounds = new google.maps.LatLngBounds();
                                                            
                for (i = 0; i < points.length; i++) {
                    var latLng = new google.maps.LatLng(points[i][0], points[i][1]);
                    var marker = new google.maps.Marker({
                        position: latLng,
                        draggable: true,
                        draggable: false
                    });
                    
                    marker.set('id', points[i][2]);
                    marker.set('module', '$module');
                    marker.set('html', points[i][3]);
                    
                    google.maps.event.addListener(marker, 'click', function() {
                        var id = this.get('id');
                        showSummary(id);
                    });

                    markers.push(marker);
                    bounds.extend(latLng);
                    map.fitBounds(bounds);
 
                }
                
                ";
                
        $returnScript .= "
                    
                markerClusterer = new MarkerClusterer(map, markers, {
                                        maxZoom: null,
                                        gridSize: null,
                                        styles: styles[0]
                                    });
                                    
                markerClusterer.openWindowMarkers = function() { return multiChoice(markerClusterer); }

                map.fitBounds(bounds);

            }
            
            function multiChoice(mc) {
                var cluster = mc.clusters_;
                var htmlContent = '';
                // if more than 1 point shares the same lat/long
                // the size of the cluster array will be 1 AND
                // the number of markers in the cluster will be > 1
                // REMEMBER: maxZoom was already reached and we can't zoom in anymore
                if (cluster.length == 1 && cluster[0].markers_.length > 1) {
                    var markers = cluster[0].markers_;
                    for (var i = 0; i < markers.length; i++) {
                        htmlContent = htmlContent + markers[i]['html'] + '<br />';
                    }
                    clusterInfoWindow.setContent(htmlContent);
                    clusterInfoWindow.open(map, markers[0]);
                    return false;
                }

                return true;
            }

            function DragListener(map) {
                
                google.maps.event.addListener(map, 'dragend', function(evt){
                    var bounds = map.getBounds();
                    var ne = bounds.getNorthEast(); // LatLng of the north-east corner
                    var sw = bounds.getSouthWest(); // LatLng of the south-west corder
                    var nw = new google.maps.LatLng(ne.lat(), sw.lng());
                    var se = new google.maps.LatLng(sw.lat(), ne.lng());

                    //console.log( ne + ', ' + sw + ', ' + nw + ', ' + se );
                    $('#loadingMap').fadeIn();
                    $.ajax({
                       type: \"GET\",
                       url: \"".url()."/results_ajax?latitude=\"+nw.lat()+\"&longitude=\"+nw.lng()+\"&se_latitude=\"+se.lat()+\"&se_longitude=\"+se.lng()+\"&".$url_search_params."\",
                       success: function(msg){ 
                            $('#resultsInfo_map').css( 'display', 'none' );
                            $('#summary_map').html(msg); 
                            $('#loadingMap').fadeOut(); 
                            $('#where').val('');
                            $('#search-info-results').css('display', 'none');
                       }
                    });
                });
            }

            function ZoomChangeListener(map) {

                google.maps.event.addListener(map, 'zoom_changed', function(evt){
                    var bounds = map.getBounds();
                    var ne = bounds.getNorthEast(); // LatLng of the north-east corner
                    var sw = bounds.getSouthWest(); // LatLng of the south-west corder
                    var nw = new google.maps.LatLng(ne.lat(), sw.lng());
                    var se = new google.maps.LatLng(sw.lat(), ne.lng());

                    //console.log( ne + ', ' + sw + ', ' + nw + ', ' + se );
                    $('#loadingMap').fadeIn();
                    $.ajax({
                       type: \"GET\",
                       url: \"".url()."/results_ajax?latitude=\"+nw.lat()+\"&longitude=\"+nw.lng()+\"&se_latitude=\"+se.lat()+\"&se_longitude=\"+se.lng()+\"&".$url_search_params."\",
                       success: function(msg){
                            $('#summary_map').html(msg); 
                            $('#loadingMap').fadeOut(); 
                            $('#where').val('');  
                            $('#search-info-results').css('display', 'none');
                       }
                    });
                });
            }
         

            function initialize() {

                var myOptions = {
                    scrollwheel: ".("n"== "y" ? "true" : "false").", 
                    scaleControl: true,   
                    zoom: 15,
                    center: new google.maps.LatLng(0, 0),
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                }

                map = new google.maps.Map(document.getElementById('resultsMap'), myOptions);

                // Listener to get pos on drag map
                // DragListener(map);

                refreshMap();

                // Listener to get pos on zoom change
                // setTimeout(function(){ZoomChangeListener(map)}, 5000);
            }

            ";
    }
    
    echo $returnScript;

    }
    else
    {
        return false;
    }

}
    
    

    function results_ajax(Request $request)
    {
        $search_lock = false;
        //$if(!$request->screen) 
        $screen = 1;
        if (!$search_lock) {

         $aux_items_per_page = (isset($_COOKIE["listing_results_per_page"]) ? $_COOKIE["listing_results_per_page"] : 10);


        $sql = "SELECT id, title, latitude, longitude FROM listing WHERE latitude != '' AND longitude != '' ORDER BY id LIMIT ".($screen == 1 ? '0, '.$aux_items_per_page : ($screen - 1) * $aux_items_per_page.", ".$aux_items_per_page);

    $result = Sql::fetch($sql);

        if (count($result)>0) {
                $returnScript = "points = [";
                $totalPoints = 0;
                foreach ($result as $row) {
                   

         //$catImagePath = "http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|".$catColorCode;
                    $catImagePath = "http://mt.googleapis.com/vt/icon/name=icons/spotlight/spotlight-poi.png&scale=1";

              $htmlCluster = "<a href=\"javascript: void(0);\" onclick=\"showSummary({$row->id});\">".addslashes($row["title"])."</a>";
                    $returnScript .= "['".$row->latitude."', '".$row->longitude."', ".$row->id.", '{$htmlCluster}', '".$catImagePath."'],";
                }
                $returnScript = substr($returnScript, 0, -1);
        
                $returnScript .= "];";

 }
  ?>
            <script type="text/javascript">
                <?php echo $returnScript ?>
                reloadPins();
            </script>
            <?php
        }
    }


    public function listing_emailform(Request $request)
    {
    header("Content-Type: text/html; charset=utf-8", TRUE);
    header("Accept-Encoding: gzip, deflate");
    header("Expires: Sat, 01 Jan 2000 00:00:00 GMT");
    header("Cache-Control: no-store, no-cache, must-revalidate");
    header("Cache-Control: post-check=0, pre-check", FALSE);
    header("Pragma: no-cache");   

          $id=$request->id;
          if(is_numeric($id) && $id>0)
            {

                $footer_text = Sql::footerText();
            
                $params['type'] = 'Home Page';
                $meta_data = Sql::HomePageSummerSale($params);
                
                $header_author = Sql::HomePageHeaderAuthor();
                
                $header_searching_tag = Sql::HomePageHeaderSearchingTag();
                $featured_images = Sql::getHomePageFeaturedRental();
                
                $listingData = Sql::getListingDetail($id);
                $account_id = $listingData[0]['account_id'];
                if(isset($account_id) && !empty($account_id))
                {
                    $account_info = MainSql::AccountInfo($account_id);
                }
                else
                {
                    $account_info=array();
                }
              
                return view('front.listing_emailform',compact('listingData','account_info','footer_text','meta_data','header_author','header_searching_tag'));

            }
            else
            {
                hearder('Location:'.url());
                return true;
            }
    }
    public function listing_emailformstore(Request $request)
    {
         if($request->_token==csrf_token())
        {
            $id=$request->id;
            $receiver=$request->receiver;
            $first_name=$request->first_name;
            $last_name=$request->last_name;
            $from=$request->from;
            $phone_number=$request->phone_number;
            $depart=$request->depart;
            $arrive=$request->arrive;
            $adults=$request->adults;
            $children=$request->children;
            $flexible_date=$request->flexible_date;
            $body=$request->body;

            $name=$request->first_name.' '.$request->last_name;


            $obj = new Listing();
            $obj->Listing($id);

           /* if ($receiver == "owner") {
        $to = $obj->getString("email");
        $saudation = "Contact ".$obj->getString('title');
    }*/

    if(!Functions::sess_getAccountIdFromSession()) {
            Cookie::make("first_name", $request->first_name);
            Cookie::make("from", $request->from);
            Cookie::make("last_name", $request->last_name);
            Cookie::make("phone_number", $request->phone_number);
            Cookie::make("depart",$request->depart);
            Cookie::make("arrive", $request->arrive);
            Cookie::make("adults", $request->adults);
            Cookie::make("children", $request->children);
            Cookie::make("flexible_date", $request->flexible_date);
            Cookie::make("body", $request->body);
        }
         $error='';
        $sql = "SELECT * FROM Blocked_users";
        $result = Sql::fetch($sql);
        if (count($result) > 0){
            foreach ($result as $row){
                if($from == $row->email){
                    $error .= "The email you are using has been blocked by the system <br />";
                }
                if($_SERVER["REMOTE_ADDR"] == $row->ip){
                    $error .= "The IP you are using has been blocked by the system <br />";
                }
            }
        }

        //$subject = stripslashes($subject);
        $body = stripslashes($body);


       if (empty($error)) {

            if ($receiver == "owner") {
                //CUSTOM - whenever someone succesfully sends an email by the "contact this listing", 
                //a row is added in Inquiry_Email_Ip. We check if the present ip/email has more records than THEME_MAX_IP_SPAM/THEME_MAX_EMAIL_SPAM.
                //If yes, it is considered a spam and will be blocked

                
             $sql = "INSERT INTO Inquiry_Email_Ip VALUES (NULL, '".$from."', '".$_SERVER["REMOTE_ADDR"]."')";
             Sql::insertSqlMain($sql);

             $sql = "SELECT COUNT(email) AS count_email, email FROM Inquiry_Email_Ip WHERE email = '$from'";
             
                if ($row=Sql::fetchMain($sql)) {                   
                    $count_email = $row[0]->count_email;
                    if ($count_email >= config('params.THEME_MAX_EMAIL_SPAM')) {
                        $email_toBlock = $row[0]->email;
                        
                        $sql = "SELECT * FROM Blocked_users WHERE email = ".$email_toBlock;
                        $result = Sql::fetch($sql);
                        if ( ! count($result) ) {
                   $sql = "INSERT INTO Blocked_users (email,lastupdated,automatic) VALUES (".$email_toBlock.", NOW(), 1)";
                            Sql::insertSql($sql);
                        }
                        
                    }
                    
                }

             $sql = "SELECT COUNT(ip) AS count_ip, ip FROM Inquiry_Email_Ip WHERE ip = '".$_SERVER["REMOTE_ADDR"]."'";
                if ($row = Sql::fetchMain($sql)) {                   
                    $count_ip = $row[0]->count_ip;
                    if ($count_ip >= config('params.THEME_MAX_EMAIL_SPAM')) {
                        $ip_toBlock = $row[0]->ip;
                        
                        $sql = "SELECT * FROM Blocked_users WHERE ip = ".$ip_toBlock;
                        $result = Sql::fetch($sql);
                        if ( ! count($result) ) {
                            $sql = "INSERT INTO Blocked_users (ip,lastupdated,automatic) VALUES (".$ip_toBlock.", NOW(), 1)";
                            Sql::insertSql($sql);
                        }
                    }
                    
                } 

                if (empty($subject)) $subject = config('params.LANG_LISTING_CONTACTSUBJECT_ISNULL_1')." ".$obj->getString("title")." ".config('params.LANG_LISTING_CONTACTSUBJECT_ISNULL_2')." ".config('params.EDIRECTORY_TITLE');

                $original_body = $body;
                $original_body = str_replace("<br />", "", $body);
                
                $body = str_replace("<br />", "\n", $body);
                $subject = stripslashes(html_entity_decode($subject));
                $original_subject = $subject;

                $name = stripslashes(html_entity_decode($name));
                $body = stripslashes($body);
                
                $body ="From: ".$name."\n\n"."Email: ".$from."\n\n"."Message:\n\n ".$body;

                $auxsubject     = htmlspecialchars_decode($subject);
                //$subject = "[".system_showText(LANG_CONTACTPRESUBJECT)." ".EDIRECTORY_TITLE."] ".$subject;              
                //!$suspend_listings && 
                if ($emailNotificationObj = Functions::system_checkEmail(config('params.SYSTEM_NEW_LEAD'))) 
                {

                    $auxbody   = $body;
                    $subject   = $emailNotificationObj->getString("subject");
                    $body      = $emailNotificationObj->getString("body");
                    $body      = Functions::system_replaceEmailVariables($body, $id, 'listing');
                    $subject   = Functions::system_replaceEmailVariables($subject, $id, 'listing');
                    $body      = html_entity_decode($body);
                    $subject   = html_entity_decode($subject)/*." - ".$auxsubject*/;
                    if ($emailNotificationObj->getString("content_type") == "text/html") {
                        $auxbody = nl2br($auxbody);
                    }
                    $message = $body;

                    $cityObj = new Location4();
                    $cityObj->Location4($obj->location_4);

                    $stateObj = new Location3();
                    $stateObj->getLocation3($obj->location_3);

                    //$message .= system_showText(LANG_LOCATIONS).":";
                    $listingAddress='';
                    if ($obj->getString("address")) $listingAddress .= " ".htmlspecialchars_decode($obj->getString("address"));
                    if ($obj->getString("address2")) $listingAddress .= " (".htmlspecialchars_decode($obj->getString("address2")).")";
                    if (($obj->getString("address")) || ($obj->getString("address2"))) $listingAddress .= ", ";
                    if ($cityObj->getString("name")) $listingAddress .= $cityObj->getString("name");
                    if (($cityObj->getString("name")) || ($stateObj->getString("name"))) $listingAddress .= " - ";
                    if ($stateObj->getString("name")) $listingAddress .= $stateObj->getString("name");
                    if ($obj->getString("zip_code")) $listingAddress .= " ".$obj->getString("zip_code");
                    $detailLink = url() . "/" . $obj->getString("friendly_url") . ".html";


                    $auxObj = new Listing();
                    $auxObj->Listing($id);
                    $request->to = $auxObj->email;
                    unset($auxObj);

                    //REPLACE CONSTANTS
                    //$message = str_replace("PROPERTY_MAIN_IMAGE", $listing_image, $message);
                    
                     $message = str_replace("LISTING_TITLE", $obj->title, $message);

                    $message = str_replace("SENDER_NAME_LINK", "<a href=\"#\" style=\"color: orange; text-decoration: none\">" . $first_name . "</a>", $message);
                    $message = str_replace("PROPERTY_ID_LINK", "<a href=\"" . $detailLink . "\">" . $obj->id . "</a>", $message);
                    $message = str_replace("RENTERS_NAME", $first_name . " " . $last_name, $message);
                    $message = str_replace("SENDER_EMAIL", $from, $message);
                    $message = str_replace("FRIEND_EMAIL",$request->to, $message);
                    $message = str_replace("PROPERTY_URL", $detailLink, $message);
                    $message = str_replace("PROPERTY_ID", $obj->id, $message);
                    $message = str_replace("LISTING_ADDRESS", $listingAddress, $message);
                    $message = str_replace("DEFAULT_URL", url(), $message);
                    $message = str_replace("CUSTOM_LINK", "http://www.vacationrentals2u.com/info/FraudOwner", $message);
                    $message = str_replace("REQUEST_ARRIVAL", $arrive, $message);
                    $message = str_replace("REQUEST_DEPARTURE", $depart, $message);
                    $message = str_replace("NUM_ADULTS", $adults, $message);
                    $message = str_replace("NUM_CHILDREN", $children, $message);
                    $message = str_replace("PHONE_NUMBER", $phone_number, $message);
                    $message = str_replace("RENTER_MESSAGE", $request->body, $message);
                    $message = str_replace("TERMS_OF_USE_URL", url() . "/popup/popup.php?pop_type=terms", $message);
                    $message = str_replace("REMOTE_ADDR", $_SERVER['REMOTE_ADDR'], $message);
                    $message = str_replace("TODAY_DATE", date("m/d/Y h:m:s A"), $message);
                    $message = str_replace("THIS_YEAR", date("Y"), $message);

                    $subject = str_replace("RENTERS_NAME", $first_name  . " " . $_POST['last_name'], $subject);
                    $subject = str_replace("PROPERTY_ID", $obj->id, $subject);
                    $subject = str_replace("REQUEST_ARRIVAL", $arrive, $subject);
                    $subject = str_replace("REQUEST_DEPARTURE", $depart, $subject);
                    $subject = str_replace("http://", "", $subject);
                    $sitemgr_send_email='amoosjohn@gmail.com';


                        $data= [
            'sitemgr_msg' => $message,
            'subject' => $subject,
            'to' =>$sitemgr_send_email,
            'sitemgr_send_email' =>$sitemgr_send_email
        ];

                    $body      = str_replace("LEAD_MESSAGE", $auxbody, $body);
                    $error = false;
       Mail::send('email.owner',['data' => $data], function($message) use ($data) {
   
        $message->from($data['sitemgr_send_email']);
     $message->to($data['to'])->bcc('amoosjohn@gmail.com')->subject($data['subject']);
    });
                   // $return = system_mail($to, $subject, $message, $sitemgr_send_email, $emailNotificationObj->getString("content_type"), "", $emailNotificationObj->getString("bcc"), $error, '', '', $from);
                    
                    //sending a copy to the renter
     $messageCopy = "This is a copy of the email sent to the listing owner\n\n" . $message;
       $datacopy= [
            'sitemgr_msg' => $messageCopy,
            'subject' => $subject,
            'from' =>$from,
            'sitemgr_send_email' =>$sitemgr_send_email
        ];

        Mail::send('email.renter',['datacopy' => $datacopy], function($message) use ($datacopy) {
   
        $message->from($datacopy['sitemgr_send_email']);
     $message->to($datacopy['from'])->bcc('amoosjohn@gmail.com')->subject($datacopy['subject']);
    });
    
     //system_mail($from, $subject, $messageCopy, $sitemgr_send_email, $emailNotificationObj->getString("content_type"), "", $


                }

          }
                //Custom: ContactReport
                    $contact_report = new ContactReport();
                    $contact_report->module = "listing";
                    $contact_report->item_id = $id;
                    $contact_report->contact_name = $name;
                    $contact_report->contact_mail = $from;
                    $contact_report->Save();
                    //$contact_report->saveInfo($_POST);

                    $arrayMessage = array();
                    $arrayMessage["LANG_LABEL_MESSAGE"] = $original_body;
                    $leadInfo["item_id"] = $id;
                    $leadInfo["member_id"] = Functions::sess_getAccountIdFromSession();
                    $leadInfo["first_name"] = $name;
                    $leadInfo["email"] = $from;
                    $leadInfo["subject"] = $original_subject;
                    $leadInfo["message"] = serialize($arrayMessage);
                    $leadInfo["type"] = "listing";
                    
                    $leadObj = new Lead();
                    $leadObj->arraymakeFromRow($leadInfo);
                    $leadObj->save();        



         $return_email_message = "";       

    $return_email_message .= "<p class=\"successMessage\">Your e-mail has been sent. Thank you.</p>";


     return redirect('listing_emailform/'.$id)->withInput(Input::all())->with('return_email_message',$return_email_message); 

          


        }



        }
        else
        {
            return false;
        }
    }
    

    public function captcha()
    {
    $md5 = md5(microtime()*time());
    $rand = rand(0, 27);
    $string = Functions::string_substr($md5, $rand, 5);

    $captcha = imagecreatefrompng(url()."/images/captcha.png");

    $fontcolor = imagecolorallocate($captcha, 0, 0, 0);
    $linecolor = imagecolorallocate($captcha, 0, 77, 234);

    imageline($captcha, rand(0, 100), 0, rand(0, 100), 30, $linecolor);
    imageline($captcha, rand(0, 100), 0, rand(0, 100), 30, $linecolor);
    imageline($captcha, rand(0, 100), 0, rand(0, 100), 30, $linecolor);

    imagestring($captcha, 5, rand(0, 55), rand(0, 15), $string, $fontcolor);

    $captchakey = md5($string);
    Session::forget('SESS_captch_key');
    Session::put('SESS_captch_key', $captchakey);
   
    ob_start();
    imagepng($captcha);
    // Capture the output
    $imagedata = ob_get_contents();
    // Clear the output buffer
    ob_end_clean();    //header("Content-type: image/png");
    return $imagedata;

    }

    public function StoreReview(Request $request)
    {
        if($request->_token==csrf_token())
        {

            $success_review = false;   
            //setting_get("review_manditory", $review_manditory);
            //setting_get("review_approve", $review_approve);
            $review_approve="on";
            $review_manditory="on";
            $allowed = true;
            $rating=$request->rating;
            $item_id=$request->item_id;
            $item_type=$request->item_type;
            $message_review= '';

            if ($item_type == "listing") {
        $itemObj = new Listing();
        $itemObj->Listing($item_id);

        $item_name = $itemObj->getString("title");
    }

             if (!$rating) {
            $message_review = config('params.LANG_MSG_REVIEW_SELECTRATING');
            $allowed = false;
        } elseif ($rating > 5 ) {
            $message_review = config('params.LANG_MSG_REVIEW_FRAUD_SELECTRATING');
            $allowed = false;
        } elseif (!trim($request->review) || !trim($request->review_title)) {
            $message_review = config('params.LANG_MSG_REVIEW_COMMENTREQUIRED');
            $allowed = false;
        }

        if ($review_manditory == "on") {
            if (!trim($request->reviewer_name) || !trim($request->reviewer_email)) {
                $message_review = config('params.LANG_MSG_REVIEW_NAMEEMAILREQUIRED');
                $allowed = false;
            }
        }

        if ($request->reviewer_email && !Functions::validate_email($request->reviewer_email)) {
            $message_review = config('params.LANG_MSG_REVIEW_TYPEVALIDEMAIL');
            $allowed = false;
        }

        if (md5($request->captchatext) != session('SESS_captch_key')) {
                $message_review = config('params.LANG_MSG_CONTACT_TYPE_CODE');

               
                $allowed = false;
            }

       $reviewObj = new Review();
        $denied_ips = $reviewObj->getDeniedIpsByItem($item_type, $itemObj->getString("id"));
        if ($denied_ips) {
            foreach ($denied_ips as $each_ip) {
                if ($_SERVER["REMOTE_ADDR"] == $each_ip) {
                    $message_review =  config('params.LANG_MSG_REVIEW_YOUALREADYGIVENOPINION');
                    $allowed = false;
                }
            }
        }   



        if ($allowed) {

           $request->ip = $_SERVER["REMOTE_ADDR"];
            $reviewObj = new Review();
            $reviewObj->Review($request->all());

            if ($review_approve != "on") {
                $reviewObj->setNumber("approved", 1);
            }
            $reviewObj->Save();

              if ($review_approve != "on") {
                $avg = $reviewObj->getRateAvgByItem($item_type, $item_id);
                if (!is_numeric($avg)) $avg = 0;
                if ($item_type == 'listing') {
                    $listing = new Listing();
                    $listing->setAvgReview($avg, $item_id);
                } 
            }

            $reviewObjs = new Review();
            $reviewObjs->Review($reviewObj->id);

            if ($reviewObj->review) {

                  /* send e-mail to listing owner */
                if($reviewObj->getString('item_type') == 'listing') {
                    $contactObj = new Contact();
                    $contactObj->Contact($itemObj->account_id);

                    if($emailNotificationObj = Functions::system_checkEmail(config('params.SYSTEM_NEW_REVIEW'))) {
                        //setting_get("sitemgr_send_email", $sitemgr_send_email);
                        //setting_get("sitemgr_email", $sitemgr_email);
                        //$sitemgr_emails = explode(",", $sitemgr_email);
                        //if ($sitemgr_emails[0]) $sitemgr_email = $sitemgr_emails[0];
                        $subject   = $emailNotificationObj->getString("subject");
                        $body      = $emailNotificationObj->getString("body");
                        $body      = Functions::system_replaceEmailVariables($body, $itemObj->getNumber('id'), 'listing');
                        $subject   = Functions::system_replaceEmailVariables($subject, $itemObj->getNumber('id'), 'listing');
                        $body      = html_entity_decode($body);
                        $subject   = html_entity_decode($subject);
                        $error = false;
                       $sitemgr_send_email='amoos@golpik.com';

                         $data= [
            'sitemgr_msg' => $body,
            'subject' => $subject,
            'to' =>$sitemgr_send_email,//$contactObj->getString("email")
            'sitemgr_send_email' =>$sitemgr_send_email
        ];

       Mail::send(['text' => 'email.signup'],['data' => $data], function($message) use ($data) {
   
        $message->from($data['sitemgr_send_email']);
     $message->to($data['to'])->bcc('amoos@golpik.com')->subject($data['subject']);
    });


    //system_mail($contactObj->getString("email"), $subject, $body, EDIRECTORY_TITLE." <$sitemgr_email>", $emailNotificationObj->getString("content_type"), "", $emailNotificationObj->getString("bcc"), $error);
                    }
                }


            }

 if ($review_approve == "on") {
                $message_review = " ".config('params.LANG_MSG_REVIEW_REVIEWSUBMITTEDAPPROVAL');
    }

            $success_review = true;  

        }


 

     return redirect('review/'.$item_id.'/'.$item_type)->withInput(Input::all())->with('message_review',$message_review)->with('success_review',$success_review); 
    

        
    }

    else
    {
        return false;
    }

}

    



}