<?php 
namespace App\Http\Controllers;
use DB;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Validator, Input, Redirect, Session, Cookie;
use App\Models\Domain\Sql;
use App\Models\Main\MainSql;
use App\Functions\Functions;

class UserController extends Controller {

    protected $layout = 'layouts.search';
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//$this->middleware('auth');
	}
    

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
        $params=array();
        $footer_text = Sql::footerText();
        $params['type'] = 'Home Page';
        $meta_data = Sql::HomePageSummerSale($params);
        
        $header_author = Sql::HomePageHeaderAuthor();
        
        $header_searching_tag = Sql::HomePageHeaderSearchingTag();
        $login='Login';
        Session::put('SESS_LOGIN', $login);
        
        return view('front.user_login',compact('meta_data','login','header_author','header_searching_tag','footer_text'));
	}
    
   public function login(Request $request)
  { 
    if($request->_token==csrf_token())
       {  

        $username=$request->username?$request->username:'';
        $password=$request->password?$request->password:'';

        $params=array();
        $destiny=$_GET['destiny'];
        $destiny=urldecode($destiny);

        if ($_SERVER["QUERY_STRING"]) {
        if (Functions::string_strpos($_SERVER["QUERY_STRING"], "query=") !== false) {
          $query = Functions::string_substr($_SERVER["QUERY_STRING"], Functions::string_strpos($_SERVER["QUERY_STRING"], "query=")+6);
        } else {
          $query = $_GET["query"] ? $_GET["query"] : $_POST["query"];
          $query = urldecode($query);
        }
      } else {
        $query = $_GET["query"] ? $_GET["query"] : $_POST["query"];
        $query = urldecode($query);
      }
        

        $advertise='yes';
         $login='Login';
        Session::put('SESS_LOGIN', $login);
        if($username!='' && $password!='')
        {

             $this->validate($request, [
          'username' => 'required|email', 'password' => 'required',
        ]); 
        $credentials = $request->only('username', 'password');
    
    
     //$advertise=$request->advertise?$request->advertise:'';
     //$destiny=$request->destiny?$request->destiny:'';
     $query_url=$query;
    
     $userSession = MainSql::userAuthanticate($request->username,$request->password);
     
       if(is_numeric($userSession) && $userSession>0)
       {
        $url='';
        if($advertise=='yes')
        {
          if ($destiny) {
          $url = $destiny;
          if ($query_url) $url .= "?".$query_url;
       } 
        }
        else {
        $url = 'sponsors';
      }
     
            Session::put('SESS_ACCOUNT_ID', $userSession );
            
            Cookie::make("username_members", $request->username, 60);
            
            Cookie::make("uid", $userSession, 60);
            
            if ($request->automatic_login) {

              Session::put('SESS_ACCOUNT_NAME', $request->username );
            
                Cookie::make("automatic_login_members", "true", 60);
                $request->password = Functions::string_strtolower(config('params.PASSWORD_ENCRYPTION')) == "on" ? md5($request->password) : $request->password;
                $aux = md5(trim($request->username).$request->password);
              
                Cookie::make("complementary_info_members",$aux, 60);
            
            
            } else {

              $profileSp = Functions::SponsersProfile();
                $ownerName= '';
                if(!empty($profileSp))
                {
                 $ownerName = $profileSp[0]->first_name.' '.$profileSp[0]->last_name;
                }
              
                Session::put('SESS_ACCOUNT_NAME', $ownerName );

                Cookie::make("automatic_login_members","false", 60);
            }
             
            return redirect($url);
        }
          else
       {
        return redirect('/login')->withErrors([
        'error' => 'These credentials do not match our records.',
    ]);
       } 
        }
        else 
        {
          $footer_text = Sql::footerText();
          $params['type'] = 'Home Page';
          $meta_data = Sql::HomePageSummerSale($params);
          
          $header_author = Sql::HomePageHeaderAuthor();
          
          $header_searching_tag = Sql::HomePageHeaderSearchingTag();
          
          return view('front.user_login',compact('meta_data','login','header_author','header_searching_tag','footer_text','destiny','query','advertise'));

        }
       
       }
       else
       {
         return false;
       }
   }
   	public function post_login(Request $request)
	{
	   if($request->_token==csrf_token())
       {
            $this->validate($request, [
    			'username' => 'required|email', 'password' => 'required',
    		]); 
    		$credentials = $request->only('username', 'password');
    
    	//	if ($credentials->fails()) {
//    			return redirect()->back()->withErrors($credentials->errors())->withInput();
//    		}
      
     $advertise=$request->advertise?$request->advertise:'';
     $destiny=$request->destiny?$request->destiny:'';
     $query_url=$request->request->get('query');
		
	   $userSession = MainSql::userAuthanticate($request->username,$request->password);
	   
       if(is_numeric($userSession) && $userSession>0)
       {
        $url='';
        if($advertise=='yes')
        {
          if ($destiny) {
          $url = $destiny;
          if ($query_url) $url .= "?".$query_url;
       } 
        }
        else {
        $url = 'sponsors';
      }
     
            Session::put('SESS_ACCOUNT_ID', $userSession );
            
            Cookie::make("username_members", $request->username, 60);
            
            Cookie::make("uid", $userSession, 60);
            
            if ($request->automatic_login) {

              Session::put('SESS_ACCOUNT_NAME', $request->username );
            
                Cookie::make("automatic_login_members", "true", 60);
                $request->password = Functions::string_strtolower(config('params.PASSWORD_ENCRYPTION')) == "on" ? md5($request->password) : $request->password;
                $aux = md5(trim($request->username).$request->password);
              
                Cookie::make("complementary_info_members",$aux, 60);
            
            
            } else {

              $profileSp = Functions::SponsersProfile();
                $ownerName= '';
                if(!empty($profileSp))
                {
                 $ownerName = $profileSp[0]->first_name.' '.$profileSp[0]->last_name;
                }
              
              Session::put('SESS_ACCOUNT_NAME', $ownerName );

                Cookie::make("automatic_login_members","false", 60);
            }
             
            return redirect($url);
        }
          else
       {
        return redirect('/login')->withErrors([
        'error' => 'These credentials do not match our records.',
    ]);
       } 

               
       }
     

 else
       {
            return false;
       }

	}
    
    function sponser_dashboard()
    {
        $userAccountId = Session::get('SESS_ACCOUNT_ID');
        $profileSp = Functions::SponsersProfile();
        $ownerName= '';
        if(!empty($profileSp))
        {
         $ownerName = $profileSp[0]->first_name.' '.$profileSp[0]->last_name.', ';
        $meta_data = Sql::sponsersMeta($userAccountId);
        $headerTitle = $ownerName.'Welcome to the Sponsor Section -'.$meta_data[0]->value;
        $headerAuthor = $meta_data[1]->value;
        $headerDescription = $meta_data[2]->value;
        $headerKeywords = $meta_data[3]->value;
        $footer_text = Sql::footerText();
        
        $login='Login';
        Session::put('SESS_LOGIN', $login);

        $spListingData = Sql::SponserListing($userAccountId);
        return view('front.sponsers_dashboard',compact('spListingData','login','headerTitle','headerAuthor','headerDescription','headerKeywords','footer_text'));
        }
        else
        {
          return redirect(url());
        }
        
    }
    public function post_logout()
    {
      Auth::logout();
      Session::put('SESS_ACCOUNT_ID','' );
      Session::flush();
      return redirect(url());
    }
    

}