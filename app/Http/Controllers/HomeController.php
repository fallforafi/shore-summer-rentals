<?php 
namespace App\Http\Controllers;
use DB;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Validator, Input, Redirect, Session, Cookie,Mail;
use App\Models\Domain\Sql;
use App\Functions\Functions;
use App\Functions\Listing;
use App\Functions\ListingLevel;
use App\Functions\ListingCategory;
use App\Functions\Listing_Category;
use App\Functions\Account;
use App\Functions\Contact;
use App\Functions\Profile;
use App\Functions\Invoice;
use App\Functions\InvoiceListing;


class HomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/
    protected $layout = 'layouts.search';
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//$this->middleware('auth');
	}
    

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
	   $params=array();
       $sliders = Sql::getHomePageSlider();
       
       $sp_images = Sql::getHomePageSpecialSection();
       
       $featured_images = Sql::getHomePageFeaturedRental();
       
       $banner_images = Sql::getHomePageBanners();
       
       $second_four_banner_images = Sql::getHomePageBannersSecondFour();
       
       $footer_text = Sql::footerText();
       
       
       $params['type'] = 'Home Page Bottom';
       $bottom_text = Sql::HomePageSummerSale($params);
       
       
       $params['type'] = 'Home Page';
       $meta_data = Sql::HomePageSummerSale($params);
       
       $header_author = Sql::HomePageHeaderAuthor();
       
       $header_searching_tag = Sql::HomePageHeaderSearchingTag();
       $params['type'] = 'SummerSale';
       $summer_sale = Sql::HomePageSummerSale($params);

        Session::put('SESS_LOGIN','');
       
       return view('front.index',compact('sliders','sp_images','featured_images','banner_images','second_four_banner_images','footer_text','bottom_text','meta_data','header_author','header_searching_tag','summer_sale'));
	}
    
  
    public function faqs()
    {
       $params=array();
       $footer_text = Sql::footerText();
       $params['type'] = 'Home Page';
       $meta_data = Sql::HomePageSummerSale($params);
       
       $header_author = Sql::HomePageHeaderAuthor();
       
       $header_searching_tag = Sql::HomePageHeaderSearchingTag();  
       return view('front.faqs',compact('meta_data','header_author','header_searching_tag','footer_text'));
	
    }
    
    public function WhyChooseUs()
    {
       $params=array();
       $footer_text = Sql::footerText();
       $params['type'] = 'Home Page';
       $meta_data = Sql::HomePageSummerSale($params);
       
       $header_author = Sql::HomePageHeaderAuthor();
       
       $header_searching_tag = Sql::HomePageHeaderSearchingTag();  
       return view('front.why_choose_us',compact('meta_data','header_author','header_searching_tag','footer_text'));
	
    }
    
    public function RentersFraudAlert()
    {
        $params=array();
        $footer_text = Sql::footerText();
        $params['type'] = 'Home Page';
        $meta_data = Sql::HomePageSummerSale($params);
        
        $header_author = Sql::HomePageHeaderAuthor();
        
        $header_searching_tag = Sql::HomePageHeaderSearchingTag();
        return view('front.fraud-alert-renters',compact('meta_data','header_author','header_searching_tag','footer_text'));
    
    }
     public function RentersOwnerAlert()
    {
        $params=array();
        $footer_text = Sql::footerText();
        $params['type'] = 'Home Page';
        $meta_data = Sql::HomePageSummerSale($params);
        
        $header_author = Sql::HomePageHeaderAuthor();
        
        $header_searching_tag = Sql::HomePageHeaderSearchingTag();
        return view('front.fraud-alert-owner',compact('meta_data','header_author','header_searching_tag','footer_text'));
    
    }
    public function OurTeam()
    {
        $params=array();
        $footer_text = Sql::footerText();
        $params['type'] = 'Home Page';
        $meta_data = Sql::HomePageSummerSale($params);
        
        $header_author = Sql::HomePageHeaderAuthor();
        
        $header_searching_tag = Sql::HomePageHeaderSearchingTag();
        return view('front.our_team',compact('meta_data','header_author','header_searching_tag','footer_text'));
    
    }
    
    public function PhotographyServices()
    {
        $params=array();
        $footer_text = Sql::footerText();
        $params['type'] = 'Home Page';
        $meta_data = Sql::HomePageSummerSale($params);
        
        $header_author = Sql::HomePageHeaderAuthor();
        
        $header_searching_tag = Sql::HomePageHeaderSearchingTag();
        return view('front.photography_services',compact('meta_data','header_author','header_searching_tag','footer_text'));
    
    }
    
    public function PrivacyPolicy()
    {
        $params=array();
        $footer_text = Sql::footerText();
        $params['type'] = 'Home Page';
        $meta_data = Sql::HomePageSummerSale($params);
        
        $header_author = Sql::HomePageHeaderAuthor();
        
        $header_searching_tag = Sql::HomePageHeaderSearchingTag();
        return view('front.privacy_policy',compact('meta_data','header_author','header_searching_tag','footer_text'));
    
    }
    
       
    public function TestimonialsOwners()
    {
        $params=array();
        $footer_text = Sql::footerText();
        $params['type'] = 'Home Page';
        $meta_data = Sql::HomePageSummerSale($params);
        
        $header_author = Sql::HomePageHeaderAuthor();
        
        $header_searching_tag = Sql::HomePageHeaderSearchingTag();
        return view('front.testimonials_from_owners',compact('meta_data','header_author','header_searching_tag','footer_text'));
    
    }
    
    public function  Advertise()
    {
        $params=array();
        $footer_text = Sql::footerText();
        $params['type'] = 'Home Page';
        $meta_data = Sql::HomePageSummerSale($params);
        
        $header_author = Sql::HomePageHeaderAuthor();
        
        $header_searching_tag = Sql::HomePageHeaderSearchingTag();
        
        $params['type'] = 'Advertise with Us Benefits';
        $advertise_benefits_text = Sql::ContentText($params);
        
        return view('front.advertise',compact('meta_data','header_author','header_searching_tag','footer_text','advertise_benefits_text'));
    
    }
     public function  listingInstruction()
    {
        $params=array();
        $footer_text = Sql::footerText();
        $params['type'] = 'Home Page';
        $meta_data = Sql::HomePageSummerSale($params);
        
        $header_author = Sql::HomePageHeaderAuthor();
        
        $header_searching_tag = Sql::HomePageHeaderSearchingTag();
        
        $params['type'] = 'CREATING & EDITING';
        $guarantee_and_verify = Sql::ContentText($params);
        
        return view('front.sponsers_listing-instructions',compact('meta_data','header_author','header_searching_tag','footer_text','guarantee-and-verify'));
    
    }
    public function  guaranteeVerify()
    {
        $params=array();
        $footer_text = Sql::footerText();
        $params['type'] = 'Home Page';
        $meta_data = Sql::HomePageSummerSale($params);
        
        $header_author = Sql::HomePageHeaderAuthor();
        
        $header_searching_tag = Sql::HomePageHeaderSearchingTag();
        
        $params['type'] = 'Guarantee and Verify';
        $guarantee_and_verify = Sql::ContentText($params);
        
        return view('front.guarantee-and-verify',compact('meta_data','header_author','header_searching_tag','footer_text','guarantee-and-verify'));
    
    }
  
  public function  cashbackReferral()
    {
        $params=array();
        $footer_text = Sql::footerText();
        $params['type'] = 'Home Page';
        $meta_data = Sql::HomePageSummerSale($params);
        
        $header_author = Sql::HomePageHeaderAuthor();
        
        $header_searching_tag = Sql::HomePageHeaderSearchingTag();
        
        $params['type'] = 'Cashback and Referral';
        $cashback_and_referral = Sql::ContentText($params);
        
        return view('front.cash-back-and-referral',compact('meta_data','header_author','header_searching_tag','footer_text','cash-back-and-referral'));
    
    }
  
  public function fraudAlertOwner()
    {
        $params=array();
        $footer_text = Sql::footerText();
        $params['type'] = 'Home Page';
        $meta_data = Sql::HomePageSummerSale($params);
        
        $header_author = Sql::HomePageHeaderAuthor();
        
        $header_searching_tag = Sql::HomePageHeaderSearchingTag();
        
        $params['type'] = 'Fraud Alert Owner';
        $fraud_alert_owner = Sql::ContentText($params);
        
        return view('front.fraud-alert-owner',compact('meta_data','header_author','header_searching_tag','footer_text','fraud-alert-owner'));
    
    }
  
  public function contentFaqs()
    {
        $params=array();
        $footer_text = Sql::footerText();
        $params['type'] = 'Home Page';
        $meta_data = Sql::HomePageSummerSale($params);
        
        $header_author = Sql::HomePageHeaderAuthor();
        
        $header_searching_tag = Sql::HomePageHeaderSearchingTag();
        
        $params['type'] = 'Frequently Asked Questions';
        $content_faqs = Sql::ContentText($params);
        
        return view('front.faqs',compact('meta_data','header_author','header_searching_tag','footer_text','faqs'));
    
    }
	
	 public function SpamWarning()
    {
        $params=array();
        $footer_text = Sql::footerText();
        $params['type'] = 'Home Page';
        $meta_data = Sql::HomePageSummerSale($params);
        
        $header_author = Sql::HomePageHeaderAuthor();
        
        $header_searching_tag = Sql::HomePageHeaderSearchingTag();
        
        $params['type'] = 'Frequently Asked Questions';
        $spam_warning = Sql::ContentText($params);
        
        return view('front.spam-warning',compact('meta_data','header_author','header_searching_tag','footer_text','faqs'));
    
    }
  

  
  public function rentalSignForm()
    {
        $params=array();
        $footer_text = Sql::footerText();
        $params['type'] = 'Home Page';
        $meta_data = Sql::HomePageSummerSale($params);
        
        $header_author = Sql::HomePageHeaderAuthor();
        
        $header_searching_tag = Sql::HomePageHeaderSearchingTag();
        
        $params['type'] = 'Rental Sign Form';
        $rental_sign_form = Sql::ContentText($params);
        
        return view('front.for-rent-sign-request',compact('meta_data','header_author','header_searching_tag','footer_text','for-rent-sign-request'));
    
    }
    
    public function orderListing(Request $request)
    {
        $params=array();
        $footer_text = Sql::footerText();
        $params['type'] = 'Home Page';
        $meta_data = Sql::HomePageSummerSale($params);

        
        $header_author = Sql::HomePageHeaderAuthor();
        
        $header_searching_tag = Sql::HomePageHeaderSearchingTag();
        
        $params['type'] = 'Order Listing';
        $order_listing_page = Sql::ContentText($params);

        $level=$request->level;

        $unique_id = Functions::system_generatePassword();

        $listingLevelObj = new ListingLevel();
        $listingLevelObj->ListingLevel(true);

        $checkoutpayment_class = "isHidden";
        $checkoutfree_class = "isHidden";

        $levelValue = $listingLevelObj->getValues($level);
        $labelName = $listingLevelObj->showLevel($level);

         $labelPrice = "";
        $labelPriceRenewal = "";

        if ($listingLevelObj->getPrice($level) > 0) {
        $labelPrice = config('params.CURRENCY_SYMBOL').$listingLevelObj->getPrice($level);
        $labelPriceRenewal = " per ";
        if (Functions::payment_getRenewalCycle("listing") > 1) {
            $labelPriceRenewal .= Functions::payment_getRenewalCycle("listing")." ";
            $labelPriceRenewal .= Functions::payment_getRenewalUnitNamePlural("listing");
        } else {
            $labelPriceRenewal .= Functions::payment_getRenewalUnitName("listing");
        }
    } else {
        $labelPrice = config('params.CURRENCY_SYMBOL')."Free";
    }

     
    $formloginaction=url('login'.'?destiny=/sponsors/addlisting');
      
        
 return view('front.order_listing',compact('meta_data','header_author','header_searching_tag','footer_text','order_listing', 'level','levelValue','labelName','labelPrice','labelPriceRenewal','checkoutpayment_class','checkoutfree_class','unique_id','title','discount_id','payment_method','formloginaction'));
    
    }

   public function ordercalculateprice(Request $request) 
   {
        header("Expires: Sat, 01 Jan 2000 00:00:00 GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", FALSE);
        header("Pragma: no-cache");

    $return = 0;

    $item = $request->item;
    $itemID = $request->item_id;
     $level=$request->level;
     $payment_tax_status="off";
    

    if ($item) {
    
       
        $itemObj = new Listing();
        $itemObj->Listing();
       
        if ($itemObj) {

            $itemObj->setString("level", $level);
            if ($item == "listing") {
                if (strpos($request->categories, "x") === false) {
//                  if ($_GET["categories"] <= MAX_CATEGORY_ALLOWED) {
                        $itemObj->setString("categories",$request->categories);
//                  }
                }
                
                $_SESSION["fb_{$item}_template_id_{$itemID}"]           = $request->listingtemplate_id;
                $_SESSION["fb_{$item}_return_categories_{$itemID}"]     = $request->return_categories;
                
                $_SESSION["go_{$item}_template_id_{$itemID}"]           = $request->listingtemplate_id;
                $_SESSION["go_{$item}_return_categories_{$itemID}"]     = $request->return_categories;

                $itemObj->setString("listingtemplate_id",$request->listingtemplate_id);
                
           }  

            $_SESSION["fb_{$item}_discount_id_{$itemID}"]           = $request->discount_id;
            $_SESSION["go_{$item}_discount_id_{$itemID}"]           = $request->discount_id;

            $itemObj->setString("discount_id", $request->discount_id);


            //setting_get("payment_tax_status", $payment_tax_status);
            //setting_get("payment_tax_value", $payment_tax_value);

            if ($payment_tax_status == "on") {
                $return = Functions::format_money($itemObj->getPrice());
                $subtotal = $return * 100;
                $tax = Functions::payment_calculateTax($return, $payment_tax_value, true, false) * 100;
                $total = Functions::payment_calculateTax($return, $payment_tax_value, true) * 100;

                if ($subtotal < 1) $subtotal = "0";
                elseif ($subtotal < 10) $subtotal = "00".$subtotal;
                elseif ($subtotal < 100) $subtotal = "0".$subtotal;

                if ($tax < 1) $tax = "0";
                elseif ($tax < 10) $tax = "00".$tax;
                elseif ($tax < 100) $tax = "0".$tax;


                if ($total < 1) $total = "0";
                elseif ($total < 10) $total = "00".$total;
                elseif ($total < 100) $total = "0".$total;

                $return = $subtotal."|".$tax."|".$total;
            } else {
                    //$levelObj = new ListingLevel();
                    //$levelObj->ListingLevel(true);
          
                $return =Functions::format_money($itemObj->getPrice()) * 100;
                if ($return < 1) $return = "0";
                elseif ($return < 10) $return = "00".$return;
                elseif ($return < 100) $return = "0".$return;
            }

         

             

               
        }
    }

       echo $return;



   }
    public function loadcategorytree(Request $request) 
   {
        header("Expires: Sat, 01 Jan 2000 00:00:00 GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", FALSE);
        header("Pragma: no-cache");
        header("Content-Type: text/html; charset=".config('params.EDIR_CHARSET'), TRUE);

        $prefix= $request->prefix;
        $category = $request->category;
        $path = $request->path;
        

        $return = "";

         if (Functions::string_strpos(Functions::string_strtolower($request->category), "category") !== false) {
        $sql_categories = "SELECT id, title FROM ".$request->category." WHERE category_id = ".$request->category_id." AND title <> '' AND enabled = 'y' ORDER BY title";
     $categories = Sql::fetch($sql_categories);
    }

     if ($categories) {

        $arrayCategoriesIds = explode(",",$request->ajax_categories);

         foreach ($categories as $category) {

             if(in_array($category->id, $arrayCategoriesIds)){
                $style = "style=\"display:none;\"";
            }
            else{
                $style = "";
            }

         if ($request->action == "main") {
                $return .= "<li class=\"categoryBullet\">".$category->title." <a id='categoryAdd".$category->id."' $style href=\"javascript:void(0);\" onclick=\"JS_addCategory(".$category->id.");\" class=\"categoryAdd\">Add</a></li>";
                $return .= "<li id=\"liContent".$category->id."\" style=\"display:none\">".$category->title."</li>";
            }


            else {
                //$path_count = count($category->getFullPath());
                $sql = "SELECT id FROM ".$request->category." WHERE category_id =".$category->id." AND title <> '' AND enabled = 'y'";
                $result = Sql::fetch($sql);//($path_count < CATEGORY_LEVEL_AMOUNT) && 
                    if (count($result) > 0) {
    $return .= "<li><a href=\"javascript:void(0);\" onclick=\"loadCategoryTree('all', '".$prefix."', '".$request->category."', ".$category->id.", 0, '".$path."',1);\" class=\"switchOpen\" id=\"".$prefix."opencategorytree_id_".$category->id."\">+</a><a href=\"javascript:void(0);\" onclick=\"loadCategoryTree('all', '".$prefix."', '".$request->category."', ".$category->id.", 0, '".$path."',1);\" class=\"categoryTitle\" id=\"".$prefix."opencategorytree_title_id_".$category->id."\">".$category->title."</a><a href=\"javascript:void(0);\" onclick=\"closeCategoryTree('".$prefix."', '".$request->category."', ".$category->id.", '".$path."');\" class=\"switchClose\" id=\"".$prefix."closecategorytree_id_".$category->id."\" style=\"display: none;\">-</a><a href=\"javascript:void(0);\" onclick=\"closeCategoryTree('".$prefix."', '".$request->category."', ".$category->id.", '".$path."');\" class=\"categoryTitle\" id=\"".$prefix."closecategorytree_title_id_".$category->id."\" style=\"display: none;\">".$category->title."</a>\n<ul id=\"".$prefix."categorytree_id_".$category->id."\" style=\"display: none;\"></ul>\n</li>\n";
                } 
                else {
               
                    $return .= "<li class=\"categoryBullet\">".$category->title." <a id='categoryAdd".$category->id."' href=\"javascript:void(0);\" $style onclick=\"JS_addCategory(".$category->id.");\" class=\"categoryAdd\">Add</a></li>";
                    $return .= "<li id=\"liContent".$category->id."\" style=\"display:none\">".$category->title."</li>";
                }
                
            }
           

         }

     }


 else {
        $return = "<li class=\"informationMessage\">No categories found.</li>";
    }
    
    echo $return;


      



   }

 public function validateAdvertise(Request $request) 
   {

    if($request->_token==csrf_token())
    {   
        header("Content-Type: text/html; charset=UTF-8", TRUE);
        header("Accept-Encoding: gzip, deflate");
        header("Expires: Sat, 01 Jan 2000 00:00:00 GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check", FALSE);
        header("Pragma: no-cache");

        $item_type=$request->item_type;
        $has_payment=$request->has_payment;

       $validate_item = Functions::validate_form($item_type, $request->all(), $message_item, 0, true);

        $validate_discount = Functions::is_valid_discount_code($request->discount_id, $item_type, $request->id, $message_discount, $discount_error_num);
         $msgError='';
         if (!$validate_item || !$validate_discount || !$has_payment) {
        $msgError.= $message_item;
        $msgError.= ($msgError ? "<br />" : "").$message_discount;
        if (!$has_payment) {
            $msgError.= ($msgError && $message_discount ? "<br />" : "")."&#149;&nbsp;".config('params.LANG_MSG_NO_PAYMENT_METHOD_SELECTED');
        }
        echo $msgError;
      
       
         
       
    } else {
        echo "ok";
    }

    }

   }
   public function order_signup(Request $request) 
   {

    if($request->_token==csrf_token())
    {   


        $payment_method=$request->payment_method;
        $return_categories=$request->feed;
        $request->retype_password = $request->password;
        $request->email = $request->username;

        $validate_account = Functions::validate_addAccount($request->all(), $message_account);
        //$tmpEMAIL = $_POST["email"];
        //unset($_POST["email"]);
        $validate_contact = Functions::validate_form("contact", $request->all(), $message_contact);

        //$validate_listing = Functions::validate_form("listing", $_POST, $message_listing,true);
        $validate_discount = Functions::is_valid_discount_code($request->discount_id, "listing",$request->id, $message_discount, $discount_error_num);
        //$validate_listing 
        if ($validate_account &&  $validate_contact &&  $validate_discount) {

            $request->notify_traffic_listing = ($request->notify_traffic_listing  ? 'y' : 'n');

            $account = new Account();
            $account->Account($request->all());
            # custom active all accounts
            $account->setString("active", "y");
            $account->Save();

            $account->changeMemberStatus(true);

            $contact = new Contact();
            $contact->Contact($request->all());
            $contact->setNumber("account_id", $account->id);
            $contact->setString("email", $request->email);
            $contact->Save();

            $profileObj = new Profile();
            $profileObj->Profile();
            $profileObj->setNumber("account_id", $account->id);
            if (!$profileObj->getString("nickname")) {
                $profileObj->setString("nickname", $request->first_name." ".$request->last_name);
            }
            $profileObj->Save();

            unset($request->email);
            unset($request->phone);
            unset($request->address);
            unset($request->address2);
            $listing = new Listing();
            $listing->Listing($request->all());
            $listing->setNumber("account_id", $account->id);
            //$status = new ItemStatus();
            $listing->setDate("renewal_date", "00/00/0000");
            $listing->Save();

            $array = explode(",", $return_categories);
            if ($array) {

                    foreach ($array as $category) {

                        if ($category) {

                            $lCatObj = new ListingCategory();
                            $lCatObj->ListingCategory($category);
                            $root_id = $lCatObj->getNumber("root_id");
                            $left = $lCatObj->getNumber("left");
                            $right = $lCatObj->getNumber("right");

                            $l_catObj = new Listing_Category();
                            $l_catObj->Listing_Category();

                            $l_catObj->setNumber("listing_id",  $listing->getNumber("id"));
                            $l_catObj->setNumber("category_id", $category);
                            $l_catObj->setString("status", 'P');
                            $l_catObj->setNumber("category_root_id", $root_id);
                            $l_catObj->setNumber("category_node_left", $left);
                            $l_catObj->setNumber("category_node_right", $right);
                            $l_catObj->Save();
                      
                        }
                    }
                }
            

            //$gallery = new Gallery($id);

            //$aux = array("account_id" => 0, "title" => $request->title, "entered" => "NOW()", "updated" => "now()");
            //$gallery->makeFromRow($aux);
            //$gallery->save();
           // $listing->setGalleries($gallery->getNumber("id"));

            $listing->setString("friendly_url", $listing->getNumber("id"));
            $listing->Save();

            // sending e-mail to user 
      if ($emailNotificationObj = Functions::system_checkEmail(config('params.SYSTEM_LISTING_SIGNUP'))) {
                $subject = $emailNotificationObj->getString("subject");
                $body = $emailNotificationObj->getString("body");
                $login_info = "Account Holder E-mail: ".$request->username;
                $login_info .= ($emailNotificationObj->getString("content_type") == "text/html"? "<br/>": "\n");
                $login_info .= "Password: ".$request->password;
                $body = str_replace("ACCOUNT_LOGIN_INFORMATION",$login_info,$body);
                $body = Functions::system_replaceEmailVariables($body, $listing->getNumber('id'), 'listing');
                $subject = Functions::system_replaceEmailVariables($subject, $listing->getNumber('id'), 'listing');
                $body = html_entity_decode($body);
                $subject = html_entity_decode($subject);
                $data= [
            'sitemgr_msg' => $body,
            'subject' => $subject,
            'email_send' =>$request->username
        ];

    Mail::send(['text' => 'email.signup'],['data' => $data], function($message) use ($data) {
   
        $message->from('amoosjohn@gmail.com');
     $message->to($data['email_send'])->cc('amoosjohn@gmail.com')->subject($data['subject']);
    });

               
                //system_mail($contact->getString("email"), $subject, $body, config('params.EDIRECTORY_TITLE_EMAIL')." <$sitemgr_email>", $emailNotificationObj->getString("content_type"), "", $emailNotificationObj->getString("bcc"), $error);
            }
            
            // site manager warning message "[".EDIRECTORY_TITLE_EMAIL."] ".
            $emailSubject = config('params.LANG_NOTIFY_SIGNUPLISTING');
            $sitemgr_msg = config('params.LANG_LABEL_SITE_MANAGER').",<br /><br />".config('params.LANG_NOTIFY_SIGNUPLISTING_1')."<br /><br />".config('params.LANG_LABEL_ACCOUNT').":<br /><br />";
            $sitemgr_msg .= "<strong>".config('params.LANG_LABEL_USERNAME2').": </strong>".Functions::system_showAccountUserName($request->username)."<br />";
            $sitemgr_msg .= "<strong>".config('params.LANG_LABEL_FIRST_NAME').": </strong>".$contact->first_name."<br />";
            $sitemgr_msg .= "<strong>".config('params.LANG_LABEL_LAST_NAME').": </strong>".$contact->last_name."<br />";
            $sitemgr_msg .= "<strong>".config('params.LANG_LABEL_COMPANY').": </strong>".$contact->company."<br />";
            $sitemgr_msg .= "<strong>".config('params.LANG_LABEL_ADDRESS').": </strong>".$contact->address." ".$contact->address2."<br />";
            $sitemgr_msg .= "<strong>".config('params.LANG_LABEL_CITY').": </strong>".$contact->city."<br />";
            $sitemgr_msg .= "<strong>".config('params.LANG_LABEL_STATE').": </strong>".$contact->state."<br />";
            $sitemgr_msg .= "<strong>".ucfirst(config('params.ZIPCODE_LABEL')).": </strong>".$contact->zip."<br />";
            $sitemgr_msg .= "<strong>".config('params.LANG_LABEL_COUNTRY').": </strong>".$contact->country."<br />";
            $sitemgr_msg .= "<strong>".config('params.LANG_LABEL_PHONE').": </strong>".$contact->phone."<br />";
            $sitemgr_msg .= "<strong>".config('params.LANG_LABEL_EMAIL').": </strong>".$contact->email."<br />";
            $sitemgr_msg .= "<br /><a href=\"".url()."/".config('params.SITEMGR_ALIAS')."/account/view.php?id=".$account->getNumber("id")."\" target=\"_blank\">".url()."/".config('params.SITEMGR_ALIAS')."/account/view.php?id=".$account->getNumber("id")."</a><br /><br />";
            $sitemgr_msg .= "Listing:<br /><br />";
            $sitemgr_msg .= "<strong>Title: </strong>".$listing->getString("title")."<br />";
            //$sitemgr_msg .= "<br /><a href=\"".((SSL_ENABLED == "on" && FORCE_SITEMGR_SSL == "on") ? SECURE_URL : NON_SECURE_URL)."/".SITEMGR_ALIAS."/".LISTING_FEATURE_FOLDER."/view.php?id=".$listing->getNumber("id")."\" target=\"_blank\">".((SSL_ENABLED == "on" && FORCE_SITEMGR_SSL == "on") ? SECURE_URL : NON_SECURE_URL)."/".SITEMGR_ALIAS."/".LISTING_FEATURE_FOLDER."/view.php?id=".$listing->getNumber("id")."</a><br /><br />";

            //setting_get("new_listing_email", $new_listing_email);

                $data= [
            'sitemgr_msg' => $sitemgr_msg,
            'emailSubject' => $emailSubject,
        ];

    Mail::send('email.help',['data' => $data], function($message) use ($data) {
   
        $message->from('amoosjohn@gmail.com');
     $message->to('amoosjohn@gmail.com')->cc('amoosjohn@gmail.com')->subject($data['emailSubject']);
    });

            /*if ($new_listing_email) {
                system_notifySitemgr($sitemgr_account_emails, $emailSubject, $sitemgr_msg, true, "", "", true, $sitemgr_listing_emails);
            }*/


            //if ($checkout) $payment_method = "checkout";

           Session::put('SESS_ACCOUNT_ID', $account->id); 
           $name=$request->first_name." ".$request->last_name ;
           Session::put('SESS_ACCOUNT_NAME',$name);
           Cookie::make("username_members", $account->getString("username"), 60);
           Cookie::make("automatic_login_members", "true", 60);

            if ($payment_method == "checkout") {
               $redirectURL='order_listing';
                //$redirectURL = DEFAULT_URL."/".MEMBERS_ALIAS."/".LISTING_FEATURE_FOLDER."/listing.php?id=".$listing->getNumber("id")."&process=signup";
            } elseif ($payment_method == "invoice") {
                $redirectURL='signup/invoice';
                
            } else {

                 $redirectURL='signup/payment/'.$payment_method;

            }

            return Redirect($redirectURL);



        } 

        else
        {

            return redirect()->back()->withInput(Input::all())->with('message_contact',$message_contact)->with('message_account',$message_account); 

        }


    }
  }
   
  public function signupInvoice()
    {

        $payment_method="invoice";
        $second_step=1;
        $params=array();
         $userAccountId = Session::get('SESS_ACCOUNT_ID');
        $profileSp = Functions::SponsersProfile();
        $ownerName= '';
        if(!empty($profileSp))
        {
           $ownerName = $profileSp[0]->first_name.' '.$profileSp[0]->last_name.', ';
       }
       $meta_data = Sql::sponsersMeta($userAccountId);
       $headerTitle = $ownerName.'Welcome to the Sponsor Section -'.$meta_data[0]->value;
       $headerAuthor = $meta_data[1]->value;
       $headerDescription = $meta_data[2]->value;
       $headerKeywords = $meta_data[3]->value;
        $footer_text = Sql::footerText();
       // $meta_data = Sql::HomePageSummerSale($params);
        
        $header_author = Sql::HomePageHeaderAuthor();
        
        $header_searching_tag = Sql::HomePageHeaderSearchingTag();
        
        $params['type'] = 'Sign up Invoice';
        $signup_payment = Sql::ContentText($params);

        $login='Login';
        Session::put('SESS_LOGIN', $login);


          //Second Step
       if ($second_step && !$payment_method) {
        $payment_message = config('params.LANG_MSG_NO_PAYMENT_METHOD_SELECTED');
        }

        $max_item_sum=50;
        $sess_id=Functions::sess_getAccountIdFromSession();



        if (isset($second_step)) {
        $itemCount = 0;    
         $all_listings = Sql::getBillingListing($sess_id,'title'); 
        foreach ($all_listings as $all_listing) {
            $listing_id[] = $all_listing->id;
            $discountlisting_id[] = $all_listing->discount_id;
            $itemCount++;
        } 

      


        if($listing_id) {
            for ($i=0; $i < count($listing_id); $i++) {

                    $list_id=$listing_id[$i];
                    $listingObj = new Listing();
                    $listingObj->Listing($list_id);
                    if (!Functions::is_valid_discount_code($discountlisting_id[$i], "listing", $listing_id[$i], $payment_message, $error_num)) {

                    $payment_message = Functions::string_substr($payment_message, 0 ,-1);
        $payment_message .= "On Listings \"<b>".$listingObj->getString("title")."</b>\"<br />";

                    }   
                    elseif (is_array($discountlisting_id)){
                        $listingObj->setString("discount_id", $discountlisting_id[$i]);
                        $listingObj->Save();
                        unset($listingObj);
                    }

                    $listings[]= Sql::getBillingListingId($listing_id[$i],$sess_id);


            }
        }   
    } 
         $listingid=0;
        $bill_info["total_bill"]=0;
        if (isset($listings)) {

            $levelObj = new ListingLevel();
            $levelObj->ListingLevel(true);
            
            foreach ($listings as $each_listing) {
                if (isset($second_step)) {
                    $auxListingObj = new Listing();
                    $auxListingObj->Listing($each_listing[0]->id);
                    $each_listing[0]->renewal_date = $auxListingObj->getNextRenewalDate();
                    unset($auxListingObj);
                }

                $category_amount = 0;
                $sql = "SELECT category_id FROM Listing_Category WHERE listing_id = {$each_listing[0]->id}";
                $result = Sql::fetch($sql);

                if(count($result)){
                    foreach($result as $row){
                        $category_amount++;
                    }

                }
                // setting some of the bill information
                //d($bill_info["listings"],1);die;
                $bill_info["listings"]["{$each_listing[0]->id}"]["id"]           = htmlspecialchars($each_listing[0]->id);
                $bill_info["listings"]["{$each_listing[0]->id}"]["title"]           = htmlspecialchars($each_listing[0]->title);
                $bill_info["listings"]["{$each_listing[0]->id}"]["address"]         = htmlspecialchars($each_listing[0]->address);
                $bill_info["listings"]["{$each_listing[0]->id}"]["level"]           = $each_listing[0]->name;
                $bill_info["listings"]["{$each_listing[0]->id}"]["level_number"]    = $each_listing[0]->level;
                $bill_info["listings"]["{$each_listing[0]->id}"]["logo"]            = ($each_listing[0]->image_id) ? 1 : 0;
                $bill_info["listings"]["{$each_listing[0]->id}"]["url"]             = ($each_listing[0]->url) ? 1 : 0;
                $bill_info["listings"]["{$each_listing[0]->id}"]["category_amount"] = ($category_amount) ? $category_amount : 0;
                $bill_info["listings"]["{$each_listing[0]->id}"]["renewal_date"]    = $each_listing[0]->renewal_date;
                $bill_info["listings"]["{$each_listing[0]->id}"]["discount_id"]     = $each_listing[0]->discount_id;
                $bill_info["listings"]["{$each_listing[0]->id}"]["status"]          = $each_listing[0]->status;


                $discountAmount = $discountType = null;
                if ($each_listing[0]->discount_id) {
                    $sql = "SELECT `amount`, `type` FROM Discount_Code WHERE id = '" .$each_listing[0]->discount_id . "'";
                    $result = Sql::fetch($sql);
                    foreach($result as $row){
                        $discountAmount = $row->amount;
                        $discountType = $row->type;
                    }
                }
                $bill_info["listings"]["{$each_listing[0]->id}"]["discount_amount"] = $discountAmount;
                $bill_info["listings"]["{$each_listing[0]->id}"]["discount_type"] = $discountType;


                // Bill pricing
                $thisListing = new Listing();
                $bill_info["listings"]["{$each_listing[0]->id}"]["total_fee"] = $thisListing->getListingPrice($each_listing[0]->level);


                unset($thisListing);
                if ($bill_info["listings"]["{$each_listing[0]->id}"]["total_fee"] <= 0) {
                    unset($bill_info["listings"]["{$each_listing[0]->id}"]);
                    continue;
                }
                // Need To Check Out
                $thisListing = new Listing();
                $thisListing->Listing($each_listing[0]->id); 
                if ($thisListing->needToCheckOut()) 
                    $bill_info["listings"]["{$each_listing[0]->id}"]["needtocheckout"] = "y";
                else $bill_info["listings"]["{$each_listing[0]->id}"]["needtocheckout"] = "n";

                unset($thisListing);

                if (($category_amount > 0) && (($category_amount - $levelObj->getFreeCategory($each_listing[0]->level)) > 0)) {
                    $bill_info["listings"]["{$each_listing[0]->id}"]["extra_category_amount"] = $category_amount - $levelObj->getFreeCategory($each_listing[0]["level"]);
                } else {
                    $bill_info["listings"]["{$each_listing[0]->id}"]["extra_category_amount"] = 0;
                }

                // Money format for listing fees
                if($bill_info["listings"]["{$each_listing[0]->id}"]["total_fee"])
                 if($bill_info["listings"]["{$each_listing[0]->id}"]["discount_type"] === 'monetary value'):
                   $bill_info["listings"]["{$each_listing[0]->id}"]["total_fee"]=Functions::format_money($bill_info["listings"]["{$each_listing[0]->id}"]["total_fee"]-$bill_info["listings"]["{$each_listing[0]->id}"]["discount_amount"]);
                     elseif($bill_info["listings"]["{$each_listing[0]->id}"]["discount_type"] === 'percentage'):
                    $bill_info["listings"]["{$each_listing[0]->id}"]["total_fee"]=Functions::format_money(floatval($bill_info["listings"]["{$each_listing[0]->id}"]["total_fee"])/((100-floatval($bill_info["listings"]["{$each_listing[0]->id}"]["discount_amount"]))/100)-$bill_info["listings"]["{$each_listing[0]->id}"]["total_fee"]);
                    else: 
                    $bill_info["listings"]["{$each_listing[0]->id}"]["total_fee"] = Functions::format_money($bill_info["listings"]["{$each_listing[0]->id}"]["total_fee"]); 
                   endif;
                


                    // Setting total bill
                
                if($bill_info["listings"]["{$each_listing[0]->id}"]["total_fee"])
                {
                    $bill_info["total_bill"] += $bill_info["listings"]["{$each_listing[0]->id}"]["total_fee"];
                }

            }
        if (empty($bill_info["listings"])) unset($bill_info["listings"]);

        }

        if ($second_step) { 
            if (($payment_method == "invoice") && ($bill_info["total_bill"] > 0)) {

            //$invoiceStatusObj = new InvoiceStatus();
            $accountObj = new Account();
            $account=$accountObj->Account($sess_id);
            //d($accountObj->username);die;
            $arr_invoice["account_id"]      = $sess_id;
            $arr_invoice["username"]        = $accountObj->getString("username");
            $arr_invoice["ip"]              = $_SERVER["REMOTE_ADDR"];
            $arr_invoice["date"]            = date("Y")."-".date("m")."-".date("d")." ".date("H").":".date("i").":".date("s");

            $arr_invoice["status"]          = 'P'; //$invoiceStatusObj->getDefault()


            if (isset($payment_tax_status) == "on")
                        $arr_invoice["tax_amount"]  = $payment_tax_value;
                    else
                        $arr_invoice["tax_amount"]  = 0;

                    // SUBTOTAL AMOUNT
                    $arr_invoice["subtotal_amount"] = str_replace(",","",$bill_info["total_bill"]);

                    // TOTAL AMOUNT
                    if (isset($payment_tax_status)  == "on")
                        $arr_invoice["amount"]      = Functions::payment_calculateTax(str_replace(",","",$bill_info["total_bill"]), $payment_tax_value, false);
                    else
                        $arr_invoice["amount"]      = str_replace(",","",$bill_info["total_bill"]);
                    
                    $arr_invoice["currency"]        = config('params.INVOICEPAYMENT_CURRENCY');
                    $arr_invoice["expire_date"]     = date("Y-m-d",mktime(0,0,0,date("m")+1,date("d"),date("Y")));

                    $invoiceObj = new Invoice();
                    $invoices=$invoiceObj->Invoice($arr_invoice);
                    //d($invoices,1);die;
                    $invoiceObj->Save();

                    $bill_info["invoice_number"] = $invoiceObj->id;


                        if ($bill_info["listings"]) 
                        foreach ($bill_info["listings"] as $id => $info)
                        {

                        $listingid=$id;    
                        $listingObj = Sql::BillingInformation($id); 
                        //$listingObj = new Listing();
                        //$listingObj->Listing($id);
                        $levelObj = new ListingLevel();
                        $levelObj->ListingLevel(true);

                        $category_amount = 0;
                        $sql = "SELECT category_id FROM Listing_Category WHERE listing_id = ".$listingObj[0]->id."";
                        $result = Sql::fetch($sql);
                        if(count($result)){
                        foreach($result as $row){
                        $category_amount++;
                            }

                        }

                        $arr_invoice_listing["invoice_id"]    = $invoiceObj->id;
                        $arr_invoice_listing["listing_id"]    = $id;
                        $arr_invoice_listing["listing_title"] = $listingObj[0]->title;
                        $arr_invoice_listing["discount_id"]   = $listingObj[0]->discount_id;
                        $arr_invoice_listing["level"]         = $listingObj[0]->level;
                        $arr_invoice_listing["level_label"]   = $listingObj[0]->name;
                        $arr_invoice_listing["renewal_date"]  = $listingObj[0]->renewal_date;
                        $arr_invoice_listing["categories"]    = ($category_amount) ? $category_amount : 0;
                        $arr_invoice_listing["amount"]        = str_replace(",","",$info["total_fee"]);

                        $arr_invoice_listing["extra_categories"] = 0;
                        if (($category_amount > 0) && (($category_amount - $levelObj->getFreeCategory($listingObj[0]->level)) > 0)) {
                            $arr_invoice_listing["extra_categories"] = $category_amount - $levelObj->getFreeCategory($listingObj[0]->level);
                        } else {
                          $arr_invoice_listing["extra_categories"] = 0;
                        }

                        $arr_invoice_listing["listingtemplate_title"] = "";
                        if (config('params.LISTINGTEMPLATE_FEATURE') == "on" && config('CUSTOM_LISTINGTEMPLATE_FEATURE') == "on") {

                            if ($listingObj->getString("listingtemplate_id")) {
                                $listingTemplateObj = new ListingTemplate($listingObj->getString("listingtemplate_id"));
                                $arr_invoice_listing["listingtemplate_title"] = $listingTemplateObj->getString("title", false);
                            }
                        }

                        $invoiceListingObj = new InvoiceListing();
                        $invoiceListingObj->InvoiceListing($arr_invoice_listing);
                        $invoiceListingObj->Save();

                        unset($listingObj);
                        unset($invoiceListingObj);

                    }
                }
            }

         $contact = new Contact();
        $contact->Contact($sess_id);
        // sending e-mail to user
   if ($emailNotificationObj = Functions::system_checkEmail(config('params.SYSTEM_INVOICE_NOTIFICATION'))) {
                $subject = $emailNotificationObj->getString("subject");
                $body = $emailNotificationObj->getString("body");
                $aux = explode("ACCOUNT_NAME",$body);
                $body = $aux[0].$contact->first_name." ".$contact->last_name.$aux[1];
                 $aux2 = explode("DEFAULT_URL",$body);
                $body = $aux2[0].url()."/".config('params.MEMBERS_ALIAS')."/billing/invoice/".$bill_info["invoice_number"]."\n".$aux2[1];

                if ($bill_info["listings"]) {
               $body = Functions::system_replaceEmailVariables($body, $listingid, 'listing');
               $subject = Functions::system_replaceEmailVariables($subject, $listingid, 'listing');    
                 }

                $body = html_entity_decode($body);
                $subject = html_entity_decode($subject);                 
                $data= [
            'sitemgr_msg' => $body,
            'subject' => $subject,
            'email_send' =>$contact->getString("email")
        ];

    Mail::send(['text' => 'email.signup'],['data' => $data], function($message) use ($data) {
   
        $message->from('amoosjohn@gmail.com');
     $message->to($data['email_send'])->cc('amoosjohn@gmail.com')->subject($data['subject']);
    });

       
            }     

        
        return view('front.sponsors_signup_invoice',compact('meta_data','headerTitle','headerAuthor','headerDescription','headerKeywords','footer_text','payment','bill_info','login'));
    
    }

  public function signupPayment(Request $request)
    {
        $payment_method=$request->payment_method;
        $second_step=1;
        $params=array();
         $userAccountId = Session::get('SESS_ACCOUNT_ID');
        $profileSp = Functions::SponsersProfile();
        $ownerName= '';
        if(!empty($profileSp))
        {
           $ownerName = $profileSp[0]->first_name.' '.$profileSp[0]->last_name.', ';
       }
       $meta_data = Sql::sponsersMeta($userAccountId);
       $headerTitle = $ownerName.'Welcome to the Sponsor Section -'.$meta_data[0]->value;
       $headerAuthor = $meta_data[1]->value;
       $headerDescription = $meta_data[2]->value;
       $headerKeywords = $meta_data[3]->value;
        $footer_text = Sql::footerText();
         $params['type'] = 'Sign up Payment';
        $meta_data = Sql::HomePageSummerSale($params);
        
        $header_author = Sql::HomePageHeaderAuthor();
        
        $header_searching_tag = Sql::HomePageHeaderSearchingTag();
        
        $params['type'] = 'Sign up Payment';
        $signup_payment = Sql::ContentText($params);

        $login='Login';
        Session::put('SESS_LOGIN', $login);


          //Second Step
       if ($second_step && !$payment_method) {
        $payment_message = config('params.LANG_MSG_NO_PAYMENT_METHOD_SELECTED');
        }

        $max_item_sum=50;
        $sess_id=Functions::sess_getAccountIdFromSession();

        if (isset($second_step)) {
        $itemCount = 0;    
         $all_listings = Sql::getBillingListing($sess_id,'title'); 
        foreach ($all_listings as $all_listing) {
            $listing_id[] = $all_listing->id;
            $discountlisting_id[] = $all_listing->discount_id;
            $itemCount++;
        } 


        if($listing_id) {
            for ($i=0; $i < count($listing_id); $i++) {

                    $list_id=$listing_id[$i];
                    $listingObj = new Listing();
                    $listingObj->Listing($list_id);
                    if (!Functions::is_valid_discount_code($discountlisting_id[$i], "listing", $listing_id[$i], $payment_message, $error_num)) {

                    $payment_message = Functions::string_substr($payment_message, 0 ,-1);
        $payment_message .= "On Listings \"<b>".$listingObj->getString("title")."</b>\"<br />";

                    }   
                    elseif (is_array($discountlisting_id)){
                        $listingObj->setString("discount_id", $discountlisting_id[$i]);
                        $listingObj->Save();
                        unset($listingObj);
                    }

                    $listings[]= Sql::getBillingListingId($listing_id[$i],$sess_id);


            }
        }   
    } 
        
        $bill_info["total_bill"]=0;
        if (isset($listings)) {

            $levelObj = new ListingLevel();
            $levelObj->ListingLevel(true);
            
            foreach ($listings as $each_listing) {
                if (isset($second_step)) {
                    $auxListingObj = new Listing();
                    $auxListingObj->Listing($each_listing[0]->id);
                    $each_listing[0]->renewal_date = $auxListingObj->getNextRenewalDate();
                    unset($auxListingObj);
                }

                $category_amount = 0;
                $sql = "SELECT category_id FROM Listing_Category WHERE listing_id = {$each_listing[0]->id}";
                $result = Sql::fetch($sql);

                if(count($result)){
                    foreach($result as $row){
                        $category_amount++;
                    }

                }
                // setting some of the bill information
                //d($bill_info["listings"],1);die;
                $bill_info["listings"]["{$each_listing[0]->id}"]["id"]           = htmlspecialchars($each_listing[0]->id);
                $bill_info["listings"]["{$each_listing[0]->id}"]["title"]           = htmlspecialchars($each_listing[0]->title);
                $bill_info["listings"]["{$each_listing[0]->id}"]["address"]         = htmlspecialchars($each_listing[0]->address);
                $bill_info["listings"]["{$each_listing[0]->id}"]["level"]           = $each_listing[0]->name;
                $bill_info["listings"]["{$each_listing[0]->id}"]["level_number"]    = $each_listing[0]->level;
                $bill_info["listings"]["{$each_listing[0]->id}"]["logo"]            = ($each_listing[0]->image_id) ? 1 : 0;
                $bill_info["listings"]["{$each_listing[0]->id}"]["url"]             = ($each_listing[0]->url) ? 1 : 0;
                $bill_info["listings"]["{$each_listing[0]->id}"]["category_amount"] = ($category_amount) ? $category_amount : 0;
                $bill_info["listings"]["{$each_listing[0]->id}"]["renewal_date"]    = $each_listing[0]->renewal_date;
                $bill_info["listings"]["{$each_listing[0]->id}"]["discount_id"]     = $each_listing[0]->discount_id;
                $bill_info["listings"]["{$each_listing[0]->id}"]["status"]          = $each_listing[0]->status;


                $discountAmount = $discountType = null;
                if ($each_listing[0]->discount_id) {
                    $sql = "SELECT `amount`, `type` FROM Discount_Code WHERE id = '" .$each_listing[0]->discount_id . "'";
                    $result = Sql::fetch($sql);
                    foreach($result as $row){
                        $discountAmount = $row->amount;
                        $discountType = $row->type;
                    }
                }
                $bill_info["listings"]["{$each_listing[0]->id}"]["discount_amount"] = $discountAmount;
                $bill_info["listings"]["{$each_listing[0]->id}"]["discount_type"] = $discountType;


                // Bill pricing
                $thisListing = new Listing();
                $bill_info["listings"]["{$each_listing[0]->id}"]["total_fee"] = $thisListing->getListingPrice($each_listing[0]->level);


                unset($thisListing);
                if ($bill_info["listings"]["{$each_listing[0]->id}"]["total_fee"] <= 0) {
                    unset($bill_info["listings"]["{$each_listing[0]->id}"]);
                    continue;
                }
                // Need To Check Out
                $thisListing = new Listing();
                $thisListing->Listing($each_listing[0]->id); 
                if ($thisListing->needToCheckOut()) 
                    $bill_info["listings"]["{$each_listing[0]->id}"]["needtocheckout"] = "y";
                else $bill_info["listings"]["{$each_listing[0]->id}"]["needtocheckout"] = "n";

                unset($thisListing);

                if (($category_amount > 0) && (($category_amount - $levelObj->getFreeCategory($each_listing[0]->level)) > 0)) {
                    $bill_info["listings"]["{$each_listing[0]->id}"]["extra_category_amount"] = $category_amount - $levelObj->getFreeCategory($each_listing[0]["level"]);
                } else {
                    $bill_info["listings"]["{$each_listing[0]->id}"]["extra_category_amount"] = 0;
                }

                // Money format for listing fees
                if($bill_info["listings"]["{$each_listing[0]->id}"]["total_fee"])
                 if($bill_info["listings"]["{$each_listing[0]->id}"]["discount_type"] === 'monetary value'):
                   $bill_info["listings"]["{$each_listing[0]->id}"]["total_fee"]=Functions::format_money($bill_info["listings"]["{$each_listing[0]->id}"]["total_fee"]-$bill_info["listings"]["{$each_listing[0]->id}"]["discount_amount"]);
                     elseif($bill_info["listings"]["{$each_listing[0]->id}"]["discount_type"] === 'percentage'):
                    $bill_info["listings"]["{$each_listing[0]->id}"]["total_fee"]=Functions::format_money(floatval($bill_info["listings"]["{$each_listing[0]->id}"]["total_fee"])/((100-floatval($bill_info["listings"]["{$each_listing[0]->id}"]["discount_amount"]))/100)-$bill_info["listings"]["{$each_listing[0]->id}"]["total_fee"]);
                    else: 
                    $bill_info["listings"]["{$each_listing[0]->id}"]["total_fee"] = Functions::format_money($bill_info["listings"]["{$each_listing[0]->id}"]["total_fee"]); 
                   endif;
                


                    // Setting total bill
                
                if($bill_info["listings"]["{$each_listing[0]->id}"]["total_fee"])
                {
                    $bill_info["total_bill"] += $bill_info["listings"]["{$each_listing[0]->id}"]["total_fee"];
                }

            }
        if (empty($bill_info["listings"])) unset($bill_info["listings"]);

        }



        
        return view('front.sponsors_signup_payment',compact('meta_data','header_author','header_searching_tag','footer_text','payment','bill_info','headerTitle','payment_method'));
    
    }

  public function cron_manager()
  {
    return view('cron_manager');
  }


    
    
    
    
    
}