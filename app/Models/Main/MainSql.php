<?php namespace App\Models\Main;

use Illuminate\Database\Eloquent\Model;

use DB;

class MainSql extends Model {
    

    public static function AdvanceSeachStatesData($states=0)
    {
        $location_3=config('tables.main.location_3');
        $sql="  SELECT 
        id,
        name 
        FROM
        ".$location_3." 
        WHERE id IN (".$states.") 
        ORDER BY name";
        $result=self::fetch($sql);
        return $result;
    }
    
    
    
    public static function AdvanceLocation($state=0,$listing_ids='')
    {
        $location_4=config('tables.main.location_4');
        $sql="SELECT 
        id,
        name 
        FROM
        ".$location_4." 
        WHERE id IN (
        ".$listing_ids."
        ) 
        AND location_3 = ".$state." 
        ORDER BY NAME ";
        $result=self::fetch($sql);
        return $result;
    }
    
    public static function AccountInfo($account_id=0)
    {
        $contact=config('tables.main.contact');
        $sql="
        SELECT 
        first_name,last_name,
        phone,
        email,
        url 
        FROM
        ".$contact." 
        WHERE account_id = ".$account_id;
        $result=self::fetch($sql);
        return $result;
    }
    
   public static function userAuthanticate($username='',$password='')
   {
    $sql = "SELECT faillogin_count, faillogin_datetime FROM Account WHERE foreignaccount = 'n' AND username = '".$username."'";
    $result=self::fetch($sql);
    if(!empty($result))
    {
        $faillogin_count = $result[0]->faillogin_count;
		$faillogin_datetime = $result[0]->faillogin_datetime;
		if (($faillogincount = (int)($faillogin_count / (config('params.FAILLOGIN_MAXFAIL')+1))) > 0) {
			if (($faillogin_count % (config('params.FAILLOGIN_MAXFAIL')+1)) == 0) {
				$faillogindatetime = preg_split("/[-, ,:]+/", $faillogin_datetime);
				$failloginnow = preg_split("/[-, ,:]+/", date("Y-m-d H:i:s"));
				if (($failloginsec = (mktime($failloginnow[3], $failloginnow[4], $failloginnow[5], $failloginnow[1], $failloginnow[2], $failloginnow[0]) - mktime($faillogindatetime[3], $faillogindatetime[4], $faillogindatetime[5], $faillogindatetime[1], $faillogindatetime[2], $faillogindatetime[0]))) < ($faillogincount*config('params.FAILLOGIN_TIMEBLOCK')*60)) {
					$authmessage = config('params.LANG_MSG_ACCOUNTLOCKED1')." ".(($faillogincount*config('params.FAILLOGIN_TIMEBLOCK'))-(int)($failloginsec/60))." ".config('params.FAILLOGIN_TIMEBLOCK');
					return $authmessage;
				}
			}
		}
        ####################################################################################################
		### END - MEMBER
		####################################################################################################

		$sql = "SELECT * FROM Account WHERE (foreignaccount = 'n' AND username = '".$username."' AND password = '".((((config('params.PASSWORD_ENCRYPTION')) == "on") ? md5($password) : $password))."') OR (foreignaccount = 'y' AND facebook_username LIKE 'facebook%' AND username = '".$username."' AND password = '".((((config('params.PASSWORD_ENCRYPTION')) == "off") ? md5($password) : $password))."')";

		$result=self::fetch($sql);
		if (!empty($result)) {

			$sql = "UPDATE Account SET faillogin_count = 0, faillogin_datetime = '0000-00-00 00:00:00' WHERE foreignaccount = 'n' AND username = '".$username."'";
			self::updateMain($sql);
            $loginSQL = "INSERT INTO Report_Login (datetime, ip, type, page, username) values (NOW(), '".$_SERVER["REMOTE_ADDR"]."', 'login', '".$_SERVER["PHP_SELF"]."', '".$username."')";
		    self::insertSql($loginSQL);
        
			return $result[0]->id;
		} else {
			//$sql = "UPDATE Account SET faillogin_count = faillogin_count + 1, faillogin_datetime = NOW() WHERE foreignaccount = 'n' AND username = ".db_formatString($username)."";
			//$dbObj->query($sql);
			return config('params.LANG_MSG_USERNAME_OR_PASSWORD_INCORRECT');
			
		}
    }
    else
    {
        return config('params.LANG_MSG_USERNAME_OR_PASSWORD_INCORRECT');
    }
    
   } 
    
     public static function getLocation($sql='')
    {
        $result=self::fetch($sql);
        return $result;
    }
    
    public static function ppi_listingsUpdate($sql='')
    {
        $data=DB::update($sql);
        return $data;
    }
    
    
    public static function fetch($sql)
    {
        $data=DB::select($sql);
        return $data;
    }
    
    public static function updateMain($sql)
    {
        $data=DB::update($sql);
        return $data;
    }
    
    public static function insert($sql)
    {
        $data=DB::insert($sql);
        return $data;
    }
    
    public static function insertSql($sql)
    {
        $data= DB::connection('domain')->insert($sql);
        return $data;
    }

     public static function fetchSql($sql)
      {
            $data= DB::select($sql)->get()->toArray();
            return $data;
      }
    
}
