<?php 

namespace App\Models\Domain;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Functions\Functions;
class Sql extends Model {

    
    static function getHomePageSlider($params=array())
    {
        $slider=config('tables.domain.slider');
        $image=config('tables.domain.image');
        $sql="SELECT 
        s.id as slider_id,s.alternative_text,
        i.id as image_id,i.prefix ,i.type,i.width,i.height
        FROM
        ".$slider." s 
        JOIN ".$image." i 
        ON s.image_id = i.id 
        WHERE s.image_id > 0 
        ORDER BY s.slide_order 
        LIMIT 4 ";
        $result=self::fetch($sql);
        return $result;
        
    }
    
    static function getHomePageSpecialSection($params=array())
    {
        $editor_choice=config('tables.domain.editor_choice');
        $image=config('tables.domain.image');
        $sql="SELECT 
        ec.id AS editor_choice_id,ec.name,
        i.id AS image_id,i.prefix ,i.type,i.width,i.height
        FROM
        ".$editor_choice." ec JOIN ".$image." i ON ec.image_id=i.id
        
        ORDER BY ec.id ";
        $result=self::fetch($sql);
        return $result;
        
    }

    static function getHomePageFeaturedRental($params=array())
    {
        $listing_summary=config('tables.domain.listing_summary');
        $image=config('tables.domain.image');
        $sql="SELECT 
        ls.id AS rental_id,ls.location_4_title,ls.location_3_title,ls.sleeps,ls.bedroom,ls.bathroom,ls.title,
        i.id AS image_id,i.prefix ,i.type,i.width,i.height
        FROM
        ".$listing_summary." ls 
        JOIN ".$image." i 
        ON ls.image_id = i.id 
        WHERE ls.status ='A' AND (ls.is_featured =1 AND ls.renewal_date >= CURDATE() ) OR ls.level = 10
        ORDER BY RAND() 
        LIMIT 0, 5 ;";
        $result=self::fetch($sql);
        
        $reportSql = "INSERT INTO Report_Listing (
        listing_id,
        report_type,
        report_amount,
        DATE
        ) 
        VALUES
        ";
        $reportSql_temp='';
        foreach($result as $val)
        {
          $reportSql_temp.='('.$val->rental_id.', 1, 1, NOW()),';
        }
        $reportSql.= rtrim($reportSql_temp, ',');
       $reportSql.= " ON DUPLICATE KEY 
        UPDATE 
        report_amount = report_amount + 1 ;";
        DB::connection('domain')->raw($reportSql);
        return $result;
        
    }
    
    static function getHomePageBanners($params=array())
    {
        $banner=config('tables.domain.banner');
        $image=config('tables.domain.image');
        $banner_level = config('tables.domain.banner_level');
        $report_banner = config('tables.domain.report_banner');
        $sql_value="SELECT value FROM ".$banner_level." WHERE name = 'middle' AND theme = 'default'  AND `active` = 'y'  LIMIT 1";
        $value=self::fetch($sql_value);
        $sql="SELECT 
        b.id,
        b.caption,
        b.destination_protocol,
        b.destination_url,
        i.id AS image_id,
        i.prefix,
        i.type,
        i.width,
        i.height 
        FROM
        ".$banner." b 
        JOIN ".$image." i 
        ON b.image_id = i.id 
        WHERE b.status = 'A' 
        AND (
        b.expiration_setting = '2' 
        OR (
        b.expiration_setting = '1' 
        AND b.impressions > 0
        ) 
        OR (
        b.expiration_setting = '1' 
        AND b.unlimited_impressions = 'y'
        )
        ) 
        AND b.type = ".$value[0]->value." 
        AND (
        b.section = 'general' 
        OR b.section = 'global'
        ) 
        ORDER BY RAND() 
        LIMIT 4";
        $result=self::fetch($sql);
        $ids_banner = array();
        if(!empty($result))
        {
            foreach($result as $val)
            {
                $ids_banner[] = $val->id;
                 
            }

            if(!empty($ids_banner))
            {
                $sql_update_report = "UPDATE ".$report_banner." SET report_amount = report_amount + 1 WHERE banner_id IN (".implode(',',$ids_banner).") AND report_type = 2 AND DATE_FORMAT(date, '%Y-%m-%d') = DATE_FORMAT(NOW(), '%Y-%m-%d')";
                self::updateSql($sql_update_report); 
            }
            
        }
        
       
        return $result;
        
    }
    
    static function getHomePageBannersSecondFour($params=array())
    {
        $banner=config('tables.domain.banner');
        $image=config('tables.domain.image');
        $banner_level = config('tables.domain.banner_level');
        $report_banner = config('tables.domain.report_banner');
        $sql_value="SELECT value FROM ".$banner_level." WHERE name = 'bottom' AND theme = 'default'  AND `active` = 'y'  LIMIT 1";
        $value=self::fetch($sql_value);
        $sql="SELECT 
        b.id,
        b.caption,
        b.destination_protocol,
        b.destination_url,
        i.id AS image_id,
        i.prefix,
        i.type,
        i.width,
        i.height 
        FROM
        ".$banner." b 
        JOIN ".$image." i 
        ON b.image_id = i.id 
        WHERE b.status = 'A' 
        AND (
        b.expiration_setting = '2' 
        OR (
        b.expiration_setting = '1' 
        AND b.impressions > 0
        ) 
        OR (
        b.expiration_setting = '1' 
        AND b.unlimited_impressions = 'y'
        )
        ) 
        AND b.type = ".$value[0]->value." 
        AND (
        b.section = 'general' 
        OR b.section = 'global'
        ) 
        ORDER BY RAND() 
        LIMIT 4";
        $result=self::fetch($sql);
        $ids_banner = array();
        if(!empty($result))
        {
            foreach($result as $val)
            {
                $ids_banner[] = $val->id;
                 
            }

            if(!empty($ids_banner))
            {
                $sql_update_report = "UPDATE ".$report_banner." SET report_amount = report_amount + 1 WHERE banner_id IN (".implode(',',$ids_banner).") AND report_type = 2 AND DATE_FORMAT(date, '%Y-%m-%d') = DATE_FORMAT(NOW(), '%Y-%m-%d')";
                self::updateSql($sql_update_report); 
            }
            
        }
        
       
        return $result;
        
    }
        
    static function footerText($params=array())
    {
        $customtext=config('tables.domain.customtext');
        $sql="SELECT 
        * 
        FROM
        ".$customtext." 
        WHERE NAME = 'footer_copyright' ";
        $result=self::fetch($sql);
        return $result;
        
    }

   static function HomePageSummerSale($params=array())    
    {
        $content=config('tables.domain.content');
        $sql=" SELECT 
        * 
        FROM
        ".$content." 
        WHERE TYPE = '".$params['type']."' ";
        $result=self::fetch($sql);
        return $result;
    }

        
    static function HomePageHeaderAuthor($params=array())
    {
        $customtext=config('tables.domain.customtext');
        $sql=" SELECT 
        NAME, VALUE 
        FROM
        ".$customtext."  
        WHERE (NAME = 'header_author') ";
        $result=self::fetch($sql);
        return $result;
        
    }
    
    
    static function HomePageHeaderSearchingTag($params=array())
    {
        $setting_search_tag=config('tables.domain.setting_search_tag');
        $sql=" SELECT 
        * 
        FROM
        ".$setting_search_tag." 
        ORDER BY id";
        $result=self::fetch($sql);
        return $result;
        
    }
    
      
    public static function AdvanceSeachStates()
    {
        $listing=config('tables.domain.listing');
        $sql="SELECT  DISTINCT 
        location_3 
        FROM
        ".$listing."  
        WHERE STATUS <> 'E'  ";
        $result_obj=self::fetch($sql);
        $result=array();
        if(!empty($result_obj))
        {
            foreach($result_obj as $val)
            {
                $result[] = $val->location_3;
            }
            return $result;
        }
        else
        {
            return array();
        }
        
    }
    
    
    
    public static function DevelopmentData()
    {
        $listingdevelopment=config('tables.domain.listingdevelopment');
        $listing=config('tables.domain.listing');
        
        $sql="SELECT DISTINCT 
        ld.`id`,ld.`name` 
        FROM
        `".$listingdevelopment."` ld 
        LEFT JOIN `".$listing."` l
        ON l.`development_id` = ld.`id` 
        WHERE l.`status` = 'A' 
        GROUP BY ld.`id` 
        ORDER BY ld.`name` ";
        $result=self::fetch($sql);
        if(!empty($result))
        {
            return $result;
        }
        else
        {
            return array();
        }
    }
    
    public static function ContentText($params=array())
    {
        
        $content=config('tables.domain.content');
        $sql=" SELECT 
        * 
        FROM
        ".$content." 
        WHERE TYPE = '".$params['type']."' ";
        $result=self::fetch($sql);
        return $result;
        
    }
     public static function getBillingListing($sess_id=0,$order)
    {
     $listing=config('tables.domain.listing');

       $sql="SELECT lis.*,lev.value,lev.name  FROM ".$listing." lis 
       LEFT JOIN ListingLevel lev
       ON lis.level=lev.value
       WHERE lis.account_id=".$sess_id." AND lev.theme='default' ORDER BY lis.id DESC";
                $result = Sql::fetch($sql);
                $res=Array();
        foreach($result as $row)
        {
               $res[] = $row; 
         }
         return $res;       

    }

     public static function getBillingListingId($id=0,$sess_id=0)
    {
        $listing=config('tables.domain.listing');

       $sql="SELECT lis.*,lev.value,lev.name  FROM ".$listing." lis 
       LEFT JOIN ListingLevel lev
       ON lis.level=lev.value
       WHERE lis.id=".$id." AND  lis.account_id=".$sess_id." AND lev.theme='default' ORDER BY lis.id DESC";
        $result = Sql::fetch($sql);
      
         return $result;       

    }

    public static function getBadgesListing($sess_id=0,$order='')
    {
     $listing=config('tables.domain.listing_choice');

       $sql="SELECT * FROM ".$listing." WHERE account_id=".$sess_id." ORDER BY ".$order;
       $result = Sql::fetch($sql);
       $res=Array();
        foreach($result as $row)
        {
               $res[] = $row; 
        }
         
         return $res;       

    }
     public static function getBadgesListingId($id=0,$sess_id=0,$order='')
    {
     $listing=config('tables.domain.listing_choice');

       $sql="SELECT * FROM ".$listing." WHERE id=".$id." AND account_id=".$sess_id." ORDER BY ".$order;
       $result = Sql::fetch($sql);
        return $result;       

    }
    
    public static function BillingInformation($id=0)
    {

       $listing=config('tables.domain.listing');
       $sql="SELECT  lis.title,lis.id,lis.account_id,lis.level,lis.address,lis.renewal_date,
       lis.discount_id,
       lev.value,lev.name,lev.price,lev.defaultlevel,lev.theme
       FROM ".$listing." lis 
       LEFT JOIN ListingLevel lev
       ON lis.level=lev.value
       WHERE lis.id=".$id." AND lev.theme='default' ORDER BY lis.id DESC";
       $result=self::fetch($sql);
       return $result;

    }

    public static function BadgesInformation($id=0)
    {
      $editor_choice=config('tables.domain.editor_choice');
      $listing_choice=config('tables.domain.listing_choice');

        $sql="SELECT lc.id as listing_choice_id,lc.editor_choice_id,lc.listing_id,lc.renewal_date,lc.account_id,
        ec.id,ec.name,ec.price
        FROM ".$listing_choice."  lc 
        JOIN ".$editor_choice." ec 
        ON lc.editor_choice_id=ec.id
        WHERE lc.id =".$id."
        ORDER BY lc.editor_choice_id DESC";

       $result=self::fetch($sql);
       return $result;

    }
    public static function getCustomInvoice($sess_id=0)
    {
     $custominvoice=config('tables.domain.custominvoice');

       $sql="SELECT * FROM ".$custominvoice." WHERE account_id=".$sess_id." AND paid='' AND
       sent='y' ORDER BY id DESC";
        $result = Sql::fetch($sql);
        $res=Array();
        foreach($result as $row)
        {
               $res[] = $row; 
         }
         return $res;       

    }
    public static function getCustomInvoiceId($id=0,$sess_id=0)
    {
     $custominvoice=config('tables.domain.custominvoice');

       $sql="SELECT * FROM ".$custominvoice." WHERE id=".$id." AND account_id=".$sess_id." ORDER BY id";
       $result = Sql::fetch($sql);
        return $result;       

    }
    
    public static function getInvoice($hidden='',$status='',$acctId=0,$order='')
    {
        $invoice=config('tables.domain.invoice');

       $sql="SELECT *  FROM ".$invoice."
       WHERE hidden = '".$hidden."' AND status != '".$status."' AND account_id=".$acctId."
       ORDER BY ".$order." DESC";
        $result = Sql::fetch($sql);
      
         return $result;       

    }
    public static function getTransaction($hidden='',$acctId=0)
    {
        $payment_log=config('tables.domain.payment_log');

       $sql="SELECT *  FROM ".$payment_log."
       WHERE hidden = '".$hidden."'  AND account_id=".$acctId."
       ORDER BY id DESC";
        $result = Sql::fetch($sql);
      
         return $result;       

    }

    //LISTING REPORTS
    Public static function retrieveListingReport($idIn = 0) {
        //$db = db_getDBObject(DEFAULT_DB, true);
        //$dbObj = db_getDBObjectByDomainID(SELECTED_DOMAIN_ID, $db);
        $data = array();

        if ($idIn){
            
            /* empty */
            $period = date('Y') . '-' . date('n');

            $data[$period]['summary']       = 0;
            $data[$period]['detail']        = 0;
            $data[$period]['click']         = 0;
            $data[$period]['email']         = 0;
            $data[$period]['phone']         = 0;
            $data[$period]['fax']           = 0;
            $data[$period]['sms']           = 0;
            $data[$period]['click_call']    = 0;
            //db_formatNumber($idIn)
            /* today */
            $sql = "SELECT CONCAT(YEAR(date) , '-', MONTH(date)) AS period, `report_type` , SUM(`report_amount`) AS amount FROM `Report_Listing` WHERE `listing_id` = " . $idIn . " GROUP BY period, `report_type`";
            $result = self::fetch($sql);
            foreach($result as $row) {
                 //d($data[$row->period]['summary'] ,1);die;
                $period=$row->period;
                if($row->report_type == 1) $data[$period]['summary']        = $row->amount;
                if($row->report_type == 2) $data[$period]['detail']         = $row->amount;
                if($row->report_type == 3) $data[$period]['click']          = $row->amount;
                if($row->report_type == 4) $data[$period]['email']          = $row->amount;
                if($row->report_type == 5) $data[$period]['phone']          = $row->amount;
                if($row->report_type == 6) $data[$period]['fax']            = $row->amount;
                if($row->report_type == 7) $data[$period]['sms']            = $row->amount;
                if($row->report_type == 8) $data[$period]['click_call']     = $row->amount;
            }

            /* daily */
            $sql = "SELECT CONCAT(YEAR(day), '-', MONTH(day)) AS period, SUM(summary_view) AS summary, SUM(detail_view) AS detail, SUM(click_thru) AS click, SUM(email_sent) AS email, SUM(phone_view) AS phone, SUM(fax_view) AS fax, SUM(send_phone) AS sms, SUM(click_call) AS click_call FROM Report_Listing_Daily WHERE listing_id = " . $idIn . " GROUP BY period";
            $result = self::fetch($sql);
            foreach($result as $row) {
                $data[$row->period]['summary']        = $row->summary;
                $data[$row->period]['detail']         = $row->detail;
                $data[$row->period]['click']          =$row->click;
                $data[$row->period]['email']          = $row->email;
                $data[$row->period]['phone']          = $row->phone;
                $data[$row->period]['fax']            = $row->fax;
                $data[$row->period]['sms']            = $row->sms;
                $data[$row->period]['click_call']     = $row->click_call;
            }

            /* monthly */
            $sql = "SELECT CONCAT(YEAR(day), '-', MONTH(day)) AS period, SUM(summary_view) AS summary, SUM(detail_view) AS detail, SUM(click_thru) AS click, SUM(email_sent) AS email, SUM(phone_view) AS phone, SUM(fax_view) AS fax, SUM(send_phone) AS sms, SUM(click_call) AS click_call FROM Report_Listing_Monthly WHERE listing_id = " . $idIn . " GROUP BY period ORDER BY day DESC";
             $result = self::fetch($sql);
           foreach($result as $row) {
                $data[$row->period]['summary']        = $row->summary;
                $data[$row->period]['detail']         = $row->detail;
                $data[$row->period]['click']          = $row->click;
                $data[$row->period]['email']          = $row->email;
                $data[$row->period]['phone']          = $row->phone;
                $data[$row->period]['fax']            = $row->fax;
                $data[$row->period]['sms']            = $row->sms;
                $data[$row->period]['click_call']     = $row->click_call;
            }
            return $data;
        
        } else {

            $lastMonth = date("Y")."-".(date("m")-1);
            $data[$lastMonth]['summary'] = 0;
            $data[$lastMonth]['detail'] = 0;
            $data[$lastMonth]['click'] = 0;
            $data[$lastMonth]['email'] = 0;
            $data[$lastMonth]['phone'] = 0;
            $data[$lastMonth]['fax'] = 0;
            $data[$lastMonth]['sms'] = 0;
            $data[$lastMonth]['click_call'] = 0;
            
            return $data;
        }
    }
    
    public static function getReviews($item_type,$item_id)
    {
         $sql="SELECT * FROM Review WHERE item_type = '".strtolower($item_type)."' AND item_id = '$item_id'";
         $result=self::fetch($sql);
         return $result;

    }
    public static function getLeads($item_type,$item_id)
    {
         $sql="SELECT * FROM Leads WHERE type = '".strtolower($item_type)."' AND item_id = '$item_id'";
         $result=self::fetch($sql);
         return $result;

    }


    

    public static function SearchingResult($params=array(), $offset = 0, $per_page = 10)
    {
        $listing_summary=config('tables.domain.listing_summary');
        $image=config('tables.domain.image');
        $rate=config('tables.domain.rate');
        $listing_choice=config('tables.domain.listing_choice');
        $review=config('tables.domain.review');
        $listing_availablity_booking=config('tables.domain.listing_availablity_booking');
        
        
        $where_sleep_str=' '; 
        if($params['sleeps_number']>0 && $params['sleeps_condition']=='exactly')
        {
            $where_sleep_str = " AND ls.sleeps=".$params['sleeps_number'];
        }
        elseif($params['sleeps_number']>0 && $params['sleeps_condition']=='orLess')
        {
            $where_sleep_str = " AND ls.sleeps<=".$params['sleeps_number'];
        }
        elseif($params['sleeps_number']>0 && $params['sleeps_condition']=='orMore')
        {
            $where_sleep_str = " AND ls.sleeps>=".$params['sleeps_number'];
        }
        
        $where_bathroom_str=' '; 

        if(isset($params['bathroom_number']) && $params['bathroom_number']>0 && $params['bathroom_condition']=='exactly')
        {
            $where_bathroom_str = " AND ls.bathroom=".$params['bathroom_number'];
        }
        elseif(isset($params['bathroom_number']) &&$params['bathroom_number']>0 && $params['bathroom_condition']=='orLess')
        {
            $where_bathroom_str = " AND ls.bathroom<=".$params['bathroom_number'];
        }
        elseif(isset($params['bathroom_number']) && $params['bathroom_number']>0 && $params['bathroom_condition']=='orMore')
        {
            $where_bathroom_str = " AND ls.bathroom>=".$params['bathroom_number'];
        }
        
        
        $where_bedroom_str=' '; 
        if($params['bedroom_number']>0 && $params['bedroom_condition']=='exactly')
        {
            $where_bedroom_str = " AND ls.bedroom=".$params['bedroom_number'];
        }
        elseif($params['bedroom_number']>0 && $params['bedroom_condition']=='orLess')
        {
            $where_bedroom_str = " AND ls.bedroom<=".$params['bedroom_number'];
        }
        elseif($params['bedroom_number']>0 && $params['bedroom_condition']=='orMore')
        {
            $where_bedroom_str = " AND ls.bedroom>=".$params['bedroom_number'];
        }
        
        $where_period_str=' '; 
        if(isset($params['period']) && $params['period']!=0)
        {
            $where_period_str=' AND ls.required_stay like "%'.$params['period'].'%" '; 
        }
        else
        {
            $where_period_str=' '; 
        }
        
        
        $where_location_1_str=' '; 
        if(isset($params['location_1']) && $params['location_1']!=0)
        {
            $where_location_1_str=' AND ls.location_1 = "'.$params['location_1'].'" '; 
        }
        else
        {
            $where_location_1_str=' '; 
        }
                        
        $where_location_3_str=' '; 
        if(isset($params['location_3']) && $params['location_3']!=0)
        {
            $where_location_3_str=' AND ls.location_3 = "'.$params['location_3'].'" '; 
        }
        else
        {
            $where_location_3_str=' '; 
        }
        
        
        $where_location_4_str=' '; 
        
        if(isset($params['location_4']) && $params['location_4']!=0 ||$params['location_4']!='')
        {
            $where_location_4_str=' AND ls.location_4 = "'.$params['location_4'].'" '; 
        }
        else
        {

            $where_location_4_str=' '; 
        }
        
        $where_development_name_str=' '; 
        if(isset($params['development_name']) && $params['development_name']!=0 && $params['development_name']!='')
        {
            $where_development_name_str=' AND ls.development_name like "%'.$params['development_name'].'%" '; 
        }
        else
        {
            $where_development_name_str=' '; 
        }
        
        $where_rental_id_str=' '; 
        if(isset($params['rental_id']) && $params['rental_id']!=0)
        {
            $where_rental_id_str=' AND ls.id = '.$params['rental_id']; 
        }
        else
        {
            $where_rental_id_str=' '; 
        }
       
        $where_view_str=' '; 
        
        if(isset($params['view']) && $params['view']!=0 || $params['view']!='')
        {
            $where_view_str=' AND ls.view like "%'.$params['view'].'%" '; 
        }
        else
        {
            $where_view_str=' '; 
        }
        
        $where_property_type_str=' '; 
        if(isset($params['property_type']) && $params['property_type']!=0 || $params['property_type']!='')
        {
            $where_property_type_str=' AND ls.property_type like "%'.$params['property_type'].'%" '; 
        }
        else
        {
            $where_property_type_str=' '; 
        }
      
        $where_feature_smokingpermitted_str=' '; 
        if(isset($params['feature_smokingpermitted']) && $params['feature_smokingpermitted']!=0 || $params['feature_smokingpermitted']!='')
        {
            $where_feature_smokingpermitted_str=' AND ls.feature_smokingpermitted like "%'.$params['feature_smokingpermitted'].'%" '; 
        }
        else
        {
            $where_feature_smokingpermitted_str=' '; 
        }
          
        
        
        
        $where_address_str=' '; 
        if(isset($params['address']) && $params['address']!=0 || $params['address']!='')
        {
            $where_address_str=' AND ls.address like "%'.$params['address'].'%" '; 
        }
        else
        {
            $where_address_str=' '; 
        }
        
        $where_feature_petsallowed_str=' '; 
        if(isset($params['feature_petsallowed']) && $params['feature_petsallowed']!=0 || $params['feature_petsallowed']!='')
        {
            $where_feature_petsallowed_str=' AND ls.feature_petsallowed like "%'.$params['feature_petsallowed'].'%" '; 
        }
        else
        {
            $where_feature_petsallowed_str=' '; 
        }
        
        $where_feature_wheelchairaccess_str=' '; 
        if(isset($params['feature_wheelchairaccess']) && $params['feature_wheelchairaccess']!=0 || $params['feature_wheelchairaccess']!='')
        {
            $where_feature_wheelchairaccess_str=' AND ls.feature_wheelchairaccess like "%'.$params['feature_wheelchairaccess'].'%" '; 
        }
        else
        {
            $where_feature_wheelchairaccess_str=' '; 
        }
         
         
         $where_distance_beach_str=' '; 
        if(isset($params['distance_beach']) && $params['distance_beach']!=0 || $params['distance_beach']!='')
        {
            $where_distance_beach_str=' AND ls.distance_beach like "%'.$params['distance_beach'].'%" '; 
        }
        else
        {
            $where_distance_beach_str=' '; 
        }
        
        $where_feature_pool_str=' '; 
        if(isset($params['feature_pool']) && $params['feature_pool']!=0 || $params['feature_pool']!='')
        {
            $where_feature_pool_str=' AND ls.feature_pool like "%'.$params['feature_pool'].'%" '; 
        }
        else
        {
            $where_feature_pool_str=' '; 
        }
        
         $where_feature_elevator_str=' '; 
        if(isset($params['feature_elevator']) && $params['feature_elevator']!=0 || $params['feature_elevator']!='')
        {
            $where_feature_elevator_str=' AND ls.feature_elevator like "%'.$params['feature_elevator'].'%" '; 
        }
        else
        {
            $where_feature_elevator_str=' '; 
        }
        
        $where_feature_boatslip_str=' '; 
        if(isset($params['feature_boatslip']) && $params['feature_boatslip']!=0 || $params['feature_boatslip']!='')
        {
            $where_feature_boatslip_str=' AND ls.feature_boatslip like "%'.$params['feature_boatslip'].'%" '; 
        }
        else
        {
            $where_feature_boatslip_str=' '; 
        }
        
        
        
         $where_amenity_internet_str=' '; 
        if(isset($params['amenity_internet']) && $params['amenity_internet']!=0 || $params['amenity_internet']!='')
        {
            $where_amenity_internet_str=' AND ls.amenity_internet like "%'.$params['amenity_internet'].'%" '; 
        }
        else
        {
            $where_amenity_internet_str=' '; 
        }
        
         
        
        
        
         $orderBy_str=' ORDER BY  '; 
         
        if(isset($params['orderByLocation_4']) && $params['orderByLocation_4']!=0 || $params['orderByLocation_4']!='')
        {
            $orderBy_str.=' ( ls.location_4 ) DESC ,'; 
        }
        if(isset($params['orderByLocation_3']) && $params['orderByLocation_3']!=0 || $params['orderByLocation_3']!='')
        {
            $orderBy_str.='( ls.location_3='.$params['orderByLocation_3'].' ) DESC ,ls.search_pos ASC, ls.level, reg_exp_order DESC, ls.backlink DESC, ls.random_number DESC, ls.title, ls.id'; 
        }
        elseif(isset($params['rating']) && $params['rating']!=0 || $params['rating']!='')
        {
            $orderBy_str.='( rv.rating='.$params['rating'].' ) DESC ,ls.search_pos ASC, ls.level, reg_exp_order DESC, ls.backlink DESC, ls.random_number DESC, ls.title, ls.id'; 
        }
        else
        {
            $orderBy_str.=' ls.search_pos ASC, ls.level, reg_exp_order DESC, ls.backlink DESC, ls.random_number DESC, ls.title, ls.id '; 
        }
        
       
        $where_str = '';
        
        if(is_numeric($params['keyword']) && $params['keyword']!=0)
        {
            
          $where_str = " WHERE ls.id=".$params['keyword']." AND ls.status = 'A' 
            AND ls.image_id IS NOT NULL  
            
            AND (
                    (
                        ls.search_pos < 6 
                        AND ls.search_pos_date >= CURDATE()
                    ) 
                    OR ls.search_pos_date = '0000-00-00'
            )";
        }
        elseif(strlen($params['keyword'])==0)
        {
            
            if(isset($params['location_3']) && $params['location_3']!=0 && ($params['location_3']!=6 || $params['location_4']>0))
            {
                $where_str = "WHERE ls.status = 'A' 
                AND ls.image_id IS NOT NULL  
                AND (
                        (
                            ls.search_pos < 6 
                            AND ls.search_pos_date >= CURDATE()
                        ) 
                        OR ls.search_pos_date = '0000-00-00'
                )"; 
            }
            else
            {
                $where_str = "WHERE ls.status = 'A' 
                AND ls.image_id IS NOT NULL  
                AND (
                    ls.latitude <= 174.73204925797 
                    AND ls.latitude >= - 114.73204925797 
                    AND ls.longitude <= 214.73204925797 
                    AND ls.longitude >= - 74.732049257973
                ) 
                AND (
                        (
                            ls.search_pos < 6 
                            AND ls.search_pos_date >= CURDATE()
                        ) 
                        OR ls.search_pos_date = '0000-00-00'
                )"; 
            }
            
        }
        else
        {

           if(strlen($params['keyword'])==0)
           {
            
            if(isset($params['location_3']) && $params['location_3']!=0 && ($params['location_3']!=6 || $params['location_4']>0))
            {
                $where_str = "WHERE ls.status = 'A' 
                AND ls.image_id IS NOT NULL  
                AND (
                        (
                            ls.search_pos < 6 
                            AND ls.search_pos_date >= CURDATE()
                        ) 
                        OR ls.search_pos_date = '0000-00-00'
                )"; 
            }
            else
            {
                $where_str = "WHERE ls.status = 'A' 
                AND ls.image_id IS NOT NULL  
                AND (
                    ls.latitude <= 174.73204925797 
                    AND ls.latitude >= - 114.73204925797 
                    AND ls.longitude <= 214.73204925797 
                    AND ls.longitude >= - 74.732049257973
                ) 
                AND (
                        (
                            ls.search_pos < 6 
                            AND ls.search_pos_date >= CURDATE()
                        ) 
                        OR ls.search_pos_date = '0000-00-00'
                )"; 
            }
             
           }
           elseif($params['keyword']===0)
           {
            
                if(isset($params['location_3']) && $params['location_3']!=0 && ($params['location_3']!=6 || $params['location_4']>0))
                {
                    $where_str = "WHERE ls.status = 'A' 
                    AND ls.image_id IS NOT NULL  
                    AND (
                            (
                                ls.search_pos < 6 
                                AND ls.search_pos_date >= CURDATE()
                            ) 
                            OR ls.search_pos_date = '0000-00-00'
                    )"; 
                }
                else
                {
                    $where_str = "WHERE ls.status = 'A' 
                    AND ls.image_id IS NOT NULL  
                    AND (
                        ls.latitude <= 174.73204925797 
                        AND ls.latitude >= - 114.73204925797 
                        AND ls.longitude <= 214.73204925797 
                        AND ls.longitude >= - 74.732049257973
                    ) 
                    AND (
                            (
                                ls.search_pos < 6 
                                AND ls.search_pos_date >= CURDATE()
                            ) 
                            OR ls.search_pos_date = '0000-00-00'
                    )"; 
                }
             
           }
           else
           {
            
                $wherematchstr = "WHERE  ( MATCH (ls.fulltextsearch_where) AGAINST ('".'"'.str_replace('-',' ',$params['keyword']).'"'."' IN BOOLEAN MODE)";
            
                if(strpos($params['keyword'], ',') !== false)
                {
                    $keywordSearch = explode(',',$params['keyword']);
                    
                    foreach($keywordSearch as $val)
                    {
                        $wherematchstr.=" OR MATCH (ls.title,ls.address,ls.location_3_title,ls.location_4_title,ls.zip_code)
                AGAINST ('".$val."' IN NATURAL LANGUAGE MODE)";
                    }
                }
                else
                {
                    $wherematchstr = $wherematchstr;
                }
                
                $where_str = $wherematchstr." ) AND ls.status = 'A' 
                AND ls.image_id IS NOT NULL AND (
                        (
                            ls.search_pos < 6 
                            AND ls.search_pos_date >= CURDATE()
                        ) 
                        OR ls.search_pos_date = '0000-00-00'
                ) 
               "; 
           }
           
        }

        
        
        
        $where_str_rate=' ';
        
        //d($params);
         $rateJoinstr = " ";
         
        if((isset($params['period']) && trim($params['period'])!='0') || (isset($params['rate_min_number']) && $params['rate_min_number']!='0') || (isset($params['rate_max_number']) && $params['rate_max_number']!='0')) 
        {
            $rateJoinstr = " LEFT JOIN ".$rate." r ON r.listing_id=ls.id
            AND r.WEEK > 0 ";
        
            $select_rate_str=' MIN(r.DAY) AS min_day,
            MAX(r.DAY) AS max_day,
            MIN(r.week) AS min_week,
            MAX(r.week) AS max_week,
            MIN(r.month) AS min_month,
            MAX(r.month) AS max_month,
            MIN(r.season) AS min_season,
            MAX(r.season) AS max_season,';
            
            if(trim($params['period'])=='day')
             {
                if(isset($params['rate_min_number']) && $params['rate_min_number']!=0 && isset($params['rate_max_number']) && $params['rate_max_number']!=0)
                {
                    $where_str_rate = 'AND r.day>='.$params['rate_min_number'].' AND r.day<='.$params['rate_max_number'];
                }
                elseif(isset($params['rate_min_number']) && $params['rate_min_number']!=0)
                {
                    $where_str_rate = 'AND r.day>='.$params['rate_min_number'];
                }
                elseif(isset($params['rate_max_number']) && $params['rate_max_number']!=0)
                {
                    $where_str_rate = 'AND r.day<='.$params['rate_max_number'];
                }
                
                $rateJoinstr = " LEFT JOIN ".$rate." r ON r.listing_id=ls.id
                AND r.DAY > 0 "; 
             }
             elseif(trim($params['period'])=='week')
             {
                if($params['rate_min_number']!=0 && isset($params['rate_max_number']) && $params['rate_max_number']!=0)
                {
                    $where_str_rate = 'AND r.week>='.$params['rate_min_number'].' AND r.week<='.$params['rate_max_number'];
                }
                elseif(isset($params['rate_min_number']) && $params['rate_min_number']!=0)
                {
                    $where_str_rate = 'AND r.week>='.$params['rate_min_number'];
                }
                elseif(isset($params['rate_max_number']) && $params['rate_max_number']!=0)
                {
                    $where_str_rate = 'AND r.week<='.$params['rate_max_number'];
                }
                
                $rateJoinstr = " LEFT JOIN ".$rate." r ON r.listing_id=ls.id
                AND r.WEEK > 0 "; 
             }
             elseif(trim($params['period'])=='month')
             {
                if( $params['rate_min_number']!=0 && isset($params['rate_max_number']) && $params['rate_max_number']!=0)
                {
                    $where_str_rate = 'AND r.month>='.$params['rate_min_number'].' AND r.month<='.$params['rate_max_number'];
                }
                elseif($params['rate_min_number']!=0)
                {
                    $where_str_rate = 'AND r.month>='.$params['rate_min_number'];
                }
                elseif( $params['rate_max_number']!=0)
                {
                    $where_str_rate = 'AND r.month<='.$params['rate_max_number'];
                }
                
                $rateJoinstr = " LEFT JOIN ".$rate." r ON r.listing_id=ls.id
                AND r.MONTH > 0 "; 
             }
             elseif(isset($params['period']) && trim($params['period'])=='season')
             {
                    if(isset($params['rate_min_number']) && $params['rate_min_number']!=0 && isset($params['rate_max_number']) && $params['rate_max_number']!=0)
                    {
                        $where_str_rate = 'AND r.season>='.$params['rate_min_number'].' AND r.season<='.$params['rate_max_number'];
                    }
                    elseif(isset($params['rate_min_number']) && $params['rate_min_number']!=0)
                    {
                        $where_str_rate = 'AND r.season>='.$params['rate_min_number'];
                    }
                    elseif(isset($params['rate_max_number']) && $params['rate_max_number']!=0)
                    {
                        $where_str_rate = 'AND r.season<='.$params['rate_max_number'];
                    }
                    
                    $rateJoinstr = " LEFT JOIN ".$rate." r ON r.listing_id=ls.id
                    AND r.SEASON > 0 "; 
             }
             else
             {
                if($params['rate_min_number']!=0 && isset($params['rate_max_number']) && $params['rate_max_number']!=0)
                {
                    $where_str_rate = 'AND r.week>='.$params['rate_min_number'].' AND r.week<='.$params['rate_max_number'];
                }
                elseif(isset($params['rate_min_number']) && $params['rate_min_number']!=0)
                {
                    $where_str_rate = 'AND r.week>='.$params['rate_min_number'];
                }
                elseif(isset($params['rate_max_number']) && $params['rate_max_number']!=0)
                {
                    $where_str_rate = 'AND r.week<='.$params['rate_max_number'];
                }
                
                $rateJoinstr = " LEFT JOIN ".$rate." r ON r.listing_id=ls.id
                AND r.WEEK > 0 "; 
             }
         
        }
        else
        {
            $select_rate_str=' ';
            $rateJoinstr='';
        }
        
         //d($params);
//         echo $where_str_rate;
        $limit_str = " LIMIT $offset, $per_page";  
        $sql_result="SELECT SQL_CALC_FOUND_ROWS 
        ls.id AS rental_id,ls.LEVEL,
        ls.title,
        ls.title REGEXP '^$' AS reg_exp_order, SQRT(POW((69.1 * (30 - ls.latitude)), 2) + POW((53.0 * (70 - ls.longitude)), 2)) AS zipcode_score,
        ls.`address`,
        ls.`address2`,
        ls.bedroom,
        ls.sleeps,
        ls.bathroom,
        ls.location_3,
        ls.location_4,
        ls.location_3_title,
        ls.location_4_title,
        ls.zip_code,
        ls.required_stay,
        ls.latitude,
        ls.longitude,
        i.id AS image_id,
        i.prefix,
        i.type,
        i.width,
        i.height,
        ".$select_rate_str."                                         
        (CASE WHEN rv.rating IS NULL  THEN 0 ELSE rv.rating END) AS rating,  
        lc.id AS listing_choice
        FROM
        ".$listing_summary." ls 
        
        LEFT JOIN ".$image." i 
        ON ls.image_id = i.id        
        LEFT JOIN  ".$review." rv ON rv.item_id=ls.id  AND rv.item_type = 'listing' AND rv.review IS NOT NULL AND rv.review != '' AND rv.approved = 1
        LEFT JOIN ".$listing_choice." lc 
        ON lc.listing_id = ls.id 
        AND lc.`status` = 'A' 
        AND DATE(lc.`renewal_date`) >= DATE(NOW()) 
        ".$rateJoinstr."                
        ".$where_str." ".$where_sleep_str.$where_bedroom_str.$where_period_str.
        $where_bathroom_str.$where_location_1_str.$where_location_3_str.$where_location_4_str.$where_development_name_str.
        $where_rental_id_str.$where_view_str.$where_property_type_str.$where_address_str.
        $where_feature_smokingpermitted_str.$where_feature_petsallowed_str.$where_feature_wheelchairaccess_str.
        $where_distance_beach_str.$where_feature_pool_str.$where_feature_elevator_str.$where_feature_boatslip_str.$where_amenity_internet_str.$where_str_rate." 
        group by ls.id
        ".$orderBy_str." ".$limit_str;
        
        $result=self::fetch($sql_result);

        $result_temp  = array();
        $sql="SELECT FOUND_ROWS() AS total";
        
        $total_rows= self::fetch($sql);
        if(!empty($result))
        {
            $rental_ids=array();
            foreach($result as $val)
            {
               $rental_ids[] = $val->rental_id; 
            }
                        
            foreach($result as $key=>$val)
            {
                $result_temp[$key]['rental_id'] = $val->rental_id;
                $result_temp[$key]['title'] = $val->title;
                $result_temp[$key]['address'] = $val->address;
                $result_temp[$key]['address2'] = $val->address2;
                $result_temp[$key]['bedroom'] = $val->bedroom;
                $result_temp[$key]['sleeps'] = $val->sleeps;
                $result_temp[$key]['bathroom'] = $val->bathroom;
                $result_temp[$key]['location_3'] = $val->location_3;
                $result_temp[$key]['location_4'] = $val->location_4;
                $result_temp[$key]['location_3_title'] = $val->location_3_title;
                $result_temp[$key]['location_4_title'] = $val->location_4_title;
                $result_temp[$key]['zip_code'] = $val->zip_code;
                $result_temp[$key]['required_stay'] = $val->required_stay;
                $result_temp[$key]['latitude'] = $val->latitude;
                $result_temp[$key]['longitude'] = $val->longitude;
                $result_temp[$key]['image_id'] = $val->image_id;
                $result_temp[$key]['prefix'] = $val->prefix;
                $result_temp[$key]['type'] = $val->type;
                $result_temp[$key]['width'] = $val->width;
                $result_temp[$key]['height'] = $val->height;
                $result_temp[$key]['rating'] = $val->rating;
                $result_temp[$key]['listing_choice'] = $val->listing_choice;
                if((isset($params['period']) && trim($params['period'])!='0') || (isset($params['rate_min_number']) && $params['rate_min_number']!='0') || (isset($params['rate_max_number']) && $params['rate_max_number']!='0')) 
                {
                    $result_temp[$key]['min_day'] = $val->min_day;
                    $result_temp[$key]['max_day'] = $val->max_day;
                    $result_temp[$key]['min_week'] = $val->min_week;
                    $result_temp[$key]['max_week'] = $val->max_week;
                    
                    $result_temp[$key]['min_month'] = $val->min_month;
                    $result_temp[$key]['max_month'] = $val->max_month;
                    
                    $result_temp[$key]['min_season'] = $val->min_season;
                    $result_temp[$key]['max_season'] = $val->max_season;
                }
                else
                {
                    $sql_rate = "SELECT MIN(r.DAY) AS min_day,
                    MAX(r.DAY) AS max_day,
                    MIN(r.week) AS min_week,
                    MAX(r.week) AS max_week,
                    MIN(r.month) AS min_month,
                    MAX(r.month) AS max_month,
                    MIN(r.season) AS min_season,
                    MAX(r.season) AS max_season
                    FROM 
                    ".$rate." r 
                    WHERE 
                    r.listing_id=".$val->rental_id."
                    AND r.WEEK > 0 GROUP BY r.`listing_id` "; 
                    $result_rate=self::fetch($sql_rate);
                
                    if(isset($result_rate[0]->min_day))
                    {
                       $result_temp[$key]['min_day'] = $result_rate[0]->min_day; 
                    }
                    else
                    {
                        $result_temp[$key]['min_day'] = 0;
                    }
                    
                    if(isset($result_rate[0]->max_day))
                    {
                       $result_temp[$key]['max_day'] = $result_rate[0]->max_day; 
                    }
                    else
                    {
                        $result_temp[$key]['max_day'] = 0;
                    }
                    
                    if(isset($result_rate[0]->min_week))
                    {
                       
                       
                       $result_temp[$key]['min_week'] = $result_rate[0]->min_week; 
                    }
                    else
                    {
    
                        $result_temp[$key]['min_week'] = 0;
                    }
                    
                    if(isset($result_rate[0]->max_week))
                    {
                       $result_temp[$key]['max_week'] = $result_rate[0]->max_week; 
                    }
                    else
                    {
                        $result_temp[$key]['max_week'] = 0;
                    }
                    
                    if(isset($result_rate[0]->max_month))
                    {
                       $result_temp[$key]['max_month'] = $result_rate[0]->max_month; 
                    }
                    else
                    {
                        $result_temp[$key]['max_month'] = 0;
                    }
                    
                    
                    if(isset($result_rate[0]->min_season))
                    {
                       $result_temp[$key]['min_season'] = $result_rate[0]->min_season; 
                    }
                    else
                    {
                        $result_temp[$key]['min_season'] = 0;
                    }
                    
                    if(isset($result_rate[0]->max_season))
                    {
                       $result_temp[$key]['max_season'] = $result_rate[0]->max_season; 
                    }
                    else
                    {
                        $result_temp[$key]['max_season'] = 0;
                    }
                }
                
            }

        }
        else
        {
            $result_temp=array();
        }
        


        return array('total' => $total_rows,'result'=>$result_temp);
        
    }
    
    
    public static function quickInfoListing($params=array())
    {
        
        $listing_summary=config('tables.domain.listing_summary');
        $image=config('tables.domain.image');
        $rate=config('tables.domain.rate');
        $listing_choice=config('tables.domain.listing_choice');
        $review=config('tables.domain.review');
        
        $sql="SELECT 
        ls.id AS rental_id,
        ls.title,
        ls.`address`,
        ls.`address2`,
        ls.bedroom,
        ls.sleeps,
        ls.bathroom,
        ls.location_3_title,
        ls.location_4_title,
        ls.zip_code,
        ls.required_stay,
        ls.latitude,
        ls.longitude,
        i.id AS image_id,
        i.prefix,
        i.type,
        i.width,
        i.height, 
        MIN(r.DAY) AS min_day,
        MAX(r.DAY) AS max_day, 
        MIN(r.week) AS min_week,
        MAX(r.week) AS max_week,
        MIN(season) AS min_season,
        MAX(season) AS max_season,
        lc.*, 
        (CASE WHEN rv.rating IS NULL  THEN 0 ELSE rv.rating END) AS rating
        
        FROM
        ".$listing_summary." ls 
        
        LEFT JOIN ".$image." i 
        ON ls.image_id = i.id
        LEFT JOIN ".$rate." r ON r.`listing_id`=ls.id AND WEEK > 0
        LEFT JOIN ".$listing_choice." lc ON lc.`listing_id`=ls.id AND lc.STATUS = 'A'
        LEFT JOIN  ".$review." rv ON rv.item_id=ls.id  AND rv.item_type = 'listing' AND rv.review IS NOT NULL AND rv.review != '' AND rv.approved = 1
        WHERE ls.id=". $params['resource_id']."
        GROUP BY ls.id 
        ORDER BY ls.id,r.from_date,lc.editor_choice_id DESC";
        $result=self::fetch($sql);
        return $result;
        
    }
   

   public static function getCompareListing($resource_ids='')
    {
        
        $listing_summary=config('tables.domain.listing_summary');
        $image=config('tables.domain.image');
        $rate=config('tables.domain.rate');
        $listing_choice=config('tables.domain.listing_choice');
        $review=config('tables.domain.review');
        
        $sql="SELECT 
        ls.id AS rental_id,
        ls.title,
        ls.`address`,
        ls.`address2`,
        ls.bedroom,
        ls.sleeps,
        ls.bathroom,
        ls.location_3_title,
        ls.location_4_title,
        ls.zip_code,
        ls.required_stay,
        ls.latitude,
        ls.longitude,
        ls.amenity_blender,
        ls.amenity_ceilingfan,
        ls.amenity_coffeemaker,
        ls.amenity_cookingrange,
        ls.amenity_cookware,
        ls.amenity_deckfurniture,
        ls.amenity_dishwasher,
        ls.amenity_dvdplayer,
        ls.amenity_microwave,
        ls.amenity_outsideshower,
        ls.amenity_oven,
        ls.amenity_refrigeratorfreezer,
        ls.amenity_telephone,
        ls.amenity_television,
        ls.amenity_utensils,
        ls.amenity_vcr,
        ls.amenity_washerdryer,
        ls.amenity_wifi,
        ls.amenity_airhockey,
        ls.amenity_alarmclock,
        ls.amenity_answeringmachine,
        ls.amenity_arcadegames,
        ls.amenity_billiards,
        ls.amenity_cdplayer,
        ls.amenity_childshighchair,
        ls.amenity_computer,
        ls.feature_airconditioning,
        ls.feature_barbecuegas,
        ls.feature_cablesatellitetv,
        ls.feature_deck,
        ls.feature_fullkitchen,
        ls.feature_garage,
        ls.feature_internetaccess,
        ls.amenity_blurayplayer,
        ls.amenity_books,
        ls.amenity_casetteplayer,
        ls.amenity_communalpool,
        ls.amenity_dishes,
        ls.amenity_exercisefacilities,
        ls.amenity_foosball,
        ls.amenity_gametable,
        ls.amenity_games,
        ls.amenity_grill,
        ls.amenity_hairdryer,
        ls.amenity_icemaker,
        ls.amenity_internet,
        ls.amenity_ironandboard,
        ls.amenity_kidsgames,
        ls.amenity_linensprovided,
        ls.amenity_lobsterpot,
        ls.amenity_minirefrigerator,
        ls.amenity_minirefrigerator,
        ls.amenity_mp3radiodock,
        ls.amenity_pinball,
        ls.amenity_pingpong,
        ls.amenity_privatepool,
        ls.amenity_radio,
        ls.amenity_sofabed,
        ls.amenity_stereo,
        ls.amenity_toaster,
        ls.amenity_toasteroven,
        ls.amenity_towelsprovided,
        ls.amenity_toys,
        ls.amenity_vacuum,
        ls.amenity_videogameconsole,
        ls.amenity_videogames,
        ls.feature_balcony,
        ls.feature_barbecuecharcoal,
        ls.feature_clubhouse,
        ls.feature_coveredparking,
        ls.feature_diningroom,
        ls.feature_elevator,
        ls.feature_familyroom,
        ls.feature_gasfireplace,
        ls.feature_gatedcommunity,
        ls.feature_wheelchairaccess,
        ls.feature_heated,
        ls.feature_heatedpool,
        ls.feature_hottubjacuzzi,
        ls.feature_kitchenette,
        ls.feature_livingroom,
        ls.feature_loft,
        ls.feature_onsitesecurity,
        ls.feature_patio,
        ls.feature_petsallowed,
        ls.feature_playroom,
        ls.feature_pool,
        ls.feature_porch,
        ls.feature_rooftopdeck, 
        ls.feature_sauna,
        ls.feature_smokingpermitted,
        ls.feature_woodfireplace,
        ls.activity_antiquing,
        ls.activity_basketballcourt,
        ls.activity_beachcombing, 
        ls.activity_bicycling,
        ls.activity_bikerentals,
        ls.activity_birdwatching,
        ls.activity_boatrentals,
        ls.activity_boating,
        ls.activity_botanicalgarden,
        ls.activity_canoe,
        ls.activity_churches,
        ls.activity_cinemas,
        ls.activity_bikesprovided,
        ls.activity_deepseafishing,
        ls.activity_fishing, 
        ls.activity_fitnesscenter,
        ls.activity_golf,
        ls.activity_healthbeautyspa,
        ls.activity_hiking,
        ls.activity_horsebackriding,
        ls.activity_horseshoes,
        ls.activity_hotairballooning,
        ls.activity_iceskating,
        ls.activity_jetskiing,
        ls.activity_kayaking, 
        ls.activity_livetheater,
        ls.activity_marina,
        ls.activity_miniaturegolf,
        ls.activity_mountainbiking,
        ls.activity_moviecinemas,
        ls.activity_museums,
        ls.activity_paddleboating,
        ls.activity_paragliding,
        ls.activity_parasailing,
        ls.activity_playground,
        ls.activity_recreationcenter,
        ls.activity_restaurants,
        ls.activity_rollerblading,
        ls.activity_sailing,
        ls.activity_shelling,
        ls.activity_shopping,
        ls.activity_sightseeing,
        ls.activity_skiing,
        ls.activity_bayfishing,
        ls.activity_spa,
        ls.activity_surffishing,
        ls.activity_surfing,
        ls.activity_swimming,
        ls.activity_tennis,
        ls.activity_themeparks,
        ls.activity_waterparks,
        ls.activity_walking,
        ls.activity_waterskiing,
        ls.activity_watertubing,
        ls.activity_wildlifeviewing,
        ls.activity_zoo,    
        i.id AS image_id,
        i.prefix,
        i.type,
        i.width,
        i.height, 
        MIN(r.DAY) AS min_day,
        MAX(r.DAY) AS max_day, 
        MIN(r.week) AS min_week,
        MAX(r.week) AS max_week,
        MIN(season) AS min_season,
        MAX(season) AS max_season,
        lc.*, 
        (CASE WHEN rv.rating IS NULL  THEN 0 ELSE rv.rating END) AS rating
        FROM
        ".$listing_summary." ls 
        
        LEFT JOIN ".$image." i 
        ON ls.image_id = i.id
        LEFT JOIN ".$rate." r ON r.`listing_id`=ls.id AND WEEK > 0
        LEFT JOIN ".$listing_choice." lc ON lc.`listing_id`=ls.id AND lc.STATUS = 'A'
        LEFT JOIN  ".$review." rv ON rv.item_id=ls.id  AND rv.item_type = 'listing' AND rv.review IS NOT NULL AND rv.review != '' AND rv.approved = 1
        WHERE ls.id IN (".$resource_ids.")
        GROUP BY ls.id 
        ORDER BY FIELD(ls.id,".$resource_ids.")";
        $result=self::fetch($sql);
        return $result;
        
    }
    
    public static function getListingDetail($resource_ids=0)
    {
        $listing_summary=config('tables.domain.listing_summary');
        $image=config('tables.domain.image');
        $rate=config('tables.domain.rate');
        $listing_choice=config('tables.domain.listing_choice');
        $review=config('tables.domain.review');
        $gallery_image=config('tables.domain.gallery_image');
        $gallery_item=config('tables.domain.gallery_item');
        $rate_information=config('tables.domain.rateinformation');
        $listing_availablity_booking=config('tables.domain.listing_availablity_booking');
        $editor_choice=config('tables.domain.editor_choice');
        
        
        
        
        $sql="SELECT 
        ls.id AS rental_id,
        ls.`account_id`,
        ls.updated,
        ls.entered,
        ls.number_views,
        ls.title,
        ls.`address`,
        ls.`address2`,
        ls.bedroom,
        ls.sleeps,
        ls.bathroom,
        ls.location_3_title,
        ls.location_4_title,
        ls.location_1_abbreviation,
        ls.location_3_abbreviation,
        ls.zip_code,
        ls.property_type,
        ls.required_stay,
        ls.latitude,
        ls.longitude,
        ls.distance_beach,
        ls.description,
        
        
        ls.amenity_blender,
        ls.amenity_ceilingfan,
        ls.amenity_coffeemaker,
        ls.amenity_cookingrange,
        ls.amenity_cookware,
        ls.amenity_deckfurniture,
        ls.amenity_dishwasher,
        ls.amenity_dvdplayer,
        ls.amenity_microwave,
        ls.amenity_outsideshower,
        ls.amenity_oven,
        ls.amenity_refrigeratorfreezer,
        ls.amenity_telephone,
        ls.amenity_television,
        ls.amenity_utensils,
        ls.amenity_vcr,
        ls.amenity_washerdryer,
        ls.amenity_wifi,
        ls.amenity_airhockey,
        ls.amenity_alarmclock,
        ls.amenity_answeringmachine,
        ls.amenity_arcadegames,
        ls.amenity_billiards,
        ls.amenity_cdplayer,
        ls.amenity_childshighchair,
        ls.amenity_computer,
        ls.amenity_blurayplayer,
        ls.amenity_books,
        ls.amenity_casetteplayer,
        ls.amenity_communalpool,
        ls.amenity_dishes,
        ls.amenity_exercisefacilities,
        ls.amenity_foosball,
        ls.amenity_gametable,
        ls.amenity_games,
        ls.amenity_grill,
        ls.amenity_hairdryer,
        ls.amenity_icemaker,
        ls.amenity_internet,
        ls.amenity_ironandboard,
        ls.amenity_kidsgames,
        ls.amenity_linensprovided,
        ls.amenity_lobsterpot,
        ls.amenity_minirefrigerator,
        ls.amenity_mp3radiodock,
        ls.amenity_pinball,
        ls.amenity_pingpong,
        ls.amenity_privatepool,
        ls.amenity_radio,
        ls.amenity_sofabed,
        ls.amenity_stereo,
        ls.amenity_toaster,
        ls.amenity_toasteroven,
        ls.amenity_towelsprovided,
        ls.amenity_toys,
        ls.amenity_vacuum,
        ls.amenity_videogameconsole,
        ls.amenity_videogames,
        
        
        ls.feature_airconditioning,
        ls.feature_barbecuegas,
        ls.feature_cablesatellitetv,
        ls.feature_deck,
        ls.feature_fullkitchen,
        ls.feature_garage,
        ls.feature_internetaccess,
        
        ls.feature_balcony,
        ls.feature_boatslip,
        ls.feature_barbecuecharcoal,
        ls.feature_clubhouse,
        ls.feature_coveredparking,
        ls.feature_diningroom,
        ls.feature_elevator,
        ls.feature_familyroom,
        ls.feature_gasfireplace,
        ls.feature_gatedcommunity,
        ls.feature_wheelchairaccess,
        ls.feature_heated,
        ls.feature_heatedpool,
        ls.feature_hottubjacuzzi,
        ls.feature_kitchenette,
        ls.feature_livingroom,
        ls.feature_loft,
        ls.feature_onsitesecurity,
        ls.feature_patio,
        ls.feature_petsallowed,
        ls.feature_playroom,
        ls.feature_pool,
        ls.feature_porch,
        ls.feature_rooftopdeck, 
        ls.feature_sauna,
        ls.feature_smokingpermitted,
        ls.feature_woodfireplace,
        
        
        ls.activity_antiquing,
        ls.activity_basketballcourt,
        ls.activity_beachcombing, 
        ls.activity_bicycling,
        ls.activity_bikerentals,
        ls.activity_birdwatching,
        ls.activity_boatrentals,
        ls.activity_boating,
        ls.activity_botanicalgarden,
        ls.activity_canoe,
        ls.activity_churches,
        ls.activity_cinemas,
        ls.activity_bikesprovided,
        ls.activity_deepseafishing,
        ls.activity_fishing, 
        ls.activity_fitnesscenter,
        ls.activity_golf,
        ls.activity_healthbeautyspa,
        ls.activity_hiking,
        ls.activity_horsebackriding,
        ls.activity_horseshoes,
        ls.activity_hotairballooning,
        ls.activity_iceskating,
        ls.activity_jetskiing,
        ls.activity_kayaking, 
        ls.activity_livetheater,
        ls.activity_marina,
        ls.activity_miniaturegolf,
        ls.activity_mountainbiking,
        ls.activity_moviecinemas,
        ls.activity_museums,
        ls.activity_paddleboating,
        ls.activity_paragliding,
        ls.activity_parasailing,
        ls.activity_playground,
        ls.activity_recreationcenter,
        ls.activity_restaurants,
        ls.activity_rollerblading,
        ls.activity_sailing,
        ls.activity_shelling,
        ls.activity_shopping,
        ls.activity_sightseeing,
        ls.activity_skiing,
        ls.activity_bayfishing,
        ls.activity_spa,
        ls.activity_surffishing,
        ls.activity_surfing,
        ls.activity_swimming,
        ls.activity_tennis,
        ls.activity_themeparks,
        ls.activity_waterparks,
        ls.activity_walking,
        ls.activity_waterskiing,
        ls.activity_watertubing,
        ls.activity_wildlifeviewing,
        ls.activity_zoo,    
        
        MIN(r.DAY) AS min_day,
        MAX(r.DAY) AS max_day, 
        MIN(r.week) AS min_week,
        MAX(r.week) AS max_week,
        MIN(season) AS min_season,
        MAX(season) AS max_season,
        (CASE WHEN rv.rating IS NULL  THEN 0 ELSE rv.rating END) AS rating,
        ri.checkin,
        ri.checkout,
        ri.payment_types,
        ri.payment_terms,
        ri.cancellation_policy,
        ri.security_deposit,
        ri.notes 
        FROM
        ".$listing_summary." ls 
        
        LEFT JOIN ".$rate." r ON r.`listing_id`=ls.id AND WEEK > 0
       
        LEFT JOIN  ".$review." rv ON rv.item_id=ls.id  AND rv.item_type = 'listing' AND rv.review IS NOT NULL AND rv.review != '' AND rv.approved = 1
        LEFT JOIN ".$rate_information." ri ON ri.listing_id=ls.id
        WHERE ls.id =".$resource_ids."
        GROUP BY ls.id 
        ORDER BY ls.id,r.from_date DESC";
        $result=self::fetch($sql);
        // gallery images of listing property
        $sql_gallery = "SELECT 
        i.*,gim.image_caption  
        FROM
        ".$gallery_image." gim 
        JOIN ".$gallery_item." git 
        ON git.gallery_id = gim.gallery_id 
        JOIN ".$image." i 
        ON i.id = gim.image_id 
        WHERE git.item_id =".$resource_ids." 
        AND git.item_type = 'listing' ";
        $result_gallery=self::fetch($sql_gallery);

      // gallery main image of listing property
        $sql_main = "SELECT 
        i.*,gim.image_caption,gim.image_default
        FROM
        ".$gallery_image." gim 
        JOIN ".$gallery_item." git 
        ON git.gallery_id = gim.gallery_id 
        JOIN ".$image." i 
        ON i.id = gim.image_id 
        WHERE git.item_id =".$resource_ids." 
        AND gim.image_default='y'
        AND git.item_type = 'listing' ";
        $result_main=self::fetch($sql_main);
        
        // listing rates
        $sql_rates = "SELECT 
        from_date,to_date,DAY,WEEK,MONTH,season,sameday_in_out 
        FROM
        ".$rate." 
        WHERE listing_id =".$resource_ids."
        ORDER BY from_date ASC";
        $result_rates=self::fetch($sql_rates);
        
        // Availablity
        $sql_availablity = "SELECT 
        DATE_FORMAT(start_date,'%m-%d-%Y') AS start_date,start_date_period,DATE_FORMAT(end_date,'%m-%d-%Y') AS end_date,end_date_period,customer_name 
        FROM
        ".$listing_availablity_booking." 
        WHERE listing_id =".$resource_ids."
        ORDER BY start_date,
        start_date_period ";
        $result_availablity=self::fetch($sql_availablity);
        
        // Specials
        $sql_specials = "SELECT lc.description AS sp_description,lc.renewal_date AS sp_renewal_date,ec.name AS sp_name,ec.price AS sp_price,ec.renewal_period AS sp_renewal_period,
        ec.title AS sp_title,ec.meta_description AS sp_meta_description,ec.meta_keys AS sp_meta_keys
        FROM ".$listing_choice." lc JOIN ".$editor_choice." ec ON ec.id=lc.editor_choice_id WHERE lc.listing_id=".$resource_ids."
        
        AND lc.status = 'A'
        AND DATE(lc.`renewal_date`) >= DATE(NOW()) 
        ORDER BY lc.editor_choice_id ";
        $result_specials=self::fetch($sql_specials);
        
        
        
      
  
        $result_temp = array();
        if(!empty($result))
        {
            foreach($result as $key=>$val)
            {
                $result_temp[$key]['rental_id'] = $val->rental_id;
                $result_temp[$key]['account_id'] = $val->account_id;
                $result_temp[$key]['updated'] = $val->updated;
                $result_temp[$key]['entered'] = $val->entered;
                $result_temp[$key]['number_views'] = $val->number_views;
                
                                                
                $result_temp[$key]['title'] = $val->title;
                $result_temp[$key]['address'] = $val->address;
                $result_temp[$key]['address2'] = $val->address2;
                $result_temp[$key]['bedroom'] = $val->bedroom;
                $result_temp[$key]['sleeps'] = $val->sleeps;
                $result_temp[$key]['bathroom'] = $val->bathroom;
                $result_temp[$key]['location_3_title'] = $val->location_3_title;
                $result_temp[$key]['location_4_title'] = $val->location_4_title;
                
                $result_temp[$key]['location_1_abbreviation'] = $val->location_1_abbreviation;
                $result_temp[$key]['location_3_abbreviation'] = $val->location_3_abbreviation;
                
                
                
                $result_temp[$key]['zip_code'] = $val->zip_code;
                $result_temp[$key]['property_type'] = $val->property_type;

                $result_temp[$key]['required_stay'] = $val->required_stay;
                $result_temp[$key]['latitude'] = $val->latitude;
                $result_temp[$key]['longitude'] = $val->longitude;
                $result_temp[$key]['distance_beach'] = $val->distance_beach;
                $result_temp[$key]['description'] = $val->description;
                
                
                
                $result_temp[$key]['amenity_blender'] = $val->amenity_blender;
                $result_temp[$key]['amenity_ceilingfan'] = $val->amenity_ceilingfan;
                
                $result_temp[$key]['amenity_coffeemaker'] = $val->amenity_coffeemaker;
                $result_temp[$key]['amenity_cookingrange'] = $val->amenity_cookingrange;
                $result_temp[$key]['amenity_cookware'] = $val->amenity_cookware;
                $result_temp[$key]['amenity_deckfurniture'] = $val->amenity_deckfurniture;
                $result_temp[$key]['amenity_dishwasher'] = $val->amenity_dishwasher;
                $result_temp[$key]['amenity_dvdplayer'] = $val->amenity_dvdplayer;
                
                $result_temp[$key]['amenity_microwave'] = $val->amenity_microwave;
                $result_temp[$key]['amenity_outsideshower'] = $val->amenity_outsideshower;
                $result_temp[$key]['amenity_oven'] = $val->amenity_oven;
                $result_temp[$key]['amenity_refrigeratorfreezer'] = $val->amenity_refrigeratorfreezer;
                $result_temp[$key]['amenity_telephone'] = $val->amenity_telephone;
                $result_temp[$key]['amenity_television'] = $val->amenity_television;
                
                $result_temp[$key]['amenity_utensils'] = $val->amenity_utensils;
                $result_temp[$key]['amenity_vcr'] = $val->amenity_vcr;
                $result_temp[$key]['amenity_washerdryer'] = $val->amenity_washerdryer;
                $result_temp[$key]['amenity_airhockey'] = $val->amenity_airhockey;
                $result_temp[$key]['amenity_alarmclock'] = $val->amenity_alarmclock;
                $result_temp[$key]['amenity_answeringmachine'] = $val->amenity_answeringmachine;
                
                
                $result_temp[$key]['amenity_arcadegames'] = $val->amenity_arcadegames;
                $result_temp[$key]['amenity_billiards'] = $val->amenity_billiards;
                $result_temp[$key]['amenity_cdplayer'] = $val->amenity_cdplayer;
                $result_temp[$key]['amenity_childshighchair'] = $val->amenity_childshighchair;
                $result_temp[$key]['amenity_computer'] = $val->amenity_computer;
                
                $result_temp[$key]['feature_airconditioning'] = $val->feature_airconditioning;
                $result_temp[$key]['feature_barbecuegas'] = $val->feature_barbecuegas;
                $result_temp[$key]['feature_boatslip'] = $val->feature_boatslip;
                
                $result_temp[$key]['feature_cablesatellitetv'] = $val->feature_cablesatellitetv;
                $result_temp[$key]['feature_deck'] = $val->feature_deck;
                $result_temp[$key]['feature_fullkitchen'] = $val->feature_fullkitchen;
                
                
                $result_temp[$key]['feature_garage'] = $val->feature_garage;
                $result_temp[$key]['feature_internetaccess'] = $val->feature_internetaccess;
                $result_temp[$key]['amenity_blurayplayer'] = $val->amenity_blurayplayer;
                $result_temp[$key]['amenity_books'] = $val->amenity_books;
                $result_temp[$key]['amenity_casetteplayer'] = $val->amenity_casetteplayer;
                
                $result_temp[$key]['amenity_communalpool'] = $val->amenity_communalpool;
                $result_temp[$key]['amenity_dishes'] = $val->amenity_dishes;
                $result_temp[$key]['amenity_exercisefacilities'] = $val->amenity_exercisefacilities;
                $result_temp[$key]['amenity_foosball'] = $val->amenity_foosball;
                $result_temp[$key]['amenity_foosball'] = $val->amenity_foosball;
                
                $result_temp[$key]['amenity_gametable'] = $val->amenity_gametable;
                $result_temp[$key]['amenity_games'] = $val->amenity_games;
                $result_temp[$key]['amenity_grill'] = $val->amenity_grill;
                $result_temp[$key]['amenity_hairdryer'] = $val->amenity_hairdryer;
                $result_temp[$key]['amenity_icemaker'] = $val->amenity_icemaker;
                
                
                $result_temp[$key]['amenity_internet'] = $val->amenity_internet;
                $result_temp[$key]['amenity_ironandboard'] = $val->amenity_ironandboard;
                $result_temp[$key]['amenity_kidsgames'] = $val->amenity_kidsgames;
                $result_temp[$key]['amenity_linensprovided'] = $val->amenity_linensprovided;
                $result_temp[$key]['amenity_lobsterpot'] = $val->amenity_lobsterpot;
                
                $result_temp[$key]['amenity_minirefrigerator'] = $val->amenity_minirefrigerator;
                $result_temp[$key]['amenity_mp3radiodock'] = $val->amenity_mp3radiodock;
                $result_temp[$key]['amenity_pinball'] = $val->amenity_pinball;
                $result_temp[$key]['amenity_pingpong'] = $val->amenity_pingpong;
                $result_temp[$key]['amenity_privatepool'] = $val->amenity_privatepool;
                
                $result_temp[$key]['amenity_radio'] = $val->amenity_radio;
                $result_temp[$key]['amenity_sofabed'] = $val->amenity_sofabed;
                $result_temp[$key]['amenity_stereo'] = $val->amenity_stereo;
                $result_temp[$key]['amenity_toaster'] = $val->amenity_toaster;
                $result_temp[$key]['amenity_toasteroven'] = $val->amenity_toasteroven;
                
                $result_temp[$key]['amenity_towelsprovided'] = $val->amenity_towelsprovided;
                $result_temp[$key]['amenity_toys'] = $val->amenity_toys;
                $result_temp[$key]['amenity_vacuum'] = $val->amenity_vacuum;
                $result_temp[$key]['amenity_videogameconsole'] = $val->amenity_videogameconsole;
                $result_temp[$key]['amenity_videogames'] = $val->amenity_videogames;
                
                $result_temp[$key]['feature_balcony'] = $val->feature_balcony;
                $result_temp[$key]['feature_barbecuecharcoal'] = $val->feature_barbecuecharcoal;
                $result_temp[$key]['feature_clubhouse'] = $val->feature_clubhouse;
                $result_temp[$key]['feature_coveredparking'] = $val->feature_coveredparking;
                $result_temp[$key]['feature_diningroom'] = $val->feature_diningroom;
                
                $result_temp[$key]['feature_elevator'] = $val->feature_elevator;
                $result_temp[$key]['feature_familyroom'] = $val->feature_familyroom;
                $result_temp[$key]['feature_gasfireplace'] = $val->feature_gasfireplace;
                $result_temp[$key]['feature_gatedcommunity'] = $val->feature_gatedcommunity;
                $result_temp[$key]['feature_wheelchairaccess'] = $val->feature_wheelchairaccess;
                
                $result_temp[$key]['feature_heated'] = $val->feature_heated;
                $result_temp[$key]['feature_heatedpool'] = $val->feature_heatedpool;
                $result_temp[$key]['feature_hottubjacuzzi'] = $val->feature_hottubjacuzzi;
                $result_temp[$key]['feature_kitchenette'] = $val->feature_kitchenette;
                $result_temp[$key]['feature_livingroom'] = $val->feature_livingroom;
                
                $result_temp[$key]['feature_loft'] = $val->feature_loft;
                $result_temp[$key]['feature_onsitesecurity'] = $val->feature_onsitesecurity;
                $result_temp[$key]['feature_patio'] = $val->feature_patio;
                $result_temp[$key]['feature_petsallowed'] = $val->feature_petsallowed;
                $result_temp[$key]['feature_playroom'] = $val->feature_playroom;
                
                $result_temp[$key]['feature_pool'] = $val->feature_pool;
                $result_temp[$key]['feature_porch'] = $val->feature_porch;
                $result_temp[$key]['feature_rooftopdeck'] = $val->feature_rooftopdeck;
                $result_temp[$key]['feature_sauna'] = $val->feature_sauna;
                $result_temp[$key]['feature_smokingpermitted'] = $val->feature_smokingpermitted;
                
                $result_temp[$key]['feature_woodfireplace'] = $val->feature_woodfireplace;
                $result_temp[$key]['activity_antiquing'] = $val->activity_antiquing;
                $result_temp[$key]['activity_basketballcourt'] = $val->activity_basketballcourt;
                $result_temp[$key]['activity_beachcombing'] = $val->activity_beachcombing;
                $result_temp[$key]['activity_bicycling'] = $val->activity_bicycling;
                
                $result_temp[$key]['activity_bikerentals'] = $val->activity_bikerentals;
                $result_temp[$key]['activity_birdwatching'] = $val->activity_birdwatching;
                $result_temp[$key]['activity_boatrentals'] = $val->activity_boatrentals;
                $result_temp[$key]['activity_boating'] = $val->activity_boating;
                $result_temp[$key]['activity_botanicalgarden'] = $val->activity_botanicalgarden;
                
                $result_temp[$key]['activity_canoe'] = $val->activity_canoe;
                $result_temp[$key]['activity_churches'] = $val->activity_churches;
                $result_temp[$key]['activity_cinemas'] = $val->activity_cinemas;
                $result_temp[$key]['activity_bikesprovided'] = $val->activity_bikesprovided;
                $result_temp[$key]['activity_deepseafishing'] = $val->activity_deepseafishing;
                
                $result_temp[$key]['activity_fishing'] = $val->activity_fishing;
                $result_temp[$key]['activity_fitnesscenter'] = $val->activity_fitnesscenter;
                $result_temp[$key]['activity_golf'] = $val->activity_golf;
                $result_temp[$key]['activity_healthbeautyspa'] = $val->activity_healthbeautyspa;
                $result_temp[$key]['activity_hiking'] = $val->activity_hiking;
                
                $result_temp[$key]['activity_horsebackriding'] = $val->activity_horsebackriding;
                $result_temp[$key]['activity_horseshoes'] = $val->activity_horseshoes;
                $result_temp[$key]['activity_hotairballooning'] = $val->activity_hotairballooning;
                $result_temp[$key]['activity_iceskating'] = $val->activity_iceskating;
                $result_temp[$key]['activity_jetskiing'] = $val->activity_jetskiing;
                
                $result_temp[$key]['activity_kayaking'] = $val->activity_kayaking;
                $result_temp[$key]['activity_livetheater'] = $val->activity_livetheater;
                $result_temp[$key]['activity_marina'] = $val->activity_marina;
                $result_temp[$key]['activity_miniaturegolf'] = $val->activity_miniaturegolf;
                $result_temp[$key]['activity_mountainbiking'] = $val->activity_mountainbiking;
                
                $result_temp[$key]['activity_moviecinemas'] = $val->activity_moviecinemas;
                $result_temp[$key]['activity_museums'] = $val->activity_museums;
                $result_temp[$key]['activity_paddleboating'] = $val->activity_paddleboating;
                $result_temp[$key]['activity_paragliding'] = $val->activity_paragliding;
                $result_temp[$key]['activity_parasailing'] = $val->activity_parasailing;
                
                $result_temp[$key]['activity_playground'] = $val->activity_playground;
                $result_temp[$key]['activity_recreationcenter'] = $val->activity_recreationcenter;
                $result_temp[$key]['activity_restaurants'] = $val->activity_restaurants;
                $result_temp[$key]['activity_rollerblading'] = $val->activity_rollerblading;
                $result_temp[$key]['activity_sailing'] = $val->activity_sailing;
                
                $result_temp[$key]['activity_shelling'] = $val->activity_shelling;
                $result_temp[$key]['activity_shopping'] = $val->activity_shopping;
                $result_temp[$key]['activity_sightseeing'] = $val->activity_sightseeing;
                $result_temp[$key]['activity_skiing'] = $val->activity_skiing;
                $result_temp[$key]['activity_bayfishing'] = $val->activity_bayfishing;
                
                $result_temp[$key]['activity_spa'] = $val->activity_spa;
                $result_temp[$key]['activity_surffishing'] = $val->activity_surffishing;
                $result_temp[$key]['activity_surfing'] = $val->activity_surfing;
                $result_temp[$key]['activity_swimming'] = $val->activity_swimming;
                $result_temp[$key]['activity_tennis'] = $val->activity_tennis;
                
                $result_temp[$key]['activity_themeparks'] = $val->activity_themeparks;
                $result_temp[$key]['activity_waterparks'] = $val->activity_waterparks;
                $result_temp[$key]['activity_walking'] = $val->activity_walking;
                $result_temp[$key]['activity_waterskiing'] = $val->activity_waterskiing;
                $result_temp[$key]['activity_watertubing'] = $val->activity_watertubing;
                
                $result_temp[$key]['activity_wildlifeviewing'] = $val->activity_wildlifeviewing;
                $result_temp[$key]['activity_zoo'] = $val->activity_zoo;
                
                
                $result_temp[$key]['min_day'] = $val->min_day;
                $result_temp[$key]['max_day'] = $val->max_day;
                $result_temp[$key]['min_week'] = $val->min_week;
                
                $result_temp[$key]['max_week'] = $val->max_week;
                $result_temp[$key]['min_season'] = $val->min_season;
                $result_temp[$key]['max_season'] = $val->max_season;
                $result_temp[$key]['rating'] = $val->rating;
                
                $result_temp[$key]['checkin'] = $val->checkin;
                $result_temp[$key]['checkout'] = $val->checkout;
                $result_temp[$key]['payment_types'] = $val->payment_types;
                $result_temp[$key]['payment_terms'] = $val->payment_terms;
                $result_temp[$key]['cancellation_policy'] = $val->cancellation_policy;
                $result_temp[$key]['security_deposit'] = $val->security_deposit;
                $result_temp[$key]['notes'] = $val->notes;
                
            
        
                if(!empty($result_gallery))
                {
                    foreach($result_gallery as $v)
                    {
                        $result_temp[$key]['image'][] = $v;
                    } 
                }

                if(!empty($result_main))
                {
                    foreach($result_main as $v)
                    {
                        $result_temp[$key]['main_image'][] = $v;
                    } 
                }

                
               
               if(!empty($result_rates))
               {
                    foreach($result_rates as $v)
                    {
                        $result_temp[$key]['rate_list'][] = $v;
                    }
               }
                
                if(!empty($result_availablity))
                {
                    foreach($result_availablity as $v)
                    {
                        $result_temp[$key]['availablity'][] = $v;
                    }
                }
                
                if(!empty($result_specials))
                {
                   foreach($result_specials as $v)
                    {
                        $result_temp[$key]['result_specials'][] = $v;
                    } 
                }  
            }
        }
        else
        {
          $result_temp = array();  
        }
        return $result_temp;
    }
    
    public static function getListingTitle($resource_ids=0)
    {
        
        $listing_summary=config('tables.domain.listing_summary');
        $sql='SELECT ls.id AS item_id,
        ls.title,
        ls.`address`,
        ls.`address2`,
        ls.bedroom,
        ls.sleeps,
        ls.bathroom,
        ls.location_3_title,
        ls.location_4_title,
        ls.zip_code FROM
        '.$listing_summary.' ls WHERE ls.id='.$resource_ids;
        $result=self::fetch($sql);
        return $result;
    } 
    
    public static function getSpecials($specials_id=0, $offset = 0, $per_page = 10)
    {
        $listing_summary=config('tables.domain.listing_summary');
        $image=config('tables.domain.image');
        $rate=config('tables.domain.rate');
        $listing_choice=config('tables.domain.listing_choice');
        $review=config('tables.domain.review');
        $listing_availablity_booking=config('tables.domain.listing_availablity_booking');
        
        $sql_ec = "SELECT DISTINCT listing_id FROM ".$listing_choice." WHERE status = 'A' AND editor_choice_id IN (".$specials_id.")"; 
        $result_ec=self::fetch($sql_ec);
        
        $specials_ids = array();
        if(isset($result_ec) && !empty($result_ec))
        {
             foreach($result_ec as $val)
            {
               $specials_ids[] = $val->listing_id; 
            }
            $cleansp_ids = implode(",",$specials_ids);
        }
        else
        {
             $cleansp_ids='';
        }
       
        if($cleansp_ids!='')
        {
            $sql_listing = "SELECT id from ".$listing_summary." WHERE Listing_Summary.status = 'A' AND (latitude <= 174.73204925797 AND latitude >= -114.73204925797 AND longitude <= 214.73204925797 AND longitude >= -74.732049257973) AND Listing_Summary.id IN (".$cleansp_ids.") AND (( Listing_Summary.search_pos < 6 AND Listing_Summary.search_pos_date >= CURDATE()) OR Listing_Summary.search_pos_date = '0000-00-00')";
            $result_list=self::fetch($sql_listing);
            $specials_ids_temp = array();
            if(isset($result_list) && !empty($result_list))
            {
                foreach($result_list as $val)
                {
                    $specials_ids_temp[] = $val->id; 
                }
                $cleansp_ids_temp = implode(",",$specials_ids_temp);
            }
            else
            {
                $cleansp_ids_temp='';
            }
        }
        else
        {
            $cleansp_ids_temp='';
        }
           
            
            
        $limit_str = " LIMIT $offset, $per_page";  
        
        if($cleansp_ids_temp!='')
        {
            $orderBy_str='ORDER BY ls.location_4_title ASC,
            ls.location_3_title ASC,
            ls.search_pos ASC,
            ls.level,
            reg_exp_order DESC,
            ls.backlink DESC,
            ls.random_number DESC,
            ls.title,
            ls.id '; 
             
            $sql_result="SELECT SQL_CALC_FOUND_ROWS 
            ls.id AS rental_id,ls.LEVEL,
            ls.title,
            ls.`address`,
            ls.`address2`,
            ls.bedroom,
            ls.sleeps,
            ls.bathroom,
            ls.location_3,
            ls.location_3_title,
            ls.location_4_title,
            ls.zip_code,
            ls.required_stay,
            ls.latitude,
            ls.longitude,
            i.id AS image_id,
            i.prefix,
            i.type,
            i.width,
            i.height,
            (CASE WHEN rv.rating IS NULL  THEN 0 ELSE rv.rating END) AS rating,  
            lc.id AS listing_choice,
            lc.description as lc_description,
            lc.`renewal_date`,
             ls.title REGEXP '^$' AS reg_exp_order,
            SQRT(
                POW((69.1 * (30 - latitude)), 2) + POW((53.0 * (70 - longitude)), 2)
            ) AS zipcode_score 
       
            FROM
            ".$listing_summary." ls 
            
            LEFT JOIN ".$image." i 
            ON ls.image_id = i.id        
            LEFT JOIN  ".$review." rv ON rv.item_id=ls.id  AND rv.item_type = 'listing' AND rv.review IS NOT NULL AND rv.review != '' AND rv.approved = 1
            LEFT JOIN ".$listing_choice." lc 
            ON lc.listing_id = ls.id 
            AND lc.`status` = 'A' 
            AND DATE(lc.`renewal_date`) >= DATE(NOW()) 
            WHERE ls.id in (".$cleansp_ids_temp.")
            GROUP BY ls.id 
            ".$orderBy_str." ".$limit_str;
   
           
        
        $result=self::fetch($sql_result);
        $result_temp  = array();
        $sql="SELECT FOUND_ROWS() AS total";
            
        $total_rows= self::fetch($sql);
        if(!empty($result))
        {
            $rental_ids=array();
           
            
            foreach($result as $key=>$val)
            {
                $result_temp[$key]['rental_id'] = $val->rental_id;
                $result_temp[$key]['title'] = $val->title;
                $result_temp[$key]['address'] = $val->address;
                $result_temp[$key]['address2'] = $val->address2;
                $result_temp[$key]['bedroom'] = $val->bedroom;
                $result_temp[$key]['sleeps'] = $val->sleeps;
                $result_temp[$key]['bathroom'] = $val->bathroom;
                $result_temp[$key]['location_3'] = $val->location_3;
                $result_temp[$key]['location_3_title'] = $val->location_3_title;
                $result_temp[$key]['location_4_title'] = $val->location_4_title;
                $result_temp[$key]['zip_code'] = $val->zip_code;
                $result_temp[$key]['required_stay'] = $val->required_stay;
                $result_temp[$key]['latitude'] = $val->latitude;
                $result_temp[$key]['longitude'] = $val->longitude;
                $result_temp[$key]['image_id'] = $val->image_id;
                $result_temp[$key]['prefix'] = $val->prefix;
                $result_temp[$key]['type'] = $val->type;
                $result_temp[$key]['width'] = $val->width;
                $result_temp[$key]['height'] = $val->height;
                $result_temp[$key]['rating'] = $val->rating;
                $result_temp[$key]['listing_choice'] = $val->listing_choice;
                $result_temp[$key]['lc_description'] = $val->lc_description;
                $result_temp[$key]['renewal_date'] = $val->renewal_date;
                
                
                
                $sql_rate = "SELECT MIN(r.DAY) AS min_day,
                MAX(r.DAY) AS max_day,
                MIN(r.week) AS min_week,
                MAX(r.week) AS max_week,
                MIN(r.season) AS min_season,
                MAX(r.season) AS max_season
                FROM 
                Rate r 
                WHERE 
                r.listing_id=".$val->rental_id."
                AND r.WEEK > 0 GROUP BY r.`listing_id` "; 
                $result_rate=self::fetch($sql_rate);
            
                if(isset($result_rate[0]->min_day))
                {
                   $result_temp[$key]['min_day'] = $result_rate[0]->min_day; 
                }
                else
                {
                    $result_temp[$key]['min_day'] = 0;
                }
                
                if(isset($result_rate[0]->max_day))
                {
                   $result_temp[$key]['max_day'] = $result_rate[0]->max_day; 
                }
                else
                {
                    $result_temp[$key]['max_day'] = 0;
                }
                
                if(isset($result_rate[0]->min_week))
                {
                   
                   
                   $result_temp[$key]['min_week'] = $result_rate[0]->min_week; 
                }
                else
                {

                    $result_temp[$key]['min_week'] = 0;
                }
                
                if(isset($result_rate[0]->max_week))
                {
                   $result_temp[$key]['max_week'] = $result_rate[0]->max_week; 
                }
                else
                {
                    $result_temp[$key]['max_week'] = 0;
                }
                
                
                if(isset($result_rate[0]->min_month))
                {
                   $result_temp[$key]['min_month'] = $result_rate[0]->min_month; 
                }
                else
                {
                    $result_temp[$key]['min_month'] = 0;
                }
                
                if(isset($result_rate[0]->max_month))
                {
                   $result_temp[$key]['max_month'] = $result_rate[0]->max_month; 
                }
                else
                {
                    $result_temp[$key]['max_month'] = 0;
                }
                
                
                if(isset($result_rate[0]->min_season))
                {
                   $result_temp[$key]['min_season'] = $result_rate[0]->min_season; 
                }
                else
                {
                    $result_temp[$key]['min_season'] = 0;
                }
                
                if(isset($result_rate[0]->max_season))
                {
                   $result_temp[$key]['max_season'] = $result_rate[0]->max_season; 
                }
                else
                {
                    $result_temp[$key]['max_season'] = 0;
                }
                
                
            }
        }
        else
        {
            $result_temp=array();
        }
    }
    else
    {
        $result_temp=array();
    }
        return array('total' => $total_rows,'result'=>$result_temp);
        
    } 
    
    public static function getSpecialTitle($sp_id=0)
    {
        $editor_choice=config('tables.domain.editor_choice');
        $sql='SELECT name,title,meta_description,meta_keys FROM '.$editor_choice.' WHERE id='.$sp_id;
        $result=self::fetch($sql);
        return $result;
    }
   
      
      public static function UpdateRandNum()
      {
		$listing_summary=config('tables.domain.listing_summary');
        $sqllisting="select id from ".$listing_summary." where status='A'";
        $sqllisting = self::fetch($sqllisting);
        foreach($sqllisting as $val)
        {
            $randnumber = rand(1,100000000);
            $sqlUpdate="Update ".$listing_summary." set random_number=".$randnumber.' where id='.$val->id;
            $sqlResult = self::updateSql($sqlUpdate);
        }
      } 
      
      public static function ListingLocation()
      {
        $listing_summary=config('tables.domain.listing_summary');
        $sql="SELECT DISTINCT 
        location_4 
        FROM
        ".$listing_summary." 
        WHERE STATUS = 'A' 
        AND DATE(`renewal_date`) >= DATE(NOW())
        AND location_4 <> 0 ";
        $result=self::fetch($sql);
        return $result;
                
      } 
      
      public static function getSpId($special='')
      {
        $editor_choice=config('tables.domain.editor_choice');
        $sql="SELECT 
        id 
        FROM
        ".$editor_choice." 
        WHERE LOWER(REPLACE(`name`, ' ', '')) = '".$special."' ";
        $result=self::fetch($sql);
        return $result;
                
      }
      
       public static function citiesListing($locationLevel, $module, $orderField, $limit)
      {
        
        $listing_locationcounter=config('tables.domain.listing_locationcounter');
        $sql="SELECT count as total, title, full_friendly_url as url, location_id as id FROM ".$listing_locationcounter." WHERE location_level = ".$locationLevel." AND title != '' ORDER BY ".$orderField." LIMIT ".$limit;
        $result=self::fetch($sql);
        return $result;
      }
      
      
       public static function citiesFilters($state=0)
      {
        
        $listing_locationcounter=config('tables.domain.listing_locationcounter');
        $sql="select full_friendly_url from Listing_LocationCounter where location_id=".$state;
        $resultTemp=self::fetch($sql);
        if(!empty($resultTemp) && $resultTemp[0]->full_friendly_url)
        {
           $sqlCities = "SELECT title, full_friendly_url, location_id FROM ".$listing_locationcounter." WHERE full_friendly_url LIKE '".$resultTemp[0]->full_friendly_url."%' ORDER BY title"; 
           $result=self::fetch($sqlCities);
           return $result;
        }
        else
        {
            return array();
        }
      }
      //public static function getSearchResultMeta($state=0)
//      {
//        
//        $listing_locationcounter=config('tables.domain.listing_locationcounter');
//        $sql="select title from Listing_LocationCounter where location_id=".$state;
//        $resultTemp=self::fetch($sql);
//        if(!empty($resultTemp) && $resultTemp[0]->title)
//        {
//           return $resultTemp[0]->title;
//        }
//        else
//        {
//            return '';
//        }
//      }
      
      
     
     public static function sponsersMeta($accountId=0)
     {
        $customtext=config('tables.domain.customtext');
        $sql = "SELECT name, value FROM ".$customtext." WHERE (name = 'header_title' OR name = 'header_author' OR name = 'header_description' OR name = 'header_keywords')"; 
        $result=self::fetch($sql);
        return $result;
     } 
     
     public static function SponserListingById($item_id=0,$accountId=0)
     {
        $listing=config('tables.domain.listing');
        $sql = "SELECT * FROM ".$listing." WHERE id =".$item_id; 
        $result=self::fetch($sql);
        return $result;
     }
     public static function SponserListingId($item_id=0)
     {
        $listing=config('tables.domain.listing');
        $sql = "SELECT * FROM ".$listing." WHERE id =".$item_id; 
        $result=self::fetch($sql);
        return $result;
     }
      
       public static function SponserListingRate($item_id=0)
     {
        $rate=config('tables.domain.rate');
        $sql = "SELECT * FROM ".$rate." WHERE listing_id =".$item_id; 
        $result=self::fetch($sql);
        return $result;
     }
   public static function SponserRateInformation($item_id=0)
     {
        $rateinfo=config('tables.domain.rateinformation');
        $sql = "SELECT * FROM ".$rateinfo." WHERE listing_id =".$item_id; 
        $result=self::fetch($sql);
        return $result;
     }
     public static function EditorChoice($item_id=0)
     {
         $editor_choice=config('tables.domain.editor_choice');
        $image=config('tables.domain.image');
        $sql="SELECT ec.id as editor_choice_id,ec.name,ec.image_id,ec.available,ec.price,ec.renewal_period,ec.title,
        i.id,i.prefix ,i.type,i.width,i.height
        FROM
        ".$editor_choice."  ec JOIN ".$image." i ON ec.image_id=i.id and ec.available=".$item_id."
        ORDER BY editor_choice_id ";
        $result=self::fetch($sql);
        return $result;
     }
     
    public static function SponserListing($accountId=0)
     {
        $listing=config('tables.domain.listing');
        $sql = "SELECT id, LEVEL, title, address, STATUS, friendly_url, promotion_id FROM ".$listing." WHERE account_id = ".$accountId." ORDER BY LEVEL, title"; 
        $result=self::fetch($sql);
        return $result;
     } 
     
      public static function SponserListingGallery($itemId=0 , $accountId=0)
     {
        $gallery_item=config('tables.domain.gallery_item');
        $sql = "SELECT * FROM ".$gallery_item." WHERE item_type='listing' AND item_id = ".$itemId." ORDER BY gallery_id"; 
        $result=self::fetch($sql);
        return $result;
     }
     
     public static function getGalleryId($itemId=0 , $module=0)
     {
        $gallery_item=config('tables.domain.gallery_item');
        $sql = "SELECT * FROM ".$gallery_item." WHERE item_id = ".$itemId." AND item_type = '$module' ORDER BY id"; 
        $result=self::fetch($sql);
        return $result;
     }
     
     public static function getGallery($gallery_id=0)
     {
        $gallery_item=config('tables.domain.gallery_item');
        $sql = "SELECT * FROM Gallery_Image WHERE gallery_id = $gallery_id  ORDER BY id ASC"; 
        $result=self::fetch($sql);
        return $result;
     }
     public static function getGallery_Temp($sess_id=0)
     {
        $gallery_temp=config('tables.domain.gallery_temp');
        $sql = "SELECT * FROM Gallery_Temp WHERE sess_id ='$sess_id'";  
        $result=self::fetch($sql);
        return $result; 
     }
      public static function delGallery_Temp($sess_id=0)
     {
        $gallery_temp=config('tables.domain.gallery_temp');
         $sql = "DELETE FROM Gallery_Temp WHERE sess_id = '$sess_id'";
        $result=self::fetch($sql);
        return $result;
     }
     public static function Gallery_TempCount($sess_id=0)
     {
        
        $sql = "SELECT COUNT(image_id) AS total FROM Gallery_Temp WHERE sess_id = ".$sess_id; 
        $result=self::fetch($sql);
        return $result;
     }

      public static function system_CheckListingAvailabilityBooking ($listing_id = false, $start_date = false, $start_date_period = false, $end_date = false, $end_date_period = false, $availability_id = false) {
        
        if (!$listing_id || !$start_date || !$start_date_period || !$end_date || !$end_date_period) return false;

        $return = false;
        $return_aux = false;
        
        $sql = "SELECT * FROM ListingAvailabilityBooking WHERE listing_id = ".$listing_id." AND (('".$start_date."' BETWEEN start_date AND end_date) OR ('".$end_date."' BETWEEN start_date AND end_date) OR ('".$start_date."'  < start_date AND '".$end_date."' > end_date) )";
        if($availability_id) $sql .= " AND id <> ".$availability_id;
        $result = self::fetch($sql);
        if($result){
                $rows=$result;
                if($rows){
                   $return = true;
                   foreach($rows as $rows_item){
                       if(($start_date == $rows_item->end_date && $start_date_period > $rows_item->end_date_period) || ($end_date == $rows_item->start_date && $end_date_period < $rows_item->start_date_period))
                            $return_aux = true;
                   }
                }
        }
        if($return_aux && $return) $return = false;
        return $return;
   
    }
     public static function system_CheckDateListingAvailabilityBooking ($listing_id = false, $date = false, &$availability_id_aux_color) {
        
        if (!$listing_id || !$date ) return false;

        $return = "available";
        $return_aux = "available";
        $return_aux_pm = "";
        $return_aux_am = "";
        $getdate=Functions::import_formatDate($date);
        
        $sql = "SELECT * FROM ListingAvailabilityBooking WHERE listing_id = ".$listing_id." AND (('".$getdate."' BETWEEN start_date AND end_date))";
        $result = self::fetch($sql);
        if($result){
                $rows= $result;

                if($rows){
                    $return_aux = "booked";
                    
                    foreach($rows as $rows_item){
                        
                        unset($date_compare_array,$date_compare);
                        $availability_id_aux_color = $rows_item->id;
                        $date_compare_array = explode("-",$getdate);
                        $date_compare = $date_compare_array[0]."-";
                        Functions::string_strlen($date_compare_array[1])==1? $date_compare .= "0".$date_compare_array[1] : $date_compare .= $date_compare_array[1]."-";
                        Functions::string_strlen($date_compare_array[2])==1? $date_compare .= "0".$date_compare_array[2] : $date_compare .= $date_compare_array[2];

                        if($date_compare == $rows_item->start_date && $rows_item->start_date_period==2)
                            $return_aux_pm = "booked_pm";
                        elseif($date_compare == $rows_item->end_date && $rows_item->end_date_period==1)
                            $return_aux_am = "booked_am";
                    }
                }
        }
        if($return_aux_pm && !$return_aux_am) $return_aux = $return_aux_pm;
        elseif($return_aux_am && !$return_aux_pm) $return_aux = $return_aux_am;
        if($return != $return_aux) $return = $return_aux;
        return $return;
    }
    

     public static function system_GetListingAvailabilityById($id = false)
     {
        $availablity=config('tables.domain.listing_availablity_booking');
        $sql = "SELECT * FROM ListingAvailabilityBooking WHERE id = ".$id; 
        $result=self::fetch($sql);
        return $result;
     }
     
     public static function system_GetListingAvailabilityByListingId($listing_id = false)
     {
        $availablity=config('tables.domain.listing_availablity_booking');
        $sql = "SELECT * FROM ListingAvailabilityBooking WHERE listing_id = ".$listing_id." ORDER BY start_date, start_date_period"; 
        $result=self::fetch($sql);
        return $result;
     }


      public static function system_SaveListingAvailabilityBooking ($listing_id = false, $start_date = false, $start_date_period = false, $end_date = false, $end_date_period = false, $customer_name = false, $availability_id = false) {
       
        //if (!$listing_id || !$start_date || !$start_date_period || !$end_date || !$end_date_period || !$customer_name) return false;
        
        if($availability_id){
            $sql = "UPDATE ListingAvailabilityBooking SET listing_id = ".$listing_id.", start_date = '".$start_date."', start_date_period = ".$start_date_period.", end_date = '".$end_date."', end_date_period = ".$end_date_period.", customer_name = '".$customer_name."' WHERE id = ".$availability_id;
        }
        else{
            $sql = "INSERT INTO ListingAvailabilityBooking (listing_id, start_date, start_date_period, end_date, end_date_period, customer_name) VALUES (".$listing_id.", '".$start_date."', ".$start_date_period.", '".$end_date."', ".$end_date_period.", '".$customer_name."')";
        }
      //d($sql,1);die;
       self::insertSql($sql);
        
    }
    public static function getListingLevel()
    {
        $sql="SELECT  * FROM ListingLevel WHERE theme = 'default' ORDER BY value DESC Limit 4";
         $result=self::fetch($sql);
         return $result;
    }



     public static function system_DeleteAvailabilityById ($id = false) {
        
        if (!$id) return false;

        $sql = "DELETE FROM ListingAvailabilityBooking WHERE id = ".$id;
        self::deleteSql($sql);
    }

     public static function getImageFromListing($item_id=0)
     {
        $listing=config('tables.domain.listing');
        $sql = "SELECT image_id,thumb_id FROM ".$listing." WHERE id = ".$item_id; 
        $result=self::fetch($sql);
        return $result;
     }
     
     
     public static function getImageById($Image_id=0)
     {
        
        $image=config('tables.domain.image');
        $sql = "SELECT *  FROM ".$image." WHERE id = ".$Image_id; 
        $result=self::fetch($sql);
        return $result;
     }
     
     public static function getOldImage($sql='')
     {
        
        $result=self::fetch($sql);
        return $result;
     }
     
     public static function updateImageType($sql='')
     {
        
        $result=self::updateSql($sql);
        return $result;
     }
     
     public static function updateTempGallery($sql='')
     {
        
        $result=self::updateSql($sql);
        return $result;
     }
     
      public static function updateListingDeposite($sql='')
     {
        
        $result=self::updateSql($sql);
        return $result;
     }
     
     
     
     
     public static function getImageGallery($sql='')
     {
        
        $result=self::fetch($sql);
        return $result;
     }
     
     public static function tempGalleryCount($sql='')
     {
        
        $result=self::fetch($sql);
        return $result;
     }
     
     public static function getTotalofImages($sql='')
     {
        
        $result=self::fetch($sql);
        return $result;
     }
     
     
     public static function getSelectLocation4($sql='')
     {
        
        $result=self::fetch($sql);
        return $result;
     }
    
     public static function inserImageNew($sql='')
     {
        
        $result=self::insertSql($sql);
        return $result;
     }

     public static function insertTempGallery($sql='')
     {
        
        $result=self::insertSql($sql);
        return $result;
     }
     
      public static function insertGallery($sql='')
     {
        
        $result=self::insertSql($sql);
        return $result;
     }

        public static function insertGalleryitem($sql='')
     {
        
        $result=self::insertSql($sql);
        return $result;
     }
     
     public static function getFriendlyUrl($sql='')
     {
        
        $result=self::fetch($sql);
        return $result;
     }

      public static function getId($sql='')
     {
        
        $result=self::fetch($sql);
        return $result;
     }
     
     
     
     public static function deleteImage($sql='')
     {
        
        $result=self::deleteSql($sql);
        return $result;
     }
     
               
    public static function fetch($sql)
    {
        $data= DB::connection('domain')->select($sql);
        return $data;
    } 
    
    
    
    
    public static function updateSql($sql)
    {
        $data= DB::connection('domain')->update($sql);
        return $data;
    }
    
    public static function tempGalleryupdate($sql)
    {
        $data= DB::connection('domain')->update($sql);
        return $data;
    }
    
    
    public static function inserCroppedImage($sql='')
    {
        DB::connection('domain')->insert($sql);
        $sql = "SELECT id FROM Image
        ORDER BY id DESC
        LIMIT 1;";
        $data= DB::connection('domain')->select($sql);
        return $data[0]->id;
        
    }
    public static function inserCroppedMainImage($sql='')
    {
        DB::insert($sql);
        $sql = "SELECT id FROM Image
        ORDER BY id DESC
        LIMIT 1;";
        $data= DB::select($sql);
        return $data[0]->id;
        
    }
    
    public static function insertSql($sql)
    {
        $data= DB::connection('domain')->insert($sql);
        return $data;
    }
    public static function insertSqlId($sql)
    {
        DB::connection('domain')->insert($sql);
        $sel = "SELECT id FROM Invoice
        ORDER BY id DESC
        LIMIT 1;";
        $data= DB::connection('domain')->select($sel);
        return $data[0]->id;
    }
    
    public static function deleteSql($sql)
    {
        $data= DB::connection('domain')->delete($sql);
        return $data;
    }
    public static function deleteSqlMain($sql)
    {
        $data= DB::delete($sql);
        return $data;
    }
    public static function fetchMain($sql)
    {
            $data= DB::select($sql);
            return $data;
    }
     public static function insertSqlMain($sql)
    {
            $data= DB::insert($sql);
            return $data;
   }
    public static function updateSqlMain($sql)
    {
            $data= DB::update($sql);
            return $data;
   }

   

}