<?php 
namespace App\Functions;
use App\Functions\Functions;
use DB;
use PDO;
use App\Models\Domain\Sql;
use App\Functions\Account_Domain;


	# ----------------------------------------------------------------------------------------------------
	# * FILE: class_customInvoice.php
	# ----------------------------------------------------------------------------------------------------

	class CustomInvoice extends Handle {

		var $id;
		var $account_id;
		var $title;
		var $date;
		var $sent_date;
		var $amount;
		var $paid;
		var $sent;
		var $completed;

		function CustomInvoice($var="") {


	if (is_numeric($var) && ($var)) {

				DB::connection('domain')->setFetchMode(PDO::FETCH_ASSOC);

				$row=DB::connection('domain')->table('CustomInvoice')->where('id',$var)->get();
				//$sql = "SELECT * FROM CustomInvoice WHERE id = $var";
				//$row[0] = Sql::fetch($sql);

				$this->old_account_id = $row[0]["account_id"];

				$this->makeFromRow($row);
				
			} else {
                if (!is_array($var)) {
                    $var = array();
                }
				$this->makeFromRow($var);
			}



		}

		function makeFromRow($row="") {

			$this->id			= ($row[0]["id"])			? $row[0]["id"]			: ($this->id			? $this->id			: 0);
			$this->account_id	= ($row[0]["account_id"])	? $row[0]["account_id"]	: ($this->account_id	? $this->account_id	: 0);
			$this->title		= ($row[0]["title"])		? $row[0]["title"]			: ($this->title			? $this->title		: 0);
			$this->date			= ($row[0]["date"])		? $row[0]["date"]			: ($this->date			? $this->date		: "");
			$this->sent_date	= ($row[0]["sent_date"])	? $row[0]["sent_date"]		: ($this->sent_date		? $this->sent_date	: "");
			$this->subtotal		= ($row[0]["subtotal"])	? $row[0]["subtotal"]		: ($this->subtotal		? $this->subtotal	: 0);
			$this->tax			= ($row[0]["tax"])			? $row[0]["tax"]			: ($this->tax			? $this->tax		: 0);
			$this->amount		= ($row[0]["amount"])		? $row[0]["amount"]		: ($this->amount		? $this->amount		: 0);
			$this->paid			= ($row[0]["paid"])		? $row[0]["paid"]			: ($this->paid			? $this-paid		: "");
			$this->sent			= ($row[0]["sent"])		? $row[0]["sent"]			: ($this->sent			? $this->sent		: "");
			$this->completed	= ($row[0]["completed"])	? $row[0]["completed"]		: ($this->completed		? $this->completed	: "y");
				DB::connection('domain')->setFetchMode(PDO::FETCH_CLASS);

		}

		function Save() {


			$this->prepareToSave();

			$aux_old_account = str_replace("'", "", $this->old_account_id);
			$aux_account = str_replace("'", "", $this->account_id);

			if ($this->id) {

				$sql  = "UPDATE CustomInvoice SET"
					. " account_id = $this->account_id,"
					. " title = $this->title,"
					. " date = NOW(),"
					. " sent_date = $this->sent_date,"
					. " subtotal = $this->subtotal,"
					. " tax = $this->tax,"
					. " amount = $this->amount,"
					. " paid = $this->paid,"
					. " sent = $this->sent,"
					. " completed = $this->completed"
					. " WHERE id = $this->id";
				Sql::updateSql($sql);

				if ($aux_old_account != $aux_account && $aux_account != 0) {
					$accDomain = new Account_Domain($aux_account, 1);
					$accDomain->Save();
					$accDomain->saveOnDomain($aux_account, $this);
				}

			} else {

				$sql = "INSERT INTO CustomInvoice"
					. " (account_id,"
					. " title,"
					. " date,"
					. " sent_date,"
					. " subtotal,"
					. " tax,"
					. " amount,"
					. " paid,"
					. " sent,"
					. " completed"
					. " )"
					. " VALUES"
					. " ("
					. " $this->account_id,"
					. " $this->title,"
					. " NOW(),"
					. " $this->sent_date,"
					. " $this->subtotal,"
					. " $this->tax,"
					. " $this->amount,"
					. " $this->paid,"
					. " $this->sent,"
					. " $this->completed"
					. " )";

				Sql::insertSql($sql);
				$this->id = DB::connection('domain')->getpdo()->lastInsertId();
				if ($aux_account != 0) {
					$accDomain = new Account_Domain($aux_account, 1);
					$accDomain->Save();
					$accDomain->saveOnDomain($aux_account, $this);
				}

			}

			$this->PrepareToUse();

		}

		function setItems($items_desc, $items_price) {
			$dbMain = db_getDBObject(DEFAULT_DB, true);
			if (defined("SELECTED_DOMAIN_ID")) {
				$dbObj = db_getDBObjectByDomainID(SELECTED_DOMAIN_ID, $dbMain);
			} else {
				$dbObj = db_getDBObject();
			}
			unset($dbMain);
			$sql = "DELETE FROM CustomInvoice_Items WHERE custominvoice_id = $this->id";
			$dbObj->query($sql);
			if ($items_desc && (count($items_desc) == count($items_price)) && $this->id) {
				foreach ($items_desc as $key => $each_item_desc) {
					if ($each_item_desc) {
						$sql = "INSERT INTO CustomInvoice_Items (custominvoice_id, description, price) VALUES ($this->id, ".db_formatString($each_item_desc).", ".db_formatString($items_price[$key]).")";
						$result = $dbObj->query($sql);
					}
				}
				unset($dbObj);
			} else {
				unset($dbObj);
				return false;
			}
		}

		function getItems() {
			if ($this->id) {
				
				DB::connection('domain')->setFetchMode(PDO::FETCH_ASSOC);

				$sql = "SELECT * FROM CustomInvoice_Items WHERE custominvoice_id='".$this->id."' ORDER BY id";
				$result = Sql::fetch($sql);
				foreach($result as $row[0]) $data[] = $row[0];


				if ($data) return $data;
				else return false;
			}
		}

		function getTextItems() {

			if ($this->id) {

				DB::connection('domain')->setFetchMode(PDO::FETCH_ASSOC);

				$sql = "SELECT * FROM CustomInvoice_Items WHERE custominvoice_id='".$this->id."' ORDER BY id";
				$result = Sql::fetch($sql);
				foreach($result as $row[0]) $data[] = $row[0];

				if ($data) {
					foreach ($data as $each_item) {
						$textItems[] = $each_item["description"];
					}
				}

				if ($textItems) $return_items = implode("\n", $textItems);


				return $return_items;
			}

		}

		function getTextPrices() {

			if ($this->id) {

				DB::connection('domain')->setFetchMode(PDO::FETCH_ASSOC);

				$sql = "SELECT * FROM CustomInvoice_Items WHERE custominvoice_id='".$this->id."' ORDER BY id";
				$result = Sql::fetch($sql);
				foreach($result as $row[0]) $data[] = $row[0];

				if ($data) {
					foreach ($data as $each_item) {
						$textPrices[] = $each_item["price"];
					}
				}

				if ($textPrices) $return_prices = implode("\n", $textPrices);

				unset($dbObj);

				return $return_prices;
			}

		}

		function getPrice() {
			return $this->amount;
		}

		function Delete($domain_id = false) {

			$dbMain = db_getDBObject(DEFAULT_DB, true);
			if ($domain_id) {
				$dbObj = db_getDBObjectByDomainID($domain_id, $dbMain);
			} else {
				if (defined("SELECTED_DOMAIN_ID")) {
					$dbObj = db_getDBObjectByDomainID(SELECTED_DOMAIN_ID, $dbMain);
				} else {
					$dbObj = db_getDBObject();
				}
				unset($dbMain);
			}

			$sql = "UPDATE Invoice_CustomInvoice SET custom_invoice_id = '0' WHERE custom_invoice_id = $this->id";
			$dbObj->query($sql);

			$sql = "UPDATE Payment_CustomInvoice_Log SET custom_invoice_id = '0' WHERE custom_invoice_id = $this->id";
			$dbObj->query($sql);

			$sql = "DELETE FROM CustomInvoice_Items WHERE custominvoice_id = $this->id";
			$dbObj->query($sql);

			$sql = "DELETE FROM CustomInvoice WHERE id = $this->id";
			$dbObj->query($sql);

			unset($dbObj);

		}

		function deletePerAccount($account_id = 0, $domain_id = false) {
			if (is_numeric($account_id) && $account_id > 0) {
				$dbMain = db_getDBObject(DEFAULT_DB, true);
				if ($domain_id) {
					$dbObj = db_getDBObjectByDomainID($domain_id, $dbMain);
				} else {
					if (defined("SELECTED_DOMAIN_ID")) {
						$dbObj = db_getDBObjectByDomainID(SELECTED_DOMAIN_ID, $dbMain);
					} else {
						$dbObj = db_getDBObject();
					}
					unset($dbMain);
				}
				$sql = "SELECT * FROM CustomInvoice WHERE account_id = $account_id";
				$result = $dbObj->query($sql);
				while ($row[0] = mysql_fetch_array($result)) {
					$this->makeFromRow($row[0]);
					$this->Delete($domain_id);
				}

				unset($dbObj);
			}
		}
	}

?>
