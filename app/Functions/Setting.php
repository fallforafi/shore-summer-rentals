<?php
namespace App\Functions;
use DB;

	/*==================================================================*\
	######################################################################
	#                                                                    #
	# Copyright 2005 Arca Solutions, Inc. All Rights Reserved.           #
	#                                                                    #
	# This file may not be redistributed in whole or part.               #
	# eDirectory is licensed on a per-domain basis.                      #
	#                                                                    #
	# ---------------- eDirectory IS NOT FREE SOFTWARE ----------------- #
	#                                                                    #
	# http://www.edirectory.com | http://www.edirectory.com/license.html #
	######################################################################
	\*==================================================================*/

	# ----------------------------------------------------------------------------------------------------
	# * FILE: /classes/class_setting.php
	# ----------------------------------------------------------------------------------------------------

	class Setting extends Handle {
		
		var $name;
		var $value;
		var $in_main_db = Array("sitemgr_username", 
							    "sitemgr_password", 
			                    "sitemgr_faillogin_count", 
			                    "sitemgr_faillogin_datetime", 
			                    "sitemgr_first_login", 
			                    "sitemgr_language", 
			                    "complementary_info",
			                    "sugar_url",
			                    "sugar_user",
			                    "sugar_password");
		
		function Setting($var='') {
			if ($var) {
				
				$sql = "SELECT * FROM Setting WHERE name = '$var'";
				$row = Self::fetch($sql);//fetchMain
				$this->makeFromRow($row);
			} else {
                if (!is_array($var)) {
                    $var = array();
                }
				$this->makeFromRow($var);
			}
		}
		
		function makeFromRow($row='') {
			
			$this->name		= ($row[0]->name)	? $row[0]->name	: ($this->name	? $this->name	: 0);
			$this->value	= ($row[0]->value)	? $row[0]->value	: "";
			
		}
		
		function Save($update = true) {
			
			
			
			$this->prepareToSave();

			if ($update) {
				
				$sql = "UPDATE Setting SET"
					. " value      = $this->value"
					. " WHERE name = $this->name";

				Self::updateSqlMain($sql);
				
			} else {
				
				$sql = "INSERT INTO Setting"
					. " (name,"
					. " value)"
					. " VALUES"
					. " ($this->name,"
					. " $this->value)";
				
				Self::insertSql($sql);
				
			}
			
			$this->prepareToUse();
			$this->setting_constants();
			
		}
		
        /*
	 * Function to create a constant with table of setting Information
	 */
	function setting_constants(){
		if(defined('SETTING_INFORMATION')) return false;
		unset($settingObj,$array_setting);

		$settingObj = new Setting();
		$array_setting = $settingObj->convertTableToArray();

		if(is_array($array_setting)){
			define("SETTING_INFORMATION", serialize($array_setting));
		}

	}
    
		function Delete() {
			$dbMain = db_getDBObject(DEFAULT_DB, true);
			if (in_array($this->name, $this->in_main_db)) {
				$dbObj = $dbMain;
			} else if (defined("SELECTED_DOMAIN_ID")) {
				$dbObj = db_getDBObjectByDomainID(SELECTED_DOMAIN_ID, $dbMain);
			} else {
				$dbObj = db_getDBObject();
			}
//			$dbMain->close();
			unset($dbMain);
			$sql = "DELETE FROM Setting WHERE name = ".db_formatString($this->name);
			$dbObj->query($sql);
			if (mysql_affected_rows($dbObj->link_id)) {
				setting_constants();
				return true;
			}
			return false;
		}

		function convertTableToArray(){
			

            $sql    = "SELECT * FROM Setting";
            $result = Self::fetchMain($sql);
			if(!empty($result)){
				unset($array_setting);
				$array_setting = array();
				foreach($result as $row){
					$array_setting[$row->name]["name"]	= $row->name;
					$array_setting[$row->name]["value"]	= $row->value;
				}
				return $array_setting;
			}else{
				return false;
			}

		}
		
         public static function updateSql($sql)
    {
        $data= DB::connection('domain')->update($sql);
        return $data;
    }
    
    public static function updateSqlMain($sql)
    {
        $data= DB::update($sql);
        return $data;
    }
    
    
     public static function fetch($sql)
        {
            $data= DB::connection('domain')->select($sql);
            return $data;
        }
        
        public static function fetchMain($sql)
        {
            $data= DB::select($sql);
            return $data;
        }
        
        public static function insertSql($sql)
        {
            $data= DB::connection('domain')->insert($sql);
            return $data;
        }
        public static function insertSqlMain($sql)
        {
            $data= DB::insert($sql);
            return $data;
        }
        
        
        public static function deleteSql($sql)
        {
            $data= DB::connection('domain')->delete($sql);
            return $data;
        }
        
        public static function deleteSqlMain($sql)
        {
            $data= DB::delete($sql);
            return $data;
        }
        
	}
	
?>