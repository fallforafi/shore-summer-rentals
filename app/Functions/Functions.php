<?php // Code within app\Helpers\Helper.php
namespace App\Functions;
//use Intervention\Image\Facades\Image as Image;
use DB,PDO;
use Session;
use App\Functions\DiscountCode;
use App\Functions\ListingLevel;
use App\Functions\Listing;
use App\Functions\EmailNotification;
use App\Models\Domain\Sql;


class Functions
{
    //  $dates=config('params.DEFAULT_DATE_FORMAT');
    public static function prettyJson($inputArray, $statusCode)
    {
        return response()->json($inputArray, $statusCode, array('Content-Type' => 'application/json'), JSON_PRETTY_PRINT);
    }
    
    public static function saveImage($file,$destinationPath,$destinationPathThumb='')
    {
        $extension = $file->getClientOriginalExtension();
        $fileName = rand(111,999).time().'.'.$extension;
        $image = $destinationPath . '/' .$fileName;
        $upload_success=  $file->move($destinationPath,$fileName);
        //Functions::saveThumbImage($image,'fit',$destinationPath.$fileName);
        return $fileName;
    }
	
	// remove string from any length and concatinate three dots at end any string condition needle start to end 
	public static function stringTrim($string='',$needle=0,$start=0,$end=0)
	{
		return (strlen($string) > $needle) ? substr($string, $start,$end) . '...' : $string;
	}
    
    public static function makeOrderEmailTemplate($orders,$addresses)
    {
        //d($order,1);
        $template="";
        
        echo  view('email.order',compact('orders','addresses'));
        die('aaa');
    }
    
    public static function sendEmail($to,$subject,$message,$headers='')
    {
        if($headers=='')
        {
            $headers = 'From: info@shoresummerrentals.com' . "\r\n" .
            'Reply-To: info@shoresummerrentals.com' . "\r\n" .
            'X-Mailer: PHP/' . phpversion();  
        }
        
        if(mail($to, $subject, $message, $headers))
        {
            return true;
        }
        else{
            return false;
        }
    }
    
    public static function getSpecilas()
    {
         
        $editor_choice=config('tables.domain.editor_choice');
        //$sql='SELECT * FROM '.$editor_choice.'  WHERE available=1';
        $sql='SELECT * FROM '.$editor_choice.' ';
        $result=self::fetch($sql);
        return $result;
  
    }
     public static function footerSpecials()
    {            
        $editor_choice = config('tables.domain.editor_choice');
        $sql = 'SELECT * FROM Editor_Choice ORDER BY id ASC';
        $result=self::fetch($sql);
        return $result;
  
    }
     
    
    
    public static function pagination($result_count, $per_page = 10,$page = 1, $url = '?',$url_prefix='/screen-'){        
        
        $total = $result_count;
        $adjacents = "2"; 
 
        $page = ($page == 0 ? 1 : $page);  
        $start = ($page - 1) * $per_page;                               
         
        $prev = $page - 1;                          
        $next = $page + 1;
        $lastpage = ceil($total/$per_page);
        $lpm1 = $lastpage - 1;
         
        $pagination = "";
        if($lastpage > 1)
        {   
            $pagination .= "<ul class='pagination'>";
                    $pagination .= "<li class='details'>Page $page of $lastpage</li>";
            if ($lastpage < 7 + ($adjacents * 2))
            {   
                for ($counter = 1; $counter <= $lastpage; $counter++)
                {
                    if ($counter == $page)
                        $pagination.= "<li><a class='current'>$counter</a></li>";
                    else
                        $pagination.= "<li><a href='".($url.$url_prefix.''.$counter)."'>$counter</a></li>";                    
                }
            }
            elseif($lastpage > 5 + ($adjacents * 2))
            {
                if($page < 1 + ($adjacents * 2))     
                {
                    for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++)
                    {
                        if ($counter == $page)
                            $pagination.= "<li><a class='current'>$counter</a></li>";
                        else
                            $pagination.= "<li><a href='".($url.$url_prefix.''.$counter)."'>$counter</a></li>";                    
                    }
                    $pagination.= "<li class='dot'>...</li>";
                    $pagination.= "<li><a href='".($url.$url_prefix.''.$lpm1)."'>$lpm1</a></li>";
                    $pagination.= "<li><a href='".($url.$url_prefix.''.$lastpage)."'>$lastpage</a></li>";      
                }
                elseif($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2))
                {
                    $pagination.= "<li><a href='".($url.$url_prefix.'1')."'>1</a></li>";
                    $pagination.= "<li><a href='".($url.$url_prefix.'2')."'>2</a></li>";
                    $pagination.= "<li class='dot'>...</li>";
                    for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++)
                    {
                        if ($counter == $page)
                            $pagination.= "<li><a class='current'>$counter</a></li>";
                        else
                            $pagination.= "<li><a href='".($url.$url_prefix.''.$counter)."'>$counter</a></li>";                    
                    }
                    $pagination.= "<li class='dot'>..</li>";
                    $pagination.= "<li><a href='".($url.$url_prefix.''.$lpm1)."'>$lpm1</a></li>";
                    $pagination.= "<li><a href='".($url.$url_prefix.''.$lastpage)."'>$lastpage</a></li>";      
                }
                else
                {
                    $pagination.= "<li><a href='".($url.$url_prefix.'=1')."'>1</a></li>";
                    $pagination.= "<li><a href='".($url.$url_prefix.'=2')."'>2</a></li>";
                    $pagination.= "<li class='dot'>..</li>";
                    for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++)
                    {
                        if ($counter == $page)
                            $pagination.= "<li><a class='current'>$counter</a></li>";
                        else
                            $pagination.= "<li><a href='". ($url.$url_prefix.''.$counter)."'>$counter</a></li>";                    
                    }
                }
            }
             
            if ($page < $counter - 1){ 
                $pagination.= "<li><a href='".($url.$url_prefix.''.$next)."'>Next</a></li>";
                $pagination.= "<li><a href='".($url.$url_prefix.''.$lastpage)."'>Last</a></li>";
            }else{
                $pagination.= "<li><a class='current'>Next</a></li>";
                $pagination.= "<li><a class='current'>Last</a></li>";
            }
            $pagination.= "</ul>\n";      
        }
     
     
        return $pagination;
    } 
	
	
	public static function string_ucwords($str, $charset = 'UTF-8') {
		if (isset($str) || is_numeric($str)) {
			if (function_exists('mb_convert_case')) {
				$strUCWords = mb_convert_case($str, MB_CASE_TITLE, $charset);
			} else {
				$strUCWords = ucwords($str);
			}
		} else {
			$strUCWords = false;
		}
		return $strUCWords;
	}
   public static function string_strpos($haystack, $needle, $offset = 0, $charset = 'UTF-8') {
        if (isset($haystack) && isset($needle) || (is_numeric($haystack) && is_numeric($needle))) {
            if (is_array($haystack)) $haystack = implode(",", $haystack);
            if (function_exists('mb_strpos')) {
                $position = mb_strpos($haystack, $needle, $offset, $charset);
            } else {
                $position = strpos($haystack, $needle, $offset);
            }
        } else {
            $position = false;
        }
        return $position;
    }
    
    
    public static function SponsersProfile()
    {
        $accountId = Session::get('SESS_ACCOUNT_ID');
        if($accountId>0)
        {
            $account=config('tables.main.account');
            $profile=config('tables.main.profile');
            $sql='SELECT C.state,C.city,C.zip,C.address2,C.address, C.fax,C.phone,C.email,C.first_name, C.last_name, A.has_profile, P.friendly_url,P.nickname,
                P.personal_message,P.image_id,P.facebook_image FROM Contact C LEFT JOIN '.$account.' A ON (C.account_id = A.id)
										   LEFT JOIN '.$profile.' P ON (P.account_id = A.id)
										   WHERE A.id = '.$accountId;
            $result=self::fetchMain($sql);
            return $result;
        }
    }
	
    
    
    //truncate text function
public static function truncate($text='',$chars=0) {

    
    $text = $text." ";
    $text = substr($text,0,$chars);
    $text = substr($text,0,strrpos($text,' '));
    $text = $text."...";
    return $text;

}

public static  function format_date($value = false, $format ="m/d/Y"  , $field_type = "date", $pm = false) {
    $ts_date='';
        if (!$value) return false;
        switch ($field_type) {
            case "date":
                list($year,$month,$day) = explode("-",$value);
                if ($month>0 || $day>0 || $year>0) 
                    $ts_date = mktime(0,0,0,(int)$month,(int)$day,(int)$year);
                if ($ts_date <= 0) 
                    return false;
                return date("$format",$ts_date);
            break;
            case "datetime":
                $date_time = explode(" ",$value);
                list($year,$month,$day) = explode("-",$date_time[0]);
                list($hour,$minute,$second) = explode(":",$date_time[1]);
                if ($hour>0 || $minute>0 || $second>0 || $month>0 || $day>0 || $year>0)
                    $ts_date = mktime((int)$hour,(int)$minute,(int)$second,(int)$month,(int)$day,(int)$year);
                if ($ts_date <= 0) return false;
                return date("$format",$ts_date);
            break;
            case "set_event_datetime":
                $date_time = explode(" ",$value);
                list($month,$day,$year) = explode("/",$date_time[0]);
                list($hour,$minute,$second) = explode(":",$date_time[1]);
                if ($pm and $hour and $hour < 12) $hour = $hour + 12;
                if (!$pm and $hour and $hour == 12) $hour = $hour - 12;
                $hour = $hour ? $hour : "00";
                $minute = $minute ? $minute : "00";
                $second = $second ? $second : "00";
                if ($hour>0 || $minute>0 || $second>0 || $month>0 || $day>0 || $year>0)
                    $ts_date = mktime((int)$hour,(int)$minute,(int)$second,(int)$month,(int)$day,(int)$year);
                if ($ts_date <= 0) return false;
                return date("$format",$ts_date);
            break;
            case "get_event_datetime":
                $year = string_substr($value,0,4);
                $month = string_substr($value,5,2);
                $day = string_substr($value,8,2);
                $hour = string_substr($value,11,2);
                $minute = string_substr($value,14,2);
                $second = "00";
                if ($hour >= 12) $data["am_pm"] = "pm";
                elseif ($hour < 12) $data["am_pm"] = "am";
                if ($hour>0 || $minute>0 || $second>0 || $month>0 || $day>0 || $year>0)
                    $ts_date = mktime((int)$hour,(int)$minute,(int)$second,(int)$month,(int)$day,(int)$year);
                $data["date"] = date("$format",$ts_date);
                $data["time"] = date("h:i",$ts_date);
                return $data;
            break;
            case "timestamp":
                return date("$format",$value);
            break;
            case "gettimestamp":
                $date_time = explode(" ",$value);
                list($year,$month,$day) = explode("-",$date_time[0]);
                list($hour,$minute,$second) = explode(":",$date_time[1]);
                if ($hour>0 || $minute>0 || $second>0 || $month>0 || $day>0 || $year>0)
                    $ts_date = mktime((int)$hour,(int)$minute,(int)$second,(int)$month,(int)$day,(int)$year);
                if ($ts_date <= 0) return false;
                return $ts_date;
            break;
            case "dbtimestamp":
                $hour   = string_substr($value, 8, 2);
                $minute = string_substr($value, 10, 2);
                $second = string_substr($value, 12, 2);
                $month  = string_substr($value, 4, 2);
                $day    = string_substr($value, 6, 2);
                $year   = string_substr($value, 0, 4);
                if ($hour>0 || $minute>0 || $second>0 || $month>0 || $day>0 || $year>0)
                    return date($format, mktime((int)string_substr($value, 8, 2), (int)string_substr($value, 10, 2), (int)string_substr($value, 12, 2), (int)string_substr($value, 4, 2), (int)string_substr($value, 6, 2), (int)string_substr($value, 0, 4)));
                else
                    return false;
            break;
            case "datetocompare" :
                return string_substr($value,6,4).string_substr($value,0,2).string_substr($value,3,2);
            break;
            case "datestring" : 
                $date_time = explode(" ", $value);
                list($year, $month, $day) = explode("-", $date_time[0]);
                list($hour, $minute, $second) = explode(":", $date_time[1]);                  
                if ($hour > 0 || $minute > 0 || $second > 0 || $month > 0 || $day > 0 || $year > 0) {
                    $ts_date = mktime((int)$hour, (int)$minute, (int)$second, (int)$month, (int)$day, (int)$year);
                }
                if ($ts_date <= 0) return false;
                
                $isSitemgrLang = false;
                if ((strpos($_SERVER["PHP_SELF"], "/".config('params.SITEMGR_ALIAS')."") !== false)) {
                    $isSitemgrLang = true;
                    setting_get("sitemgr_language", $sitemgr_language_aux);
                }
                
                $thisYear = date("Y");

                if ((config('params.EDIR_LANGUAGE') == "en_us" && !$isSitemgrLang) || ($isSitemgrLang && $sitemgr_language_aux == "en_us")) {
                  
                    return date("M", $ts_date)." ".date("j", $ts_date).date("S", $ts_date).($year > $thisYear ? ", ".$year : "");
                } else {
                    return date("$format", $ts_date);
                }
                
            break;
        }
    }
    public static function validate_date($date) {
        
        $default_date_format = "m/d/Y";

        $aux = explode("/", $date);
        
        if (count($aux) == 3 ) {

            if (is_numeric($aux[0]) && is_numeric($aux[1]) && is_numeric($aux[2])) {
                if (trim($default_date_format) == "m/d/Y") {
                    $month = $aux[0];
                    $day   = $aux[1];
                    $year  = $aux[2];
                } elseif (trim($default_date_format) == "d/m/Y") {
                    $month = $aux[1];
                    $day   = $aux[0];
                    $year  = $aux[2];
                }

                if (checkdate((int)$month, (int)$day, (int)$year)) {
                    return true;
                }

                return false;

            }

            return false;

        }

        return false;

    }
    // format money from numeric values
    public static function format_money ($value, $decimal = true) {
        $value = number_format($value, 2, ".", ",");
        $value = str_replace(",","",$value);
        if (!is_numeric($value)) return "0.00";
        $aux = explode(".",$value);
        $cents = (count($aux) > 1)    ? array_pop($aux)   : "";
        $cents = (strlen($cents) > 2) ? string_substr($cents,0,2): $cents;
        $cents = str_pad($cents,2,"0",STR_PAD_RIGHT);
        $value = implode("",$aux);
        $formated_money = ($decimal) ? $value.".".$cents : $value ;
        return $formated_money;
    }

    //For Calender
    public static function system_getMonthByNumber($number,$abreviation = false)  {
        $month="";
        if($abreviation){
            switch($number) {
                case 1:  $month = "Jan"; break;
                case 2:  $month = "Feb"; break;
                case 3:  $month = "Mar"; break;
                case 4:  $month = "Apr"; break;
                case 5:  $month = "May"; break;
                case 6:  $month = "Jun"; break;
                case 7:  $month = "Jul"; break;
                case 8:  $month = "Aug"; break;
                case 9:  $month = "Sep"; break;
                case 10: $month = "Oct"; break;
                case 11: $month = "Nov"; break;
                case 12: $month = "Dec"; break;
           }
        }else{
            switch($number) {
                case 1:  $month = "January";   break;
                case 2:  $month = "February"; break;
                case 3:  $month = "March";     break;
                case 4:  $month = "April";     break;
                case 5:  $month = "May";      break;
                case 6:  $month = "June";     break;
                case 7:  $month = "July";     break;
                case 8:  $month = "August";    break;
                case 9:  $month = "September";  break;
                case 10: $month = "October";   break;
                case 11: $month = "November";  break;
                case 12: $month = "December";  break;
           }
        }
        
       return $month;
    }

    public static function system_ChangeCalendarColor ($class = false) {
        if (!$class ) return false;
        if($class == "listing_booked_booked") $class_return = "listing_booked_booked_1";
        if($class == "listing_booked_booked_1") $class_return = "listing_booked_booked";
        if($class == "listing_booked_available_am") $class_return = "listing_booked_available_am_1";
        if($class == "listing_booked_available_am_1") $class_return = "listing_booked_available_am";
        if($class == "listing_booked_available_pm") $class_return = "listing_booked_available_pm_1";
        if($class == "listing_booked_available_pm_1") $class_return = "listing_booked_available_pm";
        return $class_return;

        
    }
 

    public static function import_formatDate($date) {

        if (strpos($date, "/")) {

            $aux = explode("/", $date);

            if (count($aux) == 3) {

                if (config('params.DEFAULT_DATE_FORMAT') == "m/d/Y") {
                    $month = $aux[0];
                    $day = $aux[1];
                    $year = $aux[2];
                } elseif (config('params.DEFAULT_DATE_FORMAT') == "d/m/Y") {
                    $month = $aux[1];
                    $day = $aux[0];
                    $year = $aux[2];
                }

                if (checkdate((int)$month, (int)$day, (int)$year)) {
                    $date_formated = $year."-".$month."-".$day;
                } else {
                    $date_formated = "0000-00-00";
                }

            } else {
                $date_formated = "0000-00-00";
            }

        } else if (strpos($date, "-")) {

            $aux = explode("-", $date);

            if (count($aux) == 3) {

                if (checkdate((int)$aux[1], (int)$aux[2], (int)$aux[0])) {
                    $date_formated = $date;
                } else {
                    $date_formated = "0000-00-00";
                }

            } else {
                $date_formated = "0000-00-00";
            }

        } else {
            $date_formated = "0000-00-00";
        }

        return $date_formated;
    }
      //session function
     public static function sess_getAccountIdFromSession() {
        return Session::get('SESS_ACCOUNT_ID');
    }
    

  
     //Payment Functions
    public static function payment_getRenewalPeriod($item) {

        return constant(string_strtoupper($item)."_RENEWAL_PERIOD");

    }

    public static function payment_getRenewalCycle($item) {

        return Functions::string_substr(config("params.".Functions::string_strtoupper($item)."_RENEWAL_PERIOD"), 0, Functions::string_strlen(config("params.".Functions::string_strtoupper($item)."_RENEWAL_PERIOD"))-1);
    }

    public static function payment_getRenewalUnit($item) {

        return Functions::string_substr(config("params.".Functions::string_strtoupper($item)."_RENEWAL_PERIOD"), Functions::string_strlen(config("params.".Functions::string_strtoupper($item)."_RENEWAL_PERIOD"))-1);
    }

    public static function payment_getRenewalUnitName($item) {

        $unit = Functions::payment_getRenewalUnit($item);
        if ($unit == "Y") $unitname = 'year';
        elseif ($unit == "M") $unitname = 'month';
        elseif ($unit == "D") $unitname = 'day';
        return $unitname;
    }

     public static function payment_calculateTax ($price, $tax, $formatValue = true, $amount = true) {
        if ($amount) {
            $value = ($price * (1 + $tax / 100));
            if ($formatValue) return Functions::format_money($value);
            else return $value;
        } else {
            $value = (($price * (1 + $tax / 100)) - $price);
            if ($formatValue) return Functions::format_money($value);
            else return $value;
        }
    }

      //String Functions
    public static function string_substr($string, $start, $length = false, $charset = 'UTF-8') {
        if ((isset($string) || is_numeric($string)) && is_numeric($start)) {
            if (function_exists('mb_substr')) {
                if (!$length) $length = Functions::string_strlen($string);
                $subStr = mb_substr($string, $start, $length, $charset);
            } else {
                if ($length) $subStr = substr($string, $start, $length);
                else $subStr = substr($string, $start);
            }
        } else {
            $subStr = false;
        }
        return $subStr;
    }

    public static function string_strtoupper($str, $charset = 'UTF-8') {
        if (isset($str) || !is_numeric($str)) {
            if (function_exists('mb_strtoupper')) {
                $strUpper = mb_strtoupper($str, $charset);
            } else {
                $strUpper = strtoupper($str);
            }
        } else {
            $strUpper = false;
        }
        return $strUpper;
    }

       public static function string_strlen($str, $charset = 'UTF-8') {
        if (isset($str) || is_numeric($str)) {
            if (function_exists('mb_strlen') && !is_array($str)) {
                $strLen = mb_strlen($str, $charset);
            } else {
                $strLen = strlen($str);
            }
        } else {
            $strLen = false;
        }
        return $strLen;
    }

   public static  function string_htmlentities($string, $flags = ENT_COMPAT, $charset = 'UTF-8', $double_encode = true) {
        if (isset($string) || is_numeric($string)) {
            $htmlEntities = htmlentities($string, $flags, $charset);
        } else {
            $htmlEntities = false;
        }
        return $htmlEntities;
    }



   public static function format_getTimeString($value){
        $str_time = "";
        $start_time_am_pm="";

        $startTimeStr = explode(":", $value);
        $startTimeStr[0] = Functions::string_substr($startTimeStr[0],-2);
        if (config('params.CLOCK_TYPE') == '24') {
            $start_time_hour = $startTimeStr[0];
        } elseif (config('CLOCK_TYPE') == '12') {
            if ($startTimeStr[0] > "12") {
                $start_time_hour = $startTimeStr[0] - 12;
                $start_time_am_pm = "pm";
            } elseif ($startTimeStr[0] == "12") {
                $start_time_hour = 12;
                $start_time_am_pm = "pm";
            } elseif ($startTimeStr[0] == "00") {
                $start_time_hour = 12;
                $start_time_am_pm = "am";
            } else {
                $start_time_hour = $startTimeStr[0];
                $start_time_am_pm = "am";
            }
        }
        if ($start_time_hour < 10) $start_time_hour = "0".($start_time_hour+0);
        $start_time_min = $startTimeStr[0]; // edit from $startTimeStr[1]
        $str_time .= $start_time_hour.":".$start_time_min." ".$start_time_am_pm;

        return $str_time;
    }

    Public static function string_strtolower($str, $charset = 'UTF-8') {
        if (isset($str) || is_numeric($str)) {
            if (function_exists('mb_strtolower')) {
                $strLower = mb_strtolower($str, $charset);
            } else {
                $strLower = strtolower($str);
            }
        } else {
            $strLower = false;
        }
        return $strLower;
    }
    //Status Functions
    public static function check_status($getstatus="")
        {
              $name = Array(config('params.LANG_LABEL_ACTIVE'), config('params.LANG_LABEL_SUSPENDED'),config('params.LANG_LABEL_EXPIRED'), config('params.LANG_LABEL_PENDING'));
               $style = Array("status-active", "status-suspended", "status-expired", "status-pending");

                     $status  = Array("A", "S", "E", "P");
                       $return='';
                  for ($i=0; $i<count($status); $i++) {
                    
                   if($status[$i]==$getstatus)
                    {
                       $return= '<span class="'.$style[$i].'">'.$name[$i].'</span>';
                         break;
                    }
                    //else
                    //{
                    //$return= '<span class="status-pending">Pending</span>';
                    //break;

                    //}
                    

                }

                return $return;
      }

   

    public static function system_showText($text) {
        return $text;
    }   

     //User Account Validation
    public static function validate_memberCurrentPassword($array='', $account_id, &$error) {
        //extract($array);
        $error = "";
     

        $sql = "SELECT * FROM Account WHERE id = ".$account_id." AND 
        password = '".(Functions::string_strtolower(config('params.PASSWORD_ENCRYPTION') == "on") ? md5($array) : $array)."'";

        $user = Functions::fetchMain($sql);
        if (count($user)) {
            return true;

        } else {
            $error = "&#149;&nbsp; Current Password is incorrect.";
            return $error;
            return false;
        }
    } 

      public static function validate_MEMBERS_account($isforeignAcc,$username,$password,$retype_password,&$error, $currentUserID = 0) {
        
        //extract($array);
        
        $error = "";
        
        if ($isforeignAcc != "y" && (Functions::string_strpos($_SERVER["PHP_SELF"], "/resetpassword.php") === false)) {
            $usernameError = Functions::validate_username($username);
            if ($usernameError) $errors[] = $usernameError;

            //$account_exists = db_getFromDB('account', 'username', db_formatString($username));
            $sql = "SELECT * FROM Account WHERE username = '$username'";
            $account_exists = self::fetchMain($sql);
            if ($currentUserID && $account_exists[0]->id && $currentUserID != $account_exists[0]->id){
                $errors[] = "&#149;&nbsp;Please choose a different e-mail.";
            }

            if (isset($errors)) {
                $error .= implode("<br />", $errors);
            }
        }
        
        if ((Functions::string_strlen($password)) || (Functions::string_strlen($retype_password))) {
            $error .= Functions::validate_password($password, $retype_password, true);
        }
        
        if ($error) {
            return  $error;
            return false;
        }
        return true;
    }

     public static function validate_password($password, $retype_password="", $required=false) {

        $error = "";

        if (preg_match("/(\s)/", $password)) {

            $error = "&#149;&nbsp;Blank space is not allowed for password.";

        } elseif (Functions::string_strlen($password) > config('params.PASSWORD_MAX_LEN')) {

             $error = "&#149;&nbsp;Please enter a password with a maximum of ".config('params.PASSWORD_MAX_LEN')." characters";

        } elseif (Functions::string_strlen($password) < config('params.PASSWORD_MIN_LEN')) {

            $error = "&#149;&nbsp;Please enter a password with a minimum of ".config('params.PASSWORD_MIN_LEN')." characters";

        } elseif ($password == "abc123") {

            $error = "&#149;&nbsp;Password abc123 not allowed!";

        } elseif ($retype_password) {

            if ($password != $retype_password) {

  $error = "&#149;&nbsp;Passwords do not match. Please enter the same content for \"password\" and \"retype password\" fields.";
            }

        } elseif ($required) {

  $error = "&#149;&nbsp;Passwords do not match. Please enter the same content for \"password\" and \"retype password\" fields.";
        }

        return $error;

    }

     public static function validate_username($username) {

        $error = "";

        if (!$username) {

            $error = "&#149;&nbsp; E-mail is required.";

        } elseif ($username) {


            if (!Functions::validate_email($username)) {
                $error = "&#149;&nbsp;Please enter a valid e-mail.";
            }

            if (preg_match("/(\s)/", $username)) {

                $error = "&#149;&nbsp;Spaces are not allowed for e-mail.";

            } elseif (preg_match('/[^0-9a-zA-Z\@\.\_\-]/i', $username)) {

                $error = "&#149;&nbsp;Special characters are not allowed for e-mail.";

            } elseif (Functions::string_strlen($username) > 80) {

                $error = "&#149;&nbsp;Please type an e-mail with a maximum of 80 characters";

            } elseif (Functions::string_strlen($username) < 4) {


                 $error = "&#149;&nbsp;Please type an e-mail with a minimum of 4 characters";

            }

        }

        return $error;

    } 

    //this function is valid for email and username validation
    public static function validate_email($email) {

        //e-mail injection
        if(preg_match('/^\r/',$email) || preg_match('/^\n/',$email)){
            return false;
        }

        //$regex = '/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/';

        //fix to accept final with .info, for example
        $regex = '/^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$/';
        if(preg_match($regex, $email) > 0) {
            return true;
        } else {
            return false;
        }
    }

    //System Functions


    /*
     * Function to get information about language
     */
    public static function setting_getSettingInformation($index){

        if(!defined('SETTING_INFORMATION')){
            setting_constants();
        }

        $aux_setting_information = unserialize(SETTING_INFORMATION);
        $array_setting_information = $aux_setting_information[$index];

        if(is_array($array_setting_information)){
            return $array_setting_information;
        }else{
            return false;
        }

    }
    public static function setting_new($name, $value) {
        if ($name) {
            $settingObj = new Setting();
            $settingObj->Setting($name);
            if (!$settingObj->getString("name")) {
                $settingObj->setString("name", $name);
                $settingObj->setString("value", $value);
                $settingObj->Save($update = false);
                return true;
            }
        }
        return false;
    }
    public static function setting_get($name, &$value) {
        if ($name) {

            /*unset($array_settings);
            $array_settings = setting_getSettingInformation($name);
            if((is_array($array_settings)) && ($_SERVER['REQUEST_METHOD']!="POST")){
                $value = $array_settings["value"];
                return true;
            }
            else{*/
                $settingObj = new Setting();
                $settingObj->Setting($name);
                if ($settingObj->getString("name")) {
                    $value = $settingObj->getString("value");
                    return true;
               // }
            }
            
        }
        $value = "";
        return false;
    }

    public static function setting_set($name, $value) {
        if ($name) {
            $settingObj = new Setting();
            $settingObj->Setting($name);
            if ($settingObj->getString("name")) {
                $settingObj->setString("value", $value);
                $settingObj->Save();
                //setting_constants();
                return true;
            }
        }
        return false;
    }

     public static function system_generatePassword($numeric = false) {
        if ($numeric) {
            $string = "1234567890";
            $len = 5;
        } else {
            $string = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            $len = 8;
        }
        $pass='';
        srand((double)microtime()*1000000);
        for ($i=0; $i < $len; $i++) {
            $num   = rand() % Functions::string_strlen($string);
            $tmp   = Functions::string_substr($string, $num, 1);
            $pass  .= $tmp;
        }
        return $pass;
    }
   

   public static function is_valid_discount_code($discount_id, $item_type, $item_id, &$message, &$error_num) {

        if (!$discount_id && $discount_id != "0") return true;
        
        $item_type_name = ucwords($item_type);
        $itemObj = new Listing();
        $itemObj->$item_type_name($item_id);

        $discountCodeObj = new DiscountCode();
        $discountCodeObj->DiscountCode($discount_id);       
        $auxCode = $discountCodeObj->id;

        if ((strlen($auxCode) <= 0)) {

            $error_num = 1;
            $message .= "&#149;&nbsp;Inexistent ".config('params.LANG_LABEL_DISCOUNTCODE')." \"<b>".$discount_id."</b>\".";
            return false;

        } else {

            if ($discountCodeObj->status != "A") {

                $error_num = 2;
                $message .= "&#149;&nbsp;".config('params.LANG_LABEL_DISCOUNTCODE')." \"<b>".$discount_id."</b>\" ".config('params.LANG_MSG_IS_NOT_AVAILABLE');
                return false;

            } elseif ($discountCodeObj->$item_type != "on") {

                $error_num = 3;
                $message .= "&#149;&nbsp;".config('params.LANG_LABEL_DISCOUNTCODE')." \"<b>".$discount_id."</b>is not available for this item type.";
                return false;

            } elseif (($discountCodeObj->recurring != "yes") && ($itemObj->id > 0)) {

                //$dbObj_main = db_getDBObject(DEFAULT_DB, true);
                //$dbObj = db_getDBObjectByDomainID(SELECTED_DOMAIN_ID, $dbObj_main);

                $sql = "SELECT * FROM Payment_".$item_type_name."_Log WHERE discount_id = '".$discountCodeObj->id."' AND ".$item_type."_id = '".$itemObj->id."' ORDER BY renewal_date DESC LIMIT 1";
                $result = Sql::fetch($sql);

                if (count($result) >= 1) {

                    $error_num = 4;
                    $message .= "&#149;&nbsp;".config('params.LANG_LABEL_DISCOUNTCODE')." <strong>".$discount_id."</strong> ".config('params.LANG_MSG_CANNOT_BE_USED_TWICE');
                    return false;

                }

                $sql = "SELECT * FROM Invoice_".$item_type_name." WHERE discount_id = '".$discountCodeObj->id."' AND ".$item_type."_id = '".$itemObj->id."' ORDER BY renewal_date DESC LIMIT 1";
                $result = Sql::fetch($sql);

                if (count($result) >= 1) {

                    $error_num = 4;
                    $message .= "&#149;&nbsp;".config('params.LANG_LABEL_DISCOUNTCODE')." <strong>".$discount_id."</strong> ".config('params.LANG_MSG_CANNOT_BE_USED_TWICE');
                    return false;

                }

            }

        }

        return true;

    }

    public static function validate_form($form, $array=array(), &$error,$orderListing = false)
        {
            extract($array);
            $errors=array();
            

            $ctr_url = (isset($array["display_url"])) ? true : false;

           if ($form == "contact") {
            if (!$first_name)                       $errors[] = "&#149;&nbsp;".config('params.LANG_MSG_FIRST_NAME_IS_REQUIRED');
            if (!$last_name)                        $errors[] = "&#149;&nbsp;".config('params.LANG_MSG_LAST_NAME_IS_REQUIRED');
            if (!$username)// && $isforeignAcc == "y"
                $errors[] = "&#149;&nbsp;".config('params.LANG_MSG_EMAIL_IS_REQUIRED');
            if (!$address)                          $errors[] = "&#149;&nbsp;".config('params.LANG_MSG_ADDRESS1_IS_REQUIRED');
            if (!$city)                             $errors[] = "&#149;&nbsp;".config('params.LANG_MSG_CITY_IS_REQUIRED');
            if (!$state)                            $errors[] = "&#149;&nbsp;".config('params.LANG_MSG_STATE_IS_REQUIRED');
            if (!$zip)                              $errors[] = "&#149;&nbsp;".config('params.LANG_MSG_ZIPCODE_IS_REQUIRED');
            if (!$phone)                            $errors[] = "&#149;&nbsp;".config('params.LANG_MSG_PHONE_IS_REQUIRED');
        }


            
            //if (isset($array["listingtemplate_id"]) && config('params.CUSTOM_LISTINGTEMPLATE_FEATURE') == "on") {
//                
//                if (USING_THEME_TEMPLATE && THEME_TEMPLATE_ID && THEME_TEMPLATE_ID == $_POST["listingtemplate_id"]){
//                    $isThemeTemplate = true;
//                } else {
//                    $isThemeTemplate = false;
//                }
//                
//              $listingTemplateObj = new ListingTemplate($_POST["listingtemplate_id"]);
//              $templateFields = $listingTemplateObj->getListingTemplateFields("", $isThemeTemplate);
//
//              if ($templateFields) {
//                  foreach ($templateFields as $each_field) {
//                      if ($each_field["required"]=="y") {
//                          $elems[] = array('name'=>$each_field["field"], 'label'=>(defined($each_field["label"]) ? constant($each_field["label"]) :$each_field["label"]), 'type'=>'text', 'required'=>true, 'len_max'=>'65535');
//                      }
//                  }
//              }
//          }
            
 if ($form == "listing") {

            $elems[] = array('name'=>'title',             'label'=>config('params.LANG_LABEL_NAME_OR_TITLE'),       'type'=>'text',   'required'=>true,     'len_max'=>'100'                   );
            $elems[] = array('name'=>'friendly_url',      'label'=>config('params.LANG_LABEL_PAGE_NAME'),           'type'=>'text',   'required'=>false,    'len_max'=>'255'                   );
            $elems[] = array('name'=>'description',       'label'=>config('params.LANG_LABEL_SUMMARY_DESCRIPTION'), 'type'=>'text',   'required'=>false,    'len_max'=>'255'                   );
            $elems[] = array('name'=>'renewal_date',      'label'=>config('params.LANG_LABEL_RENEWAL_DATE'),        'type'=>'text',   'required'=>false,    'len_max'=>'10',    'cont'=>'date' );
            //$elems[] = array('name'=>'url',               'label'=>config('params.LANG_LABEL_WEB_ADDRESS'),         'type'=>'text',   'required'=>$crt_url, 'len_max'=>'255'                   );
            $elems[] = array('name'=>'fax',               'label'=>config('params.LANG_LABEL_FAX'),                 'type'=>'text',   'required'=>false,    'len_max'=>'255'                   );
            $elems[] = array('name'=>'long_description',  'label'=>config('params.LANG_LABEL_LONG_DESCRIPTION'),    'type'=>'text',   'required'=>false,    'len_max'=>'65535'                 );
            $elems[] = array('name'=>'status',            'label'=>config('params.LANG_LABEL_STATUS'),              'type'=>'text',   'required'=>false,    'len_max'=>'1'                     );
            $elems[] = array('name'=>'level',             'label'=>config('params.LANG_LABEL_LEVEL'),               'type'=>'select', 'required'=>false,    'len_max'=>'2',    'cont'=>'digit' );
            $elems[] = array('name'=>'return_categories', 'label'=>config('params.LANG_LABEL_CATEGORY_PLURAL'),     'type'=>'text',   'required'=>true,                                        );
            
            if ( isset($orderListing) ) {
                $elems[] = array('name'=>'email',             'label'=>config('params.LANG_LABEL_EMAIL'),               'type'=>'text',   'required'=>false,        'len_max'=>'255'                     );
                $elems[] = array('name'=>'phone',             'label'=>config('params.LANG_LABEL_PHONE'),               'type'=>'text',   'required'=>false,        'len_max'=>'255'                   );
                $elems[] = array('name'=>'address',           'label'=>config('params.LANG_LABEL_ADDRESS1'),            'type'=>'text',   'required'=>false,        'len_max'=>'255'                   );
                $elems[] = array('name'=>'zip_code',          'label'=>config('params.ZIPCODE_LABEL'),  'type'=>'text',   'required'=>false,        'len_max'=>'255'                     );
                $elems[] = array('name'=>'location_1',        'label'=>config('params.LANG_LABEL_COUNTRY'),             'type'=>'select',   'required'=>false,      'len_max'=>'255'                     );
                $elems[] = array('name'=>'location_3',        'label'=>config('params.LANG_LABEL_STATE'),               'type'=>'select',   'required'=>false,      'len_max'=>'255'                     );
                $elems[] = array('name'=>'location_4',        'label'=>config('params.LANG_LABEL_CITY'),                'type'=>'select',   'required'=>false,      'len_max'=>'255'                     ); 
            } else {
                $elems[] = array('name'=>'account_id', 'label'=>config('params.LANG_LABEL_ACCOUNT'), 'type'=>'text', 'required'=>true, 'cont'=>'digit' );
                $elems[] = array('name'=>'email',             'label'=>config('params.LANG_LABEL_EMAIL'),               'type'=>'text',   'required'=>true,     'len_max'=>'255'                     );
                $elems[] = array('name'=>'phone',             'label'=>config('params.LANG_LABEL_PHONE'),               'type'=>'text',   'required'=>true,     'len_max'=>'255'                   );
                $elems[] = array('name'=>'address',           'label'=>config('params.LANG_LABEL_ADDRESS1'),            'type'=>'text',   'required'=>true,     'len_max'=>'255'                   );
                $elems[] = array('name'=>'zip_code',          'label'=>config('params.ZIPCODE_LABEL'),  'type'=>'text',   'required'=>true,     'len_max'=>'255'                     );
                #57: Label Changes on listing form
                $elems[] = array('name'=>'contact_fname',     'label'=>config('params.LANG_LABEL_FIRST_NAME'),          'type'=>'text',   'required'=>true,     'len_max'=>'255'                     );
                $elems[] = array('name'=>'contact_lname',     'label'=>config('params.LANG_LABEL_LAST_NAME'),           'type'=>'text',   'required'=>true,     'len_max'=>'255'                     );
                $elems[] = array('name'=>'location_1',        'label'=>config('params.LANG_LABEL_COUNTRY'),             'type'=>'select', 'required'=>true,     'len_max'=>'255'                     );
                if(isset($array['new_location3_field']) && $array['new_location3_field'] == "" && isset($array['new_location3_friendly']) && $array['new_location3_friendly'] == "") {
                    $elems[] = array('name'=>'location_3',        'label'=>config('params.LANG_LABEL_STATE'),    'type'=>'select',   'required'=>true,      'len_max'=>'255'                     );
                }
                if(isset($array['new_location4_field']) && $array['new_location4_field'] == "" && isset($array['new_location4_friendly']) && $array['new_location4_friendly'] == "") {
                    $elems[] = array('name'=>'location_4',        'label'=>config('params.LANG_LABEL_CITY'),     'type'=>'select',   'required'=>true,      'len_max'=>'255'                     );
                }
            }
            $f = new FormValidator;
            $f->FormValidator($elems);
            $err = $f->validate($_POST);


            if ($err) {
                $errors[] = "<b>".config('params.LANG_MSG_FIELDS_CONTAIN_ERRORS')."</b>";
                $valid = $f->getValidElems();
                foreach ($valid as $field_title => $field_array) {
                    foreach ($field_array as $field) if (!$field['validation']) $errors[] = "&#149;&nbsp;".$field['label'];
                }
            }


            $return_categories_array = isset($return_categories)?explode(",", $return_categories):array();
            $return_categories_array = array_unique($return_categories_array);

            if(count($return_categories_array) > config('params.LISTING_MAX_CATEGORY_ALLOWED')) $errors[] = "&#149;&nbsp;".config('params.LANG_MSG_MAX_OF_CATEGORIES_1')." ".config('params.LISTING_MAX_CATEGORY_ALLOWED')." ".config('params.LANG_MSG_MAX_OF_CATEGORIES_2');


            /*if (isset($friendly_url)) {
                $sql = "SELECT friendly_url FROM Listing WHERE friendly_url = ".$friendly_url."";
                if(isset($id)) $sql .= " AND id != $id ";
                $sql .= " LIMIT 1";
                $sqlFriendlyURLResult = Sql::getFriendlyUrl($sql);
                $rs = isset($sqlFriendlyURLResult[0]->friendly_url)?$sqlFriendlyURLResult[0]->friendly_url:0;
                if($rs> 0) $errors[] = "&#149;&nbsp;".config('params.LANG_MSG_FRIENDLY_URL_IN_USE');
                if(!preg_match(config('params.FRIENDLYURL_REGULAREXPRESSION'), $friendly_url)) $errors[] = "&#149;&nbsp;".config('params.LANG_MSG_PAGE_NAME_INVALID_CHARS');
            }*/
            
            if (isset($latitude)){
                if (!is_numeric($latitude) || $latitude < -90 || $latitude > 90){
                    $errors[] = "&#149;&nbsp;".config('params.LANG_LABEL_MAP_INVALID_LAT');
                }
            }
            
            if (isset($longitude)){
               if (!is_numeric($latitude) || $latitude < -90 || $latitude > 90){
                    $errors[] = "&#149;&nbsp;".config('params.LANG_LABEL_MAP_INVALID_LON');
                } 
            }

            if (isset($array_keywords)) {
                if (count($array_keywords) > config('params.MAX_KEYWORDS')) {
                    $errors[] = "&#149;&nbsp;".config('params.LANG_MSG_MAX_OF_KEYWORDS_ALLOWED_1')." ".config('params.MAX_KEYWORDS')." ".config('params.LANG_MSG_MAX_OF_KEYWORDS_ALLOWED_2');
                }
                $kwlarge = false;
                foreach ($array_keywords as $kw) {
                    if (strlen($kw) > 50) {
                        $kwlarge = true;
                    }
                }
                if (isset($kwlarge) && $kwlarge) {
                    $errors[] = "&#149;&nbsp;".config('params.LANG_MSG_PLEASE_INCLUDE_KEYWORDS');
                }
            }

        }
          $error='';
      if ($errors) {
            $error .= implode("<br />", $errors);
            return false;
        }

        return true;

            
          
        }
        
 
       public static function validate_addAccount($array, &$error) {

        extract($array);

        $error = "";

        $usernameError = Functions::validate_username($username);
        if ($usernameError) $errors[] = $usernameError;

        $sql="SELECT * FROM Account WHERE username='".$username."'";
        $account_exists = Sql::fetchMain($sql);
        if (count($account_exists)>0){
            $errors[] = "&#149;&nbsp;".config('params.LANG_MSG_CHOOSE_DIFFERENT_USERNAME');
        }

        if ($password == '0') {
            $errors[] = "&#149;&nbsp;".config('params.LANG_MSG_ENTER_PASSWORD_WITH_MIN_CHARS')." ".config('params.PASSWORD_MAX_LEN')." Characters";
        } else if (!$password) {
            $errors[] = "&#149;&nbsp;".config('params.LANG_MSG_PASSWORD_IS_REQUIRED');
        } else {
            $pass_error = Functions::validate_password($password, $password, true);
            if($pass_error) $errors[] = $pass_error;
        }

        if (!$agree_tou) $errors[] = "&#149;&nbsp;".config('params.LANG_MSG_IGREETERMS_IS_REQUIRED');

        
        if (isset($errors)) {
            $error .= implode("<br />", $errors);
            return false;
        }

        return true;

    }
    /**
    * Verify if email is Enabled or Disabled
    ********************************************************************/
    public static function system_checkEmail($id) {
        $email = new EmailNotification();
        $email->EmailNotification($id);
        if ($email->getString("deactivate")) {
            return false;
        } else {
            return $email;
        }
    }
   //Replace the variables in the email body
   public static function system_replaceEmailVariables($body, $id, $item="listing", $redeem_code = "", $userName = "") {

        switch ($item) {
            /*case 'banner': $obj = new Banner($id); break;
            case 'classified': $obj = new Classified($id); break;
            case 'article': $obj = new Article($id); break;
            case 'event': $obj = new Event($id); break;*/
            case 'listing': $obj = new Listing(); $obj->Listing($id); break;
           /* case 'promotion': $obj = new Promotion($id); break;
           case 'post': $obj = new Post($id);*/
            case 'account': $acc = new Account(); $acc->Account($id);
        }

        if (!isset($acc)){
        $acc = new Account();
        $acc->Account($obj->getNumber('account_id'));

        $acc_cont = new Contact();
        $acc_cont->Contact($acc->getNumber('id'));

        } 

        Functions::setting_get("sitemgr_email", $sitemgr_email);
       //$sitemgr_emails = explode(",", $sitemgr_email);

        //if ($sitemgr_emails[0]) $sitemgr_email = $sitemgr_emails[0];

        $body = str_replace("ACCOUNT_NAME", ($userName ? $userName : $acc_cont->getString('first_name').' '.$acc_cont->getString('last_name')),$body);
        $body = str_replace("ACCOUNT_USERNAME",$acc->getString('username'),$body);
        $body = str_replace("ACCOUNT_PASSWORD",$acc->getString('username'),$body);

        /*switch ($item) {
            case 'banner':
                $body = str_replace(array("ITEM_TITLE", "BANNER_TITLE"), $obj->getString('caption'), $body);
            break;
            case 'classified':
                $levelObj = new ClassifiedLevel();
                if ($levelObj->getDetail($obj->getString('level')) == "y") {
                    $detailLink = "".CLASSIFIED_config('params.DEFAULT_URL')."/".$obj->getString("friendly_url").".html"; 
                } else {
                    $detailLink = CLASSIFIED_config('params.DEFAULT_URL')."/results.php?id=".$obj->getString("id");
                }
                $body = str_replace(array("ITEM_TITLE", "CLASSIFIED_TITLE"), $obj->getString('title'), $body);
            break;
            case 'article':
                $detailLink = "".ARTICLE_config('params.DEFAULT_URL')."/".$obj->getString("friendly_url").".html";
                $body = str_replace(array("ITEM_TITLE", "ARTICLE_TITLE"), $obj->getString('title'), $body);
            break;
            case 'event':
                $levelObj = new EventLevel();
                if ($levelObj->getDetail($obj->getString('level')) == "y") {
                    $detailLink = "".EVENT_config('params.DEFAULT_URL')."/".$obj->getString("friendly_url").".html";
                } else {
                    $detailLink = EVENT_config('params.DEFAULT_URL')."/results.php?id=".$obj->getString("id");
                }
                $body = str_replace(array("ITEM_TITLE", "EVENT_TITLE"), $obj->getString('title'), $body);
            break;
            case 'listing':
                $levelObj = new ListingLevel();
                if ($levelObj->getDetail($obj->getString('level')) == "y") {
                    $detailLink = "".LISTING_config('params.DEFAULT_URL')."/".$obj->getString("friendly_url").".html";
                } else {
                    $detailLink = LISTING_config('params.DEFAULT_URL')."/results.php?id=".$obj->getString("id");
                }
                $body = str_replace(array("ITEM_TITLE", "LISTING_TITLE"), $obj->getString('title'), $body);
            break;
            case 'promotion':
                $detailLink = "".PROMOTION_config('params.DEFAULT_URL')."/results.php?id=".$obj->getNumber("id");
                $body = str_replace(array("ITEM_TITLE"), $obj->getString('name'), $body);
            break;
            case 'post':
                $detailLink = "".BLOG_config('params.DEFAULT_URL')."/".$obj->getString("friendly_url").".html";
                $body = str_replace(array("ITEM_TITLE", "BLOG_TITLE"), $obj->getString('title'), $body);
            break;
        }*/

        //if (isset($detailLink)) $body = str_replace("ITEM_URL", $detailLink, $body);

        //$body = str_replace("ITEM_TYPE", $item, $body);
        
        //$body = str_replace("REDEEM_CODE", $redeem_code, $body);

        //$body = str_replace("ARTICLE_config('params.DEFAULT_URL')",ARTICLE_config('params.DEFAULT_URL'),$body);
        //$body = str_replace("CLASSIFIED_config('params.DEFAULT_URL')",CLASSIFIED_config('params.DEFAULT_URL'),$body);
        //$body = str_replace("EVENT_config('params.DEFAULT_URL')",EVENT_config('params.DEFAULT_URL'),$body);
        //$body = str_replace("LISTING_config('params.DEFAULT_URL')",LISTING_config('params.DEFAULT_URL'),$body);

        $body = str_replace(array("ITEM_TITLE", "LISTING_TITLE"), $obj->getString('title'), $body);
        $body = str_replace("EDIRECTORY_TITLE",config('params.EDIRECTORY_TITLE'),$body);
        $body = str_replace("SITEMGR_EMAIL",$sitemgr_email,$body);
        $body = str_replace("config('params.DEFAULT_URL')",url(),$body);//config('params.config('params.DEFAULT_URL')')
        $body = str_replace("MEMBERS_URL",config('params.MEMBERS_ALIAS'),$body);

        return $body;

    }

    public static function system_showAccountUserName($username) {
        if (($pos = Functions::string_strpos($username, "::")) !== false) {
            $username = Functions::string_substr($username, $pos+2);
        }
        return $username;
    }


    public static function log_addCronRecord($link, $type, $message, $update = false, &$log_id, $finished = false, $time = "") {
        if (config('params.ENABLE_CRON_LOG')) {
            if ($update) {
                $sql = "UPDATE Cron_Log SET history = CONCAT(history, '\n$message') ".($finished ? ", finished = 'y', time = '$time'" : "")." WHERE id = ".($log_id ? $log_id : 0)."";
                Sql::updateSqlMain($sql);
            } else {
       $sql = "INSERT INTO Cron_Log (domain_id, cron, date, history) VALUES (1, '$type', NOW(), '$message')";
                //;mysql_query($sql, $link);
                Sql::insertSqlMain($sql);
                $log_id = DB::getpdo()->lastInsertId();
            }
        }
    }

   
     public static function report_PrepareListingStatsReviewToEmail($reports, $item_id, $item_type, $default_url = 'ss'){
        
        unset($body_email);
        
        if (is_array($reports)) {
            
            $addHeader = true;
            $closeHeader = false;
                    
            foreach ($reports as $key => $report) {

                list($year, $month) = explode('-', $key);

                if ($month == (date("m")-1)) {

                    if ($addHeader) {
                        $body_email = "
                            <table cellpadding=\"2\" cellspacing=\"2\" border=\"0\">
                                <tr>
                                    <td width=\"125\">
                                        <b>Date</b>
                                    </td>
                                    <td width=\"90\">
                                        <b>Summary</b>
                                    </td>
                                    <td width=\"90\">
                                        <b>Detail</b>
                                    </td>
                                    <td width=\"90\">
                                        <b>ClickThru</b>
                                    </td>
                                    <td width=\"90\">
                                        <b>E-mail</b>
                                    </td>
                                    <td width=\"90\">
                                        <b>Phone</b>
                                    </td>
                                    <td width=\"50\">
                                        <b>Fax</b>
                                    </td>";
                                    /*if (TWILIO_APP_ENABLED == "on"){
                                        if (TWILIO_APP_ENABLED_SMS == "on"){
                                            $body_email .= "
                                                <td width=\"115\">
                                                    <b>".system_showText(LANG_LABEL_SENDPHONE)."</b>
                                                </td>";
                                        }
                                        if (TWILIO_APP_ENABLED_CALL == "on"){
                                            $body_email .= "    
                                                <td width=\"118\">
                                                    <b>".system_showText(LANG_LABEL_CLICKTOCALL)."</b>
                                                </td>";
                                        }
                                    } */
                                    $body_email .= "
                                </tr>";

                        $addHeader = false;
                        $closeHeader = true;
                    }
                    ///*system_showDate('F', mktime(0, 0, 0, $month, 1, $year))*/
                    $body_email .= "<tr>
                        <td>March / ".$year."</td>
                        <td>".(($report['summary']) ? $report['summary'] : 0)."</td>
                        <td>".(($report['detail']) ? $report['detail'] : 0)."</td>
                        <td>".(($report['click']) ? $report['click'] : 0)."</td>
                        <td>".(($report['email']) ? $report['email'] : 0)."</td>
                        <td>".(($report['phone']) ? $report['phone'] : 0)."</td>
                        <td>".(($report['fax']) ? $report['fax'] : 0)."</td>";
                        /*if (TWILIO_APP_ENABLED == "on") {
                            if (TWILIO_APP_ENABLED_SMS == "on"){
                                $body_email .= "
                                <td>".(($report['sms']) ? $report['sms'] : 0)."</td>";
                            }
                            if (TWILIO_APP_ENABLED_CALL == "on"){
                                $body_email .= "
                                <td>".(($report['click_call']) ? $report['click_call'] : 0)."</td>";
                            }
                        }*/
                    $body_email .= "    
                    </tr>";
                }
            }
            
            if ($closeHeader) {
                $body_email .= "</table>";
            }
        }
            
        if ($item_id) {

            /*
            * Prepare Reviews / Working just for Listing
            */
            unset($array_general_review);
            $array_general_review = array();

            $listingObj = new Listing();
            $listingObj->Listing($item_id);
            $rate_avg = htmlspecialchars($listingObj->getString("avg_review"));
            $rate_avg = (isset($rate_avg) && $rate_avg != 0) ? round($rate_avg, 2) : 'N/A';

           
            $year = date("Y");
            $lastmonth = date("m")-1;
            $startDate = $year."-".$lastmonth."-01 00:00:00";
            $sql = "SELECT * FROM Review WHERE item_type = '$item_type' AND item_id = ".htmlspecialchars($item_id)." AND review IS NOT NULL AND review != '' AND approved=1 AND added >= '$startDate' AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 30 DAY), '%Y%m%d') <= added AND added <= NOW()";

            $r = Sql::fetch($sql);
            $review_amount = count($r);

            unset($rate_stars);
            $rate_stars='';
            for ($x=0 ; $x < 5 ;$x++) {
                if (round($rate_avg) > $x){
                    $rate_stars .= "<img src='".$default_url."/images/img_rateMiniStarOn.png' alt='Star On' />";
                }else{
                    $rate_stars .= "<img src='".$default_url."/images/img_rateMiniStarOff.png' alt='Star Off' />";
                }
            }

            $i=0;
            foreach($r as $row){

                if ($row->rating) {
                    unset($rate_stars);
                    for ($x=0 ; $x < 5 ;$x++) {
                        if ($row->rating > $x){
                            $rate_stars .= "<img src=\"".$default_url."/images/img_rateMiniStarOn.png\" alt=\"Star On\" align=\"bottom\" />";
                        }else{
                            $rate_stars .= "<img src=\"".$default_url."/images/img_rateMiniStarOff.png\" alt=\"Star Off\" align=\"bottom\" />";
                        }
                    }
                    $array_general_review[$i]["rate_starts"]    = $rate_stars;
                    $array_general_review[$i]["review_title"]   = $row->review_title;
                    $array_general_review[$i]["reviewer_name"]  = $row->reviewer_name;
                    $array_general_review[$i]["reviewer_email"] = $row->reviewer_email;
                    $array_general_review[$i]["review"]         = $row->review;
                    $array_general_review[$i]["added"]          = $row->added;
                    $i++;
                }

            }



            if(count($array_general_review) > 0){

                $body_email .= "<br /><br /> 
                                <table cellpadding=\"2\" cellspacing=\"2\" border=\"0\">
                                    <tr>
                                        <td>
                                            <b>Reviews and comments from the last month</b>
                                        </td>
                                    </tr>";
                for($i=0;$i<count($array_general_review);$i++){
                    $body_email .= "<tr><td>&nbsp;</td></tr>";
                    $body_email .= "<tr><td nowrap>".$array_general_review[$i]["rate_starts"]."</td></tr>";
                    $body_email .= "<tr><td nowrap>".$array_general_review[$i]["review_title"]."</td></tr>";
                    $body_email .= "<tr><td nowrap>".$array_general_review[$i]["reviewer_name"]." - ".format_date($array_general_review[$i]["added"])."</td></tr>";
                    $body_email .= "<tr><td nowrap>".$array_general_review[$i]["review"]."</td></tr>";

                }
                $body_email .= "</table>";


            }
        } 
        /*else {

            unset($array_general_review);
            $array_general_review = array();

            $rate_avg = 4;
            $review_amount = 1;
            $rating = 4;

            unset($rate_stars);
            for ($x=0 ; $x < 5 ;$x++) {
                if (round($rate_avg) > $x){
                    $rate_stars .= "<img src='".$default_url."/images/img_rateMiniStarOn.png' alt='Star On' />";
                }else{
                    $rate_stars .= "<img src='".$default_url."/images/img_rateMiniStarOff.png' alt='Star Off' />";
                }
            }
            $i = 0;
            while($i < $review_amount){

                unset($rate_stars);
                for ($x=0 ; $x < 5 ;$x++) {
                    if ($rating > $x){
                        $rate_stars .= "<img src=\"".$default_url."/images/img_rateMiniStarOn.png\" alt=\"Star On\" align=\"bottom\" />";
                    }else{
                        $rate_stars .= "<img src=\"".$default_url."/images/img_rateMiniStarOff.png\" alt=\"Star Off\" align=\"bottom\" />";
                    }
                }
                $lastMonth1 = (date("m")-1)."-".date("d")."-".date("Y");
                $lastMonth2 = date("d")."-".(date("m")-1)."-".date("Y");
                $array_general_review[$i]["rate_starts"]    = $rate_stars;
                $array_general_review[$i]["review_title"]   = LANG_LABEL_ADVERTISE_REVIEW_TITLE;
                $array_general_review[$i]["reviewer_name"]  = LANG_LABEL_ADVERTISE_VISITOR;
                $array_general_review[$i]["reviewer_email"] = LANG_LABEL_ADVERTISE_ITEM_EMAIL;
                $array_general_review[$i]["review"]         = "Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum.";
                $array_general_review[$i]["added"]          = str_replace("-", "/", (DEFAULT_DATE_FORMAT == "m/d/Y" ? $lastMonth1 : $lastMonth2));
                $i++;
            }

            if(count($array_general_review) > 0){

                $body_email .= "<table cellpadding=\"2\" cellspacing=\"2\" border=\"0\">
                                    <tr>
                                        <td width=\"125\">
                                            <b>".system_showText(LANG_REVIEWS_MONTH)."</b>
                                        </td>
                                    </tr>";
                for($i=0;$i<count($array_general_review);$i++){

                    $body_email .= "<tr><td nowrap>".$array_general_review[$i]["rate_starts"]."</td></tr>";
                    $body_email .= "<tr><td nowrap>".$array_general_review[$i]["review_title"]."</td></tr>";
                    $body_email .= "<tr><td nowrap>".$array_general_review[$i]["reviewer_name"]." - ".$array_general_review[$i]["added"]."</td></tr>";
                    $body_email .= "<tr><td nowrap>".$array_general_review[$i]["review"]."</td></tr>";

                }
                $body_email .= "</table>";

            }   
        }*/

        return $body_email;
    }
    
    /**
     * Save new to approved record
     * @param integer $domain_id
     * @param integer $item_id
     * @param string $item_type
     * @param string $item_title
     * @param string $review_title
     */
    public static function activity_newToApproved($domain_id = 0, $item_id = 0, $item_type = "", $item_title = "''", $content = "''", $assoc_item = 0, $rate = 0, $reply_id = 0) {
        
        if (Functions::string_strpos($item_type, "review") !== false) { //get reviewer name and content
            $item_id = str_replace("'", "", $item_id);
            $reviewObj = new Review();
            $reviewObj->Review($item_id);
            $reviewer_name = $reviewObj->getString("reviewer_name");
            $review_content = ($reply_id ? $reviewObj->getString("response") : $reviewObj->getString("review"));
        }


        $sql = "INSERT INTO To_Approved (domain_id, item_id, item_type, item_title, content, assoc_item, rate, reviewer_name, review_content, reply_id, `date`) VALUES ($domain_id, $item_id, '$item_type', $item_title, $content, $assoc_item, $rate, ".$reviewer_name.", ".$review_content.", $reply_id, NOW())";
        Sql::insertSql($sql);

    }


     public static  function system_getLevelDetail($table){
        
        $arrayLevels = array();
      
        $sql = "SELECT value FROM $table WHERE detail = 'y' AND active = 'y' AND theme = 'default' ORDER BY value";
        $result = Sql::fetch($sql);
        if (count($result) > 0){
            foreach ($result as $row){
              $arrayLevels[] = $row->value;
            }
            return $arrayLevels;
        } else {
            return $arrayLevels;
        }
    }


    //
     public static function system_showTruncatedText($text, $length, $extraChar = "...", $isClass = false) {
        unset($return);
        unset($tLen);
        unset($ecLen);
        $text = html_entity_decode($text);
        $tLen = strlen($text);
        if ($tLen > $length) {
            $ecLen = strlen($extraChar);
            $return = substr($text, 0, ($length - $ecLen)).$extraChar;
        } else {
            $return = $text;
        }
        return !$isClass? htmlspecialchars($return): $return;
    }
    // ARRAY TO NAME-VALUE PAIRS
    public static function system_array2nvp($array, $separator = "&") {
        foreach ($array as $name=>$value) {
            $arrayNVP[] = $name."=".$value;
        }
        $nvpString = implode($separator, $arrayNVP);
        return $nvpString;
    }

      public static function activity_newActivity($domain_id = 0, $account_id = 0, $payment_amount = 0, $action = "", $item_type = "", $item_title = "''") {
        
        
        $sql = "INSERT INTO Recent_Activity (domain_id, account_id, payment_amount, action, item_type, item_title, date) VALUES ($domain_id, $account_id, $payment_amount, '$action', '$item_type', $item_title, NOW())";
        Sql::insertSqlMain($sql);
       
    }
    
    
    /**
     * Use this to verify if the domain dropdown must be disabled or not according to URL
     * @package Classes
     * @name domain_updateDashboard
     * @param int $item
     * @access Public
     */
    public static function domain_updateDashboard($item = "", $action = "", $value = 0, $domain_id = SELECTED_DOMAIN_ID) {
        
        if ($item == "revenue"){
            $sql = "UPDATE Dashboard SET $item = ($item + $value) WHERE domain_id = ".$domain_id;
        }else{
            if ($action == "inc"){
                $sql = "UPDATE Dashboard SET $item = ($item + 1) WHERE domain_id = ".$domain_id;
            } else {
                $sql = "UPDATE Dashboard SET $item = ($item - 1) WHERE domain_id = ".$domain_id;
            }
        }
        Sql::insertSqlMain($sql);

    }


    //Site Map
    public static function sitemap_writeFile($file_path, $file_content) {
        $file = fopen($file_path, "a");
        if (!is_writeable($file_path)) {
            die("File: $file_path is not writable".PHP_EOL);
        }
        if ($file) {
            if (fwrite($file, $file_content, Functions::string_strlen($file_content))) {
                fclose($file);
                return true;
            }
        }
        fclose($file);
        return false;
    }
    public static function sitemap_printHeader($encoding="UTF-8") {
        if (!isset($lastmod)) $lastmod = date("Y-m-d");
        $buffer = "";
        $buffer .= "<?xml version=\"1.0\" encoding=\"".$encoding."\"?>".PHP_EOL;
        $buffer .= "<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">".PHP_EOL;
        $buffer .= "\t<url>".PHP_EOL;
        $buffer .= "\t\t<loc>".('http://' == "http://" ? "http://shoresummerrentals.com" : config('params.DEFAULT_URL'))."</loc>".PHP_EOL;
        $buffer .= "\t\t<lastmod>".$lastmod."</lastmod>".PHP_EOL;
        $buffer .= "\t\t<changefreq>monthly</changefreq>".PHP_EOL;
        $buffer .= "\t\t<priority>1.0000</priority>".PHP_EOL;
        $buffer .= "\t</url>".PHP_EOL;
        return $buffer;
    }
    public static function sitemap_printNodeUrl($loc, $lastmod=false) {
        if (!isset($lastmod)) $lastmod = date("Y-m-d");
        $buffer = "";
        $buffer .= "\t<url>".PHP_EOL;
        $buffer .= "\t\t<loc>".$loc."</loc>".PHP_EOL;
        $buffer .= "\t\t<lastmod>".$lastmod."</lastmod>".PHP_EOL;
        $buffer .= "\t\t<changefreq>".($loc == config('params.DEFAULT_URL')."/" || $loc == config('params.DEFAULT_URL') ? "monthly" : "weekly")."</changefreq>".PHP_EOL;
        $buffer .= "\t\t<priority>".($loc == config('params.DEFAULT_URL')."/" || $loc == config('params.DEFAULT_URL') ? "1.0000" : "0.8000")."</priority>".PHP_EOL;
        
        $buffer .= "\t</url>".PHP_EOL;
        return $buffer;
    }
     public static function sitemap_printFooter() {
        $buffer = "";
        $buffer .= "</urlset>".PHP_EOL;
        return $buffer;
    }
    public static function sitemap_createFile() {
        $path = public_path();
        $buffer_init = Functions::sitemap_printHeader();
        if (!Functions::sitemap_writeFile($path.'/sitemap.xml', $buffer_init)) die('ERROR WHILE SAVING THE '.$path.'/sitemap.xml!'.PHP_EOL);
    }

    public static function sitemap_finishFile() 
    {
        $path = public_path();
        $buffer_end = Functions::sitemap_printFooter();
        if (!Functions::sitemap_writeFile($path.'/sitemap.xml', $buffer_end)) die('ERROR WHILE SAVING THE '.$path.'/sitemap.xml!'.PHP_EOL);
    }

    public static function sitemapgen_makeSitemap($path) 
    {

     if (config('params.SITEMAP_FEATURE') == "on") 
    {

            $access = true;
            /*$sql = "SELECT * FROM Registration WHERE name = 'extra' AND domain = '' AND date_time = '0000-00-00 00:00:00' LIMIT 1";
            $result = Sql::fetchMain($sql);
            if ($result) {
                if

                ($row = $result) {
                    if ($row[0]->value == Functions::string_strtoupper(md5(VERSION."SITEMAP_FEATUREon"))) {
                        $access = true;
                    }
                }
            }*/

            if ($access) {

                $files_listinglocation = false;
                $files_listingcategory = false;
                $files_listingdetail = false;
                $files_content = false;

                if (config('params.SITEMAP_HASLISTINGCATEGORY') == "y"){
                    unset($aux_listingcategory);
                    $aux_listingcategory = Functions::sitemap_createModuleCategories($path, 'listing');
                    if(is_array($aux_listingcategory)){
                        $files_listingcategory[] = $aux_listingcategory;
                    }

                }

                 //Add Specials
                //$editorChoices = db_getFromDB("editor_choice", '', 1, "all", "id", "object", SELECTED_DOMAIN_ID);
            $editorChoices=DB::connection('domain')->table('Editor_Choice')->orderBy('id')->get();

                if ($editorChoices) {
                    $default_lastmod = date("Y-m-d");
                    $buffer_specials = "";
                    foreach($editorChoices as $editor) {
                        $badge_name = str_replace(" ", "", strtolower($editor->name));

                        switch ($badge_name) {
                            case 'beachfrontrentals':
                                $badgeLink = (config('params.DEFAULT_URL') == "http://" ? "http://shoresummerrentals.com" : config('params.DEFAULT_URL')) . '/view-ocean_front/orderby-city';
                                break;
                            case 'bayfrontrentals':
                                $badgeLink = (config('params.DEFAULT_URL') == "http://" ? "http://shoresummerrentals.com" : config('params.DEFAULT_URL')) . '/view-bay_front/orderby-city';
                                break;
                            case 'petfriendlyrentals':
                                $badgeLink = (config('params.DEFAULT_URL') == "http://" ? "http://shoresummerrentals.com" : config('params.DEFAULT_URL')) . '/feature-petsallowed/orderby-city';
                                break;
                            default:
                                $badgeLink = (config('params.DEFAULT_URL') == "http://" ? "http://shoresummerrentals.com" : config('params.DEFAULT_URL')) . '/specials-'.preg_replace('/[^A-Za-z0-9\. -]/', '', $badge_name) . '/orderby-city';
                                break;
                        }
                        $buffer_specials .= Functions::sitemap_printNodeUrl($badgeLink, $default_lastmod);
                    }

                    if (!Functions::sitemap_writeFile($path.'/sitemap.xml', $buffer_specials)) die('ERROR WHILE SAVING THE '.$path.'/sitemap.xml!'.PHP_EOL);
                }


              if (config('params.SITEMAP_HASLISTINGLOCATION') == "y"){
                    $files_listinglocation = Functions::sitemap_createModuleLocations($path, 'listing');
               }
                
                if (config('params.SITEMAP_HASLISTINGDETAIL') == "y"){
                    $files_listingdetail = Functions::sitemap_createModuleDetails($path, 'listing');
                }
                if (config('params.SITEMAP_HASCONTENT')  == "y"){
                    $files_content = Functions::sitemap_createContentMap($path);
                }



            }

    }
    else
     {

            if (file_exists($path.'/index.xml')) {
                @unlink($path.'/index.xml');
            }
     }



    }
    public static function sitemap_createModuleCategories($path,$module)
    {

        Functions::setting_get("default_url", $url);
        $url_protocol = "http://".( 'off'== "on" ?  "www." : "");//SITEMAP_ADD_WWW
        $default_url = "$url_protocol$url";
        $item_default_url ='http://shoresummerrentals.com/listing' ;
        //$item_default_url = constant(string_strtoupper($module)."_DEFAULT_URL");
        
        if (!$_SERVER["HTTP_HOST"]){
            if (self::string_strpos($item_default_url, $default_url) === false) {
                $item_default_url = $default_url.str_replace("http://", "", $item_default_url);
            }
        }
        
       
        
        $table = self::string_ucwords($module);
        if ($module == "deal"){
            $table = self::string_ucwords("listing");
        }
        
        $category_query = "SELECT id, 
                                  title, 
                                  friendly_url 
                            FROM ".$table."Category 
                            WHERE category_id=0 AND 
                                  title !='' AND
                                  friendly_url != '' AND
                                  enabled = 'y'
                         ORDER BY title";
        
        $category_result = self::fetch($category_query);
        $default_lastmod = date("Y-m-d");
        $buffer_category = "";
        $files = false;
        $file_number = 0;
        $url_number = 0;
        
        if(count($category_result)>0){
        
            foreach ($category_result as $category_row) {
              
                $category_str = $item_default_url."/".$category_row->friendly_url;//ALIAS_CATEGORY_URL_DIVISOR
                $buffer_category .= self::sitemap_printNodeUrl($category_str, $default_lastmod);
                $url_number++;
                if ($url_number == config('params.SITEMAP_MAXURL')) {

                    if (!self::sitemap_writeFile($path.'/sitemap.xml', $buffer_category)){
                        die('ERROR WHILE SAVING THE '.$path.'/sitemap.xml!'.PHP_EOL);
                    }
                    $buffer_category = "";
                    $files[] = $module."category".$file_number.".xml";
                    $file_number++;
                    $url_number = 0;
                }



               /* $sub1category_query = "SELECT id, title, friendly_url FROM ".$table."Category WHERE category_id=".$category_row['id']." AND title !='' AND friendly_url != '' AND enabled = 'y' ORDER BY title";
                $sub1category_result = $dbObj->query($sub1category_query);
                if(mysql_num_rows($sub1category_result)){
                
                  
                       
                      
                        }*/
                    }
                }
           
        if ($url_number > 0) {
            //$buffer_category .= sitemap_printFooter();
            if (!self::sitemap_writeFile($path.'/sitemap.xml', $buffer_category)){
                die('ERROR WHILE SAVING THE '.$path.'/sitemap.xml!'.PHP_EOL);
            }
            $buffer_category = "";
            $files[] = $module."category".$file_number.".xml";
            $file_number++;
            $url_number = 0;
        }
        if (is_array($files)) {
            return $files;
        } else {
            return false;
        }

    }

     public static function sitemap_buildUrlPath($_locations, $_location_level, $location_father_id, &$buffer_location, $location_str, &$url_number, &$file_number, &$files) {
         self::system_retrieveLocationRelationship ($_locations, $_location_level, $_location_father_level, $_location_child_level);
         $location_query = "SELECT id, name, friendly_url FROM Location_". $_location_level." WHERE location_".$_location_father_level."=".$location_father_id." ORDER BY name";
         unset($locations_result);
         
         $path = public_path();//EDIRECTORY_ROOT
         $default_lastmod = date("Y-m-d");
         $locations_result = self::fetchMain($location_query);
          foreach ($locations_result as $location) {
            
            $str_location_toWrite = $location_str."/". $location->friendly_url;
            $location_id = $location->id;
            $buffer_location .= self::sitemap_printNodeUrl($str_location_toWrite, $default_lastmod);
            $url_number++;
            if ($url_number == config('params.SITEMAP_MAXURL')) {
               
                if (!self::sitemap_writeFile($path.'/sitemap.xml', $buffer_location)) die('ERROR WHILE SAVING THE '.$path.'/sitemap.xml!'.PHP_EOL);
                $buffer_location = "";
                $files[] = $module."location".$file_number.".xml";
                $file_number++;
                $url_number = 0;
            }

            if ($_location_child_level) {
                self::sitemap_buildUrlPath($_locations, $_location_child_level, $location_id, $buffer_location, $str_location_toWrite, $url_number, $file_number, $files);
            }
        }
    }
     public static  function sitemap_createModuleLocations($path, $module) {

         Functions::setting_get("default_url", $url);
        $url_protocol = "http://".( 'off'== "on" ?  "www." : "");//SITEMAP_ADD_WWW
        $default_url = "$url_protocol$url";
        $item_default_url ='http://shoresummerrentals.com/listing' ;//constant(Functions::string_strtoupper($module)."_DEFAULT_URL")
        if (!$_SERVER["HTTP_HOST"]){
            if (Functions::string_strpos($item_default_url, $default_url) === false) {
                $item_default_url = $default_url.str_replace("http://", "", $item_default_url);
            }
        }
        
        /*if ($module == "promotion"){
            $module = "deal";
        }*/
        
        $_locations = explode(",", config('params.EDIR_LOCATIONS'));
        $_location_level = $_locations[0];
        self::system_retrieveLocationRelationship ($_locations, $_location_level, $_location_father_level, $_location_child_level);
        $location_query = "SELECT id, name, friendly_url FROM Location_". $_location_level." ORDER BY name";
        unset($locations_result);
        $locations_result = self::fetchMain($location_query);
        $default_lastmod = date("Y-m-d");
        $buffer_location = "";
        $files = false;
        $file_number = 0;
        $url_number = 0;
        foreach ($locations_result as $location) {
           
            $location_str = $item_default_url."/".$location->friendly_url;//ALIAS_LOCATION_URL_DIVISOR
            $location_id = $location->id;
            $buffer_location .= Functions::sitemap_printNodeUrl($location_str, $default_lastmod);
            $url_number++;
            if ($url_number == config('params.SITEMAP_MAXURL')) {
              
                if (!Functions::sitemap_writeFile($path.'/sitemap.xml', $buffer_location)) die('ERROR WHILE SAVING THE '.$path.'/sitemap.xml!'.PHP_EOL);
                $buffer_location = "";
                $files[] = $module."location".$file_number.".xml";
                $file_number++;
                $url_number = 0;
            }

            if (isset($_location_child_level)) {
                Functions::sitemap_buildUrlPath($_locations, $_location_child_level, $location_id, $buffer_location, $location_str, $url_number, $file_number, $files);
            }

        }

        if ($url_number > 0) {
            
            if (!Functions::sitemap_writeFile($path.'/sitemap.xml', $buffer_location)) die('ERROR WHILE SAVING THE '.$path.'/sitemap.xml!'.PHP_EOL);
            $buffer_location = "";
            $files[] = $module."location".$file_number.".xml";
            $file_number++;
            $url_number = 0;
        }
        return $files;
    }
    public static function sitemap_createModuleDetails($path, $module) {
       
        Functions::setting_get("default_url", $url);
        $url_protocol = "http://".( 'off'== "on" ?  "www." : "");//SITEMAP_ADD_WWW
        $default_url = "$url_protocol$url";
        $item_default_url ='http://shoresummerrentals.com/listing' ;//constant(Functions::string_strtoupper($module)."_DEFAULT_URL")
        if (!$_SERVER["HTTP_HOST"]){
            if (Functions::string_strpos($item_default_url, $default_url) === false) {
                $item_default_url = $default_url.str_replace("http://", "", $item_default_url);
            }
        }
        
     if($module=='listing') {
        
            $levelsdetail_query = "SELECT value FROM ".ucfirst($module)."Level WHERE detail = 'y' AND theme = 'default'";
            $levelsdetail_result =self::fetch($levelsdetail_query);
            $levelsdetail = array();
            foreach ($levelsdetail_result as $arr) {
                $levelsdetail[] = $arr->value;
            }
            $items_query = "SELECT id, DATE(updated) AS updated, title, friendly_url FROM ".ucfirst($module)." WHERE FIND_IN_SET(level, '".implode(',', $levelsdetail)."') AND status = 'A' ORDER BY title";
            $items_result = self::fetch($items_query);
            
        }
        $buffer_moduleDetails = "";
        $files = false;
        $file_number = 0;
        $url_number = 0;
        foreach ($items_result as $item) {
            
            $loc = "".$item_default_url."/".$item->friendly_url.".html";
            $lastmod = $item->updated;
            $buffer_moduleDetails .= Functions::sitemap_printNodeUrl($loc, $lastmod);
            $url_number++;
            if ($url_number == config('params.SITEMAP_MAXURL')) {
                //$buffer_moduleDetails .= sitemap_printFooter();
                if (!Functions::sitemap_writeFile($path.'/sitemap.xml', $buffer_moduleDetails)) die('ERROR WHILE SAVING THE '.$path.'/sitemap.xml!'.PHP_EOL);
                $buffer_moduleDetails = "";
                $files[] = $module."detail".$file_number.".xml";
                $file_number++;
                $url_number = 0;
            }
        }
        if ($url_number > 0) {
            if (!Functions::sitemap_writeFile($path.'/sitemap.xml', $buffer_moduleDetails)) die('ERROR WHILE SAVING THE '.$path.'/sitemap.xml!'.PHP_EOL);
            $buffer_moduleDetails = "";
            $files[] = $module."detail".$file_number.".xml";
            $file_number++;
            $url_number = 0;
        }
        return $files;
    }
     public static function system_retrieveLocationRelationship ($_locations, $_location_level, &$_location_father_level, &$_location_child_level) {
        $location_key = array_search ($_location_level, $_locations);
        if ($location_key!==false) {
            if ($location_key==0) $_location_father_level = false; else $_location_father_level = $_locations[$location_key-1];
            if ($location_key==(count($_locations)-1)) $_location_child_level = false; else $_location_child_level = $_locations[$location_key+1];
        }
    }
    //Export Files
    public static function export_formatToCSV($field) {
        $field = str_replace("\n\r", "", $field);
        $field = str_replace("\r\n", "", $field);
        $field = str_replace("\n", "", $field);
        $field = str_replace("\r", "", $field);
        $field = str_replace("'", "\'", $field);
        $field = str_replace('"', "\'", $field);
        if (self::string_strpos($field, ",")!==false) {
            $field = "\"".$field."\"";
        }
        
        if (!$field || $field == "0000-00-00" || $field == "0000-00-00 00:00:00" || (is_numeric($field) && round($field) == 0)) $field = "" ;
        
        return $field;
    }

    public static function export_progress($filename, $message, $error = false, $item_scalability = "", $tableCron = "", $dbObj = false, $domain_id = 0) {
        if ($error && $item_scalability == "on" && $dbObj && $domain_id > 0) {
            $message .= "||error";
            $sql_finished = "UPDATE $tableCron SET finished = 'Y', scheduled = 'N', running_cron = 'N', filename = '' WHERE domain_id = ".$domain_id." AND type = 'csv'";
            $dbObj->query($sql_finished);
        }
        
        if (!$handle = fopen($filename, "w")) {
            setting_get("sitemgr_email", $sitemgr_email);
            $eDirMailerObj = new EDirMailer(EDIR_ADMIN_EMAIL, "[eDirectory] - Export Process", "Error: file open (".$filename.").", $sitemgr_email);
            $eDirMailerObj->send();
            exit;
        }
        if (fwrite($handle, $message) === false) {
            setting_get("sitemgr_email", $sitemgr_email);
            $eDirMailerObj = new EDirMailer(EDIR_ADMIN_EMAIL, "[eDirectory] - Export Process", "Error: file write (".$filename.").", $sitemgr_email);
            $eDirMailerObj->send();
            exit;
        }
        if (!fclose($handle)) {
            setting_get("sitemgr_email", $sitemgr_email);
            $eDirMailerObj = new EDirMailer(EDIR_ADMIN_EMAIL, "[eDirectory] - Export Process", "Error: file close (".$filename.").", $sitemgr_email);
            $eDirMailerObj->send();
            exit;
        }
    }
    public static function sitemap_createContentMap($path) {
       
        Functions::setting_get("default_url", $url);
        $url_protocol = "http://".( 'off'== "on" ?  "www." : "");//SITEMAP_ADD_WWW
        $default_url = "$url_protocol$url";
        //$url_protocol = "http://".(SITEMAP_ADD_WWW == "on" ?  "www." : "");
        $content_query  = "SELECT C.`id`, C.`updated`, C.`title`, C.`url` FROM `Content` AS C WHERE C.`section` = 'client' AND C.`url` != '' AND C.`sitemap` = '1'";
        $content_result = self::fetch($content_query);
        $buffer_content = "";
        $files = false;
        $file_number = 0;
        $url_number = 0;
        foreach ($content_result as $content) {
            
            if (self::string_strpos($content->updated, "0000-00-00") !== false) {
                $lastmod = date("Y-m-d");
            } else {
                $lastmod = date("Y-m-d", strtotime($content->updated));
            }
            $loc = $default_url."/content/".$content->url.".html";
            $buffer_content .= self::sitemap_printNodeUrl($loc, $lastmod);
            $url_number++;
            if ($url_number == config('params.SITEMAP_MAXURL')) {
                if (!self::sitemap_writeFile($path.'/sitemap.xml', $buffer_content)) die('ERROR WHILE SAVING THE '.$path.'/sitemap.xml!'.PHP_EOL);
                $buffer_content = "";
                $files[] = "content".$file_number.".xml";
                $file_number++;
                $url_number = 0;
            }
        }
        if ($url_number > 0) {
            if (!self::sitemap_writeFile($path.'/sitemap.xml', $buffer_content)) die('ERROR WHILE SAVING THE '.$path.'/sitemap.xml!'.PHP_EOL);
            $buffer_content = "";
            $files[] = "content".$file_number.".xml";
            $file_number++;
            $url_number = 0;
        }
        return $files;
    }

    Public static function export_ExportToCSV($export_type = "listing", $file, $removecontrol, $id = false)
        {
        if (!is_numeric($id) && !$id && $id <= 0) {
            echo "Invalid ID";
            exit;
        }
        
        if ($export_type == "listing"){
            $item_scalability = config('params.LISTING_SCALABILITY_OPTIMIZATION');
            $tableCron = "Control_Export_Listing";
            $tableModule = "Listing";
            $field1 = "total_listing_exported";
            $field2 = "last_listing_id";
            $levelType = "ListingLevel";
        } 
        if($item_scalability != "on" && !$file){
            echo "Need file name!";
            exit;
        }

        if (!$file) {
            /*setting_get("sitemgr_email", $sitemgr_email);
            $eDirMailerObj = new EDirMailer(EDIR_ADMIN_EMAIL, "[eDirectory] - Export Process", "Error: not get file.", $sitemgr_email);
            $eDirMailerObj->send();
            echo "Haven't file!";
            exit;*/
        }
      define('IMPORT_FOLDER',public_path()."/custom/domain_1/import_files");

        $filename = IMPORT_FOLDER."/export_". $file;
    
        if ($removecontrol) {
    
            if (!unlink($filename)) {
                //setting_get("sitemgr_email", $sitemgr_email);
                //$eDirMailerObj = new EDirMailer(EDIR_ADMIN_EMAIL, "[eDirectory] - Export Process", "Error: file unlink (".$filename.").", $sitemgr_email);
                //$eDirMailerObj->send();
                ////exit;
            }
    
        } 
        else 
        {
    
            /*
             * Check if need do a new file
             */
            DB::connection('domain')->setFetchMode(PDO::FETCH_ASSOC);
            $sql_count = "SELECT COUNT(id) AS total FROM $tableModule";
            $result_count = Sql::fetch($sql_count);
            if ($result_count) {
                if ($row_count = $result_count) {
                    $item_amount = $row_count[0]["total"];
                } else {
                    export_progress($filename, system_showText(LANG_SITEMGR_EXPORT_ERRORNUMBER)." 20006<br />".system_showText(LANG_SITEMGR_EXPORT_CONTACTSUPPORT), true, $item_scalability, $tableCron, $dbObj, $id);
                    exit;
                }
            } else {
                export_progress($filename, system_showText(LANG_SITEMGR_EXPORT_ERRORNUMBER)." 20005<br />".system_showText(LANG_SITEMGR_EXPORT_CONTACTSUPPORT), true, $item_scalability, $tableCron, $dbObj, $id);
                exit;
            }
            
            if($item_scalability == "on"){
            
                /*
                 * Get setting to do export
                 */
                $sql_settings = "SELECT id,
                                        last_run_date, 
                                        $field1, 
                                        $field2, 
                                        block,
                                        datediff(last_run_date, now()) as date_diff,
                                        finished,
                                        filename
                                    FROM $tableCron
                                    WHERE domain_id = ".$id." AND type = 'csv'";
                $result_settings = $dbObj->query($sql_settings);
                if(mysql_num_rows($result_settings)){
                    $aux_add_header = true;
                    $row_settings = mysql_fetch_assoc($result_settings);
                    if($row_settings["finished"] == "Y"){
                        $aux_add_header = true;
                    }else{
                        $aux_add_header = false;
                    }
                }
            }else{
                $aux_add_header = true;
            }
        
            /*
             * Writing header
             */
            if($aux_add_header){
                
                //export_progress($filename, "0");
        
                if ($export_type == "listing")
                {
                    $handle = fopen(IMPORT_FOLDER."/edirectory_sample.csv", "r");
                } 
                $sample_header = fgets($handle);
                fclose($handle);
                
                if ($export_type == "listing" || !$export_type){    
                    if (config('params.DEMO_DEV_MODE')) {
                        $sql = "SHOW columns FROM Listing WHERE Field LIKE 'amenity\_%' OR Field LIKE 'feature\_%' OR Field LIKE 'activity\_%'";
                    } else {
                        $sql = "SELECT DISTINCT COLUMN_NAME AS Field FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Listing' AND COLUMN_NAME LIKE 'amenity\_%' OR COLUMN_NAME LIKE 'feature\_%' OR COLUMN_NAME LIKE 'activity\_%' ORDER BY COLUMN_NAME";
                    }
                    $result =Sql::fetch($sql);
                    $array_fields = array();
                    foreach ($result as $fields) {
                        $array_fields2[] = $fields["Field"];
                        $array_fields[] = ucwords(str_replace("_", " ", $fields["Field"]));
                    }
                    $array_fields = implode(",", $array_fields);
                    $string_fields = implode(",", $array_fields2);

                    $export_header = "Account Username,Account Password,Account Contact First Name,Account Contact Last Name,Account Contact Company,Account Contact Address,Account Contact Address2,Account Contact Country,Account Contact State,Account Contact City,Account Contact Postal Code,Account Contact Phone,Account Contact Fax,Account Contact Email,Account Contact URL,Listing ID,Listing Title,Listing SEO Title,Listing Email,Listing URL,Listing Address,Listing Address2,Listing Country,Listing Country Abbreviation,Listing Region,Listing Region Abbreviation,Listing State,Listing State Abbreviation,Listing City,Listing City Abbreviation,Listing Neighborhood,Listing Neighborhood Abbreviation,Listing Postal Code,Listing Latitude,Listing Longitude,Listing Phone,Listing Fax,Listing Contact First Name,Listing Contact Last Name,Listing Short Description,Listing Long Description,Listing SEO Description,Listing Keywords,Listing Renewal Date,Listing Status,Listing Level,Listing Category 1,Listing Category 2,Listing Category 3,Listing Category 4,Listing Category 5,Listing Template,Custom ID,Listing # of Bedrooms,Listing Bedding Arrangement,Listing # of Bathrooms,Listing Maximum Occupancy,Property Types,Views,Distance to Beach,Development Name,Required Stay,Airport Abbreviation,Airport Distance,Check In Time,Check Out Time,Payment Types Accepted,Booking Deposit and Payment Terms,Cancellation Policy,Security Deposit,Notes,".$array_fields."\n";
                } 
                $sample_header_array = explode(",", $sample_header);
                $export_header_array = explode(",", $export_header);
               
                if (count($sample_header_array) != count($export_header_array)) {
                    export_progress($filename, system_showText(LANG_SITEMGR_EXPORT_ERRORNUMBER)." 20000<br />".system_showText(LANG_SITEMGR_EXPORT_CONTACTSUPPORT), true, $item_scalability, $tableCron, $dbObj, $id);
                    exit;
                }
                for ($i = 0; $i < count($sample_header_array); $i++) {
                    $sample_header_array[$i] = str_replace("\n\r", "", $sample_header_array[$i]);
                    $sample_header_array[$i] = str_replace("\r\n", "", $sample_header_array[$i]);
                    $sample_header_array[$i] = str_replace("\n", "", $sample_header_array[$i]);
                    $sample_header_array[$i] = str_replace("\r", "", $sample_header_array[$i]);
                    $export_header_array[$i] = str_replace("\n\r", "", $export_header_array[$i]);
                    $export_header_array[$i] = str_replace("\r\n", "", $export_header_array[$i]);
                    $export_header_array[$i] = str_replace("\n", "", $export_header_array[$i]);
                    $export_header_array[$i] = str_replace("\r", "", $export_header_array[$i]);
                    if ($sample_header_array[$i] != $export_header_array[$i]) {
                        export_progress($filename, system_showText(LANG_SITEMGR_EXPORT_ERRORNUMBER)." 20001<br />".system_showText(LANG_SITEMGR_EXPORT_CONTACTSUPPORT), true, $item_scalability, $tableCron, $dbObj, $id);
                        exit;
                    }
                }
        
                /*if($item_scalability == "on"){
                    /*
                     * Save filename on table to continue on the next cicle of cron
                     
                    $sql_filename = "UPDATE $tableCron SET
                                            filename = '".$file."',
                                            $field1 = 0,
                                            last_run_date = NOW(),
                                            $field2 = 0,
                                            finished = 'N'
                                      WHERE domain_id = ".$id." AND type = 'csv'";
/
                    $aux_total_item = 0;


                    $dbObj->query($sql_filename);
                }*/

               if (!$handle = fopen($filename, "a")) {
                    export_progress($filename, system_showText(LANG_SITEMGR_EXPORT_ERRORNUMBER)." 20002<br />".system_showText(LANG_SITEMGR_EXPORT_CONTACTSUPPORT), true, $item_scalability, $tableCron, $dbObj, $id);
                    exit;
                }
        
                if (fwrite($handle, $export_header) === false) {
                    export_progress($filename, system_showText(LANG_SITEMGR_EXPORT_ERRORNUMBER)." 20004<br />".system_showText(LANG_SITEMGR_EXPORT_CONTACTSUPPORT), true, $item_scalability, $tableCron, $dbObj, $id);
                    exit;
                }
            }else{
                /*
                 * Aux to start new process
                 */
                $aux_total_item = $row_settings["$field1"];
                
                if (!$handle = fopen(IMPORT_FOLDER."/".$row_settings["filename"], "a")) {
                    export_progress($filename, system_showText(LANG_SITEMGR_EXPORT_ERRORNUMBER)." 20002<br />".system_showText(LANG_SITEMGR_EXPORT_CONTACTSUPPORT), true, $item_scalability, $tableCron, $dbObj, $id);
                    exit;
                }
            }
            
            if($item_scalability == "on"){
                $i = $aux_total_item;
            }else{
                $i = 1;
            }
            $statusObj = new ItemStatus();
             $statusObj->ItemStatus();
            $levelObj = new ListingLevel(false,$id);
            //$levelObj->ListingLevel();
    
            if ($export_type == "listing" || !$export_type){
                /*
                 * Get listings to export
                 */
                     $sql = "SELECT Listing_Summary.id as item_id,
                                Listing_Summary.account_id as item_account_id,
                                Listing_Summary.title as item_title,
                                Listing_Summary.email as item_email,
                                Listing_Summary.url as item_url,
                                Listing_Summary.address as item_address,
                                Listing_Summary.address2 as item_address2,
                                Listing_Summary.location_1_title as item_location_1_title,
                                Listing_Summary.location_1_abbreviation as item_location_1_abbreviation,
                                Listing_Summary.location_2_title as item_location_2_title,
                                Listing_Summary.location_2_abbreviation as item_location_2_abbreviation,
                                Listing_Summary.location_3_title as item_location_3_title,
                                Listing_Summary.location_3_abbreviation as item_location_3_abbreviation,
                                Listing_Summary.location_4_title as item_location_4_title,
                                Listing_Summary.location_4_abbreviation as item_location_4_abbreviation,
                                Listing_Summary.location_5_title as item_location_5_title,
                                Listing_Summary.location_5_abbreviation as item_location_5_abbreviation,
                                Listing_Summary.zip_code as item_zip_code,
                                Listing_Summary.latitude as item_latitude,
                                Listing_Summary.longitude as item_longitude,
                                Listing_Summary.phone as item_phone,
                                Listing_Summary.fax as item_fax,
                                Listing_Summary.contact_fname as item_contact_fname,
                                Listing_Summary.contact_lname as item_contact_lname,
                                Listing_Summary.description as item_short_description,
                                Listing_Summary.status as item_status,
                                Listing_Summary.level as item_level,
                                Listing_Summary.listingtemplate_id as item_template_id,
                                Listing_Summary.template_title as item_template_title,
                                Listing_Summary.bedroom as item_bedrooms,
                                Listing_Summary.bedsize as item_bedsize,
                                Listing_Summary.bathroom as item_bathrooms,
                                Listing_Summary.sleeps as item_sleeps,
                                Listing_Summary.property_type as item_property_type,
                                Listing_Summary.view as item_view,
                                Listing_Summary.distance_beach as item_distance_beach,
                                Listing_Summary.development_name as item_development_name,
                                Listing_Summary.required_stay as item_required_stay,
                                Listing_Summary.airport_abbreviation as item_airport_abbreviation,
                                Listing_Summary.airport_distance as item_airport_distance
                            FROM Listing_Summary Listing_Summary";

                if ($item_scalability == "on") {
                    $sql .= " WHERE Listing_Summary.id > ".$row_settings["$field2"];
                    $sql .= " ORDER BY Listing_Summary.id limit ".$row_settings["block"];
                }
            }

            $result =Sql::fetch($sql);
            if ($result) {
    
                foreach ($result as $row) {//($i <= $item_amount)
                    
                    $account_username = "";
                    $account_password = "";
                    $account_contact_first_name = "";
                    $account_contact_last_name = "";
                    $account_contact_company = "";
                    $account_contact_address = "";
                    $account_contact_address2 = "";
                    $account_contact_country = "";
                    $account_contact_state = "";
                    $account_contact_city = "";
                    $account_contact_postal_code = "";
                    $account_contact_phone = "";
                    $account_contact_fax = "";
                    $account_contact_email = "";
                    $account_contact_url = "";
                    $item_id = "";
                    $item_title = "";
                    $item_seo_title = "";
                    $item_email = "";
                    $item_url = "";
                    $item_address = "";
                    $item_address2 = ""; //for listings only
                    
                    $item_location1 = "";
                    $item_location1_abbreviation = "";
                    $item_location2 = "";
                    $item_location2_abbreviation = "";
                    $item_location3 = "";
                    $item_location3_abbreviation = "";
                    $item_location4 = "";
                    $item_location4_abbreviation = "";
                    $item_location5 = "";
                    $item_location5_abbreviation = "";
                    $item_postal_code = "";
                    $item_latitude = "";
                    $item_longitude = "";
                    $item_phone = "";
                    $item_fax = ""; //for listings only
                    $item_short_description = "";
                    $item_long_description = "";
                    $item_seo_description = "";
                    $item_keywords = "";
                    $item_renewal_date = "";
                    $item_status = "";
                    $item_level = "";
                    $item_category_1 = "";
                    $item_category_2 = "";
                    $item_category_3 = "";
                    $item_category_4 = "";
                    $item_category_5 = "";
                    $item_template = ""; //for listings only
                    $item_custom_id = "";
                    
                    # NEW ITEMS
                    $item_bedrooms = ""; //for listings only
                    $item_bedsize = ""; //for listings only
                    $item_bathrooms = ""; //for listings only
                    $item_sleeps = ""; //for listings only
                    $item_contact_fname = ""; //for listings only
                    $item_contact_lname = ""; //for listings only
                    $item_property_type = ""; //for listings only
                    $item_view = ""; //for listings only
                    $item_distance_beach = ""; //for listings only
                    $item_development_name = ""; //for listings only
                    $item_required_stay = ""; //for listings only
                    $item_airport_abbreviation = ""; //for listings only
                    $item_airport_distance = ""; //for listings only
                    $item_rate_checkin = ""; //for listings only
                    $item_rate_checkout = ""; //for listings only
                    $item_rate_payment_types = ""; //for listings only
                    $item_rate_payment_terms = ""; //for listings only
                    $item_rate_cancellation_policy = ""; //for listings only
                    $item_rate_security_deposit = ""; //for listings only
                    $item_rate_notes = ""; //for listings only
                    $item_property_type = ""; //for listings only
    
                    $item_id = $row['item_id'];
                    
                    if ($export_type == "listing") {
                        /*
                         *  These fields above don't exists on the table Listing_Summary
                         */                 

                        $sql2 = "
                            SELECT seo_title, seo_description, long_description, keywords, renewal_date, custom_id, ".$string_fields." FROM Listing WHERE id = ".$item_id;

                        $result2 =Sql::fetch($sql2);
                        if ($result2) {
                            $row2= $result2;
                            $item_seo_title = self::export_formatToCSV($row2[0]['seo_title']);
                            $item_seo_description = self::export_formatToCSV($row2[0]['seo_description']);
                            $item_long_description = self::export_formatToCSV($row2[0]['long_description']);
                            $item_keywords = self::export_formatToCSV($row2[0]['keywords']);
                            $item_renewal_date_aux = self::format_date($row2[0]['renewal_date'], config('params.DEFAULT_DATE_FORMAT'));
                            $item_renewal_date = self::export_formatToCSV($item_renewal_date_aux);
                            $item_custom_id = self::export_formatToCSV($row2[0]['custom_id']);
                            foreach ($array_fields2 as $field) {
                                ${"item_".$field} = self::export_formatToCSV($row2[0]["$field"]);
                            }

                        }
                    } 
                    /****************************************************************/
                    $item_id_export                 = self::export_formatToCSV($item_id);
                    $item_title                     = self::export_formatToCSV($row["item_title"]);
                    $item_email                     = self::export_formatToCSV($row["item_email"]);
                    $item_url                       = self::export_formatToCSV($row["item_url"]);
                    $item_address                   = self::export_formatToCSV($row["item_address"]);
                    $item_address2                  = self::export_formatToCSV($row["item_address2"]);
                    //$item_location                  = self::export_formatToCSV($row["item_location"]);
                    //$item_contact_name              = self::export_formatToCSV($row["item_contact_name"]);
                    $item_postal_code               = self::export_formatToCSV($row["item_zip_code"]);
                    $item_latitude                  = self::export_formatToCSV($row["item_latitude"]);
                    $item_longitude                 = self::export_formatToCSV($row["item_longitude"]);
                    $item_phone                     = self::export_formatToCSV($row["item_phone"]);
                    $item_fax                       = self::export_formatToCSV($row["item_fax"]);
                    $item_short_description         = self::export_formatToCSV($row["item_short_description"]);
                    $item_template                  = self::export_formatToCSV($row["item_template_title"]);
                    $item_status                    = self::export_formatToCSV(self::string_strtolower($statusObj->getStatus($row["item_status"])));
                    //$item_level                     = self::export_formatToCSV(self::string_strtolower($levelObj->getLevel($row["item_level"])));
                    if ($export_type == "listing") {
                        $item_location1                 = self::export_formatToCSV($row["item_location_1_title"]);
                        $item_location1_abbreviation    = self::export_formatToCSV($row["item_location_1_abbreviation"]);
                        $item_location2                 = self::export_formatToCSV($row["item_location_2_title"]);
                        $item_location2_abbreviation    = self::export_formatToCSV($row["item_location_2_abbreviation"]);
                        $item_location3                 = self::export_formatToCSV($row["item_location_3_title"]);
                        $item_location3_abbreviation    = self::export_formatToCSV($row["item_location_3_abbreviation"]);
                        $item_location4                 = self::export_formatToCSV($row["item_location_4_title"]);
                        $item_location4_abbreviation    = self::export_formatToCSV($row["item_location_4_abbreviation"]);
                        $item_location5                 = self::export_formatToCSV($row["item_location_5_title"]);
                        $item_location5_abbreviation    = self::export_formatToCSV($row["item_location_5_abbreviation"]);

                        $item_bedrooms  = self::export_formatToCSV($row["item_bedrooms"]);
                        $item_bedsize  = self::export_formatToCSV($row["item_bedsize"]);
                        $item_bathrooms = self::export_formatToCSV($row["item_bathrooms"]);
                        $item_sleeps    = self::export_formatToCSV($row["item_sleeps"]);

                        $item_contact_fname            = self::export_formatToCSV($row["item_contact_fname"]);
                        $item_contact_lname            = self::export_formatToCSV($row["item_contact_lname"]);
                        $item_property_type            = self::export_formatToCSV($row["item_property_type"]);
                        $item_view                     = self::export_formatToCSV($row["item_view"]);
                        $item_distance_beach           = self::export_formatToCSV($row["item_distance_beach"]);
                        $item_development_name         = self::export_formatToCSV($row["item_development_name"]);
                        $item_required_stay            = self::export_formatToCSV($row["item_required_stay"]);
                        $item_airport_abbreviation     = self::export_formatToCSV($row["item_airport_abbreviation"]);
                        $item_airport_distance         = self::export_formatToCSV($row["item_airport_distance"]);

                        $rateObj = new RateInformation($item_id);
                        $item_rate_checkin             = self::export_formatToCSV($rateObj->checkin);
                        $item_rate_checkout            = self::export_formatToCSV($rateObj->checkout);
                        $item_rate_payment_types       = self::export_formatToCSV($rateObj->payment_types);
                        $item_rate_payment_terms       = self::export_formatToCSV($rateObj->payment_terms);
                        $item_rate_cancellation_policy = self::export_formatToCSV($rateObj->cancellation_policy);
                        $item_rate_security_deposit    = self::export_formatToCSV($rateObj->security_deposit);
                        $item_rate_notes               = self::export_formatToCSV($rateObj->notes);
                        
                        
                    }

                    if ($row["item_account_id"]){
                         //DB::connection('domain')->setFetchMode(PDO::FETCH_CLASS);

                        $account = new Account($row["item_account_id"]);
                        //$account->Account($row["item_account_id"]);

                        $row["account_username"] = $account->getString("username");
                        $row["account_password"] = $account->getString("password");


                        $contact = new Contact($row["item_account_id"]);
                        //$contact = new Contact();
                        //DB::connection('domain')->setFetchMode(PDO::FETCH_ASSOC);


                        $row["contact_first_name"] = $contact->getString("first_name");
                        $row["contact_last_name"] = $contact->getString("last_name");
                        $row["contact_company"] = $contact->getString("company");
                        $row["contact_address"] = $contact->getString("address");
                        $row["contact_address2"] = $contact->getString("address2");
                        $row["contact_country"] = $contact->getString("country");
                        $row["contact_state"] = $contact->getString("state");
                        $row["contact_city"] = $contact->getString("city");
                        $row["contact_zip"] = $contact->getString("zip");
                        $row["contact_phone"] = $contact->getString("phone");
                        $row["contact_fax"] = $contact->getString("fax");
                        $row["contact_email"] = $contact->getString("email");
                        $row["contact_url"] = $contact->getString("url");
                    } else {
                        $row["account_username"] = "";
                        $row["account_password"] = "";
                        $row["contact_first_name"] = "";
                        $row["contact_last_name"] = "";
                        $row["contact_company"] = "";
                        $row["contact_address"] = "";
                        $row["contact_address2"] = "";
                        $row["contact_country"] = "";
                        $row["contact_state"] = "";
                        $row["contact_city"] = "";
                        $row["contact_zip"] = "";
                        $row["contact_phone"] = "";
                        $row["contact_fax"] = "";
                        $row["contact_email"] = "";
                        $row["contact_url"] = "";
                    }

                    $account_username = self::export_formatToCSV($row["account_username"]);
                    $account_password = self::export_formatToCSV($row["account_password"]);

                    $account_contact_first_name     = self::export_formatToCSV($row["contact_first_name"]);
                    $account_contact_last_name      = self::export_formatToCSV($row["contact_last_name"]);
                    $account_contact_company        = self::export_formatToCSV($row["contact_company"]);
                    $account_contact_address        = self::export_formatToCSV($row["contact_address"]);
                    $account_contact_address2       = self::export_formatToCSV($row["contact_address2"]);
                    $account_contact_country        = self::export_formatToCSV($row["contact_country"]);
                    $account_contact_state          = self::export_formatToCSV($row["contact_state"]);
                    $account_contact_city           = self::export_formatToCSV($row["contact_city"]);
                    $account_contact_postal_code    = self::export_formatToCSV($row["contact_zip"]);
                    $account_contact_phone          = self::export_formatToCSV($row["contact_phone"]);
                    $account_contact_fax            = self::export_formatToCSV($row["contact_fax"]);
                    $account_contact_email          = self::export_formatToCSV($row["contact_email"]);
                    $account_contact_url            = self::export_formatToCSV($row["contact_url"]);
    
                    /*====================================================================================================*/
    
                    if ($export_type == "listing") {
                        /*
                        * Get categories to export
                        */
                        $sql_categories = "SELECT category_id FROM Listing_Category WHERE listing_id = ".$item_id;
                        $result_categories = Sql::fetch($sql_categories);

                        if(count($result_categories)>0){
                            $listing_category_count=1;
                            foreach($result_categories as $row_categories){
                                unset($aux_array_category, $string_category, $categoryObj);
                                  DB::connection('domain')->setFetchMode(PDO::FETCH_CLASS);

                                $categoryObj = new ListingCategory();
                                 $categoryObj->ListingCategory($row_categories["category_id"]);
                                  DB::connection('domain')->setFetchMode(PDO::FETCH_ASSOC);

                                $sql_category = "SELECT title
                                                    FROM ListingCategory
                                                    WHERE root_id = ".$categoryObj->getNumber('root_id')." AND
                                                        ListingCategory.left <= ".$categoryObj->getNumber("left")." AND
                                                        ListingCategory.right >= ".$categoryObj->getNumber("right")."
                                                    ORDER BY ListingCategory.left";
                                $result_category = Sql::fetch($sql_category);
                                if(count($result_category)>0){
                                     foreach($result_category as $row_category){
                                        $aux_array_category[] = $row_category["title"];
                                    }
                                    $string_category = implode(" -> ",$aux_array_category);
                                }
                                ${"item_category_".$listing_category_count} = self::export_formatToCSV($string_category);
                                $listing_category_count++;

                            }
                        }

                        $newItemsString = $item_property_type.",".$item_view.",".$item_distance_beach.",".$item_development_name.",".$item_required_stay.",".$item_airport_abbreviation.",".$item_airport_distance.",".$item_rate_checkin.",".$item_rate_checkout.",".$item_rate_payment_types.",".$item_rate_payment_terms.",".$item_rate_cancellation_policy.",".$item_rate_security_deposit.",".$item_rate_notes;

                        $this_item_line = "".$account_username.",".$account_password.",".$account_contact_first_name.",".$account_contact_last_name.",".$account_contact_company.",".$account_contact_address.",".$account_contact_address2.",".$account_contact_country.",".$account_contact_state.",".$account_contact_city.",".$account_contact_postal_code.",".$account_contact_phone.",".$account_contact_fax.",".$account_contact_email.",".$account_contact_url.",".$item_id_export.",".$item_title.",".$item_seo_title.",".$item_email.",".$item_url.",".$item_address.",".$item_address2.",".$item_location1.",".$item_location1_abbreviation.",".$item_location2.",".$item_location2_abbreviation.",".$item_location3.",".$item_location3_abbreviation.",".$item_location4.",".$item_location4_abbreviation.",".$item_location5.",".$item_location5_abbreviation.",".$item_postal_code.",".$item_latitude.",".$item_longitude.",".$item_phone.",".$item_fax.",".$item_contact_fname.",".$item_contact_lname.",".$item_short_description.",".$item_long_description.",".$item_seo_description.",".$item_keywords.",".$item_renewal_date.",".$item_status.",".$item_level.",".$item_category_1.",".$item_category_2.",".$item_category_3.",".$item_category_4.",".$item_category_5.",".$item_template.",".$item_custom_id.",".$item_bedrooms.",".$item_bedsize.",".$item_bathrooms.",".$item_sleeps.",".$newItemsString;
                        foreach ($array_fields2 as $field) 
                            $this_item_line .= ",".${"item_".$field};
                        $this_item_line .= "\n";
                
                    }
    
                    if ($item_title) {
                        if (fwrite($handle, $this_item_line) === false) {
                            export_progress($filename, system_showText(LANG_SITEMGR_EXPORT_ERRORNUMBER)." 20008<br />".system_showText(LANG_SITEMGR_EXPORT_CONTACTSUPPORT), true, $item_scalability, $tableCron, $dbObj, $id);
                            exit;
                        }
                    }
    
                    //self::export_progress($filename, floor($i/$item_amount*100));
    
                    $i++;
                    
                    /*
                     * Save item_id to next cicle of cron
                     */
                    if($item_scalability == "on"){
                        unset($sql_settings_update);
                        $sql_settings_update = "UPDATE $tableCron SET
                                                        $field2 = ".$item_id.", 
                                                        $field1 = $field1+1
                                                    WHERE domain_id = ".$id." AND type = 'csv'";
                        $dbObj->query($sql_settings_update);
                    }
                    
    
                }
                
                /*
                 * Change finished field if exported all itens
                 */
                if($item_scalability == "on"){
                    if($i >= $item_amount){
                        $sql_finished = "UPDATE $tableCron SET finished = 'Y', scheduled = 'N' WHERE domain_id =".$id." AND type = 'csv'";
                        $dbObj->query($sql_finished);
                        export_progress($filename, 100);
                    }
                
                    /*
                     * Save that this cicle finished
                     */
                    $sql_cicle = "UPDATE $tableCron SET running_cron = 'N' WHERE domain_id = ".$id." AND type = 'csv'";
                    $dbObj->query($sql_cicle);
                    
                }else{
                    if ($i < $item_amount || $item_amount == 0) {
                        export_progress($filename, 100);
                    }
                }
    
            } else {
                export_progress($filename, system_showText(LANG_SITEMGR_EXPORT_ERRORNUMBER)." 20007<br />".system_showText(LANG_SITEMGR_EXPORT_CONTACTSUPPORT), true, $item_scalability, $tableCron, $dbObj, $id);
                exit;
            }
    
            if($handle){
                if (!fclose($handle)) {
                    export_progress($filename, system_showText(LANG_SITEMGR_EXPORT_ERRORNUMBER)." 20003<br />".system_showText(LANG_SITEMGR_EXPORT_CONTACTSUPPORT), true, $item_scalability, $tableCron, $dbObj, $id);
                    exit;
                }
            }   
        }
        
    }
    public static function html_listingNewCheckboxes($listing, $table) {
   
        $count = 0;
        $html = "";

        if (config('params.DEMO_DEV_MODE')) {
            $sql = "SHOW columns FROM Listing WHERE Field LIKE '$table\_%'";
        } else {
            $sql = "SELECT DISTINCT COLUMN_NAME AS Field FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Listing' AND COLUMN_NAME LIKE '$table\_%' ORDER BY COLUMN_NAME";   
        }
        $result = self::fetch($sql);

        $html .= "<table class='standard-table'>";
        $html .= "<tr><th colspan='6' class='standard-tabletitle'>".config('params.LANG_LABEL_'.strtoupper($table).'_PLURAL')."</th></tr>";
        foreach ($result as $row) {         
            if (($count % 3) == 0)
                $html .= "<tr>";
            $html .= "<th><input type='checkbox' name='".$row->Field."' id='".$row->Field."' value='y' class='inputCheck' ";
            if ($listing->$row->Field == "y") $html .= "checked />";
            else $html .= "/>";
            $html .= "</th>";

            $html .= "<td><label for='".$row->Field."'>".config('params.LANG_LABEL_'.strtoupper($row->Field))."</label></td>";
            $count++;
            if (($count % 3) == 0 && $count >= 3)               
                $html .= "</tr>";
            
        }
        $html .= "</table>";
        echo ($html);


    }
    public static function fetch($sql)
    {
        $data= DB::connection('domain')->select($sql);
        return $data;
    }
    
    public static function fetchMain($sql)
    {
        $data= DB::select($sql);
        return $data;
    }
    
}