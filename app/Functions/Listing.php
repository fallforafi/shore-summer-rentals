<?php
namespace App\Functions;
use DB;
use App\Functions\Gallery;
use App\Functions\ListingCategory;
use App\Functions\Listing_Category;
use App\Models\Domain\Sql;
use App\Functions\Review;
use App\Functions\Account_Domain;
use App\Functions\Location1;
 use App\Functions\Location2;
 use App\Functions\Location3;
 use App\Functions\Location4;
 use App\Functions\Location5;
use App\Functions\ListingSummary;
use App\Functions\Setting;
use App\Functions\Promotion;
use App\Functions\DiscountCode;
use Session;




	/*==================================================================*\
	######################################################################
	#                                                                    #
	# Copyright 2005 Arca Solutions, Inc. All Rights Reserved.           #
	#                                                                    #
	# This file may not be redistributed in whole or part.               #
	# eDirectory is licensed on a per-domain basis.                      #
	#                                                                    #
	# ---------------- eDirectory IS NOT FREE SOFTWARE ----------------- #
	#                                                                    #
	# http://www.edirectory.com | http://www.edirectory.com/license.html #
	######################################################################
	\*==================================================================*/

	# ----------------------------------------------------------------------------------------------------
	# * FILE: /classes/class_listing.php
	# ----------------------------------------------------------------------------------------------------

	/**
	 * <code>
	 *		$listingObj = new Listing($id);
	 * <code>
	 * @copyright Copyright 2005 Arca Solutions, Inc.
	 * @author Arca Solutions, Inc.
	 * @version 8.0.00
	 * @package Classes
	 * @name Listing
	 * @method Listing
	 * @method makeFromRow
	 * @method Save
	 * @method Delete
	 * @method updateImage
	 * @method getCategories
	 * @method setCategories
	 * @method updateCategoryStatusByID
	 * @method retrieveListingsbyPromotion_id
	 * @method getPrice
	 * @method hasRenewalDate
	 * @method needToCheckOut
	 * @method getNextRenewalDate
	 * @method setLocationManager
	 * @method getLocationManager
	 * @method getLocationString
	 * @method setFullTextSearch
	 * @method getGalleries
	 * @method setGalleries
	 * @method setMapTuning
	 * @method setNumberViews
	 * @method setAvgReview
	 * @method hasDetail
	 * @method deletePerAccount
	 * @method SaveToFeaturedTemp
	 * @method removePromotionID
	 * @method getListingByFriendlyURL
	 * @method getListingToApp
	 * @method GetInfoToApp
	 * @access Public
	 */
	class Listing extends Handle{

		/**
		 * @var integer
		 * @access Private
		 */
		var $id;
		/**
		 * @var integer
		 * @access Private
		 */
		var $account_id;
		/**
		 * @var integer
		 * @access Private
		 */
		var $image_id;
		/**
		 * @var integer
		 * @access Private
		 */
		var $thumb_id;
		/**
		 * @var integer
		 * @access Private
		 */
		var $promotion_id;
		/**
		 * @var integer
		 * @access Private
		 */
		var $location_1;
		/**
		 * @var integer
		 * @access Private
		 */
		var $location_2;
		/**
		 * @var integer
		 * @access Private
		 */
		var $location_3;
		/**
		 * @var integer
		 * @access Private
		 */
		var $location_4;
		/**
		 * @var integer
		 * @access Private
		 */
		var $location_5;
		/**
		 * @var date
		 * @access Private
		 */
		var $renewal_date;
		/**
		 * @var integer
		 * @access Private
		 */
		var $discount_id;
		/**
		 * @var integer
		 * @access Private
		 */
		var $reminder;
		/**
		 * @var date
		 * @access Private
		 */
		var $updated;
		/**
		 * @var date
		 * @access Private
		 */
		var $entered;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $title;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $seo_title;
		/**
		 * @var char
		 * @access Private
		 */
		var $claim_disable;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $friendly_url;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $email;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $url;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $display_url;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $address;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $address2;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $zip_code;
        /**
		 * @var varchar
		 * @access Private
		 */
		var $zip5;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $phone;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $fax;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $description;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $seo_description;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $long_description;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $video_snippet;
        /**
		 * @var varchar
		 * @access Private
		 */
		var $video_description;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $keywords;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $seo_keywords;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $attachment_file;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $attachment_caption;
        /**
		 * @var varchar
		 * @access Private
		 */
		var $features;
        /**
		 * @var integer
		 * @access Private
		 */
		var $price;
        /**
		 * @var varchar
		 * @access Private
		 */
		var $facebook_page;
		/**
		 * @var char
		 * @access Private
		 */
		var $status;
        /**
		 * @var char
		 * @access Private
		 */
		var $suspended_sitemgr;
		/**
		 * @var integer
		 * @access Private
		 */
		var $level;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $locations;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $hours_work;
		/**
		 * @var integer
		 * @access Private
		 */
		var $listingtemplate_id;
        /**
		 * @var varchar
		 * @access Private
		 */
		var $custom_text0;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_text1;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_text2;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_text3;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_text4;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_text5;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_text6;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_text7;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_text8;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_text9;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_short_desc0;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_short_desc1;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_short_desc2;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_short_desc3;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_short_desc4;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_short_desc5;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_short_desc6;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_short_desc7;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_short_desc8;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_short_desc9;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_long_desc0;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_long_desc1;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_long_desc2;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_long_desc3;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_long_desc4;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_long_desc5;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_long_desc6;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_long_desc7;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_long_desc8;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_long_desc9;
        /**
		 * @var char
		 * @access Private
		 */
		var $custom_checkbox0;
		/**
		 * @var char
		 * @access Private
		 */
		var $custom_checkbox1;
		/**
		 * @var char
		 * @access Private
		 */
		var $custom_checkbox2;
		/**
		 * @var char
		 * @access Private
		 */
		var $custom_checkbox3;
		/**
		 * @var char
		 * @access Private
		 */
		var $custom_checkbox4;
		/**
		 * @var char
		 * @access Private
		 */
		var $custom_checkbox5;
		/**
		 * @var char
		 * @access Private
		 */
		var $custom_checkbox6;
		/**
		 * @var char
		 * @access Private
		 */
		var $custom_checkbox7;
		/**
		 * @var char
		 * @access Private
		 */
		var $custom_checkbox8;
		/**
		 * @var char
		 * @access Private
		 */
		var $custom_checkbox9;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_dropdown0;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_dropdown1;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_dropdown2;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_dropdown3;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_dropdown4;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_dropdown5;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_dropdown6;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_dropdown7;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_dropdown8;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_dropdown9;
		/**
		 * @var integer
		 * @access Private
		 */
		var $number_views;
		/**
		 * @var integer
		 * @access Private
		 */
		var $avg_review;
        /*
         * @var real
         * @access Private
         */
        var $latitude;
        /*
         * @var real
         * @access Private
         */
        var $longitude;
		/**
		 * @var integer
		 * @access Private
		 */
		var $map_zoom;

		/**
		 * @var mixed
		 * @access Private
		 */
		var $locationManager;

		/**
		 * @var array
		 * @access Private
		 */
		var $data_in_array;
		/**
		 * @var integer
		 * @access Private
		 */
		var $domain_id;
		/**
		 * @var integer
		 * @access Private
		 */
		var $package_id;
		/**
		 * @var integer
		 * @access Private
		 */
		var $package_price;
		/**
		 * @var char
		 * @access Private
		 */
		var $backlink;
        /**
		 * @var string
		 * @access Private
		 */
		var $backlink_url;
		/**
		 * @var integer
		 * @access Private
		 */
		var $clicktocall_number;
		/**
		 * @var integer
		 * @access Private
		 */
		var $clicktocall_extension;
		/**
		 * @var date
		 * @access Private
		 */
		var $clicktocall_date;
		var $activation_date;
		/**
		 * @var integer
		 * @access Private
		 */
		var $bedroom;
		var $bedsize;
		var $sleeps;
		var $bathroom;
		var $property_type;
		var $view;
		var $distance_beach;
		var $development_name;
        var $development_id;
        

		var $airport_distance;
		var $airport_abbreviation;

		var $contact_fname;
		var $contact_lname;

		var $external_link1;
		var $external_link_text1;
		var $external_link2;
		var $external_link_text2;
		var $external_link3;
		var $external_link_text3;
		/**
		 * @var char
		 * @access Private
		 */
		var $amenity_airhockey;
		var $amenity_alarmclock;
		var $amenity_answeringmachine;
		var $amenity_arcadegames;
		var $amenity_billiards;
		var $amenity_blender;
		var $amenity_blurayplayer;
		var $amenity_books;
		var $amenity_casetteplayer;
		var $amenity_cdplayer;
		var $amenity_ceilingfan;
		var $amenity_childshighchair;
		var $amenity_coffeemaker;
		var $amenity_communalpool;
		var $amenity_computer;
		var $amenity_cookware;
		var $amenity_cookingrange;
		var $amenity_deckfurniture;
		var $amenity_dishwasher;
		var $amenity_dishes;
		var $amenity_dvdplayer;
		var $amenity_exercisefacilities;
		var $amenity_foosball;
		var $amenity_gametable;
		var $amenity_games;
		var $amenity_grill;
		var $amenity_hairdryer;
		var $amenity_icemaker;
		var $amenity_internet;
		var $amenity_ironandboard;
		var $amenity_kidsgames;
		var $amenity_linensprovided;
		var $amenity_lobsterpot;
		var $amenity_microwave;
		var $amenity_minirefrigerator;
		var $amenity_mp3radiodock;
		var $amenity_outsideshower;
		var $amenity_oven;
		var $amenity_pinball;
		var $amenity_pingpong;
		var $amenity_privatepool;
		var $amenity_radio;
		var $amenity_refrigeratorfreezer;
		var $amenity_sofabed;
		var $amenity_stereo;
		var $amenity_telephone;
		var $amenity_television;
		var $amenity_toaster;
		var $amenity_toasteroven;
		var $amenity_towelsprovided;
		var $amenity_toys;
		var $amenity_utensils;
		var $amenity_vacuum;
		var $amenity_vcr;
		var $amenity_videogameconsole;
		var $amenity_videogames;
		var $amenity_washerdryer;
		var $amenity_wifi;
		var $feature_airconditioning;
		var $feature_balcony;
		var $feature_barbecuecharcoal;
		var $feature_barbecuegas;
		var $feature_boatslip;
		var $feature_cablesatellitetv;
		var $feature_clubhouse;
		var $feature_coveredparking;
		var $feature_deck;
		var $feature_diningroom;
		var $feature_elevator;
		var $feature_familyroom;
		var $feature_fullkitchen;
		var $feature_garage;
		var $feature_gasfireplace;
		var $feature_gatedcommunity;
		var $feature_wheelchairaccess;
		var $feature_heated;
		var $feature_heatedpool;
		var $feature_hottubjacuzzi;
		var $feature_internetaccess;
		var $feature_kitchenette;
		var $feature_livingroom;
		var $feature_loft;
		var $feature_onsitesecurity;
		var $feature_patio;
		var $feature_petsallowed;
		var $feature_playroom;
		var $feature_pool;
		var $feature_porch;
		var $feature_rooftopdeck;
		var $feature_sauna;
		var $feature_smokingpermitted;
		var $feature_woodfireplace;
		var $activity_antiquing;
		var $activity_basketballcourt;
		var $activity_beachcombing;
		var $activity_bicycling;
		var $activity_bikerentals;
		var $activity_birdwatching;
		var $activity_boatrentals;
		var $activity_boating;
		var $activity_botanicalgarden;
		var $activity_canoe;
		var $activity_churches;
		var $activity_cinemas;
		var $activity_bikesprovided;
		var $activity_deepseafishing;
		var $activity_fishing;
		var $activity_fitnesscenter;
		var $activity_golf;
		var $activity_healthbeautyspa;
		var $activity_hiking;
		var $activity_horsebackriding;
		var $activity_horseshoes;
		var $activity_hotairballooning;
		var $activity_iceskating;
		var $activity_jetskiing;
		var $activity_kayaking;
		var $activity_livetheater;
		var $activity_marina;
		var $activity_miniaturegolf;
		var $activity_mountainbiking;
		var $activity_museums;
		var $activity_paddleboating;
		var $activity_paragliding;
		var $activity_parasailing;
		var $activity_playground;
		var $activity_recreationcenter;
		var $activity_restaurants;
		var $activity_rollerblading;
		var $activity_sailing;
		var $activity_shelling;
		var $activity_shopping;
		var $activity_sightseeing;
		var $activity_skiing;
		var $activity_bayfishing;
		var $activity_spa;
		var $activity_surffishing;
		var $activity_surfing;
		var $activity_swimming;
		var $activity_tennis;
		var $activity_themeparks;
		var $activity_walking;
		var $activity_waterparks;
		var $activity_waterskiing;
		var $activity_watertubing;
		var $activity_wildlifeviewing;
		var $activity_zoo;
		var $isppilisting;
		var $deposit_amount;
		var $tax_per_inquiry;
		var $unique_email;
		/**
		 * @var string
		 * @access Private
		 */
		var $is_featured;
		/**
		 * @var date
		 * @access Private
		 */
		var $featured_date;
		/**
		 * @var integer
		 * @access Private
		 */
		var $search_pos;
		/**
		 * @var date
		 * @access Private
		 */
		var $search_pos_date;
		var $required_stay;

		

		/**
		 * <code>
		 *		$listingObj = new Listing($id);
		 *		//OR
		 *		$listingObj = new Listing($row);
		 * <code>
		 *
		 * @name Listing
		 * @access Public
		 * @param mixed $var
		 */
		function Listing($var='') {

			if (is_numeric($var) && $var!='') {//

				$sql = "SELECT * FROM Listing WHERE id = $var";					
				$row = self::fetch($sql);


                if(!empty($row))
                {

                    $this->old_account_id = $row[0]->account_id;

				    $this->makeFromRow($row);


                }
                else
                {
                    return false;
                }

				
			} else {

                if (!is_array($var)) {
                    $var = array();
                }
				$this->arrayMakeFromRow($var);
			}



		}

		/**
		 * <code>
		 *		$this->makeFromRow($row);
		 * <code>
		 
		 * @name makeFromRow
		 * @access Public
		 * @param array $row
		 */
		function makeFromRow($row='') {

			$status = new ItemStatus();
			$status->ItemStatus();
			$level = new ListingLevel();
			$level->ListingLevel();
			$status = $status->getDefaultStatus();
			$level = 'P';

			$this->id					= ($row[0]->id)					? $row[0]->id					: ($this->id				? $this->id					: 0);
			$this->account_id			= ($row[0]->account_id)			? $row[0]->account_id			: 0;
			$this->image_id				= isset($row[0]->image_id)			? $row[0]->image_id				: ($this->image_id			? $this->image_id			: 0);
			$this->thumb_id				= ($row[0]->thumb_id)			? $row[0]->thumb_id				: ($this->thumb_id			? $this->thumb_id			: 0);
			$this->promotion_id			= ($row[0]->promotion_id)		? $row[0]->promotion_id			: ($this->promotion_id		? $this->promotion_id		: 0);
			$this->location_1			= ($row[0]->location_1)			? $row[0]->location_1			: 0;
			$this->location_2			= ($row[0]->location_2)			? $row[0]->location_2			: 0;
			$this->location_3			= ($row[0]->location_3)			? $row[0]->location_3			: 0;
			$this->location_4			= ($row[0]->location_4)			? $row[0]->location_4			: 0;
			$this->location_5			= ($row[0]->location_5)			? $row[0]->location_5			: 0;
			$this->renewal_date			= ($row[0]->renewal_date)		? $row[0]->renewal_date			: ($this->renewal_date		? $this->renewal_date		: 0);
			$this->discount_id			= ($row[0]->discount_id)			? $row[0]->discount_id			: "";
			$this->reminder				= ($row[0]->reminder)			? $row[0]->reminder				: ($this->reminder			? $this->reminder			: 0);
			$this->entered				= ($row[0]->entered)				? $row[0]->entered				: ($this->entered			? $this->entered			: "");
			$this->updated				= ($row[0]->updated)				? $row[0]->updated				: ($this->updated			? $this->updated			: "");
			$this->title				= ($row[0]->title)				? $row[0]->title					: ($this->title				? $this->title				: "");
			$this->seo_title			= ($row[0]->seo_title)			? $row[0]->seo_title				: ($this->seo_title			? $this->seo_title			: "");
			$this->claim_disable		= ($row[0]->claim_disable)		? $row[0]->claim_disable			: "n";
			$this->friendly_url			= ($row[0]->friendly_url)		? $row[0]->friendly_url			: "";
			$this->email				= ($row[0]->email)				? $row[0]->email					: "";
			$this->url					= ($row[0]->url)					? $row[0]->url					: "";
			$this->display_url			= ($row[0]->display_url)			? $row[0]->display_url			: "";
			$this->address				= ($row[0]->address)				? $row[0]->address				: "";
			$this->address2				= ($row[0]->address2)			? $row[0]->address2				: "";
			$this->zip_code				= ($row[0]->zip_code)			? $row[0]->zip_code				: "";
			$this->zip5                 = ($row[0]->zip5)                ? $row[0]->zip5                  : "";
			$this->phone				= ($row[0]->phone)				? $row[0]->phone					: "";
			$this->fax					= ($row[0]->fax)					? $row[0]->fax					: "";
			$this->description			= ($row[0]->description)         ? $row[0]->description			: "";
			$this->seo_description		= ($row[0]->seo_description)     ? $row[0]->seo_description		: ($this->seo_description	? $this->seo_description	: "");
			$this->long_description     = ($row[0]->long_description)	? $row[0]->long_description		: "";
			$this->video_snippet		= ($row[0]->video_snippet)		? $row[0]->video_snippet			: "";
			$this->video_description	= ($row[0]->video_description)	? $row[0]->video_description		: "";
			$this->keywords             = ($row[0]->keywords)			? $row[0]->keywords				: "";
			$this->seo_keywords         = ($row[0]->seo_keywords)		? $row[0]->seo_keywords			: ($this->seo_keywords		? $this->seo_keywords		: "");
			$this->attachment_file		= ($row[0]->attachment_file)		? $row[0]->attachment_file		: ($this->attachment_file	? $this->attachment_file	: "");
			$this->attachment_caption	= ($row[0]->attachment_caption)	? $row[0]->attachment_caption	: "";
			$this->features             = ($row[0]->features)            ? $row[0]->features              : "";
			$this->price                = ($row[0]->price)               ? $row[0]->price                 : ($this->price		? $this->price		: "");
			$this->facebook_page        = ($row[0]->facebook_page)       ? $row[0]->facebook_page         : "";
			$this->status				= ($row[0]->status)				? $row[0]->status				: $status;
			$this->suspended_sitemgr	= ($row[0]->suspended_sitemgr)   ? $row[0]->suspended_sitemgr		: ($this->suspended_sitemgr		? $this->suspended_sitemgr		: "n");
			$this->level				= ($row[0]->level)				? $row[0]->level					: ($this->level				? $this->level				: 'P');
			$this->hours_work			= ($row[0]->hours_work)			? $row[0]->hours_work			: "";
			$this->locations			= ($row[0]->locations)			? $row[0]->locations				: "";
			$this->latitude             = ($row[0]->latitude)			? $row[0]->latitude				: ($this->latitude		? $this->latitude		: "");
			$this->longitude			= ($row[0]->longitude)			? $row[0]->longitude				: ($this->longitude		? $this->longitude		: "");
			$this->map_zoom             = ($row[0]->map_zoom)            ? $row[0]->map_zoom              : 0;
			$this->listingtemplate_id	= ($row[0]->listingtemplate_id)	? $row[0]->listingtemplate_id	: 0;

            $this->custom_text0			= ($row[0]->custom_text0)		? $row[0]->custom_text0			: "";
			$this->custom_text1			= ($row[0]->custom_text1)		? $row[0]->custom_text1			: "";
			$this->custom_text2			= ($row[0]->custom_text2)		? $row[0]->custom_text2			: "";
			$this->custom_text3			= ($row[0]->custom_text3)		? $row[0]->custom_text3			: "";
			$this->custom_text4			= ($row[0]->custom_text4)		? $row[0]->custom_text4			: "";
			$this->custom_text5			= ($row[0]->custom_text5)		? $row[0]->custom_text5			: "";
			$this->custom_text6			= ($row[0]->custom_text6)		? $row[0]->custom_text6			: "";
			$this->custom_text7			= ($row[0]->custom_text7)		? $row[0]->custom_text7			: "";
			$this->custom_text8			= ($row[0]->custom_text8)		? $row[0]->custom_text8			: "";
			$this->custom_text9			= ($row[0]->custom_text9)		? $row[0]->custom_text9			: "";
			$this->custom_short_desc0	= ($row[0]->custom_short_desc0)	? $row[0]->custom_short_desc0	: "";
			$this->custom_short_desc1	= ($row[0]->custom_short_desc1)	? $row[0]->custom_short_desc1	: "";
			$this->custom_short_desc2	= ($row[0]->custom_short_desc2)	? $row[0]->custom_short_desc2	: "";
			$this->custom_short_desc3	= ($row[0]->custom_short_desc3)	? $row[0]->custom_short_desc3	: "";
			$this->custom_short_desc4	= ($row[0]->custom_short_desc4)	? $row[0]->custom_short_desc4	: "";
			$this->custom_short_desc5	= ($row[0]->custom_short_desc5)	? $row[0]->custom_short_desc5	: "";
			$this->custom_short_desc6	= ($row[0]->custom_short_desc6)	? $row[0]->custom_short_desc6	: "";
			$this->custom_short_desc7	= ($row[0]->custom_short_desc7)	? $row[0]->custom_short_desc7	: "";
			$this->custom_short_desc8	= ($row[0]->custom_short_desc8)	? $row[0]->custom_short_desc8	: "";
			$this->custom_short_desc9	= ($row[0]->custom_short_desc9)	? $row[0]->custom_short_desc9	: "";
			$this->custom_long_desc0	= ($row[0]->custom_long_desc0)	? $row[0]->custom_long_desc0		: "";
			$this->custom_long_desc1	= ($row[0]->custom_long_desc1)	? $row[0]->custom_long_desc1		: "";
			$this->custom_long_desc2	= ($row[0]->custom_long_desc2)	? $row[0]->custom_long_desc2		: "";
			$this->custom_long_desc3	= ($row[0]->custom_long_desc3)	? $row[0]->custom_long_desc3		: "";
			$this->custom_long_desc4	= ($row[0]->custom_long_desc4)	? $row[0]->custom_long_desc4		: "";
			$this->custom_long_desc5	= ($row[0]->custom_long_desc5)	? $row[0]->custom_long_desc5		: "";
			$this->custom_long_desc6	= ($row[0]->custom_long_desc6)	? $row[0]->custom_long_desc6		: "";
			$this->custom_long_desc7	= ($row[0]->custom_long_desc7)	? $row[0]->custom_long_desc7		: "";
			$this->custom_long_desc8	= ($row[0]->custom_long_desc8)	? $row[0]->custom_long_desc8		: "";
			$this->custom_long_desc9	= ($row[0]->custom_long_desc9)	? $row[0]->custom_long_desc9		: "";
            $this->custom_checkbox0		= ($row[0]->custom_checkbox0)	? $row[0]->custom_checkbox0		: "n";
			$this->custom_checkbox1		= ($row[0]->custom_checkbox1)	? $row[0]->custom_checkbox1		: "n";
			$this->custom_checkbox2		= ($row[0]->custom_checkbox2)	? $row[0]->custom_checkbox2		: "n";
			$this->custom_checkbox3		= ($row[0]->custom_checkbox3)	? $row[0]->custom_checkbox3		: "n";
			$this->custom_checkbox4		= ($row[0]->custom_checkbox4)	? $row[0]->custom_checkbox4		: "n";
			$this->custom_checkbox5		= ($row[0]->custom_checkbox5)	? $row[0]->custom_checkbox5		: "n";
			$this->custom_checkbox6		= ($row[0]->custom_checkbox6)	? $row[0]->custom_checkbox6		: "n";
			$this->custom_checkbox7		= ($row[0]->custom_checkbox7)	? $row[0]->custom_checkbox7		: "n";
			$this->custom_checkbox8		= ($row[0]->custom_checkbox8)	? $row[0]->custom_checkbox8		: "n";
			$this->custom_checkbox9		= ($row[0]->custom_checkbox9)	? $row[0]->custom_checkbox9		: "n";
			$this->custom_dropdown0		= ($row[0]->custom_dropdown0)	? $row[0]->custom_dropdown0		: "";
			$this->custom_dropdown1		= ($row[0]->custom_dropdown1)	? $row[0]->custom_dropdown1		: "";
			$this->custom_dropdown2		= ($row[0]->custom_dropdown2)	? $row[0]->custom_dropdown2		: "";
			$this->custom_dropdown3		= ($row[0]->custom_dropdown3)	? $row[0]->custom_dropdown3		: "";
			$this->custom_dropdown4		= ($row[0]->custom_dropdown4)	? $row[0]->custom_dropdown4		: "";
			$this->custom_dropdown5		= ($row[0]->custom_dropdown5)	? $row[0]->custom_dropdown5		: "";
			$this->custom_dropdown6		= ($row[0]->custom_dropdown6)	? $row[0]->custom_dropdown6		: "";
			$this->custom_dropdown7		= ($row[0]->custom_dropdown7)	? $row[0]->custom_dropdown7		: "";
			$this->custom_dropdown8		= ($row[0]->custom_dropdown8)	? $row[0]->custom_dropdown8		: "";
			$this->custom_dropdown9		= ($row[0]->custom_dropdown9)	? $row[0]->custom_dropdown9		: "";

			$this->number_views			= ($row[0]->number_views)		? $row[0]->number_views			: ($this->number_views		? $this->number_views	: 0);
			$this->avg_review			= ($row[0]->avg_review)			? $row[0]->avg_review			: ($this->avg_review		? $this->avg_review		: 0);
			$this->package_id			= ($row[0]->package_id)			? $row[0]->package_id			: ($this->package_id			? $this->package_id				: 0);
			$this->package_price		= ($row[0]->package_price)		? $row[0]->package_price			: ($this->package_price			? $this->package_price			: 0);
			$this->backlink				= ($row[0]->backlink)			? $row[0]->backlink				: ($this->backlink				? $this->backlink				: "n");
            $this->backlink_url				= ($row[0]->backlink_url)			? $row[0]->backlink_url				: ($this->backlink_url				? $this->backlink_url				: "");
            $this->clicktocall_number		= ($row[0]->clicktocall_number)		? $row[0]->clicktocall_number	: ($this->clicktocall_number	? $this->clicktocall_number			: "");
            $this->clicktocall_extension	= ($row[0]->clicktocall_extension)	? $row[0]->clicktocall_extension	: ($this->clicktocall_extension	? $this->clicktocall_extension		: 0);
            $this->clicktocall_date			= ($row[0]->clicktocall_date)		? $row[0]->clicktocall_date		: ($this->clicktocall_date		? $this->clicktocall_date			: "");
            $this->activation_date			= ($row[0]->activation_date)			? $row[0]->activation_date		: ($this->activation_date		? $this->activation_date			: "");
            $this->bedroom			= ($row[0]->bedroom)			? $row[0]->bedroom		: "";
            $this->bedsize			= ($row[0]->bedsize)			? $row[0]->bedsize		: "";
            $this->sleeps			= ($row[0]->sleeps)			? $row[0]->sleeps		: "";
            $this->bathroom			= ($row[0]->bathroom)		? $row[0]->bathroom		: "";
            $this->property_type			= ($row[0]->property_type)		? $row[0]->property_type		: "";
            $this->view						= ($row[0]->view)				? $row[0]->view				: ($this->view				? $this->view				: "");
            $this->distance_beach			= ($row[0]->distance_beach)		? $row[0]->distance_beach	: ($this->distance_beach	? $this->distance_beach		: "");
            $this->development_name			= ($row[0]->development_name)		? $row[0]->development_name		: "";
            
            $this->development_id			= ($row[0]->development_id)		? $row[0]->development_id		: "";
            
            $this->airport_distance			= ($row[0]->airport_distance)		? $row[0]->airport_distance		: "";
            $this->airport_abbreviation			= ($row[0]->airport_abbreviation)		? $row[0]->airport_abbreviation		: "";

            $this->contact_fname			= ($row[0]->contact_fname)		? $row[0]->contact_fname		: "";
            $this->contact_lname			= ($row[0]->contact_lname)		? $row[0]->contact_lname		: "";

            $this->external_link1			= ($row[0]->external_link1)		? $row[0]->external_link1		: "";
            $this->external_link_text1			= ($row[0]->external_link_text1)		? $row[0]->external_link_text1		: "";
            $this->external_link2			= ($row[0]->external_link2)		? $row[0]->external_link2		: "";
            $this->external_link_text2			= ($row[0]->external_link_text2)		? $row[0]->external_link_text2		: "";
            $this->external_link3			= ($row[0]->external_link3)		? $row[0]->external_link3		: "";
            $this->external_link_text3			= ($row[0]->external_link_text3)		? $row[0]->external_link_text3		: "";

            $this->amenity_airhockey = ($row[0]->amenity_airhockey)	? $row[0]->amenity_airhockey	: "n";
			$this->amenity_alarmclock = ($row[0]->amenity_alarmclock)	? $row[0]->amenity_alarmclock	: "n";
			$this->amenity_answeringmachine = ($row[0]->amenity_answeringmachine)	? $row[0]->amenity_answeringmachine	: "n";
			$this->amenity_arcadegames = ($row[0]->amenity_arcadegames)	? $row[0]->amenity_arcadegames	: "n";
			$this->amenity_billiards = ($row[0]->amenity_billiards)	? $row[0]->amenity_billiards	: "n";
			$this->amenity_blender = ($row[0]->amenity_blender)	? $row[0]->amenity_blender	: "n";
			$this->amenity_blurayplayer = ($row[0]->amenity_blurayplayer)	? $row[0]->amenity_blurayplayer	: "n";
			$this->amenity_books = ($row[0]->amenity_books)	? $row[0]->amenity_books	: "n";
			$this->amenity_casetteplayer = ($row[0]->amenity_casetteplayer)	? $row[0]->amenity_casetteplayer	: "n";
			$this->amenity_cdplayer = ($row[0]->amenity_cdplayer)	? $row[0]->amenity_cdplayer	: "n";
			$this->amenity_ceilingfan = ($row[0]->amenity_ceilingfan)	? $row[0]->amenity_ceilingfan	: "n";
			$this->amenity_childshighchair = ($row[0]->amenity_childshighchair)	? $row[0]->amenity_childshighchair	: "n";
			$this->amenity_coffeemaker = ($row[0]->amenity_coffeemaker)	? $row[0]->amenity_coffeemaker	: "n";
			$this->amenity_communalpool = ($row[0]->amenity_communalpool)	? $row[0]->amenity_communalpool	: "n";
			$this->amenity_computer = ($row[0]->amenity_computer)	? $row[0]->amenity_computer	: "n";
			$this->amenity_cookware = ($row[0]->amenity_cookware)	? $row[0]->amenity_cookware	: "n";
			$this->amenity_cookingrange = ($row[0]->amenity_cookingrange)	? $row[0]->amenity_cookingrange	: "n";
			$this->amenity_deckfurniture = ($row[0]->amenity_deckfurniture)	? $row[0]->amenity_deckfurniture	: "n";
			$this->amenity_dishwasher = ($row[0]->amenity_dishwasher)	? $row[0]->amenity_dishwasher	: "n";
			$this->amenity_dishes = ($row[0]->amenity_dishes)	? $row[0]->amenity_dishes	: "n";
			$this->amenity_dvdplayer = ($row[0]->amenity_dvdplayer)	? $row[0]->amenity_dvdplayer	: "n";
			$this->amenity_exercisefacilities = ($row[0]->amenity_exercisefacilities)	? $row[0]->amenity_exercisefacilities	: "n";
			$this->amenity_foosball = ($row[0]->amenity_foosball)	? $row[0]->amenity_foosball	: "n";
			$this->amenity_gametable = ($row[0]->amenity_gametable)	? $row[0]->amenity_gametable	: "n";
			$this->amenity_games = ($row[0]->amenity_games)	? $row[0]->amenity_games	: "n";
			$this->amenity_grill = ($row[0]->amenity_grill)	? $row[0]->amenity_grill	: "n";
			$this->amenity_hairdryer = ($row[0]->amenity_hairdryer)	? $row[0]->amenity_hairdryer	: "n";
			$this->amenity_icemaker = ($row[0]->amenity_icemaker)	? $row[0]->amenity_icemaker	: "n";
			$this->amenity_internet = ($row[0]->amenity_internet)	? $row[0]->amenity_internet	: "n";
			$this->amenity_ironandboard = ($row[0]->amenity_ironandboard)	? $row[0]->amenity_ironandboard	: "n";
			$this->amenity_kidsgames = ($row[0]->amenity_kidsgames)	? $row[0]->amenity_kidsgames	: "n";
			$this->amenity_linensprovided = ($row[0]->amenity_linensprovided)	? $row[0]->amenity_linensprovided	: "n";
			$this->amenity_lobsterpot = ($row[0]->amenity_lobsterpot)	? $row[0]->amenity_lobsterpot	: "n";
			$this->amenity_microwave = ($row[0]->amenity_microwave)	? $row[0]->amenity_microwave	: "n";
			$this->amenity_minirefrigerator = ($row[0]->amenity_minirefrigerator)	? $row[0]->amenity_minirefrigerator	: "n";
			$this->amenity_mp3radiodock = ($row[0]->amenity_mp3radiodock)	? $row[0]->amenity_mp3radiodock	: "n";
			$this->amenity_outsideshower = ($row[0]->amenity_outsideshower)	? $row[0]->amenity_outsideshower	: "n";
			$this->amenity_oven = ($row[0]->amenity_oven)	? $row[0]->amenity_oven	: "n";
			$this->amenity_pinball = ($row[0]->amenity_pinball)	? $row[0]->amenity_pinball	: "n";
			$this->amenity_pingpong = ($row[0]->amenity_pingpong)	? $row[0]->amenity_pingpong	: "n";
			$this->amenity_privatepool = ($row[0]->amenity_privatepool)	? $row[0]->amenity_privatepool	: "n";
			$this->amenity_radio = ($row[0]->amenity_radio)	? $row[0]->amenity_radio	: "n";
			$this->amenity_refrigeratorfreezer = ($row[0]->amenity_refrigeratorfreezer)	? $row[0]->amenity_refrigeratorfreezer	: "n";
			$this->amenity_sofabed = ($row[0]->amenity_sofabed)	? $row[0]->amenity_sofabed	: "n";
			$this->amenity_stereo = ($row[0]->amenity_stereo)	? $row[0]->amenity_stereo	: "n";
			$this->amenity_telephone = ($row[0]->amenity_telephone)	? $row[0]->amenity_telephone	: "n";
			$this->amenity_television = ($row[0]->amenity_television)	? $row[0]->amenity_television	: "n";
			$this->amenity_toaster = ($row[0]->amenity_toaster)	? $row[0]->amenity_toaster	: "n";
			$this->amenity_toasteroven = ($row[0]->amenity_toasteroven)	? $row[0]->amenity_toasteroven	: "n";
			$this->amenity_towelsprovided = ($row[0]->amenity_towelsprovided)	? $row[0]->amenity_towelsprovided	: "n";
			$this->amenity_toys = ($row[0]->amenity_toys)	? $row[0]->amenity_toys	: "n";
			$this->amenity_utensils = ($row[0]->amenity_utensils)	? $row[0]->amenity_utensils	: "n";
			$this->amenity_vacuum = ($row[0]->amenity_vacuum)	? $row[0]->amenity_vacuum	: "n";
			$this->amenity_vcr = ($row[0]->amenity_vcr)	? $row[0]->amenity_vcr	: "n";
			$this->amenity_videogameconsole = ($row[0]->amenity_videogameconsole)	? $row[0]->amenity_videogameconsole	: "n";
			$this->amenity_videogames = ($row[0]->amenity_videogames)	? $row[0]->amenity_videogames	: "n";
			$this->amenity_washerdryer = ($row[0]->amenity_washerdryer)	? $row[0]->amenity_washerdryer	: "n";
			$this->amenity_wifi = ($row[0]->amenity_wifi)	? $row[0]->amenity_wifi	: "n";

			$this->feature_airconditioning = ($row[0]->feature_airconditioning)	? $row[0]->feature_airconditioning	: "n";
			$this->feature_balcony = ($row[0]->feature_balcony)	? $row[0]->feature_balcony	: "n";
			$this->feature_barbecuecharcoal = ($row[0]->feature_barbecuecharcoal)	? $row[0]->feature_barbecuecharcoal	: "n";
			$this->feature_barbecuegas = ($row[0]->feature_barbecuegas)	? $row[0]->feature_barbecuegas	: "n";
			$this->feature_boatslip = ($row[0]->feature_boatslip)	? $row[0]->feature_boatslip	: "n";
			$this->feature_cablesatellitetv = ($row[0]->feature_cablesatellitetv)	? $row[0]->feature_cablesatellitetv	: "n";
			$this->feature_clubhouse = ($row[0]->feature_clubhouse)	? $row[0]->feature_clubhouse	: "n";
			$this->feature_coveredparking = ($row[0]->feature_coveredparking)	? $row[0]->feature_coveredparking	: "n";
			$this->feature_deck = ($row[0]->feature_deck)	? $row[0]->feature_deck	: "n";
			$this->feature_diningroom = ($row[0]->feature_diningroom)	? $row[0]->feature_diningroom	: "n";
			$this->feature_elevator = ($row[0]->feature_elevator)	? $row[0]->feature_elevator	: "n";
			$this->feature_familyroom = ($row[0]->feature_familyroom)	? $row[0]->feature_familyroom	: "n";
			$this->feature_fullkitchen = ($row[0]->feature_fullkitchen)	? $row[0]->feature_fullkitchen	: "n";
			$this->feature_garage = ($row[0]->feature_garage)	? $row[0]->feature_garage	: "n";
			$this->feature_gasfireplace = ($row[0]->feature_gasfireplace)	? $row[0]->feature_gasfireplace	: "n";
			$this->feature_gatedcommunity = ($row[0]->feature_gatedcommunity)	? $row[0]->feature_gatedcommunity	: "n";
			$this->feature_wheelchairaccess = ($row[0]->feature_wheelchairaccess)	? $row[0]->feature_wheelchairaccess	: "n";
			$this->feature_heated = ($row[0]->feature_heated)	? $row[0]->feature_heated	: "n";
			$this->feature_heatedpool = ($row[0]->feature_heatedpool)	? $row[0]->feature_heatedpool	: "n";
			$this->feature_hottubjacuzzi = ($row[0]->feature_hottubjacuzzi)	? $row[0]->feature_hottubjacuzzi	: "n";
			$this->feature_internetaccess = ($row[0]->feature_internetaccess)	? $row[0]->feature_internetaccess	: "n";
			$this->feature_kitchenette = ($row[0]->feature_kitchenette)	? $row[0]->feature_kitchenette	: "n";
			$this->feature_livingroom = ($row[0]->feature_livingroom)	? $row[0]->feature_livingroom	: "n";
			$this->feature_loft = ($row[0]->feature_loft)	? $row[0]->feature_loft	: "n";
			$this->feature_onsitesecurity = ($row[0]->feature_onsitesecurity)	? $row[0]->feature_onsitesecurity	: "n";
			$this->feature_patio = ($row[0]->feature_patio)	? $row[0]->feature_patio	: "n";
			$this->feature_petsallowed = ($row[0]->feature_petsallowed)		? $row[0]->feature_petsallowed		: "n";
			$this->feature_playroom = ($row[0]->feature_playroom)	? $row[0]->feature_playroom	: "n";
			$this->feature_pool = ($row[0]->feature_pool)	? $row[0]->feature_pool	: "n";
			$this->feature_porch = ($row[0]->feature_porch)	? $row[0]->feature_porch	: "n";
			$this->feature_rooftopdeck = ($row[0]->feature_rooftopdeck)	? $row[0]->feature_rooftopdeck	: "n";
			$this->feature_sauna = ($row[0]->feature_sauna)	? $row[0]->feature_sauna	: "n";
			$this->feature_smokingpermitted = ($row[0]->feature_smokingpermitted)	? $row[0]->feature_smokingpermitted	: "n";
			$this->feature_woodfireplace = ($row[0]->feature_woodfireplace)	? $row[0]->feature_woodfireplace	: "n";
			
			$this->activity_antiquing = ($row[0]->activity_antiquing)			? $row[0]->activity_antiquing			: "n";
			$this->activity_basketballcourt = ($row[0]->activity_basketballcourt)			? $row[0]->activity_basketballcourt			: "n";
			$this->activity_beachcombing = ($row[0]->activity_beachcombing)			? $row[0]->activity_beachcombing			: "n";
			$this->activity_bicycling = ($row[0]->activity_bicycling)			? $row[0]->activity_bicycling			: "n";
			$this->activity_bikerentals = ($row[0]->activity_bikerentals)			? $row[0]->activity_bikerentals			: "n";
			$this->activity_birdwatching = ($row[0]->activity_birdwatching)			? $row[0]->activity_birdwatching			: "n";
			$this->activity_boatrentals = ($row[0]->activity_boatrentals)			? $row[0]->activity_boatrentals			: "n";
			$this->activity_boating = ($row[0]->activity_boating)			? $row[0]->activity_boating			: "n";
			$this->activity_botanicalgarden = ($row[0]->activity_botanicalgarden)			? $row[0]->activity_botanicalgarden			: "n";
			$this->activity_canoe = ($row[0]->activity_canoe)			? $row[0]->activity_canoe			: "n";
			$this->activity_churches = ($row[0]->activity_churches)			? $row[0]->activity_churches			: "n";
			$this->activity_cinemas = ($row[0]->activity_cinemas)			? $row[0]->activity_cinemas			: "n";
			$this->activity_bikesprovided = ($row[0]->activity_bikesprovided)			? $row[0]->activity_bikesprovided			: "n";
			$this->activity_deepseafishing = ($row[0]->activity_deepseafishing)			? $row[0]->activity_deepseafishing			: "n";
			$this->activity_fishing = ($row[0]->activity_fishing)			? $row[0]->activity_fishing			: "n";
			$this->activity_fitnesscenter = ($row[0]->activity_fitnesscenter)			? $row[0]->activity_fitnesscenter			: "n";
			$this->activity_golf = ($row[0]->activity_golf)			? $row[0]->activity_golf			: "n";
			$this->activity_healthbeautyspa = ($row[0]->activity_healthbeautyspa)			? $row[0]->activity_healthbeautyspa			: "n";
			$this->activity_hiking = ($row[0]->activity_hiking)			? $row[0]->activity_hiking			: "n";
			$this->activity_horsebackriding = ($row[0]->activity_horsebackriding)			? $row[0]->activity_horsebackriding			: "n";
			$this->activity_horseshoes = ($row[0]->activity_horseshoes)			? $row[0]->activity_horseshoes			: "n";
			$this->activity_hotairballooning = ($row[0]->activity_hotairballooning)			? $row[0]->activity_hotairballooning			: "n";
			$this->activity_iceskating = ($row[0]->activity_iceskating)			? $row[0]->activity_iceskating			: "n";
			$this->activity_jetskiing = ($row[0]->activity_jetskiing)			? $row[0]->activity_jetskiing			: "n";
			$this->activity_kayaking = ($row[0]->activity_kayaking)			? $row[0]->activity_kayaking			: "n";
			$this->activity_livetheater = ($row[0]->activity_livetheater)			? $row[0]->activity_livetheater			: "n";
			$this->activity_marina = ($row[0]->activity_marina)			? $row[0]->activity_marina			: "n";
			$this->activity_miniaturegolf = ($row[0]->activity_miniaturegolf)			? $row[0]->activity_miniaturegolf			: "n";
			$this->activity_mountainbiking = ($row[0]->activity_mountainbiking)			? $row[0]->activity_mountainbiking			: "n";
			$this->activity_museums = ($row[0]->activity_museums)			? $row[0]->activity_museums			: "n";
			$this->activity_paddleboating = ($row[0]->activity_paddleboating)			? $row[0]->activity_paddleboating			: "n";
			$this->activity_paragliding = ($row[0]->activity_paragliding)			? $row[0]->activity_paragliding			: "n";
			$this->activity_parasailing = ($row[0]->activity_parasailing)			? $row[0]->activity_parasailing			: "n";
			$this->activity_playground = ($row[0]->activity_playground)			? $row[0]->activity_playground			: "n";
			$this->activity_recreationcenter = ($row[0]->activity_recreationcenter)			? $row[0]->activity_recreationcenter			: "n";
			$this->activity_restaurants = ($row[0]->activity_restaurants)			? $row[0]->activity_restaurants			: "n";
			$this->activity_rollerblading = ($row[0]->activity_rollerblading)			? $row[0]->activity_rollerblading			: "n";
			$this->activity_sailing = ($row[0]->activity_sailing)			? $row[0]->activity_sailing			: "n";
			$this->activity_shelling = ($row[0]->activity_shelling)			? $row[0]->activity_shelling			: "n";
			$this->activity_shopping = ($row[0]->activity_shopping)			? $row[0]->activity_shopping			: "n";
			$this->activity_sightseeing = ($row[0]->activity_sightseeing)			? $row[0]->activity_sightseeing			: "n";
			$this->activity_skiing = ($row[0]->activity_skiing)			? $row[0]->activity_skiing			: "n";
			$this->activity_bayfishing = ($row[0]->activity_bayfishing)			? $row[0]->activity_bayfishing			: "n";
			$this->activity_spa = ($row[0]->activity_spa)			? $row[0]->activity_spa			: "n";
			$this->activity_surffishing = ($row[0]->activity_surffishing)			? $row[0]->activity_surffishing			: "n";
			$this->activity_surfing = ($row[0]->activity_surfing)			? $row[0]->activity_surfing			: "n";
			$this->activity_swimming = ($row[0]->activity_swimming)			? $row[0]->activity_swimming			: "n";
			$this->activity_tennis = ($row[0]->activity_tennis)			? $row[0]->activity_tennis			: "n";
			$this->activity_themeparks = ($row[0]->activity_themeparks)			? $row[0]->activity_themeparks			: "n";
			$this->activity_walking = ($row[0]->activity_walking)			? $row[0]->activity_walking			: "n";
			$this->activity_waterparks = ($row[0]->activity_waterparks)			? $row[0]->activity_waterparks			: "n";
			$this->activity_waterskiing = ($row[0]->activity_waterskiing)			? $row[0]->activity_waterskiing			: "n";
			$this->activity_watertubing = ($row[0]->activity_watertubing)			? $row[0]->activity_watertubing			: "n";
			$this->activity_wildlifeviewing = ($row[0]->activity_wildlifeviewing)			? $row[0]->activity_wildlifeviewing			: "n";
			$this->activity_zoo = ($row[0]->activity_zoo)			? $row[0]->activity_zoo			: "n";
            
            $this->isppilisting        = ($row[0]->isppilisting)     ? $row[0]->isppilisting        : ($this->isppilisting    ? $this->isppilisting    : "");
            $this->deposit_amount        = ($row[0]->deposit_amount)     ? $row[0]->deposit_amount        : ($this->deposit_amount    ? $this->deposit_amount    : 0);
            $this->tax_per_inquiry        = ($row[0]->tax_per_inquiry)     ? $row[0]->tax_per_inquiry        : ($this->tax_per_inquiry    ? $this->tax_per_inquiry    : 0);
            $this->unique_email        = ($row[0]->unique_email)     ? $row[0]->unique_email        : ($this->unique_email    ? $this->unique_email    : "n");
			$this->required_stay = ($row[0]->required_stay)? $row[0]->required_stay: '';
			$this->is_featured = ($row[0]->is_featured			? $row[0]->is_featured			: 0);
			$this->featured_date = ($row[0]->featured_date)	? $row[0]->featured_date: '';
			$this->search_pos = ($row[0]->search_pos			? $row[0]->search_pos			: 6);
			$this->search_pos_date = ($row[0]->search_pos_date			? $row[0]->search_pos_date			: "");

			$this->data_in_array 	= $row;

		}


        function arrayMakeFromRow($row='') {

			$status = new ItemStatus();
			$status->ItemStatus();
			$level = new ListingLevel();
			$level->ListingLevel();
			$status = $status->getDefaultStatus();
			$level = 'P';

			$this->id					=(isset($row["id"]))					? $row["id"]					: ($this->id				? $this->id					: 0);
			$this->account_id			=(isset($row["account_id"]))			? $row["account_id"]			: 0;
			$this->image_id				=(isset($row["image_id"]))			? $row["image_id"]				: ($this->image_id			? $this->image_id			: 0);
			$this->thumb_id				=(isset($row["thumb_id"]))			? $row["thumb_id"]				: ($this->thumb_id			? $this->thumb_id			: 0);
			$this->promotion_id			=(isset($row["promotion_id"]))		? $row["promotion_id"]			: ($this->promotion_id		? $this->promotion_id		: 0);
			$this->location_1			=(isset($row["location_1"]))			? $row["location_1"]			: 0;
			$this->location_2			=(isset($row["location_2"]))			? $row["location_2"]			: 0;
			$this->location_3			=(isset($row["location_3"]))			? $row["location_3"]			: 0;
			$this->location_4			=(isset($row["location_4"]))			? $row["location_4"]			: 0;
			$this->location_5			=(isset($row["location_5"]))			? $row["location_5"]			: 0;
			$this->renewal_date			=(isset($row["renewal_date"]))		? $row["renewal_date"]			: ($this->renewal_date		? $this->renewal_date		: 0);
			$this->discount_id			=(isset($row["discount_id"]))			? $row["discount_id"]			: "";
			$this->reminder				=(isset($row["reminder"]))			? $row["reminder"]				: ($this->reminder			? $this->reminder			: 0);
			$this->entered				=(isset($row["entered"]))				? $row["entered"]				: ($this->entered			? $this->entered			: "");
			$this->updated				=(isset($row["updated"]))				? $row["updated"]				: ($this->updated			? $this->updated			: "");
			$this->title				=(isset($row["title"]))				? $row["title"]					: ($this->title				? $this->title				: "");
			$this->seo_title			=(isset($row["seo_title"]))			? $row["seo_title"]				: ($this->seo_title			? $this->seo_title			: "");
			$this->claim_disable		=(isset($row["claim_disable"]))		? $row["claim_disable"]			: "n";
			$this->friendly_url			=(isset($row["friendly_url"]))		? $row["friendly_url"]			: "";
			$this->email				=(isset($row["email"]))				? $row["email"]					: "";
			$this->url					=(isset($row["url"]))					? $row["url"]					: "";
			$this->display_url			=(isset($row["display_url"]))			? $row["display_url"]			: "";
			$this->address				=(isset($row["address"]))				? $row["address"]				: "";
			$this->address2				=(isset($row["address2"]))			? $row["address2"]				: "";
			$this->zip_code				=(isset($row["zip_code"]))			? $row["zip_code"]				: "";
			$this->zip5                 =(isset($row["zip5"]))                ? $row["zip5"]                  : "";
			$this->phone				=(isset($row["phone"]))				? $row["phone"]					: "";
			$this->fax					=(isset($row["fax"]))					? $row["fax"]					: "";
			$this->description			=(isset($row["description"]))         ? $row["description"]			: "";
			$this->seo_description		=(isset($row["seo_description"]))     ? $row["seo_description"]		: ($this->seo_description	? $this->seo_description	: "");
			$this->long_description     =(isset($row["long_description"]))	? $row["long_description"]		: "";
			$this->video_snippet		=(isset($row["video_snippet"]))		? $row["video_snippet"]			: "";
			$this->video_description	=(isset($row["video_description"]))	? $row["video_description"]		: "";
			$this->keywords             =(isset($row["keywords"]))			? $row["keywords"]				: "";
			$this->seo_keywords         =(isset($row["seo_keywords"]))		? $row["seo_keywords"]			: ($this->seo_keywords		? $this->seo_keywords		: "");
			$this->attachment_file		=(isset($row["attachment_file"]))		? $row["attachment_file"]		: ($this->attachment_file	? $this->attachment_file	: "");
			$this->attachment_caption	=(isset($row["attachment_caption"]))	? $row["attachment_caption"]	: "";
			$this->features             =(isset($row["features"]))            ? $row["features"]              : "";
			$this->price                =(isset($row["price"]))               ? $row["price"]                 : ($this->price		? $this->price		: "");
			$this->facebook_page        =(isset($row["facebook_page"]))       ? $row["facebook_page"]         : "";
			$this->status				=(isset($row["status"]))				? $row["status"]				: $status;
			$this->suspended_sitemgr	=(isset($row["suspended_sitemgr"]))   ? $row["suspended_sitemgr"]		: ($this->suspended_sitemgr		? $this->suspended_sitemgr		: "n");
			$this->level				=(isset($row["level"]))				? $row["level"]					: ($this->level				? $this->level				: 'P');
			$this->hours_work			=(isset($row["hours_work"]))			? $row["hours_work"]			: "";
			$this->locations			=(isset($row["locations"]))			? $row["locations"]				: "";
			$this->latitude             =(isset($row["latitude"]))			? $row["latitude"]				: ($this->latitude		? $this->latitude		: "");
			$this->longitude			=(isset($row["longitude"]))			? $row["longitude"]				: ($this->longitude		? $this->longitude		: "");
			$this->map_zoom             =(isset($row["map_zoom"]))            ? $row["map_zoom"]              : 0;
			$this->listingtemplate_id	=(isset($row["listingtemplate_id"]))	? $row["listingtemplate_id"]	: 0;

            $this->custom_text0			=(isset($row["custom_text0"]))		? $row["custom_text0"]			: "";
			$this->custom_text1			=(isset($row["custom_text1"]))		? $row["custom_text1"]			: "";
			$this->custom_text2			=(isset($row["custom_text2"]))		? $row["custom_text2"]			: "";
			$this->custom_text3			=(isset($row["custom_text3"]))		? $row["custom_text3"]			: "";
			$this->custom_text4			=(isset($row["custom_text4"]))		? $row["custom_text4"]			: "";
			$this->custom_text5			=(isset($row["custom_text5"]))		? $row["custom_text5"]			: "";
			$this->custom_text6			=(isset($row["custom_text6"]))		? $row["custom_text6"]			: "";
			$this->custom_text7			=(isset($row["custom_text7"]))		? $row["custom_text7"]			: "";
			$this->custom_text8			=(isset($row["custom_text8"]))		? $row["custom_text8"]			: "";
			$this->custom_text9			=(isset($row["custom_text9"]))		? $row["custom_text9"]			: "";
			$this->custom_short_desc0	=(isset($row["custom_short_desc0"]))	? $row["custom_short_desc0"]	: "";
			$this->custom_short_desc1	=(isset($row["custom_short_desc1"]))	? $row["custom_short_desc1"]	: "";
			$this->custom_short_desc2	=(isset($row["custom_short_desc2"]))	? $row["custom_short_desc2"]	: "";
			$this->custom_short_desc3	=(isset($row["custom_short_desc3"]))	? $row["custom_short_desc3"]	: "";
			$this->custom_short_desc4	=(isset($row["custom_short_desc4"]))	? $row["custom_short_desc4"]	: "";
			$this->custom_short_desc5	=(isset($row["custom_short_desc5"]))	? $row["custom_short_desc5"]	: "";
			$this->custom_short_desc6	=(isset($row["custom_short_desc6"]))	? $row["custom_short_desc6"]	: "";
			$this->custom_short_desc7	=(isset($row["custom_short_desc7"]))	? $row["custom_short_desc7"]	: "";
			$this->custom_short_desc8	=(isset($row["custom_short_desc8"]))	? $row["custom_short_desc8"]	: "";
			$this->custom_short_desc9	=(isset($row["custom_short_desc9"]))	? $row["custom_short_desc9"]	: "";
			$this->custom_long_desc0	=(isset($row["custom_long_desc0"]))	? $row["custom_long_desc0"]		: "";
			$this->custom_long_desc1	=(isset($row["custom_long_desc1"]))	? $row["custom_long_desc1"]		: "";
			$this->custom_long_desc2	=(isset($row["custom_long_desc2"]))	? $row["custom_long_desc2"]		: "";
			$this->custom_long_desc3	=(isset($row["custom_long_desc3"]))	? $row["custom_long_desc3"]		: "";
			$this->custom_long_desc4	=(isset($row["custom_long_desc4"]))	? $row["custom_long_desc4"]		: "";
			$this->custom_long_desc5	=(isset($row["custom_long_desc5"]))	? $row["custom_long_desc5"]		: "";
			$this->custom_long_desc6	=(isset($row["custom_long_desc6"]))	? $row["custom_long_desc6"]		: "";
			$this->custom_long_desc7	=(isset($row["custom_long_desc7"]))	? $row["custom_long_desc7"]		: "";
			$this->custom_long_desc8	=(isset($row["custom_long_desc8"]))	? $row["custom_long_desc8"]		: "";
			$this->custom_long_desc9	=(isset($row["custom_long_desc9"]))	? $row["custom_long_desc9"]		: "";
            $this->custom_checkbox0		=(isset($row["custom_checkbox0"]))	? $row["custom_checkbox0"]		: "n";
			$this->custom_checkbox1		=(isset($row["custom_checkbox1"]))	? $row["custom_checkbox1"]		: "n";
			$this->custom_checkbox2		=(isset($row["custom_checkbox2"]))	? $row["custom_checkbox2"]		: "n";
			$this->custom_checkbox3		=(isset($row["custom_checkbox3"]))	? $row["custom_checkbox3"]		: "n";
			$this->custom_checkbox4		=(isset($row["custom_checkbox4"]))	? $row["custom_checkbox4"]		: "n";
			$this->custom_checkbox5		=(isset($row["custom_checkbox5"]))	? $row["custom_checkbox5"]		: "n";
			$this->custom_checkbox6		=(isset($row["custom_checkbox6"]))	? $row["custom_checkbox6"]		: "n";
			$this->custom_checkbox7		=(isset($row["custom_checkbox7"]))	? $row["custom_checkbox7"]		: "n";
			$this->custom_checkbox8		=(isset($row["custom_checkbox8"]))	? $row["custom_checkbox8"]		: "n";
			$this->custom_checkbox9		=(isset($row["custom_checkbox9"]))	? $row["custom_checkbox9"]		: "n";
			$this->custom_dropdown0		=(isset($row["custom_dropdown0"]))	? $row["custom_dropdown0"]		: "";
			$this->custom_dropdown1		=(isset($row["custom_dropdown1"]))	? $row["custom_dropdown1"]		: "";
			$this->custom_dropdown2		=(isset($row["custom_dropdown2"]))	? $row["custom_dropdown2"]		: "";
			$this->custom_dropdown3		=(isset($row["custom_dropdown3"]))	? $row["custom_dropdown3"]		: "";
			$this->custom_dropdown4		=(isset($row["custom_dropdown4"]))	? $row["custom_dropdown4"]		: "";
			$this->custom_dropdown5		=(isset($row["custom_dropdown5"]))	? $row["custom_dropdown5"]		: "";
			$this->custom_dropdown6		=(isset($row["custom_dropdown6"]))	? $row["custom_dropdown6"]		: "";
			$this->custom_dropdown7		=(isset($row["custom_dropdown7"]))	? $row["custom_dropdown7"]		: "";
			$this->custom_dropdown8		=(isset($row["custom_dropdown8"]))	? $row["custom_dropdown8"]		: "";
			$this->custom_dropdown9		=(isset($row["custom_dropdown9"]))	? $row["custom_dropdown9"]		: "";

			$this->number_views			=(isset($row["number_views"]))		? $row["number_views"]			: ($this->number_views		? $this->number_views	: 0);
			$this->avg_review			=(isset($row["avg_review"]))			? $row["avg_review"]			: ($this->avg_review		? $this->avg_review		: 0);
			$this->package_id			=(isset($row["package_id"]))			? $row["package_id"]			: ($this->package_id			? $this->package_id				: 0);
			$this->package_price		=(isset($row["package_price"]))		? $row["package_price"]			: ($this->package_price			? $this->package_price			: 0);
			$this->backlink				=(isset($row["backlink"]))			? $row["backlink"]				: ($this->backlink				? $this->backlink				: "n");
            $this->backlink_url				=(isset($row["backlink_url"]))			? $row["backlink_url"]				: ($this->backlink_url				? $this->backlink_url				: "");
            $this->clicktocall_number		=(isset($row["clicktocall_number"]))		? $row["clicktocall_number"]	: ($this->clicktocall_number	? $this->clicktocall_number			: "");
            $this->clicktocall_extension	=(isset($row["clicktocall_extension"]))	? $row["clicktocall_extension"]	: ($this->clicktocall_extension	? $this->clicktocall_extension		: 0);
            $this->clicktocall_date			=(isset($row["clicktocall_date"]))		? $row["clicktocall_date"]		: ($this->clicktocall_date		? $this->clicktocall_date			: "");
            $this->activation_date			=(isset($row["activation_date"]))			? $row["activation_date"]		: ($this->activation_date		? $this->activation_date			: "");
            $this->bedroom			=(isset($row["bedroom"]))			? $row["bedroom"]		: "";
            $this->bedsize			=(isset($row["bedsize"]))			? $row["bedsize"]		: "";
            $this->sleeps			=(isset($row["sleeps"]))			? $row["sleeps"]		: "";
            $this->bathroom			=(isset($row["bathroom"]))		? $row["bathroom"]		: "";
            $this->property_type			=(isset($row["property_type"]))		? $row["property_type"]		: "";
            $this->view						=(isset($row["view"]))				? $row["view"]				: ($this->view				? $this->view				: "");
            $this->distance_beach			=(isset($row["distance_beach"]))		? $row["distance_beach"]	: ($this->distance_beach	? $this->distance_beach		: "");
            $this->development_name			=(isset($row["development_name"]))		? $row["development_name"]		: "";
            
            $this->development_id			= (isset($row["development_id"]))		? $row["development_id"]		: "";
            
            $this->airport_distance			=(isset($row["airport_distance"]))		? $row["airport_distance"]		: "";
            $this->airport_abbreviation			=(isset($row["airport_abbreviation"]))		? $row["airport_abbreviation"]		: "";

            $this->contact_fname			=(isset($row["contact_fname"]))		? $row["contact_fname"]		: "";
            $this->contact_lname			=(isset($row["contact_lname"]))		? $row["contact_lname"]		: "";

            $this->external_link1			=(isset($row["external_link1"]))		? $row["external_link1"]		: "";
            $this->external_link_text1			=(isset($row["external_link_text1"]))		? $row["external_link_text1"]		: "";
            $this->external_link2			=(isset($row["external_link2"]))		? $row["external_link2"]		: "";
            $this->external_link_text2			=(isset($row["external_link_text2"]))		? $row["external_link_text2"]		: "";
            $this->external_link3			=(isset($row["external_link3"]))		? $row["external_link3"]		: "";
            $this->external_link_text3			=(isset($row["external_link_text3"]))		? $row["external_link_text3"]		: "";

            $this->amenity_airhockey =(isset($row["amenity_airhockey"]))	? $row["amenity_airhockey"]	: "n";
			$this->amenity_alarmclock =(isset($row["amenity_alarmclock"]))	? $row["amenity_alarmclock"]	: "n";
			$this->amenity_answeringmachine =(isset($row["amenity_answeringmachine"]))	? $row["amenity_answeringmachine"]	: "n";
			$this->amenity_arcadegames =(isset($row["amenity_arcadegames"]))	? $row["amenity_arcadegames"]	: "n";
			$this->amenity_billiards =(isset($row["amenity_billiards"]))	? $row["amenity_billiards"]	: "n";
			$this->amenity_blender =(isset($row["amenity_blender"]))	? $row["amenity_blender"]	: "n";
			$this->amenity_blurayplayer =(isset($row["amenity_blurayplayer"]))	? $row["amenity_blurayplayer"]	: "n";
			$this->amenity_books =(isset($row["amenity_books"]))	? $row["amenity_books"]	: "n";
			$this->amenity_casetteplayer =(isset($row["amenity_casetteplayer"]))	? $row["amenity_casetteplayer"]	: "n";
			$this->amenity_cdplayer =(isset($row["amenity_cdplayer"]))	? $row["amenity_cdplayer"]	: "n";
			$this->amenity_ceilingfan =(isset($row["amenity_ceilingfan"]))	? $row["amenity_ceilingfan"]	: "n";
			$this->amenity_childshighchair =(isset($row["amenity_childshighchair"]))	? $row["amenity_childshighchair"]	: "n";
			$this->amenity_coffeemaker =(isset($row["amenity_coffeemaker"]))	? $row["amenity_coffeemaker"]	: "n";
			$this->amenity_communalpool =(isset($row["amenity_communalpool"]))	? $row["amenity_communalpool"]	: "n";
			$this->amenity_computer =(isset($row["amenity_computer"]))	? $row["amenity_computer"]	: "n";
			$this->amenity_cookware =(isset($row["amenity_cookware"]))	? $row["amenity_cookware"]	: "n";
			$this->amenity_cookingrange =(isset($row["amenity_cookingrange"]))	? $row["amenity_cookingrange"]	: "n";
			$this->amenity_deckfurniture =(isset($row["amenity_deckfurniture"]))	? $row["amenity_deckfurniture"]	: "n";
			$this->amenity_dishwasher =(isset($row["amenity_dishwasher"]))	? $row["amenity_dishwasher"]	: "n";
			$this->amenity_dishes =(isset($row["amenity_dishes"]))	? $row["amenity_dishes"]	: "n";
			$this->amenity_dvdplayer =(isset($row["amenity_dvdplayer"]))	? $row["amenity_dvdplayer"]	: "n";
			$this->amenity_exercisefacilities =(isset($row["amenity_exercisefacilities"]))	? $row["amenity_exercisefacilities"]	: "n";
			$this->amenity_foosball =(isset($row["amenity_foosball"]))	? $row["amenity_foosball"]	: "n";
			$this->amenity_gametable =(isset($row["amenity_gametable"]))	? $row["amenity_gametable"]	: "n";
			$this->amenity_games =(isset($row["amenity_games"]))	? $row["amenity_games"]	: "n";
			$this->amenity_grill =(isset($row["amenity_grill"]))	? $row["amenity_grill"]	: "n";
			$this->amenity_hairdryer =(isset($row["amenity_hairdryer"]))	? $row["amenity_hairdryer"]	: "n";
			$this->amenity_icemaker =(isset($row["amenity_icemaker"]))	? $row["amenity_icemaker"]	: "n";
			$this->amenity_internet =(isset($row["amenity_internet"]))	? $row["amenity_internet"]	: "n";
			$this->amenity_ironandboard =(isset($row["amenity_ironandboard"]))	? $row["amenity_ironandboard"]	: "n";
			$this->amenity_kidsgames =(isset($row["amenity_kidsgames"]))	? $row["amenity_kidsgames"]	: "n";
			$this->amenity_linensprovided =(isset($row["amenity_linensprovided"]))	? $row["amenity_linensprovided"]	: "n";
			$this->amenity_lobsterpot =(isset($row["amenity_lobsterpot"]))	? $row["amenity_lobsterpot"]	: "n";
			$this->amenity_microwave =(isset($row["amenity_microwave"]))	? $row["amenity_microwave"]	: "n";
			$this->amenity_minirefrigerator =(isset($row["amenity_minirefrigerator"]))	? $row["amenity_minirefrigerator"]	: "n";
			$this->amenity_mp3radiodock =(isset($row["amenity_mp3radiodock"]))	? $row["amenity_mp3radiodock"]	: "n";
			$this->amenity_outsideshower =(isset($row["amenity_outsideshower"]))	? $row["amenity_outsideshower"]	: "n";
			$this->amenity_oven =(isset($row["amenity_oven"]))	? $row["amenity_oven"]	: "n";
			$this->amenity_pinball =(isset($row["amenity_pinball"]))	? $row["amenity_pinball"]	: "n";
			$this->amenity_pingpong =(isset($row["amenity_pingpong"]))	? $row["amenity_pingpong"]	: "n";
			$this->amenity_privatepool =(isset($row["amenity_privatepool"]))	? $row["amenity_privatepool"]	: "n";
			$this->amenity_radio =(isset($row["amenity_radio"]))	? $row["amenity_radio"]	: "n";
			$this->amenity_refrigeratorfreezer =(isset($row["amenity_refrigeratorfreezer"]))	? $row["amenity_refrigeratorfreezer"]	: "n";
			$this->amenity_sofabed =(isset($row["amenity_sofabed"]))	? $row["amenity_sofabed"]	: "n";
			$this->amenity_stereo =(isset($row["amenity_stereo"]))	? $row["amenity_stereo"]	: "n";
			$this->amenity_telephone =(isset($row["amenity_telephone"]))	? $row["amenity_telephone"]	: "n";
			$this->amenity_television =(isset($row["amenity_television"]))	? $row["amenity_television"]	: "n";
			$this->amenity_toaster =(isset($row["amenity_toaster"]))	? $row["amenity_toaster"]	: "n";
			$this->amenity_toasteroven =(isset($row["amenity_toasteroven"]))	? $row["amenity_toasteroven"]	: "n";
			$this->amenity_towelsprovided =(isset($row["amenity_towelsprovided"]))	? $row["amenity_towelsprovided"]	: "n";
			$this->amenity_toys =(isset($row["amenity_toys"]))	? $row["amenity_toys"]	: "n";
			$this->amenity_utensils =(isset($row["amenity_utensils"]))	? $row["amenity_utensils"]	: "n";
			$this->amenity_vacuum =(isset($row["amenity_vacuum"]))	? $row["amenity_vacuum"]	: "n";
			$this->amenity_vcr =(isset($row["amenity_vcr"]))	? $row["amenity_vcr"]	: "n";
			$this->amenity_videogameconsole =(isset($row["amenity_videogameconsole"]))	? $row["amenity_videogameconsole"]	: "n";
			$this->amenity_videogames =(isset($row["amenity_videogames"]))	? $row["amenity_videogames"]	: "n";
			$this->amenity_washerdryer =(isset($row["amenity_washerdryer"]))	? $row["amenity_washerdryer"]	: "n";
			$this->amenity_wifi =(isset($row["amenity_wifi"]))	? $row["amenity_wifi"]	: "n";

			$this->feature_airconditioning =(isset($row["feature_airconditioning"]))	? $row["feature_airconditioning"]	: "n";
			$this->feature_balcony =(isset($row["feature_balcony"]))	? $row["feature_balcony"]	: "n";
			$this->feature_barbecuecharcoal =(isset($row["feature_barbecuecharcoal"]))	? $row["feature_barbecuecharcoal"]	: "n";
			$this->feature_barbecuegas =(isset($row["feature_barbecuegas"]))	? $row["feature_barbecuegas"]	: "n";
			$this->feature_boatslip =(isset($row["feature_boatslip"]))	? $row["feature_boatslip"]	: "n";
			$this->feature_cablesatellitetv =(isset($row["feature_cablesatellitetv"]))	? $row["feature_cablesatellitetv"]	: "n";
			$this->feature_clubhouse =(isset($row["feature_clubhouse"]))	? $row["feature_clubhouse"]	: "n";
			$this->feature_coveredparking =(isset($row["feature_coveredparking"]))	? $row["feature_coveredparking"]	: "n";
			$this->feature_deck =(isset($row["feature_deck"]))	? $row["feature_deck"]	: "n";
			$this->feature_diningroom =(isset($row["feature_diningroom"]))	? $row["feature_diningroom"]	: "n";
			$this->feature_elevator =(isset($row["feature_elevator"]))	? $row["feature_elevator"]	: "n";
			$this->feature_familyroom =(isset($row["feature_familyroom"]))	? $row["feature_familyroom"]	: "n";
			$this->feature_fullkitchen =(isset($row["feature_fullkitchen"]))	? $row["feature_fullkitchen"]	: "n";
			$this->feature_garage =(isset($row["feature_garage"]))	? $row["feature_garage"]	: "n";
			$this->feature_gasfireplace =(isset($row["feature_gasfireplace"]))	? $row["feature_gasfireplace"]	: "n";
			$this->feature_gatedcommunity =(isset($row["feature_gatedcommunity"]))	? $row["feature_gatedcommunity"]	: "n";
			$this->feature_wheelchairaccess =(isset($row["feature_wheelchairaccess"]))	? $row["feature_wheelchairaccess"]	: "n";
			$this->feature_heated =(isset($row["feature_heated"]))	? $row["feature_heated"]	: "n";
			$this->feature_heatedpool =(isset($row["feature_heatedpool"]))	? $row["feature_heatedpool"]	: "n";
			$this->feature_hottubjacuzzi =(isset($row["feature_hottubjacuzzi"]))	? $row["feature_hottubjacuzzi"]	: "n";
			$this->feature_internetaccess =(isset($row["feature_internetaccess"]))	? $row["feature_internetaccess"]	: "n";
			$this->feature_kitchenette =(isset($row["feature_kitchenette"]))	? $row["feature_kitchenette"]	: "n";
			$this->feature_livingroom =(isset($row["feature_livingroom"]))	? $row["feature_livingroom"]	: "n";
			$this->feature_loft =(isset($row["feature_loft"]))	? $row["feature_loft"]	: "n";
			$this->feature_onsitesecurity =(isset($row["feature_onsitesecurity"]))	? $row["feature_onsitesecurity"]	: "n";
			$this->feature_patio =(isset($row["feature_patio"]))	? $row["feature_patio"]	: "n";
			$this->feature_petsallowed =(isset($row["feature_petsallowed"]))		? $row["feature_petsallowed"]		: "n";
			$this->feature_playroom =(isset($row["feature_playroom"]))	? $row["feature_playroom"]	: "n";
			$this->feature_pool =(isset($row["feature_pool"]))	? $row["feature_pool"]	: "n";
			$this->feature_porch =(isset($row["feature_porch"]))	? $row["feature_porch"]	: "n";
			$this->feature_rooftopdeck =(isset($row["feature_rooftopdeck"]))	? $row["feature_rooftopdeck"]	: "n";
			$this->feature_sauna =(isset($row["feature_sauna"]))	? $row["feature_sauna"]	: "n";
			$this->feature_smokingpermitted =(isset($row["feature_smokingpermitted"]))	? $row["feature_smokingpermitted"]	: "n";
			$this->feature_woodfireplace =(isset($row["feature_woodfireplace"]))	? $row["feature_woodfireplace"]	: "n";
			
			$this->activity_antiquing =(isset($row["activity_antiquing"]))			? $row["activity_antiquing"]			: "n";
			$this->activity_basketballcourt =(isset($row["activity_basketballcourt"]))			? $row["activity_basketballcourt"]			: "n";
			$this->activity_beachcombing =(isset($row["activity_beachcombing"]))			? $row["activity_beachcombing"]			: "n";
			$this->activity_bicycling =(isset($row["activity_bicycling"]))			? $row["activity_bicycling"]			: "n";
			$this->activity_bikerentals =(isset($row["activity_bikerentals"]))			? $row["activity_bikerentals"]			: "n";
			$this->activity_birdwatching =(isset($row["activity_birdwatching"]))			? $row["activity_birdwatching"]			: "n";
			$this->activity_boatrentals =(isset($row["activity_boatrentals"]))			? $row["activity_boatrentals"]			: "n";
			$this->activity_boating =(isset($row["activity_boating"]))			? $row["activity_boating"]			: "n";
			$this->activity_botanicalgarden =(isset($row["activity_botanicalgarden"]))			? $row["activity_botanicalgarden"]			: "n";
			$this->activity_canoe =(isset($row["activity_canoe"]))			? $row["activity_canoe"]			: "n";
			$this->activity_churches =(isset($row["activity_churches"]))			? $row["activity_churches"]			: "n";
			$this->activity_cinemas =(isset($row["activity_cinemas"]))			? $row["activity_cinemas"]			: "n";
			$this->activity_bikesprovided =(isset($row["activity_bikesprovided"]))			? $row["activity_bikesprovided"]			: "n";
			$this->activity_deepseafishing =(isset($row["activity_deepseafishing"]))			? $row["activity_deepseafishing"]			: "n";
			$this->activity_fishing =(isset($row["activity_fishing"]))			? $row["activity_fishing"]			: "n";
			$this->activity_fitnesscenter =(isset($row["activity_fitnesscenter"]))			? $row["activity_fitnesscenter"]			: "n";
			$this->activity_golf =(isset($row["activity_golf"]))			? $row["activity_golf"]			: "n";
			$this->activity_healthbeautyspa =(isset($row["activity_healthbeautyspa"]))			? $row["activity_healthbeautyspa"]			: "n";
			$this->activity_hiking =(isset($row["activity_hiking"]))			? $row["activity_hiking"]			: "n";
			$this->activity_horsebackriding =(isset($row["activity_horsebackriding"]))			? $row["activity_horsebackriding"]			: "n";
			$this->activity_horseshoes =(isset($row["activity_horseshoes"]))			? $row["activity_horseshoes"]			: "n";
			$this->activity_hotairballooning =(isset($row["activity_hotairballooning"]))			? $row["activity_hotairballooning"]			: "n";
			$this->activity_iceskating =(isset($row["activity_iceskating"]))			? $row["activity_iceskating"]			: "n";
			$this->activity_jetskiing =(isset($row["activity_jetskiing"]))			? $row["activity_jetskiing"]			: "n";
			$this->activity_kayaking =(isset($row["activity_kayaking"]))			? $row["activity_kayaking"]			: "n";
			$this->activity_livetheater =(isset($row["activity_livetheater"]))			? $row["activity_livetheater"]			: "n";
			$this->activity_marina =(isset($row["activity_marina"]))			? $row["activity_marina"]			: "n";
			$this->activity_miniaturegolf =(isset($row["activity_miniaturegolf"]))			? $row["activity_miniaturegolf"]			: "n";
			$this->activity_mountainbiking =(isset($row["activity_mountainbiking"]))			? $row["activity_mountainbiking"]			: "n";
			$this->activity_museums =(isset($row["activity_museums"]))			? $row["activity_museums"]			: "n";
			$this->activity_paddleboating =(isset($row["activity_paddleboating"]))			? $row["activity_paddleboating"]			: "n";
			$this->activity_paragliding =(isset($row["activity_paragliding"]))			? $row["activity_paragliding"]			: "n";
			$this->activity_parasailing =(isset($row["activity_parasailing"]))			? $row["activity_parasailing"]			: "n";
			$this->activity_playground =(isset($row["activity_playground"]))			? $row["activity_playground"]			: "n";
			$this->activity_recreationcenter =(isset($row["activity_recreationcenter"]))			? $row["activity_recreationcenter"]			: "n";
			$this->activity_restaurants =(isset($row["activity_restaurants"]))			? $row["activity_restaurants"]			: "n";
			$this->activity_rollerblading =(isset($row["activity_rollerblading"]))			? $row["activity_rollerblading"]			: "n";
			$this->activity_sailing =(isset($row["activity_sailing"]))			? $row["activity_sailing"]			: "n";
			$this->activity_shelling =(isset($row["activity_shelling"]))			? $row["activity_shelling"]			: "n";
			$this->activity_shopping =(isset($row["activity_shopping"]))			? $row["activity_shopping"]			: "n";
			$this->activity_sightseeing =(isset($row["activity_sightseeing"]))			? $row["activity_sightseeing"]			: "n";
			$this->activity_skiing =(isset($row["activity_skiing"]))			? $row["activity_skiing"]			: "n";
			$this->activity_bayfishing =(isset($row["activity_bayfishing"]))			? $row["activity_bayfishing"]			: "n";
			$this->activity_spa =(isset($row["activity_spa"]))			? $row["activity_spa"]			: "n";
			$this->activity_surffishing =(isset($row["activity_surffishing"]))			? $row["activity_surffishing"]			: "n";
			$this->activity_surfing =(isset($row["activity_surfing"]))			? $row["activity_surfing"]			: "n";
			$this->activity_swimming =(isset($row["activity_swimming"]))			? $row["activity_swimming"]			: "n";
			$this->activity_tennis =(isset($row["activity_tennis"]))			? $row["activity_tennis"]			: "n";
			$this->activity_themeparks =(isset($row["activity_themeparks"]))			? $row["activity_themeparks"]			: "n";
			$this->activity_walking =(isset($row["activity_walking"]))			? $row["activity_walking"]			: "n";
			$this->activity_waterparks =(isset($row["activity_waterparks"]))			? $row["activity_waterparks"]			: "n";
			$this->activity_waterskiing =(isset($row["activity_waterskiing"]))			? $row["activity_waterskiing"]			: "n";
			$this->activity_watertubing =(isset($row["activity_watertubing"]))			? $row["activity_watertubing"]			: "n";
			$this->activity_wildlifeviewing =(isset($row["activity_wildlifeviewing"]))			? $row["activity_wildlifeviewing"]			: "n";
			$this->activity_zoo =(isset($row["activity_zoo"]))			? $row["activity_zoo"]			: "n";
            
            $this->isppilisting        =(isset($row["isppilisting"]))     ? $row["isppilisting"]        : ($this->isppilisting    ? $this->isppilisting    : "");
            $this->deposit_amount        =(isset($row["deposit_amount"]))     ? $row["deposit_amount"]        : ($this->deposit_amount    ? $this->deposit_amount    : 0);
            $this->tax_per_inquiry        =(isset($row["tax_per_inquiry"]))     ? $row["tax_per_inquiry"]        : ($this->tax_per_inquiry    ? $this->tax_per_inquiry    : 0);
            $this->unique_email        =(isset($row["unique_email"]))     ? $row["unique_email"]        : ($this->unique_email    ? $this->unique_email    : "n");
			$this->required_stay =(isset($row["required_stay"])			? $row["required_stay"]			: '');
			$this->is_featured =(isset($row["is_featured"])			? $row["is_featured"]			: 0);
			$this->featured_date =(isset($row["featured_date"])			? $row["featured_date"]			: '');
			$this->search_pos =(isset($row["search_pos"])			? $row["search_pos"]			: 6);
			$this->search_pos_date =(isset($row["search_pos_date"])			? $row["search_pos_date"]			: "");

			$this->data_in_array 	= $row;

		}
		/**
		 * <code>
		 *		//Using this in forms or other pages.
		 *		$listingObj->Save();
		 * <br /><br />
		 *		//Using this in Listing() class.
		 *		$this->Save();
		 * </code>
		 
		 * @name Save
		 * @access Public
		 */
		function Save() {


			$this->prepareToSave();
			//$aux_old_account = str_replace("'", "", $this->old_account_id);
			$aux_account = str_replace("'", "", $this->account_id);

			$this->friendly_url = strtolower($this->friendly_url);
			//d($this->id,1);
			if ($this->id && ($this->id!="'0'")) {


				$sql = "SELECT status FROM Listing WHERE id = $this->id";
				$result = self::fetch($sql);
                
				//if ($row == $result[0]->status) 
                $last_status = (isset($result[0]->status))?$result[0]->status:'P';

				$this_status = $this->status;
				$this_id = $this->id;



				$sql = "UPDATE Listing SET"
					. " account_id         = $this->account_id,"
					. " image_id           = $this->image_id,"
					. " thumb_id           = $this->thumb_id,"
					. " promotion_id       = $this->promotion_id,"
					. " location_1         = $this->location_1,"
					. " location_2         = $this->location_2,"
					. " location_3         = $this->location_3,"
					. " location_4         = $this->location_4,"
					. " location_5         = $this->location_5,"
					. " renewal_date       = $this->renewal_date,"
					. " discount_id        = $this->discount_id,"
					. " reminder           = $this->reminder,"
					. " updated            = NOW(),"
					. " title              = $this->title,"
					. " seo_title          = $this->seo_title,"
					. " claim_disable      = $this->claim_disable,"
					. " friendly_url       = $this->friendly_url,"
					. " email              = $this->email,"
					. " url                = $this->url,"
					. " display_url        = $this->display_url,"
					. " address            = $this->address,"
					. " address2           = $this->address2,"
					. " zip_code           = $this->zip_code,"
					. " phone              = $this->phone,"
					. " fax                = $this->fax,"
					. " description        = $this->description,"
					. " seo_description    = $this->seo_description,"
					. " long_description   = $this->long_description,"
					. " video_snippet      = $this->video_snippet,"
					. " video_description  = $this->video_description,"
					. " keywords           = $this->keywords,"
					. " seo_keywords       = $this->seo_keywords,"
					. " attachment_file    = $this->attachment_file,"
					. " attachment_caption = $this->attachment_caption,"
					. " features           = $this->features,"
					. " price              = $this->price,"
					. " facebook_page      = $this->facebook_page,"
					. " status             = $this->status,"
					. " suspended_sitemgr  = $this->suspended_sitemgr,"
					. " level              = $this->level,"
					. " hours_work         = $this->hours_work,"
					. " locations          = $this->locations,"
					. " listingtemplate_id = $this->listingtemplate_id,"
                    . " custom_text0       = $this->custom_text0,"
					. " custom_text1       = $this->custom_text1,"
					. " custom_text2       = $this->custom_text2,"
					. " custom_text3       = $this->custom_text3,"
					. " custom_text4       = $this->custom_text4,"
					. " custom_text5       = $this->custom_text5,"
					. " custom_text6       = $this->custom_text6,"
					. " custom_text7       = $this->custom_text7,"
					. " custom_text8       = $this->custom_text8,"
					. " custom_text9       = $this->custom_text9,"
					. " custom_short_desc0 = $this->custom_short_desc0,"
					. " custom_short_desc1 = $this->custom_short_desc1,"
					. " custom_short_desc2 = $this->custom_short_desc2,"
					. " custom_short_desc3 = $this->custom_short_desc3,"
					. " custom_short_desc4 = $this->custom_short_desc4,"
					. " custom_short_desc5 = $this->custom_short_desc5,"
					. " custom_short_desc6 = $this->custom_short_desc6,"
					. " custom_short_desc7 = $this->custom_short_desc7,"
					. " custom_short_desc8 = $this->custom_short_desc8,"
					. " custom_short_desc9 = $this->custom_short_desc9,"
					. " custom_long_desc0  = $this->custom_long_desc0,"
					. " custom_long_desc1  = $this->custom_long_desc1,"
					. " custom_long_desc2  = $this->custom_long_desc2,"
					. " custom_long_desc3  = $this->custom_long_desc3,"
					. " custom_long_desc4  = $this->custom_long_desc4,"
					. " custom_long_desc5  = $this->custom_long_desc5,"
					. " custom_long_desc6  = $this->custom_long_desc6,"
					. " custom_long_desc7  = $this->custom_long_desc7,"
					. " custom_long_desc8  = $this->custom_long_desc8,"
					. " custom_long_desc9  = $this->custom_long_desc9,"
                    . " custom_checkbox0   = $this->custom_checkbox0,"
					. " custom_checkbox1   = $this->custom_checkbox1,"
					. " custom_checkbox2   = $this->custom_checkbox2,"
					. " custom_checkbox3   = $this->custom_checkbox3,"
					. " custom_checkbox4   = $this->custom_checkbox4,"
					. " custom_checkbox5   = $this->custom_checkbox5,"
					. " custom_checkbox6   = $this->custom_checkbox6,"
					. " custom_checkbox7   = $this->custom_checkbox7,"
					. " custom_checkbox8   = $this->custom_checkbox8,"
					. " custom_checkbox9   = $this->custom_checkbox9,"
					. " custom_dropdown0   = $this->custom_dropdown0,"
					. " custom_dropdown1   = $this->custom_dropdown1,"
					. " custom_dropdown2   = $this->custom_dropdown2,"
					. " custom_dropdown3   = $this->custom_dropdown3,"
					. " custom_dropdown4   = $this->custom_dropdown4,"
					. " custom_dropdown5   = $this->custom_dropdown5,"
					. " custom_dropdown6   = $this->custom_dropdown6,"
					. " custom_dropdown7   = $this->custom_dropdown7,"
					. " custom_dropdown8   = $this->custom_dropdown8,"
					. " custom_dropdown9   = $this->custom_dropdown9,"
					. " number_views	   = $this->number_views,"
					. " avg_review		   = $this->avg_review,"
					. " latitude           = $this->latitude,"
					. " longitude          = $this->longitude,"
					. " map_zoom           = $this->map_zoom,"
					. " package_id		   = $this->package_id,"
					. " package_price	   = $this->package_price,"
					. " backlink		   = $this->backlink,"
					. " backlink_url            = $this->backlink_url,"
					. " clicktocall_number		= $this->clicktocall_number,"
					. " clicktocall_extension	= $this->clicktocall_extension,"
					. " clicktocall_date		= $this->clicktocall_date,"
					. " activation_date			= $this->activation_date,"
					. " bedroom			= $this->bedroom,"
					. " bedsize			= $this->bedsize,"
					. " sleeps			= $this->sleeps,"
					. " bathroom		= $this->bathroom,"
					. " property_type		= $this->property_type,"
					. " `view`		= $this->view,"
					. " distance_beach		= $this->distance_beach,"
					. " development_name		= $this->development_name,"
                    . " development_id		= $this->development_id,"
					. " airport_distance		= $this->airport_distance,"
					. " airport_abbreviation		= $this->airport_abbreviation,"
					. " contact_fname		= $this->contact_fname,"
					. " contact_lname		= $this->contact_lname,"
					. " external_link1		= $this->external_link1,"
					. " external_link_text1		= $this->external_link_text1,"
					. " external_link2		= $this->external_link2,"
					. " external_link_text2		= $this->external_link_text2,"
					. " external_link3		= $this->external_link3,"
					. " external_link_text3		= $this->external_link_text3,"
					. " amenity_airhockey = $this->amenity_airhockey,"
					. " amenity_alarmclock = $this->amenity_alarmclock,"
					. " amenity_answeringmachine = $this->amenity_answeringmachine,"
					. " amenity_arcadegames = $this->amenity_arcadegames,"
					. " amenity_billiards = $this->amenity_billiards,"
					. " amenity_blender = $this->amenity_blender,"
					. " amenity_blurayplayer = $this->amenity_blurayplayer,"
					. " amenity_books = $this->amenity_books,"
					. " amenity_casetteplayer = $this->amenity_casetteplayer,"
					. " amenity_cdplayer = $this->amenity_cdplayer,"
					. " amenity_ceilingfan = $this->amenity_ceilingfan,"
					. " amenity_childshighchair = $this->amenity_childshighchair,"
					. " amenity_coffeemaker = $this->amenity_coffeemaker,"
					. " amenity_communalpool = $this->amenity_communalpool,"
					. " amenity_computer = $this->amenity_computer,"
					. " amenity_cookware = $this->amenity_cookware,"
					. " amenity_cookingrange = $this->amenity_cookingrange,"
					. " amenity_deckfurniture = $this->amenity_deckfurniture,"
					. " amenity_dishwasher  = $this->amenity_dishwasher,"
					. " amenity_dishes = $this->amenity_dishes,"
					. " amenity_dvdplayer = $this->amenity_dvdplayer,"
					. " amenity_exercisefacilities = $this->amenity_exercisefacilities,"
					. " amenity_foosball = $this->amenity_foosball,"
					. " amenity_gametable = $this->amenity_gametable,"
					. " amenity_games = $this->amenity_games,"
					. " amenity_grill = $this->amenity_grill,"
					. " amenity_hairdryer = $this->amenity_hairdryer,"
					. " amenity_icemaker = $this->amenity_icemaker,"
					. " amenity_internet = $this->amenity_internet,"
					. " amenity_ironandboard = $this->amenity_ironandboard,"
					. " amenity_kidsgames = $this->amenity_kidsgames,"
					. " amenity_linensprovided = $this->amenity_linensprovided,"
					. " amenity_lobsterpot = $this->amenity_lobsterpot,"
					. " amenity_microwave = $this->amenity_microwave,"
					. " amenity_minirefrigerator = $this->amenity_minirefrigerator,"
					. " amenity_mp3radiodock = $this->amenity_mp3radiodock,"
					. " amenity_outsideshower = $this->amenity_outsideshower,"
					. " amenity_oven = $this->amenity_oven,"
					. " amenity_pinball = $this->amenity_pinball,"
					. " amenity_pingpong = $this->amenity_pingpong,"
					. " amenity_privatepool = $this->amenity_privatepool,"
					. " amenity_radio = $this->amenity_radio,"
					. " amenity_refrigeratorfreezer = $this->amenity_refrigeratorfreezer,"
					. " amenity_sofabed = $this->amenity_sofabed,"
					. " amenity_stereo = $this->amenity_stereo,"
					. " amenity_telephone = $this->amenity_telephone,"
					. " amenity_television = $this->amenity_television,"
					. " amenity_toaster = $this->amenity_toaster,"
					. " amenity_toasteroven = $this->amenity_toasteroven,"
					. " amenity_towelsprovided = $this->amenity_towelsprovided,"
					. " amenity_toys = $this->amenity_toys,"
					. " amenity_utensils = $this->amenity_utensils,"
					. " amenity_vacuum = $this->amenity_vacuum,"
					. " amenity_vcr = $this->amenity_vcr,"
					. " amenity_videogameconsole = $this->amenity_videogameconsole,"
					. " amenity_videogames = $this->amenity_videogames,"
					. " amenity_washerdryer = $this->amenity_washerdryer,"
					. " amenity_wifi = $this->amenity_wifi,"
					. " feature_airconditioning = $this->feature_airconditioning,"
					. " feature_balcony = $this->feature_balcony,"
					. " feature_barbecuecharcoal = $this->feature_barbecuecharcoal,"
					. " feature_barbecuegas = $this->feature_barbecuegas,"
					. " feature_boatslip = $this->feature_boatslip,"
					. " feature_cablesatellitetv = $this->feature_cablesatellitetv,"
					. " feature_clubhouse = $this->feature_clubhouse,"
					. " feature_coveredparking = $this->feature_coveredparking,"
					. " feature_deck = $this->feature_deck,"
					. " feature_diningroom = $this->feature_diningroom,"
					. " feature_elevator = $this->feature_elevator,"
					. " feature_familyroom = $this->feature_familyroom,"
					. " feature_fullkitchen = $this->feature_fullkitchen,"
					. " feature_garage = $this->feature_garage,"
					. " feature_gasfireplace = $this->feature_gasfireplace,"
					. " feature_gatedcommunity = $this->feature_gatedcommunity,"
					. " feature_wheelchairaccess = $this->feature_wheelchairaccess,"
					. " feature_heated = $this->feature_heated,"
					. " feature_heatedpool = $this->feature_heatedpool,"
					. " feature_hottubjacuzzi = $this->feature_hottubjacuzzi,"
					. " feature_internetaccess = $this->feature_internetaccess,"
					. " feature_kitchenette = $this->feature_kitchenette,"
					. " feature_livingroom = $this->feature_livingroom,"
					. " feature_loft = $this->feature_loft,"
					. " feature_onsitesecurity = $this->feature_onsitesecurity,"
					. " feature_patio = $this->feature_patio,"
					. " feature_petsallowed = $this->feature_petsallowed,"
					. " feature_playroom = $this->feature_playroom,"
					. " feature_pool = $this->feature_pool,"
					. " feature_porch = $this->feature_porch,"
					. " feature_rooftopdeck = $this->feature_rooftopdeck,"
					. " feature_sauna = $this->feature_sauna,"
					. " feature_smokingpermitted = $this->feature_smokingpermitted,"
					. " feature_woodfireplace = $this->feature_woodfireplace,"
					. " activity_antiquing = $this->activity_antiquing,"
					. " activity_basketballcourt = $this->activity_basketballcourt,"
					. " activity_beachcombing = $this->activity_beachcombing,"
					. " activity_bicycling = $this->activity_bicycling,"
					. " activity_bikerentals = $this->activity_bikerentals,"
					. " activity_birdwatching = $this->activity_birdwatching,"
					. " activity_boatrentals = $this->activity_boatrentals,"
					. " activity_boating = $this->activity_boating,"
					. " activity_botanicalgarden = $this->activity_botanicalgarden,"
					. " activity_canoe = $this->activity_canoe,"
					. " activity_churches = $this->activity_churches,"
					. " activity_cinemas = $this->activity_cinemas,"
					. " activity_bikesprovided = $this->activity_bikesprovided,"
					. " activity_deepseafishing = $this->activity_deepseafishing,"
					. " activity_fishing = $this->activity_fishing,"
					. " activity_fitnesscenter = $this->activity_fitnesscenter,"
					. " activity_golf = $this->activity_golf,"
					. " activity_healthbeautyspa = $this->activity_healthbeautyspa,"
					. " activity_hiking = $this->activity_hiking,"
					. " activity_horsebackriding = $this->activity_horsebackriding,"
					. " activity_horseshoes = $this->activity_horseshoes,"
					. " activity_hotairballooning = $this->activity_hotairballooning,"
					. " activity_iceskating = $this->activity_iceskating,"
					. " activity_jetskiing = $this->activity_jetskiing,"
					. " activity_kayaking = $this->activity_kayaking,"
					. " activity_livetheater = $this->activity_livetheater,"
					. " activity_marina = $this->activity_marina,"
					. " activity_miniaturegolf = $this->activity_miniaturegolf,"
					. " activity_mountainbiking = $this->activity_mountainbiking,"
					. " activity_museums = $this->activity_museums,"
					. " activity_paddleboating = $this->activity_paddleboating,"
					. " activity_paragliding = $this->activity_paragliding,"
					. " activity_parasailing = $this->activity_parasailing,"
					. " activity_playground = $this->activity_playground,"
					. " activity_recreationcenter = $this->activity_recreationcenter,"
					. " activity_restaurants = $this->activity_restaurants,"
					. " activity_rollerblading = $this->activity_rollerblading,"
					. " activity_sailing = $this->activity_sailing,"
					. " activity_shelling = $this->activity_shelling,"
					. " activity_shopping = $this->activity_shopping,"
					. " activity_sightseeing = $this->activity_sightseeing,"
					. " activity_skiing = $this->activity_skiing,"
					. " activity_bayfishing = $this->activity_bayfishing,"
					. " activity_spa = $this->activity_spa,"
					. " activity_surffishing = $this->activity_surffishing,"
					. " activity_surfing = $this->activity_surfing,"
					. " activity_swimming = $this->activity_swimming,"
					. " activity_tennis = $this->activity_tennis,"
					. " activity_themeparks = $this->activity_themeparks,"
					. " activity_walking = $this->activity_walking,"
					. " activity_waterparks = $this->activity_waterparks,"
					. " activity_waterskiing = $this->activity_waterskiing,"
					. " activity_watertubing = $this->activity_watertubing,"
					. " activity_wildlifeviewing = $this->activity_wildlifeviewing,"
					. " activity_zoo = $this->activity_zoo,"
					. " isppilisting = $this->isppilisting,"
					. " deposit_amount = $this->deposit_amount,"
					. " tax_per_inquiry = $this->tax_per_inquiry,"
					. " unique_email = $this->unique_email,"
					. " required_stay = $this->required_stay,"
					. " is_featured = $this->is_featured,"
					. " featured_date = $this->featured_date,"
					. " search_pos = $this->search_pos,"
					. " search_pos_date = $this->search_pos_date"

					. " WHERE id           = $this->id";

				$row=self::insertSql($sql);

					


				$last_status = str_replace("\"", "", $last_status);
				$last_status = str_replace("'", "", $last_status);
				$this_status = str_replace("\"", "", $this_status);
				$this_status = str_replace("'", "", $this_status);
				$this_id = str_replace("\"", "", $this_id);
				$this_id = str_replace("'", "", $this_id);
				$this->system_countActiveListingByCategory($this_id);
                $aux_log_domain_id=1;
				if ($last_status != "P" && $this_status == "P"){
					$this->activity_newToApproved($aux_log_domain_id, $this->id, "listing", $this->title);
				} else if ($last_status == "P" && $this_status != "P") {
					$this->activity_deleteRecord($aux_log_domain_id, $this->id, "listing");
				} else if ($last_status == $this_status){
					$this->activity_updateRecord($aux_log_domain_id, $this->id, $this->title, "item", "listing");
				}

				/*
				 * Populate Listings to front
				 */
			
				$listingSummaryObj = new ListingSummary();

				$listingSummaryObj->PopulateTable($this->id, "update");
				$this->updateCategoryStatusByID();

				if (isset($aux_old_account) && $aux_old_account != $aux_account && $aux_account != 0) {
					$accDomain = new Account_Domain($aux_account, SELECTED_DOMAIN_ID);
					$accDomain->Save();
					$accDomain->saveOnDomain($aux_account, $this);
				}
                
			} else {
			
			//d('save',1);
                $aux_seoDescription = $this->description;
                $aux_seoDescription = str_replace(array("\r\n", "\n"), " ", $aux_seoDescription);
                $aux_seoDescription = str_replace("\\\"", "", $aux_seoDescription);
                
				$sql = "INSERT INTO Listing"
					. " (account_id,"
					. " image_id,"
					. " thumb_id,"
					. " promotion_id,"
					. " location_1,"
					. " location_2,"
					. " location_3,"
					. " location_4,"
					. " location_5,"
					. " renewal_date,"
					. " discount_id,"
					. " reminder,"
					. " fulltextsearch_keyword,"
					. " fulltextsearch_where,"
					. " updated,"
					. " entered,"
					. " title,"
					. " seo_title,"
					. " claim_disable,"
					. " friendly_url,"
					. " email,"
					. " url,"
					. " display_url,"
					. " address,"
					. " address2,"
					. " zip_code,"
					. " phone,"
					. " fax,"
					. " description,"
					. " seo_description,"
					. " long_description,"
					. " video_snippet,"
					. " video_description,"
					. " keywords,"
					. " seo_keywords,"
					. " attachment_file,"
					. " attachment_caption,"
					. " features,"
					. " price,"
					. " facebook_page,"
					. " status,"
					. " level,"
					. " hours_work,"
					. " locations,"
					. " listingtemplate_id,"
                    . " custom_text0,"
					. " custom_text1,"
					. " custom_text2,"
					. " custom_text3,"
					. " custom_text4,"
					. " custom_text5,"
					. " custom_text6,"
					. " custom_text7,"
					. " custom_text8,"
					. " custom_text9,"
					. " custom_short_desc0,"
					. " custom_short_desc1,"
					. " custom_short_desc2,"
					. " custom_short_desc3,"
					. " custom_short_desc4,"
					. " custom_short_desc5,"
					. " custom_short_desc6,"
					. " custom_short_desc7,"
					. " custom_short_desc8,"
					. " custom_short_desc9,"
					. " custom_long_desc0,"
					. " custom_long_desc1,"
					. " custom_long_desc2,"
					. " custom_long_desc3,"
					. " custom_long_desc4,"
					. " custom_long_desc5,"
					. " custom_long_desc6,"
					. " custom_long_desc7,"
					. " custom_long_desc8,"
					. " custom_long_desc9,"
                    . " custom_checkbox0,"
					. " custom_checkbox1,"
					. " custom_checkbox2,"
					. " custom_checkbox3,"
					. " custom_checkbox4,"
					. " custom_checkbox5,"
					. " custom_checkbox6,"
					. " custom_checkbox7,"
					. " custom_checkbox8,"
					. " custom_checkbox9,"
					. " custom_dropdown0,"
					. " custom_dropdown1,"
					. " custom_dropdown2,"
					. " custom_dropdown3,"
					. " custom_dropdown4,"
					. " custom_dropdown5,"
					. " custom_dropdown6,"
					. " custom_dropdown7,"
					. " custom_dropdown8,"
					. " custom_dropdown9,"
					. " number_views,"
					. " avg_review,"
					. " latitude,"
					. " longitude,"
					. " map_zoom,"
					. " package_id,"
					. " package_price,"
					. " backlink,"
					. " backlink_url,"
					. " clicktocall_number,"
					. " clicktocall_extension,"
					. " clicktocall_date,"
					. " activation_date,"
					. " bedroom,"
					. " bedsize,"
					. " sleeps,"
					. " bathroom,"
					. " property_type,"
					. " `view`,"
					. " distance_beach,"
					. " development_name,"
                    . " development_id,"
					. " airport_distance,"
					. " airport_abbreviation,"
					. " contact_fname,"
					. " contact_lname,"
					. " external_link1,"
					. " external_link_text1,"
					. " external_link2,"
					. " external_link_text2,"
					. " external_link3,"
					. " external_link_text3,"
					. " amenity_airhockey,"
					. " amenity_alarmclock,"
					. " amenity_answeringmachine,"
					. " amenity_arcadegames,"
					. " amenity_billiards,"
					. " amenity_blender,"
					. " amenity_blurayplayer,"
					. " amenity_books,"
					. " amenity_casetteplayer,"
					. " amenity_cdplayer,"
					. " amenity_ceilingfan,"
					. " amenity_childshighchair,"
					. " amenity_coffeemaker,"
					. " amenity_communalpool,"
					. " amenity_computer,"
					. " amenity_cookware,"
					. " amenity_cookingrange,"
					. " amenity_deckfurniture,"
					. " amenity_dishwasher ,"
					. " amenity_dishes,"
					. " amenity_dvdplayer,"
					. " amenity_exercisefacilities,"
					. " amenity_foosball,"
					. " amenity_gametable,"
					. " amenity_games,"
					. " amenity_grill,"
					. " amenity_hairdryer,"
					. " amenity_icemaker,"
					. " amenity_internet,"
					. " amenity_ironandboard,"
					. " amenity_kidsgames,"
					. " amenity_linensprovided,"
					. " amenity_lobsterpot,"
					. " amenity_microwave,"
					. " amenity_minirefrigerator,"
					. " amenity_mp3radiodock,"
					. " amenity_outsideshower,"
					. " amenity_oven,"
					. " amenity_pinball,"
					. " amenity_pingpong,"
					. " amenity_privatepool,"
					. " amenity_radio,"
					. " amenity_refrigeratorfreezer,"
					. " amenity_sofabed,"
					. " amenity_stereo,"
					. " amenity_telephone,"
					. " amenity_television,"
					. " amenity_toaster,"
					. " amenity_toasteroven,"
					. " amenity_towelsprovided,"
					. " amenity_toys,"
					. " amenity_utensils,"
					. " amenity_vacuum,"
					. " amenity_vcr,"
					. " amenity_videogameconsole,"
					. " amenity_videogames,"
					. " amenity_washerdryer,"
					. " amenity_wifi,"
					. " feature_airconditioning,"
					. " feature_balcony,"
					. " feature_barbecuecharcoal,"
					. " feature_barbecuegas,"
					. " feature_boatslip,"
					. " feature_cablesatellitetv,"
					. " feature_clubhouse,"
					. " feature_coveredparking,"
					. " feature_deck,"
					. " feature_diningroom,"
					. " feature_elevator,"
					. " feature_familyroom,"
					. " feature_fullkitchen,"
					. " feature_garage,"
					. " feature_gasfireplace,"
					. " feature_gatedcommunity,"
					. " feature_wheelchairaccess,"
					. " feature_heated,"
					. " feature_heatedpool,"
					. " feature_hottubjacuzzi,"
					. " feature_internetaccess,"
					. " feature_kitchenette,"
					. " feature_livingroom,"
					. " feature_loft,"
					. " feature_onsitesecurity,"
					. " feature_patio,"
					. " feature_petsallowed,"
					. " feature_playroom,"
					. " feature_pool,"
					. " feature_porch,"
					. " feature_rooftopdeck,"
					. " feature_sauna,"
					. " feature_smokingpermitted,"
					. " feature_woodfireplace,"
					. " activity_antiquing,"
					. " activity_basketballcourt,"
					. " activity_beachcombing,"
					. " activity_bicycling,"
					. " activity_bikerentals,"
					. " activity_birdwatching,"
					. " activity_boatrentals,"
					. " activity_boating,"
					. " activity_botanicalgarden,"
					. " activity_canoe,"
					. " activity_churches,"
					. " activity_cinemas,"
					. " activity_bikesprovided,"
					. " activity_deepseafishing,"
					. " activity_fishing,"
					. " activity_fitnesscenter,"
					. " activity_golf,"
					. " activity_healthbeautyspa,"
					. " activity_hiking,"
					. " activity_horsebackriding,"
					. " activity_horseshoes,"
					. " activity_hotairballooning,"
					. " activity_iceskating,"
					. " activity_jetskiing,"
					. " activity_kayaking,"
					. " activity_livetheater,"
					. " activity_marina,"
					. " activity_miniaturegolf,"
					. " activity_mountainbiking,"
					. " activity_museums,"
					. " activity_paddleboating,"
					. " activity_paragliding,"
					. " activity_parasailing,"
					. " activity_playground,"
					. " activity_recreationcenter,"
					. " activity_restaurants,"
					. " activity_rollerblading,"
					. " activity_sailing,"
					. " activity_shelling,"
					. " activity_shopping,"
					. " activity_sightseeing,"
					. " activity_skiing,"
					. " activity_bayfishing,"
					. " activity_spa,"
					. " activity_surffishing,"
					. " activity_surfing,"
					. " activity_swimming,"
					. " activity_tennis,"
					. " activity_themeparks,"
					. " activity_walking,"
					. " activity_waterparks,"
					. " activity_waterskiing,"
					. " activity_watertubing,"
					. " activity_wildlifeviewing,"
					. " activity_zoo,"
					. " isppilisting,"
					. " deposit_amount,"
					. " tax_per_inquiry,"
					. " unique_email,"
					. " is_featured,"
					. " featured_date,"
					. " search_pos,"
					. " search_pos_date,"
					. " required_stay)"
					. " VALUES"
					. " ($this->account_id,"
					. " $this->image_id,"
					. " $this->thumb_id,"
					. " $this->promotion_id,"
					. " $this->location_1,"
					. " $this->location_2,"
					. " $this->location_3,"
					. " $this->location_4,"
					. " $this->location_5,"
					. " $this->renewal_date,"
					. " $this->discount_id,"
					. " $this->reminder,"
					. " '',"
					. " '',"
					. " NOW(),"
					. " NOW(),"
					. " $this->title,"
					. " $this->title,"
					. " $this->claim_disable,"
					. " $this->friendly_url,"
					. " $this->email,"
					. " $this->url,"
					. " $this->display_url,"
					. " $this->address,"
					. " $this->address2,"
					. " $this->zip_code,"
					. " $this->phone,"
					. " $this->fax,"
					. " $this->description,"
					. " $aux_seoDescription,"
					. " $this->long_description,"
					. " $this->video_snippet,"
					. " $this->video_description,"
					. " $this->keywords,"
					. " ".str_replace(" || ", ", ", $this->keywords).","
					. " $this->attachment_file,"
					. " $this->attachment_caption,"
					. " $this->features,"
					. " $this->price,"
					. " $this->facebook_page,"
					. " $this->status,"
					. " $this->level,"
					. " $this->hours_work,"
					. " $this->locations,"
					. " $this->listingtemplate_id,"
                    . " $this->custom_text0,"
					. " $this->custom_text1,"
					. " $this->custom_text2,"
					. " $this->custom_text3,"
					. " $this->custom_text4,"
					. " $this->custom_text5,"
					. " $this->custom_text6,"
					. " $this->custom_text7,"
					. " $this->custom_text8,"
					. " $this->custom_text9,"
					. " $this->custom_short_desc0,"
					. " $this->custom_short_desc1,"
					. " $this->custom_short_desc2,"
					. " $this->custom_short_desc3,"
					. " $this->custom_short_desc4,"
					. " $this->custom_short_desc5,"
					. " $this->custom_short_desc6,"
					. " $this->custom_short_desc7,"
					. " $this->custom_short_desc8,"
					. " $this->custom_short_desc9,"
					. " $this->custom_long_desc0,"
					. " $this->custom_long_desc1,"
					. " $this->custom_long_desc2,"
					. " $this->custom_long_desc3,"
					. " $this->custom_long_desc4,"
					. " $this->custom_long_desc5,"
					. " $this->custom_long_desc6,"
					. " $this->custom_long_desc7,"
					. " $this->custom_long_desc8,"
					. " $this->custom_long_desc9,"
                    . " $this->custom_checkbox0,"
					. " $this->custom_checkbox1,"
					. " $this->custom_checkbox2,"
					. " $this->custom_checkbox3,"
					. " $this->custom_checkbox4,"
					. " $this->custom_checkbox5,"
					. " $this->custom_checkbox6,"
					. " $this->custom_checkbox7,"
					. " $this->custom_checkbox8,"
					. " $this->custom_checkbox9,"
					. " $this->custom_dropdown0,"
					. " $this->custom_dropdown1,"
					. " $this->custom_dropdown2,"
					. " $this->custom_dropdown3,"
					. " $this->custom_dropdown4,"
					. " $this->custom_dropdown5,"
					. " $this->custom_dropdown6,"
					. " $this->custom_dropdown7,"
					. " $this->custom_dropdown8,"
					. " $this->custom_dropdown9,"
					. " $this->number_views,"
					. " $this->avg_review,"
					. " $this->latitude,"
					. " $this->longitude,"
					. " $this->map_zoom,"
					. " $this->package_id,"
					. " $this->package_price,"
					. " $this->backlink,"
					. " $this->backlink_url,"
					. " $this->clicktocall_number,"
					. " $this->clicktocall_extension,"
					. " $this->clicktocall_date,"
					. " $this->activation_date,"
					. " $this->bedroom,"
					. " $this->bedsize,"
					. " $this->sleeps,"
					. " $this->bathroom,"
					. " $this->property_type,"
					. " $this->view,"
					. " $this->distance_beach,"
					. " $this->development_name,"
                    . " $this->development_id,"
					. " $this->airport_distance,"
					. " $this->airport_abbreviation,"
					. " $this->contact_fname,"
					. " $this->contact_lname,"
					. " $this->external_link1,"
					. " $this->external_link_text1,"
					. " $this->external_link2,"
					. " $this->external_link_text2,"
					. " $this->external_link3,"
					. " $this->external_link_text3,"
					. " $this->amenity_airhockey,"
					. " $this->amenity_alarmclock,"
					. " $this->amenity_answeringmachine,"
					. " $this->amenity_arcadegames,"
					. " $this->amenity_billiards,"
					. " $this->amenity_blender,"
					. " $this->amenity_blurayplayer,"
					. " $this->amenity_books,"
					. " $this->amenity_casetteplayer,"
					. " $this->amenity_cdplayer,"
					. " $this->amenity_ceilingfan,"
					. " $this->amenity_childshighchair,"
					. " $this->amenity_coffeemaker,"
					. " $this->amenity_communalpool,"
					. " $this->amenity_computer,"
					. " $this->amenity_cookware,"
					. " $this->amenity_cookingrange,"
					. " $this->amenity_deckfurniture,"
					. " $this->amenity_dishwasher,"
					. " $this->amenity_dishes,"
					. " $this->amenity_dvdplayer,"
					. " $this->amenity_exercisefacilities,"
					. " $this->amenity_foosball,"
					. " $this->amenity_gametable,"
					. " $this->amenity_games,"
					. " $this->amenity_grill,"
					. " $this->amenity_hairdryer,"
					. " $this->amenity_icemaker,"
					. " $this->amenity_internet,"
					. " $this->amenity_ironandboard,"
					. " $this->amenity_kidsgames,"
					. " $this->amenity_linensprovided,"
					. " $this->amenity_lobsterpot,"
					. " $this->amenity_microwave,"
					. " $this->amenity_minirefrigerator,"
					. " $this->amenity_mp3radiodock,"
					. " $this->amenity_outsideshower,"
					. " $this->amenity_oven,"
					. " $this->amenity_pinball,"
					. " $this->amenity_pingpong,"
					. " $this->amenity_privatepool,"
					. " $this->amenity_radio,"
					. " $this->amenity_refrigeratorfreezer,"
					. " $this->amenity_sofabed,"
					. " $this->amenity_stereo,"
					. " $this->amenity_telephone,"
					. " $this->amenity_television,"
					. " $this->amenity_toaster,"
					. " $this->amenity_toasteroven,"
					. " $this->amenity_towelsprovided,"
					. " $this->amenity_toys,"
					. " $this->amenity_utensils,"
					. " $this->amenity_vacuum,"
					. " $this->amenity_vcr,"
					. " $this->amenity_videogameconsole,"
					. " $this->amenity_videogames,"
					. " $this->amenity_washerdryer,"
					. " $this->amenity_wifi,"
					. " $this->feature_airconditioning,"
					. " $this->feature_balcony,"
					. " $this->feature_barbecuecharcoal,"
					. " $this->feature_barbecuegas,"
					. " $this->feature_boatslip,"
					. " $this->feature_cablesatellitetv,"
					. " $this->feature_clubhouse,"
					. " $this->feature_coveredparking,"
					. " $this->feature_deck,"
					. " $this->feature_diningroom,"
					. " $this->feature_elevator,"
					. " $this->feature_familyroom,"
					. " $this->feature_fullkitchen,"
					. " $this->feature_garage,"
					. " $this->feature_gasfireplace,"
					. " $this->feature_gatedcommunity,"
					. " $this->feature_wheelchairaccess,"
					. " $this->feature_heated,"
					. " $this->feature_heatedpool,"
					. " $this->feature_hottubjacuzzi,"
					. " $this->feature_internetaccess,"
					. " $this->feature_kitchenette,"
					. " $this->feature_livingroom,"
					. " $this->feature_loft,"
					. " $this->feature_onsitesecurity,"
					. " $this->feature_patio,"
					. " $this->feature_petsallowed,"
					. " $this->feature_playroom,"
					. " $this->feature_pool,"
					. " $this->feature_porch,"
					. " $this->feature_rooftopdeck,"
					. " $this->feature_sauna,"
					. " $this->feature_smokingpermitted,"
					. " $this->feature_woodfireplace,"
					. " $this->activity_antiquing,"
					. " $this->activity_basketballcourt,"
					. " $this->activity_beachcombing,"
					. " $this->activity_bicycling,"
					. " $this->activity_bikerentals,"
					. " $this->activity_birdwatching,"
					. " $this->activity_boatrentals,"
					. " $this->activity_boating,"
					. " $this->activity_botanicalgarden,"
					. " $this->activity_canoe,"
					. " $this->activity_churches,"
					. " $this->activity_cinemas,"
					. " $this->activity_bikesprovided,"
					. " $this->activity_deepseafishing,"
					. " $this->activity_fishing,"
					. " $this->activity_fitnesscenter,"
					. " $this->activity_golf,"
					. " $this->activity_healthbeautyspa,"
					. " $this->activity_hiking,"
					. " $this->activity_horsebackriding,"
					. " $this->activity_horseshoes,"
					. " $this->activity_hotairballooning,"
					. " $this->activity_iceskating,"
					. " $this->activity_jetskiing,"
					. " $this->activity_kayaking,"
					. " $this->activity_livetheater,"
					. " $this->activity_marina,"
					. " $this->activity_miniaturegolf,"
					. " $this->activity_mountainbiking,"
					. " $this->activity_museums,"
					. " $this->activity_paddleboating,"
					. " $this->activity_paragliding,"
					. " $this->activity_parasailing,"
					. " $this->activity_playground,"
					. " $this->activity_recreationcenter,"
					. " $this->activity_restaurants,"
					. " $this->activity_rollerblading,"
					. " $this->activity_sailing,"
					. " $this->activity_shelling,"
					. " $this->activity_shopping,"
					. " $this->activity_sightseeing,"
					. " $this->activity_skiing,"
					. " $this->activity_bayfishing,"
					. " $this->activity_spa,"
					. " $this->activity_surffishing,"
					. " $this->activity_surfing,"
					. " $this->activity_swimming,"
					. " $this->activity_tennis,"
					. " $this->activity_themeparks,"
					. " $this->activity_walking,"
					. " $this->activity_waterparks,"
					. " $this->activity_waterskiing,"
					. " $this->activity_watertubing,"
					. " $this->activity_wildlifeviewing,"
					. " $this->activity_zoo,"
					. " $this->isppilisting,"
					. " $this->deposit_amount,"
					. " $this->tax_per_inquiry,"
					. " $this->unique_email,"
					. " $this->is_featured,"
					. " $this->featured_date,"
					. " $this->search_pos,"
					. " $this->search_pos_date,"
					. " $this->required_stay)";
					self::insertSql($sql);
                    $sql="Select max(id) as id from Listing";
                    $tempId  = Self::fetch($sql);
                    if($tempId[0]->id)
                    {
                        $this->id = $tempId[0]->id;
                    }
				    
                   
				if (Session::get('SESS_ACCOUNT_ID') || strpos($_SERVER["PHP_SELF"],"order_") !== false){
					$this->activity_newActivity(1, $this->account_id, 0, "newitem", "listing", $this->title);
				}

				if ($this->status == "'P'"){
					//$this->activity_newToApproved(1, $this->id, "listing", $this->title);
				}

				$this->domain_updateDashboard("number_listings","inc", 0, 1);

				/*
				 * Populate Listings to front
				 */
				unset($listingSummaryObj);
				$listingSummaryObj = new ListingSummary();
				/*
				 * Used to package
				 */
				$this->prepareToUse(); //prevent some fields to be saved with empty quotes
				if(is_numeric($this->domain_id)){
					$listingSummaryObj->setNumber("domain_id",$this->domain_id);
				}else{
					$listingSummaryObj->domain_id = '';
				}


				$listingSummaryObj->PopulateTable($this->id, "insert");

				//Reload the Listing object variables

				$sql = "SELECT * FROM Listing WHERE id = $this->id";
				$row = Self::fetch($sql);
				$this->makeFromRow($row);
				$this->prepareToSave();

				$this_status = $this->status;
				$this_id = $this->id;
				$this_status = str_replace("\"", "", $this_status);
				$this_status = str_replace("'", "", $this_status);
				$this_id = str_replace("\"", "", $this_id);
				$this_id = str_replace("'", "", $this_id);
				$this->system_countActiveListingByCategory($this_id);

				/*
				 * Save to featured temp
				 */
				$this->SaveToFeaturedTemp();

				if ($aux_account != 0) {
					$this->domain_SaveAccountInfoDomain($aux_account, $this);
				}

			}

            $this->prepareToUse();

            /**
             * Save listing_id on Promotion table
             */
            if($this->promotion_id != "0"){
                unset($promotionObj);
                $promotionObj = new Promotion($this->promotion_id);
                $promotionObj->setListingId($this);
            }
                        
			//$this->setFullTextSearch();
		}

		/**
		 * <code>
		 *		//Using this in forms or other pages.
		 *		$listingObj->Delete();
		 * <code>
		 * @copyright Copyright 2005 Arca Solutions, Inc.
		 * @author Arca Solutions, Inc.
		 * @version 8.0.00
		 * @name Delete
		 * @access Public
		 */
		function Delete($domain_id = false, $update_count = true) {
			$dbMain = db_getDBObject(DEFAULT_DB, true);
			if ($domain_id) {
				$dbObj = db_getDBObjectByDomainID($domain_id, $dbMain);
				$domain_extra_file_dir = EDIRECTORY_ROOT."/custom/domain_$domain_id/extra_files/";
			} else {
				if (defined("SELECTED_DOMAIN_ID")) {
					$dbObj = db_getDBObjectByDomainID(SELECTED_DOMAIN_ID, $dbMain);
				} else {
					$dbObj = db_getDBObject();
				}
				$domain_extra_file_dir = EXTRAFILE_DIR;
				unset($dbMain);
			}

			### LISTING CATEGORY STATUS
			if ($this->status != "P") {
				$sql = "UPDATE Listing SET status = 'P' WHERE id = $this->id";
				$dbObj->query($sql);
			}

			if (SHOW_CATEGORY_COUNT == "on" && $update_count) system_countActiveListingByCategory($this->id, false, $domain_id);

			### REVIEWS
			$sql = "SELECT id FROM Review WHERE item_type='listing' AND item_id= $this->id";
			$result = $dbObj->query($sql);
			while ($row = mysql_fetch_assoc($result)) {
				$reviewObj = new Review($row["id"]);
				$reviewObj->Delete($domain_id);
			}

			### LISTING_CATEOGRY
			$sql = "DELETE FROM Listing_Category WHERE listing_id = $this->id";
			$dbObj->query($sql);
            
			### CHOICES
			$sql = "DELETE FROM Listing_Choice WHERE listing_id = $this->id";
			$dbObj->query($sql);

			### GALERY
            $sql = "SELECT gallery_id FROM Gallery_Item WHERE item_id = $this->id AND item_type = 'listing'";
            $row = mysql_fetch_array($dbObj->query($sql));
            $gallery_id = $row["gallery_id"]; 
            if ($gallery_id) {
                $gallery = new Gallery($gallery_id);
                $gallery->delete();
            }

			### IMAGE
			if ($this->image_id) {
				$image = new Image($this->image_id);
				if ($image) $image->Delete($domain_id);
			}
			if ($this->thumb_id) {
				$image = new Image($this->thumb_id);
				if ($image) $image->Delete($domain_id);
			}

			### ATTACHMENT
			if ($this->attachment_file) {
				if (file_exists($domain_extra_file_dir.$this->attachment_file)) {
					@unlink($domain_extra_file_dir.$this->attachment_file);
				}
			}

			### INVOICE
			$sql = "UPDATE Invoice_Listing SET listing_id = '0' WHERE listing_id = $this->id";
			$dbObj->query($sql);

			### PAYMENT
			$sql = "UPDATE Payment_Listing_Log SET listing_id = '0' WHERE listing_id = $this->id";
			$dbObj->query($sql);

			### CLAIM
			$sql = "UPDATE Claim SET status = 'incomplete' WHERE listing_id = $this->id AND status = 'progress'";
			$dbObj->query($sql);
			$sql = "UPDATE Claim SET listing_id = '0' WHERE listing_id = $this->id";
			$dbObj->query($sql);

			### CheckIn
			$sql = "DELETE FROM CheckIn WHERE item_id = $this->id";
			$dbObj->query($sql);

			### Rate Info
			$sql = "DELETE FROM Rate WHERE listing_id = $this->id";
			$dbObj->query($sql);

            ### Rate
			$sql = "DELETE FROM Rate WHERE listing_id = $this->id";
			$dbObj->query($sql);

            ### Availability Booking
			$sql = "DELETE FROM ListingAvailabilityBooking WHERE listing_id = $this->id";
			$dbObj->query($sql);

			### ImportImagens
			$sql = "DELETE FROM ImportImagens WHERE listing_id = $this->id";
			$dbObj->query($sql);

            ### Promotion
            $sql = "UPDATE Promotion SET    fulltextsearch_where = '',
                                            listing_id = 0, 
                                            listing_status = '', 
                                            listing_level = 0, 
                                            listing_location1 = 0, 
                                            listing_location2 = 0, 
                                            listing_location3 = 0, 
                                            listing_location4 = 0, 
                                            listing_location5 = 0, 
                                            listing_address = '', 
                                            listing_address2 = '', 
                                            listing_zipcode = '', 
                                            listing_zip5 = '0', 
                                            listing_latitude = '', 
                                            listing_longitude = ''
                   WHERE listing_id = $this->id";
            $dbObj->query($sql);

			/*
			 * Populate Listings to front
			 */
			unset($listingSummaryObj);
			$listingSummaryObj = new ListingSummary();
			$listingSummaryObj->Delete($this->id);

			### LISTING
			$sql = "DELETE FROM Listing WHERE id = $this->id";
			$dbObj->query($sql);

			if ($domain_id){
				$domain_idDash = $domain_id;
			} else {
				$domain_idDash = SELECTED_DOMAIN_ID;
			}

			domain_updateDashboard("number_listings", "dec", 0, $domain_idDash);

			activity_deleteRecord($domain_idDash, $this->id, "listing");

		}

		/**
		 * <code>
		 *		//Using this in forms or other pages.
		 *		$listingObj->updateImage($imageArray);
		 * <br /><br />
		 *		//Using this in Listing() class.
		 *		$this->updateImage($imageArray);
		 * </code>
		 * @copyright Copyright 2005 Arca Solutions, Inc.
		 * @author Arca Solutions, Inc.
		 * @version 8.0.00
		 * @name updateImage
		 * @access Public
		 * @param array $imageArray
		 */
		function updateImage($imageArray) {
			unset($imageObj);
			if ($this->image_id) {
				$imageobj = new Image($this->image_id);
				if ($imageobj) $imageobj->delete();
			}
			$this->image_id = $imageArray["image_id"];
			unset($imageObj);
			if ($this->thumb_id) {
				$imageObj = new Image($this->thumb_id);
				if ($imageObj) $imageObj->delete();
			}
			$this->thumb_id = $imageArray["thumb_id"];
			unset($imageObj);
		}

		/**
		 * <code>
		 *		//Using this in forms or other pages.
		 *		$listingObj->getCategories(...);
		 * <br /><br />
		 *		//Using this in Listing() class.
		 *		$this->getCategories(...);
		 * </code>
		 * @copyright Copyright 2005 Arca Solutions, Inc.
		 * @author Arca Solutions, Inc.
		 * @version 8.0.00
		 * @name getCategories
		 * @access Public
		 * @param boolean $have_data
		 * @param array $data
		 * @param integer $id
		 * @param boolean $getAll
		 * @param boolean $object
		 * @param boolean $bulk
		 * @return array $categories
		 */
		function getCategories($have_data = false, $data = false, $id = false, $getAll = false, $object=false, $bulk=false) {
			
			
			if ($have_data) {
				if ($data["cat_1_id"]) $ids[] = $data["cat_1_id"];
				if ($data["cat_2_id"]) $ids[] = $data["cat_2_id"];
				if ($data["cat_3_id"]) $ids[] = $data["cat_3_id"];
				if ($data["cat_4_id"]) $ids[] = $data["cat_4_id"];
				if ($data["cat_5_id"]) $ids[] = $data["cat_5_id"];

				if (is_array($ids)) {
					$ids = array_unique($ids);
					$sql = "SELECT * FROM ListingCategory WHERE id IN (".implode(",", $ids).")";
					$r = Self::fetch($sql);
					foreach($r as $row) {
						$categories[] = new ListingCategory($row);
					}
				}

			} else {
				if(!$id){
					$id = $this->id ;
				}
				if($id){


					$sql_main = "SELECT category.root_id,
										listing_category.category_id
										FROM Listing_Category listing_category
										INNER JOIN ListingCategory category ON category.id = listing_category.category_id
										WHERE listing_category.listing_id = ".$id." AND category.root_id > 0";

					$result_main = Self::fetch($sql_main);
					if($result_main){

						$aux_array_categories = array();
						foreach($result_main as $row){
							if (!$object && !$bulk) {
								$aux_array_categories[] = $row->root_id;
							}
							if ($getAll) {
								$aux_array_categories[] = $row->category_id;
							}
						}

						if(count($aux_array_categories) > 0){
							$sql = "SELECT	id,
											title,
											page_title,
											friendly_url,
											enabled,
											category_id
										FROM ListingCategory
										WHERE id IN (".implode(",",$aux_array_categories).")";

                             
                            if(!$object){
                                $result = Self::fetch($sql);
                            }else{
                                $result = Self::fetch($sql);
                            }


							
							//if(mysql_num_rows($result) > 0){
							if($result){
								$categories = array();
								foreach($result as $row){
									if (!$object){
										$categories[] =$cat = new ListingCategory();
										$cat->ListingCategory($row);
                                    } else {

										$categories[] = $row;
                                    }
								}



							}
						}
					}
				}
			}

			if(isset($categories) && count($categories) > 0){
				return $categories;
			}else{
				return false;
			}
		}

		/**
		 * <code>
		 *		//Using this in forms or other pages.
		 *		$listingObj->setCategories($categories);
		 * <br /><br />
		 *		//Using this in Listing() class.
		 *		$this->setCategories($categories);
		 * </code>
		 * @copyright Copyright 2005 Arca Solutions, Inc.
		 * @author Arca Solutions, Inc.
		 * @version 8.0.00
		 * @name setCategories
		 * @access Public
		 * @param array $array
		 */
		function setCategories($array) {
			
			if ($this->id) {
				//$this->system_countActiveListingByCategory($this->id);

				$sql = "DELETE FROM Listing_Category WHERE listing_id = ".$this->id;
				Self::fetch($sql);

				if ($array) {

					foreach ($array as $category) {

						if ($category) {

							$lCatObj = new ListingCategory();
                            $lCatObj->ListingCategory($category);
							$root_id = $lCatObj->getNumber("root_id");
							$left = $lCatObj->getNumber("left");
							$right = $lCatObj->getNumber("right");

							$l_catObj = new Listing_Category();
                          	$l_catObj->Listing_Category();

							$l_catObj->setNumber("listing_id", $this->id);
							$l_catObj->setNumber("category_id", $category);
							$l_catObj->setString("status", $this->status);
							$l_catObj->setNumber("category_root_id", $root_id);
							$l_catObj->setNumber("category_node_left", $left);
							$l_catObj->setNumber("category_node_right", $right);
							$l_catObj->Save();
							
						}
					}
				}

				//$this->setFullTextSearch();
				//$this->system_countActiveListingByCategory($this->id);
			}
		}
        
        /**
		 * <code>
		 *		//Using this in forms or other pages.
		 *		$listingObj->updateCategoryStatusByID($categories);
		 * <br /><br />
		 *		//Using this in Listing() class.
		 *		$this->updateCategoryStatusByID($categories);
		 * </code>
		 * @copyright Copyright 2005 Arca Solutions, Inc.
		 * @author Arca Solutions, Inc.
		 * @version 8.0.00
		 * @name updateCategoryStatusByID
		 * @access Public
		 */
		function updateCategoryStatusByID() {
			
			$sql_update = "UPDATE Listing_Category SET status = $this->status WHERE listing_id = $this->id";
			Self::updateSql($sql_update);
		}

		/**
		 * <code>
		 *		//Using this in forms or other pages.
		 *		$listingObj->retrieveListingsbyPromotion_id($promotion_id);
		 * <br /><br />
		 *		//Using this in Listing() class.
		 *		$this->retrieveListingsbyPromotion_id($promotion_id);
		 * </code>
		 * @copyright Copyright 2005 Arca Solutions, Inc.
		 * @author Arca Solutions, Inc.
		 * @version 8.0.00
		 * @name retrieveListingsbyPromotion_id
		 * @access Public
		 * @param integer $promotion_id
		 * @return array $listings
		 */
		function retrieveListingsbyPromotion_id($promotion_id) {
			$dbMain = db_getDBObject(DEFAULT_DB, true);
			if (defined("SELECTED_DOMAIN_ID")) {
				$dbObj = db_getDBObjectByDomainID(SELECTED_DOMAIN_ID, $dbMain);
			} else {
				$dbObj = db_getDBObject();
			}

			unset($dbMain);
			$sql = "SELECT * FROM Listing WHERE promotion_id = $promotion_id";
			$r = $dbObj->query($sql);
			while ($row = mysql_fetch_assoc($r)) {
				$listings[] = new Listing($row["id"]);
			}
			return $listings;
		}

		/**
		 * <code>
		 *		//Using this in forms or other pages.
		 *		$listingObj->getPrice();
		 * <br /><br />
		 *		//Using this in Listing() class.
		 *		$this->getPrice();
		 * </code>
		 * @copyright Copyright 2005 Arca Solutions, Inc.
		 * @author Arca Solutions, Inc.
		 * @version 8.0.00
		 * @name getPrice
		 * @access Public
		 * @return double $price
		 */
		function getPrice() {
            //return false;//written by me
			$price = 0;

		
			
			 //Check if have price by package
			//$levelObj = new ListingLevel();
			$levelObj = new ListingLevel();
			$levelObj->ListingLevel(true);
			if($this->package_id){
				$price = $this->package_price;
			}else{
				$price = $price + $levelObj->getPrice($this->level);
			}

			$sql = "SELECT COUNT(id) AS total FROM Listing_Category WHERE listing_id = ".$this->id;
			$result = self::fetch($sql);
			if(count($result) > 0){
				
				$category_amount = $result[0]->total;
			}
			
			if(isset($this->categories) && !$this->id){
				$category_amount = $this->categories;
			}
			/*
			if (($category_amount > 0) && (($category_amount - $levelObj->getFreeCategory($this->level)) > 0)) {
				$extra_category_amount = $category_amount - $levelObj->getFreeCategory($this->level);
			} else {
				$extra_category_amount = 0;
			}

			if ($extra_category_amount > 0) $price = $price + ($levelObj->getCategoryPrice($this->level) * $extra_category_amount);

			if (LISTINGTEMPLATE_FEATURE == "on" && CUSTOM_LISTINGTEMPLATE_FEATURE == "on") {
				if ($this->listingtemplate_id) {
					$listingTemplateObj = new ListingTemplate($this->listingtemplate_id);
					if ($listingTemplateObj->getString("status") == "enabled") {
						$price = $price + $listingTemplateObj->getString("price");
					} else {
						$sql = "UPDATE Listing SET listingtemplate_id = 0 WHERE id = ".$this->id;
						$result = self::updateSql($sql);

						/*
						 * Populate Listings to front
						
						$sql = "UPDATE Listing_Summary SET
									listingtemplate_id = 0,
									template_layout_id = 0,
									template_cat_id = 0,
									template_title = '',
									template_status = '',
									template_price = 0
								WHERE id = $this->id";
						$result = self::updateSql($sql);
					}
				}
			}*/

			if ($this->discount_id) {

				$discountCodeObj = new DiscountCode();
				$discountCodeObj->DiscountCode($this->discount_id);

				if (Functions::is_valid_discount_code($this->discount_id, "listing", $this->id, $discount_message, $discount_error)) {

			if ($discountCodeObj->id && $discountCodeObj->expire_date >= date('Y-m-d')) {

						if ($discountCodeObj->type == "percentage") {
							$price = $price * (1 - $discountCodeObj->amount/100);
						} elseif ($discountCodeObj->type== "monetary value") {
							$price = $price - $discountCodeObj->amount;
						}

					}
				}
			}

					/* elseif (($discountCodeObj->type == 'percentage' && $discountCodeObj->amount == '100.00') || ($discountCodeObj->type == 'monetary value' && $discountCodeObj->amount > $price)) {

						$this->status = 'E';
						$this->renewal_date = $discountCodeObj->expire_date;
						$sql = "UPDATE Listing SET status = 'E', renewal_date = '".$discountCodeObj->expire_date."', discount_id = '' WHERE id = ".$this->id;
						$result = self::updateSql($sql);
                        
                        $sql = "UPDATE Promotion SET listing_status = 'E' WHERE listing_id = ".$this->id;
						$result = self::updateSql($sql);

						/*
						 * //Populate Listings to front
						 
						$sql = "UPDATE Listing_Summary SET
									status = 'E',
									renewal_date = '".$discountCodeObj->expire_date."'
								WHERE id = $this->id";
						$result = self::updateSql($sql);
					}

				} else {

					if ( ($discountCodeObj->type == 'percentage' && $discountCodeObj->amount == '100.00') || ($discountCodeObj->type == 'monetary value' && $discountCodeObj->amount > $price) ) {
						$this->status = 'E';
						$this->renewal_date = $discountCodeObj->expire_date;
						$sql = "UPDATE Listing SET status = 'E', renewal_date = '".$discountCodeObj->expire_date."', discount_id = '' WHERE id = ".$this->id;

						/*
						 //Populate Listings to front
						 
						$sql2 = "UPDATE Listing_Summary SET
									status = 'E',
									renewal_date = '".$discountCodeObj->expire_date."'
								WHERE id = $this->id";
						$result = self::updateSql($sql2);
                        
                        $sql3 = "UPDATE Promotion SET listing_status = 'E' WHERE listing_id = ".$this->id;
						$result = self::updateSql($sql3);
                        
					} else {
						$sql = "UPDATE Listing SET discount_id = '' WHERE id = ".$this->id;
					}
					$result = self::updateSql($sql);
				}

			}*/


			if ($price <= 0) $price = 0;
			return $price;

		}
		function getListingPrice($level=0) {
            //return false;//written by me
			$price = 0;


			$sql = "SELECT * FROM ListingLevel WHERE theme='default' and value = ".$level;
	  		$total = self::fetch($sql);
			$price = (isset($total[0])?$total[0]->price:0);

			if ($price <= 0) $price = 0;

			return $price;

		}

		/**
		 * <code>
		 *		//Using this in forms or other pages.
		 *		$listingObj->hasRenewalDate();
		 * <br /><br />
		 *		//Using this in Listing() class.
		 *		$this->hasRenewalDate();
		 * </code>
		 * @copyright Copyright 2005 Arca Solutions, Inc.
		 * @author Arca Solutions, Inc.
		 * @version 8.0.00
		 * @name hasRenewalDate
		 * @access Public
		 * @return boolean
		 */
		function hasRenewalDate() {

			if (config('params.PAYMENT_FEATURE') != "on") 
				return false;


			if ((config('params.CREDITCARDPAYMENT_FEATURE') != "on") && (config('params.INVOICEPAYMENT_FEATURE') != "on") && (config('params.MANUALPAYMENT_FEATURE') != "on")) 
				return false;

			if ($this->getPrice() <= 0)
				return false;
			
			return true;
		}

		/**
		 * <code>
		 *		//Using this in forms or other pages.
		 *		$listingObj->needToCheckOut();
		 * <br /><br />
		 *		//Using this in Listing() class.
		 *		$this->needToCheckOut();
		 * </code>
		 * @name needToCheckOut
		 * @access Public
		 * @return boolean
		 */
		function needToCheckOut($isppilisting = false) {
			//&& ($this->getString("isppilisting") == false || $isppilisting == false)
			if ($this->hasRenewalDate() ) {


				$today = date("Y-m-d");
				$today = explode("-", $today);
				$today_year = $today[0];
				$today_month = $today[1];
				$today_day = $today[2];
				$timestamp_today = mktime(0, 0, 0, $today_month, $today_day, $today_year);

				$this_renewaldate = $this->renewal_date;
				$renewaldate = explode("-", $this_renewaldate);
				$renewaldate_year = $renewaldate[0];
				$renewaldate_month = $renewaldate[1];
				$renewaldate_day = $renewaldate[2];
				$timestamp_renewaldate = mktime(0, 0, 0, $renewaldate_month, $renewaldate_day, $renewaldate_year);

				if (($this->status == "E") || ($this_renewaldate == "0000-00-00") || ($timestamp_today > $timestamp_renewaldate)) {
					return true;
				}

			}

			return false;

		}

		/**
		 * <code>
		 *		//Using this in forms or other pages.
		 *		$listingObj->getNextRenewalDate($times);
		 * <br /><br />
		 *		//Using this in Listing() class.
		 *		$this->getNextRenewalDate($times);
		 * </code>
		 * @copyright Copyright 2005 Arca Solutions, Inc.
		 * @author Arca Solutions, Inc.
		 * @version 8.0.00
		 * @name getNextRenewalDate
		 * @access Public
		 * @param integer $times
		 * @return date $nextrenewaldate
		 */
		function getNextRenewalDate($times = 1) {

			$nextrenewaldate = "0000-00-00";

			if ($this->hasRenewalDate()) {

				if ($this->needToCheckOut()) {

					$today = date("Y-m-d");
					$today = explode("-", $today);
					$start_year = $today[0];
					$start_month = $today[1];
					$start_day = $today[2];

				} else {

					$this_renewaldate = $this->renewal_date;
					$renewaldate = explode("-", $this_renewaldate);
					$start_year = $renewaldate[0];
					$start_month = $renewaldate[1];
					$start_day = $renewaldate[2];

				}

				$renewalcycle = Functions::payment_getRenewalCycle("listing");
				$renewalunit = Functions::payment_getRenewalUnit("listing");

				if ($renewalunit == "Y") {
					$nextrenewaldate = date("Y-m-d", mktime(0, 0, 0, (int)$start_month, (int)$start_day, (int)$start_year+($renewalcycle*$times)));
				} elseif ($renewalunit == "M") {
					$nextrenewaldate = date("Y-m-d", mktime(0, 0, 0, (int)$start_month+($renewalcycle*$times), (int)$start_day, (int)$start_year));
				} elseif ($renewalunit == "D") {
					$nextrenewaldate = date("Y-m-d", mktime(0, 0, 0, (int)$start_month, (int)$start_day+($renewalcycle*$times), (int)$start_year));
				}

			}

			return $nextrenewaldate;

		}

		/**
		 * <code>
		 *		//Using this in forms or other pages.
		 *		$listingObj->setLocationManager($locationManager);
		 * <br /><br />
		 *		//Using this in Listing() class.
		 *		$this->setLocationManager($locationManager);
		 * </code>
		 * @copyright Copyright 2005 Arca Solutions, Inc.
		 * @author Arca Solutions, Inc.
		 * @version 8.0.00
		 * @name setLocationManager
		 * @access Public
		 * @param mixed &$locationManager
		 */
		function setLocationManager(&$locationManager) {
			$this->locationManager =& $locationManager;
		}

		/**
		 * <code>
		 *		//Using this in forms or other pages.
		 *		$listingObj->getLocationManager();
		 * <br /><br />
		 *		//Using this in Listing() class.
		 *		$this->getLocationManager();
		 * </code>
		 * @copyright Copyright 2005 Arca Solutions, Inc.
		 * @author Arca Solutions, Inc.
		 * @version 8.0.00
		 * @name getLocationManager
		 * @access Public
		 * @return mixed &$this->locationManager
		 */
		function &getLocationManager() {
			return $this->locationManager; /* NEVER auto-instantiate this*/
		}

		/**
		 * <code>
		 *		//Using this in forms or other pages.
		 *		$listingObj->getLocationString(...);
		 * <br /><br />
		 *		//Using this in Listing() class.
		 *		$this->getLocationString(...);
		 * </code>
		 * @copyright Copyright 2005 Arca Solutions, Inc.
		 * @author Arca Solutions, Inc.
		 * @version 8.0.00
		 * @name getLocationString
		 * @access Public
		 * @param varchar $format
		 * @param boolean $forceManagerCreation
		 * @return string locationString
		 */
		function getLocationString($format, $forceManagerCreation = false, $lineBreak = true) {
			if($forceManagerCreation && !$this->locationManager) $this->locationManager = new LocationManager();
			return db_getLocationString($this, $format, true, $lineBreak);
		}

		/**
		 * <code>
		 *		//Using this in forms or other pages.
		 *		$listingObj->setFullTextSearch();
		 * <br /><br />
		 *		//Using this in Listing() class.
		 *		$this->setFullTextSearch();
		 * </code>
		 * @copyright Copyright 2005 Arca Solutions, Inc.
		 * @author Arca Solutions, Inc.
		 * @version 8.0.00
		 * @name setFullTextSearch
		 * @access Public
		 */
		function setFullTextSearch() {

			

			// Custom
			$fulltextsearch_where[] = $this->id;
			$fulltextsearch_keyword[] = $this->id;

			if ($this->title) {
				$fulltextsearch_keyword[] = $this->title;
                $addkeyword=$this->format_addApostWords($this->title);
                if ($addkeyword) $fulltextsearch_keyword[] = $addkeyword;
                unset($addkeyword);
			}      

            if ($this->keywords) {
                $string=str_replace(" || ", " ", $this->keywords);
                $fulltextsearch_keyword[] = $string;
                $addkeyword=$this->format_addApostWords($string);
                if ($addkeyword!='')  $fulltextsearch_keyword[] =$addkeyword;
                unset($addkeyword);
            }

            if ($this->description) {
                $fulltextsearch_keyword[] = substr($this->description, 0, 100);
            }

			if ($this->address) {
				$fulltextsearch_where[] = $this->address;
			}

			if ($this->zip_code) {
				$fulltextsearch_where[] = $this->zip_code;
			}

			$_locations = explode(",", "1,3,4");
			//foreach ($_locations as $each_location) {
				//unset ($objLocation);
				//$objLocationLabel = "Location".$each_location;
				//$attributeLocation = 'location_'.$each_location;
				
                
                	$objLocation = new Location1;
                    $attributeLocation = 'id';
                    
				$objLocation->SetString("id", $attributeLocation);
				$locationsInfo = $objLocation->retrieveLocationById();
                
				if ($locationsInfo[0]->id) {
					$fulltextsearch_where[] = $locationsInfo[0]->name;
					if ($locationsInfo[0]->abbreviation) {
						$fulltextsearch_where[] = $locationsInfo[0]->abbreviation;
					}
				}
                
                unset ($objLocation);
                	$objLocation = new Location3;
				$objLocation->SetString("id", 'id');
				$locationsInfo = $objLocation->retrieveLocationById();
				if ($locationsInfo[0]->id) {
					$fulltextsearch_where[] = $locationsInfo[0]->name;
					if ($locationsInfo[0]->abbreviation) {
						$fulltextsearch_where[] = $locationsInfo[0]->abbreviation;
					}
				}
                
                unset ($objLocation);
                	$objLocation = new Location4;
				$objLocation->SetString("id", 'id');
				$locationsInfo = $objLocation->retrieveLocationById();
				if ($locationsInfo[0]->id) {
					$fulltextsearch_where[] = $locationsInfo[0]->name;
					if ($locationsInfo[0]->abbreviation) {
						$fulltextsearch_where[] = $locationsInfo[0]->abbreviation;
					}
				}
                
                
                
			//}

			$categories = $this->getCategories(false, false, $this->id, true, true);
			if ($categories) {
				foreach ($categories as $category) {
					unset($parents);
					$category_id = $category->id;
					while ($category_id != 0) {
						$sql = "SELECT * FROM ListingCategory WHERE id = $category_id";
						$result = $dbObj->query($sql);
						if (mysql_num_rows($result) > 0) {
							$category_info = mysql_fetch_assoc($result);

                            if ($category_info["enabled"] == "y") {
                                if ($category_info["title"]) {
                                    $fulltextsearch_keyword[] = $category_info["title"];
                                }

                                if ($category_info["keywords"]) {
                                    $fulltextsearch_keyword[] = str_replace(array("\r\n", "\n"), " ", $category_info["keywords"]);
                                }
                            }

							$category_id = $category_info["category_id"];
						} else {
							$category_id = 0;
						}
					}
				}
			}

			if (is_array($fulltextsearch_keyword)) {
				$fulltextsearch_keyword_sql = $this->db_formatString(implode(" ", $fulltextsearch_keyword));
				// Custom
				if (is_array($fulltextsearch_where)) 
					$fulltextsearch_keyword_sql .= $this->db_formatString(implode(" ", $fulltextsearch_where));
				$sql = "UPDATE Listing SET fulltextsearch_keyword = $fulltextsearch_keyword_sql WHERE id = $this->id";
				$result = Self::updateSql($sql);

				$sql = "UPDATE Listing_Summary SET fulltextsearch_keyword = $fulltextsearch_keyword_sql WHERE id = $this->id";
				$result = Self::updateSql($sql);

			}
			if (is_array($fulltextsearch_where)) {
				$fulltextsearch_where_sql = $this->db_formatString(implode(" ", $fulltextsearch_where));
				$sql = "UPDATE Listing SET fulltextsearch_where = $fulltextsearch_where_sql WHERE id = $this->id";
				$result = Self::updateSql($sql);

				$sql = "UPDATE Listing_Summary SET fulltextsearch_where = $fulltextsearch_where_sql WHERE id = $this->id";
				$result = Self::updateSql($sql);
                
                $sql = "UPDATE Promotion SET fulltextsearch_where = $fulltextsearch_where_sql WHERE listing_id = $this->id";
				$result = Self::updateSql($sql);
			}

		}

		/**
		 * <code>
		 *		//Using this in forms or other pages.
		 *		$listingObj->getGalleries();
		 * <br /><br />
		 *		//Using this in Listing() class.
		 *		$this->getGalleries();
		 * </code>
		 * @copyright Copyright 2005 Arca Solutions, Inc.
		 * @author Arca Solutions, Inc.
		 * @version 8.0.00
		 * @name getGalleries
		 * @access Public
		 * @return array $galleries
		 */
		function getGalleries() {
			$dbMain = db_getDBObject(DEFAULT_DB, true);
			if (defined("SELECTED_DOMAIN_ID")) {
				$dbObj = db_getDBObjectByDomainID(SELECTED_DOMAIN_ID, $dbMain);
			} else {
				$dbObj = db_getDBObject();
			}

			unset($dbMain);
			$sql = "SELECT * FROM Gallery_Item WHERE item_type='listing' AND item_id = $this->id ORDER BY gallery_id";
			$r = $dbObj->query($sql);
			if ($this->id > 0) {
                while ($row = mysql_fetch_array($r)){
                    $galleries[] = $row["gallery_id"];
                }
            }
			return $galleries;
		}

		/**
		 * <code>
		 *		//Using this in forms or other pages.
		 *		$listingObj->setGalleries($gallery);
		 * <br /><br />
		 *		//Using this in Listing() class.
		 *		$this->setGalleries($gallery);
		 * </code>
		 * @copyright Copyright 2005 Arca Solutions, Inc.
		 * @author Arca Solutions, Inc.
		 * @version 8.0.00
		 * @name setGalleries
		 * @access Public
		 * @param integer $gallery
		 */
		function setGalleries($gallery = false) {
			
			
			$sql = "DELETE FROM Gallery_Item WHERE item_type='listing' AND item_id = $this->id";
			Self::deleteSql($sql);

			if ($gallery) {
				$sql = "INSERT INTO Gallery_Item (item_id, gallery_id, item_type) VALUES ($this->id, $gallery, 'listing')";
				$rs3 = self::insertSql($sql);
			}
		}

		/**
		 * <code>
		 *		//Using this in forms or other pages.
		 *		$listingObj->setMapTuning(...);
		 * <br /><br />
		 *		//Using this in Listing() class.
		 *		$this->setMapTuning(...);
		 * </code>
		 * @copyright Copyright 2005 Arca Solutions, Inc.
		 * @author Arca Solutions, Inc.
		 * @version 8.0.00
		 * @name setMapTuning
		 * @access Public
		 * @param varchar $latitude_longitude
		 * @param integer $map_zoom
		 */
		function setMapTuning($latitude_longitude = "", $map_zoom) {
			$dbMain = db_getDBObject(DEFAULT_DB, true);
			if (defined("SELECTED_DOMAIN_ID")) {
				$dbObj = db_getDBObjectByDomainID(SELECTED_DOMAIN_ID, $dbMain);
			} else {
				$dbObj = db_getDBObject();
			}
            unset($dbMain);
            
            $auxCoord = explode(",", $latitude_longitude);
            $latitude = $auxCoord[0];
            $longitude = $auxCoord[1];
			
			$sql = "UPDATE Listing SET latitude = ".db_formatString($latitude).", longitude = ".db_formatString($longitude).", map_zoom = ".db_formatNumber($map_zoom)." WHERE id = ".$this->id."";
			$dbObj->query($sql);
            
            $sql = "UPDATE Promotion SET listing_latitude = ".db_formatString($latitude).", listing_longitude = ".db_formatString($longitude)." WHERE listing_id = ".$this->id."";
			$dbObj->query($sql);
			/*
			 * Populate Listings to front
			 */
			unset($listingSummaryObj);
			$listingSummaryObj = new ListingSummary();
			$listingSummaryObj->PopulateTable($this->id, "update");
		}

		/**
		 * <code>
		 *		//Using this in forms or other pages.
		 *		$listingObj->setNumberViews($id);
		 * <br /><br />
		 *		//Using this in Listing() class.
		 *		$this->setNumberViews($id);
		 * </code>
		 * @copyright Copyright 2005 Arca Solutions, Inc.
		 * @author Arca Solutions, Inc.
		 * @version 8.0.00
		 * @name setNumberViews
		 * @access Public
		 * @param integer $id
		 */
		function setNumberViews($id) {
			$dbMain = db_getDBObject(DEFAULT_DB, true);
			if (defined("SELECTED_DOMAIN_ID")) {
				$dbObj = db_getDBObjectByDomainID(SELECTED_DOMAIN_ID, $dbMain);
			} else {
				$dbObj = db_getDBObject();
			}

			unset($dbMain);
			$sql = "UPDATE Listing SET number_views = ".$this->number_views." + 1 WHERE Listing.id = ".$id;
			$dbObj->query($sql);
            
            $sql = "UPDATE Listing_Summary SET number_views = ".$this->number_views." + 1 WHERE Listing_Summary.id = ".$id;
			$dbObj->query($sql);

		}

		/**
		 * <code>
		 *		//Using this in forms or other pages.
		 *		$listingObj->setAvgReview(...);
		 * <br /><br />
		 *		//Using this in Listing() class.
		 *		$this->setAvgReview(...);
		 * </code>
		 * @copyright Copyright 2005 Arca Solutions, Inc.
		 * @author Arca Solutions, Inc.
		 * @version 8.0.00
		 * @name setAvgReview
		 * @access Public
		 * @param integer $avg
		 * @param integer $id
		 */
		function setAvgReview($avg, $id) {
		
			$sql = "UPDATE Listing SET avg_review = ".$avg." WHERE Listing.id = ".$id;
			self::updateSql($sql);
			/*
			 * Populate Listings to front
			 */
			unset($listingSummaryObj);
			$listingSummaryObj = new ListingSummary();
			$listingSummaryObj->PopulateTable($id, "update");
		}

		/**
		 * <code>
		 *		//Using this in forms or other pages.
		 *		$listingObj->hasDetail();
		 * <br /><br />
		 *		//Using this in Listing() class.
		 *		$this->hasDetail();
		 * </code>
		 * @copyright Copyright 2005 Arca Solutions, Inc.
		 * @author Arca Solutions, Inc.
		 * @version 8.0.00
		 * @name hasDetail
		 * @access Public
		 * @return mixed $detail
		 */
		function hasDetail() {
			$listingLevel = new ListingLevel();
			$detail = $listingLevel->getDetail($this->level);
			unset($listingLevel);
			return $detail;
		}

		/**
		 * <code>
		 *		//Using this in forms or other pages.
		 *		$listingObj->deletePerAccount($account_id);
		 * <br /><br />
		 *		//Using this in Listing() class.
		 *		$this->deletePerAccount($account_id);
		 * </code>
		 * @copyright Copyright 2005 Arca Solutions, Inc.
		 * @author Arca Solutions, Inc.
		 * @version 8.0.00
		 * @name deletePerAccount
		 * @access Public
		 * @param integer $account_id
		 * @param integer $domain_id
		 */
		function deletePerAccount($account_id = 0, $domain_id = false) {
			if (is_numeric($account_id) && $account_id > 0) {
				
				$sql = "SELECT * FROM Listing WHERE account_id = $account_id";
				$result = self::fetch($sql);
				foreach($result as $row){
					$this->makeFromRow($row);
					$this->Delete($domain_id);
				}
			}
		}

		/**
		 * <code>
		 *		//Using this in forms or other pages.
		 *		$listingObj->SaveToFeaturedTemp();
		 * <br /><br />
		 *		//Using this in Listing() class.
		 *		$this->SaveToFeaturedTemp();
		 * </code>
		 * @copyright Copyright 2005 Arca Solutions, Inc.
		 * @author Arca Solutions, Inc.
		 * @version 8.0.00
		 * @name SaveToFeaturedTemp
		 * @access Public
		 */
		function SaveToFeaturedTemp() {
		
			$sql = "INSERT INTO Listing_FeaturedTemp (listing_id,status) VALUES (".$this->id.",'R')";
			Self::insertSql($sql);
		}

        /**
		 * <code>
		 *		//Using this in forms or other pages.
		 *		$listingObj->removePromotionID();
		 * <br /><br />
		 *		//Using this in Listing() class.
		 *		$this->removePromotionID();
		 * </code>
		 * @copyright Copyright 2005 Arca Solutions, Inc.
		 * @author Arca Solutions, Inc.
		 * @version 8.0.00
		 * @name removePromotionID
		 * @access Public
		 */
        function removePromotionID() {
            if (!$this->id){
                return false;
            }
            $dbMain = db_getDBObject(DEFAULT_DB, true);
            if (defined("SELECTED_DOMAIN_ID")) {
                $db = db_getDBObjectByDomainID(SELECTED_DOMAIN_ID, $dbMain);
            } else {
                $db = db_getDBObject();
            }
            unset($dbMain);
            /*
             * Clear Promotion table
             */
            $sql = "UPDATE Promotion SET    fulltextsearch_where = '',
                                            listing_id = 0, 
                                            listing_status = '', 
                                            listing_level = 0, 
                                            listing_location1 = 0, 
                                            listing_location2 = 0, 
                                            listing_location3 = 0, 
                                            listing_location4 = 0, 
                                            listing_location5 = 0, 
                                            listing_address = '', 
                                            listing_address2 = '', 
                                            listing_zipcode = '', 
                                            listing_zip5 = '0', 
                                            listing_latitude = '', 
                                            listing_longitude = ''
                    WHERE id = ".$this->promotion_id;
            $db->query($sql);
            
            /**
             * Clear Listing Table
             */
            $sql_1 = "UPDATE Listing SET promotion_id = 0 WHERE id = $this->id";
            $sql_2 = "UPDATE Listing_Summary SET promotion_id = 0, promotion_start_date = '0000-00-00', promotion_end_date = '0000-00-00' WHERE id = $this->id";
            if($db->query($sql_1) && $db->query($sql_2)){
                return true;
            }
        }
         
        
        /**
		 * <code>
		 *		//Using this in forms or other pages.
		 *		$listingObj->getListingByFriendlyURL($friendly_url);
		 * <br /><br />
		 *		//Using this in Listing() class.
		 *		$this->getListingByFriendlyURL($friendly_url);
		 * </code>
		 * @copyright Copyright 2005 Arca Solutions, Inc.
		 * @author Arca Solutions, Inc.
		 * @version 8.0.00
		 * @name getListingByFriendlyURL
         * @param string $friendly_url
		 * @access Public
		 */
		function getListingByFriendlyURL($friendly_url) {
			$dbObj = db_getDBObject();
			$sql = "SELECT * FROM Listing WHERE friendly_url = '".$friendly_url."'";
			$result = $dbObj->query($sql);
			if (mysql_num_rows($result)) {
				$this->makeFromRow(mysql_fetch_assoc($result));
				return true;
			} else {
				return false;
			}
		}
        
       /**
		 * <code>
		 *		//Using this in forms or other pages.
		 *		$listingObj->getListingToApp($array_get, $aux_returnArray, $aux_fields, $items, $auxTable, $aux_Where);
		 * <br /><br />
		 *		//Using this in Listing() class.
		 *		$this->getListingToApp($array_get, $aux_returnArray, $aux_fields, $items, $auxTable, $aux_Where);
		 * </code>
		 * @copyright Copyright 2005 Arca Solutions, Inc.
		 * @author Arca Solutions, Inc.
		 * @version 8.0.00
		 * @name getListingToApp
		 * @access Public
		 */
        function getListingToApp($account_id = false) {
            
            if ($this->id > 0 && $this->status == 'A') {
                
                /**
                 * Fields to detail page
                 */
                unset($aux_detail_fields);
                
                $aux_detail_fields[] = "id";
                $aux_detail_fields[] = "title";
                $aux_detail_fields[] = "email";
                $aux_detail_fields[] = "phone";
                $aux_detail_fields[] = "url";
                $aux_detail_fields[] = "latitude";
                $aux_detail_fields[] = "longitude";
                $aux_detail_fields[] = "description";
                $aux_detail_fields[] = "long_description";
                $aux_detail_fields[] = "level";
                $aux_detail_fields[] = "fax";
                $aux_detail_fields[] = "avg_review";
                $aux_detail_fields[] = "video_snippet";
                $aux_detail_fields[] = "video_description";
                if (API_IN_USE == "api2") {
                    $aux_detail_fields[] = "status";
                }
                
                /*
                 * Number fields
                 */
                unset($number_fields);
                $number_fields[] = "latitude";
                $number_fields[] = "longitude";
                $number_fields[] = "level";
                $number_fields[] = "avg_review";
                $number_fields[] = "id";
                $number_fields[] = "promotion_id";
                
                unset($add_info);
                $locationsToshow = system_retrieveLocationsToShow();
                $locationsParam = "A, B, ".system_formatLocation($locationsToshow.", z");

                $add_info["location_information"] = $this->getLocationString($locationsParam, true);
                
                foreach ($this->data_in_array as $key => $value) {
                
                    if (strpos($key, "image_id") !== false) {
                        unset($imageObj);
                        $imageObj = new Image($value);
                        if ($imageObj->imageExists()) {
                            $add_info["imageurl"] = $imageObj->getPath();
                        } else {
                            $firstGalImage = system_getImageFromGallery("listing", $this->id);
                            if ($firstGalImage) {
                                $add_info["imageurl"] = $firstGalImage;
                            } else {
                                $add_info["imageurl"] = NULL;
                            }
                        }
                    } elseif($key == "promotion_id") {
                        
                        unset($promotionObj, $promotionInfo);
                        $promotionObj = new Promotion($value);
                        $promotionInfo = $promotionObj->getDealByListing($this->id);
                        $arrayDealInfo = array();
                     
                        if ((!validate_date_deal($promotionObj->getDate("start_date"), $promotionObj->getDate("end_date"))) || (!validate_period_deal($promotionObj->getNumber("visibility_start"),$promotionObj->getNumber("visibility_end")))){
                            $add_info["deal_name"]          = "";   
                            $add_info["deal_remaining"]     = 0;
                            $add_info["deal_price"]         = 0;
                            $add_info["deal_description"]   = "";
                            $add_info["deal_realvalue"]     = 0;
                            $add_info["deal_id"]            = 0;
                            $add_info["deal_discount"]      = "";
                            $thisHasDeal = false;
                        } else {
 
                            $add_info["deal_name"]          = $promotionObj->getString("name");
                            $add_info["deal_remaining"]     = (float)$promotionInfo["deal_info"]["left"];
                            $add_info["deal_price"]         = (float)$promotionObj->getNumber("dealvalue");
                            $add_info["deal_description"]   = $promotionObj->getString("long_description");
                            $add_info["deal_realvalue"]     = (float)$promotionObj->getNumber("realvalue");
                            $add_info["deal_id"]            = (float)$value;
                            $thisHasDeal = true;

                            /**
                             * Calculate percentage
                             */
                            if ($promotionObj->realvalue > 0) {
                                $aux_percentage = round(100-(($promotionObj->dealvalue*100)/$promotionObj->realvalue));
                            } else {
                                $aux_percentage = 0;
                            }
                            $add_info["deal_discount"] = $aux_percentage."%";
                        }
                        
                        if (API_IN_USE == "api3") {
                            
                            $imageObj = new Image($promotionObj->getNumber("image_id"));
                            
                            $arrayDealInfo["id"] = $add_info["deal_id"];
                            $arrayDealInfo["title"] = $add_info["deal_name"];
                            if ($imageObj->imageExists()) {
                                $arrayDealInfo["imageurl"] = $imageObj->getPath();
                            }
                            $arrayDealInfo["remaining"] = $add_info["deal_remaining"];
                            $arrayDealInfo["description"] = $promotionObj->getString("description");
                            $arrayDealInfo["long_description"] = $add_info["deal_description"];
                            $arrayDealInfo["start_date"] = $promotionObj->getString("start_date");
                            $arrayDealInfo["end_date"] = $promotionObj->getString("end_date");
                            $arrayDealInfo["conditions"] = $promotionObj->getString("conditions");
                            $arrayDealInfo["dealvalue"] = $add_info["deal_price"];
                            $arrayDealInfo["realvalue"] = $add_info["deal_realvalue"];
                            $arrayDealInfo["discount"] = $add_info["deal_discount"];
                            $arrayDealInfo["avg_review"] = (int)$promotionObj->getNumber("avg_review");
                            $arrayDealInfo["friendly_url"] = PROMOTION_DEFAULT_URL."/".ALIAS_SHARE_URL_DIVISOR."/".$promotionObj->getNumber("listing_id");
                            if ($account_id) {
                                $arrayDealInfo["redeem_code"] = $promotionObj->alreadyRedeemed($promotionObj->getNumber("id"), $account_id);
                            }
                            /**
                            * Get number of Reviews
                            */
                            unset($reviewObj);
                            $reviewObj = new Review();
                            $reviewObj->item_type = "promotion";
                            $reviewObj->item_id = $promotionObj->getNumber("id");
                            $arrayDealInfo["total_reviews"] = (float)$reviewObj->GetTotalReviewsByItemID();
                            $add_info["deal"] = ($thisHasDeal ? $arrayDealInfo : NULL);
                            unset($add_info["deal_name"], $add_info["deal_remaining"], $add_info["deal_price"], $add_info["deal_description"], $add_info["deal_realvalue"], $add_info["deal_id"], $add_info["deal_discount"]);
                        } 
                        
                    }
                    
                    /**
                     * Get just fields to show on detail App
                     */
                    if (!is_numeric($key) && in_array($key, $aux_detail_fields)) {
                        
                        if ($key != "image_id") {
                            if (is_array($aux_fields)) {
                                $add_info[array_search($key, $aux_fields)] = ((is_numeric($value) && in_array($key,$number_fields)) ? (float)$value : $value);
                            } else {
                                $add_info[$key] = ((is_numeric($value) && in_array($key,$number_fields)) ? (float)$value : $value);
                            }
                        }
                    }
                }
               
                /**
                 * Get galleries
                 */
                unset($aux_galleries);
                
                $aux_galleries = $this->getGalleries();
                if (is_array($aux_galleries)) {
                    
                    $galleryObj = new Gallery();
                    
                    for ($i = 0; $i < count($aux_galleries); $i++) {
                        
                        $images = $galleryObj->getAllImages($aux_galleries[$i]);
                        
                        if (is_array($images)) {
                            
                            $k = 0;
                            for ($j = 0; $j < count($images); $j++) {
                        
                                unset($imageObj);
                                $imageObj = new Image($images[$j]["image_id"]);
                                if ($imageObj->imageExists()) {
                                    $add_info["gallery"][$k]["imageurl"] = $imageObj->getPath();
                                    $add_info["gallery"][$k]["caption"] = $images[$j]["image_caption"];
                                    $k++;
                                }    

                            }                       
                        }
                    }
                }
                
                /**
                 * Get number of Reviews
                 */
                unset($reviewObj);
                $reviewObj = new Review();
                $reviewObj->item_type = "listing";
                $reviewObj->item_id = $this->id;
                $add_info["total_reviews"] = (float)$reviewObj->GetTotalReviewsByItemID();
                
                /**
                 * Get number of Checkins
                 */
                unset($checkinObj);
                $checkinObj = new Checkin();
                $checkinObj->item_id = $this->id;
                $add_info["total_checkins"] = (float)$checkinObj->GetTotalCheckinsByItemID();
                
                /**
                 * Get categories 
                 */
                unset($listingCategoriesObj);
                $listingCategoriesObj = new Listing_Category();
                $listingCategories = $listingCategoriesObj->getCategoriesByListingID($this->id);
                if ($listingCategories) {
                    $add_info["categories"] = $listingCategories;
                } else {
                    $add_info["categories"] = NULL;
                }
                
                /**
                 * Preparing friendly URL
                 */
                $add_info["friendly_url"] = LISTING_DEFAULT_URL."/".ALIAS_SHARE_URL_DIVISOR."/".$this->friendly_url.".html";
                
                if (is_array($add_info)) {
                    return $add_info;
                } else {
                    return false;
                }
                
            } else {
                return false;
            }
        }
		
       /**
		 * <code>
		 *		//Using this in forms or other pages.
		 *		$listingObj->GetInfoToApp($array_get, $aux_returnArray, $aux_fields, $items, $auxTable, $aux_Where);
		 * <br /><br />
		 *		//Using this in Listing() class.
		 *		$this->GetInfoToApp($array_get, $aux_returnArray, $aux_fields, $items, $auxTable, $aux_Where);
		 * </code>
		 * @copyright Copyright 2005 Arca Solutions, Inc.
		 * @author Arca Solutions, Inc.
		 * @version 8.0.00
		 * @name GetInfoToApp
         * @param array $array_get
         * @param array $aux_returnArray
         * @param array $aux_fields
         * @param array $items
         * @param array $auxTable
         * @param array $aux_Where
		 * @access Public
		 */
        function GetInfoToApp($array_get, &$aux_returnArray, &$aux_fields, &$items, &$auxTable, &$aux_Where) {
            
            extract($array_get);
            
            /**
             * Prepare columns with alias
             */
            if (is_array($aux_fields)) {

                unset($fields_to_map);

                foreach ($aux_fields as $key => $value) {
                    if (strpos($value, " AS ") !== false) {
                        $fields_to_map[] = $value;
                    } else {
                        $fields_to_map[] = $value." AS `".$key."`";
                    }
                }
            }
            
            if ($id) {
                    
                /*
                 * Get Listing
                 */
                unset($listingObj, $listingInfo);
                $listingObj = new Listing($id);

                $listingInfo = $listingObj->getListingToApp($account_id);

                if (!is_array($listingInfo)) {

                    $aux_returnArray[(API_IN_USE == "api2" ? "error" : "message")]         = "No results found.";
                    $aux_returnArray["type"]            = $resource;
                    $aux_returnArray["total_results"]   = 0; 
                    $aux_returnArray["total_pages"]     = 0; 
                    $aux_returnArray["results_per_page"]= 0; 
                    
                } else {
                    $items[] = $listingInfo;
                    $aux_returnArray["type"]            = $resource;
                    $aux_returnArray["total_results"]   = 1; 
                    $aux_returnArray["total_pages"]     = 1; 
                    $aux_returnArray["results_per_page"]= 1;
                }

            } else {

                $auxTable = "Listing_Summary";
                $aux_Where[] = "status = 'A'";
                
                if (API_IN_USE == "api2") {
                    $aux_orderBy[] = "level";
                    $aux_orderBy[] = "title";
                }
                
                if ($featured) {
                    $level = implode(",", system_getLevelDetail("ListingLevel"));
                    if ($level) {
                       $aux_Where[] = "level IN ($level)"; 
                    } else {
                        $aux_Where[] = "id IN (0)";
                    }
                }

            }

            if ($searchBy) {
                if ($searchBy == "keyword" || $searchBy == "keyword_where") {

                    unset($searchReturn);
                    $searchReturn["from_tables"]    = "Listing_Summary";
                    $searchReturn["order_by"]       = "Listing_Summary.level";
                    $searchReturn["where_clause"]   = "Listing_Summary.status = 'A' ";
                    $searchReturn["select_columns"] = implode(", ", $aux_fields);
                    $searchReturn["group_by"]       = false;

                    $letterField = "title";
                    search_frontListingAppKeyword($array_get, $searchReturn);

                    $pageObj = new pageBrowsing($searchReturn["from_tables"], $page, $aux_results_per_page, $searchReturn["order_by"], $letterField, $letter, $searchReturn["where_clause"], $searchReturn["select_columns"], "Listing_Summary", $searchReturn["group_by"]);

                    $items = $pageObj->retrievePage("array");

                    if (!is_array($items)) {
                        $aux_returnArray[(API_IN_USE == "api2" ? "error" : "message")]     = "No results found.";
                    }
                    
                    $aux_returnArray["type"]            = $resource;
                    $aux_returnArray["total_results"]   = $pageObj->record_amount; 
                    $aux_returnArray["total_pages"]     = $pageObj->pages; 
                    $aux_returnArray["results_per_page"]= $pageObj->limit; 


                } elseif (($searchBy == "map" || $searchBy == "map_review") && ($drawLat0 && $drawLat1 && $drawLong0 && $drawLong1)) {
                    
                    /**
                     * Search on map with coordinates and / or keyword
                     */
                    $letterField = "title";
                    $searchReturn = search_frontListingDrawMap($array_get, "listing_results_api", $fields_to_map, ($searchBy == "map_review" ? true : false));
                    $pageObj = new pageBrowsing($searchReturn["from_tables"], $page, $aux_results_per_page, $searchReturn["order_by"], $letterField, $letter, $searchReturn["where_clause"], $searchReturn["select_columns"], "Listing_Summary", $searchReturn["group_by"]);

                    $items = $pageObj->retrievePage("array");
                    
                    if (!is_array($items)) {
                        $aux_returnArray[(API_IN_USE == "api2" ? "error" : "message")]     = "No results found.";
                    }
                    
                    $aux_returnArray["type"]            = $resource;
                    $aux_returnArray["total_results"]   = $pageObj->record_amount; 
                    $aux_returnArray["total_pages"]     = $pageObj->pages; 
                    $aux_returnArray["results_per_page"]= $pageObj->limit; 
                   

                } elseif ($searchBy == "category" && $category_id) {

                    /*
                     * Get Listing by category_id
                     */
                    
                    //Create a category object to get hierarchy of categories
                    unset($aux_categoryObj, $aux_cat_hierarchy);
                    $aux_categoryObj = new ListingCategory($category_id);
                    $aux_cat_hierarchy = $aux_categoryObj->getHierarchy($category_id, false, true);
                    if ($aux_cat_hierarchy) {
                        unset($listing_CategoryObj);
                        $listing_CategoryObj = new Listing_Category();
                        $listings_id = $listing_CategoryObj->getListingsByCategoryHierarchy($aux_categoryObj->root_id,$aux_categoryObj->left,$aux_categoryObj->right);
                    }
                       
                    if ($listings_id) {
                        $aux_Where[] = " id in (".$listings_id.")";
                        unset($searchReturn);
                        $searchReturn["from_tables"]    = "Listing_Summary";
//                        search_prepareFilters($array_get, $searchReturn, "Listing_Summary", $aux_Where);
                    } else {
                        $aux_returnArray[(API_IN_USE == "api2" ? "error" : "message")] = "No results found.";
                    }

                } else {
                    $return["type"]             = $resource;
                    $return["total_results"]    = 0;
                    $return["total_pages"]      = 0;
                    $return["results_per_page"] = 0;
                    $return["success"]          = FALSE;
                    $return["message"]          = "Wrong Search, check the parameters";
                    api_formatReturn($return);
                }
            }
        }

        function getRates( $range = false, $order_by = false ){
        	$dbMain = db_getDBObject(DEFAULT_DB, true);
            if (defined("SELECTED_DOMAIN_ID")) {
                $db = db_getDBObjectByDomainID(SELECTED_DOMAIN_ID, $dbMain);
            } else {
                $db = db_getDBObject();
            }
            unset($dbMain);

            $orderby = 'ORDER BY from_date ASC';
            if ( $order_by == 'title_asc' ) {
            	$orderby = 'ORDER BY title ASC';
            } elseif ( $order_by == 'title_desc' ) {
            	$orderby = 'ORDER BY title DESC';
            } elseif ( $order_by == 'from_date_asc' ) {
            	$orderby = 'ORDER BY from_date ASC';
            } elseif ( $order_by == 'from_date_desc' ) {
            	$orderby = 'ORDER BY from_date DESC';
            } elseif ( $order_by == 'to_date_asc' ) {
            	$orderby = 'ORDER BY to_date ASC';
            } elseif ( $order_by == 'to_date_desc' ) {
            	$orderby = 'ORDER BY to_date DESC';
            }

            if ( $range ) {

            	//DAY
            	$sql = "SELECT 
            			MIN(day) AS min_day,
            			MAX(day) AS max_day
            		FROM Rate WHERE listing_id = $this->id
					AND day > 0 
	            	$orderby";

	            $results = $db->query($sql);
				while( $row = mysql_fetch_assoc($results) ) {
					$data[0]["min_day"] = $row["min_day"];
					$data[0]["max_day"] = $row["max_day"];
				}

            	//WEEK
            	$sql = "SELECT
            			MIN(week) AS min_week,
            			MAX(week) AS max_week
            		FROM Rate WHERE listing_id = $this->id
	            	AND week > 0 
            		$orderby";

            	$results = $db->query($sql);
				while( $row = mysql_fetch_assoc($results) ) {
					$data[0]["min_week"] = $row["min_week"];
					$data[0]["max_week"] = $row["max_week"];
				}

            	//MONTH
            	$sql = "SELECT
            			MIN(month) AS min_month,
            			MAX(month) AS max_month
            		FROM Rate WHERE listing_id = $this->id
	            	AND month > 0 
            		$orderby";

            	$results = $db->query($sql);
				while( $row = mysql_fetch_assoc($results) ) {
					$data[0]["min_month"] = $row["min_month"];
					$data[0]["max_month"] = $row["max_month"];
				}

            	//SEASON
            	$sql = "SELECT 
            			MIN(season) AS min_season,
            			MAX(season) AS max_season
            		FROM Rate WHERE listing_id = $this->id
	            	AND season > 0 
            		$orderby";

            	$results = $db->query($sql);
				while( $row = mysql_fetch_assoc($results) ) {
					$data[0]["min_season"] = $row["min_season"];
					$data[0]["max_season"] = $row["max_season"];
				}

            } else {
            	$sql = "SELECT * FROM Rate WHERE listing_id = $this->id $orderby";

            	$results = $db->query($sql);
				while( $row = mysql_fetch_assoc($results) ) {
					$data[] = $row;
				}
            }

			if ( $data ) return $data;
			else return false;
        }

        function getExtraFields($return_type = "array"){
        	$dbMain = db_getDBObject(DEFAULT_DB, true);
            if (defined("SELECTED_DOMAIN_ID")) {
                $db = db_getDBObjectByDomainID(SELECTED_DOMAIN_ID, $dbMain);
            } else {
                $db = db_getDBObject();
            }
            unset($dbMain);

            $return_array = array();

            //sleeps / bedroom / bathroom
            $return_array['sleeps'][] = 'sleeps_min';
            $return_array['sleeps'][] = 'sleeps_max';
            $return_array['bedroom'][] = 'bedroom_min';
            $return_array['bedroom'][] = 'bedroom_max';
            $return_array['bathroom'][] = 'bathroom_min';
            $return_array['bathroom'][] = 'bathroom_max';

            //Amenities
        	if (DEMO_DEV_MODE) {
				$sql = "SHOW columns FROM Listing WHERE Field LIKE 'amenity\_%'";
			} else {
				$sql = "SELECT DISTINCT COLUMN_NAME AS Field FROM INFORMATION_SCHEMA.COLUMNS 
					WHERE TABLE_NAME = 'Listing' AND COLUMN_NAME LIKE 'amenity\_%' ORDER BY COLUMN_NAME";	
			}
        	$results = $db->query($sql);
			while( $row = mysql_fetch_assoc($results) ) {
				$field = str_replace("amenity_", "", $row['Field']);
				$return_array['amenity'][$field] = constant("LANG_LABEL_AMENITY_".strtoupper($field));
			}

            //Features
        	if (DEMO_DEV_MODE) {
				$sql = "SHOW columns FROM Listing WHERE Field LIKE 'feature\_%'";
			} else {
				$sql = "SELECT DISTINCT COLUMN_NAME AS Field FROM INFORMATION_SCHEMA.COLUMNS 
					WHERE TABLE_NAME = 'Listing' AND COLUMN_NAME LIKE 'feature\_%' ORDER BY COLUMN_NAME";	
			}
        	$results = $db->query($sql);
			while( $row = mysql_fetch_assoc($results) ) {
				$field = str_replace("feature_", "", $row['Field']);
				$return_array['feature'][$field] = constant("LANG_LABEL_FEATURE_".strtoupper($field));
			}

            //Activities
        	if (DEMO_DEV_MODE) {
				$sql = "SHOW columns FROM Listing WHERE Field LIKE 'activity\_%'";
			} else {
				$sql = "SELECT DISTINCT COLUMN_NAME AS Field FROM INFORMATION_SCHEMA.COLUMNS 
					WHERE TABLE_NAME = 'Listing' AND COLUMN_NAME LIKE 'activity\_%' ORDER BY COLUMN_NAME";	
			}
        	$results = $db->query($sql);
			while( $row = mysql_fetch_assoc($results) ) {
				$field = str_replace("activity_", "", $row['Field']);
				$return_array['activity'][$field] = constant("LANG_LABEL_ACTIVITY_".strtoupper($field));
			}

        	if ($return_type == "array") {
        		//Default
        		//Return as array
        		return $return_array;

        	} else {
        		//Compile as object
        		foreach ($return_array as $key => $value) {
        			$this->$key = $return_array[$key];
        		}
        	}

        }

        function getCompareFields(){
        	$dbMain = db_getDBObject(DEFAULT_DB, true);
            if (defined("SELECTED_DOMAIN_ID")) {
                $db = db_getDBObjectByDomainID(SELECTED_DOMAIN_ID, $dbMain);
            } else {
                $db = db_getDBObject();
            }
            unset($dbMain);

            $return_array = array();

            //sleeps / badroom / bathroom
            $return_array['detail']['bedroom'] = 'Bedrooms';
            $return_array['detail']['sleeps'] = 'Sleeps';
            $return_array['detail']['bathroom'] = 'Bathrooms';

            //Amenities
        	if (DEMO_DEV_MODE) {
				$sql = "SHOW columns FROM Listing WHERE Field LIKE 'amenity\_%'";
			} else {
				$sql = "SELECT DISTINCT COLUMN_NAME AS Field FROM INFORMATION_SCHEMA.COLUMNS 
					WHERE TABLE_NAME = 'Listing' AND COLUMN_NAME LIKE 'amenity\_%' ORDER BY COLUMN_NAME";	
			}
        	$results = $db->query($sql);
			while( $row = mysql_fetch_assoc($results) ) {
				//$field = str_replace("amenity_", "", $row['Field']);
				$field = $row['Field'];
				$return_array['amenity'][$field] = constant("LANG_LABEL_".strtoupper($field));
			}

            //Features
        	if (DEMO_DEV_MODE) {
				$sql = "SHOW columns FROM Listing WHERE Field LIKE 'feature\_%'";
			} else {
				$sql = "SELECT DISTINCT COLUMN_NAME AS Field FROM INFORMATION_SCHEMA.COLUMNS 
					WHERE TABLE_NAME = 'Listing' AND COLUMN_NAME LIKE 'feature\_%' ORDER BY COLUMN_NAME";	
			}
        	$results = $db->query($sql);
			while( $row = mysql_fetch_assoc($results) ) {
				//$field = str_replace("feature_", "", $row['Field']);
				$field = $row['Field'];
				$return_array['feature'][$field] = constant("LANG_LABEL_".strtoupper($field));
			}

            //Activities
        	if (DEMO_DEV_MODE) {
				$sql = "SHOW columns FROM Listing WHERE Field LIKE 'activity\_%'";
			} else {
				$sql = "SELECT DISTINCT COLUMN_NAME AS Field FROM INFORMATION_SCHEMA.COLUMNS 
					WHERE TABLE_NAME = 'Listing' AND COLUMN_NAME LIKE 'activity\_%' ORDER BY COLUMN_NAME";	
			}
        	$results = $db->query($sql);
			while( $row = mysql_fetch_assoc($results) ) {
				//$field = str_replace("activity_", "", $row['Field']);
				$field = $row['Field'];
				$return_array['activity'][$field] = constant("LANG_LABEL_".strtoupper($field));
			}

			return $return_array;
        }

        function checkEmail($email) {
        	$dbMain = db_getDBObject(DEFAULT_DB, true);
        	$dbObj = db_getDBObjectByDomainID(SELECTED_DOMAIN_ID, $dbMain);
			$sql = "SELECT email_list FROM Listing WHERE id = $this->id";
			if ($row = mysql_fetch_assoc($dbObj->query($sql))) {
				$email_list = json_decode($row["email_list"]);
				if ($email_list) {
					foreach ($email_list as $value) {
						if ($email == $value)
							return true;
					}
				}
			}
			return false;

		}

		function addToList($email) {
			$dbMain = db_getDBObject(DEFAULT_DB, true);
			$dbObj = db_getDBObjectByDomainID(SELECTED_DOMAIN_ID, $dbMain);
			$sql = "SELECT email_list FROM Listing WHERE id = $this->id";
			if ($row = mysql_fetch_assoc($dbObj->query($sql))) {
				$email_list = array();
				$email_list = json_decode($row["email_list"]);
				$email_list[] = $email;
				$email_list = json_encode($email_list);
				$sqlUpdate = "UPDATE Listing set email_list = '$email_list'";
				$dbObj->query($sqlUpdate);
			}
		}

		function listingInquired() {
			$dbMain = db_getDBObject(DEFAULT_DB, true);
        	$dbObj = db_getDBObjectByDomainID(SELECTED_DOMAIN_ID, $dbMain);
			$sql = "UPDATE Listing SET deposit_amount = deposit_amount - tax_per_inquiry WHERE id = $this->id";
			$dbObj->query($sql);
		}

		function suspendPPI_program() {
			$dbMain = db_getDBObject(DEFAULT_DB, true);
			$dbObj = db_getDBObjectByDomainID(SELECTED_DOMAIN_ID, $dbMain);
			$sql = "UPDATE Listing SET status = 'S' WHERE id = $this->id";
			$dbObj->query($sql);
		}

		function ppiWarning() {
			$dbMain = db_getDBObject(DEFAULT_DB, true);
			$dbObj = db_getDBObjectByDomainID(SELECTED_DOMAIN_ID, $dbMain);
			$sql = "SELECT ppi_warning FROM Listing WHERE id = $this->id";
			if ($row = mysql_fetch_assoc($dbObj->query($sql)))
				if ($row["ppi_warning"] == 'n')
					return true;
				else return false;
		}

		function ppiWarned() {
			$dbMain = db_getDBObject(DEFAULT_DB, true);
			$dbObj = db_getDBObjectByDomainID(SELECTED_DOMAIN_ID, $dbMain);
			$sql = "UPDATE Listing SET ppi_warning = 'y' WHERE id = $this->id";
			$dbObj->query($sql);
		}

		public function getVisitsAmount() {
			$dbMain = db_getDBObject(DEFAULT_DB, true);
			$dbObj = db_getDBObjectByDomainID(SELECTED_DOMAIN_ID, $dbMain);
			$sql = "SELECT SUM(report_amount) AS amount FROM Report_Listing WHERE listing_id = $this->id AND report_type = 2";
			$result = mysql_fetch_assoc( $dbObj->query($sql) );
			return $result['amount'];
		}

		public function getRateInformation(){
			$rateInfo = new RateInformation( $this->id );
			$data = array();
			if ( !empty($rateInfo) ) {
				$data['checkin']             = $rateInfo->checkin;
				$data['checkout']            = $rateInfo->checkout;
				$data['payment_types']       = $rateInfo->payment_types;
				$data['payment_terms']       = $rateInfo->payment_terms;
				$data['cancellation_policy'] = $rateInfo->cancellation_policy;
				$data['security_deposit']    = $rateInfo->security_deposit;
				$data['notes']               = $rateInfo->notes;
			}
			if ( !empty($data) ) return $data;
			else return false;
		}

		public function setFriendlyUrl() {
			$dbMain = db_getDBObject(DEFAULT_DB, true);
			$dbObj = db_getDBObjectByDomainID(SELECTED_DOMAIN_ID, $dbMain);
			$sql = "UPDATE Listing SET friendly_url = ".db_formatString($this->id)." WHERE id = $this->id";
			$dbObj->query($sql);
			$sql = "UPDATE Listing_Summary SET friendly_url = ".db_formatString($this->id)." WHERE id = $this->id";
			$dbObj->query($sql);
		}

		public function setCustomId($custom_id) {
			$dbMain = db_getDBObject(DEFAULT_DB, true);
			$dbObj = db_getDBObjectByDomainID(SELECTED_DOMAIN_ID, $dbMain);
			$sql = "UPDATE Listing SET custom_id = ".db_formatString($custom_id)." WHERE id = $this->id";
			$dbObj->query($sql);
		}

		public function hasCustomId() {
			$dbMain = db_getDBObject(DEFAULT_DB, true);
			$dbObj = db_getDBObjectByDomainID(SELECTED_DOMAIN_ID, $dbMain);
			$sql = "SELECT custom_id FROM Listing WHERE id = $this->id";
			$result = mysql_fetch_assoc($dbObj->query($sql));
			if ( $result['custom_id'] ) {
				return $result['custom_id'];
			}
			return false;
		}

        
        
        
        
        
        
        function system_addItemGallery($gallery_hash, $title = "", &$galleryIDC, &$image_id, &$thumb_id, $blog = false){
		
		
		$sess_id = $gallery_hash;
		
		if (!$blog){
		
			$gallery = new Gallery($galleryIDC);
			if (!$galleryIDC || $galleryIDC==''){
			 
				$aux = array("account_id"=>0,"title"=>$title,"entered"=>"NOW()","updated"=>"now()");
				$gallery->makeFromRow($aux);
				$gallery->save();
			}

			$sql = "SELECT 
						image_id,
						image_caption,
						thumb_id,
						thumb_caption,
						image_default
					FROM Gallery_Temp
					WHERE sess_id = '$sess_id'";
			$auxT = $r = Self::fetch($sql);
			foreach($auxT as $aux){

                
				if ($aux->image_default == "y"){
					$image_id = $aux->image_id;
					$thumb_id = $aux->thumb_id;
				}
				$row["image_id"] = $aux->image_id;
				$row['image_caption'] = $aux->image_caption;
				$row['thumb_id'] = $aux->thumb_id;
				$row['thumb_caption'] = $aux->thumb_caption;
				$row['image_default'] = $aux->image_default;
				$row['order'] = 0;
				$gallery->AddImage($row);
				$gallery->save();
				$galleryIDC = $gallery->id;
			}
			$sql = "DELETE FROM Gallery_Temp WHERE sess_id = '$sess_id'";
			Self::deleteSql($sql);
		} else {
			$sql = "SELECT 
							image_id,
							image_caption,
							thumb_id,
							thumb_caption,
							image_default
						FROM Gallery_Temp
						WHERE sess_id = '$sess_id'";
			$r = Self::fetch($sql);
			foreach($r as $aux){
				$image_id=$aux->image_id;
				$thumb_id=$aux->thumb_id;
				$_POST["image_caption"] = $aux->image_caption;
				$_POST["thumb_caption"] = $aux->thumb_caption;
			}

			$sql = "DELETE FROM Gallery_Temp WHERE sess_id = '$sess_id'";

			Self::deleteSql($sql);
		}
	}
    
    function system_countActiveListingByCategory($listingID = "", $category_id = false, $domain_id = false) {
		if (is_numeric($category_id) && $category_id > 0) {
			$listingCatObj = new ListingCategory();
			$listingCatObj->countActiveListingByCategory($category_id, $domain_id);
		} else {
			
			

			if (is_numeric($listingID) && $listingID > 0) {
				$sqlCat = "	SELECT LC.`root_id` AS `category_id`
							FROM `ListingCategory` LC
							LEFT JOIN `Listing_Category` L_C ON (L_C.`category_id` = LC.`id`)
							WHERE L_C.`listing_id` = $listingID";
			} else {
				$sqlCat = "SELECT `id` AS `category_id` FROM `ListingCategory` WHERE `category_id` = 0";
			}
			$resCat = Self::fetch($sqlCat);
			if (count($resCat) > 0) {
				$listingCatObj = new ListingCategory();
				foreach($resCat as $rowCat) {
					$listingCatObj->countActiveListingByCategory($rowCat->category_id, $domain_id);
				}
			}
		}
	}
    
    
    /**
	 * Save new to approved record
	 * @copyright Copyright 2005 Arca Solutions, Inc.
	 * @author Arca Solutions, Inc.
     * @param integer $domain_id
     * @param integer $item_id
     * @param string $item_type
     * @param string $item_title
     * @param string $review_title
     */
    function activity_newToApproved($domain_id = 0, $item_id = 0, $item_type = "", $item_title = "''", $content = "''", $assoc_item = 0, $rate = 0, $reply_id = 0) {
       
        
        if (strpos($item_type, "review") !== false) { //get reviewer name and content
            $item_id = str_replace("'", "", $item_id);
            $reviewObj = new Review();
            $reviewObj->Review($item_id);
            $reviewer_name = $reviewObj->getString("reviewer_name");
            $review_content = ($reply_id ? $reviewObj->getString("response") : $reviewObj->getString("review"));
        }
        
        $sql = "INSERT INTO To_Approved (domain_id, item_id, item_type, item_title, content, assoc_item, rate, reviewer_name, review_content, reply_id, date) VALUES ($domain_id, $item_id, '$item_type', $item_title, $content, $assoc_item, $rate, '".$reviewer_name."', '".$review_content."', $reply_id, NOW())";
        d($sql);die;
        Self::insertSqlMain($sql);

    }
    
    
    /**
	 * Delete approved records
	 * @copyright Copyright 2005 Arca Solutions, Inc.
	 * @author Arca Solutions, Inc.
     * @param integer $domain_id
     * @param integer $item_id
     * @param string $item_type
     */
	function activity_deleteRecord($domain_id = 0, $item_id = 0, $item_type = "", $reviewResponse = false) {
	
		$sql = "DELETE FROM To_Approved WHERE domain_id = $domain_id AND item_id = $item_id AND item_type = '$item_type'".($reviewResponse ? " AND reply_id = 1" : "");
		Self::deleteSqlMain($sql);
	}
    
    
    /**
	 * Update to approved records
	 * @copyright Copyright 2005 Arca Solutions, Inc.
	 * @author Arca Solutions, Inc.
     * @param integer $domain_id
     * @param integer $item_id
     * @param string $newtitle
     * @param string $item
     * @param string $item_type
     */
	function activity_updateRecord($domain_id = 0, $item_id = 0, $newtitle = "", $item = "", $item_type = "", $rate = 0, $reviewer = "''", $review = "''", $reviewResponse = false) {
		
        
        if ($item_type == "listing" || $item_type == "article" || $item_type == "promotion") {
            $sql = "UPDATE To_Approved SET item_title = $newtitle WHERE domain_id = $domain_id AND assoc_item = $item_id AND item_type = 'review_$item_type'";
            Self::updateSqlMain($sql);
        } elseif ($item_type == "post") {
            $sql = "UPDATE To_Approved SET item_title = $newtitle WHERE domain_id = $domain_id AND assoc_item = $item_id AND item_type = 'blog_comment'";
            Self::updateSqlMain($sql);
        }
        
		if ($item == "item") $item .= "_title";
		$sql = "UPDATE To_Approved SET ".$item." = $newtitle, `rate` = $rate, `reviewer_name` = $reviewer, `review_content` = $review WHERE domain_id = $domain_id AND item_id = $item_id AND item_type = '$item_type'".($reviewResponse ? " AND reply_id = 1" : "");
        Self::updateSqlMain($sql);
	}
    
    
    
    function format_addApostWords($string){
        if (!$string)return false;
        $stringARR=explode(" ",$string);

        foreach ($stringARR as $word){
            $newword = $word;
            if (stripos($word,"'s"))
                $newword=str_replace("'s", "", $word);

            if (stripos($word,"s'"))
                $newword=str_replace("s'", "", $word);

            if ($newword)
                $newStringArr[]=$newword;

            unset($newword);
        }
        if (is_array($newStringArr)){
            $newStringArr=array_unique($newStringArr);
            return (implode(' ',$newStringArr));
        } else return false;
    }
    
    
    
    function db_formatString($string, $default = "", $import = false, $simpleQuotes = true) {
        if ($import){
            if (!$string){
               $string = "'".$string."'"; 
            } elseif (is_string($string)) {
                if ((strpos($string,"\'") !== false) || (strpos($string,"\\") !== false) || (strpos($string,"\\\"") !== false) || !get_magic_quotes_gpc()){
                    $string = stripslashes($string);
                }
                $string  = addslashes($string);
                $string = "'".$string."'";
            } elseif (is_numeric($string)) {
                return $string;
            } else {
                $string = "'".$string."'";
            }
            return $string;
            
        } else {
            
            if (empty($string) && $string != "0") {
                $string = $default;
            }
            if (($string[0]=="'" && $string[strlen($string)-1]=="'") || ($string[0]=='"' && $string[strlen($string)-1]=='"')) {
                $string = substr($string, 1, strlen($string)-2);
            }
            if ($this->db_stringNeedsAddslashes($string)) {
                $string = addslashes($string);
            }
            if ($simpleQuotes){
                return "'".$string."'";
            } else {
                return $string;
            }
        }
		
	}
    
    
    function db_stringNeedsAddslashes($str) {
		if (($qp = strpos($str,"'")) !== false || ($qp = strpos($str,"\"")) !== false) {
		if ($str[$qp-1] != "\\")
			return true;
		else
			return $this->db_stringNeedsAddslashes(substr($str,$qp+1,strlen($str)));
		}
		return false;
	}
    
    
    
    function zipproximity_updateDB($table, $id) {

		$lat = '';
		$lon = '';
		$sql = "SELECT zip_code, latitude, longitude FROM ".$table." WHERE id = '".$id."'";
		$r = Self::fetch($sql);
		if (!empty($r)) {
		  $row = (array)$r;

			$zip = $row[0]->zip_code;
			$lat = $row[0]->latitude;
			$lon = $row[0]->longitude;
			if ($this->zipproximity_getZip5($zip, $zip5)) {
				if ($this->zipproximity_validateZip5($zip5)) {
					$this->zipproximity_saveZip5($table, $id, $zip5, $lat, $lon);
					return true;
				}
			}
		}
		$this->zipproximity_saveZip5($table, $id, "0", $lat, $lon);
		return false;
	}
    
    
    
    function zipproximity_getZip5Fields($zip5, &$latitude, &$longitude) {
		
		$sql = "SELECT ZipCode, Latitude, Longitude FROM ZipCode_Data WHERE ZipCode = '".$zip5."' LIMIT 1";
		$r = Self::fetchMain($sql);
		if (!empty($r)) {
		  $row = (array)$r;
			$latitude = $row[0]->Latitude;
			$longitude = $row[0]->Longitude;
			return true;
		}
		return false;
	}
    
    
    function zipproximity_saveZip5($table, $id, $zip5, $lati, $long) {
	
		if ($this->zipproximity_getZip5Fields($zip5, $latitude, $longitude)) {
            if ($lati){
                $latitude = $lati;
            }
            if ($long){
                $longitude = $long;
            }
			$sql = "UPDATE ".$table." SET zip5 = '".$zip5."', latitude = '".$latitude."', longitude = '".$longitude."' WHERE id = '".$id."'";
			$r = Self::updateSql($sql);
            
            if ($table == "Listing"){
                $sql = "UPDATE Promotion SET listing_zip5 = '".$zip5."', listing_latitude = '$lati', listing_longitude = '$long' WHERE listing_id = '".$id."'";
                $r = Self::updateSql($sql);
            }
            
			return true;
		}
		$sql = "UPDATE ".$table." SET zip5 = '0', latitude = '$lati', longitude = '$long' WHERE id = '".$id."'";
		$r = Self::updateSql($sql);
        
        if ($table == "Listing"){
            $sql = "UPDATE Promotion SET listing_zip5 = '0', listing_latitude = '$lati', listing_longitude = '$long' WHERE listing_id = '".$id."'";
            $r = Self::updateSql($sql);
        }
		return false;
	}
    
        
    function zipproximity_validateZip5($zip5) {
		
		$sql = "SELECT ZipCode FROM ZipCode_Data WHERE ZipCode = '".$zip5."' LIMIT 1";
		$result = Self::fetchMain($sql);
		if (count($result) > 0) {
			return true;
		}
		return false;
	}
    
    	# ----------------------------------------------------------------------------------------------------
	# * FILE: /functions/zipproximity_funct.php
	# ----------------------------------------------------------------------------------------------------

	// ---------------------------------------------------------------------------------------------------- \\
	// PRIVATE FUNCTIONS                                                                                    \\
	// ---------------------------------------------------------------------------------------------------- \\

	function zipproximity_getZip5($zip, &$zip5) {

		$zip = trim($zip);
		$zip5 = "";

		if (($zip) && (strlen($zip) > 0)) {

			##################################################
			# United States ZipCode (US)
			##################################################
			if (config('params.ZIPCODE_US') == "on") {
				if (preg_match("/^[0-9]/", substr($zip, 0, 1))) {
					while (preg_match("/^[0-9]/", substr($zip, 0, 1))) {
						$zip5 .= substr($zip, 0, 1);
						$zip = substr($zip, 1);
					}
					if (($zip5) && (strlen($zip5) > 0)) {
						if (strlen($zip5) > 5) {
							$zip5 = substr($zip5, 0, 5);
						}
						$zip5 = $zip5 + 0;
						if (($zip5) && ($zip5 > 0)) {
							while (strlen($zip5) < 5) {
								$zip5 = "0" . $zip5;
							}
							$zip5 = strtoupper($zip5);
							return true;
						}
					}
				}
			}
			##################################################

			##################################################
			# Canada ZipCode (CA)
			##################################################
			if (config('params.ZIPCODE_CA') == "on") {
				if (preg_match("/^[a-zA-Z]/", substr($zip, 0, 1))) {
					if (!preg_match("/^(\s{1})/", substr($zip, 3, 1))) {
						$zip = substr($zip, 0, 3)." ".substr($zip, 3);
					}
					if (!preg_match("/^[0-9]/", substr($zip, 4, 1))) {
						$zip = substr($zip, 0, 4).substr($zip, 5);
					}
					$zip5 = substr($zip, 0, 7);
					if (preg_match("/^([a-zA-Z]{1})([0-9]{1})([a-zA-Z]{1})(\s{1})([0-9]{1})([a-zA-Z]{1})([0-9]{1})/", $zip5)) {
						$zip5 = strtoupper($zip5);
						return true;
					}
				}
			}
			##################################################

			##################################################
			# United Kingdom ZipCode (UK)
			##################################################
			if (config('params.ZIPCODE_UK') == "on") {
				if (preg_match("/^[0-9a-zA-Z]/", substr($zip, 0, 1))) {
					while (preg_match("/^[0-9a-zA-Z]/", substr($zip, 0, 1))) {
						$zip5 .= substr($zip, 0, 1);
						$zip = substr($zip, 1);
					}
					if (($zip5) && (strlen($zip5) > 0)) {
						if (strlen($zip5) > 4) {
							$zip5 = substr($zip5, 0, 4);
						}
						if (($zip5) && (strlen($zip5) > 0)) {
							$zip5 = strtoupper($zip5);
							return true;
						}
					}
				}
			}
			##################################################

			##################################################
			# Australia ZipCode (AU)
			##################################################
			if (config('params.ZIPCODE_AU') == "on") {
				if (preg_match("/^[0-9]/", substr($zip, 0, 1))) {
					while (preg_match("/^[0-9]/", substr($zip, 0, 1))) {
						$zip5 .= substr($zip, 0, 1);
						$zip = substr($zip, 1);
					}
					if (($zip5) && (strlen($zip5) > 0)) {
						if (strlen($zip5) > 4) {
							$zip5 = substr($zip5, 0, 4);
						}
						$zip5 = $zip5 + 0;
						if (($zip5) && ($zip5 > 0)) {
							while (strlen($zip5) < 4) {
								$zip5 = "0" . $zip5;
							}
							$zip5 =strtoupper($zip5);
							return true;
						}
					}
				}
			}
			##################################################

		}

		return false;

	}
    
    
    
    function system_updateMaptuningDate($table, $id, $maptuning_done){
		if (isset($maptuning_done) && $maptuning_done == "y" && ($table == "Listing" || $table == "Classified" || $table == "Event") && $id){
			
			
			$sql = "UPDATE $table SET maptuning_date = NOW() WHERE id = ".$id;
			Self::updateSql($sql);
		}
	}
    
    
    function setting_get($name, &$value) {
		if ($name) {

			unset($array_settings);
			$array_settings = $this->setting_getSettingInformation($name);
			if((is_array($array_settings)) && ($_SERVER['REQUEST_METHOD']!="POST")){
				$value = $array_settings["value"];
				return true;
			}else{
				$settingObj = new Setting($name);
				if ($settingObj->getString("name")) {
					$value = $settingObj->getString("value");
					return true;
				}
			}
			
		}
		$value = "";
		return false;
	}
    
    
    /*
	 * Function to get information about language
	 */
	function setting_getSettingInformation($index){

		if(!defined('SETTING_INFORMATION')){
			$this->setting_constants();
		}

		$aux_setting_information = unserialize(SETTING_INFORMATION);
		$array_setting_information = (isset($aux_setting_information[$index]))?$aux_setting_information[$index]:'';

		if(is_array($array_setting_information)){
			return $array_setting_information;
		}else{
			return false;
		}

	}
    
    
    /*
	 * Function to create a constant with table of setting Information
	 */
	function setting_constants(){
		if(defined('SETTING_INFORMATION')) return false;
		unset($settingObj,$array_setting);

		$settingObj = new Setting();
		$array_setting = $settingObj->convertTableToArray();

		if(is_array($array_setting)){
			define("SETTING_INFORMATION", serialize($array_setting));
		}

	}
    
    
    
    
    # ----------------------------------------------------------------------------------------------------
	# * FILE: /functions/activity_funct.php
	# ----------------------------------------------------------------------------------------------------


    /**
	 * Save new active record
     * @param integer $domain_id
     * @param integer $account_id
     * @param real $payment_amount
     * @param string $action
     * @param string $item_type
     * @param string $item_title
     */
    function activity_newActivity($domain_id = 0, $account_id = 0, $payment_amount = 0, $action = "", $item_type = "", $item_title = "''") {
        
        
        $sql = "INSERT INTO Recent_Activity (domain_id, account_id, payment_amount, action, item_type, item_title, date) VALUES ($domain_id, $account_id, $payment_amount, '$action', '$item_type', $item_title, NOW())";
        Self::insertSqlMain($sql);
       
    }
    
    
    /**
	 * Use this to verify if the domain dropdown must be disabled or not according to URL
	 * @copyright Copyright 2005 Arca Solutions, Inc.
	 * @author Arca Solutions, Inc.
	 * @version 7.8.00
	 * @package Classes
	 * @name domain_updateDashboard
	 * @param int $item
	 * @access Public
	 */
	function domain_updateDashboard($item = "", $action = "", $value = 0, $domain_id = SELECTED_DOMAIN_ID) {
		
		if ($item == "revenue"){
			$sql = "UPDATE Dashboard SET $item = ($item + $value) WHERE domain_id = ".$domain_id;
		}else{
			if ($action == "inc"){
				$sql = "UPDATE Dashboard SET $item = ($item + 1) WHERE domain_id = ".$domain_id;
			} else {
				$sql = "UPDATE Dashboard SET $item = ($item - 1) WHERE domain_id = ".$domain_id;
			}
		}
		Self::insertSqlMain($sql);

	}
    
    
    function domain_SaveAccountInfoDomain($aux_account, $object) {
		if (is_numeric(str_replace("'","",$object->domain_id))) {
			$accDomain = new Account_Domain($aux_account, str_replace("'","",$object->domain_id));
		} else {
			$accDomain = new Account_Domain($aux_account, SELECTED_DOMAIN_ID);
		}
		$accDomain->Save();
		$accDomain->saveOnDomain($aux_account, $object);
	}
    
    
    public static function updateSql($sql)
    {
        $data= DB::connection('domain')->update($sql);
        return $data;
    }
    
    public static function updateSqlMain($sql)
    {
        $data= DB::update($sql);
        return $data;
    }
    
    
     public static function fetch($sql)
        {
            $data= DB::connection('domain')->select($sql);
            return $data;
        }
        
        public static function fetchMain($sql)
        {
            $data= DB::select($sql);
            return $data;
        }
        
        public static function insertSql($sql)
        {
            $data= DB::connection('domain')->insert($sql);
            return $data;
        }
        public static function insertSqlMain($sql)
        {
            $data= DB::insert($sql);
            return $data;
        }
        
        
        public static function deleteSql($sql)
        {
            $data= DB::connection('domain')->delete($sql);
            return $data;
        }
        
        public static function deleteSqlMain($sql)
        {
            $data= DB::delete($sql);
            return $data;
        }
        
        
        
        
        
	}
?>