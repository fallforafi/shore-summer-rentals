<?php
namespace App\Functions;
use App\Functions\Setting;
use DB;
use Session;





	/**
	 * <code>
	 *		$rateObj = new Rate($id);
	 * <code>
	 * @version 8.0.00
	 * @package Classes
	 * @name Rate
	 * @method Rate
	 * @method makeFromRow
	 * @method Save
	 * @method Delete
	 * @access Public
	 */

	class Rate extends Handle {

		/**
		 * @var integer
		 * @access Private
		 */
		var $id;
		/**
		 * @var integer
		 * @access Private
		 */
		var $listing_id;
		/**
		 * @var integer
		 * @access Private
		 */
		var $from_date;
		/**
		 * @var date
		 * @access Private
		 */
		var $to_date;
		/**
		 * @var real
		 * @access Private
		 */
		var $day;		
		/**
		 * @var real
		 * @access Private
		 */
		var $week;		
		/**
		 * @var real
		 * @access Private
		 */
		var $month;		
		/**
		 * @var real
		 * @access Private
		 */
		var $season;
	

		/**
		 * <code>
		 *		$rateObj = new Rate($id);
		 * <code>
		 * @copyright Copyright 2005 Arca Solutions, Inc.
		 * @author Arca Solutions, Inc.
		 * @version 8.0.00
		 * @name Rate
		 * @access Public
		 * @param integer $var
		 */
		function Rate($var='') {
			if (is_numeric($var) && ($var)) {
				
				$sql = "SELECT * FROM Rate WHERE id = $var";
				$row = self::fetch($sql);

			if(!empty($row))
                {
                    $this->old_account_id = $row[0]->account_id;

				    $this->makeFromRow($row);
                }
                else
                {
                    return false;
                }
				
			} else {

                if (!is_array($var)) {
                    $var = array();
                }
				$this->makeFromRow($var);
			}
		}

		/**
		 * <code>
		 *		$this->makeFromRow($row);
		 * <code>
		 * @copyright Copyright 2005 Arca Solutions, Inc.
		 * @author Arca Solutions, Inc.
		 * @version 8.0.00
		 * @name makeFromRow
		 * @access Public
		 * @param array $row
		 */
		function makeFromRow($row='') {

			$this->id				= ($row[0]["id"])				? $row[0]["id"]			: ($this->id			? $this->id				: 0);
			$this->listing_id 		= ($row[0]["listing_id"]) 		? $row[0]["listing_id"] 	: ($this->listing_id	? $this->listing_id		: 0);
			$this->setDate("from_date", $row[0]["from_date"]);
			$this->setDate("to_date", $row[0]["to_date"]);
			$this->day            	= ($row[0]["day"])           	? $row[0]["day"]         		: ($this->day     			? $this->day      		: 0);
			$this->week            	= ($row[0]["week"])           	? $row[0]["week"]         		: ($this->week     			? $this->week      		: 0);
			$this->month            = ($row[0]["month"])           ? $row[0]["month"]         	: ($this->month     		? $this->month      	: 0);
			$this->season           = ($row[0]["season"])          ? $row[0]["season"]        	: ($this->season     		? $this->season      	: 0);
			
		}

		/**
		 * <code>
		 *		//Using this in forms or other pages.
		 *		$rateObj->Save();
		 * <br /><br />
		 *		//Using this in Rate() class.
		 *		$this->Save();
		 * </code>
		 * @copyright Copyright 2005 Arca Solutions, Inc.
		 * @author Arca Solutions, Inc.
		 * @version 8.0.00
		 * @name Save
		 * @access Public
		 */
		function Save() {

			//$this->prepareToSave();

			if ($this->id) {
				$sql  = "UPDATE Rate SET"
					. " listing_id = $this->listing_id,"
					. " from_date = $this->from_date,"
					. " to_date = $this->to_date,"
					. " day = $this->day,"
					. " week = $this->week,"
					. " month = $this->month,"
					. " season = $this->season"
					. " WHERE id = $this->id";
                       
				Self::insertSql($sql);
                  
                // activity_updateRecord(SELECTED_DOMAIN_ID, $this->id, $this->name, "item", "rate");

			} else {
                
				$sql = "INSERT INTO Rate (
							listing_id,
							from_date,
							to_date,
							day,
							week,
							month,
							season
						) VALUES (
							$this->listing_id,
							'$this->from_date',
							'$this->to_date',
							$this->day,
							$this->week,
							$this->month,
							$this->season
						)";
				 //d($sql,1);die;
     
               Self::insertSql($sql);


               //$this->id = mysql_insert_id($dbObj->link_id);

				// if (sess_getAccountIdFromSession()) {
				// 	activity_newActivity(SELECTED_DOMAIN_ID, sess_getAccountIdFromSession(), 0, "newitem", "rate", $this->name);
				// }
			}

			$this->prepareToUse();
		}

		/**
		 * <code>
		 *		//Using this in forms or other pages.
		 *		$rateObj->Delete();
		 * <code>
		 * @copyright Copyright 2005 Arca Solutions, Inc.
		 * @author Arca Solutions, Inc.
		 * @version 8.0.00
		 * @name Delete
		 * @access Public
		 * @param integer $domain_id
		 */
		function Delete($id) {
			$sql = "DELETE FROM Rate WHERE id = $id";
			Self::deleteSql($sql);
		}

		function DeleteAllFromId($listing_id) {
			

			$sql = "DELETE FROM Rate WHERE listing_id = $listing_id";
			Self::deleteSql($sql);
		} 


	public static function updateSql($sql)
    {
        $data= DB::connection('domain')->update($sql);
        return $data;
    }
    
    public static function updateSqlMain($sql)
    {
        $data= DB::update($sql);
        return $data;
    }
    
    
     public static function fetch($sql)
        {
            $data= DB::connection('domain')->select($sql);
            return $data;
        }
        
        public static function fetchMain($sql)
        {
            $data= DB::select($sql);
            return $data;
        }
        
        public static function insertSql($sql)
        {
            $data= DB::connection('domain')->insert($sql);
            return $data;
        }
        public static function insertSqlMain($sql)
        {
            $data= DB::insert($sql);
            return $data;
        }
        
        
        public static function deleteSql($sql)
        {
            $data= DB::connection('domain')->delete($sql);
            return $data;
        }
        
        public static function deleteSqlMain($sql)
        {
            $data= DB::delete($sql);
            return $data;
        }
    }
?>
