<?php 
namespace App\Functions;
use App\Functions\Functions;
use DB;
use PDO;
use App\Models\Domain\Sql;

	# ----------------------------------------------------------------------------------------------------
	# * FILE: PaymentListingChoiceLog.php
	# ----------------------------------------------------------------------------------------------------

	/**
	 * <code>
	 *		$paymentListingLogObj = new PaymentListingLog($id);
	 * <code>
	 * @method PaymentListingLog
	 * @method makeFromRow
	 * @method Save
	 * @access Public
	 */
	class PaymentListingChoiceLog extends Handle {

		/**
		 * @var integer
		 * @access Private
		 */
		var $payment_log_id;
		var $listing_id;
		var $listingchoice_id;
		var $badge_name;
		var $listing_title;
		var $renewal_date;
		var $amount;
		var $domain_id;

		/**
		 * <code>
		 *		$paymentListingLogObj = new PaymentListingLog($id);
		 * <code>
		 * @name PaymentListingLog
		 * @access Public
		 * @param integer $var
		 */
		function PaymentListingChoiceLog($var="", $domain_id = false) {
			$this->domain_id = $domain_id;
			DB::connection('domain')->setFetchMode(PDO::FETCH_ASSOC);

			if (is_numeric($var) && ($var)) {
				
				$row=DB::connection('domain')->table('Payment_ListingChoice_Log')->where('payment_log_id',$var)->get();

				$this->makeFromRow($row);
			} else {
                if (!is_array($var)) {
                    $var = array();
                }
				$this->makeFromRow($var);
			}
		}

		/**
		 * <code>
		 *		$this->makeFromRow($row);
		 * <code>
		 * @name makeFromRow
		 * @access Public
		 * @param array $row
		 */
		function makeFromRow($row="") {

			$this->payment_log_id   = (isset($row["payment_log_id"]))		? $row["payment_log_id"]	: ($this->payment_log_id	? $this->payment_log_id		: 0);
			$this->listing_id 		= ($row["listing_id"])			? $row["listing_id"]		: ($this->listing_id		? $this->listing_id			: 0);
			$this->listingchoice_id = ($row["listingchoice_id"])	? $row["listingchoice_id"]	: ($this->listingchoice_id	? $this->listingchoice_id	: 0);
			$this->badge_name       = ($row["badge_name"])			? $row["badge_name"]		: ($this->badge_name		? $this->badge_name			: "");
			$this->listing_title    = ($row["listing_title"])		? $row["listing_title"]		: ($this->listing_title		? $this->listing_title		: "");
			$this->renewal_date     = ($row["renewal_date"])		? $row["renewal_date"]		: ($this->renewal_date		? $this->renewal_date		: 0);
			$this->amount           = ($row["amount"])				? $row["amount"]			: ($this->amount			? $this->amount				: 0);



		}

		/**
		 * <code>
		 *		//Using this in forms or other pages.
		 *		$paymentListingLogObj->Save();
		 * <br /><br />
		 *		//Using this in PaymentListingLog() class.
		 *		$this->Save();
		 * </code>
		 * @name Save
		 * @access Public
		 */
		function Save() {

			$this->PrepareToSave();

			$sql = "INSERT INTO Payment_ListingChoice_Log"
				. " (payment_log_id,"
				. " listingchoice_id,"
				. " listing_id,"
				. " badge_name,"
				. " listing_title,"
				. " renewal_date,"
				. " amount"
				. " )"
				. " VALUES"
				. " ("
				. " $this->payment_log_id,"
				. " $this->listingchoice_id,"
				. " $this->listing_id,"
				. " $this->badge_name,"
				. " $this->listing_title,"
				. " $this->renewal_date,"
				. " $this->amount"
				. " )";

				Sql::insertSql($sql);

				$this->id = DB::connection('domain')->getpdo()->lastInsertId();

			$this->PrepareToUse();

		}

	}

?>

