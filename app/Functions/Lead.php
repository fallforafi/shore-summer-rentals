<?php
namespace App\Functions;
use DB,Mail;
use App\Functions\Functions;
use App\Models\Domain\Sql;
use App\Models\Main\MainSql;
use App\Functions\ItemStatus;
use App\Functions\Listing;

	/*==================================================================*\
	######################################################################
	#                                                                    #
	#  Inc. All Rights Reserved.           #
	#                                                                    #
	# This file may not be redistributed in whole or part.               #
	# eDirectory is licensed on a per-domain basis.                      #
	#                                                                    #
	# ---------------- eDirectory IS NOT FREE SOFTWARE ----------------- #
	#                                                                    #
	#  #
	######################################################################
	\*==================================================================*/

	# ----------------------------------------------------------------------------------------------------
	# * FILE: /classes/class_Lead.php
	# ----------------------------------------------------------------------------------------------------

	/**
	 * <code>
	 *		$leadObj = new Lead($id, $module, $domain_id);
	 * <code>
	 * @version 10.0.01
	 * @package Classes
	 * @name Lead
	 * @method Lead
	 * @method makeFromRow
	 * @method Save
	 * @method Delete
	 * @method Reply
	 * @method Forward
	 * @access Public
	 */
	class Lead extends Handle {

		/**
		 * @var integer
		 * @access Private
		 */
		var $id;
        /**
		 * @var integer
		 * @access Private
		 */
		var $item_id;
        /**
		 * @var integer
		 * @access Private
		 */
		var $member_id;
         /**
		 * @var string
		 * @access Private
		 */
		var $type;
        /**
		 * @var string
		 * @access Private
		 */
		var $first_name;
         /**
		 * @var string
		 * @access Private
		 */
		var $last_name;
        /**
		 * @var string
		 * @access Private
		 */
		var $email;
         /**
		 * @var string
		 * @access Private
		 */
		var $phone;
        /**
		 * @var string
		 * @access Private
		 */
		var $subject;
        /**
		 * @var string
		 * @access Private
		 */
		var $message;
		/**
		 * @var datetitme
		 * @access Private
		 */
		var $entered;
        /**
		 * @var date
		 * @access Private
		 */
		var $reply_date;
        /**
		 * @var date
		 * @access Private
		 */
		var $forward_date;
		/**
		 * @var char
		 * @access Private
		 */
		var $status;
        /**
		 * @var char
		 * @access Private
		 */
		var $new;
        /**
		 * @var integer
		 * @access Private
		 */
		var $domain_id;
        /**
		 * @var mixed
		 * @access Private
		 */
        var $data_in_array;

		/**
		 * <code>
		 *		$leadObj = new Lead($id, $domain_id);
		 * <code>
		 * @name Lead
		 * @access Public
		 * @param integer $var
		 * @param integer $domain_id
		 */
		function Lead($var = "") {

			if (is_numeric($var) && ($var)) {
				
				$sql = "SELECT * FROM Leads WHERE id = $var";
				$row = Sql::fetch($sql);
				$this->makeFromRow($row);
			} else {
                if (!is_array($var)) {
                    $var = array();
                }
				$this->makeFromRow($var);
			}
		}

		/**
		 * <code>
		 *		$this->makeFromRow($row);
		 * <code>
		 * @copyright Copyright 2005 Arca Solutions, Inc.
		 * @author Arca Solutions, Inc.
		 * @version 10.0.01
		 * @name makeFromRow
		 * @access Public
		 * @param array $row
		 */
		function makeFromRow($row = "") {
			
            $status = new ItemStatus();
            
            $this->id					= ($row[0]->id)					? $row[0]->id					: ($this->id				? $this->id					: 0);
            $this->item_id				= ($row[0]->item_id)				? $row[0]->item_id				: ($this->item_id			? $this->item_id			: 0);
            $this->member_id			= ($row[0]->member_id)			? $row[0]->member_id				: ($this->member_id			? $this->member_id			: 0);
			$this->type                 = ($row[0]->type)				? $row[0]->type					: ($this->type				? $this->type				: "");
			$this->first_name           = ($row[0]->first_name)			? $row[0]->first_name			: ($this->first_name		? $this->first_name			: "");
			$this->last_name            = ($row[0]->last_name)			? $row[0]->last_name             : ($this->last_name         ? $this->last_name			: "");
			$this->email                = ($row[0]->email)				? $row[0]->email					: ($this->email				? $this->email				: "");
			$this->phone                = ($row[0]->phone)				? $row[0]->phone					: ($this->phone				? $this->phone				: "");
			$this->subject              = ($row[0]->subject)				? $row[0]->subject               : ($this->subject			? $this->subject			: "");
			$this->message              = ($row[0]->message)				? $row[0]->message               : ($this->message			? $this->message			: "");
            $this->entered				= ($row[0]->entered)				? $row[0]->entered				: ($this->entered			? $this->entered			: "");
            $this->reply_date			= ($row[0]->reply_date)			? $row[0]->reply_date			: ($this->reply_date		? $this->reply_date			: "");
            $this->forward_date			= ($row[0]->forward_date)		? $row[0]->forward_date			: ($this->forward_date		? $this->forward_date		: "");
            $this->new                  = ($row[0]->new)                 ? $row[0]->new                   : ($this->new               ? $this->new                : "y");
            $this->status				= ($row[0]->status)				? $row[0]->status				: $status->getDefaultStatus();
            $this->data_in_array        = $row;

		}
		function arraymakeFromRow($row = "") {
			
            $status = new ItemStatus();
            $status->ItemStatus();
            
            $this->id					= (isset($row["id"]))					? $row["id"]					: ($this->id				? $this->id					: 0);
            $this->item_id				= (isset($row["item_id"]))				? $row["item_id"]				: ($this->item_id			? $this->item_id			: 0);
            $this->member_id			= (isset($row["member_id"]))				? $row["member_id"]				: ($this->member_id			? $this->member_id			: 0);
			$this->type                 = (isset($row["type"]))				? $row["type"]					: ($this->type				? $this->type				: "");
			$this->first_name           = (isset($row["first_name"]))			? $row["first_name"]			: ($this->first_name		? $this->first_name			: "");
			$this->last_name            = (isset($row["last_name"]))			? $row["last_name"]             : ($this->last_name         ? $this->last_name			: "");
			$this->email                = (isset($row["email"]))				? $row["email"]					: ($this->email				? $this->email				: "");
			$this->phone                = (isset($row["phone"]))				? $row["phone"]					: ($this->phone				? $this->phone				: "");
			$this->subject              = (isset($row["subject"]))				? $row["subject"]               : ($this->subject			? $this->subject			: "");
			$this->message              = (isset($row["message"]))				? $row["message"]               : ($this->message			? $this->message			: "");
            $this->entered				= (isset($row["entered"]))				? $row["entered"]				: ($this->entered			? $this->entered			: "");
            $this->reply_date			= (isset($row["reply_date"]))			? $row["reply_date"]			: ($this->reply_date		? $this->reply_date			: "");
            $this->forward_date			= (isset($row["forward_date"]))		? $row["forward_date"]			: ($this->forward_date		? $this->forward_date		: "");
            $this->new                  = (isset($row["new"]))                 ? $row["new"]                   : ($this->new               ? $this->new                : "y");
            $this->status				= (isset($row["status"]))				? $row["status"]				: $status->getDefaultStatus();
            $this->data_in_array        = $row;

		}

		/**
		 * <code>
		 *		//Using this in forms or other pages.
		 *		$leadObj->Save();
		 * <br /><br />
		 *		//Using this in Lead() class.
		 *		$this->Save();
		 * </code>
		 * @copyright Copyright 2005 Arca Solutions, Inc.
		 * @author Arca Solutions, Inc.
		 * @version 10.0.01
		 * @name Save
		 * @access Public
		 */
		function Save() {

            
			$this->prepareToSave();
            
			if ($this->id) {
                
				$sql  = "UPDATE Leads SET"
					. " status = $this->status,
                        new = $this->new"
					. " WHERE id = $this->id";
					Sql::insertSql($sql);
                
			} else {
                
				$sql = "INSERT INTO Leads ("
					. " item_id, 
                        member_id, 
                        type, 
                        first_name, 
                        last_name, 
                        email, 
                        phone, 
                        subject, 
                        message, 
                        entered, 
                        status)"
					. " VALUES"
					. " ($this->item_id, 
                        $this->member_id, 
                        $this->type, 
                        $this->first_name, 
                        $this->last_name, 
                        $this->email, 
                        $this->phone, 
                        $this->subject, 
                        $this->message, 
                        NOW(), 
                        $this->status)";
				
				Sql::insertSql($sql);
				$this->id = DB::getpdo()->lastInsertId();
                
                if ($this->type == "'listing'") {
                    $itemObj = new Listing(str_replace("'", "", $this->item_id));
                    $itemTitle = $itemObj->getString("title");
                } 
                /*elseif ($this->type == "'event'") {
                    $itemObj = new Event(str_replace("'", "", $this->item_id));
                     $itemTitle = $itemObj->getString("title");
                } elseif ($this->type == "'classified'") {
                    $itemObj = new Classified(str_replace("'", "", $this->item_id));
                     $itemTitle = $itemObj->getString("title");
                } else {
                    $itemTitle = system_showText(LANG_LABEL_GENERAL_FORM);
                }
                
                activity_newActivity($aux_log_domain_id, $this->member_id, 0, "newlead", str_replace("'", "", $this->type), db_formatString($itemTitle));*/

			}
			
			$this->prepareToUse();
		}

		/**
		 * <code>
		 *		//Using this in forms or other pages.
		 *		$leadObj->Delete();
		 * <code>
		 * @copyright Copyright 2005 Arca Solutions, Inc.
		 * @author Arca Solutions, Inc.
		 * @version 10.0.01
		 * @name Delete
		 * @access Public
		 */
		function Delete() {
            
			$dbMain = db_getDBObject(DEFAULT_DB, true);

			if ($this->domain_id) {
                $dbObj = db_getDBObjectByDomainID($this->domain_id, $dbMain);
			} elseif (defined("SELECTED_DOMAIN_ID")) {
                $dbObj = db_getDBObjectByDomainID(SELECTED_DOMAIN_ID, $dbMain);
			} else {
                $dbObj = db_getDBObject();
			}

			unset($dbMain);

			$sql = "DELETE FROM Leads WHERE id = $this->id";
			$dbObj->query($sql);
		}
        
        /**
		 * <code>
		 *		//Using this in forms or other pages.
		 *		$leadObj->Reply($message);
		 * <code>
		 * @copyright Copyright 2005 Arca Solutions, Inc.
		 * @author Arca Solutions, Inc.
		 * @version 10.0.01
		 * @name Reply
         * @param text $message
         * @param string $to
		 * @access Public
        */
		function Reply($message, $to) {
         
            $sql = "UPDATE Leads SET status = 'A', reply_date = NOW() WHERE id = $this->id";
            Sql::insertSql($sql);

           	$reply = '';
            if ( $this->type == 'listing' ) { 
	        	$replyItem = new Listing();
	        	$replyItem->Listing($this->item_id);
	        	$reply = $replyItem->getString('email');
	        }
            //setting_get("sitemgr_email", $sitemgr_email);
	        $sitemgr_email='amoosjohn@gmail.com';

             $data= [
            'sitemgr_msg' => $message,
            'subject' => 'RE:'.$this->subject,
            'email_send' =>$to,
            'from'=>$sitemgr_email
        ];

    Mail::send(['text' => 'email.signup'],['data' => $data], function($message) use ($data) {
   
        $message->from($data['from']);
     $message->to($data['email_send'])->subject($data['subject']);
    });

            //system_mail($to, "Re: ".$this->subject, $message, " <$sitemgr_email>", "text/plain", '', '', $error, '', '', $reply);
        }
        
        /**
		 * <code>
		 *		//Using this in forms or other pages.
		 *		$leadObj->Forward($message);
		 * <code>
		 * @copyright Copyright 2005 Arca Solutions, Inc.
		 * @author Arca Solutions, Inc.
		 * @version 10.0.01
		 * @name Forward
         * @param text $message
         * @param string $to
		 * @access Public
        */
		function Forward($message, $to) {
            

			unset($dbMain);
            
           $sql = "UPDATE Leads SET status = 'A', forward_date = NOW() WHERE id = $this->id";
           Sql::insertSql($sql);
            
            setting_get("sitemgr_email", $sitemgr_email);
            system_mail($to, "Fwd: ".$this->subject, $message, " <$sitemgr_email>", "text/plain", '', '', $error);
        }

	}
?>