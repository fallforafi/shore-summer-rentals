<?php 

 namespace App\Functions;
 use DB;
 use App\Functions\Location1;
 use App\Functions\Location2;
 use App\Functions\Location3;
 use App\Functions\Location4;
 use App\Functions\Location5;
	/*==================================================================*\
	######################################################################
	#                                                                    #
	# Copyright 2005 Arca Solutions, Inc. All Rights Reserved.           #
	#                                                                    #
	# This file may not be redistributed in whole or part.               #
	# eDirectory is licensed on a per-domain basis.                      #
	#                                                                    #
	# ---------------- eDirectory IS NOT FREE SOFTWARE ----------------- #
	#                                                                    #
	# http://www.edirectory.com | http://www.edirectory.com/license.html #
	######################################################################
	\*==================================================================*/

	# ----------------------------------------------------------------------------------------------------
	# * FILE: /classes/class_ListingSummary.php
	# ----------------------------------------------------------------------------------------------------

	/**
	 * <code>
	 *		$listingObj = new ListingSummary($id);
	 * <code>
	 * @copyright Copyright 2005 Arca Solutions, Inc.
	 * @author Arca Solutions, Inc.
	 * @version 8.0.00
	 * @package Classes
	 * @name ListingSummary
	 * @method ListingSummary
	 * @method makeFromRow
	 * @method Add
	 * @method Update
	 * @method Delete
	 * @method PopulateTable
	 * @method PopulateTableCategory
	 * @method PopulateTableLocation
	 * @access Public
	 */
	class ListingSummary extends Handle {
		/**
		 * @var integer
		 * @access Private
		 */
		var $id;
		/**
		 * @var integer
		 * @access Private
		 */
		var $account_id;
		/**
		 * @var integer
		 * @access Private
		 */
		var $location_1;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $location_1_title;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $location_1_abbreviation;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $location_1_friendly_url;
		/**
		 * @var integer
		 * @access Private
		 */
		var $location_2;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $location_2_title;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $location_2_abbreviation;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $location_2_friendly_url;
		/**
		 * @var integer
		 * @access Private
		 */
		var $location_3;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $location_3_title;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $location_3_abbreviation;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $location_3_friendly_url;
		/**
		 * @var integer
		 * @access Private
		 */
		var $location_4;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $location_4_title;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $location_4_abbreviation;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $location_4_friendly_url;
		/**
		 * @var integer
		 * @access Private
		 */
		var $location_5;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $location_5_title;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $location_5_abbreviation;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $location_5_friendly_url;

		/**
		 * @var varchar
		 * @access Private
		 */
		var $title;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $friendly_url;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $email;
		/**
		 * @var char
		 * @access Private
		 */
		var $show_email;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $url;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $display_url;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $address;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $address2;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $zip_code;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $zip5;
		/**
		 * @var double
		 * @access Private
		 */
		var $latitude;
		/**
		 * @var double
		 * @access Private
		 */
		var $longitude;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $phone;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $fax;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $description;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $attachment_file;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $attachment_caption;
		/**
		 * @var char
		 * @access Private
		 */
		var $status;
		/**
		 * @var date
		 * @access Private
		 */
		var $renewal_date;
		/**
		 * @var integer
		 * @access Private
		 */
		var $level;
		/**
		 * @var integer
		 * @access Private
		 */
		var $random_number;
		/**
		 * @var char
		 * @access Private
		 */
		var $claim_disable;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $locations;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $keywords;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $seo_keywords;
		/**
		 * @var varchar
		 * @access Private
		 */
		
		var $fulltextsearch_keyword;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $fulltextsearch_where;
        /**
		 * @var char
		 * @access Private
		 */
		var $custom_checkbox0;
		/**
		 * @var char
		 * @access Private
		 */
		var $custom_checkbox1;
		/**
		 * @var char
		 * @access Private
		 */
		var $custom_checkbox2;
		/**
		 * @var char
		 * @access Private
		 */
		var $custom_checkbox3;
		/**
		 * @var char
		 * @access Private
		 */
		var $custom_checkbox4;
		/**
		 * @var char
		 * @access Private
		 */
		var $custom_checkbox5;
		/**
		 * @var char
		 * @access Private
		 */
		var $custom_checkbox6;
		/**
		 * @var char
		 * @access Private
		 */
		var $custom_checkbox7;
		/**
		 * @var char
		 * @access Private
		 */
		var $custom_checkbox8;
		/**
		 * @var char
		 * @access Private
		 */
		var $custom_checkbox9;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_dropdown0;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_dropdown1;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_dropdown2;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_dropdown3;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_dropdown4;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_dropdown5;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_dropdown6;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_dropdown7;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_dropdown8;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_dropdown9;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_text0;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_text1;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_text2;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_text3;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_text4;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_text5;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_text6;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_text7;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_text8;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_text9;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_short_desc0;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_short_desc1;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_short_desc2;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_short_desc3;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_short_desc4;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_short_desc5;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_short_desc6;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_short_desc7;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_short_desc8;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_short_desc9;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_long_desc0;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_long_desc1;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_long_desc2;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_long_desc3;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_long_desc4;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_long_desc5;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_long_desc6;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_long_desc7;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_long_desc8;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $custom_long_desc9;
		/**
		 * @var integer
		 * @access Private
		 */
		var $number_views;
		/**
		 * @var integer
		 * @access Private
		 */
		var $avg_review;
        /**
		 * @var integer
		 * @access Private
		 */
		var $price;
		/**
		 * @var date
		 * @access Private
		 */
		var $promotion_start_date;
		/**
		 * @var date
		 * @access Private
		 */
		var $promotion_end_date;
		/**
		 * @var integer
		 * @access Private
		 */
		var $thumb_id;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $thumb_type;
		/**
		 * @var integer
		 * @access Private
		 */
		var $thumb_width;
		/**
		 * @var integer
		 * @access Private
		 */
		var $thumb_height;
		/**
		 * @var integer
		 * @access Private
		 */
		var $listingtemplate_id;
		/**
		 * @var integer
		 * @access Private
		 */
		var $template_layout_id;
		/**
		 * @var integer
		 * @access Private
		 */
		var $template_cat_id;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $template_title;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $template_status;
		/**
		 * @var integer
		 * @access Private
		 */
		var $image_id;
		/**
		 * @var date
		 * @access Private
		 */
		var $updated;
		/**
		 * @var date
		 * @access Private
		 */
		var $entered;
		/**
		 * @var integer
		 * @access Private
		 */
		var $promotion_id;
		/**
		 * @var integer
		 * @access Private
		 */
		var $domain_id;
		/**
		 * @var char
		 * @access Private
		 */
		var $backlink;
		/**
		 * @var integer
		 * @access Private
		 */
		var $clicktocall_number;
		/**
		 * @var integer
		 * @access Private
		 */
		var $clicktocall_extension;
		var $bedroom;
		var $bedsize;
		var $sleeps;
		var $bathroom;
		var $property_type;
		var $view;
		var $distance_beach;
		var $development_name;

		var $airport_distance;
		var $airport_abbreviation;

		var $contact_fname;
		var $contact_lname;

		var $amenity_airhockey;
		var $amenity_alarmclock;
		var $amenity_answeringmachine;
		var $amenity_arcadegames;
		var $amenity_billiards;
		var $amenity_blender;
		var $amenity_blurayplayer;
		var $amenity_books;
		var $amenity_casetteplayer;
		var $amenity_cdplayer;
		var $amenity_ceilingfan;
		var $amenity_childshighchair;
		var $amenity_coffeemaker;
		var $amenity_communalpool;
		var $amenity_computer;
		var $amenity_cookware;
		var $amenity_cookingrange;
		var $amenity_deckfurniture;
		var $amenity_dishwasher;
		var $amenity_dishes;
		var $amenity_dvdplayer;
		var $amenity_exercisefacilities;
		var $amenity_foosball;
		var $amenity_gametable;
		var $amenity_games;
		var $amenity_grill;
		var $amenity_hairdryer;
		var $amenity_icemaker;
		var $amenity_internet;
		var $amenity_ironandboard;
		var $amenity_kidsgames;
		var $amenity_linensprovided;
		var $amenity_lobsterpot;
		var $amenity_microwave;
		var $amenity_minirefrigerator;
		var $amenity_mp3radiodock;
		var $amenity_outsideshower;
		var $amenity_oven;
		var $amenity_pinball;
		var $amenity_pingpong;
		var $amenity_privatepool;
		var $amenity_radio;
		var $amenity_refrigeratorfreezer;
		var $amenity_sofabed;
		var $amenity_stereo;
		var $amenity_telephone;
		var $amenity_television;
		var $amenity_toaster;
		var $amenity_toasteroven;
		var $amenity_towelsprovided;
		var $amenity_toys;
		var $amenity_utensils;
		var $amenity_vacuum;
		var $amenity_vcr;
		var $amenity_videogameconsole;
		var $amenity_videogames;
		var $amenity_washerdryer;
		var $amenity_wifi;		
		var $feature_airconditioning;
		var $feature_balcony;
		var $feature_barbecuecharcoal;
		var $feature_barbecuegas;
		var $feature_boatslip;
		var $feature_cablesatellitetv;
		var $feature_clubhouse;
		var $feature_coveredparking;
		var $feature_deck;
		var $feature_diningroom;
		var $feature_elevator;
		var $feature_familyroom;
		var $feature_fullkitchen;
		var $feature_garage;
		var $feature_gasfireplace;
		var $feature_gatedcommunity;
		var $feature_wheelchairaccess;
		var $feature_heated;
		var $feature_heatedpool;
		var $feature_hottubjacuzzi;
		var $feature_internetaccess;
		var $feature_kitchenette;
		var $feature_livingroom;
		var $feature_loft;
		var $feature_onsitesecurity;
		var $feature_patio;
		var $feature_petsallowed;
		var $feature_playroom;
		var $feature_pool;
		var $feature_porch;
		var $feature_rooftopdeck;
		var $feature_sauna;
		var $feature_smokingpermitted;
		var $feature_woodfireplace;
		var $activity_antiquing;
		var $activity_basketballcourt;
		var $activity_beachcombing;
		var $activity_bicycling;
		var $activity_bikerentals;
		var $activity_birdwatching;
		var $activity_boatrentals;
		var $activity_boating;
		var $activity_botanicalgarden;
		var $activity_canoe;
		var $activity_churches;
		var $activity_cinemas;
		var $activity_bikesprovided;
		var $activity_deepseafishing;
		var $activity_fishing;
		var $activity_fitnesscenter;
		var $activity_golf;
		var $activity_healthbeautyspa;
		var $activity_hiking;
		var $activity_horsebackriding;
		var $activity_horseshoes;
		var $activity_hotairballooning;
		var $activity_iceskating;
		var $activity_jetskiing;
		var $activity_kayaking;
		var $activity_livetheater;
		var $activity_marina;
		var $activity_miniaturegolf;
		var $activity_mountainbiking;
		var $activity_museums;
		var $activity_paddleboating;
		var $activity_paragliding;
		var $activity_parasailing;
		var $activity_playground;
		var $activity_recreationcenter;
		var $activity_restaurants;
		var $activity_rollerblading;
		var $activity_sailing;
		var $activity_shelling;
		var $activity_shopping;
		var $activity_sightseeing;
		var $activity_skiing;
		var $activity_bayfishing;
		var $activity_spa;
		var $activity_surffishing;
		var $activity_surfing;
		var $activity_swimming;
		var $activity_tennis;
		var $activity_themeparks;
		var $activity_walking;
		var $activity_waterparks;
		var $activity_waterskiing;
		var $activity_watertubing;
		var $activity_wildlifeviewing;
		var $activity_zoo;
		var $required_stay;
		var $is_featured;
		var $featured_date;
		/**
		 * @var integer
		 * @access Private
		 */
		var $search_pos;
		/**
		 * @var date
		 * @access Private
		 */
		var $search_pos_date;

		/**
		 * <code>
		 *		$listingObj = new ListingSummary($id);
		 *		//OR
		 *		$listingObj = new ListingSummary($row);
		 * <code>
		 * @copyright Copyright 2005 Arca Solutions, Inc.
		 * @author Arca Solutions, Inc.
		 * @version 8.0.00
		 * @name ListingSummary
		 * @access Public
		 * @param mixed $var
		 */
		function ListingSummary($var='') {

			if (is_numeric($var) && ($var)) {
				$dbMain = db_getDBObject(DEFAULT_DB, true);
				if (defined("SELECTED_DOMAIN_ID")) {
					$db = db_getDBObjectByDomainID(SELECTED_DOMAIN_ID, $dbMain);
				} else {
					$db = db_getDBObject();
				}
				unset($dbMain);
				$sql = "SELECT * FROM Listing_Summary WHERE id = $var";
				$row = mysql_fetch_array($db->query($sql));
				$this->makeFromRow($row);
			} else {
                if (!is_array($var)) {
                    $var = array();
                }
				$this->makeFromRow($var);
			}

		}

		/**
		 * <code>
		 *		$this->makeFromRow($row);
		 * <code>
		 * @copyright Copyright 2005 Arca Solutions, Inc.
		 * @author Arca Solutions, Inc.
		 * @version 8.0.00
		 * @name makeFromRow
		 * @access Public
		 * @param array $row
		 */
		function makeFromRow($row='') {
		$this->id								= ($row->id)						? $row->id							: 0;
			$this->account_id						= ($row->account_id)				? $row->account_id					: 0;

			$this->location_1						= ($row->location_1)				? $row->location_1					: 0;
			$this->location_1_title					= ($row->location_1_title)		? $row->location_1_title				: "";
			$this->location_1_abbreviation			= ($row->location_1_abbreviation)	? $row->location_1_abbreviation		: "";
			$this->location_1_friendly_url			= ($row->location_1_friendly_url)	? $row->location_1_friendly_url		: "";
			
			$this->location_2						= ($row->location_2)				? $row->location_2					: 0;
			$this->location_2_title					= ($row->location_2_title)		? $row->location_2_title				: "";
			$this->location_2_abbreviation			= ($row->location_2_abbreviation)	? $row->location_2_abbreviation		: "";
			$this->location_2_friendly_url			= ($row->location_2_friendly_url)	? $row->location_2_friendly_url		: "";
			
			$this->location_3						= ($row->location_3)				? $row->location_3					: 0;
			$this->location_3_title					= ($row->location_3_title)		? $row->location_3_title				: "";
			$this->location_3_abbreviation			= ($row->location_3_abbreviation)	? $row->location_3_abbreviation		: "";
			$this->location_3_friendly_url			= ($row->location_3_friendly_url)	? $row->location_3_friendly_url		: "";
			
			$this->location_4						= ($row->location_4)				? $row->location_4					: 0;
			$this->location_4_title					= ($row->location_4_title)		? $row->location_4_title				: "";
			$this->location_4_abbreviation			= ($row->location_4_abbreviation)	? $row->location_4_abbreviation		: "";
			$this->location_4_friendly_url			= ($row->location_4_friendly_url)	? $row->location_4_friendly_url		: "";
			
			$this->location_5						= ($row->location_5)				? $row->location_5					: 0;
			$this->location_5_title					= ($row->location_5_title)		? $row->location_5_title				: "";
			$this->location_5_abbreviation			= ($row->location_5_abbreviation)	? $row->location_5_abbreviation		: "";
			$this->location_5_friendly_url			= ($row->location_5_friendly_url)	? $row->location_5_friendly_url		: "";

			$this->title							= ($row->title)					? $row->title							: "";
			$this->friendly_url						= ($row->friendly_url)			? $row->friendly_url					: "";
			$this->email							= ($row->email)					? $row->email							: "";
			$this->url								= ($row->url)						? $row->url							: "";
			$this->display_url						= ($row->display_url)				? $row->display_url					: "";
			$this->address							= ($row->address)					? $row->address						: "";
			$this->address2							= ($row->address2)				? $row->address2						: "";
			$this->zip_code							= ($row->zip_code)				? $row->zip_code						: "";
			$this->phone							= ($row->phone)					? $row->phone							: "";
			$this->fax								= ($row->fax)						? $row->fax							: "";

			$this->description						= ($row->description)             ? $row->description					: "";

			$this->show_email						= ($row->show_email)				? $row->show_email						: "";
			$this->zip5								= ($row->zip5)					? $row->zip5								: "";
			$this->latitude							= ($row->latitude)				? $row->latitude							: "";
			$this->longitude						= ($row->longitude)				? $row->longitude							: "";

			$this->attachment_file					= ($row->attachment_file)			? $row->attachment_file					: "";
			$this->attachment_caption				= ($row->attachment_caption)		? $row->attachment_caption				: "";
			$this->status							= ($row->status)					? $row->status							: "";
			$this->setDate("renewal_date", $row->renewal_date);
			$this->level							= ($row->level)					? $row->level								: "";
			$this->random_number					= ($row->random_number)			? $row->random_number						: 0;
			$this->claim_disable					= ($row->claim_disable)			? $row->claim_disable						: "n";
			$this->locations						= ($row->locations)				? $row->locations							: "";

			$this->keywords                         = ($row->keywords)				? $row->keywords								: "";

			$this->seo_keywords                     = ($row->seo_keywords)			? $row->seo_keywords							: ($this->seo_keywords		? $this->seo_keywords		: "");
			
			$this->fulltextsearch_keyword			= ($row->fulltextsearch_keyword)	? $row->fulltextsearch_keyword				: "";
			$this->fulltextsearch_where				= ($row->fulltextsearch_where)	? $row->fulltextsearch_where					: "";
            
            $this->custom_checkbox0					= ($row->custom_checkbox0)		? $row->custom_checkbox0		: "";
			$this->custom_checkbox1					= ($row->custom_checkbox1)		? $row->custom_checkbox1		: "";
			$this->custom_checkbox2					= ($row->custom_checkbox2)		? $row->custom_checkbox2		: "";
			$this->custom_checkbox3					= ($row->custom_checkbox3)		? $row->custom_checkbox3		: "";
			$this->custom_checkbox4					= ($row->custom_checkbox4)		? $row->custom_checkbox4		: "";
			$this->custom_checkbox5					= ($row->custom_checkbox5)		? $row->custom_checkbox5		: "";
			$this->custom_checkbox6					= ($row->custom_checkbox6)		? $row->custom_checkbox6		: "";
			$this->custom_checkbox7					= ($row->custom_checkbox7)		? $row->custom_checkbox7		: "";
			$this->custom_checkbox8					= ($row->custom_checkbox8)		? $row->custom_checkbox8		: "";
			$this->custom_checkbox9					= ($row->custom_checkbox9)		? $row->custom_checkbox9		: "";
			$this->custom_dropdown0					= ($row->custom_dropdown0)		? $row->custom_dropdown0		: "";
			$this->custom_dropdown1					= ($row->custom_dropdown1)		? $row->custom_dropdown1		: "";
			$this->custom_dropdown2					= ($row->custom_dropdown2)		? $row->custom_dropdown2		: "";
			$this->custom_dropdown3					= ($row->custom_dropdown3)		? $row->custom_dropdown3		: "";
			$this->custom_dropdown4					= ($row->custom_dropdown4)		? $row->custom_dropdown4		: "";
			$this->custom_dropdown5					= ($row->custom_dropdown5)		? $row->custom_dropdown5		: "";
			$this->custom_dropdown6					= ($row->custom_dropdown6)		? $row->custom_dropdown6		: "";
			$this->custom_dropdown7					= ($row->custom_dropdown7)		? $row->custom_dropdown7		: "";
			$this->custom_dropdown8					= ($row->custom_dropdown8)		? $row->custom_dropdown8		: "";
			$this->custom_dropdown9					= ($row->custom_dropdown9)		? $row->custom_dropdown9		: "";
			$this->custom_text0						= ($row->custom_text0)			? $row->custom_text0			: "";
			$this->custom_text1						= ($row->custom_text1)			? $row->custom_text1			: "";
			$this->custom_text2						= ($row->custom_text2)			? $row->custom_text2			: "";
			$this->custom_text3						= ($row->custom_text3)			? $row->custom_text3			: "";
			$this->custom_text4						= ($row->custom_text4)			? $row->custom_text4			: "";
			$this->custom_text5						= ($row->custom_text5)			? $row->custom_text5			: "";
			$this->custom_text6						= ($row->custom_text6)			? $row->custom_text6			: "";
			$this->custom_text7						= ($row->custom_text7)			? $row->custom_text7			: "";
			$this->custom_text8						= ($row->custom_text8)			? $row->custom_text8			: "";
			$this->custom_text9						= ($row->custom_text9)			? $row->custom_text9			: "";
			$this->custom_short_desc0				= ($row->custom_short_desc0)		? $row->custom_short_desc0	: "";
			$this->custom_short_desc1				= ($row->custom_short_desc1)		? $row->custom_short_desc1	: "";
			$this->custom_short_desc2				= ($row->custom_short_desc2)		? $row->custom_short_desc2	: "";
			$this->custom_short_desc3				= ($row->custom_short_desc3)		? $row->custom_short_desc3	: "";
			$this->custom_short_desc4				= ($row->custom_short_desc4)		? $row->custom_short_desc4	: "";
			$this->custom_short_desc5				= ($row->custom_short_desc5)		? $row->custom_short_desc5	: "";
			$this->custom_short_desc6				= ($row->custom_short_desc6)		? $row->custom_short_desc6	: "";
			$this->custom_short_desc7				= ($row->custom_short_desc7)		? $row->custom_short_desc7	: "";
			$this->custom_short_desc8				= ($row->custom_short_desc8)		? $row->custom_short_desc8	: "";
			$this->custom_short_desc9				= ($row->custom_short_desc9)		? $row->custom_short_desc9	: "";
			$this->custom_long_desc0				= ($row->custom_long_desc0)		? $row->custom_long_desc0		: "";
			$this->custom_long_desc1				= ($row->custom_long_desc1)		? $row->custom_long_desc1		: "";
			$this->custom_long_desc2				= ($row->custom_long_desc2)		? $row->custom_long_desc2		: "";
			$this->custom_long_desc3				= ($row->custom_long_desc3)		? $row->custom_long_desc3		: "";
			$this->custom_long_desc4				= ($row->custom_long_desc4)		? $row->custom_long_desc4		: "";
			$this->custom_long_desc5				= ($row->custom_long_desc5)		? $row->custom_long_desc5		: "";
			$this->custom_long_desc6				= ($row->custom_long_desc6)		? $row->custom_long_desc6		: "";
			$this->custom_long_desc7				= ($row->custom_long_desc7)		? $row->custom_long_desc7		: "";
			$this->custom_long_desc8				= ($row->custom_long_desc8)		? $row->custom_long_desc8		: "";
			$this->custom_long_desc9				= ($row->custom_long_desc9)		? $row->custom_long_desc9		: "";
			
			$this->number_views						= ($row->number_views)			? $row->number_views			: 0;
			$this->avg_review						= ($row->avg_review)				? $row->avg_review			: 0;
			$this->price                            = ($row->price)                   ? $row->price                 : 0;
			$this->setDate("promotion_start_date", $row->promotion_start_date);
			$this->setDate("promotion_end_date", $row->promotion_end_date);

			$this->thumb_id							= ($row->thumb_id)						? $row->thumb_id							: 0;
			$this->image_id							= ($row->image_id)						? $row->image_id							: 0;
			$this->thumb_type						= ($row->thumb_type)						? $row->thumb_type						: 0;
			$this->thumb_width						= ($row->thumb_width)						? $row->thumb_width						: 0;
			$this->thumb_height						= ($row->thumb_height)					? $row->thumb_height						: 0;
			$this->listingtemplate_id				= ($row->listingtemplate_id)				? $row->listingtemplate_id				: 0;
			$this->template_layout_id				= ($row->template_layout_id)				? $row->template_layout_id				: 0;
			$this->template_cat_id					= ($row->template_cat_id)					? $row->template_cat_id					: 0;
			$this->template_title					= ($row->template_title)					? $row->template_title					: "";
			$this->template_status					= ($row->template_status)					? $row->template_status					: "";
			$this->template_price					= ($row->template_price)					? $row->template_price					: "0.00";
			
			$this->entered							= ($row->entered)							? $row->entered							: ($this->entered				? $this->entered					: "");
			$this->updated							= ($row->updated)							? $row->updated							: ($this->updated				? $this->updated					: "");
			$this->promotion_id						= ($row->promotion_id)					? $row->promotion_id						: 0;
			$this->backlink							= ($row->backlink)						? $row->backlink							: ($this->backlink				? $this->backlink					: "n");
			$this->clicktocall_number				= ($row->clicktocall_number)				? $row->clicktocall_number				: ($this->clicktocall_number	? $this->clicktocall_number			: "");
			$this->clicktocall_extension			= ($row->clicktocall_extension)			? $row->clicktocall_extension				: ($this->clicktocall_extension	? $this->clicktocall_extension		: 0);
            $this->bedroom			= ($row->bedroom)			? $row->bedroom		: "";
            $this->bedsize			= ($row->bedsize)			? $row->bedsize		: "";
            $this->sleeps			= ($row->sleeps)			? $row->sleeps		: "";
            $this->bathroom			= ($row->bathroom)		? $row->bathroom		: "";
            $this->property_type			= ($row->property_type)		? $row->property_type		: "";
            $this->view						= ($row->view)				? $row->view				: ($this->view				? $this->view				: "");
            $this->distance_beach			= ($row->distance_beach)		? $row->distance_beach	: ($this->distance_beach	? $this->distance_beach		: "");
            $this->development_name			= ($row->development_name)		? $row->development_name		: "";
            $this->airport_distance			= ($row->airport_distance)		? $row->airport_distance		: "";
            $this->airport_abbreviation			= ($row->airport_abbreviation)		? $row->airport_abbreviation		: "";

            $this->contact_fname			= ($row->contact_fname)		? $row->contact_fname		: "";
            $this->contact_lname			= ($row->contact_lname)		? $row->contact_lname		: "";

            $this->amenity_airhockey = ($row->amenity_airhockey)	? $row->amenity_airhockey	: "n";
			$this->amenity_alarmclock = ($row->amenity_alarmclock)	? $row->amenity_alarmclock	: "n";
			$this->amenity_answeringmachine = ($row->amenity_answeringmachine)	? $row->amenity_answeringmachine	: "n";
			$this->amenity_arcadegames = ($row->amenity_arcadegames)	? $row->amenity_arcadegames	: "n";
			$this->amenity_billiards = ($row->amenity_billiards)	? $row->amenity_billiards	: "n";
			$this->amenity_blender = ($row->amenity_blender)	? $row->amenity_blender	: "n";
			$this->amenity_blurayplayer = ($row->amenity_blurayplayer)	? $row->amenity_blurayplayer	: "n";
			$this->amenity_books = ($row->amenity_books)	? $row->amenity_books	: "n";
			$this->amenity_casetteplayer = ($row->amenity_casetteplayer)	? $row->amenity_casetteplayer	: "n";
			$this->amenity_cdplayer = ($row->amenity_cdplayer)	? $row->amenity_cdplayer	: "n";
			$this->amenity_ceilingfan = ($row->amenity_ceilingfan)	? $row->amenity_ceilingfan	: "n";
			$this->amenity_childshighchair = ($row->amenity_childshighchair)	? $row->amenity_childshighchair	: "n";
			$this->amenity_coffeemaker = ($row->amenity_coffeemaker)	? $row->amenity_coffeemaker	: "n";
			$this->amenity_communalpool = ($row->amenity_communalpool)	? $row->amenity_communalpool	: "n";
			$this->amenity_computer = ($row->amenity_computer)	? $row->amenity_computer	: "n";
			$this->amenity_cookware = ($row->amenity_cookware)	? $row->amenity_cookware	: "n";
			$this->amenity_cookingrange = ($row->amenity_cookingrange)	? $row->amenity_cookingrange	: "n";
			$this->amenity_deckfurniture = ($row->amenity_deckfurniture)	? $row->amenity_deckfurniture	: "n";
			$this->amenity_dishwasher = ($row->amenity_dishwasher)	? $row->amenity_dishwasher	: "n";
			$this->amenity_dishes = ($row->amenity_dishes)	? $row->amenity_dishes	: "n";
			$this->amenity_dvdplayer = ($row->amenity_dvdplayer)	? $row->amenity_dvdplayer	: "n";
			$this->amenity_exercisefacilities = ($row->amenity_exercisefacilities)	? $row->amenity_exercisefacilities	: "n";
			$this->amenity_foosball = ($row->amenity_foosball)	? $row->amenity_foosball	: "n";
			$this->amenity_gametable = ($row->amenity_gametable)	? $row->amenity_gametable	: "n";
			$this->amenity_games = ($row->amenity_games)	? $row->amenity_games	: "n";
			$this->amenity_grill = ($row->amenity_grill)	? $row->amenity_grill	: "n";
			$this->amenity_hairdryer = ($row->amenity_hairdryer)	? $row->amenity_hairdryer	: "n";
			$this->amenity_icemaker = ($row->amenity_icemaker)	? $row->amenity_icemaker	: "n";
			$this->amenity_internet = ($row->amenity_internet)	? $row->amenity_internet	: "n";
			$this->amenity_ironandboard = ($row->amenity_ironandboard)	? $row->amenity_ironandboard	: "n";
			$this->amenity_kidsgames = ($row->amenity_kidsgames)	? $row->amenity_kidsgames	: "n";
			$this->amenity_linensprovided = ($row->amenity_linensprovided)	? $row->amenity_linensprovided	: "n";
			$this->amenity_lobsterpot = ($row->amenity_lobsterpot)	? $row->amenity_lobsterpot	: "n";
			$this->amenity_microwave = ($row->amenity_microwave)	? $row->amenity_microwave	: "n";
			$this->amenity_minirefrigerator = ($row->amenity_minirefrigerator)	? $row->amenity_minirefrigerator	: "n";
			$this->amenity_mp3radiodock = ($row->amenity_mp3radiodock)	? $row->amenity_mp3radiodock	: "n";
			$this->amenity_outsideshower = ($row->amenity_outsideshower)	? $row->amenity_outsideshower	: "n";
			$this->amenity_oven = ($row->amenity_oven)	? $row->amenity_oven	: "n";
			$this->amenity_pinball = ($row->amenity_pinball)	? $row->amenity_pinball	: "n";
			$this->amenity_pingpong = ($row->amenity_pingpong)	? $row->amenity_pingpong	: "n";
			$this->amenity_privatepool = ($row->amenity_privatepool)	? $row->amenity_privatepool	: "n";
			$this->amenity_radio = ($row->amenity_radio)	? $row->amenity_radio	: "n";
			$this->amenity_refrigeratorfreezer = ($row->amenity_refrigeratorfreezer)	? $row->amenity_refrigeratorfreezer	: "n";
			$this->amenity_sofabed = ($row->amenity_sofabed)	? $row->amenity_sofabed	: "n";
			$this->amenity_stereo = ($row->amenity_stereo)	? $row->amenity_stereo	: "n";
			$this->amenity_telephone = ($row->amenity_telephone)	? $row->amenity_telephone	: "n";
			$this->amenity_television = ($row->amenity_television)	? $row->amenity_television	: "n";
			$this->amenity_toaster = ($row->amenity_toaster)	? $row->amenity_toaster	: "n";
			$this->amenity_toasteroven = ($row->amenity_toasteroven)	? $row->amenity_toasteroven	: "n";
			$this->amenity_towelsprovided = ($row->amenity_towelsprovided)	? $row->amenity_towelsprovided	: "n";
			$this->amenity_toys = ($row->amenity_toys)	? $row->amenity_toys	: "n";
			$this->amenity_utensils = ($row->amenity_utensils)	? $row->amenity_utensils	: "n";
			$this->amenity_vacuum = ($row->amenity_vacuum)	? $row->amenity_vacuum	: "n";
			$this->amenity_vcr = ($row->amenity_vcr)	? $row->amenity_vcr	: "n";
			$this->amenity_videogameconsole = ($row->amenity_videogameconsole)	? $row->amenity_videogameconsole	: "n";
			$this->amenity_videogames = ($row->amenity_videogames)	? $row->amenity_videogames	: "n";
			$this->amenity_washerdryer = ($row->amenity_washerdryer)	? $row->amenity_washerdryer	: "n";
			$this->amenity_wifi = ($row->amenity_wifi)	? $row->amenity_wifi	: "n";

			$this->feature_airconditioning = ($row->feature_airconditioning)	? $row->feature_airconditioning	: "n";
			$this->feature_balcony = ($row->feature_balcony)	? $row->feature_balcony	: "n";
			$this->feature_barbecuecharcoal = ($row->feature_barbecuecharcoal)	? $row->feature_barbecuecharcoal	: "n";
			$this->feature_barbecuegas = ($row->feature_barbecuegas)	? $row->feature_barbecuegas	: "n";
			$this->feature_boatslip = ($row->feature_boatslip)	? $row->feature_boatslip	: "n";
			$this->feature_cablesatellitetv = ($row->feature_cablesatellitetv)	? $row->feature_cablesatellitetv	: "n";
			$this->feature_clubhouse = ($row->feature_clubhouse)	? $row->feature_clubhouse	: "n";
			$this->feature_coveredparking = ($row->feature_coveredparking)	? $row->feature_coveredparking	: "n";
			$this->feature_deck = ($row->feature_deck)	? $row->feature_deck	: "n";
			$this->feature_diningroom = ($row->feature_diningroom)	? $row->feature_diningroom	: "n";
			$this->feature_elevator = ($row->feature_elevator)	? $row->feature_elevator	: "n";
			$this->feature_familyroom = ($row->feature_familyroom)	? $row->feature_familyroom	: "n";
			$this->feature_fullkitchen = ($row->feature_fullkitchen)	? $row->feature_fullkitchen	: "n";
			$this->feature_garage = ($row->feature_garage)	? $row->feature_garage	: "n";
			$this->feature_gasfireplace = ($row->feature_gasfireplace)	? $row->feature_gasfireplace	: "n";
			$this->feature_gatedcommunity = ($row->feature_gatedcommunity)	? $row->feature_gatedcommunity	: "n";
			$this->feature_wheelchairaccess = ($row->feature_wheelchairaccess)	? $row->feature_wheelchairaccess	: "n";
			$this->feature_heated = ($row->feature_heated)	? $row->feature_heated	: "n";
			$this->feature_heatedpool = ($row->feature_heatedpool)	? $row->feature_heatedpool	: "n";
			$this->feature_hottubjacuzzi = ($row->feature_hottubjacuzzi)	? $row->feature_hottubjacuzzi	: "n";
			$this->feature_internetaccess = ($row->feature_internetaccess)	? $row->feature_internetaccess	: "n";
			$this->feature_kitchenette = ($row->feature_kitchenette)	? $row->feature_kitchenette	: "n";
			$this->feature_livingroom = ($row->feature_livingroom)	? $row->feature_livingroom	: "n";
			$this->feature_loft = ($row->feature_loft)	? $row->feature_loft	: "n";
			$this->feature_onsitesecurity = ($row->feature_onsitesecurity)	? $row->feature_onsitesecurity	: "n";
			$this->feature_patio = ($row->feature_patio)	? $row->feature_patio	: "n";
			$this->feature_petsallowed = ($row->feature_petsallowed)		? $row->feature_petsallowed		: "n";
			$this->feature_playroom = ($row->feature_playroom)	? $row->feature_playroom	: "n";
			$this->feature_pool = ($row->feature_pool)	? $row->feature_pool	: "n";
			$this->feature_porch = ($row->feature_porch)	? $row->feature_porch	: "n";
			$this->feature_rooftopdeck = ($row->feature_rooftopdeck)	? $row->feature_rooftopdeck	: "n";
			$this->feature_sauna = ($row->feature_sauna)	? $row->feature_sauna	: "n";
			$this->feature_smokingpermitted = ($row->feature_smokingpermitted)	? $row->feature_smokingpermitted	: "n";
			$this->feature_woodfireplace = ($row->feature_woodfireplace)	? $row->feature_woodfireplace	: "n";
			
			$this->activity_antiquing = ($row->activity_antiquing)			? $row->activity_antiquing			: "n";
			$this->activity_basketballcourt = ($row->activity_basketballcourt)			? $row->activity_basketballcourt			: "n";
			$this->activity_beachcombing = ($row->activity_beachcombing)			? $row->activity_beachcombing			: "n";
			$this->activity_bicycling = ($row->activity_bicycling)			? $row->activity_bicycling			: "n";
			$this->activity_bikerentals = ($row->activity_bikerentals)			? $row->activity_bikerentals			: "n";
			$this->activity_birdwatching = ($row->activity_birdwatching)			? $row->activity_birdwatching			: "n";
			$this->activity_boatrentals = ($row->activity_boatrentals)			? $row->activity_boatrentals			: "n";
			$this->activity_boating = ($row->activity_boating)			? $row->activity_boating			: "n";
			$this->activity_botanicalgarden = ($row->activity_botanicalgarden)			? $row->activity_botanicalgarden			: "n";
			$this->activity_canoe = ($row->activity_canoe)			? $row->activity_canoe			: "n";
			$this->activity_churches = ($row->activity_churches)			? $row->activity_churches			: "n";
			$this->activity_cinemas = ($row->activity_cinemas)			? $row->activity_cinemas			: "n";
			$this->activity_bikesprovided = ($row->activity_bikesprovided)			? $row->activity_bikesprovided			: "n";
			$this->activity_deepseafishing = ($row->activity_deepseafishing)			? $row->activity_deepseafishing			: "n";
			$this->activity_fishing = ($row->activity_fishing)			? $row->activity_fishing			: "n";
			$this->activity_fitnesscenter = ($row->activity_fitnesscenter)			? $row->activity_fitnesscenter			: "n";
			$this->activity_golf = ($row->activity_golf)			? $row->activity_golf			: "n";
			$this->activity_healthbeautyspa = ($row->activity_healthbeautyspa)			? $row->activity_healthbeautyspa			: "n";
			$this->activity_hiking = ($row->activity_hiking)			? $row->activity_hiking			: "n";
			$this->activity_horsebackriding = ($row->activity_horsebackriding)			? $row->activity_horsebackriding			: "n";
			$this->activity_horseshoes = ($row->activity_horseshoes)			? $row->activity_horseshoes			: "n";
			$this->activity_hotairballooning = ($row->activity_hotairballooning)			? $row->activity_hotairballooning			: "n";
			$this->activity_iceskating = ($row->activity_iceskating)			? $row->activity_iceskating			: "n";
			$this->activity_jetskiing = ($row->activity_jetskiing)			? $row->activity_jetskiing			: "n";
			$this->activity_kayaking = ($row->activity_kayaking)			? $row->activity_kayaking			: "n";
			$this->activity_livetheater = ($row->activity_livetheater)			? $row->activity_livetheater			: "n";
			$this->activity_marina = ($row->activity_marina)			? $row->activity_marina			: "n";
			$this->activity_miniaturegolf = ($row->activity_miniaturegolf)			? $row->activity_miniaturegolf			: "n";
			$this->activity_mountainbiking = ($row->activity_mountainbiking)			? $row->activity_mountainbiking			: "n";
			$this->activity_museums = ($row->activity_museums)			? $row->activity_museums			: "n";
			$this->activity_paddleboating = ($row->activity_paddleboating)			? $row->activity_paddleboating			: "n";
			$this->activity_paragliding = ($row->activity_paragliding)			? $row->activity_paragliding			: "n";
			$this->activity_parasailing = ($row->activity_parasailing)			? $row->activity_parasailing			: "n";
			$this->activity_playground = ($row->activity_playground)			? $row->activity_playground			: "n";
			$this->activity_recreationcenter = ($row->activity_recreationcenter)			? $row->activity_recreationcenter			: "n";
			$this->activity_restaurants = ($row->activity_restaurants)			? $row->activity_restaurants			: "n";
			$this->activity_rollerblading = ($row->activity_rollerblading)			? $row->activity_rollerblading			: "n";
			$this->activity_sailing = ($row->activity_sailing)			? $row->activity_sailing			: "n";
			$this->activity_shelling = ($row->activity_shelling)			? $row->activity_shelling			: "n";
			$this->activity_shopping = ($row->activity_shopping)			? $row->activity_shopping			: "n";
			$this->activity_sightseeing = ($row->activity_sightseeing)			? $row->activity_sightseeing			: "n";
			$this->activity_skiing = ($row->activity_skiing)			? $row->activity_skiing			: "n";
			$this->activity_bayfishing = ($row->activity_bayfishing)			? $row->activity_bayfishing			: "n";
			$this->activity_spa = ($row->activity_spa)			? $row->activity_spa			: "n";
			$this->activity_surffishing = ($row->activity_surffishing)			? $row->activity_surffishing			: "n";
			$this->activity_surfing = ($row->activity_surfing)			? $row->activity_surfing			: "n";
			$this->activity_swimming = ($row->activity_swimming)			? $row->activity_swimming			: "n";
			$this->activity_tennis = ($row->activity_tennis)			? $row->activity_tennis			: "n";
			$this->activity_themeparks = ($row->activity_themeparks)			? $row->activity_themeparks			: "n";
			$this->activity_walking = ($row->activity_walking)			? $row->activity_walking			: "n";
			$this->activity_waterparks = ($row->activity_waterparks)			? $row->activity_waterparks			: "n";
			$this->activity_waterskiing = ($row->activity_waterskiing)			? $row->activity_waterskiing			: "n";
			$this->activity_watertubing = ($row->activity_watertubing)			? $row->activity_watertubing			: "n";
			$this->activity_wildlifeviewing = ($row->activity_wildlifeviewing)			? $row->activity_wildlifeviewing			: "n";
			$this->activity_zoo = ($row->activity_zoo)			? $row->activity_zoo			: "n";
			$this->required_stay = ($row->required_stay			? $row->required_stay			: "n");
			$this->is_featured = ($row->is_featured			? $row->is_featured			: "n");
			$this->featured_date = ($row->featured_date			? $row->featured_date			: "n");
			$this->search_pos = ($row->search_pos			? $row->search_pos			: 6);
			$this->search_pos_date = ($row->search_pos_date			? $row->search_pos_date			: "");
					
		}

		/**
		 * <code>
		 *		//Using this in forms or other pages.
		 *		$listingObj->Update();
		 * <br /><br />
		 *		//Using this in ListingSummary() class.
		 *		$this->Update();
		 * </code>
		 * @copyright Copyright 2005 Arca Solutions, Inc.
		 * @author Arca Solutions, Inc.
		 * @version 8.0.00
		 * @name Update
		 * @access Public
		 */
		function Update(){

			
			$this->prepareToSave();

			$sql = "UPDATE Listing_Summary SET "
					. " account_id = $this->account_id,"
					. " location_1 = $this->location_1,"
					. " location_1_title = $this->location_1_title,"
					. " location_1_abbreviation = $this->location_1_abbreviation,"
					. " location_1_friendly_url = $this->location_1_friendly_url,"
					. " location_2 = $this->location_2,"
					. " location_2_title = $this->location_2_title,"
					. " location_2_abbreviation = $this->location_2_abbreviation,"
					. " location_2_friendly_url = $this->location_2_friendly_url,"
					. " location_3 = $this->location_3,"
					. " location_3_title = $this->location_3_title,"
					. " location_3_abbreviation = $this->location_3_abbreviation,"
					. " location_3_friendly_url = $this->location_3_friendly_url,"
					. " location_4 = $this->location_4,"
					. " location_4_title = $this->location_4_title,"
					. " location_4_abbreviation = $this->location_4_abbreviation,"
					. " location_4_friendly_url = $this->location_4_friendly_url,"
					. " location_5 = $this->location_5,"
					. " location_5_title = $this->location_5_title,"
					. " location_5_abbreviation = $this->location_5_abbreviation,"
					. " location_5_friendly_url = $this->location_5_friendly_url,"
					. " title = $this->title,"
					. " friendly_url = $this->friendly_url,"
					. " email = $this->email,"
					. " show_email = $this->show_email,"
					. " url = $this->url,"
					. " display_url = $this->display_url,"
					. " address = $this->address,"
					. " address2 = $this->address2,"
					. " zip_code = $this->zip_code,"
					. " zip5 = $this->zip5,"
					. " latitude = $this->latitude,"
					. " longitude = $this->longitude,"
					. " phone = $this->phone,"
					. " fax = $this->fax,"
					. " description = $this->description,"
					. " attachment_file = $this->attachment_file,"
					. " attachment_caption = $this->attachment_caption,"
					. " status = $this->status,"
					. " renewal_date = $this->renewal_date,"
					. " level = $this->level,"
					. " random_number = $this->random_number,"
					. " claim_disable = $this->claim_disable,"
					. " locations = $this->locations,"
					. " keywords = $this->keywords,"
					. " seo_keywords = $this->seo_keywords,"
					. " fulltextsearch_keyword = $this->fulltextsearch_keyword,"
					. " fulltextsearch_where = $this->fulltextsearch_where,"
                    . " custom_text0       = $this->custom_text0,"
					. " custom_text1       = $this->custom_text1,"
					. " custom_text2       = $this->custom_text2,"
					. " custom_text3       = $this->custom_text3,"
					. " custom_text4       = $this->custom_text4,"
					. " custom_text5       = $this->custom_text5,"
					. " custom_text6       = $this->custom_text6,"
					. " custom_text7       = $this->custom_text7,"
					. " custom_text8       = $this->custom_text8,"
					. " custom_text9       = $this->custom_text9,"
					. " custom_short_desc0 = $this->custom_short_desc0,"
					. " custom_short_desc1 = $this->custom_short_desc1,"
					. " custom_short_desc2 = $this->custom_short_desc2,"
					. " custom_short_desc3 = $this->custom_short_desc3,"
					. " custom_short_desc4 = $this->custom_short_desc4,"
					. " custom_short_desc5 = $this->custom_short_desc5,"
					. " custom_short_desc6 = $this->custom_short_desc6,"
					. " custom_short_desc7 = $this->custom_short_desc7,"
					. " custom_short_desc8 = $this->custom_short_desc8,"
					. " custom_short_desc9 = $this->custom_short_desc9,"
					. " custom_long_desc0  = $this->custom_long_desc0,"
					. " custom_long_desc1  = $this->custom_long_desc1,"
					. " custom_long_desc2  = $this->custom_long_desc2,"
					. " custom_long_desc3  = $this->custom_long_desc3,"
					. " custom_long_desc4  = $this->custom_long_desc4,"
					. " custom_long_desc5  = $this->custom_long_desc5,"
					. " custom_long_desc6  = $this->custom_long_desc6,"
					. " custom_long_desc7  = $this->custom_long_desc7,"
					. " custom_long_desc8  = $this->custom_long_desc8,"
					. " custom_long_desc9  = $this->custom_long_desc9,"
					. " custom_checkbox0   = $this->custom_checkbox0,"
					. " custom_checkbox1   = $this->custom_checkbox1,"
					. " custom_checkbox2   = $this->custom_checkbox2,"
					. " custom_checkbox3   = $this->custom_checkbox3,"
					. " custom_checkbox4   = $this->custom_checkbox4,"
					. " custom_checkbox5   = $this->custom_checkbox5,"
					. " custom_checkbox6   = $this->custom_checkbox6,"
					. " custom_checkbox7   = $this->custom_checkbox7,"
					. " custom_checkbox8   = $this->custom_checkbox8,"
					. " custom_checkbox9   = $this->custom_checkbox9,"
					. " custom_dropdown0   = $this->custom_dropdown0,"
					. " custom_dropdown1   = $this->custom_dropdown1,"
					. " custom_dropdown2   = $this->custom_dropdown2,"
					. " custom_dropdown3   = $this->custom_dropdown3,"
					. " custom_dropdown4   = $this->custom_dropdown4,"
					. " custom_dropdown5   = $this->custom_dropdown5,"
					. " custom_dropdown6   = $this->custom_dropdown6,"
					. " custom_dropdown7   = $this->custom_dropdown7,"
					. " custom_dropdown8   = $this->custom_dropdown8,"
					. " custom_dropdown9   = $this->custom_dropdown9,"
					. " number_views		= $this->number_views,"
					. " avg_review			= $this->avg_review,"
					. " price               = $this->price,"
					. " promotion_start_date = $this->promotion_start_date,"
					. " promotion_end_date = $this->promotion_end_date,"
					. " thumb_id = $this->thumb_id,"
					. " thumb_type = $this->thumb_type,"
					. " thumb_width= $this->thumb_width,"
					. " thumb_height = $this->thumb_height,"
					. " listingtemplate_id = $this->listingtemplate_id,"
					. " template_layout_id = $this->template_layout_id,"
					. " template_cat_id = $this->template_cat_id,"
					. " template_title = $this->template_title,"
					. " template_status = $this->template_status,"
					. " template_price = $this->template_price,"
					. " image_id = $this->image_id,"
					. " updated = $this->updated,"
					. " entered = $this->entered,"
					. " promotion_id = $this->promotion_id,"
					. " backlink = $this->backlink,"
					. " clicktocall_number = $this->clicktocall_number,"
					. " bedroom 	= $this->bedroom,"
					. " bedsize 	= $this->bedsize,"
					. " sleeps 		= $this->sleeps,"
					. " bathroom 	= $this->bathroom,"
					. " property_type 	= $this->property_type,"
					. " `view` 		= $this->view,"
					. " distance_beach 		= $this->distance_beach,"
					. " development_name 		= $this->development_name,"
					. " airport_distance 		= $this->airport_distance,"
					. " airport_abbreviation 		= $this->airport_abbreviation,"
					. " contact_fname 		= $this->contact_fname,"
					. " contact_lname 		= $this->contact_lname,"
					. " amenity_airhockey = $this->amenity_airhockey,"
					. " amenity_alarmclock = $this->amenity_alarmclock,"
					. " amenity_answeringmachine = $this->amenity_answeringmachine,"
					. " amenity_arcadegames = $this->amenity_arcadegames,"
					. " amenity_billiards = $this->amenity_billiards,"
					. " amenity_blender = $this->amenity_blender,"
					. " amenity_blurayplayer = $this->amenity_blurayplayer,"
					. " amenity_books = $this->amenity_books,"
					. " amenity_casetteplayer = $this->amenity_casetteplayer,"
					. " amenity_cdplayer = $this->amenity_cdplayer,"
					. " amenity_ceilingfan = $this->amenity_ceilingfan,"
					. " amenity_childshighchair = $this->amenity_childshighchair,"
					. " amenity_coffeemaker = $this->amenity_coffeemaker,"
					. " amenity_communalpool = $this->amenity_communalpool,"
					. " amenity_computer = $this->amenity_computer,"
					. " amenity_cookware = $this->amenity_cookware,"
					. " amenity_cookingrange = $this->amenity_cookingrange,"
					. " amenity_deckfurniture = $this->amenity_deckfurniture,"
					. " amenity_dishwasher  = $this->amenity_dishwasher,"
					. " amenity_dishes = $this->amenity_dishes,"
					. " amenity_dvdplayer = $this->amenity_dvdplayer,"
					. " amenity_exercisefacilities = $this->amenity_exercisefacilities,"
					. " amenity_foosball = $this->amenity_foosball,"
					. " amenity_gametable = $this->amenity_gametable,"
					. " amenity_games = $this->amenity_games,"
					. " amenity_grill = $this->amenity_grill,"
					. " amenity_hairdryer = $this->amenity_hairdryer,"
					. " amenity_icemaker = $this->amenity_icemaker,"
					. " amenity_internet = $this->amenity_internet,"
					. " amenity_ironandboard = $this->amenity_ironandboard,"
					. " amenity_kidsgames = $this->amenity_kidsgames,"
					. " amenity_linensprovided = $this->amenity_linensprovided,"
					. " amenity_lobsterpot = $this->amenity_lobsterpot,"
					. " amenity_microwave = $this->amenity_microwave,"
					. " amenity_minirefrigerator = $this->amenity_minirefrigerator,"
					. " amenity_mp3radiodock = $this->amenity_mp3radiodock,"
					. " amenity_outsideshower = $this->amenity_outsideshower,"
					. " amenity_oven = $this->amenity_oven,"
					. " amenity_pinball = $this->amenity_pinball,"
					. " amenity_pingpong = $this->amenity_pingpong,"
					. " amenity_privatepool = $this->amenity_privatepool,"
					. " amenity_radio = $this->amenity_radio,"
					. " amenity_refrigeratorfreezer = $this->amenity_refrigeratorfreezer,"
					. " amenity_sofabed = $this->amenity_sofabed,"
					. " amenity_stereo = $this->amenity_stereo,"
					. " amenity_telephone = $this->amenity_telephone,"
					. " amenity_television = $this->amenity_television,"
					. " amenity_toaster = $this->amenity_toaster,"
					. " amenity_toasteroven = $this->amenity_toasteroven,"
					. " amenity_towelsprovided = $this->amenity_towelsprovided,"
					. " amenity_toys = $this->amenity_toys,"
					. " amenity_utensils = $this->amenity_utensils,"
					. " amenity_vacuum = $this->amenity_vacuum,"
					. " amenity_vcr = $this->amenity_vcr,"
					. " amenity_videogameconsole = $this->amenity_videogameconsole,"
					. " amenity_videogames = $this->amenity_videogames,"
					. " amenity_washerdryer = $this->amenity_washerdryer,"
					. " amenity_wifi = $this->amenity_wifi,"
					. " feature_airconditioning = $this->feature_airconditioning,"
					. " feature_balcony = $this->feature_balcony,"
					. " feature_barbecuecharcoal = $this->feature_barbecuecharcoal,"
					. " feature_barbecuegas = $this->feature_barbecuegas,"
					. " feature_boatslip = $this->feature_boatslip,"
					. " feature_cablesatellitetv = $this->feature_cablesatellitetv,"
					. " feature_clubhouse = $this->feature_clubhouse,"
					. " feature_coveredparking = $this->feature_coveredparking,"
					. " feature_deck = $this->feature_deck,"
					. " feature_diningroom = $this->feature_diningroom,"
					. " feature_elevator = $this->feature_elevator,"
					. " feature_familyroom = $this->feature_familyroom,"
					. " feature_fullkitchen = $this->feature_fullkitchen,"
					. " feature_garage = $this->feature_garage,"
					. " feature_gasfireplace = $this->feature_gasfireplace,"
					. " feature_gatedcommunity = $this->feature_gatedcommunity,"
					. " feature_wheelchairaccess = $this->feature_wheelchairaccess,"
					. " feature_heated = $this->feature_heated,"
					. " feature_heatedpool = $this->feature_heatedpool,"
					. " feature_hottubjacuzzi = $this->feature_hottubjacuzzi,"
					. " feature_internetaccess = $this->feature_internetaccess,"
					. " feature_kitchenette = $this->feature_kitchenette,"
					. " feature_livingroom = $this->feature_livingroom,"
					. " feature_loft = $this->feature_loft,"
					. " feature_onsitesecurity = $this->feature_onsitesecurity,"
					. " feature_patio = $this->feature_patio,"
					. " feature_petsallowed = $this->feature_petsallowed,"
					. " feature_playroom = $this->feature_playroom,"
					. " feature_pool = $this->feature_pool,"
					. " feature_porch = $this->feature_porch,"
					. " feature_rooftopdeck = $this->feature_rooftopdeck,"
					. " feature_sauna = $this->feature_sauna,"
					. " feature_smokingpermitted = $this->feature_smokingpermitted,"
					. " feature_woodfireplace = $this->feature_woodfireplace,"
					. " activity_antiquing = $this->activity_antiquing,"
					. " activity_basketballcourt = $this->activity_basketballcourt,"
					. " activity_beachcombing = $this->activity_beachcombing,"
					. " activity_bicycling = $this->activity_bicycling,"
					. " activity_bikerentals = $this->activity_bikerentals,"
					. " activity_birdwatching = $this->activity_birdwatching,"
					. " activity_boatrentals = $this->activity_boatrentals,"
					. " activity_boating = $this->activity_boating,"
					. " activity_botanicalgarden = $this->activity_botanicalgarden,"
					. " activity_canoe = $this->activity_canoe,"
					. " activity_churches = $this->activity_churches,"
					. " activity_cinemas = $this->activity_cinemas,"
					. " activity_bikesprovided = $this->activity_bikesprovided,"
					. " activity_deepseafishing = $this->activity_deepseafishing,"
					. " activity_fishing = $this->activity_fishing,"
					. " activity_fitnesscenter = $this->activity_fitnesscenter,"
					. " activity_golf = $this->activity_golf,"
					. " activity_healthbeautyspa = $this->activity_healthbeautyspa,"
					. " activity_hiking = $this->activity_hiking,"
					. " activity_horsebackriding = $this->activity_horsebackriding,"
					. " activity_horseshoes = $this->activity_horseshoes,"
					. " activity_hotairballooning = $this->activity_hotairballooning,"
					. " activity_iceskating = $this->activity_iceskating,"
					. " activity_jetskiing = $this->activity_jetskiing,"
					. " activity_kayaking = $this->activity_kayaking,"
					. " activity_livetheater = $this->activity_livetheater,"
					. " activity_marina = $this->activity_marina,"
					. " activity_miniaturegolf = $this->activity_miniaturegolf,"
					. " activity_mountainbiking = $this->activity_mountainbiking,"
					. " activity_museums = $this->activity_museums,"
					. " activity_paddleboating = $this->activity_paddleboating,"
					. " activity_paragliding = $this->activity_paragliding,"
					. " activity_parasailing = $this->activity_parasailing,"
					. " activity_playground = $this->activity_playground,"
					. " activity_recreationcenter = $this->activity_recreationcenter,"
					. " activity_restaurants = $this->activity_restaurants,"
					. " activity_rollerblading = $this->activity_rollerblading,"
					. " activity_sailing = $this->activity_sailing,"
					. " activity_shelling = $this->activity_shelling,"
					. " activity_shopping = $this->activity_shopping,"
					. " activity_sightseeing = $this->activity_sightseeing,"
					. " activity_skiing = $this->activity_skiing,"
					. " activity_bayfishing = $this->activity_bayfishing,"
					. " activity_spa = $this->activity_spa,"
					. " activity_surffishing = $this->activity_surffishing,"
					. " activity_surfing = $this->activity_surfing,"
					. " activity_swimming = $this->activity_swimming,"
					. " activity_tennis = $this->activity_tennis,"
					. " activity_themeparks = $this->activity_themeparks,"
					. " activity_walking = $this->activity_walking,"
					. " activity_waterparks = $this->activity_waterparks,"
					. " activity_waterskiing = $this->activity_waterskiing,"
					. " activity_watertubing = $this->activity_watertubing,"
					. " activity_wildlifeviewing = $this->activity_wildlifeviewing,"
					. " activity_zoo = $this->activity_zoo,"
					. " required_stay = $this->required_stay,"
					. " is_featured = $this->is_featured,"
					. " featured_date = $this->featured_date,"
					. " search_pos = $this->search_pos,"
					. " search_pos_date = $this->search_pos_date"
					. " WHERE id = ".$this->id;


			Self::updateSql($sql);
			
		}

		/**
		 * <code>
		 *		//Using this in forms or other pages.
		 *		$listingObj->Add();
		 * <br /><br />
		 *		//Using this in ListingSummary() class.
		 *		$this->Add();
		 * </code>
		 * @copyright Copyright 2005 Arca Solutions, Inc.
		 * @author Arca Solutions, Inc.
		 * @version 8.0.00
		 * @name Add
		 * @access Public
		 */
		function Add(){
			
			
			$this->prepareToSave();


			$sql = "INSERT Listing_Summary ("
					. " id,"
					. " account_id,"
					. " location_1,"
					. " location_1_title,"
					. " location_1_abbreviation,"
					. " location_1_friendly_url,"
					. " location_2,"
					. " location_2_title,"
					. " location_2_abbreviation,"
					. " location_2_friendly_url,"
					. " location_3,"
					. " location_3_title,"
					. " location_3_abbreviation,"
					. " location_3_friendly_url,"
					. " location_4,"
					. " location_4_title,"
					. " location_4_abbreviation,"
					. " location_4_friendly_url,"
					. " location_5,"
					. " location_5_title,"
					. " location_5_abbreviation,"
					. " location_5_friendly_url,"
					. " title,"
					. " friendly_url,"
					. " email,"
					. " show_email,"
					. " url,"
					. " display_url,"
					. " address,"
					. " address2,"
					. " zip_code,"
					. " zip5,"
					. " latitude,"
					. " longitude,"
					. " phone,"
					. " fax,"
					. " description,"
					. " attachment_file,"
					. " attachment_caption,"
					. " status,"
					. " renewal_date,"
					. " level,"
					. " random_number,"
					. " claim_disable,"
					. " locations,"
					. " keywords,"
					. " seo_keywords,"
					. " fulltextsearch_keyword,"
					. " fulltextsearch_where,"
                    . " custom_text0,"
					. " custom_text1,"
					. " custom_text2,"
					. " custom_text3,"
					. " custom_text4,"
					. " custom_text5,"
					. " custom_text6,"
					. " custom_text7,"
					. " custom_text8,"
					. " custom_text9,"
					. " custom_short_desc0,"
					. " custom_short_desc1,"
					. " custom_short_desc2,"
					. " custom_short_desc3,"
					. " custom_short_desc4,"
					. " custom_short_desc5,"
					. " custom_short_desc6,"
					. " custom_short_desc7,"
					. " custom_short_desc8,"
					. " custom_short_desc9,"
					. " custom_long_desc0,"
					. " custom_long_desc1,"
					. " custom_long_desc2,"
					. " custom_long_desc3,"
					. " custom_long_desc4,"
					. " custom_long_desc5,"
					. " custom_long_desc6,"
					. " custom_long_desc7,"
					. " custom_long_desc8,"
					. " custom_long_desc9,"
					. " custom_checkbox0,"
					. " custom_checkbox1,"
					. " custom_checkbox2,"
					. " custom_checkbox3,"
					. " custom_checkbox4,"
					. " custom_checkbox5,"
					. " custom_checkbox6,"
					. " custom_checkbox7,"
					. " custom_checkbox8,"
					. " custom_checkbox9,"
					. " custom_dropdown0,"
					. " custom_dropdown1,"
					. " custom_dropdown2,"
					. " custom_dropdown3,"
					. " custom_dropdown4,"
					. " custom_dropdown5,"
					. " custom_dropdown6,"
					. " custom_dropdown7,"
					. " custom_dropdown8,"
					. " custom_dropdown9,"
					. " number_views,"
					. " avg_review,"
					. " price,"
					. " promotion_start_date,"
					. " promotion_end_date,"
					. " thumb_id,"
					. " thumb_type,"
					. " thumb_width,"
					. " thumb_height,"
					. " listingtemplate_id,"
					. " template_layout_id,"
					. " template_cat_id,"
					. " template_title,"
					. " template_status,"
					. " template_price,"
					. " image_id,"
					. " updated,"
					. " entered,"
					. " promotion_id,"
					. " backlink,"
					. " clicktocall_number,"
					. " clicktocall_extension,"
					. " bedroom,"
					. " bedsize,"
					. " sleeps,"
					. " bathroom,"
					. " property_type,"
					. " `view`,"
					. " distance_beach,"
					. " development_name,"
					. " airport_distance,"
					. " airport_abbreviation,"
					. " contact_fname,"
					. " contact_lname,"
					. " amenity_airhockey,"
					. " amenity_alarmclock,"
					. " amenity_answeringmachine,"
					. " amenity_arcadegames,"
					. " amenity_billiards,"
					. " amenity_blender,"
					. " amenity_blurayplayer,"
					. " amenity_books,"
					. " amenity_casetteplayer,"
					. " amenity_cdplayer,"
					. " amenity_ceilingfan,"
					. " amenity_childshighchair,"
					. " amenity_coffeemaker,"
					. " amenity_communalpool,"
					. " amenity_computer,"
					. " amenity_cookware,"
					. " amenity_cookingrange,"
					. " amenity_deckfurniture,"
					. " amenity_dishwasher ,"
					. " amenity_dishes,"
					. " amenity_dvdplayer,"
					. " amenity_exercisefacilities,"
					. " amenity_foosball,"
					. " amenity_gametable,"
					. " amenity_games,"
					. " amenity_grill,"
					. " amenity_hairdryer,"
					. " amenity_icemaker,"
					. " amenity_internet,"
					. " amenity_ironandboard,"
					. " amenity_kidsgames,"
					. " amenity_linensprovided,"
					. " amenity_lobsterpot,"
					. " amenity_microwave,"
					. " amenity_minirefrigerator,"
					. " amenity_mp3radiodock,"
					. " amenity_outsideshower,"
					. " amenity_oven,"
					. " amenity_pinball,"
					. " amenity_pingpong,"
					. " amenity_privatepool,"
					. " amenity_radio,"
					. " amenity_refrigeratorfreezer,"
					. " amenity_sofabed,"
					. " amenity_stereo,"
					. " amenity_telephone,"
					. " amenity_television,"
					. " amenity_toaster,"
					. " amenity_toasteroven,"
					. " amenity_towelsprovided,"
					. " amenity_toys,"
					. " amenity_utensils,"
					. " amenity_vacuum,"
					. " amenity_vcr,"
					. " amenity_videogameconsole,"
					. " amenity_videogames,"
					. " amenity_washerdryer,"
					. " amenity_wifi,"
					. " feature_airconditioning,"
					. " feature_balcony,"
					. " feature_barbecuecharcoal,"
					. " feature_barbecuegas,"
					. " feature_boatslip,"
					. " feature_cablesatellitetv,"
					. " feature_clubhouse,"
					. " feature_coveredparking,"
					. " feature_deck,"
					. " feature_diningroom,"
					. " feature_elevator,"
					. " feature_familyroom,"
					. " feature_fullkitchen,"
					. " feature_garage,"
					. " feature_gasfireplace,"
					. " feature_gatedcommunity,"
					. " feature_wheelchairaccess,"
					. " feature_heated,"
					. " feature_heatedpool,"
					. " feature_hottubjacuzzi,"
					. " feature_internetaccess,"
					. " feature_kitchenette,"
					. " feature_livingroom,"
					. " feature_loft,"
					. " feature_onsitesecurity,"
					. " feature_patio,"
					. " feature_petsallowed,"
					. " feature_playroom,"
					. " feature_pool,"
					. " feature_porch,"
					. " feature_rooftopdeck,"
					. " feature_sauna,"
					. " feature_smokingpermitted,"
					. " feature_woodfireplace,"
					. " activity_antiquing,"
					. " activity_basketballcourt,"
					. " activity_beachcombing,"
					. " activity_bicycling,"
					. " activity_bikerentals,"
					. " activity_birdwatching,"
					. " activity_boatrentals,"
					. " activity_boating,"
					. " activity_botanicalgarden,"
					. " activity_canoe,"
					. " activity_churches,"
					. " activity_cinemas,"
					. " activity_bikesprovided,"
					. " activity_deepseafishing,"
					. " activity_fishing,"
					. " activity_fitnesscenter,"
					. " activity_golf,"
					. " activity_healthbeautyspa,"
					. " activity_hiking,"
					. " activity_horsebackriding,"
					. " activity_horseshoes,"
					. " activity_hotairballooning,"
					. " activity_iceskating,"
					. " activity_jetskiing,"
					. " activity_kayaking,"
					. " activity_livetheater,"
					. " activity_marina,"
					. " activity_miniaturegolf,"
					. " activity_mountainbiking,"
					. " activity_museums,"
					. " activity_paddleboating,"
					. " activity_paragliding,"
					. " activity_parasailing,"
					. " activity_playground,"
					. " activity_recreationcenter,"
					. " activity_restaurants,"
					. " activity_rollerblading,"
					. " activity_sailing,"
					. " activity_shelling,"
					. " activity_shopping,"
					. " activity_sightseeing,"
					. " activity_skiing,"
					. " activity_bayfishing,"
					. " activity_spa,"
					. " activity_surffishing,"
					. " activity_surfing,"
					. " activity_swimming,"
					. " activity_tennis,"
					. " activity_themeparks,"
					. " activity_walking,"
					. " activity_waterparks,"
					. " activity_waterskiing,"
					. " activity_watertubing,"
					. " activity_wildlifeviewing,"
					. " activity_zoo,"
					. " is_featured,"
					. " featured_date,"
					. " search_pos,"
					. " search_pos_date,"
					. " required_stay)"
					. " VALUES"
					. " ($this->id,"
					. " $this->account_id,"
					. " $this->location_1,"
					. " $this->location_1_title,"
					. " $this->location_1_abbreviation,"
					. " $this->location_1_friendly_url,"
					. " $this->location_2,"
					. " $this->location_2_title,"
					. " $this->location_2_abbreviation,"
					. " $this->location_2_friendly_url,"
					. " $this->location_3,"
					. " $this->location_3_title,"
					. " $this->location_3_abbreviation,"
					. " $this->location_3_friendly_url,"
					. " $this->location_4,"
					. " $this->location_4_title,"
					. " $this->location_4_abbreviation,"
					. " $this->location_4_friendly_url,"
					. " $this->location_5,"
					. " $this->location_5_title,"
					. " $this->location_5_abbreviation,"
					. " $this->location_5_friendly_url,"
					. " $this->title,"
					. " $this->friendly_url,"
					. " $this->email,"
					. " $this->show_email,"
					. " $this->url,"
					. " $this->display_url,"
					. " $this->address,"
					. " $this->address2,"
					. " $this->zip_code,"
					. " $this->zip5,"
					. " $this->latitude,"
					. " $this->longitude,"
					. " $this->phone,"
					. " $this->fax,"
					. " $this->description,"
					. " $this->attachment_file,"
					. " $this->attachment_caption,"
					. " $this->status,"
					. " $this->renewal_date,"
					. " $this->level,"
					. " $this->random_number,"
					. " $this->claim_disable,"
					. " $this->locations,"
					. " $this->keywords,"
					. " $this->seo_keywords,"
					. " $this->fulltextsearch_keyword,"
					. " $this->fulltextsearch_where,"
                    . " $this->custom_text0,"
					. " $this->custom_text1,"
					. " $this->custom_text2,"
					. " $this->custom_text3,"
					. " $this->custom_text4,"
					. " $this->custom_text5,"
					. " $this->custom_text6,"
					. " $this->custom_text7,"
					. " $this->custom_text8,"
					. " $this->custom_text9,"
					. " $this->custom_short_desc0,"
					. " $this->custom_short_desc1,"
					. " $this->custom_short_desc2,"
					. " $this->custom_short_desc3,"
					. " $this->custom_short_desc4,"
					. " $this->custom_short_desc5,"
					. " $this->custom_short_desc6,"
					. " $this->custom_short_desc7,"
					. " $this->custom_short_desc8,"
					. " $this->custom_short_desc9,"
					. " $this->custom_long_desc0,"
					. " $this->custom_long_desc1,"
					. " $this->custom_long_desc2,"
					. " $this->custom_long_desc3,"
					. " $this->custom_long_desc4,"
					. " $this->custom_long_desc5,"
					. " $this->custom_long_desc6,"
					. " $this->custom_long_desc7,"
					. " $this->custom_long_desc8,"
					. " $this->custom_long_desc9,"
					. " $this->custom_checkbox0,"
					. " $this->custom_checkbox1,"
					. " $this->custom_checkbox2,"
					. " $this->custom_checkbox3,"
					. " $this->custom_checkbox4,"
					. " $this->custom_checkbox5,"
					. " $this->custom_checkbox6,"
					. " $this->custom_checkbox7,"
					. " $this->custom_checkbox8,"
					. " $this->custom_checkbox9,"
					. " $this->custom_dropdown0,"
					. " $this->custom_dropdown1,"
					. " $this->custom_dropdown2,"
					. " $this->custom_dropdown3,"
					. " $this->custom_dropdown4,"
					. " $this->custom_dropdown5,"
					. " $this->custom_dropdown6,"
					. " $this->custom_dropdown7,"
					. " $this->custom_dropdown8,"
					. " $this->custom_dropdown9,"
					. " $this->number_views,"
					. " $this->avg_review,"
					. " $this->price,"
					. " $this->promotion_start_date,"
					. " $this->promotion_end_date,"
					. " $this->thumb_id,"
					. " $this->thumb_type,"
					. " $this->thumb_width,"
					. " $this->thumb_height,"
					. " $this->listingtemplate_id,"
					. " $this->template_layout_id,"
					. " $this->template_cat_id,"
					. " $this->template_title,"
					. " $this->template_status,"
					. " $this->template_price,"
					. " $this->image_id,"
					. " $this->updated,"
					. " $this->entered,"
					. " $this->promotion_id,"
					. " $this->backlink,"
					. " $this->clicktocall_number,"
					. " $this->clicktocall_extension,"
					. " $this->bedroom,"
					. " $this->bedsize,"
					. " $this->sleeps,"
					. " $this->bathroom,"
					. " $this->property_type,"
					. " $this->view,"
					. " $this->distance_beach,"
					. " $this->development_name,"
					. " $this->airport_distance,"
					. " $this->airport_abbreviation,"
					. " $this->contact_fname,"
					. " $this->contact_lname,"
					. " $this->amenity_airhockey,"
					. " $this->amenity_alarmclock,"
					. " $this->amenity_answeringmachine,"
					. " $this->amenity_arcadegames,"
					. " $this->amenity_billiards,"
					. " $this->amenity_blender,"
					. " $this->amenity_blurayplayer,"
					. " $this->amenity_books,"
					. " $this->amenity_casetteplayer,"
					. " $this->amenity_cdplayer,"
					. " $this->amenity_ceilingfan,"
					. " $this->amenity_childshighchair,"
					. " $this->amenity_coffeemaker,"
					. " $this->amenity_communalpool,"
					. " $this->amenity_computer,"
					. " $this->amenity_cookware,"
					. " $this->amenity_cookingrange,"
					. " $this->amenity_deckfurniture,"
					. " $this->amenity_dishwasher,"
					. " $this->amenity_dishes,"
					. " $this->amenity_dvdplayer,"
					. " $this->amenity_exercisefacilities,"
					. " $this->amenity_foosball,"
					. " $this->amenity_gametable,"
					. " $this->amenity_games,"
					. " $this->amenity_grill,"
					. " $this->amenity_hairdryer,"
					. " $this->amenity_icemaker,"
					. " $this->amenity_internet,"
					. " $this->amenity_ironandboard,"
					. " $this->amenity_kidsgames,"
					. " $this->amenity_linensprovided,"
					. " $this->amenity_lobsterpot,"
					. " $this->amenity_microwave,"
					. " $this->amenity_minirefrigerator,"
					. " $this->amenity_mp3radiodock,"
					. " $this->amenity_outsideshower,"
					. " $this->amenity_oven,"
					. " $this->amenity_pinball,"
					. " $this->amenity_pingpong,"
					. " $this->amenity_privatepool,"
					. " $this->amenity_radio,"
					. " $this->amenity_refrigeratorfreezer,"
					. " $this->amenity_sofabed,"
					. " $this->amenity_stereo,"
					. " $this->amenity_telephone,"
					. " $this->amenity_television,"
					. " $this->amenity_toaster,"
					. " $this->amenity_toasteroven,"
					. " $this->amenity_towelsprovided,"
					. " $this->amenity_toys,"
					. " $this->amenity_utensils,"
					. " $this->amenity_vacuum,"
					. " $this->amenity_vcr,"
					. " $this->amenity_videogameconsole,"
					. " $this->amenity_videogames,"
					. " $this->amenity_washerdryer,"
					. " $this->amenity_wifi,"
					. " $this->feature_airconditioning,"
					. " $this->feature_balcony,"
					. " $this->feature_barbecuecharcoal,"
					. " $this->feature_barbecuegas,"
					. " $this->feature_boatslip,"
					. " $this->feature_cablesatellitetv,"
					. " $this->feature_clubhouse,"
					. " $this->feature_coveredparking,"
					. " $this->feature_deck,"
					. " $this->feature_diningroom,"
					. " $this->feature_elevator,"
					. " $this->feature_familyroom,"
					. " $this->feature_fullkitchen,"
					. " $this->feature_garage,"
					. " $this->feature_gasfireplace,"
					. " $this->feature_gatedcommunity,"
					. " $this->feature_wheelchairaccess,"
					. " $this->feature_heated,"
					. " $this->feature_heatedpool,"
					. " $this->feature_hottubjacuzzi,"
					. " $this->feature_internetaccess,"
					. " $this->feature_kitchenette,"
					. " $this->feature_livingroom,"
					. " $this->feature_loft,"
					. " $this->feature_onsitesecurity,"
					. " $this->feature_patio,"
					. " $this->feature_petsallowed,"
					. " $this->feature_playroom,"
					. " $this->feature_pool,"
					. " $this->feature_porch,"
					. " $this->feature_rooftopdeck,"
					. " $this->feature_sauna,"
					. " $this->feature_smokingpermitted,"
					. " $this->feature_woodfireplace,"
					. " $this->activity_antiquing,"
					. " $this->activity_basketballcourt,"
					. " $this->activity_beachcombing,"
					. " $this->activity_bicycling,"
					. " $this->activity_bikerentals,"
					. " $this->activity_birdwatching,"
					. " $this->activity_boatrentals,"
					. " $this->activity_boating,"
					. " $this->activity_botanicalgarden,"
					. " $this->activity_canoe,"
					. " $this->activity_churches,"
					. " $this->activity_cinemas,"
					. " $this->activity_bikesprovided,"
					. " $this->activity_deepseafishing,"
					. " $this->activity_fishing,"
					. " $this->activity_fitnesscenter,"
					. " $this->activity_golf,"
					. " $this->activity_healthbeautyspa,"
					. " $this->activity_hiking,"
					. " $this->activity_horsebackriding,"
					. " $this->activity_horseshoes,"
					. " $this->activity_hotairballooning,"
					. " $this->activity_iceskating,"
					. " $this->activity_jetskiing,"
					. " $this->activity_kayaking,"
					. " $this->activity_livetheater,"
					. " $this->activity_marina,"
					. " $this->activity_miniaturegolf,"
					. " $this->activity_mountainbiking,"
					. " $this->activity_museums,"
					. " $this->activity_paddleboating,"
					. " $this->activity_paragliding,"
					. " $this->activity_parasailing,"
					. " $this->activity_playground,"
					. " $this->activity_recreationcenter,"
					. " $this->activity_restaurants,"
					. " $this->activity_rollerblading,"
					. " $this->activity_sailing,"
					. " $this->activity_shelling,"
					. " $this->activity_shopping,"
					. " $this->activity_sightseeing,"
					. " $this->activity_skiing,"
					. " $this->activity_bayfishing,"
					. " $this->activity_spa,"
					. " $this->activity_surffishing,"
					. " $this->activity_surfing,"
					. " $this->activity_swimming,"
					. " $this->activity_tennis,"
					. " $this->activity_themeparks,"
					. " $this->activity_walking,"
					. " $this->activity_waterparks,"
					. " $this->activity_waterskiing,"
					. " $this->activity_watertubing,"
					. " $this->activity_wildlifeviewing,"
					. " $this->activity_zoo,"
					. " $this->is_featured,"
					. " $this->featured_date,"
					. " $this->search_pos,"
					. " $this->search_pos_date,"
					. " $this->required_stay)";

			Self::insertSql($sql);


		}

		/**
		 * <code>
		 *		//Using this in forms or other pages.
		 *		$listingObj->Delete($id);
		 * <br /><br />
		 *		//Using this in ListingSummary() class.
		 *		$this->Delete($id);
		 * </code>
		 * @copyright Copyright 2005 Arca Solutions, Inc.
		 * @author Arca Solutions, Inc.
		 * @version 8.0.00
		 * @name Delete
		 * @access Public
		 * @param integer $id
		 * @param integer $domain_id
		 */
		function Delete($id, $domain_id = false){
			$dbMain = db_getDBObject(DEFAULT_DB, true);
			if ($domain_id) {
				$db = db_getDBObjectByDomainID($domain_id, $dbMain);
			} else {
				if (defined("SELECTED_DOMAIN_ID")) {
					$db = db_getDBObjectByDomainID(SELECTED_DOMAIN_ID, $dbMain);
				} else {
					$db = db_getDBObject();
				}
				unset($dbMain);
			}
			$sql = "DELETE from Listing_Summary where id = ".$id;
			$db->query($sql);
		}

		/**
		 * <code>
		 *		//Using this in forms or other pages.
		 *		$listingObj->PopulateTableLocation(...);
		 * <br /><br />
		 *		//Using this in ListingSummary() class.
		 *		$this->PopulateTableLocation(...);
		 * </code>
		 * @copyright Copyright 2005 Arca Solutions, Inc.
		 * @author Arca Solutions, Inc.
		 * @version 8.0.00
		 * @name PopulateTableLocation
		 * @access Public
		 * @param integer $listing_id
		 * @param integer $locLevel
		 * @param varchar $field
		 */
		function PopulateTableLocation($listing_id = false, $locLevel = 0, $field){

			
			$sql = "UPDATE Listing_Summary SET "
					. " location_".$locLevel." = 0,"
					. " location_".$locLevel."_title = '',"
					. " location_".$locLevel."_abbreviation = '',"
					. " location_".$locLevel."_friendly_url = '',"
					. " fulltextsearch_where = '".$field."'"
					. " WHERE id = ".$listing_id;

			Self::updateSql($sql);
		}
		
		/**
		 * <code>
		 *		//Using this in forms or other pages.
		 *		$listingObj->PopulateTableCategory(...);
		 * <br /><br />
		 *		//Using this in ListingSummary() class.
		 *		$this->PopulateTableCategory(...);
		 * </code>
		 * @copyright Copyright 2005 Arca Solutions, Inc.
		 * @author Arca Solutions, Inc.
		 * @version 8.0.00
		 * @name PopulateTableCategory
		 * @access Public
		 * @param integer $listing_id
		 * @param varchar $field
		 */
		function PopulateTableCategory($listing_id = false, $field){

			$dbMain = db_getDBObject(DEFAULT_DB, true);
			if (defined("SELECTED_DOMAIN_ID")) {
				$db = db_getDBObjectByDomainID(SELECTED_DOMAIN_ID, $dbMain);
			} else {
				$db = db_getDBObject();
			}

			unset($dbMain);

			$sql = "UPDATE Listing_Summary SET "
					. " fulltextsearch_keyword = '".$field."'"
					. " WHERE id = ".$listing_id;

			$db->query($sql);
		}

		/**
		 * <code>
		 *		//Using this in forms or other pages.
		 *		$listingObj->PopulateTable(...);
		 * <br /><br />
		 *		//Using this in ListingSummary() class.
		 *		$this->PopulateTable(...);
		 * </code>
		 * @copyright Copyright 2005 Arca Solutions, Inc.
		 * @author Arca Solutions, Inc.
		 * @version 8.0.00
		 * @name PopulateTable
		 * @access Public
		 * @param integer $listing_id
		 * @param varchar $method
		 */
		function PopulateTable($listing_id = false, $method){

			
			
			$sql = "select
						Listing.id,
						Listing.account_id,";

			$sql .=	"	Listing.location_1,
						Listing.location_2,
						Listing.location_3,
						Listing.location_4,
						Listing.location_5,
						Listing.image_id as image_id,
						Listing.thumb_id as thumb_id,
						Listing.title,
						Listing.friendly_url,
						Listing.email,
						Listing.show_email,
						Listing.url,
						Listing.display_url,
						Listing.address,
						Listing.address2,
						Listing.zip_code,
						Listing.zip5,
						Listing.latitude,
						Listing.longitude,
						Listing.phone,
						Listing.fax,
						Listing.description,
						Listing.attachment_file,
						Listing.attachment_caption,
						Listing.status,
						Listing.renewal_date,
						Listing.level,
						Listing.random_number,
						Listing.reminder,
						Listing.fulltextsearch_keyword,
						Listing.fulltextsearch_where,
						Listing.locations,
						Listing.keywords,
						Listing.seo_keywords,
						Listing.claim_disable,
						Listing.listingtemplate_id,
                        Listing.custom_checkbox0,
						Listing.custom_checkbox1,
						Listing.custom_checkbox2,
						Listing.custom_checkbox3,
						Listing.custom_checkbox4,
						Listing.custom_checkbox5,
						Listing.custom_checkbox6,
						Listing.custom_checkbox7,
						Listing.custom_checkbox8,
						Listing.custom_checkbox9,
						Listing.custom_dropdown0,
						Listing.custom_dropdown1,
						Listing.custom_dropdown2,
						Listing.custom_dropdown3,
						Listing.custom_dropdown4,
						Listing.custom_dropdown5,
						Listing.custom_dropdown6,
						Listing.custom_dropdown7,
						Listing.custom_dropdown8,
						Listing.custom_dropdown9,
						Listing.custom_text0,
						Listing.custom_text1,
						Listing.custom_text2,
						Listing.custom_text3,
						Listing.custom_text4,
						Listing.custom_text5,
						Listing.custom_text6,
						Listing.custom_text7,
						Listing.custom_text8,
						Listing.custom_text9,
						Listing.custom_short_desc0,
						Listing.custom_short_desc1,
						Listing.custom_short_desc2,
						Listing.custom_short_desc3,
						Listing.custom_short_desc4,
						Listing.custom_short_desc5,
						Listing.custom_short_desc6,
						Listing.custom_short_desc7,
						Listing.custom_short_desc8,
						Listing.custom_short_desc9,
						Listing.custom_long_desc0,
						Listing.custom_long_desc1,
						Listing.custom_long_desc2,
						Listing.custom_long_desc3,
						Listing.custom_long_desc4,
						Listing.custom_long_desc5,
						Listing.custom_long_desc6,
						Listing.custom_long_desc7,
						Listing.custom_long_desc8,
						Listing.custom_long_desc9,
						Listing.number_views,
						Listing.avg_review,
						Listing.price,
						Listing.updated,
						Listing.entered,
						Listing.promotion_id,
						Listing.backlink,
						Listing.clicktocall_number,
						Listing.clicktocall_extension,
						Listing.bedroom,
						Listing.bedsize,
						Listing.sleeps,
						Listing.bathroom,
						Listing.property_type,
						Listing.view,
						Listing.distance_beach,
						Listing.development_name,
						Listing.airport_distance,
						Listing.airport_abbreviation,
						Listing.contact_fname,
						Listing.contact_lname,
						Listing.amenity_airhockey,
						Listing.amenity_alarmclock,
						Listing.amenity_answeringmachine,
						Listing.amenity_arcadegames,
						Listing.amenity_billiards,
						Listing.amenity_blender,
						Listing.amenity_blurayplayer,
						Listing.amenity_books,
						Listing.amenity_casetteplayer,
						Listing.amenity_cdplayer,
						Listing.amenity_ceilingfan,
						Listing.amenity_childshighchair,
						Listing.amenity_coffeemaker,
						Listing.amenity_communalpool,
						Listing.amenity_computer,
						Listing.amenity_cookware,
						Listing.amenity_cookingrange,
						Listing.amenity_deckfurniture,
						Listing.amenity_dishwasher ,
						Listing.amenity_dishes,
						Listing.amenity_dvdplayer,
						Listing.amenity_exercisefacilities,
						Listing.amenity_foosball,
						Listing.amenity_gametable,
						Listing.amenity_games,
						Listing.amenity_grill,
						Listing.amenity_hairdryer,
						Listing.amenity_icemaker,
						Listing.amenity_internet,
						Listing.amenity_ironandboard,
						Listing.amenity_kidsgames,
						Listing.amenity_linensprovided,
						Listing.amenity_lobsterpot,
						Listing.amenity_microwave,
						Listing.amenity_minirefrigerator,
						Listing.amenity_mp3radiodock,
						Listing.amenity_outsideshower,
						Listing.amenity_oven,
						Listing.amenity_pinball,
						Listing.amenity_pingpong,
						Listing.amenity_privatepool,
						Listing.amenity_radio,
						Listing.amenity_refrigeratorfreezer,
						Listing.amenity_sofabed,
						Listing.amenity_stereo,
						Listing.amenity_telephone,
						Listing.amenity_television,
						Listing.amenity_toaster,
						Listing.amenity_toasteroven,
						Listing.amenity_towelsprovided,
						Listing.amenity_toys,
						Listing.amenity_utensils,
						Listing.amenity_vacuum,
						Listing.amenity_vcr,
						Listing.amenity_videogameconsole,
						Listing.amenity_videogames,
						Listing.amenity_washerdryer,
						Listing.amenity_wifi,
						Listing.feature_airconditioning,
						Listing.feature_balcony,
						Listing.feature_barbecuecharcoal,
						Listing.feature_barbecuegas,
						Listing.feature_boatslip,
						Listing.feature_cablesatellitetv,
						Listing.feature_clubhouse,
						Listing.feature_coveredparking,
						Listing.feature_deck,
						Listing.feature_diningroom,
						Listing.feature_elevator,
						Listing.feature_familyroom,
						Listing.feature_fullkitchen,
						Listing.feature_garage,
						Listing.feature_gasfireplace,
						Listing.feature_gatedcommunity,
						Listing.feature_wheelchairaccess,
						Listing.feature_heated,
						Listing.feature_heatedpool,
						Listing.feature_hottubjacuzzi,
						Listing.feature_internetaccess,
						Listing.feature_kitchenette,
						Listing.feature_livingroom,
						Listing.feature_loft,
						Listing.feature_onsitesecurity,
						Listing.feature_patio,
						Listing.feature_petsallowed,
						Listing.feature_playroom,
						Listing.feature_pool,
						Listing.feature_porch,
						Listing.feature_rooftopdeck,
						Listing.feature_sauna,
						Listing.feature_smokingpermitted,
						Listing.feature_woodfireplace,
						Listing.activity_antiquing,
						Listing.activity_basketballcourt,
						Listing.activity_beachcombing,
						Listing.activity_bicycling,
						Listing.activity_bikerentals,
						Listing.activity_birdwatching,
						Listing.activity_boatrentals,
						Listing.activity_boating,
						Listing.activity_botanicalgarden,
						Listing.activity_canoe,
						Listing.activity_churches,
						Listing.activity_cinemas,
						Listing.activity_bikesprovided,
						Listing.activity_deepseafishing,
						Listing.activity_fishing,
						Listing.activity_fitnesscenter,
						Listing.activity_golf,
						Listing.activity_healthbeautyspa,
						Listing.activity_hiking,
						Listing.activity_horsebackriding,
						Listing.activity_horseshoes,
						Listing.activity_hotairballooning,
						Listing.activity_iceskating,
						Listing.activity_jetskiing,
						Listing.activity_kayaking,
						Listing.activity_livetheater,
						Listing.activity_marina,
						Listing.activity_miniaturegolf,
						Listing.activity_mountainbiking,
						Listing.activity_museums,
						Listing.activity_paddleboating,
						Listing.activity_paragliding,
						Listing.activity_parasailing,
						Listing.activity_playground,
						Listing.activity_recreationcenter,
						Listing.activity_restaurants,
						Listing.activity_rollerblading,
						Listing.activity_sailing,
						Listing.activity_shelling,
						Listing.activity_shopping,
						Listing.activity_sightseeing,
						Listing.activity_skiing,
						Listing.activity_bayfishing,
						Listing.activity_spa,
						Listing.activity_surffishing,
						Listing.activity_surfing,
						Listing.activity_swimming,
						Listing.activity_tennis,
						Listing.activity_themeparks,
						Listing.activity_walking,
						Listing.activity_waterparks,
						Listing.activity_waterskiing,
						Listing.activity_watertubing,
						Listing.activity_wildlifeviewing,
						Listing.activity_zoo,
						Listing.required_stay,
						Listing.is_featured,
						Listing.featured_date,
						Listing.search_pos,
						Listing.search_pos_date,
						Promotion.start_date AS promotion_start_date,
						Promotion.end_date AS promotion_end_date,
						Thumb.type AS thumb_type,
						Thumb.width AS thumb_width,
						Thumb.height AS thumb_height,
						ListingTemplate.layout_id AS template_layout_id,
						ListingTemplate.cat_id AS template_cat_id,
						ListingTemplate.title AS template_title,
						ListingTemplate.status AS template_status,
						ListingTemplate.price AS template_price
					FROM Listing
						LEFT OUTER JOIN Promotion ON (Listing.promotion_id = Promotion.id)
						LEFT OUTER JOIN Image AS Thumb ON (Listing.thumb_id = Thumb.id)
						LEFT OUTER JOIN ListingTemplate ON (Listing.listingtemplate_id = ListingTemplate.id)";

			if($listing_id){
				$sql .= " where Listing.id = ".str_replace("'","",$listing_id);
			}

			$result = Self::fetch($sql);
			if(count($result) > 0){

				foreach($result as $row){
					
					/*
					 * Get location information
					 */
					
					if($row->location_1 > 0){
						$locationObj = new Location1();
						$location1=$locationObj->getLocation1($row->location_1);
						
						$row->location_1 				= $location1[0]->id;
						$row->location_1_title		= $location1[0]->name;
						$row->location_1_abbreviation	= $location1[0]->abbreviation;
						$row->location_1_friendly_url	= $location1[0]->friendly_url;
					}else{
						$row->location_1 				= 0;
						$row->location_1_title		= "";
						$row->location_1_abbreviation	= "";
						$row->location_1_friendly_url	= "";
					}
					
					unset($locationObj);
					if($row->location_2 > 0){
						$locationObj = new Location2($row->location_2);
						$row->location_2 				= $locationObj->id;
						$row->location_2_title		= $locationObj->name;
						$row->location_2_abbreviation	= $locationObj->abbreviation;
						$row->location_2_friendly_url	= $locationObj->friendly_url;


					}else{
						$row->location_2 				= 0;
						$row->location_2_title		= "";
						$row->location_2_abbreviation	= "";
						$row->location_2_friendly_url	= "";
					}
					
					unset($locationObj);
					if($row->location_3 > 0){
						$locationObj = new Location3();
						$location3=$locationObj->getLocation3($row->location_3);
						$row->location_3 				= $location3[0]->id;
						$row->location_3_title		= $location3[0]->name;
						$row->location_3_abbreviation	= $location3[0]->abbreviation;
						$row->location_3_friendly_url	= $location3[0]->friendly_url;

					}else{
						$row->location_3 				= 0;
						$row->location_3_title		= "";
						$row->location_3_abbreviation	= "";
						$row->location_3_friendly_url	= "";
					}
					
					
					if($row->location_4 > 0){
						$locationObj = new Location4();
						$location4=$locationObj->getLocation4($row->location_4);
						$row->location_4 				= $location4[0]->id;
						$row->location_4_title		= $location4[0]->name;
						$row->location_4_abbreviation	= $location4[0]->abbreviation;
						$row->location_4_friendly_url	= $location4[0]->friendly_url;
					}else{
						$row->location_4 				= 0;
						$row->location_4_title		= "";
						$row->location_4_abbreviation	= "";
						$row->location_4_friendly_url	= "";
					}
					
					unset($locationObj);
					if($row->location_5 > 0){
						
						$locationObj = new Location5();
						$location5=$locationObj->getLocation5($row->location_5);
						$row->location_5 				= $location5[0]->id;
						$row->location_5_title		= $location5[0]->name;
						$row->location_5_abbreviation	= $location5[0]->abbreviation;
						$row->location_5_friendly_url	= $location5[0]->friendly_url;
					}else{
						$row->location_5 				= 0;
						$row->location_5_title		= "";
						$row->location_5_abbreviation	= "";
						$row->location_5_friendly_url	= "";
					}
					
					$this->makeFromRow($row);
					if($method == "update"){
						$this->Update();
					}elseif($method == "insert"){
						$this->Add();
					}
				}
			}
		}
        
        public static function updateSql($sql)
    {
        $data= DB::connection('domain')->update($sql);
        return $data;
    }
    
     public static function fetch($sql)
        {
            $data= DB::connection('domain')->select($sql);
            return $data;
        }
        
        public static function fetchMain($sql)
        {
            $data= DB::select($sql);
            return $data;
        }
        
        public static function insertSql($sql)
        {
            $data= DB::connection('domain')->insert($sql);
            return $data;
        }
        
        public static function deleteSql($sql)
        {
            $data= DB::connection('domain')->delete($sql);
            return $data;
        }
        		
	}
?>