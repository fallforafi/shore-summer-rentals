<?php
namespace App\Functions;
use App\Functions\Setting;
use DB,PDO;
use Session;

	# ----------------------------------------------------------------------------------------------------
	# * FILE: /classes/class_editorChoice.php
	# ----------------------------------------------------------------------------------------------------

	/**
	 * <code>
	 *		$editorChoiceObj = new EditorChoice($var);
	 * <code>
	 * @package Classes
	 * @name EditorChoice
	 * @method EditorChoice
	 * @method makeFromRow
	 * @method Save
	 * @method Delete
	 * @method retrieve
	 * @access Public
	 */
	class EditorChoice extends Handle {

		/**
		 * @var varchar
		 * @access Private
		 */
		var $id;
		/**
		 * @var char
		 * @access Private
		 */
		var $available;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $name;
		/**
		 * @var integer
		 * @access Private
		 */
		var $image_id;
		/**
		 * @var array
		 * @access Private
		 */
		var $image;

		var $price;
		var $renewal_period;
		var $renewal_period_value;

		/**
		
		 * @name EditorChoice
		 * @access Public
		 * @param array $var
		 */
		function EditorChoice($var="") {
		DB::connection('domain')->setFetchMode(PDO::FETCH_CLASS);

			if (is_numeric($var) && ($var)) {
				
				$sql = "SELECT * FROM Editor_Choice WHERE id = $var";
				$row = self::fetch($sql);
				if($row[0]->image_id){
					$sql = "SELECT * FROM Image WHERE id = {$row[0]->image_id}";
					$image = self::fetch($sql);
				}
				$this->image = ($image) ? $image : "";
				$this->makeFromRow($row);
			} else {
                if (!is_array($var)) {
                    $var = array();
                }
				$this->makeFromRow($var);
			}
		}


	function EditorChoicePrice($id=0)
     {
       $sql = "SELECT * FROM Editor_Choice WHERE id = ".$id;
	  	$result = self::fetch($sql);
        return $result;
     }
    
		/**
		 * <code>
		 *		$this->makeFromRow($row);
		 * <code>
		 * @name makeFromRow
		 * @access Public
		 * @param array $row
		 */
		function makeFromRow($row="") {
			$this->id			= (isset($row[0]->id))			? $row[0]->id		: 0;
			$this->available	= ($row[0]->available)	? $row[0]->available	: 0;
			$this->name			= ($row[0]->name)		? $row[0]->name		: "";
			$this->image_id		= ($row[0]->image_id)	? $row[0]->image_id	: 0;
			$this->price		= ($row[0]->price)		? $row[0]->price	: 0;
			$this->renewal_period		= ($row[0]->renewal_period)		? $row[0]->renewal_period	: "";
			$this->renewal_period_value		= ($row[0]->renewal_period_value)		? $row[0]->renewal_period_value	: 0;
		}

		/**
		 * <code>
		 *		//Using this in forms or other pages.
		 *		$editorChoiceObj->Save();
		 * <br /><br />
		 *		//Using this in EditorChoice() class.
		 *		$this->Save();
		 * </code>
		 * @copyright Copyright 2005 Arca Solutions, Inc.
		 * @author Arca Solutions, Inc.
		 * @version 8.0.00
		 * @name Save
		 * @access Public
		 */
		function Save() {
			$this->prepareToSave();
			if ($this->id) {
			
				$sql = "UPDATE Editor_Choice SET"
					. " available = $this->available,"
					. " name      = $this->name,"
					. " image_id  = $this->image_id,"
					. " price  = $this->price,"
					. " renewal_period  = $this->renewal_period,"
					. " renewal_period_value  = $this->renewal_period_value"
					. " WHERE id  = $this->id";
				Self::insertSql($sql);

			} else {
				
				$sql = "INSERT INTO Editor_Choice"
					. " (available,"
					. " name,"
					. " image_id,"
					. " price,"
					. " renewal_period,"
					. " renewal_period_value)"
					. " VALUES"
					. " ($this->available,"
					. " $this->name,"
					. " $this->image_id,"
					. " $this->price,"
					. " $this->renewal_period,"
					. " $this->renewal_period_value)";
				Self::insertSql($sql);

				$this->id = DB::connection('domain')->getpdo()->lastInsertId();
			}
			$this->prepareToUse();
		}

		/**
		 * <code>
		 *		//Using this in forms or other pages.
		 *		$editorChoiceObj->Delete();
		 * <br /><br />
		 *		//Using this in EditorChoice() class.
		 *		$this->Delete();
		 * </code>
		 * @copyright Copyright 2005 Arca Solutions, Inc.
		 * @author Arca Solutions, Inc.
		 * @version 8.0.00
		 * @name Delete
		 * @access Public
		 */
		function Delete() {
			$imageObj = new Image($this->image_id);
			$imageObj->Delete();
			$sql = "DELETE FROM Listing_Choice WHERE editor_choice_id = $this->id";
			$dbMain = db_getDBObject(DEFAULT_DB, true);
			if (defined("SELECTED_DOMAIN_ID")) {
				$dbObj = db_getDBObjectByDomainID(SELECTED_DOMAIN_ID, $dbMain);
			} else {
				$dbObj = db_getDBObject();
			}
//			$dbMain->close();
			unset($dbMain);
			$dbObj->query($sql);
			$sql = "DELETE FROM Editor_Choice WHERE id = $this->id";
			$dbObj->query($sql);
		}

		/**
		 * <code>
		 *		//Using this in forms or other pages.
		 *		$editorChoiceObj->retrieve($id);
		 * <br /><br />
		 *		//Using this in EditorChoice() class.
		 *		$this->retrieve($id);
		 * </code>
		 * @copyright Copyright 2005 Arca Solutions, Inc.
		 * @author Arca Solutions, Inc.
		 * @version 8.0.00
		 * @name retrieve
		 * @access Public
		 * @param integer $id
		 * @return array $data
		 */
		function retrieve($id){
			$sql = "SELECT * FROM Editor_Choice WHERE id = $id";
			$dbMain = db_getDBObject(DEFAULT_DB, true);
			if (defined("SELECTED_DOMAIN_ID")) {
				$dbObj = db_getDBObjectByDomainID(SELECTED_DOMAIN_ID, $dbMain);
			} else {
				$dbObj = db_getDBObject();
			}
//			$dbMain->close();
			unset($dbMain);
			$result = $dbObj->query($sql);
			$data = mysql_fetch_assoc($result);
			return $data;
		}

		function getRenewalPeriod() {
			$date_string = 'per ';
			$date_string .= '<strong>';
			if ( $this->renewal_period == 'day' ) {
				if ( $this->renewal_period_value == 7 ) $date_string .= 'week';
				elseif ( $this->renewal_period_value == 14 ) $date_string .= '2 weeks';
				elseif ( $this->renewal_period_value == 21) $date_string .= '3 weeks';
				elseif ( $this->renewal_period_value > 1 ) $date_string .= $this->renewal_period_value . 'days';
				else $date_string .= 'day';
			} elseif ( $this->renewal_period == 'month' ) {
				if ( $this->renewal_period_value == 12 ) $date_string .= 'year';
				elseif ( $this->renewal_period_value > 1 ) $date_string .= $this->renewal_period_value . 'months';
				else $date_string .= 'month';
			} elseif ( $this->renewal_period == 'year' ) {
				if ( $this->renewal_period_value > 1 ) $date_string .= $this->renewal_period_value . 'years';
				else $date_string .= 'year';
			}
			$date_string .= '</strong>';
			return $date_string;
		}

		public static function updateSql($sql)
    {
        $data= DB::connection('domain')->update($sql);
        return $data;
    }
    
    public static function updateSqlMain($sql)
    {
        $data= DB::update($sql);
        return $data;
    }
    
    
     public static function fetch($sql)
        {
            $data= DB::connection('domain')->select($sql);
            return $data;
        }
        
        public static function fetchMain($sql)
        {
            $data= DB::select($sql);
            return $data;
        }
        
        public static function insertSql($sql)
        {
            $data= DB::connection('domain')->insert($sql);
            return $data;
        }
        public static function insertSqlMain($sql)
        {
            $data= DB::insert($sql);
            return $data;
        }
        
        
        public static function deleteSql($sql)
        {
            $data= DB::connection('domain')->delete($sql);
            return $data;
        }
        
        public static function deleteSqlMain($sql)
        {
            $data= DB::delete($sql);
            return $data;
        }
	


	}

?>
