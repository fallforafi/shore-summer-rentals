<?php 
namespace App\Functions;
use App\Functions\Functions;
use DB;
use PDO;
use App\Models\Domain\Sql;

	# ----------------------------------------------------------------------------------------------------
	# * FILE: PaymentCustomInvoiceLog.php
	# ----------------------------------------------------------------------------------------------------

	/**
	 * @method PaymentListingLog
	 * @method makeFromRow
	 * @method Save
	 * @access Public
	 */
class PaymentCustomInvoiceLog extends Handle {

		/**
		 * @var integer
		 * @access Private
		 */
		var $payment_log_id;
		var $custom_invoice_id;
		var $title;
		var $date;
		var $items;
		var $items_price;
		var $amount;
		var $domain_id;

		/**
		 * @name PaymentCustomInvoiceLog
		 * @access Public
		 * @param integer $var
		 */
		function PaymentCustomInvoiceLog($var="", $domain_id = false) {
			$this->domain_id = $domain_id;
		DB::connection('domain')->setFetchMode(PDO::FETCH_ASSOC);

			if (is_numeric($var) && ($var)) {
			
				$row=DB::connection('domain')->table('Payment_CustomInvoice_Log')->where('payment_log_id',$var)->get();

				$this->makeFromRow($row);
			} else {
                if (!is_array($var)) {
                    $var = array();
                }
				$this->makeFromRow($var);
			}
		}

		/**
		 * <code>
		 *		$this->makeFromRow($row);
		 * <code>
		 * @copyright Copyright 2005 Arca Solutions, Inc.
		 * @author Arca Solutions, Inc.
		 * @version 8.0.00
		 * @name makeFromRow
		 * @access Public
		 * @param array $row
		 */
		function makeFromRow($row="") {

			$this->payment_log_id		= (isset($row["payment_log_id"]))		? $row["payment_log_id"]	: ($this->payment_log_id	? $this->payment_log_id		: 0);
			$this->custom_invoice_id	= ($row["custom_invoice_id"])	? $row["custom_invoice_id"]	: ($this->custom_invoice_id	? $this->custom_invoice_id	: 0);
			$this->title				= ($row["title"])				? $row["title"]				: ($this->title				? $this->title				: "");
			$this->date					= ($row["date"])				? $row["date"]				: ($this->date				? $this->date				: 0);
			$this->items				= ($row["items"])				? $row["items"]				: ($this->items				? $this->items				: "");
			$this->items_price			= ($row["items_price"])			? $row["items_price"]		: ($this->items_price		? $this->items_price		: "");
			$this->amount				= ($row["amount"])				? $row["amount"]			: ($this->amount			? $this->amount				: 0);

		
		}

		/**
	
		 * @name Save
		 * @access Public
		 */
		function Save() {

			$this->PrepareToSave();

			$sql = "INSERT INTO Payment_CustomInvoice_Log"
				. " (payment_log_id,"
				. " custom_invoice_id,"
				. " title,"
				. " date,"
				. " items,"
				. " items_price,"
				. " amount"
				. " )"
				. " VALUES"
				. " ("
				. " $this->payment_log_id,"
				. " $this->custom_invoice_id,"
				. " $this->title,"
				. " $this->date,"
				. " $this->items,"
				. " $this->items_price,"
				. " $this->amount"
				. " )";

			Sql::insertSql($sql);

			$this->id = DB::connection('domain')->getpdo()->lastInsertId();

			$this->PrepareToUse();

		}

	}

?>

