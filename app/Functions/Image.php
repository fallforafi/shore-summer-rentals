<?php
namespace App\Functions;
use DB;
use App\Functions\Functions;
use App\Models\Domain\Sql;
use App\Models\Main\MainSql;

	/*==================================================================*\
	######################################################################
	#                                                                    #
	# Copyright 2005 Arca Solutions, Inc. All Rights Reserved.           #
	#                                                                    #
	# This file may not be redistributed in whole or part.               #
	# eDirectory is licensed on a per-domain basis.                      #
	#                                                                    #
	# ---------------- eDirectory IS NOT FREE SOFTWARE ----------------- #
	#                                                                    #
	# http://www.edirectory.com | http://www.edirectory.com/license.html #
	######################################################################
	\*==================================================================*/

	# ----------------------------------------------------------------------------------------------------
	# * FILE: /classes/class_image.php
	# ----------------------------------------------------------------------------------------------------

	/**
	 * <code>
	 *		$imageObj = new Image($id);
	 * <code>
	 * @copyright Copyright 2005 Arca Solutions, Inc.
	 * @author Arca Solutions, Inc.
	 * @version 8.0.00
	 * @package Classes
	 * @name Image
	 * @method Image
	 * @method makeFromRow
	 * @method Save
	 * @method Delete
	 * @method imageExists
	 * @method getTag
	 * @method _getTag
	 * @method getAntialiasedTag
	 * @access Public
	 */
	class Image extends Handle {

		/**
		 * @var integer
		 * @access Private
		 */
		var $id;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $type;
		/**
		 * @var integer
		 * @access Private
		 */
		var $width;
		/**
		 * @var integer
		 * @access Private
		 */
		var $height;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $prefix;
		/**
		 * @var boolean
		 * @access Private
		 */
		var $force_main;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $IMAGE_RELATIVE_PATH;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $IMAGE_DIR;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $IMAGE_URL;

		/**
		 * <code>
		 *		$imageObj = new Image($id);
		 * <code>
		 * @copyright Copyright 2005 Arca Solutions, Inc.
		 * @author Arca Solutions, Inc.
		 * @version 8.0.00
		 * @name Lang
		 * @access Public
		 * @param mixed $var
		 */
		function Image($var="", $force_main = false) {
			$this->force_main = $force_main;

			if ($this->force_main) {
				$this->IMAGE_RELATIVE_PATH = config('params.PROFILE_IMAGE_RELATIVE_PATH');
				$this->IMAGE_DIR = 'custom/profile';
				$this->IMAGE_URL = url('custom/profile');
			} else {
				$this->IMAGE_RELATIVE_PATH = IMAGE_RELATIVE_PATH;
				$this->IMAGE_DIR = IMAGE_DIR;
				$this->IMAGE_URL = IMAGE_URL;
			}

			if (is_numeric($var) && ($var)) {
				


				$sql = "SELECT * FROM Image WHERE id = $var";
				if ($this->force_main) {
					$row = Sql::fetchMain($sql);
				} else {
					$row = Sql::fetch($sql);

				}

				$this->makeFromRow($row);
			} else {
                if (!is_array($var)) {
                    $var = array();
                }
                $this->makeFromRow($var);
            }
		}

		/**
		 * <code>
		 *		$this->makeFromRow($row);
		 * <code>
		 * @copyright Copyright 2005 Arca Solutions, Inc.
		 * @author Arca Solutions, Inc.
		 * @version 8.0.00
		 * @name makeFromRow
		 * @access Public
		 * @param array $row
		 */
		function makeFromRow($row="") {
			if ($row[0]->id) $this->id = $row[0]->id;
			else if (!$this->id) $this->id = 0;
			$row[0]->type ? $this->type = $row[0]->type : $this->type = 0;
			$row[0]->width ? $this->width = $row[0]->width : $this->width = 0;
			$row[0]->height ? $this->height = $row[0]->height : $this->height = 0;
			$row[0]->prefix ? $this->prefix = $row[0]->prefix : $this->prefix = "";
		}

		/**
		 * <code>
		 *		//Using this in forms or other pages.
		 *		$imageObj->Save();
		 * <br /><br />
		 *		//Using this in Image() class.
		 *		$this->Save();
		 * </code>
		 * @copyright Copyright 2005 Arca Solutions, Inc.
		 * @author Arca Solutions, Inc.
		 * @version 8.0.00
		 * @name Save
		 * @access Public
		 */
		function Save() {


			//$this->prepareToSave();

			if ($this->id) {
				$sql = "UPDATE Image SET"
					. " type = $this->type,"
					. " width = $this->width,"
					. " height = $this->height,"
					. " prefix = $this->prefix"
					. " WHERE id = $this->id";

				if ($this->force_main) {
					Sql::insertSqlMain($sql);
				} else {
					Sql::insertSql($sql);

				}	
			} else {
				$sql = "INSERT INTO Image"
					. " (type, width, height,prefix)"
					. " VALUES"
					. " ($this->type, $this->width, $this->height, $this->prefix)";
				if ($this->force_main) {
					Sql::insertSqlMain($sql);
				} else {
					Sql::insertSql($sql);

				}
				$this->id = DB::getpdo()->lastInsertId();

				# IMAGE LOG ------------------------------------------------------------------------------------------
				$filename =url().'image_log.txt';
				$file = fopen($filename, "a");
				$message = date('d/m/Y H:m') . " => " . $sql . "\r\n";
				fwrite($file, $message);
				fclose($file);
				# ----------------------------------------------------------------------------------------------------
			}
			
			$this->prepareToUse();
		}

		/**
		 * <code>
		 *		//Using this in forms or other pages.
		 *		$imageObj->Delete();
		 * <br /><br />
		 *		//Using this in Image() class.
		 *		$this->Delete();
		 * </code>
		 * @copyright Copyright 2005 Arca Solutions, Inc.
		 * @author Arca Solutions, Inc.
		 * @version 8.0.00
		 * @name Delete
		 * @access Public
		 * @param integer $domain_id
		 */
		function Delete($domain_id = false) {
			$type = Functions::string_strtolower($this->type);
			/*$dbMain = db_getDBObject(DEFAULT_DB, true);
			/*if ($this->force_main) {
				$db = $dbMain;
			} else if ($domain_id) {
				$db = db_getDBObjectByDomainID($domain_id, $dbMain);
			} else {
				if (defined("SELECTED_DOMAIN_ID")) {
					$db = db_getDBObjectByDomainID(SELECTED_DOMAIN_ID, $dbMain);
				} else {
					$db = db_getDBObject();
				}
				unset($dbMain);
			}*/
			$sql = "DELETE FROM Image WHERE id = $this->id";
			# IMAGE LOG ------------------------------------------------------------------------------------------
			$filename = url().'image_log.txt';
			/*$file = fopen($filename, "a");
			$message = date('d/m/Y H:m') . " => " . $sql . "\r\n";
			fwrite($file, $message);
			fclose($file);*/
			# ----------------------------------------------------------------------------------------------------
			Sql::deleteSql($sql);
			@unlink($this->IMAGE_DIR."/".$this->prefix."photo_".$this->id.".".$type);
		}

		/**
		 * <code>
		 *		//Using this in forms or other pages.
		 *		$imageObj->imageExists();
		 * <br /><br />
		 *		//Using this in Image() class.
		 *		$this->imageExists();
		 * </code>
		 * @copyright Copyright 2005 Arca Solutions, Inc.
		 * @author Arca Solutions, Inc.
		 * @version 8.0.00
		 * @name imageExists
		 * @access Public
		 * @return boolean
		 */
		function imageExists() {
			$type = Functions::string_strtolower($this->type);
			$tmpFilename = $this->IMAGE_DIR."/".$this->prefix."photo_".$this->id.".".$type;
			return (file_exists($tmpFilename) && is_readable($tmpFilename));
		}

function image_getNewDimension($maxW, $maxH, $oldW, $oldH, &$newW, &$newH) {
		if (($oldW <= $maxW) && ($oldH <= $maxH)) { // without resize
			$newW = $oldW;
			$newH = $oldH;
		} else { // with resize
			if (($maxW / $oldW) <= ($maxH / $oldH)) { // resize from width
				$newW = $oldW * ($maxW / $oldW);
				$newH = $oldH * ($maxW / $oldW);
			} elseif (($maxW / $oldW) > ($maxH / $oldH)) { // resize from height
				$newW = $oldW * ($maxH / $oldH);
				$newH = $oldH * ($maxH / $oldH);
			}
		}
	}
		/**
		 * <code>
		 *		//Using this in forms or other pages.
		 *		$imageObj->getTag(...);
		 * <br /><br />
		 *		//Using this in Image() class.
		 *		$this->getTag(...);
		 * </code>
		 * @copyright Copyright 2005 Arca Solutions, Inc.
		 * @author Arca Solutions, Inc.
		 * @version 8.0.00
		 * @name getTag
		 * @access Public
		 * @param boolean $resize
		 * @param integer $maxWidth
		 * @param integer $maxHeight
		 * @param varchar $title
		 * @param boolean $force_resize
		 * @return varchar htmlcode
		 */
		function getTag($resize = false, $maxWidth = 0, $maxHeight = 0, $title="", $force_resize = false, $alternative_text = false, $class = false, $id = false) {
			
			if (config('params.RESIZE_IMAGES_UPGRADE') == "off"){
				$force_resize = false;
			}
			
			if(config('params.FORCE_ANTIALIASED_IMAGES') == "off" || Functions::string_strtolower($this->type) == "gif") {
				return $this->_getTag($resize, $maxWidth, $maxHeight, $title, $force_resize, $alternative_text, $class, $id);
			} else {
				return $this->getAntialiasedTag(true, $maxWidth, $maxHeight, $title, $force_resize, false, $alternative_text, $class);
			}
		}

		/**
		 * <code>
		 *		//Using this in forms or other pages.
		 *		$imageObj->_getTag(...);
		 * <br /><br />
		 *		//Using this in Image() class.
		 *		$this->_getTag(...);
		 * </code>
		 * @copyright Copyright 2005 Arca Solutions, Inc.
		 * @author Arca Solutions, Inc.
		 * @version 8.0.00
		 * @name _getTag
		 * @access Public
		 * @param boolean $resize
		 * @param integer $maxWidth
		 * @param integer $maxHeight
		 * @param varchar $title
		 * @param boolean $force_resize
		 * @return varchar htmlcode
		 */
		function _getTag($resize = false, $maxWidth = 0, $maxHeight = 0, $title="", $force_resize = false, $alternative_text = false, $class = false, $id = false){
			$type = string_strtolower($this->type);
			
			/*
			 * Alternative text for SEO
			 */
			unset($aux_image_seo); 
			if($alternative_text){
				$aux_image_seo .= " alt=\"".Functions::string_htmlentities($alternative_text)."\" ";
			}else{
				if($title){
					$aux_image_seo .= " alt=\"".Functions::string_htmlentities($title)."\"";
				}else{
					$aux_image_seo .= " alt=\"\"";
				}
				
			}
			if($title){
				$aux_image_seo .= " title =\"".Functions::string_htmlentities($title)."\" ";
			}else{
				$aux_image_seo .= " title =\"\" ";			
			}
			
			$title = $aux_image_seo;
			if ($force_resize) {
				$return = "<img ".($id ? "id=\"".$id."\"": "")." ".($class ? "class=\"".$class."\"": "")." width=\"".$maxWidth."\" height=\"".$maxHeight."\" src=\"".$this->IMAGE_URL."/".$this->prefix."photo_".$this->id.".".$type."\" ".$title." border=\"0\" />";
			} elseif($resize) {
				$this->image_getNewDimension($maxWidth, $maxHeight, $this->width, $this->height, $newWidth, $newHeight);
				$return = "<img ".($id ? "id=\"".$id."\"": "")." ".($class ? "class=\"".$class."\"": "")." width=\"".(int)$newWidth."\" height=\"".(int)$newHeight."\" src=\"".$this->IMAGE_URL."/".$this->prefix."photo_".$this->id.".".$type."\" ".$title." border=\"0\" ".(RESIZE_IMAGES_UPGRADE == "off"? "style=\"display:inline\"" : "")." />";
			} else {
				$return = "<img ".($id ? "id=\"".$id."\"": "")." ".($class ? "class=\"".$class."\"": "")." src=\"".$this->IMAGE_URL."/".$this->prefix."photo_".$this->id.".".$type."\" ".$title." border=\"0\" />";
			}
			return $return;
		}

		/**
		 * <code>
		 *		//Using this in forms or other pages.
		 *		$imageObj->getAntialiasedTag(...);
		 * <br /><br />
		 *		//Using this in Image() class.
		 *		$this->getAntialiasedTag(...);
		 * </code>
		 * @copyright Copyright 2005 Arca Solutions, Inc.
		 * @author Arca Solutions, Inc.
		 * @version 8.0.00
		 * @name getAntialiasedTag
		 * @access Public
		 * @param boolean $resize
		 * @param integer $maxWidth
		 * @param integer $maxHeight
		 * @param varchar $title
		 * @param boolean $distort
		 * @param varchar $img_path
		 * @return varchar htmlcode
		 */
		function getAntialiasedTag($resize = false, $maxWidth, $maxHeight, $title = "", $distort = false, $img_path = false, $alternative_text = false, $class = false) {
			$img_path = ($img_path) ? $img_path : $this->IMAGE_RELATIVE_PATH;

			$type = Functions::string_strtolower($this->type);
			if (!$maxWidth || !$maxHeight){
				$file = EDIRECTORY_ROOT.$img_path."/".$this->prefix."photo_".$this->id.".".$type;
				list($maxWidth, $maxHeight, $fileType, $fileAttr) = getimagesize($file);
			}
			$file = $img_path."/".$this->prefix."photo_".$this->id.".".$type;
			if ($resize) {
				if (!$distort) {
					$this->image_getNewDimension($maxWidth, $maxHeight, $this->width, $this->height, $newWidth, $newHeight);
					$maxWidth = $newWidth;
					$maxHeight = $newHeight;
				}
			}
			return "<img ".($class ? "class=\"".$class."\"": "")." alt=\"".Functions::string_htmlentities($alternative_text)."\" title=\"".Functions::string_htmlentities($title)."\" src=\"".url()."/image_resizer.php?img=".$file."&newWidth=".(int)$maxWidth."&newHeight=".(int)$maxHeight."\" width=\"".(int)$maxWidth."\" height=\"".(int)$maxHeight."\" border=\"0\" />";
		}
        
        /**
		 * <code>
		 *		//Using this in forms or other pages.
		 *		$imageObj->getPath(...);
		 * <br /><br />
		 *		//Using this in Image() class.
		 *		$this->getPath(...);
		 * </code>
		 * @copyright Copyright 2005 Arca Solutions, Inc.
		 * @author Arca Solutions, Inc.
		 * @version 8.0.00
		 * @name getPath
		 * @access Public
		 * @return varchar $path
		 */
        function getPath($relativePath = true){
            $path = "";
            $type = Functions::string_strtolower($this->type);
            if ($relativePath) {
                $path = $this->IMAGE_URL."/".$this->prefix."photo_".$this->id.".".$type;
            } else {
                $path = $this->IMAGE_DIR."/".$this->prefix."photo_".$this->id.".".$type;
            }
            
            return $path;
        }

	}

?>
