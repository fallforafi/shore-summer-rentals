<?php 
namespace App\Functions;
use DB;

	/*==================================================================*\
	######################################################################
	#                                                                    #
	# Copyright 2005 Arca Solutions, Inc. All Rights Reserved.           #
	#                                                                    #
	# This file may not be redistributed in whole or part.               #
	# eDirectory is licensed on a per-domain basis.                      #
	#                                                                    #
	# ---------------- eDirectory IS NOT FREE SOFTWARE ----------------- #
	#                                                                    #
	# http://www.edirectory.com | http://www.edirectory.com/license.html #
	######################################################################
	\*==================================================================*/

	# ----------------------------------------------------------------------------------------------------
	# * FILE: /classes/class_Profile.php
	# ----------------------------------------------------------------------------------------------------

    /**
	 * <code>
	 *		$profileObj = new Profile($id);
	 * <code>
	 * @copyright Copyright 2005 Arca Solutions, Inc.
	 * @author Arca Solutions, Inc.
	 * @version 8.0.00
	 * @package Classes
	 * @name Profile
	 * @method Profile
	 * @method makeFromRow
	 * @method Save
	 * @method profileExists
	 * @method findUid
	 * @method Delete
	 * @method fUrl_Exists
	 * @method deal_done
	 * @access Public
	 */

	class Profile extends Handle {

        /**
		 * @var integer
		 * @access Private
		 */
		var $account_id;
        /**
		 * @var integer
		 * @access Private
		 */
		var $image_id;
        /**
		 * @var string
		 * @access Private
		 */
		var $facebook_image;
        /**
		 * @var integer
		 * @access Private
		 */
		var $facebook_image_height;
        /**
		 * @var integer
		 * @access Private
		 */
		var $facebook_image_width;
        /**
		 * @var string
		 * @access Private
		 */
		var $nickname;
        /**
		 * @var string
		 * @access Private
		 */
		var $friendly_url;
        /**
		 * @var date
		 * @access Private
		 */
		var $entered;
        /**
		 * @var date
		 * @access Private
		 */
		var $updated;
        /**
		 * @var string
		 * @access Private
		 */
		var $personal_message;
        /**
		 * @var string
		 * @access Private
		 */
		var $twitter_account;
        /**
		 * @var string
		 * @access Private
		 */
		var $facebook_uid;
        /**
		 * @var string
		 * @access Private
		 */
		var $tw_post;
        /**
		 * @var string
		 * @access Private
		 */
		var $tw_oauth_token;
        /**
		 * @var string
		 * @access Private
		 */
		var $tw_oauth_token_secret;
        /**
		 * @var string
		 * @access Private
		 */
		var $tw_screen_name;
        /**
		 * @var string
		 * @access Private
		 */
		var $profile_exists;
        /**
		 * @var string
		 * @access Private
		 */
		var $location;
        /**
		 * @var string
		 * @access Private
		 */
		var $usefacebooklocation;
        /**
		 * @var char
		 * @access Private
		 */
        var $profile_complete;

        /**
		 * <code>
		 *		$profileObj = new Profile($id);
		 * <code>
		 * @copyright Copyright 2005 Arca Solutions, Inc.
		 * @author Arca Solutions, Inc.
		 * @version 8.0.00
		 * @name Profile
		 * @access Public
		 * @param integer $var
		 */
		function Profile($var='') {

			if (is_numeric($var) && ($var)) {
				$sql = "SELECT * FROM Profile WHERE account_id = $var";
				$row = Self::fetchMain($sql);
				$this->makeFromRow($row);
			} else {
                if (!is_array($var)) {
                    $var = array();
                }
				$this->arrayMakeFromRow($var);
			}
		}

        /**
		 * <code>
		 *		$this->makeFromRow($row);
		 * <code>
		 * @copyright Copyright 2005 Arca Solutions, Inc.
		 * @author Arca Solutions, Inc.
		 * @version 8.0.00
		 * @name makeFromRow
		 * @access Public
		 * @param array $row
		 */
		function makeFromRow($row='') {


			isset($row[0]->account_id)              ?           $this->account_id = $row[0]->account_id:									$this->account_id = 0;
			isset($row[0]->image_id)                ?           $this->image_id = $row[0]->image_id:										$this->image_id = 0;
			isset($row[0]->facebook_image)          ?			$this->facebook_image = $row[0]->facebook_image:							$this->facebook_image = "";
			isset($row[0]->facebook_image_height)   ?           $this->facebook_image_height = $row[0]->facebook_image_height:			$this->facebook_image_height = 0;
			isset($row[0]->facebook_image_width)    ?           $this->facebook_image_width = $row[0]->facebook_image_width:				$this->facebook_image_width = 0;
            isset($row[0]->nickname)                ?           $this->nickname = $row[0]->nickname:										$this->nickname = "";
			isset($row[0]->friendly_url)            ?			$this->friendly_url = $row[0]->friendly_url:								$this->friendly_url = "";
			isset($row[0]->entered)                 ?           $this->entered = $row[0]->entered:										$this->entered = 0;
			isset($row[0]->updated)                 ?           $this->updated = $row[0]->updated:										$this->updated = 0;
			isset($row[0]->personal_message)        ?           $this->personal_message = $row[0]->personal_message:						$this->personal_message = "";
			isset($row[0]->twitter_account)         ?           $this->twitter_account = $row[0]->twitter_account:						$this->twitter_account? $this->twitter_account = $this->twitter_account: $this->twitter_account = "";
			if (isset($row[0]->facebook_uid)) $this->facebook_uid = $row[0]->facebook_uid;
			else if (!$this->facebook_uid) $this->facebook_uid = "";
			isset($row[0]->tw_post)                 ?           $this->tw_post = 1:														$this->tw_post = 0;
			isset($row[0]->tw_oauth_token)          ?			$this->tw_oauth_token = $row[0]->tw_oauth_token:							$this->tw_oauth_token = "";
			isset($row[0]->tw_oauth_token_secret)   ?           $this->tw_oauth_token_secret = $row[0]->tw_oauth_token_secret:			$this->tw_oauth_token_secret = "";
			isset($row[0]->tw_screen_name)          ?			$this->tw_screen_name = $row[0]->tw_screen_name:							$this->tw_screen_name = "";
			isset($row[0]->location)                ?           $this->location = $row[0]->location:										$this->location = "";
			isset($row[0]->usefacebooklocation)     ?           $this->usefacebooklocation = $row[0]->usefacebooklocation:				$this->usefacebooklocation = 0;
            $this->profile_complete         = (isset($row[0]->profile_complete))			? $row[0]->profile_complete                  : ($this->profile_complete				? $this->profile_complete				: "n");

			$this->profileExists();
		}

		function arrayMakeFromRow($row='') {
			isset($row["account_id"])              ?           $this->account_id = $row["account_id"]:									$this->account_id = 0;
			isset($row["image_id"])                ?           $this->image_id = $row["image_id"]:										$this->image_id = 0;
			isset($row["facebook_image"])          ?			$this->facebook_image = $row["facebook_image"]:							$this->facebook_image = "";
			isset($row["facebook_image_height"])   ?           $this->facebook_image_height = $row["facebook_image_height"]:			$this->facebook_image_height = 0;
			isset($row["facebook_image_width"])    ?           $this->facebook_image_width = $row["facebook_image_width"]:				$this->facebook_image_width = 0;
            isset($row["nickname"])                ?           $this->nickname = $row["nickname"]:										$this->nickname = "";
			isset($row["friendly_url"] )           ?			$this->friendly_url = $row["friendly_url"]:								$this->friendly_url = "";
			isset($row["entered"])                 ?           $this->entered = $row["entered"]:										$this->entered = 0;
			isset($row["updated"])               ?           $this->updated = $row["updated"]:										$this->updated = 0;
			isset($row["personal_message"])        ?           $this->personal_message = $row["personal_message"]:						$this->personal_message = "";
			isset($row["twitter_account"])         ?           $this->twitter_account = $row["twitter_account"]:						$this->twitter_account? $this->twitter_account = $this->twitter_account: $this->twitter_account = "";
			if (isset($row["facebook_uid"])) $this->facebook_uid = $row["facebook_uid"];
			else if (!$this->facebook_uid) $this->facebook_uid = "";
			isset($row["tw_post"])                 ?           $this->tw_post = 1:														$this->tw_post = 0;
			isset($row["tw_oauth_token"])          ?			$this->tw_oauth_token = $row["tw_oauth_token"]:							$this->tw_oauth_token = "";
			isset($row["tw_oauth_token_secret"])   ?           $this->tw_oauth_token_secret = $row["tw_oauth_token_secret"]:			$this->tw_oauth_token_secret = "";
			isset($row["tw_screen_name"])          ?			$this->tw_screen_name = $row["tw_screen_name"]:							$this->tw_screen_name = "";
			isset($row["location"])                ?           $this->location = $row["location"]:										$this->location = "";
			isset($row["usefacebooklocation"])     ?           $this->usefacebooklocation = $row["usefacebooklocation"]:				$this->usefacebooklocation = 0;
           $this->profile_complete         = (isset($row["profile_complete"]))			? $row["profile_complete"]                  : ($this->profile_complete				? $this->profile_complete				: "n");

			$this->profileExists();
		}



        /**
		 * <code>
		 *		//Using this in forms or other pages.
		 *		$profileObj->Save();
		 * <br /><br />
		 *		//Using this in Profile() class.
		 *		$this->Save();
		 * </code>
		 * @copyright Copyright 2005 Arca Solutions, Inc.
		 * @author Arca Solutions, Inc.
		 * @version 8.0.00
		 * @name Save
		 * @access Public
		 */
		function Save() {
			$exists = $this->profile_exists;
			
			$this->prepareToSave();
		
			if ($exists) {
				$sql  = "UPDATE Profile SET"
					. " image_id = $this->image_id,"
					. " facebook_image = $this->facebook_image,"
					. " facebook_image_height = $this->facebook_image_height,"
					. " facebook_image_width = $this->facebook_image_width,"
					. " nickname = $this->nickname,"
					. " friendly_url = $this->friendly_url,"
					. " updated = NOW(),"
					. " personal_message = $this->personal_message,"
					. " twitter_account = $this->twitter_account,"
					. " facebook_uid = $this->facebook_uid, "
					. " tw_post = $this->tw_post,"
					. " tw_oauth_token = $this->tw_oauth_token,"
					. " tw_oauth_token_secret = $this->tw_oauth_token_secret,"
					. " tw_screen_name = $this->tw_screen_name,"
					. " location = $this->location,"
					. " usefacebooklocation = $this->usefacebooklocation,"
					. " profile_complete = $this->profile_complete"       
					. " WHERE account_id = $this->account_id";

				Self::insertSqlMain($sql);
			} else {
				$auxAccID = str_replace("'", "", $this->account_id);
				if ($auxAccID > 0) {
                    
                    if ($this->friendly_url == "''") {
                        $this->friendly_url = $this->system_generateFriendlyURL(str_replace("'", "", $this->nickname));
                    }
                    
                    //Check for repeated friendly url
                    /*$sql = "SELECT account_id FROM Profile WHERE friendly_url = ".$this->friendly_url;
                    $result = Self::fetchMain($sql);
                    if (count($result) > 0) {
                        $this->friendly_url = $this->friendly_url."-".uniqid();
                    }*/
                    
                    $this->friendly_url = ($this->friendly_url);

					$sql = "INSERT INTO Profile"
						. " (account_id, image_id, facebook_image, facebook_image_height, facebook_image_width, nickname, friendly_url, entered, personal_message, twitter_account, facebook_uid, tw_post, tw_oauth_token, tw_oauth_token_secret, tw_screen_name, location, usefacebooklocation, profile_complete)"
						. " VALUES"
						. " ($this->account_id, $this->image_id, $this->facebook_image, $this->facebook_image_height, $this->facebook_image_width , $this->nickname, '$this->friendly_url', NOW(), $this->personal_message, $this->twitter_account, $this->facebook_uid, $this->tw_post, $this->tw_oauth_token, $this->tw_oauth_token_secret, $this->tw_screen_name, $this->location, $this->usefacebooklocation, $this->profile_complete)";
					Self::insertSqlMain($sql);


				}
			}


			$this->prepareToUse();
		}
        
        /**
        * <code>
        *		//Using this in forms or other pages.
        *		$profileObj->profileExists();
        * <br /><br />
        *		//Using this in Profile() class.
        *		$this->profileExists();
        * </code>
        * @copyright Copyright 2005 Arca Solutions, Inc.
        * @author Arca Solutions, Inc.
        * @version 8.0.00
        * @name Save
        * @access Public
        */
		function profileExists() {
			if ($this->account_id > 0) $this->profile_exists = true;
			else $this->profile_exists = false;
		}
        
        /**
		 * <code>
		 *		//Using this in forms or other pages.
		 *		$profileObj->findUid();
		 * <br /><br />
		 *		//Using this in Profile() class.
		 *		$this->findUid();
		 * </code>
		 * @copyright Copyright 2005 Arca Solutions, Inc.
		 * @author Arca Solutions, Inc.
		 * @version 8.0.00
		 * @name Save
		 * @access Public
		 */
		function findUid($uid=false){
			if (!$uid) return false;
			
			$sql="SELECT * FROM Profile WHERE facebook_uid = '".addslashes($uid)."'";

			$result = Self::fetchMain($sql);
			
			if ($result[0]->account_id){
				$this->makeFromRow($result);
				return true;
			} else {
                return false;
            }

		}

        /**
		 * <code>
		 *		//Using this in forms or other pages.
		 *		$profileObj->Delete();
		 * <br /><br />
		 *		//Using this in Profile() class.
		 *		$this->Delete();
		 * </code>
		 * @copyright Copyright 2005 Arca Solutions, Inc.
		 * @author Arca Solutions, Inc.
		 * @version 8.0.00
		 * @name Save
		 * @access Public
		 */
		function Delete() {
			$dbObj = db_getDBObject(DEFAULT_DB,true);
            
            ### IMAGE
			if ($this->image_id) {
				$image = new Image($this->image_id, true);
				if ($image) $image->Delete();
            }
            
			$sql = "DELETE FROM Profile WHERE account_id = $this->account_id";
			$dbObj->query($sql);
		}

        /**
		 * <code>
		 *		//Using this in forms or other pages.
		 *		$profileObj->fUrl_Exists();
		 * <br /><br />
		 *		//Using this in Profile() class.
		 *		$this->fUrl_Exists();
		 * </code>
		 * @copyright Copyright 2005 Arca Solutions, Inc.
		 * @author Arca Solutions, Inc.
		 * @version 8.0.00
		 * @name Save
		 * @access Public
		 */
		function fUrl_Exists($fUrl) {
			if ($fUrl) {
			
				$sql = " SELECT account_id FROM Profile WHERE friendly_url = '".$fUrl."'";
				$row =Sql::fetchMain($sql);
				if (count($row) > 0) {
					//$row = mysql_fetch_assoc($result);
					if ($row[0]->account_id == Functions::sess_getAccountIdFromSession()) {
						return false;
					} else {
						return true;
					}
				} else {
					return false;
				}
			} else {
				return false;
			}
		}

        /**
		 * <code>
		 *		//Using this in forms or other pages.
		 *		$profileObj->deal_done();
		 * <br /><br />
		 *		//Using this in Profile() class.
		 *		$this->deal_done();
		 * </code>
		 * @copyright Copyright 2005 Arca Solutions, Inc.
		 * @author Arca Solutions, Inc.
		 * @version 8.0.00
		 * @name Save
		 * @access Public
		 */
		function deal_done($dealtype = "twitter", $promotion_id = false, $network_response = false){

			if (!$promotion_id)  return false;

			if($dealtype == "profile"){
				$twittered = 0;
				$facebooked = 0;
			} else if($dealtype == "twitter"){
				$twittered = 1;
				$facebooked = 0;
			} else {
				$twittered = 0;
				$facebooked = 1;
			}

			$dbObj = db_getDBObject(DEFAULT_DB, true);
            $dbDomain = db_getDBObjectByDomainID(SELECTED_DOMAIN_ID, $dbObj);

			$sqlRedeem = "SELECT id FROM `Promotion_Redeem` WHERE `account_id` = ".sess_getAccountIdFromSession()." AND `promotion_id` = $promotion_id LIMIT 1";
			$resRedeem = $dbDomain->query($sqlRedeem);

			if (mysql_num_rows($resRedeem) > 0) {
				$rowRedeem = mysql_fetch_assoc($resRedeem);
				$redeem_id = $rowRedeem["id"];

				$arrayUpdate = array();

				if ($dealtype == "twitter") $arrayUpdate[]= "twittered = 1";
				if ($dealtype == "profile") $arrayUpdate[]= "facebooked = 1";

				$sqlSet = implode(",",$arrayUpdate);
				$sqlSet .= ", network_response = CONCAT(network_response, ".db_formatString("[|]".$network_response).")";

				$sql = "UPDATE Promotion_Redeem SET ".$sqlSet." WHERE id = ".$redeem_id;
				$result = $dbDomain->query($sql);
			} else {
				$redeem_code = system_generatePassword();

				$sql = "INSERT INTO Promotion_Redeem ( ";
				$sql .= "account_id, promotion_id, twittered, facebooked, network_response, datetime, redeem_code";
				$sql .= " ) VALUES (";
				$sql .= (int)sess_getAccountIdFromSession().", ";
				$sql .= (int)$promotion_id.", ";
				$sql .= "$twittered, $facebooked, ";
				$sql .= db_formatString($network_response).", ";
				$sql .= "NOW(), ".db_formatString($redeem_code)."";
				$sql .= ")";
				$result = $dbDomain->query($sql);

				$sql = "UPDATE Promotion SET amount = amount - 1 WHERE id = $promotion_id";
				$dbDomain->query($sql);
			
                //Notification to deal owner
                $promotionObj = new Promotion($promotion_id);
                $contactObj = new Contact($promotionObj->getNumber('account_id'));
                if ($emailNotificationObj = system_checkEmail(SYSTEM_NEW_DEAL)) {
                    setting_get("sitemgr_email", $sitemgr_email);
                    $sitemgr_emails = explode(",", $sitemgr_email);
                    if ($sitemgr_emails[0]) $sitemgr_email = $sitemgr_emails[0];
                    $subject   = $emailNotificationObj->getString("subject");
                    $body      = $emailNotificationObj->getString("body");
                    $body      = system_replaceEmailVariables($body, $promotionObj->getNumber('id'), 'promotion');
                    $subject   = system_replaceEmailVariables($subject, $promotionObj->getNumber('id'), 'promotion');
                    $body      = html_entity_decode($body);
                    $subject   = html_entity_decode($subject);
                    $error = false;
                    system_mail($contactObj->getString("email"), $subject, $body, EDIRECTORY_TITLE." <$sitemgr_email>", $emailNotificationObj->getString("content_type"), "", $emailNotificationObj->getString("bcc"), $error);
                }

                //Notification to user
                unset($contactObj);
                $contactObj = new Contact(sess_getAccountIdFromSession());
                if ($emailNotificationObj = system_checkEmail(SYSTEM_DEAL_DONE)) {
                    setting_get("sitemgr_email", $sitemgr_email);
                    $sitemgr_emails = explode(",", $sitemgr_email);
                    if ($sitemgr_emails[0]) $sitemgr_email = $sitemgr_emails[0];
                    $subject   = $emailNotificationObj->getString("subject");
                    $body      = $emailNotificationObj->getString("body");
                    $body      = system_replaceEmailVariables($body, $promotionObj->getNumber('id'), 'promotion', $redeem_code, $contactObj->getString('first_name').' '.$contactObj->getString('last_name'));
                    $subject   = system_replaceEmailVariables($subject, $promotionObj->getNumber('id'), 'promotion');
                    $body      = html_entity_decode($body);
                    $subject   = html_entity_decode($subject);
                    $error = false;
                    system_mail($contactObj->getString("email"), $subject, $body, EDIRECTORY_TITLE." <$sitemgr_email>", $emailNotificationObj->getString("content_type"), "", $emailNotificationObj->getString("bcc"), $error);
                }
            }

			return $redeem_code;
		}  
        
        
        
	function system_generateFriendlyURL($string) {
		
		 $chars_Accent = array('À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Æ', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ð', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ø', 'Ù', 'Ú', 'Û', 'Ü', 'Ý', 'ß', 'à', 'á', 'â', 'ã', 'ä', 'å', 'æ', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ø', 'ù', 'ú', 'û', 'ü', 'ý', 'ÿ', 'Ā', 'ā', 'Ă', 'ă', 'Ą', 'ą', 'Ć', 'ć', 'Ĉ', 'ĉ', 'Ċ', 'ċ', 'Č', 'č', 'Ď', 'ď', 'Đ', 'đ', 'Ē', 'ē', 'Ĕ', 'ĕ', 'Ė', 'ė', 'Ę', 'ę', 'Ě', 'ě', 'Ĝ', 'ĝ', 'Ğ', 'ğ', 'Ġ', 'ġ', 'Ģ', 'ģ', 'Ĥ', 'ĥ', 'Ħ', 'ħ', 'Ĩ', 'ĩ', 'Ī', 'ī', 'Ĭ', 'ĭ', 'Į', 'į', 'İ', 'ı', 'Ĳ', 'ĳ', 'Ĵ', 'ĵ', 'Ķ', 'ķ', 'Ĺ', 'ĺ', 'Ļ', 'ļ', 'Ľ', 'ľ', 'Ŀ', 'ŀ', 'Ł', 'ł', 'Ń', 'ń', 'Ņ', 'ņ', 'Ň', 'ň', 'ŉ', 'Ō', 'ō', 'Ŏ', 'ŏ', 'Ő', 'ő', 'Œ', 'œ', 'Ŕ', 'ŕ', 'Ŗ', 'ŗ', 'Ř', 'ř', 'Ś', 'ś', 'Ŝ', 'ŝ', 'Ş', 'ş', 'Š', 'š', 'Ţ', 'ţ', 'Ť', 'ť', 'Ŧ', 'ŧ', 'Ũ', 'ũ', 'Ū', 'ū', 'Ŭ', 'ŭ', 'Ů', 'ů', 'Ű', 'ű', 'Ų', 'ų', 'Ŵ', 'ŵ', 'Ŷ', 'ŷ', 'Ÿ', 'Ź', 'ź', 'Ż', 'ż', 'Ž', 'ž', 'ſ', 'ƒ', 'Ơ', 'ơ', 'Ư', 'ư', 'Ǎ', 'ǎ', 'Ǐ', 'ǐ', 'Ǒ', 'ǒ', 'Ǔ', 'ǔ', 'Ǖ', 'ǖ', 'Ǘ', 'ǘ', 'Ǚ', 'ǚ', 'Ǜ', 'ǜ', 'Ǻ', 'ǻ', 'Ǽ', 'ǽ', 'Ǿ', 'ǿ');
    $chars_no_Accent = array('A', 'A', 'A', 'A', 'A', 'A', 'AE', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'D', 'N', 'O', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'Y', 's', 'a', 'a', 'a', 'a', 'a', 'a', 'ae', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'y', 'y', 'A', 'a', 'A', 'a', 'A', 'a', 'C', 'c', 'C', 'c', 'C', 'c', 'C', 'c', 'D', 'd', 'D', 'd', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'G', 'g', 'G', 'g', 'G', 'g', 'G', 'g', 'H', 'h', 'H', 'h', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'IJ', 'ij', 'J', 'j', 'K', 'k', 'L', 'l', 'L', 'l', 'L', 'l', 'L', 'l', 'l', 'l', 'N', 'n', 'N', 'n', 'N', 'n', 'n', 'O', 'o', 'O', 'o', 'O', 'o', 'OE', 'oe', 'R', 'r', 'R', 'r', 'R', 'r', 'S', 's', 'S', 's', 'S', 's', 'S', 's', 'T', 't', 'T', 't', 'T', 't', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'W', 'w', 'Y', 'y', 'Y', 'Z', 'z', 'Z', 'z', 'Z', 'z', 's', 'f', 'O', 'o', 'U', 'u', 'A', 'a', 'I', 'i', 'O', 'o', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'A', 'a', 'AE', 'ae', 'O', 'o');
		
		$string_friendly_url = preg_replace("/[^a-zA-Z0-9]/", "-", str_replace($chars_Accent, $chars_no_Accent, $string));
		$string_friendly_url = strtolower(preg_replace("/[\\-]{2,}/", "-", $string_friendly_url));
		
		return $string_friendly_url;
		
	}
    
        public static function updateSql($sql)
    {
        $data= DB::connection('domain')->update($sql);
        return $data;
    }
    
     public static function fetch($sql)
        {
            $data= DB::connection('domain')->select($sql);
            return $data;
        }
        
        public static function fetchMain($sql)
        {
            $data= DB::select($sql);
            return $data;
        }
        
        public static function insertSql($sql)
        {
            $data= DB::connection('domain')->insert($sql);
            return $data;
        }
        
        public static function deleteSql($sql)
        {
            $data= DB::connection('domain')->delete($sql);
            return $data;
        }
        
        
         public static function updateSqlMain($sql)
        {
            $data= DB::update($sql);
            return $data;
        }
        
        
        
         public static function insertSqlMain($sql)
        {
            $data= DB::insert($sql);
            return $data;
        }
        
	}
?>