<?php
namespace App\Functions;
use DB;

	/*==================================================================*\
	######################################################################
	#                                                                    #
	# Copyright 2005 Arca Solutions, Inc. All Rights Reserved.           #
	#                                                                    #
	# This file may not be redistributed in whole or part.               #
	# eDirectory is licensed on a per-domain basis.                      #
	#                                                                    #
	# ---------------- eDirectory IS NOT FREE SOFTWARE ----------------- #
	#                                                                    #
	# http://www.edirectory.com | http://www.edirectory.com/license.html #
	######################################################################
	\*==================================================================*/

	# ----------------------------------------------------------------------------------------------------
	# * FILE: /classes/class_AccountProfileContact.php
	# ----------------------------------------------------------------------------------------------------

	/**
	 * <code>
	 *		$accountObj = new AccountProfileContact($domain_id, $account_id);
	 * <code>
	 * @copyright Copyright 2005 Arca Solutions, Inc.
	 * @author Arca Solutions, Inc.
	 * @version 8.0.00
	 * @package Classes
	 * @name AccountProfileContact
	 * @method AccountProfileContact
	 * @method makeFromRow
	 * @method Save
	 * @method Delete
	 * @access Public
	 */
	class AccountProfileContact extends Handle {

		/**
		 * @var integer
		 * @access Private
		 */
		var $account_id;
        /**
		 * @var varchar
		 * @access Private
		 */
		var $username;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $first_name;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $last_name;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $nickname;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $friendly_url;
		/**
		 * @var integer
		 * @access Private
		 */
		var $image_id;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $facebook_image;
		/**
		 * @var integer
		 * @access Private
		 */
		var $facebook_image_width;
		/**
		 * @var integer
		 * @access Private
		 */
		var $facebook_image_height;
		/**
		 * @var char
		 * @access Private
		 */
		var $has_profile;
		/**
		 * @var integer
		 * @access Private
		 */
		var $domain_id;
        var $has_account;

		/**
		 * <code>
		 *		$accountObj = new AccountProfileContact($domain_id, $account_id);
		 * <code>
		 * @copyright Copyright 2005 Arca Solutions, Inc.
		 * @author Arca Solutions, Inc.
		 * @version 8.0.00
		 * @name AccountProfileContact
		 * @access Public
		 * @param integer $domain_id
		 * @param integer $var
		 */
		function AccountProfileContact($domain_id, $var='') {
			if (is_numeric($var) && ($var)) {
				$this->domain_id = $domain_id;
				
				$sql = "SELECT * FROM AccountProfileContact WHERE account_id = $var";
				$row = Sql::fetch($sql);
				$this->makeFromRow($row);
			} else {
                if (!is_array($var)) {
                    $var = array();
                }
				$this->makeFromRow($var);
			}
		}

		/**
		 * <code>
		 *		$this->makeFromRow($row);
		 * <code>
		 * @copyright Copyright 2005 Arca Solutions, Inc.
		 * @author Arca Solutions, Inc.
		 * @version 8.0.00
		 * @name makeFromRow
		 * @access Public
		 * @param array $row
		 */
		function makeFromRow($row='') {
			if ($row[0]->account_id) { $this->account_id = $row[0]->account_id; $this->has_account = 1; }
			else if (!$this->account_id) { $this->account_id = 0; $this->has_account = 0; }
            if ($row[0]->username) $this->username = $row[0]->username;
			else if (!$this->username) $this->username = "";
			if ($row[0]->first_name) $this->first_name = $row[0]->first_name;
			else if (!$this->first_name) $this->first_name = "";
			if ($row[0]->last_name) $this->last_name = $row[0]->last_name;
			else if (!$this->last_name) $this->last_name = "";
			if ($row[0]->nickname) $this->nickname = $row[0]->nickname;
			else if (!$this->nickname) $this->nickname = "";
			if ($row[0]->friendly_url) $this->friendly_url = $row[0]->friendly_url;
			else if (!$this->friendly_url) $this->friendly_url = "";
			if ($row[0]->image_id) $this->image_id = $row[0]->image_id;
			else if (!$this->image_id) $this->image_id = 0;
			if ($row[0]->facebook_image) $this->facebook_image = $row[0]->facebook_image;
			else if (!$this->facebook_image) $this->facebook_image = "";
			if ($row[0]->facebook_image_width) $this->facebook_image_width = $row[0]->facebook_image_width;
			else if (!$this->facebook_image_width) $this->facebook_image_width = 0;
			if ($row[0]->facebook_image_height) $this->facebook_image_height = $row[0]->facebook_image_height;
			else if (!$this->facebook_image_height) $this->facebook_image_height = 0;
			if ($row[0]->has_profile) $this->has_profile = $row[0]->has_profile;
			else if (!$this->has_profile) $this->has_profile = "n";
		}

		/**
		 * <code>
		 *		//Using this in forms or other pages.
		 *		$accountObj->Save();
		 * <br /><br />
		 *		//Using this in AccountProfileContact() class.
		 *		$this->Save();
		 * </code>
		 * @copyright Copyright 2005 Arca Solutions, Inc.
		 * @author Arca Solutions, Inc.
		 * @version 8.0.00
		 * @name Save
		 * @access Public
		 */
		function Save() {
			$this->prepareToSave();

			
			if ($this->has_account) {
				$sql  = "UPDATE AccountProfileContact SET"
					. " username = $this->username,"
					. " first_name = $this->first_name,"
					. " last_name = $this->last_name,"
					. " nickname = $this->nickname,"
					. " friendly_url = $this->friendly_url,"
					. " image_id = $this->image_id,"
					. " facebook_image = $this->facebook_image,"
					. " has_profile = $this->has_profile"
					. " WHERE account_id = $this->account_id";

				Self::updateSql($sql);
			} else {
				$sql = "INSERT INTO AccountProfileContact"
					. " (account_id, username, first_name, last_name, nickname, friendly_url, image_id, facebook_image, facebook_image_width, facebook_image_height, has_profile)"
					. " VALUES"
					. " ($this->account_id, $this->username, $this->first_name, $this->last_name, $this->nickname, $this->friendly_url, $this->image_id, $this->facebook_image, $this->facebook_image_width, $this->facebook_image_height, $this->has_profile)";

				Self::insertSql($sql);
			}

			$this->prepareToUse();
		}

		function Delete() {
			$dbMain = db_getDBObject(DEFAULT_DB, true);
			if ($this->domain_id) {
				$dbObj = db_getDBObjectByDomainID($this->domain_id, $dbMain);
			} else {
				if (defined("SELECTED_DOMAIN_ID")) {
					$dbObj = db_getDBObjectByDomainID(SELECTED_DOMAIN_ID, $dbMain);
				} else {
					$dbObj = db_getDBObject();
				}
				unset($dbMain);
			}
			$sql = "DELETE FROM AccountProfileContact WHERE account_id = $this->account_id";
			$dbObj->query($sql);
		}
        
           public static function updateSql($sql)
    {
        $data= DB::connection('domain')->update($sql);
        return $data;
    }
    
     public static function fetch($sql)
        {
            $data= DB::connection('domain')->select($sql);
            return $data;
        }
        
        public static function fetchMain($sql)
        {
            $data= DB::select($sql);
            return $data;
        }
        
        public static function insertSql($sql)
        {
            $data= DB::connection('domain')->insert($sql);
            return $data;
        }
        
        public static function deleteSql($sql)
        {
            $data= DB::connection('domain')->delete($sql);
            return $data;
        }
	}
?>