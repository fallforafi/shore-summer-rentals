<?php
namespace App\Functions;
use App\Functions\Setting;
use DB,PDO;
use Session;
use App\Functions\ItemStatus;
use App\Functions\EditorChoice;

	# ----------------------------------------------------------------------------------------------------
	# * FILE: listingChoice.php
	# ----------------------------------------------------------------------------------------------------

	class ListingChoice extends Handle {

		var $id;
		var $editor_choice_id;
		var $account_id;
		var $listing_id;
		var $description;
		var $status;
		var $renewal_date;

		/**
		* Listing Choice
		******************************************************/
		function check_status($getstatus="")
		{
			  $name = Array(config('params.LANG_LABEL_ACTIVE'), config('params.LANG_LABEL_SUSPENDED'),config('params.LANG_LABEL_EXPIRED'), config('params.LANG_LABEL_PENDING'));
               $style = Array("status-active", "status-suspended", "status-expired", "status-pending");

                     $status  = Array("A", "S", "E", "P");
                       $return='';
                  for ($i=0; $i<count($status); $i++) {
                   if($status[$i]==$getstatus)
                    {
                       $return= '<span class="'.$style[$i].'">'.$name[$i].'</span>';
                         break;
                    }
                    else
                    {
                    $return= '<span class="status-pending">Pending</span>';
                    break;

                    }
                    

                }

                return $return;
		}


		function ListingChoice($editor="", $listing="", $id="") {


			if ( is_numeric($id) && ($id) ) {

				$sql  = "SELECT * FROM Listing_Choice WHERE id = $id";
				$row = self::fetch($sql);

				$this->makeFromRow($row);

			} elseif ( (is_numeric($editor) && ($editor)) && (is_numeric($listing) && ($listing)) ) {

			
				$sql  = "SELECT * FROM Listing_Choice";
				$sql .= " WHERE editor_choice_id = $editor";
				$sql .= " AND   listing_id       = $listing";
				$row = self::fetch($sql);
				$this->makeFromRow($row);

			} else {
                if (!is_array($var)) {
                    $var = array();
                }
				$this->arraymakeFromRow($row);
			
			}

		}

	 function SponserListingChoice($editor=0, $listing=0)
     {
        $sql  = "SELECT * FROM Listing_Choice 
        WHERE editor_choice_id = ".$editor." AND listing_id = ".$listing;
        $result=self::fetch($sql);
        return $result;
     }
		/**
		* makeFromRow*/

		function makeFromRow($row="") {

			$status = new ItemStatus();
			$status->ItemStatus();
		  
		  	$this->id				= (isset($row[0]->id))				 ? $row[0]->id				: 0;
			$this->editor_choice_id = (isset($row[0]->editor_choice_id)) ? $row[0]->editor_choice_id: 0;
			$this->account_id       = (isset($row[0]->account_id))       ? $row[0]->account_id      : 0;
			$this->listing_id       = (isset($row[0]->listing_id))       ? $row[0]->listing_id     : 0;
			$this->description      = (isset($row[0]->description))      ? $row[0]->description      : '';
			$this->status			= (isset($row[0]->status))			 ? $row[0]->status			: $status->getDefaultStatus();
			$this->renewal_date		= (isset($row[0]->renewal_date))	 ? $row[0]->renewal_date	: ($this->renewal_date			? $this->renewal_date	: 0);

		}
		function arraymakeFromRow($row="") {

			$status = new ItemStatus();
			$status->ItemStatus();
		  
		  	$this->id				= (isset($row["id"]))				 ? $row["id"]				: 0;
			$this->editor_choice_id = (isset($row["editor_choice_id"])) ? $row["editor_choice_id"] : 0;
			$this->account_id       = (isset($row["account_id"]))       ? $row["account_id"]       : 0;
			$this->listing_id       = (isset($row["listing_id"]))       ? $row["listing_id"]       : 0;
			$this->description      = (isset($row["description"]))      ? $row["description"]      : '';
			$this->status			= (isset($row["status"]))			 ? $row["status"]			: $status->getDefaultStatus();
			$this->renewal_date		= (isset($row["renewal_date"]))	 ? $row["renewal_date"]		: ($this->renewal_date			? $this->renewal_date	: 0);

		}

		/**
		* Save*/
		function Save() {
			$this->prepareToSave();

		
			if ($this->id) {

				$sql = "UPDATE Listing_Choice SET"
					. " status = $this->status,"
					. " renewal_date = $this->renewal_date,"
					. " description = $this->description"
					. " WHERE id = $this->id";
				Self::updateSql($sql);
				
			} else {

				$sql = "INSERT INTO Listing_Choice"
					. " (editor_choice_id,"
					. "  account_id,"
					. "  listing_id,"
					. "  description,"
					. "  status,"
					. "  renewal_date)"
					. " VALUES"
					. " ($this->editor_choice_id,"
					. "  $this->account_id,"
					. "  $this->listing_id,"
					. "  $this->description,"
					. "  $this->status,"
					. "  $this->renewal_date)";
					
				Self::insertSql($sql);
				$this->id = DB::connection('domain')->getpdo()->lastInsertId();


				
			}
			
			$this->prepareToUse();

		}




		/**
		* Delete 
		******************************************************/
		function Delete() {

			$sql  = "DELETE FROM Listing_Choice";
			$sql .= " WHERE listing_id = $this->listing_id";

			$dbMain = db_getDBObject(DEFAULT_DB, true);
			if (defined("SELECTED_DOMAIN_ID")) {
				$dbObj = db_getDBObjectByDomainID(SELECTED_DOMAIN_ID, $dbMain);
			} else {
				$dbObj = db_getDBObject();
			}
			// $dbMain->close();
			unset($dbMain);
			$dbObj->query($sql);

		}

		function updateStatus() {
			$this->prepareToSave();

			$dbMain = db_getDBObject(DEFAULT_DB, true);
			if (defined("SELECTED_DOMAIN_ID")) {
				$dbObj = db_getDBObjectByDomainID(SELECTED_DOMAIN_ID, $dbMain);
			} else {
				$dbObj = db_getDBObject();
			}
			unset($dbMain);
			$sql = "UPDATE Listing_Choice SET"
				. " status = $this->status,"
				. " renewal_date = $this->renewal_date"
				. " WHERE id = $this->id";
			$dbObj->query($sql); 
			
			$this->prepareToUse();
		}
		
		/**
		* Delete Listing Choice that are Available
		*******************************************************/
		function DeleteAvailable() {

			$sql  = "DELETE FROM Listing_Choice";
			$sql .= " WHERE listing_id       = $this->listing_id";
			$sql .= " AND   editor_choice_id = $this->editor_choice_id";

			$dbMain = db_getDBObject(DEFAULT_DB, true);
			if (defined("SELECTED_DOMAIN_ID")) {
				$dbObj = db_getDBObjectByDomainID(SELECTED_DOMAIN_ID, $dbMain);
			} else {
				$dbObj = db_getDBObject();
			}
			// $dbMain->close();
			unset($dbMain);
			$dbObj->query($sql);

		}
		function getBadgesPrice($id=0) {	

			$price = 0;

			$sql = "SELECT * FROM Editor_Choice WHERE id = ".$id;
	  		$result = self::fetch($sql);

			$price = (isset($result[0])?$result[0]->price:0);
			if ($price <= 0) $price = 0;
			return $price;

		}
		function getPrice() {	

			$price = 0;

			//$editorObj = new EditorChoice();
			$editor=$this->getBadgesPrice($this->editor_choice_id);
			
			$price = $editor;

			if ($price <= 0) $price = 0;
			return $price;

		}

		function hasRenewalDate() {
			if (config('params.PAYMENT_FEATURE') != "on") return false;
			if ((config('params.CREDITCARDPAYMENT_FEATURE') != "on") && (config('params.INVOICEPAYMENT_FEATURE') != "on") && (config('params.MANUALPAYMENT_FEATURE') != "on")) return false;
			if ($this->getPrice() <= 0) return false;
			return true;
		}

		function needToCheckOut() {



			if ($this->hasRenewalDate()) {

				$today = date("Y-m-d");
				$today = explode("-", $today);
				$today_year = $today[0];
				$today_month = $today[1];
				$today_day = $today[2];
				$timestamp_today = mktime(0, 0, 0, $today_month, $today_day, $today_year);

				$this_renewaldate = $this->renewal_date;
				$renewaldate = explode("-", $this_renewaldate);
				$renewaldate_year = $renewaldate[0];
				$renewaldate_month = $renewaldate[1];
				$renewaldate_day = $renewaldate[2];
				$timestamp_renewaldate = mktime(0, 0, 0, $renewaldate_month, $renewaldate_day, $renewaldate_year);

				if (($this->status == "E") || ($this_renewaldate == "0000-00-00") || ($timestamp_today > $timestamp_renewaldate)) {
					return true;
				}

			}

			return false;

		}

		function getNextRenewalDate($times = 1) {

			$nextrenewaldate ="0000-00-00";

			if ($this->hasRenewalDate()) {

				if ($this->needToCheckOut()) {

					$today = date("Y-m-d");
					$today = explode("-", $today);
					$start_year = $today[0];
					$start_month = $today[1];
					$start_day = $today[2];

				} else {

					$this_renewaldate = $this->renewal_date;
					$renewaldate = explode("-", $this_renewaldate);
					$start_year = $renewaldate[0];
					$start_month = $renewaldate[1];
					$start_day = $renewaldate[2];

				}

					$today = date("Y-m-d");
					$today = explode("-", $today);
					$start_year = $today[0];
					$start_month = $today[1];
					$start_day = $today[2];

				$editorObj = new EditorChoice();
				$editor=$editorObj->EditorChoicePrice($this->editor_choice_id);

				
				$renewalcycle = $editor[0]->renewal_period_value;
				$renewalunit = $editor[0]->renewal_period[0];
				//d($renewalunit[0],1);die;
				if ($renewalunit == "y") {
					$nextrenewaldate = date("Y-m-d", mktime(0, 0, 0, (int)$start_month, (int)$start_day, (int)$start_year+($renewalcycle*$times)));
				} elseif ($renewalunit == "m") {
					$nextrenewaldate = date("Y-m-d", mktime(0, 0, 0, (int)$start_month+($renewalcycle*$times), (int)$start_day, (int)$start_year));
				} elseif ($renewalunit == "d") {
					$nextrenewaldate = date("Y-m-d", mktime(0, 0, 0, (int)$start_month, (int)$start_day+($renewalcycle*$times), (int)$start_year));
				}

			}
			return $nextrenewaldate;

		}

		function hasSpecials( $listing_id ) {

			/*$dbMain = db_getDBObject(DEFAULT_DB, true);
			if ($this->domain_id){
				$dbObj = db_getDBObjectByDomainID($this->domain_id, $dbMain);
			}else if (defined("SELECTED_DOMAIN_ID")) {
				$dbObj = db_getDBObjectByDomainID(SELECTED_DOMAIN_ID, $dbMain);
			} else {
				$dbObj = db_getDBObject();
			}
			unset($dbMain);*/

			$sql  = "SELECT id FROM Listing_Choice WHERE listing_id = $listing_id AND status = 'A' LIMIT 1";
			$result = self::fetch($sql);
			
			if ( $result[0]->id > 0 ) return true; 
			else return false;
		}


		public static function updateSql($sql)
    {
        $data= DB::connection('domain')->update($sql);
        return $data;
    }
    
    public static function updateSqlMain($sql)
    {
        $data= DB::update($sql);
        return $data;
    }
    
    
     public static function fetch($sql)
        {
            $data= DB::connection('domain')->select($sql);
            return $data;
        }
        
        public static function fetchMain($sql)
        {
            $data= DB::select($sql);
            return $data;
        }
        
        public static function insertSql($sql)
        {
            $data= DB::connection('domain')->insert($sql);
            return $data;
        }
        public static function insertSqlMain($sql)
        {
            $data= DB::insert($sql);
            return $data;
        }
        
        
        public static function deleteSql($sql)
        {
            $data= DB::connection('domain')->delete($sql);
            return $data;
        }
        
        public static function deleteSqlMain($sql)
        {
            $data= DB::delete($sql);
            return $data;
        }
	
	}

?>
