<?php
namespace App\Functions;
use App\Functions\Setting;
use DB;
use Session;

	/*
	\*==================================================================*/

	# ----------------------------------------------------------------------------------------------------
	# * FILE: /classes/class_RateInformationInformation.php
	# ----------------------------------------------------------------------------------------------------

	/**
	 * <code>
	 *		$rateInformationObj = new RateInformation($id);
	 * <code>
	 * @version 8.0.00
	 * @package Classes
	 * @name RateInformation
	 * @method RateInformation
	 * @method makeFromRow
	 * @method Save
	 * @method Delete
	 * @access Public
	 */

	class RateInformation extends Handle {

		/**
		 * @var integer
		 * @access Private
		 */
		// var $id;
		/**
		 * @var integer
		 * @access Private
		 */
		var $listing_id;
		/**
		 * @var string
		 * @access Private
		 */
		var $checkin;
		/**
		 * @var string
		 * @access Private
		 */
		var $checkout;
		/**
		 * @var string
		 * @access Private
		 */
		var $payment_types;
		/**
		 * @var string
		 * @access Private
		 */
		var $payment_terms;
		/**
		 * @var string
		 * @access Private
		 */
		var $cancellation_policy;
		/**
		 * @var string
		 * @access Private
		 */
		var $security_deposit;
		/**
		 * @var string
		 * @access Private
		 */
		var $notes;

		/**
		 * <code>
		 *		$rateInformationObj = new RateInformation($id);
		 * <code>
		 * @version 8.0.00
		 * @name RateInformation
		 * @access Public
		 * @param integer $var
		 */
		function RateInformation($var='') {
			
			if (is_numeric($var) && ($var)) {
				
				$sql = "SELECT * FROM RateInformation WHERE listing_id = $var";
				$row = self::fetch($sql);

			if(!empty($row))
                {
                    $this->old_account_id = $row[0]->account_id;

				    $this->makeFromRow($row);
                }
                else
                {
                    return false;
                }
				
			} else {

                if (!is_array($var)) {
                    $var = array();
                }
				$this->makeFromRow($var);
			}
		}

		/**
		 * <code>
		 *		$this->makeFromRow($row);
		 * <code>
		 * @copyright Copyright 2005 Arca Solutions, Inc.
		 * @author Arca Solutions, Inc.
		 * @version 8.0.00
		 * @name makeFromRow
		 * @access Public
		 * @param array $row
		 */
		function makeFromRow($row='') {

			// $this->id					= ($row["id"])					? $row["id"]					: ($this->id			? $this->id				: 0);
			$this->listing_id 			= ($row[0]["listing_id"]) 			? $row[0]["listing_id"] 			: ($this->listing_id	? $this->listing_id		: 0);
			$this->checkin            	= ($row[0]["checkin"])           	? $row[0]["checkin"]         		: ($this->checkin		? $this->checkin		: "");
			$this->checkout            	= ($row[0]["checkout"])           	? $row[0]["checkout"]         		: ($this->checkout		? $this->checkout		: "");
			$this->payment_types        = ($row[0]["payment_types"])       ? $row[0]["payment_types"]         : "";
			$this->payment_terms        = ($row[0]["payment_terms"])       ? $row[0]["payment_terms"]         : "";
			$this->cancellation_policy  = ($row[0]["cancellation_policy"]) ? $row[0]["cancellation_policy"]   : "";
			$this->security_deposit   	= ($row[0]["security_deposit"])  	? $row[0]["security_deposit"]    	: "";
			$this->notes   				= ($row[0]["notes"])         		? $row[0]["notes"]					: "";

		}

		/**
		 * <code>
		 *		//Using this in forms or other pages.
		 *		$rateInformationObj->Save();
		 * <br /><br />
		 *		//Using this in RateInformation() class.
		 *		$this->Save();
		 * </code>
		 * @copyright Copyright 2005 Arca Solutions, Inc.
		 * @author Arca Solutions, Inc.
		 * @version 8.0.00
		 * @name Save
		 * @access Public
		 */
		function Save() {

			//$this->prepareToSave();

				$sql = "REPLACE INTO RateInformation (
							listing_id,
							checkin,
							checkout,
							payment_types,
							payment_terms,
							cancellation_policy,
							security_deposit,
							notes
						) VALUES (
							$this->listing_id,
							'$this->checkin',
							'$this->checkout',
							'$this->payment_types',
							'$this->payment_terms',
							'$this->cancellation_policy',
							'$this->security_deposit',
							'$this->notes'
						)";
				// d($sql,1);die;    
               Self::insertSql($sql);

				
                
                // $this->id = mysql_insert_id($dbObj->link_id);

			// }

			$this->prepareToUse();
		}

		/**
		 * <code>
		 *		//Using this in forms or other pages.
		 *		$rateInformationObj->Delete();
		 * <code>
		 * @copyright Copyright 2005 Arca Solutions, Inc.
		 * @author Arca Solutions, Inc.
		 * @version 8.0.00
		 * @name Delete
		 * @access Public
		 * @param integer $domain_id
		 */
		function Delete($domain_id = false) {

			$dbMain = db_getDBObject(DEFAULT_DB, true);
			if ($domain_id) {
				$dbObj = db_getDBObjectByDomainID($domain_id, $dbMain);
			} else {
				if (defined("SELECTED_DOMAIN_ID")) {
					$dbObj = db_getDBObjectByDomainID(SELECTED_DOMAIN_ID, $dbMain);
				} else {
					$dbObj = db_getDBObject();
				}
				unset($dbMain);
			}

			$sql = "DELETE FROM RateInformation WHERE listing_id = $this->listing_id";
			$dbObj->query($sql);
		}

		public static function updateSql($sql)
    {
        $data= DB::connection('domain')->update($sql);
        return $data;
    }
    
    public static function updateSqlMain($sql)
    {
        $data= DB::update($sql);
        return $data;
    }
    
    
     public static function fetch($sql)
        {
            $data= DB::connection('domain')->select($sql);
            return $data;
        }
        
        public static function fetchMain($sql)
        {
            $data= DB::select($sql);
            return $data;
        }
        
        public static function insertSql($sql)
        {
            $data= DB::connection('domain')->insert($sql);
            return $data;
        }
        public static function insertSqlMain($sql)
        {
            $data= DB::insert($sql);
            return $data;
        }
        
        
        public static function deleteSql($sql)
        {
            $data= DB::connection('domain')->delete($sql);
            return $data;
        }
        
        public static function deleteSqlMain($sql)
        {
            $data= DB::delete($sql);
            return $data;
        }
    }
?>