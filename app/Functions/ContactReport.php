<?php
namespace App\Functions;
use DB;
use App\Functions\Functions;
use App\Models\Domain\Sql;
use App\Models\Main\MainSql;

	/*==================================================================*\
	######################################################################
	#                                                                    #
	#                                                                    #
	# This file may not be redistributed in whole or part.               #
	# eDirectory is licensed on a per-domain basis.                      #
	#                                                                    #
	# ---------------- eDirectory IS NOT FREE SOFTWARE ----------------- #
	#                                                                    #
	# http://www.edirectory.com | http://www.edirectory.com/license.html #
	######################################################################
	\*==================================================================*/

	# ----------------------------------------------------------------------------------------------------
	# * FILE: /classes/class_ContactReport.php
	# ----------------------------------------------------------------------------------------------------

	class ContactReport extends Handle {

		/**
		 * @var integer
		 * @access Private
		 */
		var $id;

		/**
		 * @var varchar
		 * @access Private
		 */
		var $module;

		/**
		 * @var integer
		 * @access Private
		 */
		var $item_id;

		/**
		 * @var date
		 * @access Private
		 */
		var $date;

		/**
		 * @var varchar
		 * @access Private
		 */
		var $contact_name;

		/**
		 * @var varchar
		 * @access Private
		 */
		var $contact_mail;							


		function ContactReport($var='') {

			if (is_numeric($var) && ($var)) {
				
				$sql = "SELECT * FROM ContactReport WHERE id = $var";

				$row = Sql::fetch($sql);


				$this->old_account_id = $row[0]->account_id;

				$this->makeFromRow($row);
			} else {
                if (!is_array($var)) {
                    $var = array();
                }
				$this->makeFromRow($var);
			}

		}


		function makeFromRow($row='') {

			$this->id				= ($row["id"])				? $row["id"]				: ($this->id			? $this->id				: 0);
			$this->module			= ($row["module"])			? $row["module"]			: ($this->module		? $this->module			: '');
			$this->item_id			= ($row["item_id"])			? $row["item_id"]			: ($this->item_id		? $this->item_id		: 0);
			$this->date				= ($row["date"])			? $row["date"]				: ($this->date			? $this->date			: '0000-00-00 00:00:00');
			$this->contact_name		= ($row["contact_name"])	? $row["contact_name"]		: ($this->contact_name	? $this->contact_name	: '');
			$this->contact_mail		= ($row["contact_mail"])	? $row["contact_mail"]		: ($this->contact_mail	? $this->contact_mail	: '');
			
			$this->data_in_array = $row;

		}

		/**
		 * <code>
		 *		//Using this in forms or other pages.
		 *		$listingObj->Save();
		 * <br /><br />
		 *		//Using this in Listing() class.
		 *		$this->Save();
		 * </code>
		 * @copyright Copyright 2005 Arca Solutions, Inc.
		 * @author Arca Solutions, Inc.
		 * @version 8.0.00
		 * @name Save
		 * @access Public
		 */
		function Save() {


			$this->prepareToSave();

			if ($this->id) {

				$sql = "UPDATE ContactReport SET"
					. " module         = $this->module,"
					. " item_id        = $this->item_id,"
					. " date           = $this->date,"
					. " contact_name   = $this->contact_name,"
					. " contact_mail   = $this->contact_mail"
					. " WHERE id       = $this->id";

				Sql::insertSql($sql);

			} else {
                
				$sql = "INSERT INTO ContactReport"
					. " (module,"
					. " item_id,"
					. " date,"
					. " contact_name,"
					. " contact_mail)"
					. " VALUES"
					. " ($this->module,"
					. " $this->item_id,"
					. " NOW(),"
					. " $this->contact_name,"
					. " $this->contact_mail)";
				
				Sql::insertSql($sql);
				$this->id = DB::getpdo()->lastInsertId();
				
				####################################################################################
				## domain_updateDashboard("number_listings","inc", 0, $aux_log_domain_id);		  ##
				####################################################################################
			}

            $this->prepareToUse();

		}


		function Delete($domain_id = false, $update_count = true) {
			$dbMain = db_getDBObject(DEFAULT_DB, true);
			if ($domain_id) {
				$dbObj = db_getDBObjectByDomainID($domain_id, $dbMain);
			} else {
				if (defined("SELECTED_DOMAIN_ID")) {
					$dbObj = db_getDBObjectByDomainID(SELECTED_DOMAIN_ID, $dbMain);
				} else {
					$dbObj = db_getDBObject();
				}
				unset($dbMain);
			}

			$sql = "DELETE FROM ContactReport_Info WHERE contact_report_id = $this->id";
			$dbObj->query($sql);

			### LISTING
			$sql = "DELETE FROM ContactReport WHERE id = $this->id";
			$dbObj->query($sql);

			if ($domain_id){
				$domain_idDash = $domain_id;
			} else {
				$domain_idDash = SELECTED_DOMAIN_ID;
			}

			//domain_updateDashboard("number_listings", "dec", 0, $domain_idDash);

			//activity_deleteRecord($domain_idDash, $this->id, "listing");

		}

		function getCount() {
			$dbMain = db_getDBObject(DEFAULT_DB, true);
			if ($domain_id) {
				$dbObj = db_getDBObjectByDomainID($domain_id, $dbMain);
			} else {
				if (defined("SELECTED_DOMAIN_ID")) {
					$dbObj = db_getDBObjectByDomainID(SELECTED_DOMAIN_ID, $dbMain);
				} else {
					$dbObj = db_getDBObject();
				}
				unset($dbMain);
			}
			$sql = "SELECT COUNT(*) as count FROM ContactReport";
			$result = $dbObj->query($sql);
			if ($result) {
				$return = mysql_fetch_assoc($result);
				return $return['count'];
			} else {
				return false;
			}	

		}

		function saveInfo($arrayInfo = array()) {
			$dbMain = db_getDBObject(DEFAULT_DB, true);
			if ($domain_id) {
				$dbObj = db_getDBObjectByDomainID($domain_id, $dbMain);
			} else {
				if (defined("SELECTED_DOMAIN_ID")) {
					$dbObj = db_getDBObjectByDomainID(SELECTED_DOMAIN_ID, $dbMain);
				} else {
					$dbObj = db_getDBObject();
				}
				unset($dbMain);
			}

			$sql = "INSERT INTO `ContactReport_Info` (
					`contact_report_id`, 
					`first_name`, 
					`last_name`, 
					`email`, 
					`phone`, 
					`arrive`, 
					`depart`, 
					`adults`, 
					`children`, 
					`is_flexible`, 
					`message`) 
				VALUES (
					" . $this->id . ", 
					" . db_formatString($_POST['first_name']) . ", 
					" . db_formatString($_POST['last_name']) . ", 
					" . db_formatString($_POST['from']) . ", 
					" . db_formatString($_POST['phone_number']) . ", 
					" . db_formatDate($_POST['arrive']) . ", 
					" . db_formatDate($_POST['depart']) . ", 
					" . db_formatString($_POST['adults']) . ", 
					" . db_formatString($_POST['children']) . ", 
					" . (isset($_POST['flexible_date']) && $_POST['flexible_date'] == "y" ? "'y'" : "'n'") . ", 
					" . db_formatString($_POST['body']) . ");";
			$dbObj->query($sql);
		}

		function getContactsByListingId($listing_id) {
			$dbMain = db_getDBObject(DEFAULT_DB, true);
			if ($domain_id) {
				$dbObj = db_getDBObjectByDomainID($domain_id, $dbMain);
			} else {
				if (defined("SELECTED_DOMAIN_ID")) {
					$dbObj = db_getDBObjectByDomainID(SELECTED_DOMAIN_ID, $dbMain);
				} else {
					$dbObj = db_getDBObject();
				}
				unset($dbMain);
			}

			$sql = "SELECT * FROM ContactReport WHERE item_id = " . $listing_id;
			$result = $dbObj->query($sql);
			if ($result) {
				while($row = mysql_fetch_assoc($result)) {
					$return[] = $row;
				}
				return $return;
			} else {
				return false;
			}	
		}

		function getInfo() {
			$dbMain = db_getDBObject(DEFAULT_DB, true);
			if ($domain_id) {
				$dbObj = db_getDBObjectByDomainID($domain_id, $dbMain);
			} else {
				if (defined("SELECTED_DOMAIN_ID")) {
					$dbObj = db_getDBObjectByDomainID(SELECTED_DOMAIN_ID, $dbMain);
				} else {
					$dbObj = db_getDBObject();
				}
				unset($dbMain);
			}

			$sql = "SELECT * FROM ContactReport_Info WHERE contact_report_id = " . $this->id . " LIMIT 1";
			$result = $dbObj->query($sql);

			$row = mysql_fetch_assoc($result);

			return $row;
		}

	}
?>