<?php
namespace App\Functions;
 use DB;

	/*==================================================================*\
	######################################################################
	#                                                                    #
	# Copyright 2005 Arca Solutions, Inc. All Rights Reserved.           #
	#                                                                    #
	# This file may not be redistributed in whole or part.               #
	# eDirectory is licensed on a per-domain basis.                      #
	#                                                                    #
	# ---------------- eDirectory IS NOT FREE SOFTWARE ----------------- #
	#                                                                    #
	# http://www.edirectory.com | http://www.edirectory.com/license.html #
	######################################################################
	\*==================================================================*/

	# ----------------------------------------------------------------------------------------------------
	# * FILE: /classes/class_Listing_Category.php
	# ----------------------------------------------------------------------------------------------------

	class Listing_Category extends Handle {

		var $id;
		var $listing_id;
		var $category_id;
		var $status;
		var $category_root_id;
		var $category_node_left;
		var $category_node_right;
		
		/*
		 * Dont save this field
		 */
		var $total_listings;
		
		function Listing_Category($var='') {

			if (is_numeric($var) && ($var)) {
				
				$sql = "SELECT * FROM Listing_Category WHERE id = $var";
				$row = (Self::fetchSql($sql));
				$this->makeFromRow($row);
			} else {
                if (!is_array($var)) {
                    $var = array();
                }
				$this->arraymakeFromRow($var);
			}
		}

		function makeFromRow($row='') {

			if ($row[0]->id) $this->id = $row[0]->id;
			else if (!$this->id) $this->id = 0;
			if ($row[0]->listing_id) $this->listing_id = $row[0]->listing_id;
			else if (!$this->listing_id) $this->listing_id = 0;
			if ($row[0]->category_id) $this->category_id = $row[0]->category_id;
			else if (!$this->category_id) $this->category_id = 0;
			if ($row[0]->status) $this->status = $row[0]->status;
			else if (!$this->status) $this->status = "";
			if ($row[0]->category_root_id) $this->category_root_id = $row[0]->category_root_id;
			else if (!$this->category_root_id) $this->category_root_id = 0;
			if ($row[0]->category_node_left) $this->category_node_left = $row[0]->category_node_left;
			else if (!$this->category_node_left) $this->category_node_left = 0;
			if ($row[0]->category_node_right) $this->category_node_right = $row[0]->category_node_right;
			else if (!$this->category_node_right) $this->category_node_right = 0;
		}
		function arraymakeFromRow($row='') {

			if (isset($row['id'])) $this->id = $row['id'];
			else if (!$this->id) $this->id = 0;
			if (isset($row['listing_id'])) $this->listing_id = $row['listing_id'];
			else if (!$this->listing_id) $this->listing_id = 0;
			if (isset($row['category_id'])) $this->category_id = $row['category_id'];
			else if (!$this->category_id) $this->category_id = 0;
			if (isset($row['status'])) $this->status = $row['status'];
			else if (!$this->status) $this->status = "";
			if (isset($row['category_root_id'])) $this->category_root_id = $row['category_root_id'];
			else if (!$this->category_root_id) $this->category_root_id = 0;
			if (isset($row['category_node_left'])) $this->category_node_left = $row['category_node_left'];
			else if (!$this->category_node_left) $this->category_node_left = 0;
			if (isset($row['category_node_right'])) $this->category_node_right = $row['category_node_right'];
			else if (!$this->category_node_right) $this->category_node_right = 0;
		}

		function Save() {
			//$insert_password = $this->password;
			//$this->prepareToSave();
			
			if ($this->id && $this->id>0) { 
				$sql  = "UPDATE Listing_Category SET"
					. " listing_id = $this->listing_id,"
					. " category_id = $this->category_id,"
					. " status = '$this->status',"
					. " category_root_id = $this->category_root_id,"
					. " category_node_left = $this->category_node_left,"
					. " category_node_right = $this->category_node_right,"
					. " WHERE id = $this->id";
				Self::updateSql($sql);
			} else {
				 $sql = "INSERT INTO Listing_Category"
					. " (listing_id, category_id, status, category_root_id, category_node_left, category_node_right)"
					. " VALUES"
					. " ($this->listing_id, $this->category_id, '$this->status', $this->category_root_id, $this->category_node_left, $this->category_node_right)";
					
				$res=self::insertSql($sql);
				$this->id = DB::getpdo()->lastInsertId();
			}
			$this->prepareToUse();
		}

		function Delete() {
			/**
			* Deleting this object
			**/
			$dbMain = db_getDBObject(DEFAULT_DB, true);
			if (defined("SELECTED_DOMAIN_ID")) {
				$dbObj = db_getDBObjectByDomainID(SELECTED_DOMAIN_ID, $dbMain);
			} else {
				$dbObj = db_getDBObject();
			}
			unset($dbMain);;
			$sql = "DELETE FROM Listing_Category WHERE id = $this->id";
			$dbObj->query($sql);
		}

		function getListings($category_id){

			$dbMain = db_getDBObject(DEFAULT_DB, true);
			if (defined("SELECTED_DOMAIN_ID")) {
				$dbObj = db_getDBObjectByDomainID(SELECTED_DOMAIN_ID, $dbMain);
			} else {
				$dbObj = db_getDBObject();
			}
			unset($dbMain);
			$sql = "SELECT DISTINCT listing_id FROM Listing_Category use index (category_status) WHERE category_id IN (".$category_id.") AND status = 'A'";

			$result = $dbObj->query($sql);
			$lines = mysql_num_rows($result);
			
			/*
			 * Total of listings
			 */
			$this->total_listings = $lines;
			unset($string_listings);
			if($lines > 0){
				$string_listings = "";
				while($row = mysql_fetch_assoc($result)){
					$lines--;
					$string_listings .= $row["listing_id"].($lines > 0 ? "," : "");
				}
			}
			
			if($string_listings){
				return $string_listings;
			}else{
				return 0;
			}
		}
		
		
		function getListingsByCategoryHierarchy($root_id, $left, $right, $letter = false){

			$dbMain = db_getDBObject(DEFAULT_DB, true);
			if (defined("SELECTED_DOMAIN_ID")) {
				$dbObj = db_getDBObjectByDomainID(SELECTED_DOMAIN_ID, $dbMain);
			} else {
				$dbObj = db_getDBObject();
			}
			unset($dbMain);

			$sql = "SELECT Listing_Category.listing_id
						FROM Listing_Category Listing_Category
						WHERE Listing_Category.category_root_id = ".$root_id." AND
							  Listing_Category.category_node_left >= ".$left." AND
							  Listing_Category.category_node_right <= ".$right." AND
		  					  Listing_Category.status = 'A'";
			
			$result = $dbObj->query($sql);
			$lines = mysql_num_rows($result);

			/*
			 * Total of listings
			 */
			$aux_count_listings = 0;
			$aux_listing_id = 0;

			unset($string_listings);
			if($lines > 0){
				$string_listings = "";
				while($row = mysql_fetch_assoc($result)){
					$lines--;
					if($row["listing_id"] != $aux_listing_id){
						$string_listings .= $row["listing_id"].($lines > 0 ? "," : "");
						$aux_count_listings++;
						$aux_listing_id = $row["listing_id"];
					}
				}
				$this->total_listings = $aux_count_listings;
			}

			if (string_substr($string_listings, -1) == ",") {
				$string_listings = string_substr($string_listings, 0, -1);
			}

			if ($letter) {
                if (!$string_listings) {
                    $string_listings = "0";
                }
				$sql = "SELECT id FROM Listing_Summary WHERE id IN ($string_listings) AND title LIKE ".db_formatString($letter."%");
				$result = $dbObj->query($sql);
				$count = mysql_num_rows($result);
				$this->total_listings = $count;
			}
            
			if ($string_listings) {
				return $string_listings;
			} else {
				return 0;
			}

		}
        
        function getCategoriesByListingID($listing_id){
            
            $dbMain = db_getDBObject(DEFAULT_DB, true);
			if (defined("SELECTED_DOMAIN_ID")) {
				$dbObj = db_getDBObjectByDomainID(SELECTED_DOMAIN_ID, $dbMain);
			} else {
				$dbObj = db_getDBObject();
			}
			unset($dbMain);
            
            $sql = "SELECT ListingCategory.title , 
                           Listing_Category.category_id
                      FROM Listing_Category Listing_Category
                        inner join ListingCategory ListingCategory on ListingCategory.id = Listing_Category.category_id
                     WHERE Listing_Category.listing_id =".$listing_id." order by ListingCategory.title";
            $result = $dbObj->query($sql);
            if(mysql_num_rows($result)){
                $i=0;
                while($row = mysql_fetch_assoc($result)){
                    $categories_array[$i][(API_IN_USE == "api2" ? "category_id" : "id")] = $row["category_id"];
                    $categories_array[$i]["name"] = $row["title"];
                    $i++;
                }
                return $categories_array;
            }else{
                return false;
            }
            
            
        }

     public static function fetch($sql)
        {
            $data= DB::select($sql);
            return $data;
        }
        
        public static function fetchMain($sql)
        {
            $data= DB::select($sql);
            return $data;
        }
        
        public static function insertMainSql($sql)
        {
            $data= DB::insert($sql);
            return $data;
        }
        
        public static function updateSql($sql)
        {
            $data= DB::update($sql);
            return $data;
        }
        
        
        
         public static function fetchMainSql($sql)
        {
            $data= DB::select($sql);
            return $data;
        }
        
        public static function fetchSql($sql)
        {
            $data= DB::connection('domain')->select($sql);
            return $data;
        }
        
        public static function insertSql($sql)
        {
            $data= DB::connection('domain')->insert($sql);
            return $data;
        }
        
        public static function updateSqlSql($sql)
        {
            $data= DB::connection('domain')->update($sql);
            return $data;
        }
        
	}

?>
