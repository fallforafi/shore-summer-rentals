<?php
namespace App\Functions;
use DB;
use App\Functions\Functions;
use App\Models\Domain\Sql;
use App\Models\Main\MainSql;
	/*==================================================================*\
	######################################################################
	#                                                                    #
	#           #
	#                                                                    #
	# This file may not be redistributed in whole or part.               #
	# eDirectory is licensed on a per-domain basis.                      #
	#                                                                    #
	# ---------------- eDirectory IS NOT FREE SOFTWARE ----------------- #
	#                                                                    #
	######################################################################
	\*==================================================================*/

	# ----------------------------------------------------------------------------------------------------
	# * FILE: /classes/class_InvoiceListingChoice.php
	# ----------------------------------------------------------------------------------------------------

	/**
	 * <code>
	 *		$invoiceListingChoiceObj = new InvoiceListingChoice($id);
	 * <code>
	 * @package Classes
	 * @name InvoiceListingChoice
	 * @method InvoiceListingChoice
	 * @method makeFromRow
	 * @method Save
	 * @access Public
	 */
	class InvoiceListingChoice extends Handle {

		/**
		 * @var integer
		 * @access Private
		 */
		var $invoice_id;
		/**
		 * @var integer
		 * @access Private
		 */
		var $listingchoice_id;
		/**
		 * @var string
		 * @access Private
		 */
		var $badge_name;
		/**
		 * @var string
		 * @access Private
		 */
		var $listing_title;
		/**
		 * @var date
		 * @access Private
		 */
		var $renewal_date;
		/**
		 * @var real
		 * @access Private
		 */
		var $amount;

		/**
		 * <code>
		 *		$invoiceListingChoiceObj = new InvoiceListingChoice($id);
		 * <code>
		 * @copyright Copyright 2005 Arca Solutions, Inc.
		 * @author Arca Solutions, Inc.
		 * @version 8.0.00
		 * @name InvoiceListingChoice
		 * @access Public
		 * @param integer $var
		 */
		function InvoiceListingChoice($var="") {
			if (is_array($var) && ($var))
				$this->makeFromRow($var);
		}

		/**
		 * <code>
		 *		$this->makeFromRow($row);
		 * <code>
		 * @copyright Copyright 2005 Arca Solutions, Inc.
		 * @author Arca Solutions, Inc.
		 * @version 8.0.00
		 * @name makeFromRow
		 * @access Public
		 * @param array $row
		 */
		function makeFromRow($row="") {
			$this->invoice_id		= ($row["invoice_id"])			? $row["invoice_id"]		: ($this->invoice_id		? $this->invoice_id			: 0);
			$this->listingchoice_id	= ($row["listingchoice_id"])	? $row["listingchoice_id"]	: ($this->listingchoice_id	? $this->listingchoice_id	: 0);
			$this->badge_name		= ($row["badge_name"])			? $row["badge_name"]		: ($this->badge_name		? $this->badge_name			: "");
			$this->listing_title	= ($row["listing_title"])		? $row["listing_title"]		: ($this->listing_title		? $this->listing_title		: "");
			$this->renewal_date		= ($row["renewal_date"])		? $row["renewal_date"]		: ($this->renewal_date		? $this->renewal_date		: 0);
			$this->amount			= ($row["amount"])				? $row["amount"]			: ($this->amount			? $this->amount				: 0);
		}

		/**
		 * <code>
		 *		//Using this in forms or other pages.
		 *		$invoiceListingChoiceObj->Save();
		 * <br /><br />
		 *		//Using this in InvoiceListingChoice() class.
		 *		$this->Save();
		 * </code>
		 * @copyright Copyright 2005 Arca Solutions, Inc.
		 * @author Arca Solutions, Inc.
		 * @version 8.0.00
		 * @name Save
		 * @access Public
		 */
		function Save() {

			//$this->PrepareToSave();


			$sql = "INSERT INTO Invoice_ListingChoice"
				. " (invoice_id,"
				. " listingchoice_id,"
				. " badge_name,"
				. " listing_title,"
                . " renewal_date,"
				. " amount"
				. " )"
				. " VALUES"
				. " ("
				. " $this->invoice_id,"
				. " $this->listingchoice_id,"
				. " '$this->badge_name',"
				. " '$this->listing_title',"
                . " '$this->renewal_date',"
				. " $this->amount"
				. " )";

			Sql::insertSql($sql);

			$this->PrepareToUse();

		}

	}
?>