<?php 
namespace App\Functions;
use App\Functions\Functions;
use DB;

	/*==================================================================*\
	######################################################################
	#                                                                    #
	# Copyright 2005 Arca Solutions, Inc. All Rights Reserved.           #
	#                                                                    #
	# This file may not be redistributed in whole or part.               #
	# eDirectory is licensed on a per-domain basis.                      #
	#                                                                    #
	# ---------------- eDirectory IS NOT FREE SOFTWARE ----------------- #
	#                                                                    #
	# http://www.edirectory.com | http://www.edirectory.com/license.html #
	######################################################################
	\*==================================================================*/

	# ----------------------------------------------------------------------------------------------------
	# * FILE: /classes/class_contact.php
	# ----------------------------------------------------------------------------------------------------

	class Contact extends Handle {

		var $account_id;
		var $updated;
		var $entered;
		var $first_name;
		var $last_name;
		var $company;
		var $address;
		var $address2;
		var $city;
		var $state;
		var $zip;
		var $country;
		var $phone;
		var $fax;
		var $email;
		var $url;

		function Contact($var='') {
			if (is_numeric($var) && ($var)) {
				
				$sql = "SELECT * FROM Contact WHERE account_id = $var";
				$row = Self::fetchMain($sql);
				$this->makeFromRow($row);
			}
			else {
                if (!is_array($var)) {
                    $var = array();
                }
				$this->arrayMakeFromRow($var);
			}
		}

		function makeFromRow($row='') {
			
			// fixing user url field if needed.
			if (trim($row[0]->url) != "" && $row[0]->url_protocol) {
				if (Functions::string_strpos($row[0]->url, "://") !== false) {
					$aux_url = explode("://", $row[0]->url);
					$aux_url = $aux_url[1];
					$row[0]->url = $aux_url;
				}
				$row[0]->url = $row[0]->url_protocol . $row[0]->url;
			}

			if ($row[0]->account_id) $this->account_id = $row[0]->account_id;
			else if (!$this->account_id) $this->account_id = 0;
			if ($row[0]->entered) $this->entered = $row[0]->entered;
			else if (!$this->entered) $this->entered = 0;
			if ($row[0]->updated) $this->updated = $row[0]->updated;
			else if (!$this->updated) $this->updated = 0;

			$this->first_name = $row[0]->first_name;
			$this->last_name  = $row[0]->last_name;
			$this->company    = $row[0]->company;
			$this->address    = $row[0]->address;
			$this->address2   = $row[0]->address2;
			$this->city       = $row[0]->city;
			$this->state      = $row[0]->state;
			$this->zip        = $row[0]->zip;
			$this->country    = $row[0]->country;
			$this->phone      = $row[0]->phone;
			$this->fax        = $row[0]->fax;
			$this->email      = $row[0]->email;
			$this->url        = $row[0]->url;

		}

		function arrayMakeFromRow($row='') {		
			// fixing user url field if needed.
			if (trim(isset($row["url"])) != "" && isset($row["url_protocol"])) {
				if (Functions::string_strpos($row["url"], "://") !== false) {
					$aux_url = explode("://", $row["url"]);
					$aux_url = $aux_url[1];
					$row["url"] = $aux_url;
				}
				$row["url"] = $row["url_protocol"] . $row["url"];
			}

			if (isset($row['account_id'])) $this->account_id = $row['account_id'];
			else if (!$this->account_id) $this->account_id = 0;
			if (isset($row['entered'])) $this->entered = $row['entered'];
			else if (!$this->entered) $this->entered = 0;
			if (isset($row['updated'])) $this->updated = $row['updated'];
			else if (!$this->updated) $this->updated = 0;

			$this->first_name = (isset($row['first_name'])?$row['first_name']:'');
			$this->last_name  = (isset($row['last_name'])?$row['last_name']:'');
			$this->company    = (isset($row['company'])?$row['company']:'');
			$this->address    = (isset($row['address'])?$row['address']:'');
			$this->address2   = (isset($row['address2'])?$row['address2']:'');
			$this->city       = (isset($row['city'])?$row['city']:'');
			$this->state      = (isset($row['state'])?$row['state']:'');
			$this->zip        = (isset($row['zip'])?$row['zip']:'');
			$this->country    = (isset($row['country'])?$row['country']:'');
			$this->phone      = (isset($row['phone'])?$row['phone']:'');
			$this->fax        = (isset($row['fax'])?$row['fax']:'');
			$this->email      = (isset($row['email'])?$row['email']:'');
			$this->url        = (isset($row['url'])?$row['url']:'');

		}

		function Save() {

			$this->prepareToSave();

			$res=DB::table('Contact')->where('account_id',$this->account_id)->get();
			if(count($res)>0)
			{
				$acc=$res[0]->account_id;
			}
			else
			{
				$acc='';
			}
			if($this->account_id==$acc)
			{
				$sql  = "UPDATE Contact SET"
				. " updated = NOW(),"
				. " first_name = $this->first_name,"
				. " last_name = $this->last_name,"
				. " company = $this->company,"
				. " address = $this->address,"
				. " address2 = $this->address2,"
				. " city = $this->city,"
				. " state = $this->state,"
				. " zip = $this->zip,"
				. " country = $this->country,"
				. " phone = $this->phone,"
				. " fax = $this->fax,"
				. " email = $this->email,"
				. " url = $this->url"
				. " WHERE account_id = $this->account_id";
				$result=Self::insertSqlMain($sql);
			}
		
	else {
				$sql = "INSERT INTO Contact"
					. " (account_id, updated, entered, first_name, last_name, company, address, address2, city, state, zip, country,phone, fax, email, url)"
					. " VALUES"
					. " ($this->account_id, NOW(), NOW(), $this->first_name, $this->last_name, $this->company, $this->address, $this->address2, $this->city, $this->state, $this->zip, $this->country,$this->phone, $this->fax, $this->email, $this->url)";
				
				Self::insertSqlMain($sql);
				

			}


			$this->prepareToUse();
		}

		function Delete() {
			$dbObj = db_getDBObject(DEFAULT_DB,true);;
			$sql = "DELETE FROM Contact WHERE account_id = $this->account_id";
			$dbObj->query($sql);
		}

		function getAccountIdsByFirstName($first_name) {
			
			$dbObj = db_getDBObject(DEFAULT_DB,true);;
			$sql = "SELECT account_id FROM Contact WHERE first_name LIKE '%" . $first_name . "%'";
			$res = $dbObj->query($sql);
			while ($row = mysql_fetch_assoc($res)) {
				$ids[] = $row['account_id'];
			}
			
			return $ids;
		}

		function getAccountIdsByLastName($last_name) {
			
			$dbObj = db_getDBObject(DEFAULT_DB,true);;
			$sql = "SELECT account_id FROM Contact WHERE last_name LIKE '%" . $last_name . "%'";
			$res = $dbObj->query($sql);
			while ($row = mysql_fetch_assoc($res)) {
				$ids[] = $row['account_id'];
			}

			return $ids;
		}

		function getAccountIdsByPhone($phone) {
			
			$dbObj = db_getDBObject(DEFAULT_DB,true);;
			$sql = "SELECT account_id FROM Contact WHERE phone = '" . $phone . "'";
			$res = $dbObj->query($sql);
			while ($row = mysql_fetch_assoc($res)) {
				$ids[] = $row['account_id'];
			}

			return $ids;
		}
        
        
        public static function updateSql($sql)
    {
        $data= DB::connection('domain')->update($sql);
        return $data;
    }
    
     public static function fetch($sql)
        {
            $data= DB::connection('domain')->select($sql);
            return $data;
        }
        
        public static function fetchMain($sql)
        {
            $data= DB::select($sql);
            return $data;
        }
        
        public static function insertSql($sql)
        {
            $data= DB::connection('domain')->insert($sql);
            return $data;
        }
        
        public static function deleteSql($sql)
        {
            $data= DB::connection('domain')->delete($sql);
            return $data;
        }
        
        
         public static function updateSqlMain($sql)
        {
            $data= DB::update($sql);
            return $data;
        }
        
        
        
         public static function insertSqlMain($sql)
        {
            $data= DB::insert($sql);
            return $data;
        }

	}

?>