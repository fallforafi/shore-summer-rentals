<?php 
namespace App\Functions;
use App\Functions\Functions;
use DB,PDO;
use App\Models\Domain\Sql;	

	# ----------------------------------------------------------------------------------------------------
	# * FILE:PaymentListingLog.php
	# ----------------------------------------------------------------------------------------------------

	/**
	 * <code>
	 *		$paymentListingLogObj = new PaymentListingLog($id);
	 * <code
	 * @package Classes
	 * @name PaymentListingLog
	 * @met
	 * @method makeFromRow
	 * @method Save
	 * @access Public
	 */
	class PaymentListingLog extends Handle {

		/**
		 * @var integer
		 * @access Private
		 */
		var $payment_log_id;
		var $listing_id;
		var $listing_title;
		var $discount_id;
		var $level;
		var $level_label;
		var $renewal_date;
		var $categories;
		var $extra_categories;
		var $listingtemplate_title;
		var $amount;
		var $domain_id;

		/**
		 * <code>
		 *		$paymentListingLogObj = new PaymentListingLog($id);
		 * <code>
		 * @name PaymentListingLog
		 * @access Public
		 * @param integer $var
		 */
		function PaymentListingLog($var="", $domain_id = false) {

				$this->domain_id = $domain_id;
				if (is_numeric($var) && ($var)) {
				DB::connection('domain')->setFetchMode(PDO::FETCH_ASSOC);

				$row=DB::connection('domain')->table('Payment_Listing_Log')->where('payment_log_id',$var)->get();

				$this->makeFromRow($row);
			} else {
                if (!is_array($var)) {
                    $var = array();
                }
				$this->makeFromRow($var);
			}
		}

		/**
		 * <code>
		 *		$this->makeFromRow($row);
		 * <code>
		 * @version 8.0.00
		 * @name makeFromRow
		 * @access Public
		 * @param array $row
		 */
		function makeFromRow($row="") {

			$this->payment_log_id			= (isset($row["payment_log_id"]))			? $row["payment_log_id"]		: ($this->payment_log_id		? $this->payment_log_id			: 0);
			$this->listing_id				= ($row["listing_id"])				? $row["listing_id"]			: ($this->listing_id			? $this->listing_id				: 0);
			$this->listing_title			= ($row["listing_title"])			? $row["listing_title"]			: ($this->listing_title			? $this->listing_title			: "");
			$this->discount_id				= ($row["discount_id"])				? $row["discount_id"]			: ($this->discount_id			? $this->discount_id			: "");
            $this->level                    = ($row["level"])                   ? $row["level"]                 : ($this->level                 ? $this->level                  : 0);
			$this->level_label			    = ($row["level_label"])				? $row["level_label"]			: ($this->level_label			? $this->level_label			: "");
			$this->renewal_date				= ($row["renewal_date"])			? $row["renewal_date"]			: ($this->renewal_date			? $this->renewal_date			: 0);
			$this->categories				= ($row["categories"])				? $row["categories"]			: ($this->categories			? $this->categories				: 0);
			$this->extra_categories			= ($row["extra_categories"])		? $row["extra_categories"]		: ($this->extra_categories		? $this->extra_categories		: 0);
			$this->listingtemplate_title	= ($row["listingtemplate_title"])	? $row["listingtemplate_title"]	: ($this->listingtemplate_title	? $this->listingtemplate_title	: "");
			$this->amount					= ($row["amount"])					? $row["amount"]				: ($this->amount				? $this->amount					: 0);
				DB::connection('domain')->setFetchMode(PDO::FETCH_CLASS);


		}

		/**
		 * <code>
		 *		//Using this in forms or other pages.
		 *		$paymentListingLogObj->Save();
		 * <br /><br />
		 *		//Using this in PaymentListingLog() class.
		 *		$this->Save();
		 * </code>
		 * @name Save
		 * @access Public
		 */
		function Save() {


			$this->PrepareToSave();

			$sql = "INSERT INTO Payment_Listing_Log"
				. " (payment_log_id,"
				. " listing_id,"
				. " listing_title,"
				. " discount_id,"
				. " level,"
				. " level_label,"
				. " renewal_date,"
				. " categories,"
				. " extra_categories,"
				. " listingtemplate_title,"
				. " amount"
				. " )"
				. " VALUES"
				. " ("
				. " $this->payment_log_id,"
				. " $this->listing_id,"
				. " $this->listing_title,"
				. " $this->discount_id,"
				. " $this->level,"
				. " $this->level_label,"
				. " $this->renewal_date,"
				. " $this->categories,"
				. " $this->extra_categories,"
				. " $this->listingtemplate_title,"
				. " $this->amount"
				. " )";

			Sql::insertSql($sql);

			$this->id = DB::connection('domain')->getpdo()->lastInsertId();


			$this->PrepareToUse();

		}

	}

?>
