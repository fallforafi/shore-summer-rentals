<?php
namespace App\Functions;
use DB;
use App\Functions\Functions;
use App\Models\Domain\Sql;
use App\Models\Main\MainSql;

	/*==================================================================*\
	######################################################################
	#                                                                    #
	#           #
	#                                                                    #
	# This file may not be redistributed in whole or part.               #
	# eDirectory is licensed on a per-domain basis.                      #
	#                                                                    #
	# ---------------- eDirectory IS NOT FREE SOFTWARE ----------------- #
	#                                                                    #
	##
	######################################################################
	\*==================================================================*/

	# ----------------------------------------------------------------------------------------------------
	# * FILE: /classes/class_Invoice.php
	# ----------------------------------------------------------------------------------------------------

	/**
	 * <code>
	 *		$invoiceObj = new Invoice($id);
	 * <code>
	 * @package Classes
	 * @name Invoice
	 * @method Invoice
	 * @method makeFromRow
	 * @method Save
	 * @method Delete
	 * @access Public
	 */

	class Invoice extends Handle {

		/**
		 * @var integer
		 * @access Private
		 */
		var $id;
		/**
		 * @var integer
		 * @access Private
		 */
		var $account_id;
		/**
		 * @var string
		 * @access Private
		 */
		var $username;
		/**
		 * @var string
		 * @access Private
		 */
		var $ip;
		/**
		 * @var date
		 * @access Private
		 */
		var $date;
		/**
		 * @var char
		 * @access Private
		 */
		var $status;
		/**
		 * @var real
		 * @access Private
		 */
		var $amount;
		/**
		 * @var integer
		 * @access Private
		 */
		var $tax_amount;
		/**
		 * @var real
		 * @access Private
		 */
		var $subtotal_amount;
		/**
		 * @var string
		 * @access Private
		 */
		var $currency;
		/**
		 * @var date
		 * @access Private
		 */
		var $expire_date;
		/**
		 * @var date
		 * @access Private
		 */
		var $payment_date;

		/**
		 * <code>
		 *		$invoiceObj = new Invoice($id);
		 * <code>
		 * @copyright Copyright 2005 Arca Solutions, Inc.
		 * @author Arca Solutions, Inc.
		 * @version 8.0.00
		 * @name Invoice
		 * @access Public
		 * @param integer $var
		 */
		function Invoice($var="") {
		
			if (is_numeric($var) && ($var)) {
					
				$sql = "SELECT * FROM Invoice WHERE id = $var";
				$row = Sql::fetch($sql);
				$this->makeFromRow($row);

			} else {
                if (!is_array($var)) {
                    $var = array();
                }
               // d($var["account_id"],1);die;
				$this->arrayMakeFromRow($var);
			}

		}

		/**
		 * <code>
		 *		$this->makeFromRow($row);
		 * <code>
		 * @copyright Copyright 2005 Arca Solutions, Inc.
		 * @author Arca Solutions, Inc.
		 * @version 8.0.00
		 * @name makeFromRow
		 * @access Public
		 * @param array $row
		 */
		function makeFromRow($row="") {
			//$invoiceStatusObj = new InvoiceStatus();

			$this->id				= (isset($row[0]->id))				? $row[0]->id				: ($this->id				? $this->id					: 0);
			$this->account_id		= (isset($row[0]->account_id))		? $row[0]->account_id	: ($this->account_id		? $this->account_id			: 0);
			$this->username			= (isset($row[0]->username))		? $row[0]->username			: ($this->username			? $this->username			: "");
			$this->ip				= (isset($row[0]->ip))				? $row[0]->ip				: ($this->ip				? $this->ip					: "");
			$this->date				= (isset($row[0]->date))			? $row[0]->date				: ($this->date				? $this->date				: "");
			$this->status			= (isset($row[0]->status))			? $row[0]->status		: ($this->status			? $this->status				: 'P');
			$this->amount			= (isset($row[0]->amount))			? $row[0]->amount			: ($this->amount			? $this->amount				: 0);
			$this->tax_amount		= (isset($row[0]->tax_amount))		? $row[0]->tax_amount	: ($this->tax_amount		? $this->tax_amount			: 0);
			$this->subtotal_amount	= (isset($row[0]->subtotal_amount))	? $row[0]->subtotal_amount	: ($this->subtotal_amount	? $this->subtotal_amount	: 0);
			$this->currency			= (isset($row[0]->currency))		? $row[0]->currency			: ($this->currency			? $this->currency			: "");
			$this->expire_date		= (isset($row[0]->expire_date))		? $row[0]->expire_date		: ($this->expire_date		? $this->expire_date		: "");
			$this->payment_date		= (isset($row[0]->payment_date))	? $row[0]->payment_date		: ($this->payment_date		? $this->payment_date		: 0);

		}
		function arrayMakeFromRow($row="") {


				$this->id				= (isset($row["id"]))				? $row["id"]				: ($this->id				? $this->id					: 0);
			$this->account_id		= ($row["account_id"])		? $row["account_id"]		: ($this->account_id		? $this->account_id			: 0);
			$this->username			= ($row["username"])		? $row["username"]			: ($this->username			? $this->username			: "");
			$this->ip				= ($row["ip"])				? $row["ip"]				: ($this->ip				? $this->ip					: "");
			$this->date				= ($row["date"])			? $row["date"]				: ($this->date				? $this->date				: "");
			$this->status			= ($row["status"])			? $row["status"]			: ($this->status			? $this->status				:'P');
			$this->amount			= ($row["amount"])			? $row["amount"]			: ($this->amount			? $this->amount				: 0);
			$this->tax_amount		= ($row["tax_amount"])		? $row["tax_amount"]		: ($this->tax_amount		? $this->tax_amount			: 0);
			$this->subtotal_amount	= ($row["subtotal_amount"])	? $row["subtotal_amount"]	: ($this->subtotal_amount	? $this->subtotal_amount	: 0);
			$this->currency			= ($row["currency"])		? $row["currency"]			: ($this->currency			? $this->currency			: "");
			$this->expire_date		= ($row["expire_date"])		? $row["expire_date"]		: ($this->expire_date		? $this->expire_date		: "");
			$this->payment_date		= (isset($row["payment_date"]))	? $row["payment_date"]		: ($this->payment_date		? $this->payment_date		: 0);

		}

		/**
		 * <code>
		 *		//Using this in forms or other pages.
		 *		$invoiceObj->Save();
		 * <br /><br />
		 *		//Using this in Invoice() class.
		 *		$this->Save();
		 * </code>
		 * @copyright Copyright 2005 Arca Solutions, Inc.
		 * @author Arca Solutions, Inc.
		 * @version 8.0.00
		 * @name Save
		 * @access Public
		 */
		function Save($updateDashboard = false) {

			//$this->PrepareToSave();

			if ($this->id) {

				$sql  = "UPDATE Invoice SET"
					. " account_id = $this->account_id,"
					. " username = $this->username,"
					. " ip = $this->ip,"
					. " date = $this->date,"
					. " status = $this->status,"
					. " amount = $this->amount,"
					. " tax_amount = $this->tax_amount,"
					. " subtotal_amount = $this->subtotal_amount,"
					. " currency = $this->currency,"
					. " expire_date = $this->expire_date,"
					. " payment_date = $this->payment_date"
					. " WHERE id = $this->id";
					
					Sql::insertSql($sql);

					if ($this->status == "'R'" && $updateDashboard)
						activity_newActivity(SELECTED_DOMAIN_ID, $this->account_id, $this->amount, "payment");

			} else {

				$sql = "INSERT INTO Invoice"
					. " (account_id,"
					. " username,"
					. " ip,"
					. " date,"
					. " status,"
					. " amount,"
					. " tax_amount,"
					. " subtotal_amount,"
					. " currency,"
					. " expire_date,"
					. " payment_date"
					. " )"
					. " VALUES"
					. " ("
					. " $this->account_id,"
					. " '$this->username',"
					. " '$this->ip',"
					. " '$this->date',"
					. " '$this->status',"
					. " $this->amount,"
					. " $this->tax_amount,"
					. " $this->subtotal_amount,"
					. " '$this->currency',"
					. " '$this->expire_date',"
					. " '$this->payment_date'"
					. " )";

				$id=Sql::insertSqlId($sql);

				$this->id = $id;

			}

			$this->PrepareToUse();

		}
        
        /**
		 * <code>
		 *		//Using this in forms or other pages.
		 *		$invoiceObj->Delete();
		 * <code>
		 * @version 8.0.00
		 * @name Delete
		 * @access Public
		 */
		function Delete(){
            //$dbMain = db_getDBObject(DEFAULT_DB, true);
            //$dbObj = db_getDBObjectByDomainID(SELECTED_DOMAIN_ID, $dbMain);
            
            $sql = "UPDATE Invoice SET hidden = 'y' WHERE id = ".$this->id;
            Sql::insertSql($sql);
        }

	

	}

?>
